/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/components/common.js":
/*!*******************************************!*\
  !*** ./resources/js/components/common.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var common = {
  init: function init() {
    this.avoidConsoleErrors();
    this.addUseragentToDoc();
    this.disableClick();
    this.detectTouchEvents();
    this.disableSubmitAfterClick();
    this.initAcquiringNotifications();
    /* START: Init Lozad */

    var observer = lozad(); // lazy loads elements with default selector as '.lozad'

    observer.observe();
    /* END: Init Lozad */
  },
  avoidConsoleErrors: function avoidConsoleErrors() {
    var method;

    var noop = function noop() {};

    var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'];
    var length = methods.length;
    var console = window.console = window.console || {};

    while (length--) {
      method = methods[length]; // Only stub undefined methods.

      if (!console[method]) {
        console[method] = noop;
      }
    }
  },
  addUseragentToDoc: function addUseragentToDoc() {
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
  },
  disableClick: function disableClick() {
    $("body").on('click', '.no-click', function (e) {
      return false;
    });
  },
  detectTouchEvents: function detectTouchEvents() {
    var hasTouch = ('ontouchstart' in window);

    if (hasTouch) {
      $('html').addClass('touch');
    }
  },
  disableSubmitAfterClick: function disableSubmitAfterClick() {
    var allowSubmit = true;
    $('input[type=submit]').on('click', function (e) {
      if (!allowSubmit) {
        return false;
      }

      var form = $(this).closest('form')[0];
      var valid = true;

      for (var f = 0; f < form.elements.length; f++) {
        var field = form.elements[f];

        if (!field.validity.valid) {
          valid = false;
          break;
        }
      }

      if (valid) {
        allowSubmit = false;
        return true;
      }
    });
  },
  initAcquiringNotifications: function initAcquiringNotifications() {
    setTimeout(function () {
      $('#acquiring-notification').removeClass('close');
    }, 1000);
    $('#acquiring-notification a.close').on('click', function (e) {
      $(this).closest('#acquiring-notification').remove();
    });
  }
};
module.exports = common;

/***/ }),

/***/ "./resources/js/components/header.js":
/*!*******************************************!*\
  !*** ./resources/js/components/header.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  init: function init(el) {
    initHeaderSearchForm();
    var resizeDebounceHandler = null;
    $(window).on("resize", function (e) {
      e.preventDefault();

      if (resizeDebounceHandler !== null) {
        clearTimeout(resizeDebounceHandler);
      }

      resizeDebounceHandler = setTimeout(swapNavPositions, 300);
    });
    swapNavPositions();
    $("#hamburger").click(function (e) {
      e.preventDefault();
      $(this).toggleClass('open');
      $("header").toggleClass('open-mobile-nav');
    });
    /*-----------------*/

    function swapNavPositions() {
      var windowWidth = $(window).width();

      if (windowWidth <= 960) {
        $("nav.secondary-nav").after($("nav.main-nav"));
        $('#search').after($('#search-form'));
        $('#search-form').after($('#search-result'));
      } else {
        $("nav.main-nav").after($("nav.secondary-nav"));
        $('#search').append($('#search-form'));
        $('#search-form').append($('#search-result'));
      }
    }

    function initHeaderSearchForm() {
      if ($("#search #search-trigger").length > 0) {
        $("#search-trigger", "#search").on("click", function (e) {
          e.preventDefault();
          $("#search-form").toggleClass("show");
          $("#search").toggleClass("show");
        });
      }

      $(document).mouseup(function (e) {
        var $click_outside = $("#search-form");

        if (!$click_outside.is(e.target) // if the target of the click isn't the container...
        && $click_outside.has(e.target).length === 0) // ... nor a descendant of the container
          {
            $("#search-form").removeClass("show");
            $("#search").removeClass("show");
            $("#search-result").removeClass("show");
          }
      });
      /*----------------------------------------------*/

      /* START: Search Form */

      if ($("#search-form").length > 0) {
        var searchEventHandler;
        $("#search-box").on('keyup', function (e) {
          e.preventDefault();
          clearTimeout(searchEventHandler);
          var q = $(this).val();

          if (e.keyCode == 13 && _q.length > 2) {
            search(q);
          }

          if (q == "") {
            resetSearchForm();
          } else if (q.length > 2) {
            searchEventHandler = setTimeout(function () {
              search(q);
            }, 500);
          } else {
            return;
          }
        });
      }
      /* END: Search Form */

    }

    function resetSearchForm() {
      $("#search-box").val('');
      $("#search-result").removeClass("show");
    }

    function search(q) {
      if (q == '' || q == ' ') {
        resetSearchForm();
        return;
      }

      $.get("/search", {
        'q': q
      }, function (data) {
        var result = $.parseJSON(data);
        var item_string = '';
        var ul = $('<ul />');
        $.each(result, function (i, item) {
          var type = $('<div />', {
            "class": 'type',
            text: item.type
          });
          var title = $('<div />', {
            "class": 'title',
            text: item.title
          });

          if (item.link) {
            var anchor = $('<a />', {
              href: item.link
            }).append(title).append(type);
          } else {
            var anchor = $('<div />').append(title).append(type);
          }

          var li = $('<li />', {
            "class": item.type
          }).append(anchor);
          ul.append(li);
        });
        var list = $('<div />', {
          "class": 'list'
        }).append(ul);
        $("#search-result").html(list);

        var _height = $(window).height() - $("#search-result").position().top - 20;

        $("#search-result").css('max-height', _height).addClass('show');
      });
    }
  }
};

/***/ }),

/***/ "./resources/js/components/newsletterSignup.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/newsletterSignup.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var newsletterSignup = {
  init: function init(el) {
    this.element = $(el);
    this.element.on('click', '.why-signup', function (self) {
      return function (e) {
        e.preventDefault();
        self.element.toggleClass('signup-benefits');
      };
    }(this));
    var resizeDebounceHandler = null;
    $(window).on("resize", function (e) {
      e.preventDefault();

      if (resizeDebounceHandler !== null) {
        clearTimeout(resizeDebounceHandler);
      }

      resizeDebounceHandler = setTimeout(swapPositions, 300);
    });
    swapPositions();
    /*-----------------*/

    function swapPositions() {
      var windowWidth = $(window).width();

      if (windowWidth <= 960) {
        $("form > .email", el).after($("form > .buttons", el));
      } else {
        $("form > .buttons", el).after($("form > .email", el));
      }
    }
  }
};
module.exports = newsletterSignup;

/***/ }),

/***/ "./resources/js/components/youtubeVideo.js":
/*!*************************************************!*\
  !*** ./resources/js/components/youtubeVideo.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var videos = {};

var util = __webpack_require__(/*! ../modules/util */ "./resources/js/modules/util.js");

var youtubeVideo = {
  init: function init(el) {
    this.element = $(el);
    this.playerDefaults = {
      autoplay: 0,
      autohide: 1,
      loop: 0,
      playlist: '',
      modestbranding: 1,
      rel: 0,
      showinfo: 0,
      controls: 0,
      disablekb: 1,
      enablejsapi: 0,
      iv_load_policy: 3
    };
    this.element.each(function () {
      var video = $(this).find('.video');
      var videoId = video.data('video-id');
      if (!video.attr('id')) video.attr("id", "id_" + videoId + util.getUniqueString());
    });
    this.addYoutubeAPI();
  },
  addYoutubeAPI: function addYoutubeAPI() {
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); // Add Youtube API ready function

    window['onYouTubePlayerAPIReady'] = function () {
      $('.youtube-video').each(function () {
        var video = $(this).find('.video');
        var elementId = video.attr('id');
        var playerVars = $.extend({}, youtubeVideo.playerDefaults);
        var options = {
          'videoId': video.data('video-id'),
          'mute': video.data('mute') ? video.data('mute') : 0
        };
        $.each(playerVars, function (index, value) {
          if (!util.isUndefined(video.data(index))) {
            playerVars[index] = video.data(index);
            options[index] = video.data(index);
          }
        });
        videos[elementId] = {};
        videos[elementId]['options'] = options;
        videos[elementId]['player'] = new YT.Player(elementId, {
          videoId: options.videoId,
          events: {
            'onReady': youtubeVideo.onPlayerReady,
            'onStateChange': youtubeVideo.onPlayerStateChange
          },
          playerVars: playerVars
        });
      });
    };

    $(".youtube-video").on('click', '.play', function (e) {
      e.preventDefault();
      var elementId = $(this).closest('.youtube-video').find('.video').attr('id');
      var videoHandler = videos[elementId]['player'];
      videoHandler.playVideo();
    });
    $(".youtube-video").on('click', '.pause', function (e) {
      e.preventDefault();
      var elementId = $(this).closest('.youtube-video').find('.video').attr('id');
      var videoHandler = videos[elementId]['player'];
      videoHandler.pauseVideo();
    });
  },
  onPlayerReady: function onPlayerReady(e) {
    var elementId = e.target.getIframe().id;
    var videoHandler = videos[elementId]['player'];
    var mute = videos[elementId]['options']['mute'];
    var autoplay = videos[elementId]['options']['autoplay'];

    if (!util.isUndefined(autoplay) && autoplay) {
      videoHandler.playVideo();
    }

    if (!util.isUndefined(mute) && mute) {
      videoHandler.mute();
    }
  },
  onPlayerStateChange: function onPlayerStateChange(e) {
    var elementId = e.target.getIframe().id;
    var element = $(e.target.getIframe());
    var loop = videos[elementId]['options']['loop'];
    var videoHandler = videos[elementId]['player'];
    var classes = ['video-unstarted', 'video-ended', 'video-playing', 'video-paused', 'video-buffering', 'video-cued'];
    classes.forEach(function (item) {
      element.parent().removeClass(item);
    });
    element.parent().addClass(classes[e.data + 1]);

    if (e.data === 0 && !util.isUndefined(loop) && loop) {
      videoHandler.seekTo(0);
    }
  }
};
module.exports = youtubeVideo;

/***/ }),

/***/ "./resources/js/modules/componentLoader.js":
/*!*************************************************!*\
  !*** ./resources/js/modules/componentLoader.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var util = __webpack_require__(/*! ./util */ "./resources/js/modules/util.js");
/**
 * Registers and executes callbacks based on instances of an element
 *
 * @class ComponentLoader
 */


var ComponentLoader = /*#__PURE__*/function () {
  function ComponentLoader() {
    _classCallCheck(this, ComponentLoader);

    this.components = [];
  }
  /**
   * Register a component selector and callback
   *
   * @param  {string} sel - A CSS-like selector which will activate the component JS
   * @param  {function} componentCb - The function to invoke when the component is found on the page
   * @return {ComponentLoader}
   */


  _createClass(ComponentLoader, [{
    key: "register",
    value: function register(sel, componentCb) {
      this.components.push({
        sel: sel,
        component: componentCb
      });
      return this;
    }
    /**
     * Activate the component JS code for each matched component element on the page
     *
     * @param  {HTMLElement} parentEl - the base element to find the component from
     * @return {ComponentLoader}
     */

  }, {
    key: "load",
    value: function load(parentEl) {
      this.components.forEach(function (component) {
        if ($(component.sel, $(parentEl)).length) {
          component.component.init(component.sel);
        }
      });
      /*function(component) {
      if($(component.sel, $(parentEl)).length)
      {
          component.component.init(component.sel);
      }
      });*/

      /*(component) => {
      $(component.sel, $(parentEl)).each(function () {
          component.component.init(this);
      });
      }*/

      return this;
    }
  }]);

  return ComponentLoader;
}();

;
module.exports = new ComponentLoader();

/***/ }),

/***/ "./resources/js/modules/util.js":
/*!**************************************!*\
  !*** ./resources/js/modules/util.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// Helper methods to abstract and simplify code
module.exports = {
  isUndefined: function isUndefined(arg) {
    return typeof arg === "undefined";
  },
  isDefined: function isDefined(arg) {
    return typeof arg !== "undefined";
  },
  isString: function isString(arg) {
    return typeof arg === "string";
  },
  isInteger: function isInteger(arg) {
    return arg === parseInt(arg);
  },
  isObject: function isObject(arg) {
    if (_typeof(arg) === "object") {
      var isObj = true;

      for (var prop in arg) {
        if (!arg.hasOwnProperty(prop)) {
          isObj = false;
        }

        break;
      }

      return isObj;
    } else {
      return false;
    }
  },
  isNull: function isNull(arg) {
    return arg === null;
  },
  isArray: function isArray(arg) {
    return arg instanceof Array;
  },
  isFunction: function isFunction(arg) {
    return typeof arg === "function";
  },
  noop: function noop() {
    return false;
  },
  setDebugMode: function setDebugMode(el, isSet) {
    var classApplierMethod = isSet ? "addClass" : "removeClass";

    if (el) {
      $(el)[classApplierMethod]("debug");
    }
  },
  doIfExists: function doIfExists(selector, ifExistsFn, ifNotExistsFn) {
    if (typeof selector !== "string" || !selector.length) {
      return false;
    }

    if ($(selector).length) {
      if (this.isFunction(ifExistsFn)) {
        ifExistsFn();
      }

      return true;
    } else {
      if (this.isFunction(ifNotExistsFn)) {
        ifNotExistsFn();
      }

      return false;
    }
  },
  getUniqueString: function getUniqueString() {
    return ("0000" + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
  },
  debounce: function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
          args = arguments;

      var later = function later() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };

      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }
};

/***/ }),

/***/ "./resources/js/script.js":
/*!********************************!*\
  !*** ./resources/js/script.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function ($) {
  "use strict"; //Hide Loading Box (Preloader)

  function handlePreloader() {
    if ($('.preloader').length) {
      $('.preloader').delay(100).fadeOut(100);
    }
  } //Update Header Style and Scroll to Top


  function headerStyle() {
    if ($('.main-header').length) {
      var windowpos = $(window).scrollTop();
      var siteHeader = $('.main-header');
      var scrollLink = $('.scroll-to-top');

      if (windowpos >= 200) {
        siteHeader.addClass('fixed-header');
        scrollLink.fadeIn(300);
      } else {
        siteHeader.removeClass('fixed-header');
        scrollLink.fadeOut(300);
      }
    }
  }

  headerStyle(); // Sticky header

  function stickyHeader() {
    if ($('.main-header').length) {
      var sticky = $('.main-header'),
          scroll = $(window).scrollTop();
      if (scroll >= 265) sticky.addClass('fixed-header');else sticky.removeClass('fixed-header');
    }

    ;
  } //Submenu Dropdown Toggle


  if ($('.main-header .navigation li.dropdown ul').length) {
    $('.main-header .navigation li.dropdown').append('<div class="dropdown-btn"><span class="fa fa-angle-down"></span></div>'); //Dropdown Button

    $('.main-header .navigation li.dropdown .dropdown-btn').on('click', function () {
      $(this).prev('ul').slideToggle(500);
    }); //Disable dropdown parent link

    $('.main-header .navigation li.dropdown > a,.hidden-bar .side-menu li.dropdown > a').on('click', function (e) {
      e.preventDefault();
    });
  } // Home popup 


  if ($('#home-popup').length) {
    $('#home-popup').modal('show');
    $('#home-popup a.close').on('click', function (e) {
      $('#home-popup').modal('hide');
    });
  } //Revolution Slider

  /* if ($('.main-slider .tp-banner').length) {
      
      var MainSlider = $('.main-slider');
      var strtHeight = MainSlider.attr('data-start-height');
      var slideOverlay = "'" + MainSlider.attr('data-slide-overlay') + "'";
      
      $('.main-slider .tp-banner').show().revolution({
          dottedOverlay: slideOverlay,
          delay: 10000,
          startwidth: 1200,
          startheight: strtHeight,
          hideThumbs: 600,
          
          thumbWidth: 80,
          thumbHeight: 50,
          thumbAmount: 5,
          
          navigationType: "bullet",
          navigationArrows: "0",
          navigationStyle: "preview3",
          
          touchenabled: "on",
          onHoverStop: "off",
          
          swipe_velocity: 0.7,
          swipe_min_touches: 1,
          swipe_max_touches: 1,
          drag_block_vertical: false,
          
          parallax: "mouse",
          parallaxBgFreeze: "on",
          parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
          
          keyboardNavigation: "off",
          
          navigationHAlign: "center",
          navigationVAlign: "bottom",
          navigationHOffset: 0,
          navigationVOffset: 40,
          
          soloArrowLeftHalign: "left",
          soloArrowLeftValign: "center",
          soloArrowLeftHOffset: 0,
          soloArrowLeftVOffset: 0,
          
          soloArrowRightHalign: "right",
          soloArrowRightValign: "center",
          soloArrowRightHOffset: 0,
          soloArrowRightVOffset: 0,
          
          shadow: 0,
          fullWidth: "on",
          fullScreen: "off",
          
          spinner: "spinner4",
          
          stopLoop: "off",
          stopAfterLoops: -1,
          stopAtSlide: -1,
          
          shuffle: "off",
          
          autoHeight: "off",
          forceFullWidth: "on",
          
          hideThumbsOnMobile: "on",
          hideNavDelayOnMobile: 1500,
          hideBulletsOnMobile: "on",
          hideArrowsOnMobile: "on",
          hideThumbsUnderResolution: 0,
          
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          startWithSlide: 0,
          videoJsPath: "",
          fullScreenOffsetContainer: ""
      });
      
  } */
  //Accordion Box


  if ($('.accordion-box').length) {
    $(".accordion-box").on('click', '.acc-btn', function () {
      var outerBox = $(this).parents('.accordion-box');
      var target = $(this).parents('.accordion');

      if ($(this).hasClass('active') !== true) {
        $('.accordion .acc-btn').removeClass('active');
      }

      if ($(this).next('.acc-content').is(':visible')) {
        return false;
      } else {
        $(this).addClass('active');
        $(outerBox).children('.accordion').removeClass('active-block');
        $(outerBox).find('.accordion').children('.acc-content').slideUp(300);
        target.addClass('active-block');
        $(this).next('.acc-content').slideDown(300);
      }
    });
  } // Scroll to a Specific Div


  if ($('.scroll-to-target').length) {
    $(".scroll-to-target").on('click', function () {
      var target = $(this).attr('data-target'); // animate

      $('html, body').animate({
        scrollTop: $(target).offset().top
      }, 1500);
    });
  }

  $('#logo-slider, #client-slider').slick({
    dots: true,
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 7,
    variableWidth: false,
    autoplay: true,
    autoplaySpeed: 3500,
    arrows: false,
    rtl: $('body').attr('dir') == 'rtl' ? true : false,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 5
      }
    }, {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4
      }
    }, {
      breakpoint: 960,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 640,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 599,
      settings: {
        autoplay: false,
        dots: false,
        slidesToShow: 1
      }
    }]
  });
  $('#testi-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: false,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    dots: false,
    adaptiveHeight: true,
    rtl: $('body').attr('dir') == 'rtl' ? true : false
  });

  if ($(window).width() < 599) {
    $('#loc').slick({
      responsive: [{
        breakpoint: 599,
        settings: {
          autoplay: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplaySpeed: 5000,
          dots: false,
          arrows: false
        }
      }]
    });
  }

  var ihs = function () {
    __webpack_require__(/*! ./components/common */ "./resources/js/components/common.js").init();

    var componentLoader = __webpack_require__(/*! ./modules/componentLoader */ "./resources/js/modules/componentLoader.js");

    componentLoader.register("header", __webpack_require__(/*! ./components/header */ "./resources/js/components/header.js")).register(".youtube-video", __webpack_require__(/*! ./components/youtubeVideo */ "./resources/js/components/youtubeVideo.js")).register("#newsletter-signup", __webpack_require__(/*! ./components/newsletterSignup */ "./resources/js/components/newsletterSignup.js")).load(document.body);
  }();
  /* ==========================================================================
   When document is ready, do
   ========================================================================== */


  $(document).ready(function () {
    handlePreloader();
    $(".hamburger-nav").on("click", function () {
      // $(".menu").fadeToggle("slow").toggleClass("menu-hide");
      $(".menu").animate({
        height: 'toggle'
      });
    });
  });
  /* ==========================================================================
   When document is Scrollig, do
   ========================================================================== */

  $(window).on('scroll', function () {
    stickyHeader();
    headerStyle();
  });
  /* ==========================================================================
   When document is loading, do
   ========================================================================== */

  $(window).on('load', function () {
    //new LazyLoad();
    var observer = lozad(); // lazy loads elements with default selector as '.lozad'

    observer.observe();
  });
})(window.jQuery);

/***/ }),

/***/ 1:
/*!**************************************!*\
  !*** multi ./resources/js/script.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/sohail/Sites/middleeast-psionline/resources/js/script.js */"./resources/js/script.js");


/***/ })

/******/ });