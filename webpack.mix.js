let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (!mix.inProduction()) {
    mix.copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts');
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'resources/js/libs');
    mix.copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'resources/js/libs');
    mix.copy( 'resources/js/libs/modernizr-2.8.3-respond-1.4.2.min.js',  'public/js');
}


/* START: CMS */

mix.options({processCssUrls: false}).sass('resources/sass/cms/styles.scss', 'public/css/cms')
    .sass('resources/sass/cms/editor-content.scss', 'public/css/cms')
    .scripts([
            'resources/js/libs/jquery.min.js',
            'resources/js/libs/bootstrap.min.js',
            'resources/js/libs/dropzone.js',
            'resources/js/libs/isotope.pkgd.min.js',
            'resources/js/libs/imagesloaded.pkgd.min.js',
            'resources/js/libs/jquery-confirm.min.js',
            'resources/js/libs/moment.js',
            'resources/js/libs/bootstrap-datetimepicker.min.js',
            'resources/js/libs/jquery.dataTables.min.js',
            'resources/js/libs/dataTables.bootstrap.min.js',
            'resources/js/libs/jquery-ui.js',
            
            'resources/js/cms/scripts.js',
        ],
        'public/js/cms/scripts.js'
    );

/* END: CMS */



mix.options({processCssUrls: false}).sass('resources/sass/styles.scss', 'public/css')
    .sass('resources/sass/styles-ar.scss', 'public/css')
    
    
    .scripts([
            'resources/js/libs/jquery.min.js',
            'resources/js/libs/slick.js',
            'resources/js/libs/packery.pkgd.min.js',
            'resources/js/libs/imagesloaded.pkgd.min.js',
            'resources/js/libs/gmap3.js',
            'resources/js/libs/lozad.min.js'
        ],
        'public/js/plugins.js')
    .js('resources/js/scripts.js', 'public/js');



mix.sass('resources/sass/style.scss', 'public/css')
    .scripts([
            'resources/js/libs/jquery.min.js',
            'resources/js/libs/slick.js',
            'resources/js/libs/bootstrap.min.js',
            'resources/js/libs/iziModal.js',
            //'resources/js/libs/lazyload.min.js',
            'resources/js/libs/lozad.min.js'
        ],
        'public/js/home.js')
    .js('resources/js/script.js', 'public/js');


