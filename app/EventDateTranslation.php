<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDateTranslation extends Model
{
    protected $fillable = ['locale', 'location'];
    public $timestamps = false;
}
