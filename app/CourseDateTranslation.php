<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseDateTranslation extends Model
{
    protected $fillable = ['locale', 'location'];
    public $timestamps = false;
}
