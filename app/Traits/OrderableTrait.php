<?php
namespace App\Traits;

trait OrderableTrait
{

    public function updateOrder($order, $orderItem = null)
    {
        $orderItem = $orderItem?$this->findOrFail($orderItem):null;

        if ($order == 'before') {
            $this->moveToLeftOf($orderItem);
        } elseif ($order == 'after') {
            $this->moveToRightOf($orderItem);
        } elseif ($order == 'childOf') {
            $this->makeChildOf($orderItem);
        } elseif ($order == 'root') {
            $this->makeRoot();
        }
    }
}
