<?php
namespace App\Traits;

trait CustomFieldsTrait
{

    public function customFields()
    {
        return $this->hasMany(\App\CustomField::class, 'model_id')
            ->where('custom_fields.model_type', get_class($this));
    }

    public function getCustomFieldsArrayAttribute()
    {

        $customFields = [];
        foreach ($this->customFields as $field) {
            $customFields[$field->name] = $field->value;
        }

        return $customFields;
    }

    public function getCustomFieldsCollectionAttribute()
    {
        return collect($this->customFieldsArray);
    }
}
