<?php
namespace App\Traits;

use Image;
use File;
use Illuminate\Http\UploadedFile;

trait CommonModelTrait
{

    public function scopeActive($query)
    {
        return $query->where('status', '1');
    }

    public function saveImage(UploadedFile $logoFile)
    {
        $imageColumnName = $this->getImageColumnName();

        $uploadDir = $this->getUploadDir();

        if (File::isDirectory('public/' . config('cms.uploads.paths.images') . '/' . $uploadDir)) {
            File::makeDirectory('public/' . config('cms.uploads.paths.images') . '/' . $uploadDir, 0755);
        }

        $name = sha1($this->id) . '.' . $logoFile->getClientOriginalExtension();

        $this->deleteThumbs($this->{$imageColumnName});

        Image::make($logoFile)
            ->resize(config('cms.uploads.maxImageWidth'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->interlace()
            ->save(config('cms.uploads.paths.images') . '/' . $uploadDir . '/' . $name, config('cms.uploads.imageQuality'));

        $this->fill([$imageColumnName => config('cms.uploads.paths.images') . '/' . $uploadDir . '/' . $name,
            $imageColumnName . '_tinified' => 0]);

        if ($this->isDirty()) {
            $this->save();
        }
    }

    public function getUploadDir()
    {
        return $this->uploadDir ?: $this->getTable();
    }

    public function getImageColumnName()
    {
        return $this->imageColumnName ?:'logo';
    }

    public function deleteThumbs($path = '')
    {

        if (empty($path)) {
            $path = $this->{$this->getImageColumnName()};
        }

        deleteThumbs($path);
    }

    /*public function deleteThumbs($path = ''){

        if(empty($path)) {
            $path = $this->{$this->getImageColumnName()};
        }

        if(!empty($path))
        {
            $pathChunks = explode("/", $path);
            $imageName = array_pop($pathChunks);

            $files = glob( implode("/", $pathChunks) . "/tn-*" . $imageName);
            foreach($files as $file) {
                if(File::isFile($file))
                {
                    File::delete($file);
                }
            }
        }
    }*/
}
