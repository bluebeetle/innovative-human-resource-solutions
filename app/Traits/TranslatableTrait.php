<?php
namespace App\Traits;

use App;
use App\Scopes\TranslationScope;

trait TranslatableTrait
{

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TranslationScope);
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        if ($this->isTranslationAttribute($key)) {
            $locale = App::getLocale();

            $translation = $this->translations->filter(function ($value, $k) use ($locale) {
                return $value->locale == $locale;
            });

            if ($translation->isEmpty()) {
                return;
            }

            return $translation->first()->$key;
        }

        return parent::getAttribute($key);
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function isTranslationAttribute($key)
    {
        return in_array($key, $this->translatedAttributes);
    }

    /**
     * @return string
     */
    public function getTranslationModelName()
    {
        return $this->translationModel ?: $this->getTranslationModelNameDefault();
    }

    /**
     * @return string
     */
    public function getTranslationModelNameDefault()
    {
        return get_class($this).'Translation';
    }

    /**
     * @return string
     */
    public function getRelationKey()
    {
        if ($this->translationForeignKey) {
            $key = $this->translationForeignKey;
        } elseif ($this->primaryKey !== 'id') {
            $key = $this->primaryKey;
        } else {
            $key = $this->getForeignKey();
        }

        return $key;
    }

    /**
     * @return string
     */
    public function getLocaleKey()
    {
        return $this->localeKey ?: 'locale';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany($this->getTranslationModelName(), $this->getRelationKey());
    }

    public function saveTranslation(array $attributes = [], $locale = null)
    {

        if (count($attributes)<=0) {
            return;
        }

        if (is_null($locale)) {
            $locale = App::getLocale();
        }

        $count = $this->translations()->where($this->getLocaleKey(), $locale)->count();

        if ($count > 0) {
            $this->translations()->where($this->getLocaleKey(), $locale)->update($attributes);
        } else {
            $this->translations()->create([$this->getLocaleKey() => $locale] + $attributes);
        }
    }

    public function getTranslation($locale = null)
    {

        if (is_null($locale)) {
            $locale = App::getLocale();
        }

        $translation = $this->translations->filter(function ($value, $key) use ($locale) {
            return $value->locale == $locale;
        });

        if ($translation->count() == 0) {
            $this->load(['translations' => function ($query) use ($locale) {
                $query->where('locale', $locale);
            }]);

            if ($this->translations->isEmpty()) {
                $translationModel = $this->getTranslationModelName();
                return new $translationModel;
            }

            return $this->getTranslation($locale);
        }

        return $translation->first();
    }
}
