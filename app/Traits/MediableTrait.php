<?php
namespace App\Traits;

trait MediableTrait
{

    public function media()
    {
        /**
         * Convert polymorphic many-to-many to simmple many-to-many with mediable_type field.
         */
        //return $this->morphToMany('App\Media', 'mediable')->withPivot('id', 'collection');

        return $this->belongsToMany(\App\Media::class, 'mediables', 'mediable_id', 'media_id')
            ->where('mediables.mediable_type', get_class($this))
            ->using(\App\MediablePivot::class)
            ->withPivot('id', 'collection');
    }

    public function getMediaArrayAttribute()
    {
        $media = [];

        foreach ($this->media as $file) {
            $tmp = [
                'name' => $file->name,
                'title' => $file->title,
                'type' => $file->type,
                'path' => $file->path,
                'caption' => $file->pivot->caption
            ];

            $media[$file->pivot->collection][] = $tmp;
        }

        return $media;
    }

    public function getMediaCollectionAttribute()
    {
        return collect($this->mediaArray);
    }

    public function getSortedMediaArray($sortBy = 'pivot.id', $sort = 'desc')
    {
        $media = [];

        $mediaFiles = $this->media;
        if (isset($sortBy) && $sort == 'desc') {
            $mediaFiles = $mediaFiles->sortByDesc($sortBy);
        } else if (isset($sortBy)) {
            $mediaFiles = $mediaFiles->sortBy($sortBy);
        }


        foreach ($mediaFiles as $file) {
            $tmp = [
                'name' => $file->name,
                'title' => $file->title,
                'type' => $file->type,
                'path' => $file->path,
                'caption' => $file->pivot->caption
            ];

            $media[$file->pivot->collection][] = $tmp;
        }

        return $media;
    }
}
