<?php

namespace App\Http\Controllers\CMS;

use Setting;
use App\Http\Requests;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    public function edit()
    {

        $settingVars = config('cms.settingVars');
        foreach ($settingVars as $groupKey => $group) {
            if (isset($group['vars'])) {
                foreach ($group['vars'] as $varKey => $var) {
                    $settingVars[$groupKey]['vars'][$varKey]['varName'] =
                        (isset($group['namePrefix'])?$group['namePrefix'].".":"")
                        .$var['name'];
                }
            }

            if (isset($group['groups'])) {
                foreach ($group['groups'] as $subGroupKey => $subGroup) {
                    foreach ($subGroup['vars'] as $varKey => $var) {
                        $settingVars[$groupKey]['groups'][$subGroupKey]['vars'][$varKey]['varName'] =
                            (isset($group['namePrefix'])?$group['namePrefix'].".":"")
                            .(isset($subGroup['namePrefix'])?$subGroup['namePrefix'].".":"")
                            .$var['name'];
                    }
                }
            }


            $settingVars[$groupKey] = castToObj($settingVars[$groupKey]);
        }



        return view('cms.settings.form', compact('settingVars'));
    }


    public function update(Request $request)
    {

        $data = $request->only('settings');
        Setting::setArray($data['settings']);

        flash()->success('Settings have been updated.');

        return back();
    }
}
