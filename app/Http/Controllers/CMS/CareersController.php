<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Support\Str;
use App\Job;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;

class CareersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::orderBy('created_at', 'desc')->paginate(30);

        return view('cms.careers.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Job $job)
    {
        return view('cms.careers.form', compact('job'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreJobRequest $request)
    {
        $job = Job::create($request->only('name', 'apply_link', 'status') + ['created_by' => $this->admin->id]);

        $job->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $job->id] + $request->only('title', 'name', 'content', 'status'));

        flash()->success('Job has been created.');

        return redirect()->route('cms.careers.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::findOrFail($id);

        return view('cms.careers.form', compact('job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\StoreJobRequest $request, $id)
    {
        $job = Job::findOrFail($id);

        $job->fill($request->only('name', 'apply_link', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        $job->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $job->id] + $request->only('title', 'name', 'content', 'status'));

        flash()->success('Job has been updated.');

        return redirect()->route('cms.careers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Job::destroy($id);

        flash()->success('Job has been deleted.');

        return back();
    }

    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('careers', $item['id'], $item['title'], $item['content'], route('careers') . '#' . Str::slug($item['name']));
        } else {
            Search::deleteItem('careers', $item['id']);
        }
    }
}
