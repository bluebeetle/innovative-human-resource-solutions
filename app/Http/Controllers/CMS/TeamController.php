<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Support\Str;
use File;
use Image;
use App\Employee;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;
use Baum\MoveNotPossibleException;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::orderBy('lft', 'asc')->get();

        return view('cms.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employee $employee)
    {
        $orderItems = Employee::orderBy('lft', 'asc')->get();

        return view('cms.employees.form', compact('employee', 'orderItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreEmployeeRequest $request)
    {
        $employee = Employee::create($request->only('name', 'facebook', 'twitter', 'linkedin', 'status') + ['created_by' => $this->admin->id]);

        /**
         * Save Translation
         */
        $employee->saveTranslation($request->only('title', 'designation', 'content'));

        $this->updateOrder($employee, $request);

        if ($request->hasFile('dp')) {
            $employee->saveDP($request->file('dp'));
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $employee->id] + $request->only('name', 'title', 'content', 'status'));

        flash()->success('Employee has been created.');

        return redirect()->route('cms.team.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);

        $orderItems = Employee::orderBy('lft', 'asc')->get();

        return view('cms.employees.form', compact('employee', 'orderItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateEmployeeRequest $request, $id)
    {
        $employee = Employee::findOrFail($id);

        if ($response = $this->updateOrder($employee, $request)) {
            return $response;
        }

        $employee->fill($request->only('name', 'facebook', 'twitter', 'linkedin', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        /**
         * Save Translation
         */
        $employee->saveTranslation($request->only('title', 'designation', 'content'));

        if ($request->hasFile('dp')) {
            $employee->saveDP($request->file('dp'));
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $employee->id] + $request->only('name', 'title', 'content', 'status'));

        flash()->success('Employee has been updated.');

        return redirect()->route('cms.team.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);

        File::delete($employee->dp);
        $employee->deleteThumbs();

        $employee->delete();

        flash()->success('Employee has been deleted.');

        return back();
    }

    public function sort(Requests\UpdateOrderingRequest $request)
    {

        $ids = explode("|", $request->input('ids'));
        $items = [];

        for ($i=0; $i<count($ids); $i++) {
            $id = $ids[$i];
            $items[$id] = Employee::findOrFail($id);
            if ($i>0) {
                try {
                    $items[$id]->moveToRightOf($items[$ids[$i-1]]);
                } catch (MoveNotPossibleException $e) {
                    return redirect(route('cms.team.index'))->withErrors([
                        'error' => 'Cannot make an item a child of itself.'
                    ]);
                }
            }
        }

        flash()->success('Order has been saved.');

        return redirect(route('cms.team.index'));
    }

    protected function updateOrder(Employee $employee, Request $request)
    {
        if ($request->has('order', 'orderItem')) {
            try {
                $employee->updateOrder($request->input('order'), $request->input('orderItem'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.team.edit', $employee->id))->withInput()->withErrors([
                    'error' => 'Cannot make an item a child of itself.'
                ]);
            }
        }
    }

    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('clients', $item['id'], $item['title'], $item['content'], route('team') . '#' . Str::slug($item['name']));
        } else {
            Search::deleteItem('clients', $item['id']);
        }
    }

    public function tinifyImage($id)
    {
        $item = Employee::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
