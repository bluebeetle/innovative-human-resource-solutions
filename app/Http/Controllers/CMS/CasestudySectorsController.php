<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;

use App\CasestudySector;
use App\Http\Requests;

class CasestudySectorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectors = CasestudySector::all();

        return view('cms.casestudySectors.index', compact('sectors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CasestudySector $sector)
    {
        return view('cms.casestudySectors.form', compact('sector'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreCasestudySectorRequest $request)
    {
        $sector = CasestudySector::create($request->only('name') + ['created_by' => $this->admin->id]);

        $sector->saveTranslation($request->only('title'));

        flash()->success('Sector has been created');

        return redirect()->route('cms.casestudySectors.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = CasestudySector::findOrFail($id);

        return view('cms.casestudySectors.form', compact('sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateCasestudySectorRequest $request, $id)
    {
        $sector = CasestudySector::findOrFail($id);

        $sector->fill($request->only('name') + ['updated_by' => $this->admin->id])
            ->save();

        $sector->saveTranslation($request->only('title'));

        flash()->success('Sector has been updated');

        return redirect()->route('cms.casestudySectors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sector = CasestudySector::findOrFail($id);

        $sector->delete();

        flash()->success('Sector has been deleted');

        return redirect()->route('cms.casestudySectors.index');
    }
}
