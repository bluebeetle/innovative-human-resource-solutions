<?php

namespace App\Http\Controllers\CMS;

use Image;
use File;
use App\Media;
use App\Http\Requests;

class MediaController extends Controller
{
    protected $media;

    public function __construct(Media $media)
    {

        $this->media = $media;

        parent::__construct();
    }

    public function index()
    {

        $files = $this->media->all();

        return view('cms.media.index', compact('files'));
    }

    public function store(Requests\UploadMediaRequest $request)
    {

        $file = $request->file('file');

        $newFile = $this->media->makeFile($file);
        $newFile->created_by = $this->admin->id;
        $newFile->save();

        if ($newFile->type == 'image') {
            Image::make($file)
                ->resize(config('cms.uploads.maxImageWidth'), null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->interlace()
                ->save(config('cms.uploads.paths.images') . '/' . $newFile->name, config('cms.uploads.imageQuality'));
        } else {
            $file->move(config('cms.uploads.paths.documents'), $newFile->name);
        }
    }

    public function edit($id)
    {

        $file = $this->media->findOrFail($id);

        return view('cms.media.edit', compact('file'));
    }

    public function update(Requests\UpdateMediaRequest $request, $id)
    {

        $file = $this->media->findOrFail($id);


        $file->fill(array_merge($request->only('title', 'caption', 'description'), ['updated_by'=>$this->admin->id]))->save();

        flash()->success('File has been updated.');

        return redirect(route('cms.media.index'));
    }

    public function destroy($id)
    {
        $mediaFile = $this->media->findOrFail($id);

        $pathChunks = explode("/", $mediaFile->path);
        $imageName = array_pop($pathChunks);

        $files = glob(implode("/", $pathChunks) . "/*" . $mediaFile->name);
        foreach ($files as $file) {
            if (File::isFile($file)) {
                File::delete($file);
            }
        }

        $mediaFile->delete();

        flash()->success('File has been deleted.');

        return redirect(route('cms.media.index'));
    }

    public function tinifyImage($id)
    {
        $mediaFile = $this->media->findOrFail($id);

        if (File::isFile($mediaFile->path)) {
            $mediaFile->deleteThumbs();

            $code = tinifyImage($mediaFile->path);

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $mediaFile->tinified = '1';
                $mediaFile->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
