<?php

namespace App\Http\Controllers\CMS;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    /**
     * Show the form to edit profile
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {

        return view('cms.profile.form');
    }


    /**
     * Update the profile
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {

        $this->admin->fill($request->only('name', 'email'));

        if ($request->has('password') && !empty($request->input('password'))) {
            $this->admin->fill(['password' => Hash::make($request->input('password'))]);
        }

        $this->admin->save();

        flash()->success('Profile has been updated.');

        return redirect()->back();
    }
}
