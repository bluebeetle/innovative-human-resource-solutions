<?php

namespace App\Http\Controllers\CMS;

use File;
use App\Event;
use App\EventDate;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('created_at', 'desc')->paginate(10);

        return view('cms.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Event $event)
    {
        return view('cms.events.form', compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreEventRequest $request)
    {
        $event = Event::create($request->only('name', 'slug', 'recursive', 'status') + ['created_by'=>$this->admin->id]);

        $event->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        $locations = $request->input('location');
        $startDates = $request->input('start_date');
        $endDates = $request->input('end_date');
        foreach ($startDates as $dateKey => $startDate) {
            $date = $event->dates()->create(['start_date'=>$startDate, 'end_date'=> $endDates[$dateKey]]);
            $date->saveTranslation(['location'=>$locations[$dateKey]]);
        }

        // Save Logo
        if ($request->hasFile('logo')) {
            $event->saveLogo($request->file('logo'));
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $event->id] + $request->only('title', 'slug', 'content', 'status'));

        flash()->success('Event has been created.');

        return redirect()->route('cms.events.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::with('dates')->findOrFail($id);

        return view('cms.events.form', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateEventRequest $request, $id)
    {
        $event = Event::findOrFail($id);

        $event->fill($request->only('name', 'slug', 'recursive', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        $event->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        $locations = $request->input('location');
        $startDates = $request->input('start_date');
        $endDates = $request->input('end_date');
        $dateIds = [];
        foreach ($startDates as $dateKey => $startDate) {
            $dateId = $request->input('date_id_' . $dateKey);

            if ($dateId) {
                $eventDate = EventDate::findOrFail($dateId);
                $eventDate->start_date = $startDate;
                $eventDate->end_date = $endDates[$dateKey];
                $eventDate->save();

                $eventDate->saveTranslation(['location'=>$locations[$dateKey]]);
                array_push($dateIds, (int)$dateId);
            } else {
                $date = $event->dates()->create(['start_date'=>$startDate, 'end_date'=> $endDates[$dateKey]]);
                $date->saveTranslation(['location'=>$locations[$dateKey]]);
                array_push($dateIds, $date->id);
            }
        }
        $event->dates()->whereNotIn('id', $dateIds)->delete();

        // Save Logo
        if ($request->hasFile('logo')) {
            $event->saveLogo($request->file('logo'));
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $event->id] + $request->only('title', 'slug', 'content', 'status'));

        flash()->success('Event has been updated.');

        return redirect()->route('cms.events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);

        File::delete($event->logo);
        $event->deleteThumbs();

        $event->delete();

        flash()->success('Event has been deleted.');

        return redirect()->route('cms.events.index');
    }


    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('events', $item['id'], $item['title'], $item['content'], route('event', [$item['id'], $item['slug']]));
        } else {
            Search::deleteItem('events', $item['id']);
        }
    }

    public function tinifyImage($id)
    {
        $item = Event::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
