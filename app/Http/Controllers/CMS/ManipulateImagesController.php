<?php

namespace App\Http\Controllers\CMS;

use Image;
use File;
use App\Http\Requests;

class ManipulateImagesController extends Controller
{
    public function __construct()
    {
        dd('Nothing happening here');
        return false;
    }

    public function reduceImagesQuality()
    {

        $patterns = [
            './uploads/*.png',
            './uploads/*.PNG',
            './uploads/*.jpg',
            './uploads/*.JPG',
            './uploads/*.jpeg',
            './uploads/*.JPEG'
        ];

        $thumbs = [];

        foreach ($patterns as $pattern) {
            $tmpThumbs = rglob($pattern);
            $thumbs = array_merge($thumbs, $tmpThumbs);
        }

        foreach ($thumbs as $thumb) {
            echo $thumb. '<br>';
            Image::make($thumb)
                ->interlace()
                ->save(null, config('cms.uploads.imageQuality'));
        }
    }
}
