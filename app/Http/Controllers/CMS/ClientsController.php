<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Support\Str;
use File;
use Image;
use App\Client;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;
use Baum\MoveNotPossibleException;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::orderBy('lft', 'asc')->get();

        return view('cms.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $orderItems = Client::orderBy('lft', 'asc')->get();

        return view('cms.clients.form', compact('client', 'orderItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreClientRequest $request)
    {
        $client = Client::create($request->only('name', 'status') + ['created_by' => $this->admin->id]);

        $this->updateOrder($client, $request);

        /**
         * Save Translation
         */
        $client->saveTranslation($request->only('title', 'content'));

        if ($request->hasFile('logo')) {
            $client->saveLogo($request->file('logo'));
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $client->id] + $request->only('name', 'title', 'content', 'status'));

        flash()->success('Client has been created.');

        return redirect()->route('cms.clients.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);

        $orderItems = Client::orderBy('lft', 'asc')->get();

        return view('cms.clients.form', compact('client', 'orderItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateClientRequest $request, $id)
    {
        $client = Client::findOrFail($id);

        if ($response = $this->updateOrder($client, $request)) {
            return $response;
        }

        $client->fill($request->only('name', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        /**
         * Save Translation
         */
        $client->saveTranslation($request->only('title', 'content'));

        if ($request->hasFile('logo')) {
            $client->saveLogo($request->file('logo'));
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $client->id] + $request->only('name', 'title', 'content', 'status'));

        flash()->success('Client has been updated.');

        return redirect()->route('cms.clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail($id);

        File::delete($client->logo);
        $client->deleteThumbs();

        $client->delete();

        flash()->success('Client has been deleted.');

        return back();
    }

    public function sort(Requests\UpdateOrderingRequest $request)
    {

        $ids = explode("|", $request->input('ids'));
        $clients = [];

        for ($i=0; $i<count($ids); $i++) {
            $id = $ids[$i];
            $clients[$id] = Client::findOrFail($id);
            if ($i>0) {
                try {
                    $clients[$id]->moveToRightOf($clients[$ids[$i-1]]);
                } catch (MoveNotPossibleException $e) {
                    return redirect(route('cms.clients.index'))->withErrors([
                        'error' => 'Cannot make an item a child of itself.'
                    ]);
                }
            }
        }

        flash()->success('Order has been saved.');

        return redirect(route('cms.clients.index'));
    }

    protected function updateOrder(Client $client, Request $request)
    {
        if ($request->has('order', 'orderItem')) {
            try {
                $client->updateOrder($request->input('order'), $request->input('orderItem'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.clients.edit', $client->id))->withInput()->withErrors([
                    'error' => 'Cannot make an item a child of itself.'
                ]);
            }
        }
    }

    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('clients', $item['id'], $item['title'], $item['content'], route('clients') . '#' . Str::slug($item['name']));
        } else {
            Search::deleteItem('clients', $item['id']);
        }
    }

    public function tinifyImage($id)
    {
        $item = Client::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
