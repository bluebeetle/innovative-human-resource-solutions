<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;

use App\BlogCategory;
use App\Http\Requests;

class BlogCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BlogCategory::all();

        return view('cms.blogCategories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BlogCategory $category)
    {
        return view('cms.blogCategories.form', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreBlogCategoryRequest $request)
    {
        $category = BlogCategory::create($request->only('name', 'slug') + ['created_by' => $this->admin->id]);

        $category->saveTranslation($request->only('title'));

        flash()->success('Category has been created');

        return redirect()->route('cms.blogCategories.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = BlogCategory::findOrFail($id);

        return view('cms.blogCategories.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateBlogCategoryRequest $request, $id)
    {
        $category = BlogCategory::findOrFail($id);

        $category->fill($request->only('name', 'slug') + ['updated_by' => $this->admin->id])->save();

        $category->saveTranslation($request->only('title'));

        flash()->success('Category has been updated');

        return redirect()->route('cms.blogCategories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = BlogCategory::findOrFail($id);

        $category->delete();

        flash()->success('Category has been deleted');

        return redirect()->route('cms.blogCategories.index');
    }
}
