<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Support\Str;
use File;
use Image;
use App\Award;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;
use Baum\MoveNotPossibleException;

class AwardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $awards = Award::orderBy('lft', 'asc')->get();

        return view('cms.awards.index', compact('awards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Award $award)
    {
        $orderItems = Award::orderBy('lft', 'asc')->get();

        return view('cms.awards.form', compact('award', 'orderItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreAwardRequest $request)
    {
        $award = Award::create($request->only('name', 'status') + ['created_by' => $this->admin->id]);

        $this->updateOrder($award, $request);

        /**
         * Save Translation
         */
        $award->saveTranslation($request->only('title', 'content'));

        if ($request->hasFile('logo')) {
            $award->saveLogo($request->file('logo'));
        }

        flash()->success('Award has been created.');

        return redirect()->route('cms.awards.index');

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $award->id] + $request->only('name', 'title', 'content', 'status'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $award = Award::findOrFail($id);

        $orderItems = Award::orderBy('lft', 'asc')->get();

        return view('cms.awards.form', compact('award', 'orderItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateAwardRequest $request, $id)
    {
        $award = Award::findOrFail($id);

        if ($response = $this->updateOrder($award, $request)) {
            return $response;
        }

        $award->fill($request->only('name', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        /**
         * Save Translation
         */
        $award->saveTranslation($request->only('title', 'content'));

        if ($request->hasFile('logo')) {
            $award->saveLogo($request->file('logo'));
        }

        flash()->success('Award has been updated.');

        return redirect()->route('cms.awards.index');

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $award->id] + $request->only('name', 'title', 'content', 'status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $award = Award::findOrFail($id);

        File::delete($award->logo);
        $award->deleteThumbs();

        $award->delete();

        flash()->success('Award has been deleted.');

        return back();
    }

    public function sort(Requests\UpdateOrderingRequest $request)
    {

        $ids = explode("|", $request->input('ids'));
        $awards = [];

        for ($i=0; $i<count($ids); $i++) {
            $id = $ids[$i];
            $awards[$id] = Award::findOrFail($id);
            if ($i>0) {
                try {
                    $awards[$id]->moveToRightOf($awards[$ids[$i-1]]);
                } catch (MoveNotPossibleException $e) {
                    return redirect(route('cms.awards.index'))->withErrors([
                        'error' => 'Cannot make an item a child of itself.'
                    ]);
                }
            }
        }

        flash()->success('Order has been saved.');

        return redirect(route('cms.awards.index'));
    }

    protected function updateOrder(Award $award, Request $request)
    {
        if ($request->has('order', 'orderItem')) {
            try {
                $award->updateOrder($request->input('order'), $request->input('orderItem'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.awards.edit', $award->id))->withInput()->withErrors([
                    'error' => 'Cannot make an item a child of itself.'
                ]);
            }
        }
    }

    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('awards', $item['id'], $item['title'], $item['content'], route('awards') . '#' . Str::slug($item['name']));
        } else {
            Search::deleteItem('awards', $item['id']);
        }
    }

    public function tinifyImage($id)
    {
        $item = Award::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
