<?php

namespace App\Http\Controllers\CMS;

use App\User;
use App\Post;
use App\CustomField;
use App\BlogCategory;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('published_at', 'desc')->get();

        return view('cms.blog.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Post $post)
    {
        $users = User::all();

        $categories = BlogCategory::all();

        return view('cms.blog.form', compact('post', 'users', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StorePostRequest $request)
    {

        $post = Post::create($request->only('name', 'author_id', 'slug', 'published_at', 'status') + ['created_by' => $this->admin->id]);

        $post->saveTranslation($request->only('title', 'banner_text', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        if (!is_null($request->input('categories'))) {
            $post->categories()->sync($request->input('categories'));
        }

        $fieldNames = $request->input('field_name');
        $fieldValues = $request->input('field_value');
        foreach ($fieldNames as $fieldPosition => $fieldName) {
            if (empty($fieldName)) {
                continue;
            }
            $field = $post->customFields()->create(['name'=>$fieldName, 'order'=> $fieldPosition+1, 'model_type'=>get_class($post)]);
            $field->saveTranslation(['value'=>$fieldValues[$fieldPosition]]);
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $post->id] + $request->only('title', 'slug', 'content', 'status'));

        flash()->success('Blog Post has been created');

        return redirect(route('cms.blog.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);

        $users = User::all();

        $categories = BlogCategory::all();

        return view('cms.blog.form', compact('post', 'users', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdatePostRequest $request, $id)
    {
        $post = Post::findOrFail($id);

        $post->fill($request->only('name', 'author_id', 'slug', 'published_at', 'status') + ['updated_by' => $this->admin->id])
            ->save();

        $post->saveTranslation($request->only('title', 'banner_text', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        $post->categories()->sync((array)$request->input('categories'));

        $fieldNames = $request->input('field_name');
        $fieldValues = $request->input('field_value');
        $fieldIds = [];
        foreach ($fieldNames as $fieldPosition => $fieldName) {
            if (empty($fieldName)) {
                continue;
            }
            $fieldId = $request->input('field_id_' . $fieldPosition);

            if ($fieldId) {
                $customField = CustomField::findOrFail($fieldId);
                $customField->name = $fieldName;
                $customField->order = $fieldPosition + 1;
                $customField->save();

                $customField->saveTranslation(['value'=>$fieldValues[$fieldPosition]]);
                array_push($fieldIds, (int)$fieldId);
            } else {
                $field = $post->customFields()->create(['name'=>$fieldName, 'order'=> $fieldPosition+1, 'model_type'=>get_class($post)]);
                $field->saveTranslation(['value'=>$fieldValues[$fieldPosition]]);
                array_push($fieldIds, $field->id);
            }
        }
        $post->customFields()->whereNotIn('id', $fieldIds)->delete();

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $post->id] + $request->only('title', 'slug', 'content', 'status'));

        flash()->success('Blog Post has been updated');

        return redirect(route('cms.blog.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        $post->delete();

        flash()->success('Blog Post has been deleted.');

        return redirect(route('cms.blog.index'));
    }

    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('news & events', $item['id'], $item['title'], $item['content'], route('article', [$item['id'], $item['slug']]));
        } else {
            Search::deleteItem('news & events', $item['id']);
        }
    }
}
