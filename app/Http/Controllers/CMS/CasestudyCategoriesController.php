<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;

use App\CasestudyCategory;
use App\Http\Requests;

class CasestudyCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CasestudyCategory::all();

        return view('cms.casestudyCategories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CasestudyCategory $category)
    {
        return view('cms.casestudyCategories.form', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreCasestudyCategoryRequest $request)
    {
        $category = CasestudyCategory::create($request->only('name') + ['created_by' => $this->admin->id]);

        $category->saveTranslation($request->only('title'));

        flash()->success('Category has been created');

        return redirect()->route('cms.casestudyCategories.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = CasestudyCategory::findOrFail($id);

        return view('cms.casestudyCategories.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateCasestudyCategoryRequest $request, $id)
    {
        $category = CasestudyCategory::findOrFail($id);

        $category->fill($request->only('name') + ['updated_by' => $this->admin->id])
            ->save();

        $category->saveTranslation($request->only('title'));

        flash()->success('Category has been updated');

        return redirect()->route('cms.casestudyCategories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = CasestudyCategory::findOrFail($id);

        $category->delete();

        flash()->success('Category has been deleted');

        return redirect()->route('cms.casestudyCategories.index');
    }
}
