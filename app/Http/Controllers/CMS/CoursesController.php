<?php

namespace App\Http\Controllers\CMS;

use File;
use App\Page;
use App\Course;
use App\CourseDate;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;
use Baum\MoveNotPossibleException;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::orderBy('lft', 'asc')->get();

        return view('cms.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Course $course)
    {
        $pages = Page::active()->orderBy('lft', 'asc')->get();

        $orderItems = Course::orderBy('lft', 'asc')->get();

        return view('cms.courses.form', compact('course', 'pages', 'orderItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreCourseRequest $request)
    {
        $course = Course::create($request->only('name', 'slug', 'cost', 'link', 'status') + ['created_by'=>$this->admin->id]);

        $course->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        $this->updateOrder($course, $request);

        // Save dates
        $locations = $request->input('location');
        $startDates = $request->input('start_date');
        $endDates = $request->input('end_date');
        foreach ($startDates as $dateKey => $startDate) {
            $date = $course->dates()->create(['start_date'=>$startDate, 'end_date'=> $endDates[$dateKey]]);
            $date->saveTranslation(['location'=>$locations[$dateKey]]);
        }

        // Save Logo
        if ($request->hasFile('logo')) {
            $course->saveLogo($request->file('logo'));
        }

        // Attach Pages
        $pages = $request->input('pages');
        $pages = $pages?array_filter($pages):[];
        if (count($pages) > 0) {
            $course->pages()->attach($pages);
        }

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $course->id] + $request->only('title', 'slug', 'content', 'status'));

        flash()->success('Course has been created.');

        return redirect()->route('cms.courses.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::with('dates')->findOrFail($id);

        $pages = Page::active()->orderBy('lft', 'asc')->get();

        $orderItems = Course::orderBy('lft', 'asc')->get();

        return view('cms.courses.form', compact('course', 'pages', 'orderItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateCourseRequest $request, $id)
    {
        $course = Course::findOrFail($id);

        if ($response = $this->updateOrder($course, $request)) {
            return $response;
        }

        $course->fill($request->only('name', 'slug', 'cost', 'link', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        $course->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        $locations = $request->input('location');
        $startDates = $request->input('start_date');
        $endDates = $request->input('end_date');
        $dateIds = [];
        foreach ($startDates as $dateKey => $startDate) {
            $dateId = $request->input('date_id_' . $dateKey);

            if ($dateId) {
                $courseDate = CourseDate::findOrFail($dateId);
                $courseDate->start_date = $startDate;
                $courseDate->end_date = $endDates[$dateKey];
                $courseDate->save();

                $courseDate->saveTranslation(['location'=>$locations[$dateKey]]);
                array_push($dateIds, (int)$dateId);
            } else {
                $date = $course->dates()->create(['start_date'=>$startDate, 'end_date'=> $endDates[$dateKey]]);
                $date->saveTranslation(['location'=>$locations[$dateKey]]);
                array_push($dateIds, $date->id);
            }
        }
        $course->dates()->whereNotIn('id', $dateIds)->delete();
        $course->dates()->whereNotIn('id', $dateIds)->delete();

        // Save Logo
        if ($request->hasFile('logo')) {
            $course->saveLogo($request->file('logo'));
        }

        // Attach Pages
        $course->pages()->detach();
        $pages = $request->input('pages');
        $pages = $pages?array_filter($pages):[];
        if (count($pages) > 0) {
            $course->pages()->attach($pages);
        }
        
        
        Page::where('uri', 'calendar/course/' . $course->id . '/' .$course->slug)
            ->where('alias', 1)
            ->update(['status' => $request->input('status')]);
        

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $course->id] + $request->only('title', 'slug', 'content', 'status'));


        flash()->success('Course has been updated.');

        return redirect()->route('cms.courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);

        File::delete($course->logo);
        $course->deleteThumbs();

        $course->delete();
    
    
        Page::where('uri', 'calendar/course/' . $course->id . '/' .$course->slug)
            ->where('alias', 1)
            ->delete();

        flash()->success('Course has been deleted.');

        return redirect()->route('cms.courses.index');
    }

    public function sort(Requests\UpdateOrderingRequest $request)
    {

        $ids = explode("|", $request->input('ids'));
        $items = [];

        for ($i=0; $i<count($ids); $i++) {
            $id = $ids[$i];
            $items[$id] = Course::findOrFail($id);
            if ($i>0) {
                try {
                    $items[$id]->moveToRightOf($items[$ids[$i-1]]);
                } catch (MoveNotPossibleException $e) {
                    return redirect(route('cms.courses.index'))->withErrors([
                        'error' => 'Cannot make an item a child of itself.'
                    ]);
                }
            }
        }

        flash()->success('Order has been saved.');

        return redirect(route('cms.courses.index'));
    }

    protected function updateOrder(Course $course, Request $request)
    {
        if ($request->has('order', 'orderItem')) {
            try {
                $course->updateOrder($request->input('order'), $request->input('orderItem'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.course.edit', $course->id))->withInput()->withErrors([
                    'error' => 'Cannot make an item a child of itself.'
                ]);
            }
        }
    }

    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('courses', $item['id'], $item['title'], $item['content'], route('course', [$item['id'], $item['slug']]));
        } else {
            Search::deleteItem('courses', $item['id']);
        }
    }

    public function tinifyImage($id)
    {
        $item = Course::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
