<?php

namespace App\Http\Controllers\CMS;

use DB;
use App\Media;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Mediable;

class MediableController extends Controller
{

    /**
     * Show the attached media files to mediable model
     */
    public function show($mediableType, $mediableId)
    {
        $config = config('cms.mediables.' . $mediableType);

        $model = app($config['model']);
        $resource = $model->find($mediableId);

        //print_r($resource->media->groupBy('collection')->toArray());

        return view('cms.mediable.show', compact('files', 'resource', 'mediableType'));
    }

    /**
     * Select from a list of media files to attach to given meidable model
     */
    public function select($mediableType, $mediableId)
    {


        $config = config('cms.mediables.' . $mediableType);

        $model = app($config['model']);
        $resource = $model->find($mediableId);

        $files = Media::all()->sortByDesc('created_at');

        return view('cms.mediable.select', compact('files', 'resource', 'mediableType'));
    }

    /**
     * Attach the selected media files to given mediable model
     */
    public function attach($mediableType, $mediableId, Request $request)
    {

        $config = config('cms.mediables.' . $mediableType);

        $model = app($config['model']);
        $resource = $model->find($mediableId);

        $files = $request->only('file');
        foreach ($files as $file) {
            $resource->media()->attach($file, ['mediable_type' => $config['model']]);
        }

        return getMediableRedirect($config, compact("mediableType", "mediableId"));
    }


    public function update($mediableType, $mediableId, Request $request)
    {

        /**
         * Delete Mediables
         */

        $requestedDeleteIds = $request->all('delete');
        if ($requestedDeleteIds['delete'] != null) {
            Mediable::destroy($requestedDeleteIds['delete']);
            /*foreach($requestedDeleteIds['delete'] as $id){
                DB::delete('delete from mediables where id = ?', array($id));
            }*/
        }

        /**
         * Update Mediables
         */
        $requestedUpdateData = $request->only('collection', 'caption');
        if (count($requestedUpdateData['collection'])) {
            foreach ($requestedUpdateData['collection'] as $id => $collection) {
                $collection = $collection?:null;
                $caption = $requestedUpdateData['caption'][$id]?:null;
                $mediable = Mediable::findOrFail($id);

                $mediable->collection =  $collection;
                $mediable->save();
                $mediable->saveTranslation(['caption' => $caption]);

                //DB::update('update mediables set collection = ? , caption = ? where id = ?', array($collection, $caption, $id));
            }
        }

        return redirect()->back();
    }
}
