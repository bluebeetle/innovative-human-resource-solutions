<?php

namespace App\Http\Controllers\CMS;

use App;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function dashboard()
    {

        return view('cms.main.dashboard');
    }

    public function setLocale($locale)
    {

        Session::put('locale', $locale);
        App::setLocale($locale);

        return redirect()->route('cms.dashboard');
    }
}
