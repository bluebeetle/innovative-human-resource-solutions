<?php

namespace App\Http\Controllers\CMS;

use File;
use Image;
use App\LogoSlider;
use Illuminate\Http\Request;
use App\Http\Requests;
use Baum\MoveNotPossibleException;

class LogoSlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logoSliders = LogoSlider::orderBy('lft', 'asc')->get();

        return view('cms.logoSliders.index', compact('logoSliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(LogoSlider $logoSlider)
    {
        $orderItems = LogoSlider::orderBy('lft', 'asc')->get();

        return view('cms.logoSliders.form', compact('logoSlider', 'orderItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreLogoSliderRequest $request)
    {
        $logoSlider = LogoSlider::create($request->only('name', 'status') + ['created_by' => $this->admin->id]);

        $logoSlider->saveTranslation($request->only('link'));

        $this->updateOrder($logoSlider, $request);

        if ($request->hasFile('logo')) {
            $logoSlider->saveLogo($request->file('logo'));
        }

        flash()->success('Logo Slider has been created.');

        return redirect()->route('cms.logoSliders.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $logoSlider = LogoSlider::findOrFail($id);

        $orderItems = LogoSlider::orderBy('lft', 'asc')->get();

        return view('cms.logoSliders.form', compact('logoSlider', 'orderItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateLogoSliderRequest $request, $id)
    {
        $logoSlider = LogoSlider::findOrFail($id);

        if ($response = $this->updateOrder($logoSlider, $request)) {
            return $response;
        }

        $logoSlider->fill($request->only('name', 'link', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        $logoSlider->saveTranslation($request->only('link'));

        if ($request->hasFile('logo')) {
            $logoSlider->saveLogo($request->file('logo'));
        }

        flash()->success('Logo Slider has been updated.');

        return redirect()->route('cms.logoSliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $logoSlider = LogoSlider::findOrFail($id);

        File::delete($logoSlider->logo);
        $logoSlider->deleteThumbs();

        $logoSlider->delete();

        flash()->success('Logo Slider has been deleted.');

        return back();
    }

    public function sort(Requests\UpdateOrderingRequest $request)
    {

        $ids = explode("|", $request->input('ids'));

        $logoSliders = [];

        for ($i=0; $i<count($ids); $i++) {
            $id = $ids[$i];
            $logoSliders[$id] = LogoSlider::findOrFail($id);
            if ($i>0) {
                try {
                    $logoSliders[$id]->moveToRightOf($logoSliders[$ids[$i-1]]);
                } catch (MoveNotPossibleException $e) {
                    return redirect(route('cms.logoSliders.index'))->withErrors([
                        'error' => 'Cannot make an item a child of itself.'
                    ]);
                }
            }
        }

        flash()->success('Order has been saved.');

        return redirect(route('cms.logoSliders.index'));
    }

    protected function updateOrder(LogoSlider $logoSlider, Request $request)
    {
        if ($request->has('order', 'orderItem')) {
            try {
                $logoSlider->updateOrder($request->input('order'), $request->input('orderItem'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.logoSliders.edit', $logoSlider->id))->withInput()->withErrors([
                    'error' => 'Cannot make an item a child of itself.'
                ]);
            }
        }
    }

    public function tinifyImage($id)
    {
        $item = LogoSlider::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
