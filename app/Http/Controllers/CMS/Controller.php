<?php

namespace App\Http\Controllers\CMS;

use App;
use Auth;
use Session;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $admin;
    protected $locale;
    protected $locales;

    public function __construct()
    {
        // Share locales
        $locales = config('cms.locales');
        $this->locales = $locales;
        view()->share('locales', $this->locales);
    
    
        $this->middleware(function ($request, $next) {
        
            view()->share('signedIn', Auth::check());
        
            $this->admin = Auth::user();
            view()->share('admin', $this->admin);
            
            $this->locale = Session::get('locale', config('app.fallback_locale'));
            App::setLocale($this->locale);
            view()->share('locale', $this->locale);
        
            return $next($request);
        });
    }
}
