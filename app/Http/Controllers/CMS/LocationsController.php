<?php

namespace App\Http\Controllers\CMS;

use File;
use App\Location;
use Illuminate\Http\Request;
use App\Http\Requests;
use Baum\MoveNotPossibleException;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();

        return view('cms.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Location $location)
    {
        $orderItems = Location::orderBy('lft', 'asc')->get();

        return view('cms.locations.form', compact('location', 'orderItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreLocationRequest $request)
    {
        $location = Location::create($request->only('name', 'coordinates', 'phone', 'head_office', 'status') + ['created_by'=>$this->admin->id]);

        $this->updateOrder($location, $request);

        $location->saveTranslation($request->only('title'));

        // Save Landscape
        if ($request->hasFile('landscape')) {
            $location->saveImage($request->file('landscape'));
        }

        flash()->success('Location has been created');

        return redirect()->route('cms.locations.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::findOrFail($id);

        $orderItems = Location::orderBy('lft', 'asc')->get();

        return view('cms.locations.form', compact('location', 'orderItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\StoreLocationRequest $request, $id)
    {
        $location = Location::findOrFail($id);

        if ($response = $this->updateOrder($location, $request)) {
            return $response;
        }

        $location->fill($request->only('name', 'coordinates', 'phone', 'head_office', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        $location->saveTranslation($request->only('title'));

        // Save Landscape
        if ($request->hasFile('landscape')) {
            $location->saveImage($request->file('landscape'));
        }

        flash()->success('Location has been updated');

        return redirect()->route('cms.locations.index');
    }

    public function sort(Requests\UpdateOrderingRequest $request)
    {

        $ids = explode("|", $request->input('ids'));
        $locations = [];

        for ($i=0; $i<count($ids); $i++) {
            $id = $ids[$i];
            $locations[$id] = Location::findOrFail($id);
            if ($i>0) {
                try {
                    $locations[$id]->moveToRightOf($locations[$ids[$i-1]]);
                } catch (MoveNotPossibleException $e) {
                    return redirect(route('cms.locations.index'))->withErrors([
                        'error' => 'Cannot make an item a child of itself.'
                    ]);
                }
            }
        }

        flash()->success('Order has been saved.');

        return redirect(route('cms.locations.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Location::destroy($id);

        flash()->success('Location has been deleted');

        return redirect()->route('cms.locations.index');
    }

    protected function updateOrder(Location $location, Request $request)
    {
        if ($request->has('order', 'orderItem')) {
            try {
                $location->updateOrder($request->input('order'), $request->input('orderItem'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.locations.edit', $location->id))->withInput()->withErrors([
                    'error' => 'Cannot make an item a child of itself.'
                ]);
            }
        }
    }

    public function tinifyImage($id)
    {
        $item = Location::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
