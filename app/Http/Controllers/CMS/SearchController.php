<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Support\Str;
use DB;
use App\Casestudy;
use App\Client;
use App\Http\Requests;
use App\Page;
use App\Post;
use App\Utilities\Search;
use App\Event;
use App\Course;
use App\Job;
use App\Employee;

class SearchController extends Controller
{

    /**
     * Generate Search Index
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {

        DB::raw("TRUNCATE search_index");

        $pages = Page::all();
        foreach ($pages as $page) {
            $this->updatePageSearchIndex($page);
        }

        $posts = Post::all();
        $type = 'news & events';
        foreach ($posts as $post) {
            $url = route('article', [$post->id, $post->slug]);
            $this->updateSearchIndex($type, $post, $url);
        }

        $casestudies = Casestudy::all();
        $type = 'case studies';
        foreach ($casestudies as $casestudy) {
            $url = route('casestudy', [$casestudy->id, $casestudy->slug]);
            $this->updateSearchIndex($type, $casestudy, $url);
        }

        $events = Event::all();
        $type = 'events';
        foreach ($events as $event) {
            $url = route('event', [$event->id, $event->slug]);
            $this->updateSearchIndex($type, $event, $url);
        }

        $courses = Course::all();
        $type = 'courses';
        foreach ($courses as $course) {
            $url = route('course', [$course->id, $course->slug]);
            $this->updateSearchIndex($type, $course, $url);
        }

        $clients = Client::all();
        $type = 'clients';
        foreach ($clients as $client) {
            $url = route('clients') . '#' . Str::slug($client->name);
            $this->updateSearchIndex($type, $client, $url);
        }

        $careers = Job::all();
        $type = 'careers';
        foreach ($careers as $career) {
            $url = route('careers') . '#' . Str::slug($career->name);
            $this->updateSearchIndex($type, $career, $url);
        }

        $employees = Employee::all();
        $type = 'team';
        foreach ($employees as $employee) {
            $url = route('team') . '#' . Str::slug($employee->name);
            $this->updateSearchIndex($type, $employee, $url);
        }

        flash()->success('Search index has been generated.');

        return redirect()->back();
    }

    protected function updateSearchIndex($type, $item, $url)
    {
        if ($item->status) {
            Search::addOrUpdateItem($type, $item->id, $item->title, $item->content, $url);
        } else {
            Search::deleteItem($type, $item->id);
        }
    }

    protected function updatePageSearchIndex($item)
    {

        if ($item->not_searchable != 1 && ! preg_match('/{.*?}/', $item->uri)) {
            Search::addOrUpdateItem('pages', $item->id, $item->title, $item->content, url($item->uri));
        } else {
            Search::deleteItem('pages', $item->id);
        }
    }
}
