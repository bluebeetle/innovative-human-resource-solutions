<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Support\Str;
use File;
use Image;
use App\Testimonial;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;
use Baum\MoveNotPossibleException;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::orderBy('lft', 'asc')->get();

        return view('cms.testimonials.index', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Testimonial $testimonial)
    {
        $orderItems = Testimonial::orderBy('lft', 'asc')->get();

        return view('cms.testimonials.form', compact('testimonial', 'orderItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreTestimonialRequest $request)
    {
        $testimonial = Testimonial::create($request->only('name', 'status') + ['created_by' => $this->admin->id]);

        $this->updateOrder($testimonial, $request);

        /**
         * Save Translation
         */
        $testimonial->saveTranslation($request->only('title', 'content'));

        if ($request->hasFile('logo')) {
            $testimonial->saveLogo($request->file('logo'));
        }

        flash()->success('Testimonial has been created.');

        return redirect()->route('cms.testimonials.index');

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $testimonial->id] + $request->only('name', 'title', 'content', 'status'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        $orderItems = Testimonial::orderBy('lft', 'asc')->get();

        return view('cms.testimonials.form', compact('testimonial', 'orderItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateTestimonialRequest $request, $id)
    {
        $testimonial = Testimonial::findOrFail($id);

        if ($response = $this->updateOrder($testimonial, $request)) {
            return $response;
        }

        $testimonial->fill($request->only('name', 'status') + ['updated_by'=>$this->admin->id])
            ->save();

        /**
         * Save Translation
         */
        $testimonial->saveTranslation($request->only('title', 'content'));

        if ($request->hasFile('logo')) {
            $testimonial->saveLogo($request->file('logo'));
        }

        flash()->success('Testimonial has been updated.');

        return redirect()->route('cms.testimonials.index');

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $testimonial->id] + $request->only('name', 'title', 'content', 'status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        File::delete($testimonial->logo);
        $testimonial->deleteThumbs();

        $testimonial->delete();

        flash()->success('Testimonial has been deleted.');

        return back();
    }

    public function sort(Requests\UpdateOrderingRequest $request)
    {

        $ids = explode("|", $request->input('ids'));
        $testimonials = [];

        for ($i=0; $i<count($ids); $i++) {
            $id = $ids[$i];
            $testimonials[$id] = Testimonial::findOrFail($id);
            if ($i>0) {
                try {
                    $testimonials[$id]->moveToRightOf($testimonials[$ids[$i-1]]);
                } catch (MoveNotPossibleException $e) {
                    return redirect(route('cms.testimonials.index'))->withErrors([
                        'error' => 'Cannot make an item a child of itself.'
                    ]);
                }
            }
        }

        flash()->success('Order has been saved.');

        return redirect(route('cms.testimonials.index'));
    }

    protected function updateOrder(Testimonial $testimonials, Request $request)
    {
        if ($request->has('order', 'orderItem')) {
            try {
                $testimonials->updateOrder($request->input('order'), $request->input('orderItem'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.testimonials.edit', $testimonials->id))->withInput()->withErrors([
                    'error' => 'Cannot make an item a child of itself.'
                ]);
            }
        }
    }

    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('testimonials', $item['id'], $item['title'], $item['content'], route('testimonials') . '#' . Str::slug($item['name']));
        } else {
            Search::deleteItem('testimonials', $item['id']);
        }
    }

    public function tinifyImage($id)
    {
        $item = Testimonial::findOrFail($id);
        $imageColumnName = $item->getImageColumnName();
        $imageTinifiedColumnName = $imageColumnName . '_tinified';

        if (File::isFile($item->{$imageColumnName})) {
            $item->deleteThumbs();

            $code = tinifyImage($item->{$imageColumnName});

            if ($code == 'tinified') {
                flash()->success('Image has been tinified.');

                $item->{$imageTinifiedColumnName} = '1';
                $item->save();
            } else if ($code == 'apiError') {
                flash()->success('Your API key is not valid, or your monthly limit is exhausted.');
            } else {
                flash()->success('Unknown error, Please try again.');
            }
        } else {
            flash()->success('Nothing to tinify.');
        }

        return redirect()->back();
    }
}
