<?php

namespace App\Http\Controllers\CMS;

use App\Casestudy;
use App\Http\Requests;
use App\CasestudySector;
use App\CasestudyCategory;
use Illuminate\Http\Request;
use App\Utilities\Search;

class CasestudiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $casestudies = Casestudy::all();

        return view('cms.casestudies.index', compact('casestudies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Casestudy $casestudy)
    {
        $categories = CasestudyCategory::all();
        $sectors = CasestudySector::all();
        return view('cms.casestudies.form', compact('casestudy', 'categories', 'sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreCasestudyRequest $request)
    {
        $casestudy = Casestudy::create($request->only('name', 'slug', 'category_id', 'sector_id', 'status') + ['created_by' => $this->admin->id]);

        $casestudy->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $casestudy->id] + $request->only('title', 'slug', 'content', 'status'));

        flash()->success('Casestudy has been created');

        return redirect()->route('cms.casestudies.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $casestudy = Casestudy::findOrFail($id);
        $categories = CasestudyCategory::all();
        $sectors = CasestudySector::all();

        return view('cms.casestudies.form', compact('casestudy', 'categories', 'sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateCasestudyRequest $request, $id)
    {
        $casestudy = Casestudy::findOrFail($id);

        $casestudy->fill($request->only('name', 'slug', 'category_id', 'sector_id', 'status') + ['updated_by' => $this->admin->id])
            ->save();

        $casestudy->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $casestudy->id] + $request->only('title', 'slug', 'content', 'status'));

        flash()->success('Casestudy has been updated');

        return redirect()->route('cms.casestudies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Casestudy::destroy($id);

        flash()->success('Casestudy has been deleted');

        return redirect()->route('cms.casestudies.index');
    }


    protected function updateSearchIndex($item)
    {

        if ($item['status']) {
            Search::addOrUpdateItem('case studies', $item['id'], $item['title'], $item['content'], route('casestudy', [$item['id'], $item['slug']]));
        } else {
            Search::deleteItem('case studies', $item['id']);
        }
    }
}
