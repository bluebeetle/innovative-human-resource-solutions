<?php

namespace App\Http\Controllers\CMS;

use App\Page;
use App\Course;
use App\CustomField;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;
use Baum\MoveNotPossibleException;

class PagesController extends Controller
{
    protected $pages;

    public function __construct(Page $pages)
    {
        $this->pages = $pages;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = $this->pages->orderBy('lft', 'asc')->get();

        return view('cms.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Page $page)
    {
        $templates = $this->getPageTemplates();

        $orderPages = $this->pages->where('hidden', false)->orderBy('lft', 'asc')->get();

        $courses = Course::active()->get();

        return view('cms.pages.form', compact('page', 'templates', 'orderPages', 'courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StorePageRequest $request)
    {

        $page = $this->pages->create(array_merge($request->only('uri', 'name', 'template', 'navigation', 'hidden', 'not_searchable', 'alias', 'status'), ['created_by'=>$this->admin->id]));

        /**
         * Save Translation
         */
        $page->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));

        $this->updatePageOrder($page, $request);

        $fieldNames = $request->input('field_name');
        $fieldValues = $request->input('field_value');
        foreach ($fieldNames as $fieldPosition => $fieldName) {
            if (empty($fieldName)) {
                continue;
            }
            $field = $page->customFields()->create(['name'=>$fieldName, 'order'=> $fieldPosition+1, 'model_type'=>get_class($page)]);
            $field->saveTranslation(['value'=>$fieldValues[$fieldPosition]]);
        }

        $courses = $request->input('courses');
        $courses = is_array($courses)?array_filter($courses):[];
        $page->courses()->sync($courses);

        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $page->id] + $request->only('uri', 'not_searchable', 'title', 'content'));


        flash()->success('Page has been created.');

        return redirect(route('cms.pages.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->pages->findOrFail($id);

        $templates = $this->getPageTemplates();

        $orderPages = $this->pages->where('hidden', false)->orderBy('lft', 'asc')->get();

        $courses = Course::active()->get();

        return view('cms.pages.form', compact('page', 'templates', 'orderPages', 'courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdatePageRequest $request, $id)
    {
        $page = $this->pages->findOrFail($id);

        if ($response = $this->updatePageOrder($page, $request)) {
            return $response;
        }

        $page->fill(array_merge($request->only('uri', 'name', 'template', 'navigation', 'hidden', 'not_searchable', 'alias', 'status'), ['updated_by'=>$this->admin->id]))
            ->save();


        /**
         * Save Translation
         */
        $page->saveTranslation($request->only('title', 'content', 'meta_title', 'meta_keywords', 'meta_description'));


        $fieldNames = $request->input('field_name');
        $fieldValues = $request->input('field_value');
        $fieldIds = [];
        foreach ($fieldNames as $fieldPosition => $fieldName) {
            if (empty($fieldName)) {
                continue;
            }
            
            $fieldId = $request->input('field_id_' . $fieldPosition);

            if ($fieldId) {
                $customField = CustomField::findOrFail($fieldId);
                $customField->name = $fieldName;
                $customField->order = $fieldPosition + 1;
                $customField->save();

                $customField->saveTranslation(['value'=>$fieldValues[$fieldPosition]]);
                array_push($fieldIds, (int)$fieldId);
            } else {
                $field = $page->customFields()->create(['name'=>$fieldName, 'order'=> $fieldPosition+1, 'model_type'=>get_class($page)]);
                $field->saveTranslation(['value'=>$fieldValues[$fieldPosition]]);
                array_push($fieldIds, $field->id);
            }
        }
        $page->customFields()->whereNotIn('id', $fieldIds)->delete();


        $courses = $request->input('courses');
        $courses = is_array($courses)?array_filter($courses):[];
        $page->courses()->sync($courses);


        /**
         * Update Search Index
         */
        $this->updateSearchIndex(['id' => $page->id] + $request->only('uri', 'not_searchable', 'title', 'content'));


        flash()->success('Page has been updated.');

        return redirect(route('cms.pages.index', $page->id));
    }

    public function confirm($id)
    {
        $page = $this->pages->findOrFail($id);

        return view('cms.pages.confirm', compact('page'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = $this->pages->findOrFail($id);

        foreach ($page->children as $child) {
            $child->makeRoot();
        }

        $page->delete();

        flash()->success('Page has been deleted.');

        return redirect(route('cms.pages.index'));
    }

    protected function getPageTemplates()
    {
        $templates = config('cms.templates');

        return ['' => ''] + array_combine(array_keys($templates), array_keys($templates));
    }

    protected function updatePageOrder(Page $page, Request $request)
    {
        if ($request->has('order')) {
            try {
                $page->updateOrder($request->input('order'), $request->input('orderPage'));
            } catch (MoveNotPossibleException $e) {
                return redirect(route('cms.pages.edit', $page->id))->withInput()->withErrors([
                    'error' => 'Cannot make a page a child of itself.'
                ]);
            }
        }
    }

    protected function updateSearchIndex($item)
    {

        if (@$item['not_searchable'] != 1 && ! preg_match('/{.*?}/', $item['uri'])) {
            Search::addOrUpdateItem('pages', $item['id'], $item['title'], $item['content'], url($item['uri']));
        } else {
            Search::deleteItem('pages', $item['id']);
        }
    }
}
