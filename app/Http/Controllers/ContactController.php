<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Thetispro\Setting\Facades\Setting;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\Mail\SendContactInquiry;

class ContactController extends Controller
{

    public function submitContact(Requests\ContactRequest $request)
    {


        $from = Setting::get('emails.from');
        $fromChunks = explode("|", $from);

        // Email
    
        $emailData = [
            'replyTo' => $fromChunks,
            'subject' => 'New contact inquiry',
            'parameters' => $request->except('_token')
        ];
    
        Mail::to(Setting::get('emails.contactEmail'))
            ->queue(new SendContactInquiry($emailData));

        // Salesforce
        $httpClient = new GuzzleHttpClient([
            'base_uri' => 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8',
            'timeout' => 10.0,
            'allow_redirects' => false
        ]);
        
        $response = $httpClient->request('POST', 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8', [
            'form_params' => [
                'oid' => '00Di0000000KIeREAW', //'00Di0000000KIeR',
                'retURL' => route('contactThankYou'),
                'first_name' => $request->input('firstName'),
                'last_name' => $request->input('lastName'),
                'company' => $request->input('company'),
                'mobile' => $request->input('mobile'),
                'email' => $request->input('email'),
                'city' => $request->input('city'),
                'Abbreviated_Description__c' => $request->input('message'),
                '00N2400000Ir9D3' => 'Website Enquiry',
                'source' => session('source'),
                'medium' => session('medium'),
                'campaign' => session('campaign'),
                'submit' => 'submit',
                'lead_source' => 'Web',
                'Lead_Source_Description__c' => 'ContactUsFormPSIME',
                'CompanyDivision__c' => 'IHS'
            ]
        ]);
        
        
        $code = $response->getStatusCode();
        
        if ($code == 200) {
            echo $response->getBody();
        } else {
            return redirect()->route('contact')->withErrors(['submissionError' => 'submissionError']);
        }
    }
    
    public function submitContactForm(Requests\ContactFormRequest $request)
    {
        
        
        $from = Setting::get('emails.from');
        $fromChunks = explode("|", $from);
        
        // Email
        $emailData = [
            'replyTo' => $fromChunks,
            'subject' => 'Assessment Inquiry',
            'parameters' => $request->except('_token')
        ];
    
        Mail::to(Setting::get('emails.contactEmail'))
            ->queue(new SendContactInquiry($emailData));

        // Salesforce
        $httpClient = new GuzzleHttpClient([
            'base_uri' => 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8',
            'timeout' => 10.0,
            'allow_redirects' => false
        ]);
        
        $response = $httpClient->request('POST', 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8', [
            'form_params' => [
                'oid' => '00D24000000YsQ8',
                'retURL' => route('contactThankYou'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'source' => session('source'),
                'medium' => session('medium'),
                'campaign' => session('campaign'),
                'submit' => 'submit'
            ]
        ]);
        
        
        $code = $response->getStatusCode();
        
        if ($code == 200) {
            echo $response->getBody();
        } else {
            return redirect()->route('contact')->withErrors(['submissionError' => 'submissionError']);
        }
    }
}
