<?php

namespace App\Http\Controllers;

use App\Casestudy;
use App\Course;
use App\Page;
use App\Event;
use App\Post;

class SitemapController extends Controller
{

    // Pages
   // Events


    public function index()
    {
        $links = $this->pages();
        $links = array_merge($links, $this->events());
        $links = array_merge($links, $this->courses());
        $links = array_merge($links, $this->blog());
        $links = array_merge($links, $this->casestudies());

        $xmlStart = '<?xml version="1.0" encoding="UTF-8"?>';
        return response($xmlStart . view('sitemap.links', ['links' => $links])->render(), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }


    public function pages()
    {
        $pages = Page::with(['translations'])->active()->where('alias', '0')->get();
        $pages->load('translations');

        $links = [];

        $locales = config('cms.locales');
        $defaultLocale = config('app.locale');

        foreach ($locales as $locale => $localeDetails)
        {
            foreach ($pages as $page)
            {
                /*$translation = $page->getTranslation($locale);
                if(1) //!is_null($translation->id))
                {*/ 
                    if($defaultLocale == $locale)
                    {
                        $link = $page->uri;
                    }
                    else if($page->uri == '/') {
                        $link = $locale;
                    }
                    else {
                        $link = $locale . '/' . ltrim($page->uri, '/');
                    }


                    if(strpos($link, '{') === FALSE){
                        $links[] = url($link);
                    }
                //}
            }
        }

        return $links;

    }

    public function events()
    {
        $items = Event::with(['translations'])->active()->get();
        $items->load(['translations']);

        $links = [];

        $locales = config('cms.locales');
        $defaultLocale = config('app.locale');

        foreach ($locales as $locale => $localeDetails)
        {
            foreach ($items as $item)
            {
                /*$translation = $item->getTranslation($locale);
                if(1) //!is_null($translation->id))
                {*/
                    if($defaultLocale == $locale)
                    {
                        $link = '/knowledge-centre/event/' . $item->id . '/' . $item->slug;
                        $bookingLink = '/knowledge-centre/book/event/' . $item->id . '/' . $item->slug;
                    }
                    else {
                        $link = $locale . '/knowledge-centre/event/' . $item->id . '/' . $item->slug;
                        $bookingLink = $locale . '/knowledge-centre/book/event/' . $item->id . '/' . $item->slug;
                    }

                    $links[] = url($link);
                    $links[] = url($bookingLink);
                //}
            }
        }

        return $links;
    }

    public function courses()
    {
        $items = Course::with(['translations'])->active()->get();
        $items->load(['translations']);

        $links = [];

        $locales = config('cms.locales');
        $defaultLocale = config('app.locale');

        foreach ($locales as $locale => $localeDetails)
        {
            foreach ($items as $item)
            {
                /*$translation = $item->getTranslation($locale);
                if(1) //!is_null($translation->id))
                {*/
                    if($defaultLocale == $locale)
                    {
                        $link = '/calendar/course/' . $item->id . '/' . $item->slug;
                        $bookingUrl = '/calendar/book/course/' . $item->id . '/' . $item->slug;
                    }
                    else {
                        $link = $locale . '/calendar/course/' . $item->id . '/' . $item->slug;
                        $bookingUrl = $locale . '/calendar/book/course/' . $item->id . '/' . $item->slug;
                    }

                    $links[] = url($link);
                    $links[] = url($bookingUrl);
                //}
            }
        }

        return $links;
    }

    public function blog()
    {
        $items = Post::with(['translations'])->active()->get();
        $items->load('translations');

        $links = [];

        $locales = config('cms.locales');
        $defaultLocale = config('app.locale');

        foreach ($locales as $locale => $localeDetails)
        {
            foreach ($items as $item)
            {
                /*$translation = $item->getTranslation($locale);
                if(1) //!is_null($translation->id))
                {*/
                    if($defaultLocale == $locale)
                    {
                        $link = '/knowledge-centre/news-events/article/' . $item->id . '/' . $item->slug;
                    }
                    else {
                        $link = $locale . '/knowledge-centre/news-events/article/' . $item->id . '/' . $item->slug;
                    }

                    $links[] = url($link);
                //}
            }
        }

        return $links;
    }

    public function casestudies()
    {
        $items = Casestudy::with(['translations'])->active()->get();
        $items->load('translations');

        $links = [];

        $locales = config('cms.locales');
        $defaultLocale = config('app.locale');

        foreach ($locales as $locale => $localeDetails)
        {
            foreach ($items as $item)
            {
                /*$translation = $item->getTranslation($locale);
                if(1)//!is_null($translation->id))
                {*/
                    if($defaultLocale == $locale)
                    {
                        $link = '/knowledge-center/casestudies/casestudy/' . $item->id . '/' . $item->slug;
                    }
                    else {
                        $link = $locale . '/knowledge-center/casestudies/casestudy/' . $item->id . '/' . $item->slug;
                    }

                    $links[] = url($link);
                //}
            }
        }

        return $links;
    }
}
