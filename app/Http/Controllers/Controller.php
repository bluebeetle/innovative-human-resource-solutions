<?php

namespace App\Http\Controllers;

use App;
use Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $user;

    public function __construct()
    {

        $this->locale = App::getLocale();
        //$this->user = Auth::user();
        //view()->share('signedIn', Auth::check());
        //view()->share('user', $this->user);
        view()->share('locale', $this->locale);
        $this->localePrefix = ($this->locale == config('app.fallback_locale') ? '' : $this->locale);
        view()->share('localePrefix', $this->localePrefix);
    
        
        if (Input::has('utm_source')) {
            session()->put('source', Input::get('utm_source'));
        }
    
        if (Input::has('utm_medium')) {
            session()->put('medium', Input::get('utm_medium'));
        }
    
        if (Input::has('utm_campaign')) {
            session()->put('campaign', Input::get('utm_campaign'));
        }
    }
}
