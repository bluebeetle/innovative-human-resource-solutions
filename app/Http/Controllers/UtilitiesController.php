<?php

namespace App\Http\Controllers;

use Image;
use Response;
use App\Http\Requests;
use Illuminate\Http\Request;

class UtilitiesController extends Controller
{

    public function imageThumb(Request $request)
    {

        $path = $request->input('path', null);
        $width = $request->input('width', null);
        $height = $request->input('height', null);
        $cropPosition = $request->input('cropPosition', null);

        $imagePath = imageThumb($path, $width, $height, $cropPosition);

        return Image::make($imagePath)->response();
    }

    public function downloadFile(Request $request)
    {

        $path = $request->input('path', null);
        $name = $request->input('name', null);
        return Response::download($path, $name);
    }
}
