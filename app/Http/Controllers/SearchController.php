<?php

namespace App\Http\Controllers;

use App\Page;
use App\Http\Requests;
use App\Utilities\Search;
use Illuminate\Http\Request;

class SearchController extends Controller
{


    public function search(Request $request)
    {

        $q = $request->input('q');
        $results = [];

        if ($request->ajax()) {
            $records = Search::search($q, 6);
            foreach ($records as $record) {
                $results[]  = [
                    'title' => $record->title,
                    'type' => $record->type,
                    'link' => $record->url,
                ];
            }

            if (count($results) == 0) {
                $results[0] =  [
                    'title' => trans('front.No Results Found'),
                    'type'  => 'no-results',
                    'link' => ''
                ];
            } else if (count($results) > 5) {
                $results = array_slice($results, 0, 5);
                $results[5] = [
                    'title' => trans('front.View All'),
                    'type'  => 'view-all',
                    'link' => '/search?q=' . $q
                ];
            }

            return json_encode($results);
        }

        if ($q!='') {
            $records = Search::search($q);

            foreach ($records as $record) {
                $results[]  = [
                'title' => $record->title,
                'content' => $record->content,
                'type' => $record->type,
                'link' => $record->url,
                ];
            }
        }

        if (count($results) == 0) {
            $results[0] =  [
                'title' => 'No Results Found',
                'type'  => 'no-results',
                'link' => ''
            ];
        }

        $pageResult = Page::active()->where('uri', 'search-result');
        if ($pageResult->count() == 0) {
            abort('404');
        }
        $page = $pageResult->first();

        $breadcrumbs = $page->getAncestorsAndSelf();

        $media = $page->mediaArray;

        return view('search', compact('page', 'results', 'breadcrumbs', 'media'));
    }
}
