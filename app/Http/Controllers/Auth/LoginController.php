<?php

namespace App\Http\Controllers\Auth;

use Gate;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    
    /**
     * Where to redirect user after login
     *
     * @return string
     */
    public function redirectPath()
    {
        
        if (Gate::allows('login-to-cms')) {
            return '/cms/dashboard';
        }
    
        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }
}
