<?php

namespace App\Http\Controllers;

use App\Course;
use App\Event;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Thetispro\Setting\Facades\Setting;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\Mail\BookNewCourse;
use App\Mail\BookNewEvent;

class BookingController extends Controller
{

    public function bookCourse(Requests\BookCourseRequest $request, $id, $slug)
    {
        
        $course = Course::find($id);

        $from = Setting::get('emails.from');
        $fromChunks = explode("|", $from);

        // Email
        
        $emailData = [
            'replyTo' => $fromChunks,
            'course' => $course,
            'parameters' => $request->except('_token')
        ];
        
        Mail::to(Setting::get('emails.bookCourse'))
            ->queue(new BookNewCourse($emailData));
        
        
        // Salesforce
        $httpClient = new GuzzleHttpClient([
            'base_uri' => 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8',
            'timeout'  => 10.0,
            'allow_redirects' => false
        ]);

        $response = $httpClient->request('POST', 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8', [
                'form_params' => [
                'oid' => '00D24000000YsQ8',
                'retURL' => route('courseBookingThankYou'),
                'first_name' => $request->input('firstName'),
                'last_name' => $request->input('lastName'),
                'title' => $request->input('jobTitle'),
                'company' => ($request->input('bookingType')=='company') ? $request->input('companyName') : '',
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                '00N2400000IOZ6v' => $request->input('bookingDate'),
                '00N2400000Ir9DN' => $request->input('methodOfPayment'),
                '00N2400000Ir9Ct' => $request->input('hearFrom'),
                '00N2400000Ir9DS' => $request->input('behalfOfParticipant')=="yes" ? "YES - Include Sponsors First Last Name, Phone Number and Email":"NO - Proceed to Electronic Signature and Date",
                '00N2400000Ir9DX' => $request->input('acceptTerms'),
                '00N2400000Ir9Dc' => $request->input('additionalRequirements'),
                '00N2400000IOkU6' => $request->input('sponsorEmail'),
                '00N2400000IOkUB' => $request->input('sponsorPhone'),
                '00N2400000Ir9D3' => 'Accreditation Programmes Registration',
                '00N2400000Iv438' => $course->title,
                'source' => session('source'),
                'medium' => session('medium'),
                'campaign' => session('campaign'),
                'submit' => 'submit'
                ]
        ]);

        $code = $response->getStatusCode();

        if ($code == 200) {
            echo $response->getBody();
        } else {
            return redirect()->route('course', [$course->id, $course->slug])->withErrors(['bookingError' => 'bookingError']);
        }
    }



    public function bookEvent(Requests\BookEventRequest $request, $id, $slug)
    {


        $event = Event::find($id);

        $from = Setting::get('emails.from');
        $fromChunks = explode("|", $from);
    
        $emailData = [
            'replyTo' => $fromChunks,
            'event' => $event,
            'parameters' => $request->except('_token')
        ];
    
        Mail::to(Setting::get('emails.bookEvent'))
            ->queue(new BookNewEvent($emailData));


        // Salesforce
        $httpClient = new GuzzleHttpClient([
            'base_uri' => 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8',
            'timeout'  => 10.0,
            'allow_redirects' => false
        ]);

        $response = $httpClient->request('POST', 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8', [
            'form_params' => [
                'oid' => '00D24000000YsQ8',
                'retURL' => route('eventBookingThankYou'),
                'first_name' => $request->input('firstName'),
                'last_name' => $request->input('lastName'),
                'title' => $request->input('jobTitle'),
                'company' => ($request->input('bookingType')=='company') ? $request->input('companyName') : '',
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                '00N2400000IOZ6v' => $request->input('bookingDate'),
                '00N2400000Ir9Ct' => $request->input('hearFrom'),
                '00N2400000Ir9D3' => $event->slug == 'mbti' ? 'MBTI Forum' : 'Professional Development Breakfast',
                '00N2400000Iv438' => $event->title,
                'source' => session('source'),
                'medium' => session('medium'),
                'campaign' => session('campaign'),
                'submit' => 'submit'
            ]
        ]);


        $code = $response->getStatusCode();

        if ($code == 200) {
            echo $response->getBody();
        } else {
            return redirect()->route('event', [$event->id, $event->slug])->withErrors(['bookingError' => 'bookingError']);
        }
    }
}
