<?php

namespace App\Http\Controllers;

#use Illuminate\Http\Request;
#use App\Http\Requests;
use App\Page;
use Session;
#use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\Input;
use Cache;

class PageController extends Controller
{
    
    
    public function show(Page $page, array $parameters)
    {
        if (!config('app.debug')) {
            $cacheName = $this->_getUniqueCacheName($page, $parameters);
            $template = Cache::rememberForever($cacheName, function () use ($page, $parameters) {
                return $this->prepareTemplate($page, $parameters)
                    ->render();
            });
            
            // The CSRF token will be old if the view is loaded from cache.
            // So replace the CSRF token bellow with new one.
            $token = csrf_token();
            $template = preg_replace(
                '/(<input\s*type="hidden"\s*name="_token"\s*value=").*?(">)/',
                "<input type=\"hidden\" name=\"_token\" value=\"" . $token . "\">",
                $template
            );
        } else {
            $template = $this->prepareTemplate($page, $parameters);
        }
        
        
        return $template; //view('page', compact('page'));
    }
    
    /*public function show(Page $page, array $parameters)
    {
        
        $template = $this->prepareTemplate($page, $parameters);


        return $template; //view('page', compact('page'));
    }*/

    protected function prepareTemplate(Page $page, array $parameters)
    {
        $templates = config('cms.templates');

        if (! $page->template || ! isset($templates[$page->template])) {
            return;
        }

        $template = app($templates[$page->template]);

        $view = sprintf('templates.%s', $template->getView());

        if (! view()->exists($view)) {
            return;
        }

        $template->prepare($view = view($view), $page, $parameters);

        //$page->view = $view;
        return $view;
    }
    
    private function _getUniqueCacheName($page, array $parameters)
    {
        $selectedHotelSlug = Session::get('selectedHotelSlug');
        
        $activeSessionVars = [];
        $sessionVarNames = ["rsvpSent", "applicationSent", "newsletterSubscribed", "promotionBookingRequest"];
        foreach ($sessionVarNames as $sessionVarNames) {
            $sessionVar = Session::get($sessionVarNames);
            if ($sessionVar == true) {
                $activeSessionVars[] = $sessionVar;
            }
        }
        
        $cacheName = $this->locale . '-'
            . $selectedHotelSlug . '-'
            . implode('-', $activeSessionVars) . '-'
            . $page->name;
        
        $getParams = Input::get();
        if (count($getParams) > 0) {
            $parameters = array_merge($parameters, $getParams);
        }
        
        foreach ($parameters as $key => $param) {
            $cacheName .= '-' . $key . '-' . $param;
        }
        
        return md5($cacheName);
    }
    
    public function clearCache()
    {
        Cache::flush();
        
        flash()->success('Cache cleared.');
        
        return redirect()->back();
    }
}
