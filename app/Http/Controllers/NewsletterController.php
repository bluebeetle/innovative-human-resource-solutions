<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client as GuzzleHttpClient;

class NewsletterController extends Controller
{

    public function subscribe(Requests\SubscribeNewsletterRequest $request)
    {
        
        // Salesforce
        $httpClient = new GuzzleHttpClient([
            'base_uri' => 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8',
            'timeout'  => 10.0,
            'allow_redirects' => false
        ]);

        // Constant Contact
        $CCResponse = $httpClient->request('POST', 'https://visitor2.constantcontact.com/api/signup', [
            'form_params' => [
                'ca' => '261a9b3c-62be-4545-a9db-251bf0f58f85',
                'list' => '2138424441',
                'source' => 'EFD',
                'email' => $request->input('email'),
                'submit' => 'submit'
            ]
        ]);

        // Salesforce
        //url('/thank-you-for-subscription')
        $SFResponse = $httpClient->request('POST', 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8', [
            'form_params' => [
                'oid' => '00Di0000000KIeR',
                'retURL' => route('thankYouForSubscription'),
                'email' => $request->input('email'),
                '00N2400000Ir9D3' => 'Newsletter Subscription',
                'source' => session('source'),
                'medium' => session('medium'),
                'campaign' => session('campaign'),
                'submit' => 'submit',
                'LeadSource' => 'Web',
                'Lead_Source_Description__c' => 'ContactUsForm',
                'CompanyDivision__c' => 'IHS'
            ]
        ]);


        $code = $SFResponse->getStatusCode();

        if ($code == 200) {
            echo $SFResponse->getBody();
        } else {
            return redirect()->route('home');
        }
    }
}
