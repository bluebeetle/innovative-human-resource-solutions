<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Request;

class BookEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bookingDate' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'jobTitle' => 'required',
            'bookingType' => 'required',
            'companyName' => 'required_if:bookingType,company',
            'phone' => 'required',
            'email' => 'required|email',
        ];
    }
}
