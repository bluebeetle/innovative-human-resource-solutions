<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Request;

class StoreCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'unique:events'],
            'slug' => ['required', 'unique:events'],
            'title' => ['required'],
            'cost' => ['required'],
            'logo' => ['required','mimes:jpeg,jpg,png','max:2048' ]
        ];

        foreach ($this->request->get('start_date') as $key => $val) {
            $rules['start_date.' . $key] = ['required', 'date'];
        }

        foreach ($this->request->get('end_date') as $key => $val) {
            $rules['end_date.' . $key] = ['required', 'date'];
        }

        foreach ($this->request->get('location') as $key => $val) {
            $rules['location.' . $key] = ['required'];
        }

        return $rules;
    }
}
