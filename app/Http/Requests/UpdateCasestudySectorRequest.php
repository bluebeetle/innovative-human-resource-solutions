<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App;
use App\Http\Requests\Request;

class UpdateCasestudySectorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:casestudy_sectors,name,'.$this->route('casestudySector')],
            'title' => ['required', 'unique:casestudy_sector_translations,title,'.$this->route('casestudySector').',sector_id,locale,'. App::getLocale()]
        ];
    }
}
