<?php

namespace App\Http\Middleware;

use Gate;
use Closure;

class AuthenticateCMS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Gate::denies('login-to-cms')) {
            return redirect('/admin');
        }
        return $next($request);
    }
}
