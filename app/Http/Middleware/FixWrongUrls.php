<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class FixWrongUrls
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = $request->getHost();
        
        if (!in_array($host, ['middleeast-psionline.localhost','ihs.bluebeetle.net', 'middleeast.psionline.com'])) {
            die('Wrong Url.');
        }


        
        /**
         * Remove index.php from url, and redirect
         */
        $url = url()->current();
        $rep_url = str_replace('/index.php', '', $url);
        if ($url !== $rep_url) {
            return redirect($rep_url, 301);
        }


        /**
         * There are two homepages one for english (path: /en) & other is used for arabic (path : /ar/home).
         * it should redirect to correct homepage for each language.
         */

         $path = trim(request()->path(), '/');
         $locale = app()->getLocale();
         if($locale == 'ar' && $path == 'ar')
         {
             return redirect('/ar/home', 301);
         }
         else if($locale == 'en' && $path == 'en/home')
         {
            return redirect('/', 301);
         }


        
        return $next($request);
    }
}
