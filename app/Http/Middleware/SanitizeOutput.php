<?php

namespace App\Http\Middleware;

use Closure;

class SanitizeOutput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if ($response instanceof \Symfony\Component\HttpFoundation\BinaryFileResponse) {
            return $response;
        } else {
            $buffer = $response->getContent();
            $replace = [
                '/\>[^\S ]+/s' => '>', //strip whitespaces after tags, except space
                '/[^\S ]+\</s' => '<', //strip whitespaces before tags, except space
                '/(\s)+/' => '\\1', // shorten multiple whitespace sequences
                "/\n+/" => "\n",
            ];

            $buffer = preg_replace(array_keys($replace), array_values($replace), $buffer);
            $response->setContent($buffer);
            return $response;
        }
    }
}
