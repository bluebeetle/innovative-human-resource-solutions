<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestimonialTranslation extends Model
{
    protected $fillable = ['locale', 'title', 'content'];

    public $timestamps = false;
}
