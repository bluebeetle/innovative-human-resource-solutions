<?php

namespace App;

use App;
use App\Presenters\Traits\PresentableTrait;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Media extends Model
{
    use PresentableTrait;
    
    protected $presenter = \App\Presenters\MediaPresenter::class;
    
    protected $fillable = ['title', 'name', 'type', 'path', 'caption', 'description', 'created_by', 'updated_by'];


    /*public function pages(){
        return $this->morphedByMany('App\Page', 'mediable')->withPivot('collection');
    }*/

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value ?: null;
    }

    public function setCaptionAttribute($value)
    {
        $this->attributes['caption'] = $value ?: null;
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = $value ?: null;
    }


    /*public function newPivot(Model $parent, array $attributes, $table, $exists, $using = null)
    {

        $config = App::make('config');
        $mediables = $config->get('cms.mediables', []);

        foreach ($mediables as $mediable) {
            if ($parent instanceof $mediable['model']) {
                return new MediablePivot($parent, $attributes, $table, $exists, $using);
            }
        }

        return parent::newPivot($parent, $attributes, $table, $exists, $using);
    }*/

    /*public function baseDir($type)
    {
        return 'images/uploads';
    }*/

    /*public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;

        $this->path = $this->baseDir() . "/" . $name;
    }*/

    public function makeFile(UploadedFile $file)
    {

        $newFile = new static;

        $newFile->name = $this->makeFileName($file);
        $newFile->type = $this->getFileType($file);
        $newFile->path = config('cms.uploads.paths.' . ($newFile->type=="image"?"images":"documents")) . '/' . $newFile->name;
        $newFile->title = $file->getClientOriginalName();

        return $newFile;
    }

    public function makeFileName(UploadedFile $file)
    {

        $name  = sha1(
            time() . $file->getClientOriginalName()
        );

        $extension = $file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }

    public function getFileType(UploadedFile $file)
    {
        $type = $file->getClientMimeType();
        $newType = '';
        if (strstr($type, 'image') !== false) {
            $newType = 'image';
        } else if (strstr($type, 'pdf') !== false) {
            $newType = 'pdf';
        }
        return $newType;
    }

    public function scopeOfType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function deleteThumbs()
    {

        deleteThumbs($this->path);
    }
}
