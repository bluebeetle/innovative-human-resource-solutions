<?php

namespace App;

use App\Traits\CommonModelTrait;
use File;
use Image;
use Baum\Node;
use App\Traits\OrderableTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Http\UploadedFile;

class Employee extends Node
{
    use TranslatableTrait;
    use OrderableTrait;
    use CommonModelTrait;

    protected $fillable = ['name', 'title', 'designation', 'content', 'dp', 'dp_tinified', 'facebook', 'twitter', 'linkedin', 'status', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title','designation', 'content'];

    public $imageColumnName = 'dp';

    public function saveDP(UploadedFile $dpFile)
    {
        $name = sha1($this->id) . '.' . $dpFile->getClientOriginalExtension();

        $this->deleteThumbs();

        Image::make($dpFile)
            ->resize(config('cms.uploads.maxImageWidth'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->interlace()
            ->save(config('cms.uploads.paths.images') . '/employees/' . $name, config('cms.uploads.imageQuality'));

        $this->fill(['dp'=> config('cms.uploads.paths.images') . '/employees/' . $name, 'dp_tinified' => 0]);

        if ($this->isDirty()) {
            $this->save();
        }
    }

    public function deleteThumbs($path = '')
    {

        if (empty($path)) {
            $path = $this->dp;
        }

        if (!empty($path)) {
            $pathChunks = explode("/", $path);
            $imageName = array_pop($pathChunks);

            $files = glob(implode("/", $pathChunks) . "/tn-*" . $imageName);
            foreach ($files as $file) {
                if (File::isFile($file)) {
                    File::delete($file);
                }
            }
        }
    }
}
