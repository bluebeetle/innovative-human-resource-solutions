<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AwardTranslation extends Model
{
    protected $fillable = ['locale', 'title', 'content'];

    public $timestamps = false;
}
