<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeTranslation extends Model
{
    protected $fillable = ['locale', 'title', 'designation', 'content'];

    public $timestamps = false;
}
