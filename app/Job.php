<?php

namespace App;

use App\Traits\CommonModelTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use TranslatableTrait;
    use CommonModelTrait;

    protected $fillable = ['name', 'apply_link', 'status', 'created_by', 'updted_by'];

    public $translatedAttributes = ['title', 'content', 'meta_title', 'meta_keywords', 'meta_description'];
}
