<?php

namespace App;

use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Relations\Pivot;

class MediablePivot extends Pivot
{
    use TranslatableTrait;

    public $table = 'mediables';

    public $timestamps = false;

    public $translationModel = \App\MediableTranslation::class;

    public $translatedAttributes = ['caption'];
}
