<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryTranslation extends Model
{

    protected $fillable = ['locale', 'title'];

    public $timestamps = false;
}
