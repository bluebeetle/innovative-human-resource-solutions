<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientTranslation extends Model
{
    protected $fillable = ['locale', 'title', 'content'];

    public $timestamps = false;
}
