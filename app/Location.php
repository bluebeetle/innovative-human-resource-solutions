<?php

namespace App;

use Baum\Node;
use App\Traits\OrderableTrait;
use App\Traits\CommonModelTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Location extends Node
{
    use TranslatableTrait;
    use OrderableTrait;
    use CommonModelTrait;

    protected $fillable = ['name', 'landscape', 'coordinates', 'head_office', 'phone', 'status', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title'];

    public $imageColumnName = 'landscape';

    public function setHeadOfficeAttribute($value)
    {
        $this->attributes['head_office'] = $value ?: 0;
    }
}
