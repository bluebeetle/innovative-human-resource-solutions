<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableTrait;

class CustomField extends Model
{
    use TranslatableTrait;

    protected $fillable = ['name', 'value', 'order', 'model_id', 'model_type'];

    public $translatedAttributes = ['value'];

    public $timestamps = false;
}
