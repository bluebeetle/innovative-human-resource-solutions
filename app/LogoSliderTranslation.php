<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogoSliderTranslation extends Model
{
    protected $fillable =  ['locale', 'link'];

    public $timestamps = false;
}
