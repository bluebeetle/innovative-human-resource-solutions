<?php

namespace App;

use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class CasestudyCategory extends Model
{
    use TranslatableTrait;

    public $translationForeignKey = 'category_id';

    protected $fillable = ['name', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title'];

    public function casestudies()
    {
        return $this->hasMany(Casestudy::class, 'category_id');
    }
}
