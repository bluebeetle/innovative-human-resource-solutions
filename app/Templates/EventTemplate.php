<?php

namespace App\Templates;

use App\Event;
use App\Page;
use Illuminate\View\View;
use Carbon\Carbon;
use App\Utilities\MetaTags as MetaTagsUtility;

class EventTemplate extends AbstractTemplate
{
    protected $view = 'event';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $event = Event::active()->with('dates')
                    ->where(function ($query) use ($parameters) {
                        $query->whereSlug($parameters['slug']);
                        $query->whereId($parameters['id']);
                    })
                    ->first();

        if (!$event) {
            abort('404');
        }
        $view->with('event', $event);

        $view->with('page', $page);

        $sortedMediaArray = $event->getSortedMediaArray();
        $view->with('sortedMediaArray', $sortedMediaArray);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $upcomingDate = $this->getUpcomingDate($event->dates);
        $view->with('upcomingDate', $upcomingDate);
        $view->with('galleryColumns', ['one-fourth','one-fourth', 'half', 'half', 'one-fourth','one-fourth']);

        $this->setPageMeta($event, $event->mediaArray);
    }

    protected function getUpcomingDate($dates)
    {
        $tmpDates = collect();
        foreach ($dates as $date) {
            $tmp = collect();
            $tmp->put('start_date', $date->start_date);
            $tmp->put('end_date', $date->end_date);
            $tmp->put('location', $date->location);
            $tmpDates->push($tmp);

            $tmp = collect();
            $tmp->put('start_date', $date->start_date->addYear());
            $tmp->put('end_date', $date->end_date->addYear());
            $tmp->put('location', $date->location);
            $tmpDates->push($tmp);
        }

        $recentUpcomingDate = $tmpDates->filter(function ($value, $key) {
            return $value['start_date'] > Carbon::now();
        })->sortBy('start_date')->first();

        return $recentUpcomingDate;
    }

    protected function setPageMeta($event, $media)
    {

        if ($event->meta_title) {
            $title = $event->meta_title;
        } else {
            $title = $event->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $event->meta_keywords,
            'description' => $event->meta_description,
            'image' => $image
        ]);
    }
}
