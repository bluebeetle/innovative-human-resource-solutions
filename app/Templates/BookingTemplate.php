<?php

namespace App\Templates;

use App\Event;
use App\Course;
use App\Page;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class BookingTemplate extends AbstractTemplate
{
    protected $view = 'booking';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $view->with('page', $page);

        if ($parameters['type']=='event') {
            $occasion = Event::active()
                ->where(function ($query) use ($parameters) {
                    $query->whereSlug($parameters['slug']);
                    $query->whereId($parameters['id']);
                })
                ->first();
        } else if ($parameters['type']=='program' || $parameters['type']=='course') {
            $parameters['type'] = 'course';
            $occasion = Course::active()
                ->where(function ($query) use ($parameters) {
                    $query->whereSlug($parameters['slug']);
                    $query->whereId($parameters['id']);
                })
                ->first();
        }

        $breadcrumbs = $page->getAncestorsAndSelf();
        $breadcrumbsCount = $breadcrumbs->count();
        $breadcrumbs->put($breadcrumbsCount, $breadcrumbs->last());
        $breadcrumbs->put($breadcrumbsCount - 1, castToObj(['title'=>$occasion->title, 'uri' => route($parameters['type'], [$occasion->id, $occasion->slug])]));

        $view->with('breadcrumbs', $breadcrumbs);

        if (!$occasion) {
            abort('404');
        }

        $view->with('type', $parameters['type']);
        $view->with('occasion', $occasion);

        $this->setPageMeta($occasion, $occasion->mediaArray);
    }

    protected function setPageMeta($occasion, $media)
    {

        if ($occasion->meta_title) {
            $title = $occasion->meta_title;
        } else {
            $title = $occasion->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $occasion->meta_keywords,
            'description' => $occasion->meta_description,
            'image' => $image
        ]);
    }
}
