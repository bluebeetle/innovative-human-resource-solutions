<?php

namespace App\Templates;

use App\Page;
use Illuminate\View\View;

abstract class AbstractTemplate
{
    protected $view;

    abstract public function prepare(View $view, Page $page, array $parameters);

    public function getView()
    {
        return $this->view;
    }
}
