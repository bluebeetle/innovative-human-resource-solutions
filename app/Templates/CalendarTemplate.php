<?php

namespace App\Templates;

use App\Course;
use App\Page;
use Illuminate\View\View;
use Carbon\Carbon;
use App\Utilities\MetaTags as MetaTagsUtility;

class CalendarTemplate extends AbstractTemplate
{
    protected $view = 'calendar';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $courses = Course::active()->with('dates')->orderBy('lft', 'asc')->get();
        $view->with('courses', $courses);

        $months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        $view->with('months', $months);

        $dates = [];
        $years = [];
        foreach ($courses as $course) {
            $tmpDates = [];
            foreach ($course->dates as $date) {
                $tmpDates[$date->start_date->format('Y')][$date->start_date->format('n')-1] = [
                    'start_date' => $date->start_date,
                    'end_date' => $date->end_date,
                    'location' => $date->location,
                    'monthsDiff' => abs($date->end_date->format('n') - $date->start_date->format('n'))
                ];
            }

            $tmpMonthDates = [];
            $monthsToSkip = 0;
            $allYears = array_keys($tmpDates);
            sort($allYears);


            foreach ($allYears as $year) {
                if ($year < date('Y')) {
                    continue;
                }

                array_push($years, $year);

                foreach ($months as $k => $month) {
                    if ($monthsToSkip > 0) {
                        $monthsToSkip--;
                        continue;
                    }

                    if (isset($tmpDates[$year][$k])) {
                        $tmpMonthDates[$year][$k] = $tmpDates[$year][$k];
                        $monthsToSkip = $tmpDates[$year][$k]['monthsDiff'];
                    } else {
                        $tmpMonthDates[$year][$k] = '';
                    }
                }
            }

            $dates[$course->id] = $tmpMonthDates;
        }

        $view->with('dates', $dates);

        $years = array_unique($years);
        sort($years);
        $view->with('years', $years);

        $this->setPageMeta($page, $page->mediaArray);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
