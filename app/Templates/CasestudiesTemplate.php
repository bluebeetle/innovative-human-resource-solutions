<?php

namespace App\Templates;

use App\Page;
use App\Casestudy;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class CasestudiesTemplate extends AbstractTemplate
{
    protected $view = 'casestudies';

    public function prepare(View $view, Page $page, array $parameters)
    {
        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $casestudies = Casestudy::active()->with('category')->with('sector')->paginate(10);
        $view->with('casestudies', $casestudies);

        $this->setPageMeta($page, $page->mediaArray);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
