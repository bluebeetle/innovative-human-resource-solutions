<?php

namespace App\Templates;

use App\Event;
use App\Page;
use App\Post;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class KnowledgeCenterTemplate extends AbstractTemplate
{
    protected $view = 'knowledgeCenter';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $view->with('page', $page);

        /* $sortedMediaArray = $page->getSortedMediaArray();
        $view->with('sortedMediaArray', $sortedMediaArray); */

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $events = Event::active()->with('media')->take(2)->get();
        $view->with('events', $events);

        /* $latestPost = Post::published()
            ->orderBy('published_at', 'desc')
            ->first();
        $view->with('latestPost', $latestPost); */

        /* $view->with('galleryColumns', ['one-fourth','one-fourth', 'half', 'half', 'one-fourth','one-fourth']); */

        $this->setPageMeta($page, $page->mediaArray);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
