<?php

namespace App\Templates;

use App\Page;
use App\Post;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class ArticleTemplate extends AbstractTemplate
{
    protected $view = 'article';

    public function prepare(View $view, Page $page, array $parameters)
    {
        $post = Post::active()
                    ->where(function ($query) use ($parameters) {
                        $query->whereSlug($parameters['slug']);
                        $query->whereId($parameters['id']);
                    })
                    ->with('categories')->first();

        if (!$post) {
            abort('404');
        }
        $view->with('post', $post);

        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $this->setPageMeta($post, $post->mediaArray);
    }

    protected function setPageMeta($post, $media)
    {

        if ($post->meta_title) {
            $title = $post->meta_title;
        } else {
            $title = $post->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $post->meta_keywords,
            'description' => $post->meta_description,
            'image' => $image
        ]);
    }
}
