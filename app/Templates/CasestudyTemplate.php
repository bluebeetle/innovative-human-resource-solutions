<?php

namespace App\Templates;

use App\Page;
use App\Casestudy;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class CasestudyTemplate extends AbstractTemplate
{
    protected $view = 'casestudy';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $casestudy = Casestudy::active()
            ->with('category')
            ->with('sector')
            ->where(function ($query) use ($parameters) {
                $query->whereSlug($parameters['slug']);
                $query->whereId($parameters['id']);
            })
            ->first();
        
        if (!$casestudy) {
            abort('404');
        }
        $view->with('casestudy', $casestudy);

        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $this->setPageMeta($casestudy, $casestudy->mediaArray);
    }

    protected function setPageMeta($casestudy, $media)
    {

        if ($casestudy->meta_title) {
            $title = $casestudy->meta_title;
        } else {
            $title = $casestudy->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $casestudy->meta_keywords,
            'description' => $casestudy->meta_description,
            'image' => $image
        ]);
    }
}
