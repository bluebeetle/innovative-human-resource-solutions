<?php

namespace App\Templates;

use App\Page;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class PageTemplate extends AbstractTemplate
{
    protected $view = 'page';


    public function prepare(View $view, Page $page, array $parameters)
    {
        
        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $children = $page->leaves()->visible()->get();
        $view->with('children', $children);

        if ($page->depth == 0) {
            $siblings = collect();
        } else {
            $siblings = $page->siblingsAndSelf()->visible()->active()->get();
            if ($siblings->count() == 1) {
                $siblings = collect();
            }
        }
        
        
        $view->with('siblings', $siblings);

        $media = $page->mediaArray;
        $view->with('media', $media);

        $customFields = $page->customFieldsCollection;
        $view->with('customFields', $customFields);

        $this->setPageMeta($page, $media);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
