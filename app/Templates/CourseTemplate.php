<?php

namespace App\Templates;

use App\Course;
use App\Page;
use Illuminate\View\View;
use Carbon\Carbon;
use App\Utilities\MetaTags as MetaTagsUtility;
use Illuminate\Http\Request;

class CourseTemplate extends AbstractTemplate
{
    protected $view = 'course';


    public function prepare(View $view, Page $page, array $parameters)
    {
        
        $course = Course::active()->with('dates')
                        ->where(function ($query) use ($parameters) {
                            $query->whereSlug($parameters['slug']);
                            $query->whereId($parameters['id']);
                        })
                        ->first();

        if (!$course) {
            abort('404');
        }
        $view->with('course', $course);

        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        if (isset($_GET['referrer'])) {
            $pageBreadcrumbs = $breadcrumbs;
            $referrerBreadcrumbs = Page::find($_GET['referrer'])->getAncestorsAndSelf();

            $breadcrumbs = collect();
            foreach ($referrerBreadcrumbs as $bc) {
                $breadcrumbs->push($bc);
            }
            $breadcrumbs->push($pageBreadcrumbs->last());
        }

        $view->with('breadcrumbs', $breadcrumbs);

        $months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        $view->with('months', $months);


        $years = [];

        $tmpDates = [];
        foreach ($course->dates as $date) {
            $tmpDates[$date->start_date->format('Y')][$date->start_date->format('n')-1] = [
                'start_date' => $date->start_date,
                'end_date' => $date->end_date,
                'location' => $date->location,
                'monthsDiff' => abs($date->end_date->format('n') - $date->start_date->format('n'))
            ];
        }

        $tmpMonthDates = [];
        $monthsToSkip = 0;
        $allYears = array_keys($tmpDates);
        sort($allYears);


        foreach ($allYears as $year) {
            if ($year < date('Y')) {
                continue;
            }

            array_push($years, $year);

            foreach ($months as $k => $month) {
                if ($monthsToSkip > 0) {
                    $monthsToSkip--;
                    continue;
                }

                if (isset($tmpDates[$year][$k])) {
                    $tmpMonthDates[$year][$k] = $tmpDates[$year][$k];
                    $monthsToSkip = $tmpDates[$year][$k]['monthsDiff'];
                } else {
                    $tmpMonthDates[$year][$k] = '';
                }
            }
        }

        $dates = $tmpMonthDates;

        $view->with('dates', $dates);

        $years = array_unique($years);
        sort($years);
        $view->with('years', $years);

        $this->setPageMeta($course, $course->mediaArray);
    }

    protected function setPageMeta($course, $media)
    {

        if ($course->meta_title) {
            $title = $course->meta_title;
        } else {
            $title = $course->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $course->meta_keywords,
            'description' => $course->meta_description,
            'image' => $image
        ]);
    }
}
