<?php

namespace App\Templates;

use App\Client;
use App\Page;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class ClientsTemplate extends AbstractTemplate
{
    protected $view = 'clients';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $siblings = $page->siblings()->visible()->active()->get();
        $view->with('siblings', $siblings);

        $clients = Client::active()->orderBy('lft', 'asc')->get();
        $view->with('clients', $clients);

        $this->setPageMeta($page, $page->mediaArray);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
