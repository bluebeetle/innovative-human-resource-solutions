<?php

namespace App\Templates;

use App\Page;
use App\Post;
use App\Event;
use App\Course;
use Carbon\Carbon;
use App\LogoSlider;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class HomeTemplate extends AbstractTemplate
{
    protected $view = 'home';


    public function prepare(View $view, Page $page, array $parameters)
    {

        $view->with('page', $page);

        $media = $page->mediaArray;
        $view->with('media', $media);

        $customFields = $page->customFieldsCollection;
        $view->with('customFields', $customFields);

        $this->setPageMeta($page, $media);

        $events = $this->getEvents();
        $view->with('events', $events);

        $courses = $this->getCourses();
        $view->with('courses', $courses);

        $latestNews = Post::published()
            ->whereHas('categories', function ($query) {
                $query->where('name', 'news');
            })
            ->orderBy('published_at', 'desc')
            ->first();
        $view->with('latestNews', $latestNews);

        $psiAcquiringNotification = Post::published()->whereSlug('launching-psi-middle-east')->first();
        $view->with('psiAcquiringNotification', $psiAcquiringNotification);

        //$allCourses = Course::active()->with('dates')->get();
        //$view->with('allCourses', $allCourses);

        $logoSliders = LogoSlider::active()->get();
        $view->with('logoSliders', $logoSliders);
    }

    protected function getEvents()
    {

        $events = collect();

        foreach (Event::active()->with('dates')->get() as $event) {
            $dates = collect();
            foreach ($event->dates as $date) {
                $tmp = collect();
                $tmp->put('start_date', $date->start_date);
                $tmp->put('end_date', $date->end_date);
                $dates->push($tmp);

                $tmp = collect();
                $tmp->put('start_date', $date->start_date->addYear());
                $tmp->put('end_date', $date->end_date->addYear());
                $dates->push($tmp);
            }

            $recentUpcomingDate = $dates->filter(function ($value, $key) {
                return $value['start_date'] > Carbon::now();
            })->sortBy('start_date')->first();
            
            if (!is_null($recentUpcomingDate)) {
                $tmpEvent = collect([
                    'id' => $event->id,
                    'slug' => $event->slug,
                    'title' => $event->title,
                    'start_date' => $recentUpcomingDate->get('start_date'),
                    'dates' => presentOccasionDates($recentUpcomingDate->get('start_date'), $recentUpcomingDate->get('end_date'), '-', null)
                ]);
    
                $events->push($tmpEvent);
            }
        }

        return $events->sortBy('start_date');
    }

    protected function getCourses()
    {

        $processedCourses = collect();

        $courses = Course::active()->with(['dates' => function ($query) {
            $query->where('start_date', '>', Carbon::now());
            $query->orderBy('start_date');
        }])->get();

        foreach ($courses as $course) {
            if ($course->dates->count()) {
                $tmpCourse = collect([
                    'id' => $course->id,
                    'slug' => $course->slug,
                    'title' => $course->title,
                    'start_date' => $course->dates->first()->start_date,
                    'dates' => presentOccasionDates($course->dates->first()->start_date, $course->dates->first()->end_date, '-', null),
                ]);

                $processedCourses->push($tmpCourse);
            }
        }

        return $processedCourses->sortBy('start_date')->take(2);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
