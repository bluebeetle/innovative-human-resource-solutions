<?php

namespace App\Templates;

use App\Page;
use App\Post;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class AboutTemplate extends AbstractTemplate
{
    protected $view = 'about';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $children = $page->leaves()->visible()->get();
        $view->with('children', $children);

        $media = $page->mediaArray;
        $view->with('media', $media);

        $customFields = $page->customFieldsCollection;
        $view->with('customFields', $customFields);

        $post_cta = Post::where('slug', 'ihs-has-been-acquired-by-psi')->first();
        $view->with('post_cta', $post_cta);

        $this->setPageMeta($page, $media);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
