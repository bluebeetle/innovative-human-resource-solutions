<?php

namespace App\Templates;

use App\Page;
use Illuminate\View\View;
use App\Utilities\MetaTags as MetaTagsUtility;

class OverviewPageTemplate extends AbstractTemplate
{
    protected $view = 'overviewPage';


    public function prepare(View $view, Page $page, array $parameters)
    {
        $view->with('page', $page);

        $breadcrumbs = $page->getAncestorsAndSelf();
        $view->with('breadcrumbs', $breadcrumbs);

        $children = $page->immediateDescendants()->active()->visible()->get();
        $view->with('children', $children);
        
        
        $media = $page->mediaArray;
        $view->with('media', $media);

        $customFields = $page->customFieldsCollection;
        $view->with('customFields', $customFields);

        $this->setPageMeta($page, $page->mediaArray);
    }

    protected function setPageMeta($page, $media)
    {

        if ($page->meta_title) {
            $title = $page->meta_title;
        } else {
            $title = $page->title;
        }

        $image = '';
        if (isset($media['meta'])) {
            $image = $media['meta'][0]['path'];
        } elseif (isset($media['banner'])) {
            $image = $media['banner'][0]['path'];
        }

        MetaTagsUtility::setAll([
            'title'=> $title,
            'keywords' => $page->meta_keywords,
            'description' => $page->meta_description,
            'image' => $image
        ]);
    }
}
