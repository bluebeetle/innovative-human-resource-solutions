<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasestudyCategoryTranslation extends Model
{
    protected $fillable = ['locale', 'title'];

    public $timestamps = false;
}
