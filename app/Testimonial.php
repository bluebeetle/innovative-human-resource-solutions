<?php

namespace App;

use File;
use Image;
use Baum\Node;
use App\Traits\OrderableTrait;
use App\Traits\CommonModelTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Http\UploadedFile;

class Testimonial extends Node
{
    use TranslatableTrait;
    use OrderableTrait;
    use CommonModelTrait;

    protected $fillable = ['name', 'title', 'content', 'logo', 'status', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title', 'content'];

    public $imageColumnName = 'logo';

    public function saveLogo(UploadedFile $logoFile)
    {
        $name = sha1($this->id) . '.' . $logoFile->getClientOriginalExtension();

        $this->deleteThumbs();

        Image::make($logoFile)
            ->resize(config('cms.uploads.maxImageWidth'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->interlace()
            ->save(config('cms.uploads.paths.images') . '/testimonial/' . $name, config('cms.uploads.imageQuality'));

        $this->fill(['logo'=> config('cms.uploads.paths.images') . '/testimonial/' . $name,
            'logo_tinified' => 0]);

        if ($this->isDirty()) {
            $this->save();
        }
    }

    public function deleteThumbs($path = '')
    {

        if (empty($path)) {
            $path = $this->logo;
        }

        if (!empty($path)) {
            $pathChunks = explode("/", $path);
            $imageName = array_pop($pathChunks);

            $files = glob(implode("/", $pathChunks) . "/tn-*" . $imageName);
            foreach ($files as $file) {
                if (File::isFile($file)) {
                    File::delete($file);
                }
            }
        }
    }
}
