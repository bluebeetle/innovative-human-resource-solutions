<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App;

class TranslationScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model, $locale = null)
    {

        if (is_null($locale)) {
            $locale = App::getLocale();
        }

        $builder->with(['translations' => function ($builder) use ($locale) {
            $builder->where('locale', $locale);
        }]);

        return $builder;
    }
}
