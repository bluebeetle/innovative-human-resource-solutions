<?php

namespace App;

use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class CasestudySector extends Model
{
    use TranslatableTrait;

    public $translationForeignKey = 'sector_id';

    protected $fillable = ['name', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title'];

    public function casestudies()
    {
        return $this->hasMany(Casestudy::class, 'sector_id');
    }
}
