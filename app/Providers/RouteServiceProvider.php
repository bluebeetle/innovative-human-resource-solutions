<?php

namespace App\Providers;

use Illuminate\Support\Str;
use App;
use App\Page;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    }
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        //
    
        $localPrefix = '';
        $request = app('request');
        if (in_array($request->segment(1), config('translatable.locales'))) {
            App::setLocale($request->segment(1));
            $localPrefix = $request->segment(1);
        }
    
        Route::group(['middleware' => 'web', 'prefix' => $localPrefix], function ($router) {
        
            $pages = Page::active()->where('alias', '0')->where(function ($q) {
            
                $q->whereNotIn('parent_id', function ($q) {
                
                    $q->select(['id'])
                        ->from('pages')
                        ->where('status', '1')
                        ->where('template', 'tabsPage');
                });
            
                $q->orWhere('parent_id', null);
            })->get();
        
            foreach ($pages as $page) {
                $router->get($page->uri, ['as' => Str::camel($page->name), function () use ($page, $router) {
                    return $this->app->call(
                        'App\Http\Controllers\PageController@show',
                        [
                            'page' => $page,
                            'parameters' => $router->current()->parameters()
                        ]
                    );
                }]);
            }
        });
    }
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }
    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}
