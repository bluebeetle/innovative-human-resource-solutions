<?php

namespace App\Providers;

//use DB;
use App\ViewComposers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Validator;
use GuzzleHttp\Client as GuzzleHttpClient;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    
        Paginator::useBootstrapThree();
        
        $this->app['view']->composer('layouts.default', ViewComposers\InjectPages::class);
        $this->app['view']->composer('layouts.home', ViewComposers\InjectPages::class);
        $this->app['view']->composer('inc.newsletterHomeWidget', ViewComposers\InjectNewsletter::class);
        $this->app['view']->composer('inc.newsletterWidget', ViewComposers\InjectNewsletter::class);
        $this->app['view']->composer('inc.locationsHomeWidget', ViewComposers\InjectLocations::class);
        $this->app['view']->composer('inc.locationsWidget', ViewComposers\InjectLocations::class);
        $this->app['view']->composer('inc.socialBarWidget', ViewComposers\InjectSocialBarData::class);
    
    
    
        Validator::extend('validateGRecaptcha', function ($attribute, $value, $parameters, $validator) {
            $secret = env('RECAPTCHA_SECRET_KEY');
            $httpClient = new GuzzleHttpClient([
                'base_uri' => 'https://www.google.com/recaptcha/api/',
                'timeout' => 10.0,
                'allow_redirects' => false
            ]);
            $result = $httpClient->request('GET', 'siteverify?secret=' .$secret . '&response=' . $value);
            $resultJson = json_decode($result->getBody());
            return $resultJson->success;
        });
        

        /*DB::listen(function ($query) {
             echo $query->sql;

             print_r($query->bindings);
            echo "\n". "<br/>";
             //echo $query->time;
        });*/
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
