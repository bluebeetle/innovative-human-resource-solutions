<?php

namespace App;

use App\Traits\CommonModelTrait;
use App\Traits\MediableTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Casestudy extends Model
{
    use TranslatableTrait;
    use MediableTrait;
    use CommonModelTrait;

    protected $fillable = ['name', 'slug', 'category_id', 'sector_id', 'status', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title', 'content', 'meta_title', 'meta_keywords', 'meta_description'];

    public function category()
    {
        return $this->belongsTo(\App\CasestudyCategory::class);
    }

    public function sector()
    {
        return $this->belongsTo(\App\CasestudySector::class);
    }
}
