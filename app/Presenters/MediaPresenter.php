<?php

namespace App\Presenters;

use File;
use Image;

class MediaPresenter extends Presenter
{

    protected $image = null;

    public function __construct($object)
    {
        parent::__construct($object);
    }


    public function getImage()
    {

        if ($this->type == 'image') {
            if (!$this->image) {
                $this->image = Image::make($this->path);
            }
        }

        return $this->image;
    }

    public function width()
    {

        $image = $this->getImage();

        return $image->width();
    }

    public function height()
    {

        $image = $this->getImage();

        return $image->height();
    }

    public function file_size()
    {

        return File::size($this->path);
    }

    public function file_size_KB()
    {

        $size = $this->file_size();

        return round($size/1024, 1);
    }
}
