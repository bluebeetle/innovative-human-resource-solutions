<?php

namespace App\Presenters;

class PagePresenter extends Presenter
{
    
    public function __construct($object)
    {
        parent::__construct($object);
    }


    public function uri_wild_card()
    {
        return $this->uri.'*';
    }

    public function pretty_uri()
    {
        $scheme = parse_url($this->uri, PHP_URL_SCHEME);
        $uri = '';
        if (empty($scheme)) {
            $uri = '/';
        }

        return $uri = $uri .ltrim($this->uri, '/');
    }

    public function link_to_padded_title($link)
    {
        $padding = str_repeat('&nbsp;', $this->depth * 4);

        $anchor = $padding . '<a href=' . $link .'>' . $this->title . '</a>';

        return $anchor;
    }

    public function link_to_padded_name($link)
    {
        $padding = str_repeat('&nbsp;', $this->depth * 4);

        $anchor = $padding . '<a href=' . $link .'>' . $this->name . '</a>';

        return $anchor;
    }

    public function padded_title()
    {
        return str_repeat('&nbsp;', $this->depth * 4).$this->title;
    }

    public function padded_name()
    {
        return str_repeat('&nbsp;', $this->depth * 4).$this->name;
    }
}
