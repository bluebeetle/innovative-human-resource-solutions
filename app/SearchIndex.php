<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchIndex extends Model
{
    protected $table = 'search_index';

    public $timestamps = false;

    protected $fillable = ['locale', 'title', 'content',  'type', 'type_id', 'url'];
}
