<?php

namespace App;

use File;
use Image;
use Baum\Node;
use App\Traits\OrderableTrait;
use App\Traits\CommonModelTrait;
use Illuminate\Http\UploadedFile;
use App\Traits\TranslatableTrait;

class LogoSlider extends Node
{
    use OrderableTrait;
    use CommonModelTrait;
    use TranslatableTrait;

    protected $fillable = ['name', 'logo', 'status', 'created_by', 'updated_by'];

    public $translatedAttributes = ['link'];

    public $imageColumnName = 'logo';

    public function saveLogo(UploadedFile $logoFile)
    {
        $name = sha1($this->id) . '.' . $logoFile->getClientOriginalExtension();

        $this->deleteThumbs();

        Image::make($logoFile)
            ->resize(config('cms.uploads.maxImageWidth'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->interlace()
            ->save(config('cms.uploads.paths.images') . '/logoSliders/' . $name, config('cms.uploads.imageQuality'));

        $this->fill(['logo'=> config('cms.uploads.paths.images') . '/logoSliders/' . $name,
        'logo_tinified' => 0]);

        if ($this->isDirty()) {
            $this->save();
        }
    }

    public function deleteThumbs($path = '')
    {

        if (empty($path)) {
            $path = $this->logo;
        }

        if (!empty($path)) {
            $pathChunks = explode("/", $path);
            $imageName = array_pop($pathChunks);

            $files = glob(implode("/", $pathChunks) . "/tn-*" . $imageName);
            foreach ($files as $file) {
                if (File::isFile($file)) {
                    File::delete($file);
                }
            }
        }
    }
}
