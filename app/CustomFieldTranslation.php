<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomFieldTranslation extends Model
{

    protected $fillable = ['locale', 'value'];

    public $timestamps = false;
}
