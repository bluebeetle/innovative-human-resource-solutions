<?php

namespace App;

use App;
use App\Presenters\Traits\PresentableTrait;
use Baum\Node;
use App\Traits\MediableTrait;
use App\Traits\OrderableTrait;
use App\Traits\TranslatableTrait;
use App\Traits\CustomFieldsTrait;
use App\Traits\CommonModelTrait;

class Page extends Node
{

    use TranslatableTrait;
    use MediableTrait;
    use OrderableTrait;
    use CustomFieldsTrait;
    use CommonModelTrait;
    use PresentableTrait;
    
    protected $presenter = \App\Presenters\PagePresenter::class;

    protected $fillable = ['name', 'uri',  'template', 'navigation', 'hidden', 'not_searchable', 'alias', 'status', 'lft', 'rgt', 'depth', 'parent_id', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title', 'content', 'meta_title','meta_keywords', 'meta_description'];

    public function courses()
    {
        return $this->belongsToMany(\App\Course::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value ?: null;
    }

    public function setTemplateAttribute($value)
    {
        $this->attributes['template'] = $value ?: null;
    }

    public function setHiddenAttribute($value)
    {
        $this->attributes['hidden'] = $value ?: 0;
    }

    public function setNotSearchableAttribute($value)
    {
        $this->attributes['not_searchable'] = $value ?: 0;
    }

    public function setAliasAttribute($value)
    {
        $this->attributes['alias'] = $value ?: 0;
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value ?: 0;
    }

    public function scopePublished($query)
    {

        return $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeVisible($query)
    {

        return $query->where('hidden', '0');
    }


    /*public function updateOrder($order, $orderPage)
    {
        $orderPage = $this->findOrFail($orderPage);

        if ($order == 'before') {
            $this->moveToLeftOf($orderPage);
        } elseif ($order == 'after') {
            $this->moveToRightOf($orderPage);
        } elseif ($order == 'childOf') {
            $this->makeChildOf($orderPage);
        }
    }*/

    /*public function scopeWithTranslation($query, $locale = null)
    {
        if(is_null($locale))
        {
            $locale = App::getLocale();
        }

        $query->with(['translations' => function($query) use ($locale) {
            $query->where('locale', $locale);
        }]);

        return $query;
    }*/
}
