<?php
namespace App\Overrides\Torann\LaravelMetaTags;

use Torann\LaravelMetaTags\MetaTag as LaravelMetaTag;
use Illuminate\Http\Request;

class MetaTag extends LaravelMetaTag
{

    /**
     * Instance of request
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var array
     */
    private $config = [];

    /**
     * Locale default for app.
     *
     * @var string
     */
    private $defaultLocale = '';

    /**
     * @var array
     */
    private $metas = [];

    /**
     * @var string
     */
    private $title;

    /**
     * OpenGraph elements
     *
     * @var array
     */
    private $og = [
        'title', 'description', 'type', 'image', 'url', 'audio',
        'determiner', 'locale', 'site_name', 'video'
    ];

    /**
     * Twitter card elements
     *
     * @var array
     */
    private $twitter = [
        'card', 'site', 'title', 'description',
        'creator', 'image:src', 'domain'
    ];

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  array $config
     * @param  string $defaultLocale
     */
    public function __construct(Request $request, array $config = [], $defaultLocale)
    {
        $this->request = $request;
        $this->config = $config;

        // Set defaults
        $this->set('title', $this->config['title']);
        $this->set('url', $this->request->url());

        // Set default locale
        $this->defaultLocale = $defaultLocale;

        // Is locales a callback
        if (is_callable($this->config['locales'])) {
            $this->setLocales(call_user_func($this->config['locales']));
        }

        parent::__construct($request, $config, $defaultLocale);
    }

    /**
     * Create meta tag from attributes
     *
     * @param  array $values
     * @return string
     */
    private function createTag(array $values, $type = 'meta')
    {
        $attributes = array_map(function ($key) use ($values) {
            $value = $this->fix($values[$key]);
            return "{$key}=\"{$value}\"";
        }, array_keys($values));

        $attributes = implode(' ', $attributes);

        return "<{$type} {$attributes}>\n    ";
    }

    /**
     * @param  string $text
     * @return string
     */
    private function fix($text)
    {
        $text = preg_replace('/<[^>]+>/', ' ', $text);
        $text = preg_replace('/[\r\n\s]+/', ' ', $text);

        return trim(str_replace('"', '&quot;', $text));
    }

    /**
     * Create canonical tags
     *
     * @return string
     */
    public function canonical()
    {
        $html = $this->createTag([
            'rel' => 'canonical',
            'href' => $this->request->url()
        ], 'link');

        foreach ($this->config['locales'] as $value) {
            // Turn current URL into a localized URL
            // using the given lang code
            $url = $this->localizedURL($value);

            $html .= $this->createTag([
                'rel' => 'alternate',
                'hreflang' => $value,
                'href' => $url
            ], 'link');
        }

        return $html;
    }

    /**
     * Returns an URL adapted to locale
     *
     * @param  string $locale
     * @return string
     */
    private function localizedURL($locale)
    {
        // Default language doesn't get a special subdomain
        $locale = ($locale !== config('app.fallback_locale')) ? '/' . strtolower($locale) : '';


        // URL elements
        $uri = $this->request->getRequestUri();
        $uri = preg_replace("#^\/" . $this->defaultLocale . "(\/*)#", "/", $uri);


        $scheme = $this->request->getScheme();

        // Get host
        //$array = explode('.', $this->request->getHttpHost());
        //$host = (array_key_exists(count($array) - 2, $array) ? $array[count($array) - 2] : '').'.'.$array[count($array) - 1];
        $host = 'middleeast.psionline.com';

        // Create URL from template
        $url = str_replace(
            ['[scheme]', '[locale]', '[host]', '[uri]'],
            [$scheme, $locale, $host, $uri],
            $this->config['locale_url']
        );

        return url($url);
    }
}
