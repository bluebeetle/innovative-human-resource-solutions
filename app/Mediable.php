<?php

namespace App;

use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Mediable extends Model
{
    use TranslatableTrait;

    public $timestamps = false;

    public $translatedAttributes = ['caption'];
}
