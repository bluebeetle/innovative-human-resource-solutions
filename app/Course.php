<?php

namespace App;

use File;
use Image;
use Baum\Node;
use Carbon\Carbon;
use App\Traits\MediableTrait;
use App\Traits\OrderableTrait;
use App\Traits\CommonModelTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;

class Course extends Node
{
    use TranslatableTrait;
    use MediableTrait;
    use CommonModelTrait;
    use OrderableTrait;

    protected $fillable = ['name', 'slug', 'logo', 'cost', 'link', 'status', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title', 'content', 'meta_title', 'meta_keywords', 'meta_description'];

    public $imageColumnName = 'logo';

    public function dates()
    {
        return $this->hasMany(\App\CourseDate::class);
    }

    public function pages()
    {
        return $this->belongsToMany(\App\Page::class);
    }

    public function saveLogo(UploadedFile $logoFile)
    {

        $name = sha1($this->id) . '.' . $logoFile->getClientOriginalExtension();

        $this->deleteThumbs();

        Image::make($logoFile)
            ->resize(config('cms.uploads.maxImageWidth'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->interlace()
            ->save(config('cms.uploads.paths.images') . '/courses/' . $name, config('cms.uploads.imageQuality'));

        $this->fill(['logo'=> config('cms.uploads.paths.images') . '/courses/' . $name,
        'logo_tinified' => 0])
            ->save();
    }

    public function deleteThumbs($path = '')
    {

        if (empty($path)) {
            $path = $this->logo;
        }

        if (!empty($path)) {
            $pathChunks = explode("/", $path);
            $imageName = array_pop($pathChunks);

            $files = glob(implode("/", $pathChunks) . "/tn-*" . $imageName);
            foreach ($files as $file) {
                if (File::isFile($file)) {
                    File::delete($file);
                }
            }
        }
    }
}
