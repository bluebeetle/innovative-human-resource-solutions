<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableTrait;

class EventDate extends Model
{
    use TranslatableTrait;

    protected $fillable = ['event_id', 'start_date', 'end_date'];

    protected $dates = ['start_date', 'end_date'];

    public $translatedAttributes = ['location'];

    public $timestamps = false;



    public function event()
    {
        return $this->belongsTo(\App\Event::class);
    }

    /*public function setStartDateAttribute($value){

        $this->attributes['start_date'] = Carbon::parse($value);
    }

    public function setEndDateAttribute($value){

        $this->attributes['end_date'] = Carbon::parse($value);
    }*/
}
