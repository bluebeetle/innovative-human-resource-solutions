<?php

namespace App;

use App\Traits\CustomFieldsTrait;
use Carbon\Carbon;
use App\Traits\MediableTrait;
use App\Traits\CommonModelTrait;
use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use TranslatableTrait;
    use MediableTrait;
    use CommonModelTrait;
    use CustomFieldsTrait;

    protected $fillable = ['name', 'slug', 'published_at', 'author_id', 'created_by', 'updated_by', 'status'];

    public $translatedAttributes = ['title', 'banner_text', 'content', 'meta_title', 'meta_keywords', 'meta_description'];

    protected $dates = ['published_at'];

    public function categories()
    {
        return $this->belongsToMany(BlogCategory::class, 'category_post', 'post_id', 'category_id');
    }

    public function author()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value ?: 0;
    }

    /**
     * This function is required despite the published_date in the $dates array
     * because the datetime format that we use is 2016-01-01 12:00 AM and
     * it cannot be saved until parsed by carbon.
     */
    public function setPublishedAtAttribute($value)
    {

        $this->attributes['published_at'] = Carbon::parse($value);
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now());
    }
}
