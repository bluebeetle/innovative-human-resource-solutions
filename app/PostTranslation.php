<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    protected $fillable = ['locale', 'title', 'content', 'meta_title', 'meta_keywords', 'meta_description'];

    public $timestamps = false;
}
