<?php
namespace App\ViewComposers;

use App\Page;
use Illuminate\View\View;

class InjectPages
{
    protected $pages;

    public function __construct(Page $pages)
    {
        $this->pages = $pages;
    }

    public function compose(View $view)
    {

        $pages = $this->pages->active()->where('hidden', '0')->orderBy('lft', 'asc')->get();
        $view->with('pages', $pages);
    }
}
