<?php
namespace App\ViewComposers;

use App\Location;
use Illuminate\View\View;

class InjectLocations
{

    public function compose(View $view)
    {

        $locations = Location::active()->orderBy('lft', 'asc')->get();
        $view->with('locations', $locations);
    }
}
