<?php
namespace App\ViewComposers;

use App\Page;
use Illuminate\View\View;

class InjectNewsletter
{

    public function compose(View $view)
    {

        $newsletterPage = Page::whereUri('homepage-newsletter')->first();
        $view->with('newsletter', $newsletterPage);
    }
}
