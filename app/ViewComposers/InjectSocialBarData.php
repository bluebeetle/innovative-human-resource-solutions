<?php
namespace App\ViewComposers;

use Cache;
use App\Page;
use Illuminate\View\View;
use Setting;
use Twitter;

class InjectSocialBarData
{

    public function compose(View $view)
    {

        $twitterUsername = Setting::get('twitterUsername');
        $latestTweet = '';//$this->fetchLatestTweet();
        $view->with('latestTweet', $latestTweet);
        $view->with('twitterUsername', $twitterUsername);
    }

    protected function fetchLatestTweet()
    {

        return Cache::remember('latestTweet', 216000, function () {
            $twitterUsername = Setting::get('twitterUsername');
            $tweet = Twitter::getUserTimeline(['screen_name' => $twitterUsername, 'count' => 1]);

            if (isset($tweet[0])) {
                return Twitter::linkify($tweet[0]);
            }

            return '';
        });
    }
}
