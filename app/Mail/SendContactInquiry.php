<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContactInquiry extends Mailable
{
    use Queueable, SerializesModels;

    
    public $data = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        if (is_array($data)) {
            $this->data = $data;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->from('info@innovative-hr.com')
            ->replyTo($this->data['replyTo'][0], @$this->data['replyTo'][1])
            ->subject($this->data['subject'])
        ->view('emails.contact');
    }
}
