<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableTrait;

class CourseDate extends Model
{

    use TranslatableTrait;

    protected $fillable = ['course_id', 'start_date', 'end_date'];

    public $translatedAttributes = ['location'];

    protected $dates = ['start_date', 'end_date'];

    public $timestamps = false;



    public function course()
    {
        return $this->belongsTo(\App\Course::class);
    }
}
