<?php

/**
 * Create image thumbnail if doesn't exist already & return its path
 *
 * @param $imagePath
 * @param null $width
 * @param null $height
 * @param null $cropPosition
 * @return string
 */
function imageThumb($imagePath, $width = null, $height = null, $cropPosition = null)
{
    if (!file_exists($imagePath)) {
        return false;
    }

    $needCrop = false;

    $imageManager = app('Intervention\Image\ImageManager');

    $image = $imageManager->make($imagePath);
    $originalWidth = $image->width();
    $originalHeight = $image->height();
    $originalRatio = round($originalWidth / $originalHeight, 2);


    if (!is_null($width) && is_null($height)) {
        $height = round($width / $originalRatio);
    }

    if (!is_null($height) && is_null($width)) {
        $width = round($height * $originalRatio);
    }

    $ratio = round(($width / $height), 2);

    if ($ratio != $originalRatio) {
        $needCrop = true;
    }


    $pathChunks = explode("/", $imagePath);
    $imageName = array_pop($pathChunks);
    $thumbName = 'tn-' . $width . '-' . ($needCrop?$height . '-':'') . (($needCrop && !is_null($cropPosition)) ? $cropPosition . '-':'') . $imageName;
    $thumbPath = implode("/", $pathChunks) . '/' . $thumbName;

    if (!file_exists($thumbPath)) {
        $imageManager = app('Intervention\Image\ImageManager');

        $image->fit($width, $height, function ($constraint) {
                //$constraint->aspectRatio();
                $constraint->upsize();
        }, $cropPosition)->save($thumbPath);
    }


    return cdnAsset('/' . $thumbPath);
}

/**
 * Delete thumbs of the given image path
 *
 * @param string $path
 */
function deleteThumbs($path)
{

    if (!empty($path)) {
        $pathChunks = explode("/", $path);
        $imageName = array_pop($pathChunks);

        $files = glob(implode("/", $pathChunks) . "/tn-*" . $imageName);
        foreach ($files as $file) {
            if (File::isFile($file)) {
                File::delete($file);
            }
        }
    }
}


function tinifyImage($path)
{

    try {
        $comporessedImage = Tinify::fromFile($path);
        $comporessedImage->toFile($path);

        $msg = 'tinified';
    } catch (Tinify\AccountException $e) {
        $msg = 'apiError';
    } catch (Exception $e) {
        $msg = 'unknownError';
    }

    return $msg;
}

/**
 * Get singular model name
 *
 * @param $model
 * @return mixed
 */
function getSingularModelName($model)
{
    $str = app('Illuminate\Support\Str');
    return $str->ucfirst($str->singular(class_basename($model)));
}

/**
 * Get plural model name
 *
 * @param $model
 * @return mixed
 */
function getPluralModelName($model)
{

    $str = app('Illuminate\Support\Str');
    return $str->ucfirst($str->plural(class_basename($model)));
}

/**
 * Get the redirect when media is attached to mediabel model
 *
 * @param $config
 * @param $args
 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
 */
function getMediableRedirect($config, $args)
{

    if (isset($config['redirectUrl'])) {
        if (isset($config['redirectParams']) && count($config['redirectParams'])) {
            foreach ($config['redirectParams'] as $param) {
                $config['redirectUrl'] = str_replace("{" . $param . "}", $args[$param], $config['redirectUrl']);
            }
        }

        return redirect($config['redirectUrl']);
    }

    // If redirectRoute or redirectAction is provided

    if (isset($config['redirectRoute']) || isset($config['redirectAction'])) {
        if (isset($config['redirectParams']) && count($config['redirectParams'])) {
            foreach ($config['redirectParams'] as $key => $param) {
                $config['redirectParams'][$key] = $args[$param];
            }
        }


        if (isset($config['redirectRoute'])) {
            return redirect()->route($config['redirectRoute'], $config['redirectParams']);
        } else //isset($config['redirectAction'])
        {
            return redirect()->action($config['redirectAction'], $config['redirectParams']);
        }
    }

    // Default redirect if no redirect option is provided
    return redirect()->route('cms.mediable.show', [$args['mediableType'], $args['mediableId']]);
}

/**
 * Get Flash object or set flash message
 *
 * @param null $message
 * @param null $title
 * @return mixed
 */
function flash($message = null, $title = null)
{
    $flash = app(\App\Utilities\Flash::class);

    if (func_num_args() == 0) {
        return $flash;
    }

    return $flash->info($message, $title);
}

/**
 * Convert array to object
 *
 * @param $array
 * @return stdClass
 */
function castToObj($array)
{

    $obj = new stdClass;
    foreach ($array as $k => $v) {
        if (strlen($k)) {
            if (is_array($v)) {
                $obj->{$k} = castToObj($v);
            } else {
                $obj->{$k} = $v;
            }
        }
    }

    return $obj;
}


/**
 * Active if the current Request URI matches the provided URI
 *
 * @param $uri
 * @param null $prefix
 * @param string $wildCard
 * @param string $activeClass
 * @return string
 */
function activeIf($uri, $prefix = null, $wildCard = "*", $activeClass = 'active')
{

    $fullUri = '';
    if (!is_null($prefix) && !empty($prefix)) {
        $fullUri = $prefix . '/';
    }
    $fullUri .= $uri;

    if (!is_null($wildCard)) {
        $fullUri .= $wildCard;
    }

    return Request::is($fullUri)?$activeClass:'';
}

/**
 *
 *
 * @param $startDate
 * @param $endDate
 * @param string $separator
 * @param string $yearFormat
 * @param string $monthFormat
 * @param string $dayFormat
 * @return string
 */
function presentOccasionDates($startDate, $endDate, $separator = '-', $yearFormat = 'Y', $monthFormat = 'M', $dayFormat = 'j')
{

    if ($endDate->eq($startDate)) {
        return $endDate->format('j') . ' ' .trans('front.' . $startDate->format('M')) . $endDate->format(($yearFormat?' '.$yearFormat:''));
    }

    if (abs($endDate->format('n') - $startDate->format('n'))) {
        $dates = $startDate->format('j').' '.trans('front.' . $startDate->format('M'));
    } else {
        $dates = $startDate->format('j');
    }
    $dates .= $separator;
    $dates .= $endDate->format('j') . ' ' . trans('front.' . $endDate->format('M')) . ' ' . $endDate->format(($yearFormat?' '.$yearFormat:''));

    return $dates;
}


function addReferrerToCourseLink($url, $pageId)
{

    $urlComponents = parse_url($url);

    $newQuery = http_build_query(['referrer' => $pageId]);

    $currentUrlComponents = parse_url(url()->current());

    if ($urlComponents['host'] == $currentUrlComponents['host'] &&
        preg_match("@calendar/course/\d*/.*@", $url)) {
        if (!empty($urlComponents['query'])) {
            $url .= '&'.$newQuery;
        } else {
            $url .= '?'.$newQuery;
        }
    }

    return $url;
}

function prefixUrl($path = null)
{
    
    $locale = App::getLocale();
    $localePrefix = ($locale == config('app.fallback_locale') ? '' : $locale);

    if (is_null($path)) {
        return $localePrefix;
    }
    else if(preg_match('/^http(s)?:\/\//', $path))
    {
        return $path;
    }

    $path = $localePrefix . '/' . $path;
    $path = preg_replace('#/+#', '/', $path);

    return $path;
}

function rglob($pattern, $flags = 0)
{
    $files = glob($pattern, $flags);

    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
    }

    return $files;
}


function cdnAsset($path)
{
    $path = preg_replace('/^\//', '', $path);

    $useCdn = config('cms.useCdn');
    $cdn = config('cms.cdn');
    if ($useCdn) {
        $cdn = preg_replace('/\/$/', '', $cdn);
        return $cdn . '/' . $path;
    }

    return '/' . $path;
}
