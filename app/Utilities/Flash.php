<?php

namespace App\Utilities;

class Flash
{

    /**
     * Create a flash message.
     *
     * @param string $title
     * @param string $message
     * @param string $level
     * @param string $key
     *
     * @return mixed
     */
    public function create($message, $title = null, $level = 'info', $key = 'flash_message')
    {
        return session()->flash($key, [
            'title'   => $title,
            'message' => $message,
            'level'   => $level
        ]);
    }


    /**
     * Create an info flash message.
     *
     * @param string $title
     * @param string $message
     *
     * @return mixed
     */
    public function info($message, $title = null)
    {
        return $this->create($message, $title);
    }


    /**
     * Create a success flash message.
     *
     * @param string $title
     * @param string $message
     *
     * @return mixed
     */
    public function success($message, $title = null)
    {
        return $this->create($message, $title, 'success');
    }


    /**
     * Create an error flash message.
     *
     * @param string $title
     * @param string $message
     *
     * @return mixed
     */
    public function error($message, $title = null)
    {
        return $this->create($message, $title, 'danger');
    }


    /**
     * Create a warning flash message.
     *
     * @param string $title
     * @param string $message
     *
     * @return mixed
     */
    public function warning($message, $title = null)
    {
        return $this->create($message, $title, 'warning');
    }
}
