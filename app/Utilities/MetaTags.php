<?php
namespace App\Utilities;

use MetaTag;

class MetaTags
{
    public static function setAll($args = [])
    {

        $title = $args['title']?:'';
        $keywords = $args['keywords']?:'';
        $description = $args['description']?:'';
        $image = $args['image']?:null;

        $metatitle = '';
        if (trim($title)!='') {
            $metatitle .= $title.' | ';
        }
        $metatitle .= trans('front.PSI Middle East | Middle East');

        MetaTag::set('title', $metatitle);
        MetaTag::set('keywords', $keywords);
        MetaTag::set('description', $description);


        if ($image) {
            MetaTag::set('image', url($image));
        }
    }
}
