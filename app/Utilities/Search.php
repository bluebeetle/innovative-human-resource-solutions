<?php
namespace App\Utilities;

use App;
use App\SearchIndex;
use DB;

class Search
{

    public static function addOrUpdateItem($type, $id, $title, $content, $url)
    {


        $records = SearchIndex::where([
            ['type' , $type],
            ['type_id', $id]
        ])->get();

        if ($records->count()) {
            $record = $records->first();

            $record->fill([
                'locale' => App::getLocale(),
                'type' => $type,
                'type_id' => $id,
                'title' => $title,
                'content' => strip_tags($content),
                'url' => $url
            ])->save();
        } else {
            $record = SearchIndex::create([
                'locale' => App::getLocale(),
                'type' => $type,
                'type_id' => $id,
                'title' => $title,
                'content' => strip_tags($content),
                'url' => $url
            ]);
        }
    }

    public static function deleteItem($type, $id)
    {

        SearchIndex::where([
            ['type' , $type],
            ['type_id', $id]
        ])->delete();
    }

    public static function search($keyword, $limit = 0)
    {

        /*$query = "SELECT *, MATCH (title) AGAINST ('$keyword' IN NATURAL LANGUAGE MODE) * 5 +
         MATCH (content) AGAINST ('$keyword' IN NATURAL LANGUAGE MODE) * 1 as score
        FROM (search_index)
        WHERE
         MATCH (title) AGAINST ('$keyword' IN NATURAL LANGUAGE MODE) * 5 +
         MATCH (content) AGAINST ('$keyword' IN NATURAL LANGUAGE MODE) * 1
        ORDER BY `score` DESC";*/

        $locale = App::getLocale();

        $query = "SELECT *, CASE WHEN title REGEXP '$keyword' THEN 1 ELSE 0 END AS keyword_in_title, MATCH (title) AGAINST ('$keyword' IN NATURAL LANGUAGE MODE) * 5 +
         MATCH (content) AGAINST ('$keyword' IN NATURAL LANGUAGE MODE) * 1 as score
         FROM (search_index)
         WHERE
         locale='" . $locale . "' AND
         (title REGEXP '$keyword' OR content REGEXP '$keyword') ORDER BY keyword_in_title DESC, score DESC";

        if ($limit > 0) {
            $query .= ' LIMIT '. $limit;
        }


        $records = DB::select($query);

        return $records;
    }
}
