<?php

namespace App;

use File;
use Image;
use Carbon\Carbon;
use App\Traits\MediableTrait;
use Illuminate\Http\UploadedFile;
use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\CommonModelTrait;

class Event extends Model
{
    use TranslatableTrait;
    use MediableTrait;
    use CommonModelTrait;

    protected $fillable = ['name', 'slug','logo', 'recursive', 'status', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title', 'content', 'meta_title', 'meta_keywords', 'meta_description'];

    public $imageColumnName = 'logo';

    public function dates()
    {
        return $this->hasMany(\App\EventDate::class);
    }

    public function setRecursiveAttribute($value)
    {
        $this->attributes['recursive'] = $value ?: 0;
    }

    public function saveLogo(UploadedFile $logoFile)
    {
        $name = sha1($this->id) . '.' . $logoFile->getClientOriginalExtension();

        $this->deleteThumbs();

        Image::make($logoFile)
            ->resize(config('cms.uploads.maxImageWidth'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->interlace()
            ->save(config('cms.uploads.paths.images') . '/events/' . $name, config('cms.uploads.imageQuality'));

        $this->fill(['logo'=> config('cms.uploads.paths.images') . '/events/' . $name,
            'logo_tinified' => 0])
            ->save();
    }

    public function deleteThumbs($path = '')
    {

        if (empty($path)) {
            $path = $this->logo;
        }

        if (!empty($path)) {
            $pathChunks = explode("/", $path);
            $imageName = array_pop($pathChunks);

            $files = glob(implode("/", $pathChunks) . "/tn-*" . $imageName);
            foreach ($files as $file) {
                if (File::isFile($file)) {
                    File::delete($file);
                }
            }
        }
    }
}
