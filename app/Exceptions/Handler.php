<?php
namespace App\Exceptions;

use App;
use App\Page;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
    
        $locale = App::getLocale();
        $localePrefix = ($locale == config('app.fallback_locale') ? '' : $locale);
    
        if ($exception instanceof NotFoundHttpException
            || $exception instanceof ModelNotFoundException
            || $exception instanceof FileNotFoundException) {
            $page = Page::whereUri('not-found')->first();
            $media = $page->mediaArray;
        
            return response()->view('errors.404', compact('page', 'media', 'locale', 'localePrefix'), 404);
        } else if ($exception instanceof HttpException) {
            $page = Page::whereUri('general-error')->first();
            $media = $page->mediaArray;
            return response()->view('errors.500', compact('page', 'media', 'locale', 'localePrefix'), 500);
        }
        
        return parent::render($request, $exception);
    }
}
