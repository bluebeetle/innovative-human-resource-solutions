<?php

namespace App;

use App\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    use TranslatableTrait;

    public $translationForeignKey = 'category_id';

    protected $fillable = ['name', 'slug', 'created_by', 'updated_by'];

    public $translatedAttributes = ['title'];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'category_post', 'category_id', 'post_id');
    }
}
