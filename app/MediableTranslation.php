<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediableTranslation extends Model
{
    protected $fillable = ['locale', 'caption'];

    public $timestamps = false;
}
