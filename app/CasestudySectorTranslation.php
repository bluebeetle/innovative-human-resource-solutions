<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasestudySectorTranslation extends Model
{
    protected $fillable = ['locale', 'title'];

    public $timestamps = false;
}
