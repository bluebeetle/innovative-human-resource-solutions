process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');

require('laravel-elixir-postcss');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(function (mix) {
    mix.copy('node_modules/bootstrap-sass/assets/fonts', elixir.config.publicPath + '/fonts');
    mix.copy('node_modules/jquery/dist/jquery.min.js', elixir.config.assetsPath + '/js/libs');
    mix.copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', elixir.config.assetsPath + '/js/libs');

    /* START: CMS Tasks */

    mix.sass('cms/styles.scss', 'public/css/cms')
        .sass('cms/editor-content.scss', 'public/css/cms')
        //.copy(elixir.config.assetsPath + '/js/cms/scripts.js', 'public/js/cms')
        .scripts([
                'libs/jquery.min.js',
                'libs/bootstrap.min.js',
                'libs/dropzone.js',
                'libs/isotope.pkgd.min.js',
                'libs/imagesloaded.pkgd.min.js',
                'libs/jquery-confirm.min.js',
                'libs/moment.js',
                'libs/bootstrap-datetimepicker.min.js',
                'libs/jquery.dataTables.min.js',
                'libs/dataTables.bootstrap.min.js',
                'libs/jquery-ui.js',

                'cms/scripts.js',
            ],
            'public/js/cms/scripts.js'
        );

    /* END: CMS Tasks */



    /* START: Frontend Tasks */

    mix.sass('styles.scss')
        .sass('styles-ar.scss')
        .copy(elixir.config.assetsPath + '/js/libs/modernizr-2.8.3-respond-1.4.2.min.js', elixir.config.publicPath + '/js')
        .scripts([
            'libs/jquery.min.js',
            'libs/slick.js',
            'libs/packery.pkgd.min.js',
            'libs/imagesloaded.pkgd.min.js',
            'libs/gmap3.js'
            ],
            'public/js/plugins.js')
        .browserify('scripts.js', null, 'resources/assets/js');

    mix.sass('style.scss')
        .scripts([
                'libs/jquery.min.js',
                'libs/slick.js',
                'libs/bootstrap.min.js',
                'libs/iziModal.js',
                'libs/lazyload.min.js',
            ],
            'public/js/home.js')
        .browserify('script.js', null, null);

    /* END: Frontend Tasks */



        /*.styles([
            'libs/medium-editor.css',
            'libs/medium-editor.default.theme.css',
        ])
        .scripts([
            'libs/medium-editor.js'
        ])*/
});

