<?php

/**
 * Utilities
 */
Route::get('/img/thumb', 'UtilitiesController@imageThumb');
Route::get('/downloadFile', 'UtilitiesController@downloadFile');

/*Route::get('/phpinfo', function() {
    return phpinfo();
});*/

Route::get('/clearCache', 'PageController@clearCache')->name('clearCache');



/* START: Sitemap */

Route::get('/sitemap.xml', 'SitemapController@index');
/*Route::get('/sitemap/pages.xml', 'SitemapController@pages');
Route::get('/sitemap/events.xml', 'SitemapController@events');
Route::get('/sitemap/blog.xml', 'SitemapController@blog');*/

/* END: Sitemap */


/*
 *  Set up locale if other language is selected
 */

$localPrefix = '';
if (in_array(Request::segment(1), config('translatable.locales'))) {
    App::setLocale(Request::segment(1));
    \Carbon\Carbon::setLocale(Request::segment(1));
    setlocale(LC_TIME, Request::segment(1));
    $localPrefix = Request::segment(1);
}


Route::group(['prefix' => $localPrefix], function () {

    Route::post('/book/course/{id}/{slug}', ['as'=>'bookCourse', 'uses' => 'BookingController@bookCourse']);
    Route::post('/book/event/{id}/{slug}', ['as'=>'bookEvent', 'uses' => 'BookingController@bookEvent']);
    Route::post('/contact', ['as'=>'submitContact', 'uses' => 'ContactController@submitContact']);
    Route::post('/your-career-guidance-hero', ['as'=>'submitContactForm', 'uses' => 'ContactController@submitContactForm']);
    Route::post('/subscribeNewsletter', ['as'=>'subscribeNewsletter', 'uses' => 'NewsletterController@subscribe']);
    Route::get('/search', ['as'=>'search', 'uses' => 'SearchController@search']);
});


/* Auth is used for CMS only. Website has no user accounts*/

Auth::routes();
Route::any('register', function () {
    abort(404);
});
Route::get('logout', ['as' =>'logout', 'uses' => 'Auth\LoginController@logout']);

/* Start : CMS Routes */

Route::get('admin', function () {
    return redirect()->route('cms.login');
});

Route::group(['prefix' => 'cms'], function () {
    
    Route::get('/', function () {
        return redirect()->route('cms.login');
    });
    Route::get('login', ['as' => 'cms.login', 'uses' => 'Auth\LoginController@showLoginForm']);

    Route::group(['namespace' => 'CMS', 'middleware' => 'auth.cms'], function () {


        /**
         * Misc CMS actions
         */
        Route::get('dashboard', ['as'=>'cms.dashboard', 'uses' => 'MainController@dashboard']);
        Route::get('setlocale/{locale}', ['as'=>'cms.setlocale', 'uses' =>  'MainController@setLocale']);


        /**
         * Profile
         */
        Route::get('profile', ['as'=>'cms.profile.edit', 'uses' =>  'ProfileController@edit']);
        Route::post('profile', ['as'=>'cms.profile.update', 'uses' =>  'ProfileController@update']);

        /**
         * Settings
         */
        Route::get('settings', ['as'=>'cms.settings.edit', 'uses' =>  'SettingsController@edit']);
        Route::post('settings', ['as'=>'cms.settings.update', 'uses' =>  'SettingsController@update']);

        /**
         * Users
         */
        Route::resource('users', 'UsersController', ['except' => ['show', 'destroy'], 'as' => 'cms']);
        Route::get('users/{users}/destroy', ['as' => 'cms.users.destroy', 'uses' => 'UsersController@destroy']);

        /**
         * Pages
         */
        //Route::get('pages/{pages}/confirm', ['as' => 'cms.pages.confirm', 'uses' => 'PagesController@confirm']);
        Route::resource('pages', 'PagesController', ['except' => ['show', 'destroy'], 'as' => 'cms']);
        Route::get('pages/{pages}/destroy', ['as' => 'cms.pages.destroy', 'uses' => 'PagesController@destroy']);

        /**
         * Logo Slider
         */
        Route::resource('logoSliders', 'LogoSlidersController', ['except' => ['show', 'destroy'], 'as' => 'cms']);
        Route::get('logoSliders/{logoSliders}/destroy', ['as' => 'cms.logoSliders.destroy', 'uses' => 'LogoSlidersController@destroy']);
        Route::post('logoSliders/sort', ['as' => 'cms.logoSliders.sort', 'uses' => 'LogoSlidersController@sort']);
        Route::get('logoSliders/{logoSliders}/tinify', ['as' => 'cms.logoSliders.tinify', 'uses' => 'LogoSlidersController@tinifyImage']);

        /**
         * Media
         */
        Route::resource('media', 'MediaController', ['as' => 'cms']);
        Route::get('media/{media}/destroy', ['as' => 'cms.media.destroy', 'uses' => 'MediaController@destroy']);
        Route::get('media/{media}/tinify', ['as' => 'cms.media.tinify', 'uses' => 'MediaController@tinifyImage']);

        /**
         * Mediable
         */
        Route::get('{mediableType}/{mediableId}/media', ['as' => 'cms.mediable.show', 'uses' => 'MediableController@show']);
        Route::post('{mediableType}/{mediableId}/update', ['as' => 'cms.mediable.update', 'uses' => 'MediableController@update']);
        Route::get('{mediableType}/{mediableId}/media/select', ['as' => 'cms.mediable.select', 'uses' => 'MediableController@select']);
        Route::post('{mediableType}/{mediableId}/media/attach', ['as' => 'cms.mediable.attach', 'uses' => 'MediableController@attach']);


        /**
         * BLog
         */
        Route::resource('blog', 'BlogController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('blog/{blog}/destroy', ['as' => 'cms.blog.destroy', 'uses' => 'BlogController@destroy']);

        /**
         * Blog Categories
         */
        Route::resource('blogCategories', 'BlogCategoriesController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('blogCategories/{blogCategories}/destroy', ['as' => 'cms.blogCategories.destroy', 'uses' => 'BlogCategoriesController@destroy']);

        /**
         * Events
         */
        Route::resource('events', 'EventsController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('events/{events}/destroy', ['as' => 'cms.events.destroy', 'uses' => 'EventsController@destroy']);
        Route::get('events/{events}/tinify', ['as' => 'cms.events.tinify', 'uses' => 'EventsController@tinifyImage']);

        /**
         * Courses
         */
        Route::resource('courses', 'CoursesController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('courses/{events}/destroy', ['as' => 'cms.courses.destroy', 'uses' => 'CoursesController@destroy']);
        Route::post('courses/sort', ['as' => 'cms.courses.sort', 'uses' => 'CoursesController@sort']);
        Route::get('courses/{courses}/tinify', ['as' => 'cms.courses.tinify', 'uses' => 'CoursesController@tinifyImage']);

        /**
         * Clients
         */
        Route::resource('clients', 'ClientsController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('clients/{clients}/destroy', ['as' => 'cms.clients.destroy', 'uses' => 'ClientsController@destroy']);
        Route::post('clients/sort', ['as' => 'cms.clients.sort', 'uses' => 'ClientsController@sort']);
        Route::get('clients/{clients}/tinify', ['as' => 'cms.clients.tinify', 'uses' => 'ClientsController@tinifyImage']);

        /**
         * Awards
         */
        Route::resource('awards', 'AwardsController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('awards/{awards}/destroy', ['as' => 'cms.awards.destroy', 'uses' => 'AwardsController@destroy']);
        Route::post('awards/sort', ['as' => 'cms.awards.sort', 'uses' => 'AwardsController@sort']);
        Route::get('awards/{awards}/tinify', ['as' => 'cms.awards.tinify', 'uses' => 'AwardsController@tinifyImage']);

        /**
         * Testimonials
         */
        Route::resource('testimonials', 'TestimonialsController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('testimonials/{testimonials}/destroy', ['as' => 'cms.testimonials.destroy', 'uses' => 'TestimonialsController@destroy']);
        Route::post('testimonials/sort', ['as' => 'cms.testimonials.sort', 'uses' => 'TestimonialsController@sort']);
        Route::get('testimonials/{testimonials}/tinify', ['as' => 'cms.testimonials.tinify', 'uses' => 'TestimonialsController@tinifyImage']);

        /**
         * Careers
         */
        Route::resource('careers', 'CareersController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('careers/{careers}/destroy', ['as' => 'cms.careers.destroy', 'uses' => 'CareersController@destroy']);

        /**
         * Locations
         */
        Route::resource('locations', 'LocationsController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('locations/{locations}/destroy', ['as' => 'cms.locations.destroy', 'uses' => 'LocationsController@destroy']);
        Route::post('locations/sort', ['as' => 'cms.locations.sort', 'uses' => 'LocationsController@sort']);
        Route::get('locations/{locations}/tinify', ['as' => 'cms.locations.tinify', 'uses' => 'LocationsController@tinifyImage']);

        /**
         * Casestudies Categories
         */
        Route::resource('casestudyCategories', 'CasestudyCategoriesController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('casestudyCategories/{casestudyCategories}/destroy', ['as' => 'cms.casestudyCategories.destroy', 'uses' => 'CasestudyCategoriesController@destroy']);
        /**
         * Casestudies Sectors
         */
        Route::resource('casestudySectors', 'CasestudySectorsController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('casestudySectors/{casestudySectors}/destroy', ['as' => 'cms.casestudySectors.destroy', 'uses' => 'CasestudySectorsController@destroy']);
        /**
         * Casestudies
         */
        Route::resource('casestudies', 'CasestudiesController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('casestudies/{casestudies}/destroy', ['as' => 'cms.casestudies.destroy', 'uses' => 'CasestudiesController@destroy']);

        /**
         * Team
         */
        Route::resource('team', 'TeamController', ['except'=>['show', 'destroy'], 'as' => 'cms']);
        Route::get('team/{team}/destroy', ['as' => 'cms.team.destroy', 'uses' => 'TeamController@destroy']);
        Route::post('team/sort', ['as' => 'cms.team.sort', 'uses' => 'TeamController@sort']);
        Route::get('team/{team}/tinify', ['as' => 'cms.team.tinify', 'uses' => 'TeamController@tinifyImage']);


        /**
         * Search Index
         */
        Route::get('search/generateIndex', ['as' => 'searchIndex', 'uses' => 'SearchController@index']);


        #Route::get('reduceImagesQuality', ['as' => 'reduceImagesQuality', 'uses' => 'ManipulateImagesController@reduceImagesQuality']);
    });
});

/* End : CMS Routes */
