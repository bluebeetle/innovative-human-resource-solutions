<?php

return [
    'path'     => base_path('storage/meta'),
    'filename' => 'settings.json',
    'fallback' => true,
];
