<?php

return [

    'locales' => [
        'en' => ['English', 'ltr' ],
        'ar' => ['Arabic', 'rtl']
    ],

    'templates' => [
        'home' => App\Templates\HomeTemplate::class,
        'homev2' => App\Templates\Homev2Template::class,
        'page' => App\Templates\PageTemplate::class,
        'overviewPage' => App\Templates\OverviewPageTemplate::class,
        'pageWithNoSidebar' => App\Templates\PageWithNoSidebarTemplate::class,
        'about' => App\Templates\AboutTemplate::class,
        'team' => App\Templates\TeamTemplate::class,
        'awards' => App\Templates\AwardsTemplate::class,
        'clients' => App\Templates\ClientsTemplate::class,
        'tabsPage' => App\Templates\TabsPageTemplate::class,
        'blog' => App\Templates\BlogTemplate::class,
        'article' => App\Templates\ArticleTemplate::class,
        'casestudies' => App\Templates\CasestudiesTemplate::class,
        'casestudy' => App\Templates\CasestudyTemplate::class,
        'event' => App\Templates\EventTemplate::class,
        'course' => App\Templates\CourseTemplate::class,
        'knowledgeCenter' => App\Templates\KnowledgeCenterTemplate::class,
        'calendar' => App\Templates\CalendarTemplate::class,
        'careers' => App\Templates\CareersTemplate::class,
        'contact' => App\Templates\ContactTemplate::class,
        'booking' => App\Templates\BookingTemplate::class,
        'fullWidth' => App\Templates\FullWidthTemplate::class,

    ],

    'mediables' => [
        'pages' => [
            'model' => App\Page::class,
            //'redirectUrl' => '/cms/{mediableType}/{mediableId}/media',
            //'redirectAction' => 'CMS\MediableController@show',
            //'redirectRoute' => 'cms.mediable.show',
            //'redirectParams' => ['mediableType', 'mediableId']
        ],
        'blog' => [
            'model' => App\Post::class
        ],
        'events' => [
            'model' => App\Event::class
        ],
        'courses' => [
            'model' => App\Course::class
        ],
        'casestudies' => [
            'model' => App\Casestudy::class
        ]
    ],

    'uploads' => [
        'maxImageWidth' => 1920,
        'imageQuality' => 50,
        'paths' => [
            'base' => 'uploads',
            'documents' => 'uploads/documents',
            'images' => 'uploads/images',
            'videos' => 'uploads/videos'
        ]
    ],

    'settingVars' => [
        /*
         *  This here to show what options can be given in here.
         * [
            'title' => 'New',
            'description' => 'New Settings',
            'namePrefix'=> 'new',
            'vars' => [
                ['name'=>'siteName', 'title'=>'Site Name', 'type' => 'textfield']
            ],
            'groups' => [
                [
                    'title' => 'Group A',
                    'description' => 'description',
                    'namePrefix'=> null,
                    'vars' => [
                        ['name' => 'url', 'title'=>'Url', 'type' => 'textfield', 'helpText' => 'Help text here'],
                        ['name'=>'textarea', 'title'=> 'textarea', 'type'=> 'textarea'],
                        ['name'=>'select', 'title'=> 'select', 'type'=> 'select', 'options' => ['a'=>'A', 'b'=>'B']],
                    ]
                ],
                [
                    'title' => 'Group A',
                    'description' => 'description',
                    'namePrefix'=> null,
                    'vars' => [
                        ['name' => 'url', 'title'=>'Url', 'type' => 'textfield', 'helpText' => 'Help text here'],
                        ['name'=>'textarea', 'title'=> 'textarea', 'type'=> 'textarea'],
                        ['name'=>'select', 'title'=> 'select', 'type'=> 'select', 'options' => ['a'=>'A', 'b'=>'B']],
                    ]
                ],
            ]
        ]*/
        [
            'title' => 'Social Media',
            'description' => 'Social Media Settings',
            'namePrefix'=> null,
            'vars' => [
                ['name'=>'facebookPageName', 'title'=>'Facebook Page Name', 'type' => 'textfield'],
                ['name'=>'facebookPageUrl', 'title'=>'Facebook Page Url', 'type' => 'textfield'],
                ['name'=>'twitterUsername', 'title'=>'Twitter Username', 'type' => 'textfield'],
                ['name'=>'linkedinPageUrl', 'title'=>'LinkedIN Page Url', 'type' => 'textfield'],
            ]
        ],
        [
        'title' => 'Emails',
        'description' => 'Emails for system generated notification',
        'namePrefix'=> 'emails',
        'vars' => [
            ['name'=>'from', 'title'=>'Send All Emails From', 'type' => 'textfield'],
            ['name'=>'bookCourse', 'title'=>'Send Course Booking Emails To', 'type' => 'textfield'],
            ['name'=>'bookEvent', 'title'=>'Send Event Booking Emails To', 'type' => 'textfield'],
            ['name'=>'contactEmail', 'title'=>'Send Contact Emails to', 'type' => 'textfield'],
        ]
        ]
    ],

    'cdn' => env('CDN', ''),
    'useCdn' => env('USE_CDN', false),

];
