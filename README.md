# Innovative HR Solutions

This is a new website for IHS. They are human capital consulting.
Here is thier webiste : http://www.ihsdubai.com/

####Repository Branches
The current active development branch is `develop` . Do all development work in development branch first.
`staging` branch is pulled on staging.
Once you are ready to move something to live, first push it to `master` branch, then deploy it from there.

####Credentials
http://wiki.bluebeetle.ae/display/BB/IHS+Credentials


##Operation
This is a Laravel based application, you need to set a virtual host to point to /public folder inside root. Then load it in the browser.


##Setup, Configuration and Development


#####Requirements
| Name | Link | Version |
|---|---|---|
| Composer | [https://getcomposer.org/] | Latest |
| Git | [https://git-scm.com/](https://git-scm.com/) | Latest |
| Node JS | [https://nodejs.org/en/](https://nodejs.org/en/) | Latest |
| Gulp | [http://gulpjs.com/](http://gulpjs.com/) | Latest |
| Apache Web Server | [https://httpd.apache.org/](https://httpd.apache.org/) | Latest |
| MySQL | [https://www.mysql.com/](https://www.mysql.com/) | Latest |

#####Setup
After cloning the repository, open a terminal window and `cd path/to/root`. Next, type `composer install` or `composer update` to install all project dependencies.

Once it finishes installing all dependencies, run `npm install` to install all the node dependencies.

Next copy and `.env.example` file and rename it to `.env` and update the configuration variables inside the file.

For Database if you don't have the database `.sql` file already, Laravel uses migrations, so you need to run the migrations in terminal so that the database is generated. `cd path/to/root`and run `php artisan migrate` . This command will generate the database structure but the data will not be there, therefor if you have the .`sql` please use that.

#####Development
Now run `gulp` or `gulp watch`while you are making changes in SCSS or JS.



##Notes to the Reader



##Changelog


###Latest updates
-


##Known Issues


**NOTE**: This section *MUST* be kept up to date or there is literally no point of having it. It's really dangerous to have it in the first place because the second it goes out of date it becomes useless, but I'm keeping it so there's at least a place to house bugs and todos.



##Contact Information
If you have questions or comments, please write to Sohail Ur rahman by email at [sohail@bluebeetle.co](sohail@bluebeetle.co).