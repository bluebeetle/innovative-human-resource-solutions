# Naming Rules
    - .my-css-class
    - #my-css-id
    - myVariable
    - myFunction() // js or php
    - JavascriptModules

# To Do

- Tiny MCE should be different for each module


- Add Data tables (http://bootstrap-table.wenzhixin.net.cn/documentation/)
- Add Markdown
- Use a different View Presenter
- User hreflang : https://support.google.com/webmasters/answer/189077?hl=en
- Add a module for Content only, e.g on homepage you have different content blocks, the newsletter section can be one content block.
- In Media files & images should be in one folder /uploads, and not /uploads/images & /uploads/documents
- Instead of customFields you can use some package that will add meta data to eloquent models. (e.g laravel-metadata, but search the best available package)

## Meta
- Add Meta plugin laravel-meta

## Media
- Add Search to Media (search by name,keyword , select type from dropdown {image,document,video,audio, unattached}
- Add isotope filters, Isotope is added
- Add Lazzy Loading, & Waypoints continous scrolling
- img/path_to_image/width/height



ALTER TABLE `clients` ADD `parent_id` INT NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `clients` ADD `lft` INT NULL DEFAULT NULL AFTER `name`, ADD `rgt` INT NULL DEFAULT NULL AFTER `lft`, ADD `depth` INT NULL DEFAULT NULL AFTER `rgt`;


Course Logo : 175 x 80
Event Logo : same
