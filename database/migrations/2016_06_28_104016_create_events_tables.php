<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Events
         */
        Schema::create('events', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('logo')->nullable();
            $table->boolean('logo_tinified')->default(0);
            $table->boolean('recursive')->default(0);
            $table->boolean('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });

        /**
         * Event Translations
         */
        Schema::create('event_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();

            $table->unique(['event_id','locale']);
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });

        /**
         * Event Dates
         */
        Schema::create('event_dates', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();

            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });

        /**
         * Event Date Translations
         */
        Schema::create('event_date_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('event_date_id')->unsigned();
            $table->string('locale')->index();
            $table->string('location');
            $table->unique(['event_date_id','locale']);
            $table->foreign('event_date_id')->references('id')->on('event_dates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_translations');
        Schema::drop('event_dates');
        Schema::drop('events');
    }
}
