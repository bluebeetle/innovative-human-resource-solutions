<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchIndexTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('search_index', function (Blueprint $table) {


            $table->engine = 'MyISAM';


            $table->increments('id');
            $table->string('locale')->index();
            $table->string('type')->index();
            $table->string('type_id')->index();
            $table->string('url');
            $table->string('title')->index();
            $table->text('content')->nullable();
        });

        DB::statement('ALTER TABLE search_index ADD FULLTEXT fulltext_content(content)');
        DB::statement('ALTER TABLE search_index ADD FULLTEXT fulltext_title(title)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('search_index');
    }
}
