<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogoSliderTranslationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Post Translations
         */
        Schema::create('logo_slider_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('logo_slider_id')->unsigned();
            $table->string('locale')->index();
            $table->string('link');

            $table->unique(['logo_slider_id','locale']);
            $table->foreign('logo_slider_id')->references('id')->on('logo_sliders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logo_slider_translations');
    }
}
