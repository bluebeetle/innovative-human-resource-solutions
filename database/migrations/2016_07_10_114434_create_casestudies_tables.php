<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasestudiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('casestudy_sectors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });



        Schema::create('casestudy_sector_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sector_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');

            $table->unique(['sector_id','locale']);
            $table->foreign('sector_id')->references('id')->on('casestudy_sectors')->onDelete('cascade');
        });


        Schema::create('casestudy_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });



        Schema::create('casestudy_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');

            $table->unique(['category_id','locale']);
            $table->foreign('category_id')->references('id')->on('casestudy_categories')->onDelete('cascade');
        });

        Schema::create('casestudies', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('sector_id')->unsigned()->nullable();
            $table->boolean('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('casestudy_categories')->onDelete('cascade');
            $table->foreign('sector_id')->references('id')->on('casestudy_sectors')->onDelete('cascade');
        });


        Schema::create('casestudy_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('casestudy_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();

            $table->unique(['casestudy_id','locale']);
            $table->foreign('casestudy_id')->references('id')->on('casestudies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('casestudy_translations');
        Schema::drop('casestudies');
        Schema::drop('casestudy_category_translations');
        Schema::drop('casestudy_categories');
        Schema::drop('casestudy_sector_translations');
        Schema::drop('casestudy_sectors');
    }
}
