<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('locations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('name');
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
            $table->integer('depth')->nullable();
            $table->string('coordinates');
            $table->boolean('head_office')->default(0);
            $table->string('phone')->nullable();
            $table->boolean('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });


        Schema::create('location_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');

            $table->unique(['location_id','locale']);
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('location_translations');
        Schema::drop('locations');
    }
}
