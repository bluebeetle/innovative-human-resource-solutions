<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFieldTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id');
            $table->string('model_type');
            $table->string('name');
            $table->string('order');
        });

        Schema::create('custom_field_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('custom_field_id')->unsigned();
            $table->string('locale')->index();
            $table->string('value');

            $table->unique(['custom_field_id','locale']);
            $table->foreign('custom_field_id')->references('id')->on('custom_fields')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('custom_field_translations');
        Schema::drop('custom_fields');
    }
}
