<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Courses
         */
        Schema::create('courses', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('logo')->nullable();
            $table->boolean('logo_tinified')->default(0);
            $table->decimal('cost', 8, 2);
            $table->boolean('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });

        /**
         * Course Translations
         */
        Schema::create('course_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->unique(['course_id','locale']);
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
        });

        /**
         * Course Dates
         */
        Schema::create('course_dates', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
        });

        /**
         * Course Date Translations
         */
        Schema::create('course_date_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('course_date_id')->unsigned();
            $table->string('locale')->index();
            $table->string('location');
            $table->unique(['course_date_id','locale']);
            $table->foreign('course_date_id')->references('id')->on('course_dates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_date_translations');
        Schema::drop('course_dates');
        Schema::drop('course_translations');
        Schema::drop('courses');
    }
}
