<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mediables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_id')->unsigned();
            $table->integer('mediable_id');
            $table->string('mediable_type');
            $table->string('collection')->nullable();


            //$table->primary(['media_id', 'mediable_id', 'mediable_type']);
            $table->foreign('media_id')->references('id')->on('media')->onDelete('cascade');
        });

        Schema::create('mediable_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mediable_id')->unsigned();
            $table->string('locale')->index();
            $table->string('caption')->nullable();


            $table->unique(['mediable_id','locale']);
            $table->foreign('mediable_id')->references('id')->on('mediables')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mediable_translations');
        Schema::drop('mediables');
    }
}
