<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->truncate();

        // Users
        DB::table('users')->insert([
            'name' => 'Sohail Rahman',
            'email' => 'sohail2rahman@gmail.com',
            'password' => Hash::make('secret'),
        ]);

        // Roles
        DB::table('roles')->insert([
            'slug' => 'superadmin',
            'name' => 'Super Admin',
            'description' => 'Super Admin',
            'status' => '0'
        ]);

        DB::table('roles')->insert([
            'slug' => 'admin',
            'name' => 'Admin',
            'description' => 'Site Admin',
        ]);

        DB::table('roles')->insert([
            'slug' => 'manager',
            'name' => 'Manager',
            'description' => 'Site Manager',
        ]);

        // Role User
        DB::table('role_user')->insert([
            'role_id' => '1',
            'user_id' => '1'
        ]);


        // Permissions
        DB::table('permissions')->insert([
            'slug' => 'login-to-cms',
            'name' => 'Login to CMS',
            'description' => 'Can loging to CMS',
        ]);

        // Permission Role
        DB::table('permission_role')->insert([
            'permission_id' => '1',
            'role_id' => '1'
        ]);
    }
}
