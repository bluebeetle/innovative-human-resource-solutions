<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('pages')->truncate();

        DB::table('pages')->insert([
            [
                'uri' => 'about',
                'parent_id' => null,
                'lft' => 3,
                'rgt' => 8,
                'depth' => 0,
                'hidden' => 0,
                'template' => null,
                'name' => 'About'
            ],
            [
                'uri' => 'contact',
                'parent_id' => 1,
                'lft' => 4,
                'rgt' => 5,
                'depth' => 1,
                'hidden' => 0,
                'template' => null,
                'name' => 'Contact'
            ],
            [
                'uri' => 'faq',
                'parent_id' => 1,
                'lft' => 6,
                'rgt' => 7,
                'depth' => 1,
                'hidden' => 0,
                'template' => null,
                'name' => 'FAQ'
            ],
            [
                'uri' => '/',
                'parent_id' => null,
                'lft' => 1,
                'rgt' => 2,
                'depth' => 0,
                'hidden' => 0,
                'template' => 'home',
                'name' => 'Home'
            ],
            [
                'uri' => 'archive',
                'parent_id' => null,
                'lft' => 9,
                'rgt' => 12,
                'depth' => 0,
                'hidden' => 0,
                'template' => 'blog',
                'name' => 'Blog'
            ],
            [
                'uri' => 'article/{id}/{slug}',
                'parent_id' => 5,
                'lft' => 10,
                'rgt' => 11,
                'depth' => 1,
                'hidden' => 1,
                'template' => 'blog.post',
                'name' => 'Blog Post'
            ],
        ]);
    }
}
