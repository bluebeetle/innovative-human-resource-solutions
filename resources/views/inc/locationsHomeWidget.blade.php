<div id="our-locations" class="auto-container">
    <h4>@lang('front.Our Locations')</h4>
    <div class="locations">
        <ul id="loc">
            @foreach($locations as $location)
                <li class="{{ $location->head_office?'active':'' }} item"><a href="{{ route('contact', [], false) }}" data-image="/{{ $location->landscape }}">{{ $location->title }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
