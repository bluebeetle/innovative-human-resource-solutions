@if($page->media()->ofType('pdf')->count() || $page->courses->count())

    <div class="tabs-wrapper">
    <ul class="tabs cf" data-tabgroup="page-{{ $page->id }}">

        @if($page->media()->ofType('pdf')->count())
            <li><a href="#page-{{ $page->id }}-downloads">@lang('front.Downloads')</a></li>
        @endif

        @if($page->courses->count())
            <li><a href="#page-{{ $page->id }}-programs">@lang('front.Related Programmes')</a></li>
        @endif

    </ul>
    <div id="page-{{ $page->id }}" class="tabgroup">
        @if($page->media()->ofType('pdf')->count())
            <div id="page-{{ $page->id }}-downloads">
                @foreach($page->media()->ofType('pdf')->get() as $file)
                    <a href="/downloadFile?path={{ $file->path }}&name={{ $file->title }}" class="pdf">
                        <span class="sprite-icon"></span>
                        {{ $file->title }}
                    </a>
                @endforeach
            </div>
        @endif
        @if($page->courses->count())
            <div id="page-{{ $page->id }}-programs">
                <ul>
                    @foreach($page->courses as $course)
                        <li><a href="#">{{ $course->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>

@endif
