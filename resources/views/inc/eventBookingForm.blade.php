
<div id="booking-form">

    <form action="/book/{{ $type }}/{{ $occasion->id }}/{{ $occasion->slug }}" method="post">

        {{ csrf_field() }}

        @if($occasion->slug == 'mbti')
        <div class="form-group">
            <h4>@lang('front.Are you certified in MBTI ?')</h4>

            <div class="radio-button">
                <input type="radio" name="certifiedMBTI" id="certified-MBTI-yes" value="yes" {{ old('certifiedMBTI') == 'yes' ? 'checked':'' }}/>
                <label for="certified-MBTI-yes">@lang('front.Yes')</label>
                <div class="check"><div class="inside"></div></div>
            </div>
            <div class="radio-button">
                <input type="radio" name="certifiedMBTI" id="certified-MBTI-no" value="no" {{ (old('certifiedMBTI') == 'no' || old('certifiedMBTI') == '') ? 'checked':'' }}/>
                <label for="certified-MBTI-no">@lang('front.No')</label>
                <div class="check"><div class="inside"></div></div>
            </div>
        </div>
        @endif

        <div class="form-group half">
            <div class="select-box {{ $errors->has('bookingDate') ? 'error' :  ''}}">
                <div class="placeholder">@lang('front.Booking Date*')</div>
                <select name="bookingDate" id="bookingDate" required>
                    <option value="">@lang('front.Booking Date*')</option>
                    @foreach($occasion->dates()->orderBy('start_date', 'asc')->get() as $date)
                        <option value="{{ $date->start_date->format('d F Y') }} - {{ $date->end_date->format('d F Y') }}" {{ ($date->start_date->format('d F Y') . ' - ' . $date->end_date->format('d F Y') == old('bookingDate'))?'selected':'' }}>
                            {{ presentOccasionDates($date->start_date, $date->end_date, ' - ') }}
                            -
                            {{ $date->location }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        @if($occasion->cost)
            <div class="form-group half">
                <div class="cost">
                    {{ $occasion->cost }}
                </div>
            </div>
        @endif


        <div class="form-group half">
            <input type="text"  name="firstName" placeholder="@lang('front.First Name*')" value="{{ old('firstName') }}" class="{{ $errors->has('firstName') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group half">
            <input type="text" name="lastName" placeholder="@lang('front.Last Name*')" value="{{ old('lastName') }}" class="{{ $errors->has('lastName') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group half">
            <input type="text" name="jobTitle" placeholder="@lang('front.Job Title*')" value="{{ old('jobTitle') }}" class="{{ $errors->has('jobTitle') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group half">
                <div class="radio-button">
                    <input type="radio" name="bookingType" id="booking-type-company" value="company" {{ old('bookingType') == 'company' ? 'checked':'' }}/>
                    <label for="booking-type-company">@lang('front.Company')</label>
                    <div class="check"><div class="inside"></div></div>
                </div>

                <div class="radio-button">
                    <input type="radio" name="bookingType" id="booking-type-individual"  value="individual" {{ (old('bookingType') == 'individual' || old('bookingType') == '') ? 'checked':'' }}/>
                    <label for="booking-type-individual">@lang('front.Individual')</label>
                    <div class="check"><div class="inside"></div></div>
                </div>
        </div>

        <div class="form-group" id="company-name">
            <input type="text" name="companyName" placeholder="@lang('front.Company Name*')" value="{{ old('companyName') }}" class="{{ $errors->has('companyName') ? 'error' :  ''}}"/>
        </div>

        <div class="form-group half">
            <input type="email" name="email" placeholder="@lang('front.Email Address*')" value="{{ old('email') }}" class="{{ $errors->has('email') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group half">
            <input type="tel" name="phone" placeholder="@lang('front.Contact Number*')" value="{{ old('phone') }}" class="{{ $errors->has('phone') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group">
            <div class="select-box">
                <div class="placeholder">@lang('front.How did you hear about us?')</div>
                <select name="hearFrom" id="hearFrom">
                    <option value="">@lang('front.How did you hear about us?')</option>
                    @foreach(['Search Engine', 'Social Network', 'Advertisement', 'Word of Mouth', 'Event', 'Forum or Blog', 'Other'] as $option)
                        <option value="{{ $option }}" {{ ($option==old('hearFrom')) ? 'selected':'' }}>@lang('front.'.$option)</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group">
            <input type="submit" value="@lang('front.Book')"/>
        </div>

    </form>
</div>
