<div id="booking-form">

    <form action="/book/{{ $type }}/{{ $occasion->id }}/{{ $occasion->slug }}" method="post">

        {{ csrf_field() }}

        @if($occasion->cost)
            <div class="form-group half right">
                <div class="cost">
                    @lang('front.AED') {{ $occasion->cost }}
                </div>
            </div>
        @endif

        <div class="form-group half">
            <div class="select-box {{ $errors->has('bookingDate') ? 'error' :  ''}}">
                <div class="placeholder">@lang('front.Booking Date*')</div>
                <select name="bookingDate" id="bookingDate" required>
                    <option value="">@lang('front.Booking Date*')</option>
                    @foreach($occasion->dates()->orderBy('start_date', 'asc')->get() as $date)
                        <option value="{{ $date->start_date->format('d F Y') }} - {{ $date->end_date->format('d F Y') }}" {{ ($date->start_date->format('d F Y') . ' - ' . $date->end_date->format('d F Y') == old('bookingDate'))?'selected':'' }}>
                            {{ presentOccasionDates($date->start_date, $date->end_date, ' - ') }}
                            -
                            {{ $date->location }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group half">
            <input type="text"  name="firstName" placeholder="@lang('front.First Name*')" value="{{ old('firstName') }}" class="{{ $errors->has('firstName') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group half">
            <input type="text" name="lastName" placeholder="@lang('front.Last Name*')" value="{{ old('lastName') }}" class="{{ $errors->has('lastName') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group half">
            <input type="text" name="jobTitle" placeholder="@lang('front.Job Title*')" value="{{ old('jobTitle') }}" class="{{ $errors->has('jobTitle') ? 'error' :  ''}}" required/>
        </div>

        <div class="form-group half">
                <div class="radio-button">
                    <input type="radio" name="bookingType" id="booking-type-company" value="company" {{ old('bookingType') == 'company' ? 'checked':'' }}/>
                    <label for="booking-type-company">@lang('front.Company')</label>
                    <div class="check"><div class="inside"></div></div>
                </div>

                <div class="radio-button">
                    <input type="radio" name="bookingType" id="booking-type-individual"  value="individual" {{ (old('bookingType') == 'individual' || old('bookingType') == '') ? 'checked':'' }}/>
                    <label for="booking-type-individual">@lang('front.Individual')</label>
                    <div class="check"><div class="inside"></div></div>
                </div>
        </div>

        <div class="form-group" id="company-name">
            <input type="text" name="companyName" placeholder="@lang('front.Company Name*')" value="{{ old('companyName') }}" class="{{ $errors->has('companyName') ? 'error' :  ''}}"/>
        </div>

        <div class="form-group half">
            <input type="email" name="email" placeholder="@lang('front.Email Address*')" value="{{ old('email') }}" class="{{ $errors->has('email') ? 'error' :  ''}}" required/>
        </div>


        <div class="form-group half">
            <input type="tel" name="phone" placeholder="@lang('front.Contact Number*')" value="{{ old('phone') }}" class="{{ $errors->has('phone') ? 'error' :  ''}}" required/>
        </div>


        <div class="form-group half">
            <div class="select-box {{ $errors->has('methodOfPayment') ? 'error' :  ''}}">
                <div class="placeholder">@lang('front.Method of Payment*')</div>
                <select name="methodOfPayment" id="methodOfPayment" required>
                    <option value="">@lang('front.Method of Payment*')</option>
                    @foreach(['Cash in Person', 'Bank Transfer', 'Credit Card', 'Cheque'] as $option)
                        <option value="{{ $option }}" {{ ($option==old('methodOfPayment')) ? 'selected':'' }}>@lang('front.'.$option)</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group" id="behalf-of-participant">
            <h4>@lang('front.Are you completing this form on behalf of participant?')</h4>

            <div class="radio-button">
                <input type="radio" name="behalfOfParticipant" id="behalf-of-participant-yes" value="yes" {{ old('behalfOfParticipant') == 'yes' ? 'checked':'' }}/>
                <label for="behalf-of-participant-yes">@lang('front.Yes')</label>
                <div class="check"><div class="inside"></div></div>
            </div>
            <div class="radio-button">
                <input type="radio" name="behalfOfParticipant" id="behalf-of-participant-no" value="no" {{ (old('behalfOfParticipant') == 'no' || old('behalfOfParticipant') == '') ? 'checked':'' }}/>
                <label for="behalf-of-participant-no">@lang('front.No')</label>
                <div class="check"><div class="inside"></div></div>
            </div>
        </div>

        <div id="sponsor-details">
            <div class="form-group half">
                <input type="text" name="sponsorFirstName" placeholder="@lang('front.Sponsor\'s First Name*')" value="{{ old('sponsorFirstName') }}" class="{{ $errors->has('sponsorFirstName') ? 'error' :  ''}}"/>
            </div>

            <div class="form-group half">
                <input type="text" name="sponsorLastName" placeholder="@lang('front.Sponsor\'s Last Name*')" value="{{ old('sponsorLastName') }}" class="{{ $errors->has('sponsorLastName') ? 'error' :  ''}}"/>
            </div>

            <div class="form-group half">
                <input type="email" name="sponsorEmail" placeholder="@lang('front.Sponsor\'s Email Address*')" value="{{ old('sponsorEmail') }}" class="{{ $errors->has('sponsorEmail') ? 'error' :  ''}}"/>
            </div>

            <div class="form-group half">
                <input type="tel" name="sponsorPhone" placeholder="@lang('front.Sponsor\'s Contact Number*')" value="{{ old('sponsorPhone') }}" class="{{ $errors->has('sponsorPhone') ? 'error' :  ''}}"/>
            </div>
        </div>

        <div class="form-group">
            <textarea name="electronicSignature" id="" cols="30" rows="10" placeholder="@lang('front.Electronic Signature and Date *')" class="{{ $errors->has('electronicSignature') ? 'error' :  ''}}" required>{{ old('electronicSignature') }}</textarea>
        </div>

        <div class="form-group">
            <div class="check-box {{ $errors->has('acceptTerms') ? 'error' :  ''}}">
                <input type="checkbox" name="acceptTerms" value="yes" id="acceptTerms" {{ old('acceptTerms') == 'yes' ? 'checked':'' }}/>
                <label for="acceptTerms">@lang('front.Acceptance of') <a href="{{ url('/legal') }}">@lang('front.Terms & Conditions*')</a></label>
                <div class="check"><div class="inside"></div></div>
            </div>
        </div>

        <div class="form-group">
            <div class="select-box">
                <div class="placeholder">@lang('front.How did you hear about us?')</div>
                <select name="hearFrom" id="hearFrom">
                    <option value="">@lang('front.How did you hear about us?')</option>
                    @foreach(['Search Engine', 'Social Network', 'Advertisement', 'Word of Mouth', 'Event', 'Forum or Blog', 'Other'] as $option)
                        <option value="{{ $option }}" {{ ($option==old('hearFrom')) ? 'selected':'' }}>@lang('front.'.$option)</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <textarea name="additionalRequirements" id="" cols="30" rows="10" placeholder="@lang('front.Additional Requirements?')">{{ old('additionalRequirements') }}</textarea>
        </div>

        <div class="form-group">
            <input type="submit" value="@lang('front.Book')"/>
        </div>

    </form>
</div>
