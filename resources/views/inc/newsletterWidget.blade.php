<div id="newsletter-signup">
    <div class="inner-wrapper">
        <div class="header-row cf clearfix">
            <div class="title">@lang('front.Sign Up to Latest News & Events')</div>
            <a href="#" class="why-signup"><span>@lang('front.Why I should sign up ?')</span><span>@lang('front.Wow, lets do it!')</span></a>
        </div>
    
        <div class="signup-benefits cf clearfix">
            <div class="nl_video">
                <div class="youtube-video embed-responsive embed-responsive-16by9" >
                    <div class="video" data-video-id="{{ $newsletter->customFieldsCollection->get('video') }}" data-autoplay="0">
                    </div>
                    <div class="overlay">
                        <a class="pause" href="#"></a>
                        <a class="play" href="#"></a>
                    </div>
                </div>
            </div>
            {!! $newsletter->content !!}
        </div>
    
        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
            hbspt.forms.create({
                portalId: "120635",
                formId: "{{ $locale == 'en' ? "fa3066bf-d93e-428b-9778-052a379446c8" : "a702534d-d2b3-496f-9f15-86be4debadb5" }}"
            });
        </script>
    </div>
</div>
