<div id="our-locations">
    <h4>@lang('front.We are present in...')</h4>
    <div class="locations">
        <ul>
            @foreach($locations as $location)
                <li class="{{ $location->head_office?'active':'' }}"><a href="{{ route('contact', [], false) }}" data-image="/{{ $location->landscape }}">{{ $location->title }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
