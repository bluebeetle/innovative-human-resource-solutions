<div id="newsletter-signup">
    <form action="{{ route('subscribeNewsletter', [], false) }}" method="post">
        {{ csrf_field() }}
        <div class="buttons">
            <input type="submit" value="@lang('front.Sign Up to Latest News & Events')"/>

            <a href="#" class="why-signup"><span>@lang('front.Why I should sign up ?')</span><span>@lang('front.Wow, lets do it!')</span></a>
        </div>
        <div class="email">
            <input type="email" placeholder="@lang('front.Email...')" name="email" required/>
        </div>

        <div class="signup-benefits cf">
            <div class="nl_video">
                <div class="youtube-video embed-responsive embed-responsive-16by9" >
                    <div class="video" data-video-id="{{ $newsletter->customFieldsCollection->get('video') }}" data-autoplay="0">
                    </div>
                    <div class="overlay">
                        <a class="pause" href="#"></a>
                        <a class="play" href="#"></a>
                    </div>
                </div>
            </div>
            {!! $newsletter->content !!}
        </div>
    </form>
</div>
