<div id="event-article">

    @include('inc.breadcrumbs', $breadcrumbs)


    @if(isset($post->mediaArray['banner']))
        <div id="banner">
            <img src="/{{ $post->mediaArray['banner'][0]['path'] }}" alt="banner"/>
        </div>
    @else
        <div id="shadow">
        </div>
    @endif


    <div id="share">
        <a href="#" class="trigger no-click">
            <i class="sprite-icon"></i>
        </a>
        <div class="share-links">
            <ul>
                <li><a target="_blank" class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}"><i class="sprite-icon"></i></a></li>
                <li><a target="_blank" class="twitter" href="http://twitter.com/share?url={{ urlencode(url()->current()) }}&amp;text={{ urlencode($post->title) }}"><i class="sprite-icon"></i></a></li>
                <li><a target="_blank" class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ urlencode(url()->current()) }}&amp;title={{ urlencode($post->title) }}&amp;summary={{ Str::limit(strip_tags($post->content), 200) }}"><i class="sprite-icon"></i></a></li>
            </ul>
        </div>
    </div>

    <div id="event-article-body">
        <div class="cf">

            @foreach($post->categories as $category)
                <div class="category {{ $category->slug }}">
                    {{ $category->title }}
                </div>
            @endforeach

            <div class="date">
                {{ $post->published_at->diffForHumans() }}
            </div>
        </div>
        <div class="left-column">

            @if($post->title)
                <h1>
                    {!! $post->title !!}
                </h1>
            @endif

            @if($post->customFieldsCollection->get('date'))
                <div class="sub-title">{{ $post->customFieldsCollection->get('date') }}</div>
            @endif

            {!! $post->content !!}
        </div>

    </div>

</div>
