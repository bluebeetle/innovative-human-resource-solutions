<div id="social" class="cf">
    <div id="facebook">
        <div class="media">
            <div class="media-left">
                <div class="logo">
                    <a href="{{ Setting::get('facebookPageUrl') }}">
                        {{--<img src="/images/fb-page-logo.png" width="100" alt="Innovative Human Resource Solutions Facebook Page"/>--}}
                    </a>
                </div>
            </div>
            <div class="media-body">
                <a href="{{ Setting::get('facebookPageUrl') }}" target="_blank" class="page-name">{!!  Setting::get('facebookPageName')  !!}</a>

                <div class="fb-like" data-href="{{ Setting::get('facebookPageUrl') }}"
                     data-layout="button_count" data-action="like" data-size="large" data-show-faces="false"
                     data-share="false"></div>
            </div>
        </div>
        <a href="{{ Setting::get('facebookPageUrl') }}" class="facebook-handle" target="_blank">
            <span class="facebook-icon"></span>
        </a>
    </div>
    <div id="twitter">
        <p>
            {!! $latestTweet !!}
        </p>
        <a href="https://twitter.com/{{ $twitterUsername }}" class="twitter-handle" target="_blank">
            <span class="twitter-icon"></span>
            @ {{ $twitterUsername }}
        </a>
    </div>
    <div id="linkedin">
        <p>
            Become a member of our PSI Middle East Company Page for the latest talent thinking from our thought leaders.
        </p>
        <div>
            <script src="//platform.linkedin.com/in.js" type="text/javascript">
                /*lang: en_US*/
            </script>
            <script type="IN/FollowCompany" data-id="{{ env('LINKEDIN_COMPANY_ID') }}" data-counter="right"></script>
        </div>
        <a href="{{ Setting::get('linkedinPageUrl') }}" target="_blank" class="linkedin-handle">
            <span class="linkedin-icon"></span>
        </a>
    </div>
</div>
