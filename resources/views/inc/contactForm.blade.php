<div id="booking-form" class="contactUs">

    <form action="{{ route('submitContactForm', [], false) }}" method="post">

        {{ csrf_field() }}

        <p style="text-align: center; margin-bottom:0">Please contact us now to learn more, and take the assessment for free.</p>
        <p style="text-align: left; padding:0px; margin:10px; 0px;"><strong style="font-weight: bold;">Phone:</strong> +971 4390 2778 </p>

        <div class="form-group">
            <input type="email" name="email" placeholder="@lang('front.Email Address*')" value="{{ old('email') }}" class="{{ $errors->has('email') ? 'error' :  ''}}" required/>
        </div>


        <div class="form-group">
            <input type="tel" name="phone" placeholder="@lang('front.Contact Number*')" value="{{ old('phone') }}" class="{{ $errors->has('phone') ? 'error' :  ''}}" required/>
        </div>


        <div class="form-group half">
            <input type="submit" value="@lang('front.Submit')"/>
        </div>

    </form>
</div>
