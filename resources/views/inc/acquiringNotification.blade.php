@if(!is_null(@$psiAcquiringNotification))
<div id="acquiring-notification" class="close">
    <div class="wrapper">
        <a href="{{route('article', [$psiAcquiringNotification->id, $psiAcquiringNotification->slug], false)}}">{{ $psiAcquiringNotification->banner_text }}</a>
        <a href="#" class="close">{!! file_get_contents('images/cross.svg') !!}</a>
    </div>
</div>
@endif