<div id="footer-newsletter-signup">
    <form action="/subscribeNewsletter" method="post">
        {{ csrf_field() }}
        <label>@lang('front.Sign Up to Latest News & Events')</label>
        <span  class="trigger-newsletter" data-izimodal-open="#modal-newsletter">@lang('front.Why I should sign up ?')</span>
        <div class="email">
            <input type="email" placeholder="@lang('Email Address')" name="email" required/>
        </div>
        <div class="buttons">
            <input type="submit" value="@lang('submit')"/>
        </div>
        <div id="modal-newsletter" class="signup-benefits cf">

            <div class="nl_video col-md-6 col-sm-6 col-xs-12">
                <div class="youtube-video embed-responsive embed-responsive-16by9" >
                    <div class="video" data-video-id="{{ $newsletter->customFieldsCollection->get('video') }}" data-autoplay="0">
                    </div>
                    <div class="overlay">
                        <a class="pause" href="#"></a>
                        <a class="play" href="#"></a>
                    </div>
                </div>
            </div>
            <div class="nl_content col-md-6 col-sm-6 col-xs-12">
            {!! $newsletter->content !!}
                <span data-izimodal-close="">Wow, lets do it!</span>

            </div>
        </div>
    </form>
</div>
