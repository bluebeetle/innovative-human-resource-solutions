<ul id="breadcrumb">
    <li><a href="/{{ $localePrefix }}{{ $localePrefix == 'ar' ? '/home':'' }}">@lang('front.Home')</a></li>
    @foreach($breadcrumbs as $k => $breadcrumbPage)
        <li>
            @if($breadcrumbs->count() == $k+1)
                {{ $breadcrumbPage->title }}
            @else
                <a href="{{ (isset($breadcrumbPage->name) ? route(Str::camel($breadcrumbPage->name),[], false) : url($breadcrumbPage->uri)) }}">{{ $breadcrumbPage->title }}</a>
            @endif
        </li>
    @endforeach
</ul>
