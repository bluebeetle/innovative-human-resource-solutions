@extends('layouts.default')

@section('title', $page->title)


@section('content')


<div class="container">
    <div id="page">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div id="page-header">

            @if(isset($media['banner']))
                <div id="banner">

                    @if($media['banner'])
                        <img class="lozad" data-src="{{ cdnAsset($media['banner'][0]['path']) }}"
                             alt="{{ $media['banner'][0]['caption'] ?? $page->title . ' banner' }}"/>
                    @endif

                    <div class="blur-bg-title">
                        <div class="bg" style="background-image: url('/{{ $media['banner'][0]['path'] }}')"></div>
                        <h1>
                            {{ $page->title }}
                        </h1>
                    </div>
                </div>
            @else
                <div id="shadow">
                </div>
            @endif

        </div>

        <div id="page-body">


            <div class="left-column">

                @if(!isset($media['banner']))
                    <h1 class="">
                        {{ $page->title }}
                    </h1>
                @endif

                {!! $page->content !!}

                    <div id="search-results" style="min-height:450px;">
                        @foreach($results as $item)
                            <div class="item {{ $item['type'] }}">
                                @if($item['link'] != '')
                                    <a href="{{ $item['link'] }}">
                                        <div class="title">{{ $item['title'] }}</div>
                                        <div class="type">{{ $item['type'] }}</div>
                                    </a>
                                @endif

                                @if($item['link'] == '')
                                    <div>
                                        <div class="title">{{ $item['title'] }}</div>
                                        <div class="type">{{ $item['type'] }}</div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
            </div>

        </div>

    </div>
</div>
@endsection
