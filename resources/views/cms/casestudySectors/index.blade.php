@extends('cms.layouts.default')

@section('title', 'Casestudy Sectors')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('cms.casestudySectors.create') }}" class="btn btn-primary">Create New Sector</a>
        </div>
    </div>

@stop

@section('content')

    <table class="table table-hover" data-table="table" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @forelse($sectors as $sector)
                <tr>
                    <td>
                        {{ $sector->name }}
                    </td>
                    <td>
                        {{ $sector->title }}
                    </td>
                    <td class="actions">
                        <a href="{{ route('cms.casestudySectors.edit', $sector->id) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="{{ route('cms.casestudySectors.destroy', $sector->id) }}" class="confirm-action">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" align="center">There are no sectors.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
