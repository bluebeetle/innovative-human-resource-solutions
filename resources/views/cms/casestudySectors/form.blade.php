@extends('cms.layouts.default')

@section('title', $sector->exists ? 'Edit Sector : '.$sector->title : 'Create New Sector')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')

    <form action="{{$sector->exists ? route('cms.casestudySectors.update', $sector->id) : route('cms.casestudySectors.store')}}"
          method="post">


        @if($sector->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Name </label>
                <input type="text"
                       class="form-control {{ $sector->exists ? '':'make-slug' }}"
                       id="name"
                       name="name"
                       placeholder="Name"
                       value="{{ old('name', $sector->name) }}"
                       required>
            </div>

            <div class="form-group">
                <label for="title">Title </label>
                <input type="text"
                       class="form-control translation-field"
                       id="title"
                       name="title"
                       placeholder="Title"
                       value="{{ old('title', $sector->title) }}"
                       required>
            </div>


        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $sector->exists ? 'Save' : 'Create' }}</button>
        </div>


    </form>


@endsection

