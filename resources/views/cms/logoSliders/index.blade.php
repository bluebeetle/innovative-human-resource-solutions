@extends('cms.layouts.default')

@section('title', 'Logo Sliders')


@section('heading')

    <div class="row heading">
        <div class="col-md-8">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-4 text-right">
            <form action="{{ route('cms.logoSliders.sort') }}" method="post" data-save-order-form="sortable">
                {{ csrf_field() }}
                <input type="hidden" name="ids" id="ids"/>
                <input type="submit" class="btn btn-primary" value="Save Order"/>
            </form>
            <a href="{{ route('cms.logoSliders.create') }}" class="btn btn-primary">Create New Logo Slider</a>
        </div>
    </div>

@stop


@section('content')

    <table class="table table-hover" data-table="table" data-sortable="sortable" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Link</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($logoSliders as $logoSlider)
            <tr id="row-{{ $logoSlider->id }}">
                <td width="50%">
                    <a href="{{ route('cms.logoSliders.edit', $logoSlider->id) }}">{{ $logoSlider->name }}</a>
                </td>
                <td width="40%">{{ $logoSlider->link }}</td>
                <td class="actions" width="10%">
                    <a href="{{ route('cms.logoSliders.edit', $logoSlider->id) }}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="{{ route('cms.logoSliders.destroy', $logoSlider->id) }}" class="confirm-action">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3" align="center">There are no logo sliders.</td>
            </tr>
        @endforelse
        </tbody>
    </table>

@endsection
