@extends('cms.layouts.default')

@section('title', $logoSlider->exists ? 'Edit Logo Slider : ' . $logoSlider->name : 'Create New Logo Slider')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop


@section('content')

    <form action="{{$logoSlider->exists ? route('cms.logoSliders.update', $logoSlider->id) : route('cms.logoSliders.store')}}"
          method="post" enctype="multipart/form-data">

        @if($logoSlider->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name </label>
            <input type="text"
                   class="form-control"
                   id="name"
                   name="name"
                   placeholder="Name"
                   value="{{ old('name', $logoSlider->name) }}"
                   required>
        </div>

        <div class="form-group row">

            @if(!empty($logoSlider->logo))
                <div class="col-md-3">
                    <img src="{{ imageThumb($logoSlider->logo, 300) }}" alt="" class="img-thumbnail img-responsive"/>

                    @if($logoSlider->logo_tinified == '0')
                        <div class="image-actions">
                            <a href="{{ route('cms.logoSliders.tinify', $logoSlider->id) }}" class="btn btn-primary">Tinify Image</a>
                        </div>
                    @endif
                </div>
            @endif

            <div class="col-md-{{ empty($logoSlider->logo)?12:9 }}">
                <label for="logo">Logo </label>
                <input type="file"
                       id="logo"
                       name="logo"
                       placeholder="Logo">
                @if(!empty($logoSlider->logo))
                    <span class="help-block">Upload a new file to replace the existing</span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="link">Link </label>
            <input type="text"
                   class="form-control"
                   id="link"
                   name="link"
                   placeholder="Link"
                   value="{{ old('link', $logoSlider->link) }}">
        </div>

        <div class="form-group row">
            <div class="col-md-12">
                <label for="order">Order</label>
            </div>
            <div class="col-md-2">
                <select id="order"
                        name="order"
                        class="form-control">
                    @foreach(
                        ['' => '', 'before' => 'Before', 'after' => 'After']
                        as $key => $value
                    )
                        <option value="{{ $key }}"
                        >{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-10">
                <select id="orderItem"
                        name="orderItem"
                        class="form-control">
                    <option value=""
                    >--</option>
                    @foreach($orderItems as $oi)
                        <option value="{{ $oi->id }}"
                        >{{ $oi->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="status">
                Status
            </label>
            <select id="status"
                    name="status"
                    class="form-control">
                <option value="0" {{ old('status', $logoSlider->status) == "0" ? 'selected':'' }}>Inactive
                </option>
                <option value="1" {{ old('status', $logoSlider->status) == "1" ? 'selected':'' }}>Active</option>
            </select>
        </div>


        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $logoSlider->exists ? 'Save' : 'Create' }}</button>
        </div>

    </form>


@endsection

