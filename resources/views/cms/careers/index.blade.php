@extends('cms.layouts.default')

@section('title', 'Jobs')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('cms.careers.create') }}" class="btn btn-primary">Create New Job</a>
        </div>
    </div>

@stop

@section('content')

    <table class="table table-hover" data-table="table" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @if($jobs->isEmpty())
            <tr>
                <td colspan="5" align="center">There are no jobs.</td>
            </tr>
        @else
            @foreach($jobs as $job)
                <tr>
                    <td>
                        <a href="{{ route('cms.careers.edit', $job->id) }}">
                        {{ $job->name }}
                        </a>
                    </td>
                    <td>
                        {{ $job->title }}
                    </td>
                    <td class="actions">
                        <a href="{{ route('cms.careers.edit', $job->id) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="{{ route('cms.careers.destroy', $job->id) }}" class="confirm-action">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection
