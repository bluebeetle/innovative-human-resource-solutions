<div class="form-group">
    <label for="{{$settingVar->varName}}">{{ $settingVar->title }}
    </label>



    <!--textfield-->
    @if(!isset($settingVar->type) || $settingVar->type=='textfield')
        <input type="text"
               class="form-control"
               name="settings[{{$settingVar->varName}}]"
               value="{{ Setting::get($settingVar->varName) }}">
    @endif

    @if($settingVar->type=='textarea')
        <textarea class="form-control"
               name="settings[{{$settingVar->varName}}]" rows="5">{{ Setting::get($settingVar->varName) }}</textarea>
    @endif

    @if($settingVar->type=='select')
        <select name="settings[{{$settingVar->varName}}]"
                class="form-control">
            @foreach($settingVar->options as $key => $value)
                <option value="{{ $key }}"
                        @if($key== Setting::get($settingVar->varName) )
                            selected
                        @endif
                >{{ $value }}</option>
            @endforeach
        </select>

    @endif




    @if(isset($settingVar->helpText))
        <span class="help-block">{{ $settingVar->helpText }}</span>
    @endif

</div>
