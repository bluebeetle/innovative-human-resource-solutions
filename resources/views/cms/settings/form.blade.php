@extends('cms.layouts.default')

@section('title', 'Settings')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')
    <form action="{{ route('cms.settings.update') }}" method="post">

        {{ csrf_field() }}

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach($settingVars as $settingKey => $settingVarsGroup)
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading-{{$settingKey}}">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$settingKey}}"
                           aria-expanded="true" aria-controls="collapse-{{$settingKey}}">
                            {{ $settingVarsGroup->title }}
                        </a>
                    </h4>
                </div>
                <div id="collapse-{{$settingKey}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <p>{{ $settingVarsGroup->description }}</p>
                        <br/>

                        @if(isset($settingVarsGroup->groups))
                            @foreach($settingVarsGroup->groups as $settingVarsSubGroup)
                                <h4>{{ $settingVarsSubGroup->title }}</h4>
                                <p>{{ $settingVarsSubGroup->description }}</p>

                                @foreach($settingVarsSubGroup->vars as $settingVar)
                                    @include('cms.settings._field')
                                @endforeach

                            @endforeach
                        @else
                            @foreach($settingVarsGroup->vars as $settingVar)
                                @include('cms.settings._field')
                            @endforeach
                        @endif


                    </div>
                </div>
            </div>
        @endforeach
        </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
    </form>
@stop
