<div class="tier clearfix" id="tier-{{ is_int($key) ? $key+1 : $key }}">
    <div class="col-md-4">
        @if($date)
            <input type="hidden" name="date_id_{{ $key }}" value="{{ $date->id }}"/>
        @endif
        <input type="text"
               class="form-control translation-field"
               id="location-{{ $key }}"
               name="location[]"
               placeholder="Location"
               value="{{ old('location.'.$key, isset($date->location)?$date->location:"" ) }}"
               required>
    </div>
    <div class="col-md-3">
        <input type="datetime"
               class="form-control datetimepicker only-date"
               id="start-date-{{ $key }}"
               name="start_date[]"
               placeholder="Starts"
               value="{{ old('start_date.'.$key, isset($date->start_date)?$date->start_date:date('Y-m-d') ) }}"
               required>
    </div>
    <div class="col-md-3">
        <input type="datetime"
               class="form-control datetimepicker only-date"
               id="end-date-{{ $key }}"
               name="end_date[]"
               placeholder="Ends"
               value="{{ old('end_date.'.$key, isset($date->end_date)?$date->end_date:date('Y-m-d') ) }}"
               required>
    </div>
    @if(is_int($key) && $key == 0)
        <div class="col-md-2">
            <a href="#" id="add-date-tier" class="btn btn-info"><span
                        class="glyphicon glyphicon-plus"></span></a>
        </div>
    @elseif (!is_int($key) || $key > 0)
        <div class="col-md-2">
            <a href="#" class="btn btn-danger remove-tier"><span
                        class="glyphicon glyphicon-remove"></span></a>
        </div>
    @endif
</div>
