@extends('cms.layouts.default')

@section('title', 'Courses')


@section('heading')

    <div class="row heading">
        <div class="col-md-8">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-4 text-right">
            <form action="{{ route('cms.courses.sort') }}" method="post" data-save-order-form="sortable">
                {{ csrf_field() }}
                <input type="hidden" name="ids" id="ids"/>
                <input type="submit" class="btn btn-primary" value="Save Order"/>
            </form>
            <a href="{{ route('cms.courses.create') }}" class="btn btn-primary">Create New Course</a>
        </div>
    </div>

@stop

@section('content')

    <table class="table table-hover" data-table="table" data-sortable="sortable" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @if($courses->isEmpty())
            <tr>
                <td colspan="5" align="center">There are no courses.</td>
            </tr>
        @else
            @foreach($courses as $course)
                <tr id="row-{{ $course->id }}">
                    <td>
                        <a href="{{ route('cms.courses.edit', $course->id) }}">
                        {{ $course->name }}
                        </a>
                    </td>
                    <td>
                        {{ $course->title }}
                    </td>
                    <td class="actions">
                        <a href="{{ route('cms.courses.edit', $course->id) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="{{ route('cms.courses.destroy', $course->id) }}" class="confirm-action">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                        <a href="{{ route('cms.mediable.show', ['courses', $course->id]) }}">
                            <span class="glyphicon glyphicon-file"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection
