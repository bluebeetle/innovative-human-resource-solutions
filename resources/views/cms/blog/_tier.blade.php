<div class="tier clearfix" id="tier-{{ is_int($key) ? $key+1 : $key }}">
    <div class="col-md-5">
        @if(isset($field))
            <input type="hidden" name="field_id_{{ $key }}" value="{{ $field->id }}"/>
        @endif
        <input type="text"
               class="form-control"
               id="field-name-{{ $key }}"
               name="field_name[]"
               placeholder="Name"
               value="{{ old('field_name.'.$key, (isset($field->name) && is_int($key))?$field->name:'') }}">
    </div>
    <div class="col-md-5">
        <input type="text"
               class="form-control translation-field"
               id="field-value-{{ $key }}"
               name="field_value[]"
               placeholder="Value"
               value="{{ old('field_value.'.$key, (isset($field->value) && is_int($key))?$field->value:'') }}">
    </div>
    @if(is_int($key) && $key == 0)
        <div class="col-md-2">
            <a href="#" id="add-custom-field-tier" class="btn btn-info"><span
                        class="glyphicon glyphicon-plus"></span></a>
        </div>
    @elseif (!is_int($key) || $key > 0)
        <div class="col-md-2">
            <a href="#" class="btn btn-danger remove-tier"><span
                        class="glyphicon glyphicon-remove"></span></a>
        </div>
    @endif
</div>
