@extends('cms.layouts.default')

@section('title', 'Blog Posts')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('cms.blog.create') }}" class="btn btn-primary">Create New Post</a>
        </div>
    </div>

@stop

@section('content')

    <table class="table table-hover" data-table="table" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th width="150">Published</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @forelse($posts as $post)
                <tr>
                    <td>
                        <a href="{{ route('cms.blog.edit', $post->id) }}">
                        {{ $post->name }}
                        </a>
                    </td>
                    <td>
                        {{ $post->title }}
                    </td>
                    <td>{{ $post->published_at->diffForHumans() }}</td>
                    <td class="actions">
                        <a href="{{ route('cms.blog.edit', $post->id) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="{{ route('cms.blog.destroy', $post->id) }}" class="confirm-action">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                        <a href="{{ route('cms.mediable.show', ['blog', $post->id]) }}">
                            <span class="glyphicon glyphicon-file"></span>
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" align="center">There are no posts.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
