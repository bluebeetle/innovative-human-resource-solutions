@extends('cms.layouts.default')

@section('title', $post->exists ? 'Edit Post : '.$post->title : 'Create New Post')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')

    <form action="{{$post->exists ? route('cms.blog.update', $post->id) : route('cms.blog.store')}}"
          method="post">


        @if($post->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs form-tablist" role="tablist">
                <li role="presentation" class="active"><a href="#content-container" aria-controls="content-container"
                                                          role="tab" data-toggle="tab">Content</a></li>
                <li role="presentation"><a href="#meta-container" aria-controls="meta-container" role="tab"
                                           data-toggle="tab">Meta</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="content-container">
                    <div class="form-group">
                        <label for="name">Name </label>
                        <input type="text"
                               class="form-control {{ $post->exists ? '':'make-slug' }}"
                               id="name"
                               name="name"
                               placeholder="Name"
                               value="{{ old('name', $post->name) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="slug">Slug
                        </label>

                        <input type="text"
                               class="form-control name-slug"
                               id="slug"
                               name="slug"
                               placeholder="Slug"
                               value="{{ old('slug', $post->slug) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="title">Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="title"
                               name="title"
                               placeholder="Title"
                               value="{{ old('title', $post->title) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="banner_text">Banner Text </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="banner_text"
                               name="banner_text"
                               placeholder="Banner Text"
                               value="{{ old('banner_text', $post->banner_text) }}"
                               >
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="published-at">Published At</label>
                        </div>
                        <div class="col-md-5">
                            <input type="datetime"
                                   class="form-control datetimepicker"
                                   id="published-at"
                                   name="published_at"
                                   placeholder="Published At"
                                   value="{{ old('published_at', $post->published_at?:date('Y-m-d') ) }}"
                                   required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Categories</label>
                        <div class="checkbox checkboxes">
                            @foreach($categories as $category)
                                <label>
                                    <input type="checkbox" name="categories[]" value="{{ $category->id }}"
                                    {{ in_array($category->id, $post->categories->pluck('id')->toArray())?"checked":"" }}
                                    />
                                    {{ $category->title }}
                                </label>
                            @endforeach
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <label>Custom Fields</label>
                            <br/>
                            <br/>
                            <div class="tiers" id="custom-field-tiers" data-tier-container="#custom-field-tiers"
                                 data-tier-source="#custom-field-tier-source" data-add-tier-trigger="#add-custom-field-tier"
                                 data-tier-id-count="{{ count($post->customFields)?:1 }}">
                                @forelse($post->customFields as $key => $field)
                                    @include('cms.blog._tier', ['key' => $key, 'field' => $field])
                                @empty
                                    @include('cms.blog._tier', ['key' => 0, 'field' => null])
                                @endforelse
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Content </label>

                        <div class="editor translation-field round-edges">
                            <textarea class="tinymce-editor form-control" name="content" id="content"
                                      rows="10">{{ old('content', $post->content) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="author_id">
                            Author
                        </label>
                        <select id="author_id"
                                name="author_id"
                                class="form-control">
                            @foreach($users as $user)
                                <option value="{{ $user->id }}" {{ old('author_id', $post->author_id?:$admin->id) == $user->id ? 'selected':'' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="status">
                            Status
                        </label>
                        <select id="status"
                                name="status"
                                class="form-control">
                            <option value="0" {{ old('status', $post->status) == "0" ? 'selected':'' }}>Inactive
                            </option>
                            <option value="1" {{ old('status', $post->status) == "1" ? 'selected':'' }}>Active</option>
                        </select>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="meta-container">
                    <div class="form-group">
                        <label for="title">Meta Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="meta_title"
                               name="meta_title"
                               placeholder="Meta Title"
                               value="{{ old('meta_title', $post->meta_title) }}"
                        >
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Keywords </label>

                        <div class="editor">
                            <textarea class="form-control  translation-field" name="meta_keywords" id="meta_keywords" rows="3"
                                      placeholder="Meta Keywords">{{ old('meta_keywords', $post->meta_keywords) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Description </label>

                        <div class="editor">
                            <textarea class="form-control  translation-field" name="meta_description" id="meta_description" rows="6"
                                      placeholder="Meta Description">{{ old('meta_description', $post->meta_description) }}</textarea>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $post->exists ? 'Save' : 'Create' }}</button>
        </div>


    </form>

    {{--Tier Source, Its not visible on the page rather its used to add more date fiels into the dates section--}}
    <div id="custom-field-tier-source" class="tier-source">
        @include('cms.blog._tier', ['key' => '{tierId}'])
    </div>


@endsection



