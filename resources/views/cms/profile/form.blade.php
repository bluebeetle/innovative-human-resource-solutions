@extends('cms.layouts.default')

@section('title', 'Profile')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop


@section('content')

    <form action="{{ route('cms.profile.update') }}"
          method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name </label>
            <input type="text"
                   class="form-control"
                   id="name"
                   name="name"
                   placeholder="Name"
                   value="{{ old('name', $admin->name) }}"
                   required>
        </div>

        <div class="form-group">
            <label for="email">Email </label>
            <input type="text"
                   class="form-control"
                   id="email"
                   name="email"
                   placeholder="Email"
                   value="{{ old('email', $admin->email) }}"
                   required>
        </div>

        <div class="form-group">
            <label for="password">Password </label>
            <input type="password"
                   class="form-control"
                   id="password"
                   name="password"
                   placeholder="Password">
        </div>

        <div class="form-group">
            <label for="password-confirmation">Confirm Password </label>
            <input type="password"
                   class="form-control"
                   id="password-confirmation"
                   name="password_confirmation"
                   placeholder="Confirm Password">
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </form>


@endsection
