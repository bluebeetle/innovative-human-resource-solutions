@extends('cms.layouts.default')

@section('title', 'Casestudies')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('cms.casestudies.create') }}" class="btn btn-primary">Create New Casestudy</a>
        </div>
    </div>

@stop

@section('content')

    <table class="table table-hover" data-table="table" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @forelse($casestudies as $casestudy)
                <tr>
                    <td>
                        {{ $casestudy->name }}
                    </td>
                    <td>
                        {{ $casestudy->title }}
                    </td>
                    <td class="actions">
                        <a href="{{ route('cms.casestudies.edit', $casestudy->id) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="{{ route('cms.casestudies.destroy', $casestudy->id) }}" class="confirm-action">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                        <a href="{{ route('cms.mediable.show', ['casestudies', $casestudy->id]) }}">
                            <span class="glyphicon glyphicon-file"></span>
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" align="center">There are no casestudies.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
