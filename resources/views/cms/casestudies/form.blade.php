@extends('cms.layouts.default')

@section('title', $casestudy->exists ? 'Edit Casestudy : '.$casestudy->title : 'Create New Casestudy')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')

    <form action="{{$casestudy->exists ? route('cms.casestudies.update', $casestudy->id) : route('cms.casestudies.store')}}"
          method="post">


        @if($casestudy->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs form-tablist" role="tablist">
                <li role="presentation" class="active"><a href="#content-container" aria-controls="content-container"
                                                          role="tab" data-toggle="tab">Content</a></li>
                <li role="presentation"><a href="#meta-container" aria-controls="meta-container" role="tab"
                                           data-toggle="tab">Meta</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="content-container">
                    <div class="form-group">
                        <label for="name">Name </label>
                        <input type="text"
                               class="form-control {{ $casestudy->exists ? '':'make-slug' }}"
                               id="name"
                               name="name"
                               placeholder="Name"
                               value="{{ old('name', $casestudy->name) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="slug">Slug
                        </label>

                        <input type="text"
                               class="form-control name-slug"
                               id="slug"
                               name="slug"
                               placeholder="Slug"
                               value="{{ old('slug', $casestudy->slug) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="title">Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="title"
                               name="title"
                               placeholder="Title"
                               value="{{ old('title', $casestudy->title) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="category_id">
                            Category
                        </label>
                        <select id="category_id"
                                name="category_id"
                                class="form-control">
                            <option value="" selected>--</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" {{ old('category_id', $casestudy->category_id) == $category->id ? 'selected':'' }}>{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="sector_id">
                            Sector
                        </label>
                        <select id="sector_id"
                                name="sector_id"
                                class="form-control">
                            <option value="" selected>--</option>
                            @foreach($sectors as $sector)
                                <option value="{{ $sector->id }}" {{ old('sector_id', $casestudy->sector_id) == $sector->id ? 'selected':'' }}>{{ $sector->title }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="name">Content </label>

                        <div class="editor translation-field round-edges">
                            <textarea class="tinymce-editor form-control" name="content" id="content"
                                      rows="10">{{ old('content', $casestudy->content) }}</textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="status">
                            Status
                        </label>
                        <select id="status"
                                name="status"
                                class="form-control">
                            <option value="0" {{ old('status', $casestudy->status) == "0" ? 'selected':'' }}>Inactive
                            </option>
                            <option value="1" {{ old('status', $casestudy->status) == "1" ? 'selected':'' }}>Active</option>
                        </select>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="meta-container">
                    <div class="form-group">
                        <label for="title">Meta Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="meta_title"
                               name="meta_title"
                               placeholder="Meta Title"
                               value="{{ old('meta_title', $casestudy->meta_title) }}"
                        >
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Keywords </label>

                        <div class="editor">
                            <textarea class="form-control translation-field" name="meta_keywords" id="meta_keywords" rows="3"
                                      placeholder="Meta Keywords">{{ old('meta_keywords', $casestudy->meta_keywords) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Description </label>

                        <div class="editor">
                            <textarea class="form-control translation-field" name="meta_description" id="meta_description" rows="6"
                                      placeholder="Meta Description">{{ old('meta_description', $casestudy->meta_description) }}</textarea>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $casestudy->exists ? 'Save' : 'Create' }}</button>
        </div>


    </form>


@endsection

