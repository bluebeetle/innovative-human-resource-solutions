@extends('cms.layouts.default')

@section('title', $category->exists ? 'Edit Category : '.$category->title : 'Create New Category')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')

    <form action="{{$category->exists ? route('cms.blogCategories.update', $category->id) : route('cms.blogCategories.store')}}"
          method="post">


        @if($category->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Name </label>
                <input type="text"
                       class="form-control {{ $category->exists ? '':'make-slug' }}"
                       id="name"
                       name="name"
                       placeholder="Name"
                       value="{{ old('name', $category->name) }}"
                       required>
            </div>

            <div class="form-group">
                <label for="slug">Slug
                </label>

                <input type="text"
                       class="form-control name-slug"
                       id="slug"
                       name="slug"
                       placeholder="Slug"
                       value="{{ old('slug', $category->slug) }}"
                       required>
            </div>

            <div class="form-group">
                <label for="title">Title </label>
                <input type="text"
                       class="form-control  translation-field"
                       id="title"
                       name="title"
                       placeholder="Title"
                       value="{{ old('title', $category->title) }}"
                       required>
            </div>


        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $category->exists ? 'Save' : 'Create' }}</button>
        </div>


    </form>


@endsection

