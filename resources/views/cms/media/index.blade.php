@extends('cms.layouts.default')

@section('title', 'Media')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#add-media" aria-expanded="false"
               aria-controls="add-media">
                Add
            </a>
        </div>
    </div>

@stop


@section('content')

    <div class="add-media collapse" id="add-media">

        <form id="add-media-form"
              action="{{ route('cms.media.store') }}"
              class="dropzone">

            {{ csrf_field() }}

            <div class="fallback">
                <input type="file"
                       name="file"
                       multiple>
            </div>

        </form>
    </div>

    <hr/>

    <div class="media-grid">
        @foreach($files as $file)
            <div class="{{$file->type}} media-file">
                    <a href="{{ route('cms.media.destroy', $file->id) }}" class="remove confirm-action">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                    <a href="{{ route("cms.media.edit", [$file->id]) }}" title="{{ $file->title }}" data-toggle="tooltip"
                       data-placement="top" class="file-link" title="{{ $file->title }}">

                        @if($file->type=='image')

                            <img src="{{ imageThumb($file->path, 200) }}"
                                 class="img-thumbnail img-responsive"
                                 alt="">

                        @elseif($file->type=='document' || $file->type=='pdf')

                            <span class="glyphicon glyphicon-file"></span>
                            <br/>
                            <span>{{ $file->title }}</span>

                        @endif
                    </a>
            </div>
        @endforeach
    </div>

@stop

@section('scripts.footer')
    <script>
        Dropzone.options.addMediaForm = {
            maxFilesize: 8,
            acceptedFiles: '.jpg, .jpeg, .png, .pdf'
        }
    </script>
@stop

