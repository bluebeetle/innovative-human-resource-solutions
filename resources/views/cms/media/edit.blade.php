@extends('cms.layouts.default')

@section('title', 'Media File')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop


@section('content')

    <div class="row media-detail">
        <div class="col-md-5">
            <div class="{{ $file->type }} media-file">
                @if($file->type == 'image')
                    <img src="{{ imageThumb($file->path, 600) }}" class="img-thumbnail img-responsive" alt=""/>
                @elseif($file->type == 'document' || $file->type == 'pdf')
                    <span class="glyphicon glyphicon-file"></span>
                @endif
            </div>
            <div>
                @if($file->type=='image')
                    <strong>Width: </strong> {{ $file->present()->width }}px <br/>
                    <strong>Height: </strong> {{ $file->present()->height }}px <br/>
                @endif
                <strong>Size: </strong> {{ $file->present()->file_size_KB }}kb
            </div>
            <div class="media-actions">
                @if($file->type == 'image' && $file->tinified == '0')
                    <a href="{{ route('cms.media.tinify', $file->id) }}" class="btn btn-primary">Tinify Image</a>
                @endif
            </div>
        </div>
        <div class="col-md-7">

            <form action="{{route('cms.media.update', $file->id)}}"
                  method="post">


                <input type="hidden" name="_method" value="PUT">

                {{ csrf_field() }}


                <div class="form-group">
                    <label for="title">Url </label>
                    <input type="text"
                           class="form-control" readonly
                           value="/{{ $file->path }}">
                </div>

                <div class="form-group">
                    <label for="title">Downloadable Url </label>
                    <input type="text"
                           class="form-control" readonly
                           value="/downloadFile?path={{ $file->path }}&name={{ $file->title }}">
                </div>

                <div class="form-group">
                    <label for="title">Title </label>
                    <input type="text"
                           class="form-control"
                           id="title"
                           name="title"
                           placeholder="Title"
                           value="{{ old('title', $file->title) }}"
                           required>
                </div>

                <div class="form-group">
                    <label for="caption">Caption </label>
                    <input type="text"
                           class="form-control"
                           id="caption"
                           name="caption"
                           placeholder="Caption"
                           value="{{ old('caption', $file->caption) }}"
                    >
                </div>

                <div class="form-group">
                    <label for="description">Description </label>
                    <textarea name="description" id="description" cols="30" rows="3" class="form-control"
                              placeholder="Description">{{ old('caption', $file->description) }}</textarea>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>


@stop



