@extends('cms.layouts.default')

@section('title', 'Attach Media')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>

            <h3>{{ getSingularModelName($resource) }} : {{ $resource->title }}</h3>
        </div>
        <div class="col-md-2 text-right">
            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#add-media" aria-expanded="false"
               aria-controls="add-media">
                Upload
            </a>
        </div>
    </div>

@stop

@section('content')

    <div class="add-media collapse" id="add-media">

        <form id="add-media-form"
              action="{{ route('cms.media.store') }}"
              class="dropzone">

            {{ csrf_field() }}

            <div class="fallback">
                <input type="file"
                       name="file"
                       multiple>
            </div>

        </form>
    </div>

    <hr/>

    <form id="attach-media-form"
          action="{{ route('cms.mediable.attach', [$mediableType, $resource->id]) }}" method="post">

        {{ csrf_field() }}

        <div class="form-actions on-top text-right">
            <button type="submit" class="btn btn-primary">Attach</button>
        </div>
        <div class="media-grid select-media">
            @foreach($files as $file)
                <div class="{{$file->type}} media-file">

                    <label title="{{ $file->title }}" data-toggle="tooltip" data-placement="top"
                           title="{{ $file->title }}">
                        <input type="checkbox" name="file[]" value="{{ $file->id }}"/>
                        @if($file->type=='image')

                            <img src="{{ imageThumb($file->path, 200) }}"
                                 class="img-thumbnail img-responsive"
                                 alt="">

                        @elseif($file->type=='document' || $file->type=='pdf')

                            <span class="glyphicon glyphicon-file"></span>
                            <br/>
                            <span>{{ $file->title }}</span>

                        @endif

                        <span class="glyphicon glyphicon-ok"></span>

                    </label>

                    {{--<div class="media-data">
                        <input type="text" name="collection[]" class="form-control" placeholder="Collection"/>
                        <input type="text" name="caption[]" class="form-control" placeholder="Caption"/>
                    </div>--}}
                </div>
            @endforeach

        </div>
    </form>

@stop

@section('scripts.footer')
    <script>
        Dropzone.options.addMediaForm = {
            maxFilesize: 8,
            acceptedFiles: '.jpg, .jpeg, .png, .pdf'
        }
    </script>
@stop

