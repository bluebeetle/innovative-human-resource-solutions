@extends('cms.layouts.default')

@section('title', 'Manage Media')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>

            <h3>{{ getSingularModelName($resource) }} : {{ $resource->title }}</h3>
        </div>
    </div>

    <hr/>

@stop

@section('content')

    <form id="attach-media-form"
          action="{{ route('cms.mediable.update', [$mediableType, $resource->id]) }}" method="post">

        {{ csrf_field() }}

        <div class="form-actions on-top text-right">
            <a class="btn btn-primary" role="button"
               href="{{ route('cms.mediable.select', [$mediableType, $resource->id]) }}">
                Attach Media
            </a>
            &nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        <div class="media-grid manage-media">

            @if($resource->media->isEmpty())
                <p class="text-center">
                    There is no media.
                </p>
            @else
                @foreach($resource->media as $file)
                    <div class="{{$file->type}} media-file">

                        <a href="#" class="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>

                        <input type="checkbox" name="delete[]" value="{{ $file->pivot->id }}"/>

                        <div class="file">
                            <label title="{{ $file->title }}" data-toggle="tooltip" data-placement="top"
                                   title="{{ $file->title }}">

                                @if($file->type=='image')

                                    <img src="{{ imageThumb($file->path, 200) }}"
                                         class="img-thumbnail img-responsive"
                                         alt="">

                                @elseif($file->type=='document' || $file->type='pdf')

                                    <span class="glyphicon glyphicon-file"></span>
                                    <br/>
                                    <span>{{ $file->title }}</span>

                                @endif

                                <span class="glyphicon glyphicon-remove"></span>

                            </label>
                        </div>

                        <div class="media-data">
                            <input type="text" name="collection[{{ $file->pivot->id }}]" class="form-control"
                                   placeholder="Collection" value="{{ $file->pivot->collection }}"/>
                            <input type="text" name="caption[{{ $file->pivot->id }}]" class="form-control translation-field"
                                   placeholder="Caption" value="{{ $file->pivot->caption }}"/>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </form>

@stop

@section('scripts.footer')
    <script>
        Dropzone.options.addMediaForm = {
            maxFilesize: 8,
            acceptedFiles: '.jpg, .jpeg, .png, .pdf'
        }
    </script>
@stop

