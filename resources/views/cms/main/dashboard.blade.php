@extends('cms.layouts.default')

@section('title', 'Dashboard')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>

    </div>

@stop


@section('content')
<div style="height:600px;">
    <h4>Ideal Image sizes</h4>
    Page Banners : 1400 x 460 <br/>
    Team Member Portraits : 420 x 420 <br/>
    Client Logos : 290 x 290 <br/>

    <br/><br/><br/>
    Please Note : All the above image sizes differ slightly, but for a better and more uniform look, please upload the same size images for these areas
    e.g All client logos should be same size
</div>
@endsection
