<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title') | {{ Setting::get('siteName') }} CMS</title>
    <link rel="stylesheet" href="/css/cms/styles.css" />

    @yield('css.header')

</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="clearfix" id="topbar">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="{{ route('cms.dashboard') }}">{{ Setting::get('siteName') }}</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                @if(count($locales)>1)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $locales[$locale][0] }} <span class="caret"></span> </a>
                        <ul class="dropdown-menu">
                            @foreach($locales as $code => $localeR)
                                <li class="{{ $code==$locale?'active':'' }}"><a href="{{ route('cms.setlocale', $code) }}">{{ $localeR[0] }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                <li>
                    <a href="/clearCache">Clear Cache</a>
                </li>
                <li>
                    <a href="/cms/search/generateIndex">Generate Search Index</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> {{$admin->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('cms.profile.edit') }}"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                        <li><a href="{{ route('cms.settings.edit') }}"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="{{ activeIf('users', 'cms') }}"><a href="{{ route('cms.users.index') }}">Users</a></li>
                <li class="{{ activeIf('pages', 'cms') }}"><a href="{{ route('cms.pages.index') }}">Pages</a></li>
                <li class="{{ activeIf('logoSliders', 'cms') }}"><a href="{{ route('cms.logoSliders.index') }}">Logo Slider</a></li>
                <li class="dropdown {{ activeIf('blog', 'cms') }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blog <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ activeIf('blog', 'cms', '') }}"><a href="{{ route('cms.blog.index') }}">Posts</a></li>
                        <li class="{{ activeIf('blogCategories', 'cms') }}"><a href="{{ route('cms.blogCategories.index') }}">Post Categories</a></li>
                    </ul>
                </li>
                <li class="dropdown {{ activeIf('casestudies', 'cms') }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Casestudies <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="{{ activeIf('casestudies', 'cms', '') }}"><a href="{{ route('cms.casestudies.index') }}">Casestudies</a></li>
                        <li class="{{ activeIf('casestudyCategories', 'cms') }}"><a href="{{ route('cms.casestudyCategories.index') }}">Casestudies Categories</a></li>
                        <li class="{{ activeIf('casestudySectors', 'cms') }}"><a href="{{ route('cms.casestudySectors.index') }}">Casestudies Sectors</a></li>
                    </ul>
                </li>
                <li class="{{ activeIf('events', 'cms') }}"><a href="{{ route('cms.events.index') }}">Events</a></li>
                <li class="{{ activeIf('courses', 'cms') }}"><a href="{{ route('cms.courses.index') }}">Courses</a></li>
                <li class="{{ activeIf('awards', 'cms') }}"><a href="{{ route('cms.awards.index') }}">Awards</a></li>
                <li class="{{ activeIf('testimonials', 'cms') }}"><a href="{{ route('cms.testimonials.index') }}">Testimonials</a></li>
                <li class="{{ activeIf('clients', 'cms') }}"><a href="{{ route('cms.clients.index') }}">Clients</a></li>
                <li class="{{ activeIf('careers', 'cms') }}"><a href="{{ route('cms.careers.index') }}">Careers</a></li>
                <li class="{{ activeIf('locations', 'cms') }}"><a href="{{ route('cms.locations.index') }}">Locations</a></li>
                <li class="{{ activeIf('team', 'cms') }}"><a href="{{ route('cms.team.index') }}">Team</a></li>
                <li class="{{ activeIf('media', 'cms') }}"><a href="{{ route('cms.media.index') }}">Media</a></li>
            </ul>

        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    @yield('heading')

    @include('cms.inc.alerts')

    @yield('content')

    <div class="credits">
        Powered by <a href="http://www.bluebeetle.ae" target="_blank" class="bb-logo-small" title="Dubai Web Design Agency - Blue Beetle">Blue Beetle</a>
    </div>
</div>


<script src="/packages/tinymce/tinymce.min.js"></script>
<script src="/js/cms/scripts.js"></script>
@yield('scripts.footer')

</body>
</html>
