@if(count($errors) > 0)
    <div class="alert alert-danger">

        <strong>We found some errors!</strong>

        <br/>
        <br/>

        @foreach($errors->all() as $error)
            - {{ $error }} <br/>
        @endforeach

    </div>
@endif

@if(session()->has('flash_message'))

    <div class="alert alert-{{ session("flash_message.level") }} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @if(session()->has('flash_message.title'))
            <strong>{{ session("flash_message.title") }}</strong>
        @endif
        {{ session("flash_message.message") }}
    </div>

@endif
