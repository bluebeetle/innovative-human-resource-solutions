@extends('cms.layouts.default')

@section('title', 'Awards')


@section('heading')

    <div class="row heading">
        <div class="col-md-8">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-4 text-right">
            <form action="{{ route('cms.awards.sort') }}" method="post" data-save-order-form="sortable">
                {{ csrf_field() }}
                <input type="hidden" name="ids" id="ids"/>
                <input type="submit" class="btn btn-primary" value="Save Order"/>
            </form>
            <a href="{{ route('cms.awards.create') }}" class="btn btn-primary">Create New Award</a>
        </div>
    </div>

@stop


@section('content')

    <table class="table table-hover" data-table="table" data-sortable="sortable" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($awards as $award)
            <tr id="row-{{ $award->id }}">
                <td width="50%">
                    <a href="{{ route('cms.awards.edit', $award->id) }}">{{ $award->name }}</a>
                </td>
                <td width="40%">{{ $award->title }}</td>
                <td class="actions" width="10%">
                    <a href="{{ route('cms.awards.edit', $award->id) }}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="{{ route('cms.awards.destroy', $award->id) }}" class="confirm-action">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3" align="center">There are no awards.</td>
            </tr>
        @endforelse
        </tbody>
    </table>

@endsection
