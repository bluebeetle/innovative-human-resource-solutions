@extends('cms.layouts.default')

@section('title', $employee->exists ? 'Edit Employee : ' . $employee->name : 'Create New Employee')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop


@section('content')

    <form action="{{$employee->exists ? route('cms.team.update', $employee->id) : route('cms.team.store')}}"
          method="post" enctype="multipart/form-data">

        @if($employee->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name </label>
            <input type="text"
                   class="form-control"
                   id="name"
                   name="name"
                   placeholder="Name"
                   value="{{ old('name', $employee->name) }}"
                   required>
        </div>

        <div class="form-group">
            <label for="name">Title </label>
            <input type="text"
                   class="form-control translation-field"
                   id="title"
                   name="title"
                   placeholder="Title"
                   value="{{ old('title', $employee->title) }}"
                   required>
        </div>

            <div class="form-group">
                <label for="designation">Designation </label>
                <input type="text"
                       class="form-control translation-field"
                       id="designation"
                       name="designation"
                       placeholder="Designation"
                       value="{{ old('designation', $employee->designation) }}"
                       required>
            </div>

        <div class="form-group row">

            @if(!empty($employee->dp))
                <div class="col-md-3">
                    <img src="{{ imageThumb($employee->dp, 300) }}" alt="" class="img-thumbnail img-responsive"/>


                @if($employee->dp_tinified == '0')
                    <div class="image-actions">
                        <a href="{{ route('cms.team.tinify', $employee->id) }}" class="btn btn-primary">Tinify Image</a>
                    </div>
                @endif
                </div>
            @endif

            <div class="col-md-{{ empty($employee->dp)?12:9 }}">
                <label for="dp">Display Picture </label>
                <input type="file"
                       id="dp"
                       name="dp"
                       placeholder="Display Picture">
                @if(!empty($employee->dp))
                    <span class="help-block">Upload a new file to replace the existing</span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">
                <label for="order">Order</label>
            </div>
            <div class="col-md-2">
                <select id="order"
                        name="order"
                        class="form-control">
                    @foreach(
                        ['' => '', 'before' => 'Before', 'after' => 'After']
                        as $key => $value
                    )
                        <option value="{{ $key }}"
                        >{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-10">
                <select id="orderItem"
                        name="orderItem"
                        class="form-control">
                    @foreach(
                    ['' => ''] + $orderItems->pluck('title', 'id')->toArray()
                    as $key=>$value
                    )
                        <option value="{{ $key }}"
                        >{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Content </label>

            <div class="editor translation-field round-edges">
                            <textarea class="form-control content-editor" name="content" id="content"
                                      rows="10">{{ old('content', $employee->content) }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="facebook">Facebook </label>
            <input type="text"
                   class="form-control"
                   id="facebook"
                   name="facebook"
                   placeholder="Facebook Handle"
                   value="{{ old('facebook', $employee->facebook) }}"
                   >
        </div>

            <div class="form-group">
                <label for="twitter">Twitter </label>
                <input type="text"
                       class="form-control"
                       id="twitter"
                       name="twitter"
                       placeholder="Twitter Handle"
                       value="{{ old('twitter', $employee->twitter) }}"
                       >
            </div>

            <div class="form-group">
                <label for="linkedin">Linkedin </label>
                <input type="text"
                       class="form-control"
                       id="linkedin"
                       name="linkedin"
                       placeholder="Linkedin Handle"
                       value="{{ old('linkedin', $employee->linkedin) }}"
                       >
            </div>

        <div class="form-group">
            <label for="status">
                Status
            </label>
            <select id="status"
                    name="status"
                    class="form-control">
                <option value="0" {{ old('status', $employee->status) == "0" ? 'selected':'' }}>Inactive
                </option>
                <option value="1" {{ old('status', $employee->status) == "1" ? 'selected':'' }}>Active</option>
            </select>
        </div>


        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $employee->exists ? 'Save' : 'Create' }}</button>
        </div>

    </form>

@endsection

@section('scripts.footer')
    <script>
        tinymce.init({
            selector: 'textarea.content-editor',
            plugins: [
                "autolink link image lists preview hr anchor pagebreak",
                "visualblocks visualchars code fullscreen media nonbreaking",
                "table contextmenu template paste textpattern"
            ],
            toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect | bullist numlist link unlink | removeformat nonbreaking  | code",
            min_height : 300,
            max_height: 500,
            menubar: false,
            formats : {
                alignleft: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
                aligncenter: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
                alignright: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
                alignfull: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
                bold: {inline : 'span', 'classes' : 'bold'},
                italic: {inline : 'span', 'classes' : 'italic'},
                underline: {inline : 'span', 'classes' : 'underline', exact : true},
                strikethrough: {inline : 'del'}
            },
            block_formats: 'Paragraph=p;Header 2=h2;Header 3=h3; Header 4=h4;Header 5=h5',
            style_formats: [
            ],
            content_css : '/css/cms/editor-content.css?' + new Date().getTime(),
            relative_urls : false

        });
    </script>
@stop
