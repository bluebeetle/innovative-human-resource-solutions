@extends('cms.layouts.default')

@section('title', 'Locations')


@section('heading')

    <div class="row heading">
        <div class="col-md-8">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-4 text-right">
            <form action="{{ route('cms.locations.sort') }}" method="post" data-save-order-form="sortable">
                {{ csrf_field() }}
                <input type="hidden" name="ids" id="ids"/>
                <input type="submit" class="btn btn-primary" value="Save Order"/>
            </form>
            <a href="{{ route('cms.locations.create') }}" class="btn btn-primary">Create New Location</a>
        </div>
    </div>

@stop


@section('content')

    <table class="table table-hover" data-table="table" data-sortable="sortable" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($locations as $location)
            <tr id="row-{{ $location->id }}">
                <td>
                    <a href="{{ route('cms.locations.edit', $location->id) }}">{{ $location->name }}</a>
                </td>
                <td>{{ $location->title }}</td>
                <td class="actions">
                    <a href="{{ route('cms.locations.edit', $location->id) }}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="{{ route('cms.locations.destroy', $location->id) }}" class="confirm-action">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3" align="center">There are no locations.</td>
            </tr>
        @endforelse
        </tbody>
    </table>

@endsection
