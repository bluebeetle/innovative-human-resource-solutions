@extends('cms.layouts.default')

@section('title', $location->exists ? 'Edit Location : ' . $location->name : 'Create New Location')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop


@section('content')

    <form action="{{$location->exists ? route('cms.locations.update', $location->id) : route('cms.locations.store')}}"
          method="post" enctype="multipart/form-data">

        @if($location->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name </label>
            <input type="text"
                   class="form-control"
                   id="name"
                   name="name"
                   placeholder="Name"
                   value="{{ old('name', $location->name) }}"
                   required>
        </div>

        <div class="form-group">
            <label for="name">Title </label>
            <input type="text"
                   class="form-control translation-field"
                   id="title"
                   name="title"
                   placeholder="Title"
                   value="{{ old('title', $location->title) }}"
                   required>
        </div>

        <div class="form-group row">

            @if(!empty($location->landscape))
                <div class="col-md-3">
                    <img src="{{ imageThumb($location->landscape, 300) }}" alt="" class="img-thumbnail img-responsive"/>

                    @if($location->landscape_tinified == '0')
                        <div class="image-actions">
                            <a href="{{ route('cms.locations.tinify', $location->id) }}" class="btn btn-primary">Tinify Image</a>
                        </div>
                    @endif
                </div>
            @endif

            <div class="col-md-{{ empty($location->landscape)?12:9 }}">
                <label for="landscape">Landscape </label>
                <input type="file"
                       id="landscape"
                       name="landscape"
                       placeholder="Landscape">
                @if(!empty($location->landscape))
                    <span class="help-block">Upload a new file to replace the existing</span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">
                <label for="order">Order</label>
            </div>
            <div class="col-md-2">
                <select id="order"
                        name="order"
                        class="form-control">
                    @foreach(
                        ['' => '', 'before' => 'Before', 'after' => 'After']
                        as $key => $value
                    )
                        <option value="{{ $key }}"
                        >{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-10">
                <select id="orderItem"
                        name="orderItem"
                        class="form-control">
                    @foreach(
                    ['' => ''] + $orderItems->pluck('title', 'id')->toArray()
                    as $key=>$value
                    )
                        <option value="{{ $key }}"
                        >{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Phone </label>
            <input type="text"
                   class="form-control"
                   id="phone"
                   name="phone"
                   placeholder="Phone"
                   value="{{ old('phone', $location->phone) }}"
            >
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="head_office" value="1"
                       id="hidden" {{old('head_office', $location->head_office)=="1"?"checked":""}}>
                Head Office

                <span class="help-block">
                Check if this location is head office. (Should be only one head office).
            </span>
            </label>
        </div>


        <div class="form-group">
            <label for="coordinates">Coordinates </label>

            <input type="text"
                   class="form-control"
                   id="coordinates"
                   name="coordinates"
                   placeholder="Coordinates"
                   value="{{ old('coordinates', $location->coordinates) }}"
                   required>
            <span class="help-block">Get the coordinates from <a href="https://www.google.com/maps" target="_blank">https://www.google.com/maps</a></span>
        </div>

        <div class="form-group">
            <label for="status">
                Status
            </label>
            <select id="status"
                    name="status"
                    class="form-control">
                <option value="0" {{ old('status', $location->status) == "0" ? 'selected':'' }}>Inactive
                </option>
                <option value="1" {{ old('status', $location->status) == "1" ? 'selected':'' }}>Active</option>
            </select>
        </div>


        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $location->exists ? 'Save' : 'Create' }}</button>
        </div>

    </form>


@endsection
