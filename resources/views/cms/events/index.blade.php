@extends('cms.layouts.default')

@section('title', 'Events')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('cms.events.create') }}" class="btn btn-primary">Create New Event</a>
        </div>
    </div>

@stop

@section('content')

    <table class="table table-hover" data-table="table" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @forelse($events as $event)
                <tr>
                    <td>
                        <a href="{{ route('cms.events.edit', $event->id) }}">
                        {{ $event->name }}
                        </a>
                    </td>
                    <td>
                        {{ $event->title }}
                    </td>
                    <td class="actions">
                        <a href="{{ route('cms.events.edit', $event->id) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="{{ route('cms.events.destroy', $event->id) }}" class="confirm-action">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                        <a href="{{ route('cms.mediable.show', ['events', $event->id]) }}">
                            <span class="glyphicon glyphicon-file"></span>
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" align="center">There are no events.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
