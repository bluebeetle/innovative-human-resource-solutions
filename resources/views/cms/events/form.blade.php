@extends('cms.layouts.default')

@section('title', $event->exists ? 'Edit Event : '.$event->title : 'Create New Event')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')

    <form action="{{$event->exists ? route('cms.events.update', $event->id) : route('cms.events.store')}}"
          method="post" enctype="multipart/form-data">


        @if($event->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs form-tablist" role="tablist">
                <li role="presentation" class="active"><a href="#content-container" aria-controls="content-container"
                                                          role="tab" data-toggle="tab">Content</a></li>
                <li role="presentation"><a href="#meta-container" aria-controls="meta-container" role="tab"
                                           data-toggle="tab">Meta</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="content-container">
                    <div class="form-group">
                        <label for="name">Name </label>
                        <input type="text"
                               class="form-control {{ $event->exists ? '':'make-slug' }}"
                               id="name"
                               name="name"
                               placeholder="Name"
                               value="{{ old('name', $event->name) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="slug">Slug
                        </label>

                        <input type="text"
                               class="form-control name-slug"
                               id="slug"
                               name="slug"
                               placeholder="Slug"
                               value="{{ old('slug', $event->slug) }}"
                               required>
                    </div>


                    <div class="form-group">
                        <label for="title">Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="title"
                               name="title"
                               placeholder="Title"
                               value="{{ old('title', $event->title) }}"
                               required>
                    </div>

                    <div class="form-group row">

                        @if(!empty($event->logo))
                            <div class="col-md-3">
                                <img src="{{ imageThumb($event->logo, 300) }}" alt="" class="img-thumbnail img-responsive"/>

                                @if($event->logo_tinified == '0')
                                    <div class="image-actions">
                                        <a href="{{ route('cms.events.tinify', $event->id) }}" class="btn btn-primary">Tinify Image</a>
                                    </div>
                                @endif
                            </div>
                        @endif

                        <div class="col-md-{{ empty($event->logo)?12:9 }}">
                            <label for="name">Logo </label>
                            <input type="file"
                                   id="logo"
                                   name="logo"
                                   placeholder="Logo">
                            @if(!empty($event->logo))
                                <span class="help-block">Upload a new file to replace the existing</span>
                            @endif
                        </div>
                    </div>

                    <div class="checkbox">
                        <label><input type="checkbox" name="recursive" value="1" {{ (old('recursive', $event->recursive) == "1") ? "checked":"" }}/> Yearly Recursive</label>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <label>Dates</label>
                            <br/>
                            <br/>

                            <div class="clearfix">
                                <div class="col-md-4">
                                    <label for="start-date">Location</label>
                                </div>
                                <div class="col-md-3">
                                    <label for="start-date">Starts</label>
                                </div>
                                <div class="col-md-3">
                                    <label for="end-date">Ends</label>
                                </div>
                            </div>

                            <div class="tiers" id="date-tiers" data-tier-container="#date-tiers"
                                 data-tier-source="#date-tier-source" data-add-tier-trigger="#add-date-tier"
                                 data-tier-id-count="{{ count($event->dates)?:1 }}">
                                @forelse($event->dates()->orderBy('start_date', 'asc')->get() as $key => $date)
                                    @include('cms.events._tier', ['key' => $key, 'date' => $date])
                                @empty
                                    @include('cms.events._tier', ['key' => 0, 'date' => null])
                                @endforelse
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <label for="name">Content </label>

                        <div class="editor translation-field round-edges">
                            <textarea class="tinymce-editor form-control" name="content" id="content"
                          rows="10">{{ old('content', $event->content) }}</textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="status">
                            Status
                        </label>
                        <select id="status"
                                name="status"
                                class="form-control">
                            <option value="0" {{ old('status', $event->status) == "0" ? 'selected':'' }}>Inactive
                            </option>
                            <option value="1" {{ old('status', $event->status) == "1" ? 'selected':'' }}>Active</option>
                        </select>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="meta-container">
                    <div class="form-group">
                        <label for="title">Meta Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="meta_title"
                               name="meta_title"
                               placeholder="Meta Title"
                               value="{{ old('meta_title', $event->meta_title) }}"
                        >
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Keywords </label>

                        <div class="editor">
<textarea class="form-control translation-field" name="meta_keywords" id="meta_keywords" rows="3"
          placeholder="Meta Keywords">{{ old('meta_keywords', $event->meta_keywords) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Description </label>

                        <div class="editor">
<textarea class="form-control translation-field" name="meta_description" id="meta_description" rows="6"
          placeholder="Meta Description">{{ old('meta_description', $event->meta_description) }}</textarea>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $event->exists ? 'Save' : 'Create' }}</button>
        </div>

    </form>

    {{--Tier Source, Its not visible on the page rather its used to add more date fiels into the dates section--}}
    <div id="date-tier-source" class="tier-source">
        @include('cms.events._tier', ['key' => '{tierId}', 'date' => null])
    </div>

@endsection




