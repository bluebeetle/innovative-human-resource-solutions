@extends('cms.layouts.default')

@section('title', 'Users')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('cms.users.create') }}" class="btn btn-primary">Create New User</a>
        </div>
    </div>

@stop


@section('content')

    <table class="table table-hover" data-table="table" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>E-mail</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    <a href="{{ route('cms.users.edit', $user->id) }}">{{ $user->name }}</a>
                </td>
                <td>{{ $user->email }}</td>
                <td class="actions">
                    <a href="{{ route('cms.users.edit', $user->id) }}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="{{ route('cms.users.destroy', $user->id) }}" class="confirm-action">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $users->render() !!}
@endsection
