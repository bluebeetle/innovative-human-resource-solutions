@extends('cms.layouts.default')

@section('title', $user->exists ? 'Edit User : ' . $user->name : 'Create New User')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop


@section('content')

    <form action="{{$user->exists ? route('cms.users.update', $user->id) : route('cms.users.store')}}"
          method="post">

        @if($user->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name </label>
            <input type="text"
                   class="form-control"
                   id="name"
                   name="name"
                   placeholder="Name"
                   value="{{ old('name', $user->name) }}"
                   required>
        </div>

        <div class="form-group">
            <label for="email">Email </label>
            <input type="text"
                   class="form-control"
                   id="email"
                   name="email"
                   placeholder="Email"
                   value="{{ old('email', $user->email) }}"
                   required>
        </div>

        <div class="form-group">
            <label for="password">Password </label>
            <input type="password"
                   class="form-control"
                   id="password"
                   name="password"
                   placeholder="Password">
        </div>

        <div class="form-group">
            <label for="password-confirmation">Confirm Password </label>
            <input type="password"
                   class="form-control"
                   id="password-confirmation"
                   name="password_confirmation"
                   placeholder="Confirm Password">
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $user->exists ? 'Save' : 'Create' }}</button>
        </div>

    </form>


@endsection
