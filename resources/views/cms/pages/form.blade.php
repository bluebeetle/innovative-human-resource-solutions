@extends('cms.layouts.default')

@section('title', $page->exists ? 'Edit Page : '.$page->title : 'Create New Page')


@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')

    <form action="{{$page->exists ? route('cms.pages.update', $page->id) : route('cms.pages.store')}}"
          method="post">


        @if($page->exists)
            <input type="hidden" name="_method" value="PUT">
        @endif

        {{ csrf_field() }}

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs form-tablist" role="tablist">
                <li role="presentation" class="active"><a href="#content-container" aria-controls="content-container"
                                                          role="tab" data-toggle="tab">Content</a></li>
                <li role="presentation"><a href="#meta-container" aria-controls="meta-container" role="tab"
                                           data-toggle="tab">Meta</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="content-container">
                    <div class="form-group alias-field">
                        <label for="name">Name </label>
                        <input type="text"
                               class="form-control {{ $page->exists ? '':'make-slug' }} "
                               id="name"
                               name="name"
                               placeholder="Name"
                               value="{{ old('name', $page->name) }}"
                               required>
                        <span class="help-block">
                            {!!  $page->exists? "Please don't change, it might effect the function of some pages.<br/>" : ""!!}
                            Its only for reference & is not visible anywhere on the website.
                        </span>
                    </div>

                    <div class="form-group alias-field">
                        <label for="uri">URI
                        </label>

                        <input type="text"
                               class="form-control name-slug "
                               id="uri"
                               name="uri"
                               placeholder="URI"
                               value="{{ old('uri', $page->uri) }}"
                               required>
                    </div>

                    <div class="checkbox alias-field">
                        <label class="">
                            <input type="checkbox" name="alias" value="1"
                                   id="alias" {{old('alias', $page->alias)=="1"?"checked":""}} class="">
                            Alias of another page or url

                            <span class="help-block">
                            It will make this page alias to another page or link. <br/>
                            The link to target page will be entered as URI above.
                        </span>
                        </label>
                    </div>

                    <div class="form-group alias-field">
                        <label for="title">Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="title"
                               name="title"
                               placeholder="Title"
                               value="{{ old('title', $page->title) }}"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="template">Template</label>
                        <select id="template"
                                name="template"
                                class="form-control ">
                            @foreach($templates as $key => $template)
                                <option value="{{ $key }}"
                                        @if($key==old('template', $page->template?:'page'))
                                        selected
                                        @endif
                                >{{ $template }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group row alias-field">
                        <div class="col-md-12">
                            <label for="order">Order</label>
                        </div>
                        <div class="col-md-2">
                            <select id="order"
                                    name="order"
                                    class="form-control ">
                                @foreach(
                                    ['' => '', 'before' => 'Before', 'after' => 'After', 'childOf' => 'Child Of'] + ($page->exists ? ['root' => 'Root'] : [] )
                                    as $key => $value
                                )
                                    <option value="{{ $key }}"
                                    >{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-10">
                            <select id="orderPage"
                                    name="orderPage"
                                    class="form-control ">
                                <option value=""
                                >--</option>
                                @foreach(
                                $orderPages
                                as $p
                                )
                                    <option value="{{ $p->id }}"
                                    >{!! $p->present()->padded_title !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <label>Custom Fields</label>
                            <br/>
                            <br/>
                            <div class="tiers" id="custom-field-tiers" data-tier-container="#custom-field-tiers"
                                 data-tier-source="#custom-field-tier-source" data-add-tier-trigger="#add-custom-field-tier"
                                 data-tier-id-count="{{ count($page->customFields)?:1 }}">
                                @forelse($page->customFields as $key => $field)
                                    @include('cms.pages._tier', ['key' => $key, 'field' => $field])
                                @empty
                                    @include('cms.pages._tier', ['key' => 0, 'field' => null])
                                @endforelse
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Content </label>

                        <div class="editor translation-field round-edges">
                            <textarea class="tinymce-editor form-control" name="content" id="content"
                                      rows="10">{{ old('content', $page->content) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="courses">
                            Relevant Courses
                        </label>
                        <select id="courses"
                                name="courses[]"
                                class="form-control" multiple size="8">
                            <option value="">--</option>
                            @foreach($courses as $course)
                                <option value="{{ $course->id }}" {{ (in_array($course->id, $page->courses->pluck('id')->toArray()))?"selected":"" }}>{{ $course->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group alias-field">
                        <label for="navigation">Navigation </label>

                        <select id="navigation"
                                name="navigation"
                                class="form-control">
                                <option value="">--</option>
                                <option value="top"
                                        @if("top"==old('navigation', $page->navigation))
                                        selected
                                        @endif
                                >top</option>
                                <option value="secondary"
                                        @if("secondary"==old('navigation', $page->navigation))
                                        selected
                                        @endif
                                >secondary</option>
                                <option value="footer"
                                        @if("footer"==old('navigation', $page->navigation))
                                        selected
                                        @endif
                                >footer</option>
                        </select>

                    </div>

                    <div class="checkbox alias-field alias-field">
                        <label>
                            <input type="checkbox" name="hidden" value="1"
                                   id="hidden" {{old('hidden', $page->hidden)=="1"?"checked":""}}>
                            Hide page from navigation

                            <span class="help-block">
                            Checking this will hide the page from the navigation. Can only be applied to pages without children.
                        </span>
                        </label>
                    </div>

                    <div class="checkbox alias-field alias-field">
                        <label>
                            <input type="checkbox" name="not_searchable" value="1"
                                   id="not_searchable" {{old('not_searchable', $page->not_searchable)=="1"?"checked":""}}>
                            Hide from search

                            <span class="help-block">
                            Checking this will hide this page from site search.
                            </span>
                        </label>
                    </div>

                    <div class="form-group alias-field">
                        <label for="status">
                            Status
                        </label>
                        <select id="status"
                                name="status"
                                class="form-control">
                            <option value="0" {{ old('status', $page->status) == "0" ? 'selected':'' }}>Inactive
                            </option>
                            <option value="1" {{ old('status', $page->status) == "1" ? 'selected':'' }}>Active</option>
                        </select>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="meta-container">
                    <div class="form-group">
                        <label for="title">Meta Title </label>
                        <input type="text"
                               class="form-control translation-field"
                               id="meta_title"
                               name="meta_title"
                               placeholder="Meta Title"
                               value="{{ old('meta_title', $page->meta_title) }}"
                        >
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Keywords </label>

                        <div class="editor">
                            <textarea class="form-control translation-field" name="meta_keywords" id="meta_keywords" rows="3"
                                      placeholder="Meta Keywords">{{ old('meta_keywords', $page->meta_keywords) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Description </label>

                        <div class="editor">
                            <textarea class="form-control translation-field" name="meta_description" id="meta_description" rows="6"
                                      placeholder="Meta Description">{{ old('meta_description', $page->meta_description) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ $page->exists ? 'Save' : 'Create' }}</button>
        </div>


    </form>

    {{--Tier Source, Its not visible on the page rather its used to add more date fiels into the dates section--}}
    <div id="custom-field-tier-source" class="tier-source">
        @include('cms.pages._tier', ['key' => '{tierId}'])
    </div>


@endsection

