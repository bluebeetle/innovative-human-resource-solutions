@extends('cms.layouts.default')

@section('title', 'Pages')


@section('heading')

    <div class="row heading">
        <div class="col-md-10">
            <h3>@yield('title')</h3>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('cms.pages.create') }}" class="btn btn-primary">Create New Page</a>
        </div>
    </div>

@stop

@section('content')

    <table class="table table-hover" data-table="table" id="record-list">
        <thead>
        <tr>
            <th>Name</th>
            <th>URI</th>
            <th>Template</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @forelse($pages as $page)
                <tr class="{{ $page->hidden ? 'warning' : '' }}">
                    <td>
                        {{--{!! $page->linkToPaddedName(route('cms.pages.edit', $page->id)) !!}--}}
                        {!! $page->present()->padded_name !!}
                    </td>
                    <td><a href="{{ url($page->uri) }}" target="_blank">{{ $page->present()->pretty_uri }}</a></td>
                    <td>
                        {{ $page->template }}
                    </td>
                    <td class="actions">
                        <a href="{{ route('cms.pages.edit', $page->id) }}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="{{ route('cms.pages.destroy', $page->id) }}" class="confirm-action">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                        <a href="{{ route('cms.mediable.show', ['pages', $page->id]) }}">
                            <span class="glyphicon glyphicon-file"></span>
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" align="center">There are no pages.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
