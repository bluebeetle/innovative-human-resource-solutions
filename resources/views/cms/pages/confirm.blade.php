@extends('cms.layouts.default')

@section('title', 'Delete '.$page->title)

@section('heading')

    <div class="row heading">
        <div class="col-md-12">
            <h3>@yield('title')</h3>
        </div>
    </div>

@stop

@section('content')

    <form action="{{ route('cms.pages.destroy', $page->id) }}" method="post">

        {{ csrf_field() }}

        <input type="hidden" name="_method" value="DELETE">

        <div class="alert alert-danger">
            <strong>Warning!</strong> You are about to delete a page.
            <br/>
            This action cannot be undone. Are you sure you want to continue?
        </div>

        <input type="submit" class="btn btn-danger" value="Yes, delete !"/>
        &nbsp;&nbsp;&nbsp;
        <a href="{{ route('cms.pages.index') }}" class="btn btn-success">
            No, get me out of here!
        </a>
    </form>
@endsection
