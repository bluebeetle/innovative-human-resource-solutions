@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="page">

        {{--@include('inc.breadcrumbs', $breadcrumbs)--}}


        <div id="page-header">

            @if(isset($media['banner']))
                <div id="banner">

                    @if($media['banner'])
                        <img src="/{{ $media['banner'][0]['path'] }}"
                             alt="{{ $media['banner'][0]['caption'] ?? $page->title . ' banner' }}"/>
                    @endif

                    <div class="blur-bg-title">
                        <div class="bg" style="background-image: url('/{{ $media['banner'][0]['path'] }}')"></div>
                        <h1>
                            {{ $page->title }}
                        </h1>
                    </div>
                </div>
            @else
                <div id="shadow">
                </div>
            @endif

        </div>

        <div id="page-body">

            <div class="left-column">

                @if(!isset($media['banner']))
                    <h1 class="text-uppercase">
                        {{ $page->title }}
                    </h1>
                @endif

                {!! $page->content !!}

            </div>

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
