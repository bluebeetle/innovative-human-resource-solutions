<urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    @foreach($links as $link)
        <url>
            <loc>{{ url($link) }}</loc>
            <changefreq>weekly</changefreq>
        </url>
    @endforeach
</urlset>