<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($sitemaps as $sitemap)
        <sitemap>
            <loc>{{ url('/sitemap/' . $sitemap . '.xml') }}</loc>
            <lastmod>{{ gmdate("Y-m-d\TH:i:s\Z") }}</lastmod>
        </sitemap>
    @endforeach
</sitemapindex>