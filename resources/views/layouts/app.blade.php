<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title') | {{ Setting::get('siteName') }}</title>
    <link rel="stylesheet" href="/css/cms/styles.css" />

    @yield('css.header')

</head>
<body>


<div class="container">

    @yield('heading')

    @include('cms.inc.alerts')

    @yield('content')

</div>


<script src="/packages/tinymce/tinymce.min.js"></script>
<script src="/js/cms/scripts.js"></script>
@yield('scripts.footer')

</body>
</html>
