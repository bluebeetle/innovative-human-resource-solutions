<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{{ $locale }}"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="{{ $locale }}"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="{{ $locale }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ $locale }}"> <!--<![endif]-->

<head>

    @if (url(prefixUrl($page->uri)) === url('/ar'))
        <script type="text/javascript">
            window.location.href = "{{URL::to('/')}}"
        </script>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--<title>@yield('title') :: @lang('IHS')</title>--}}

    <title>{{ MetaTag::get('title') }}</title>

    {!! MetaTag::tag('description') !!}
    {!! MetaTag::tag('keywords') !!}
    {!! MetaTag::tag('image') !!}

    {!! MetaTag::canonical() !!}

    {!! MetaTag::openGraph() !!}

    {!! MetaTag::twitterCard() !!}

    {{--Set default share picture after custom section pictures--}}
    {!! MetaTag::tag('image', asset('images/social.jpg')) !!}

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="ec9oxyf_JdTRiijFjiuLmYZY6Hl2EPWeqJyVW66KNx8" />

    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link rel="apple-touch-icon" href="favicon.png">

    <link rel="stylesheet" href="{{ cdnAsset('/css/styles.css?v=23453') }}">
    <link rel="stylesheet" href="{{ cdnAsset('/css/ie10.css') }}">
    <!--[if IE]>
    <link rel="stylesheet" href="{{ cdnAsset('/css/ie.css') }}" />
    <![endif]-->

    {{--If the current language is other than default (Englis /en),
    Then include the styles in bellow if clause--}}
    @if($localePrefix !== "")
        <link rel="stylesheet" href="{{ cdnAsset('/css/styles-' . $localePrefix . '.css') }}">
    @endif
    <script>
        @if(env('GOOGLE_TAG_MANAGER_TRACKING_ID'))
        /* Google Tag Manager */
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','{{  env("GOOGLE_TAG_MANAGER_TRACKING_ID") }}');
        /* End Google Tag Manager */
        @endif
    </script>
</head>

<body dir="{{ $locale == 'ar' ? 'rtl' : 'ltr' }}">
@include('inc.acquiringNotification')
@if(env('GOOGLE_TAG_MANAGER_TRACKING_ID'))
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{  env('GOOGLE_TAG_MANAGER_TRACKING_ID') }}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@endif
<div id="fb-root"></div>
<script>
    @if(env('FACEBOOK_APP_ID'))

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId={{ env('FACEBOOK_APP_ID') }}";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    @endif
</script>

<!-- Header: START -->
<div class="header-container">
    <header class="">

        <a href="#" id="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </a>
        @if ($locale == 'ar')
            <div id="logo">
                <a href="/{{ $localePrefix }}/home"></a>
            </div>
            @else
            <div id="logo">
                <a href="/{{ $localePrefix }}"></a>
            </div>
        @endif


        <div id="search">
            <a href="#" id="search-trigger"  class="sprite-icon">@lang('front.Search')</a>
            <form action="{{ $localePrefix != '' ? '/' . $localePrefix : '' }}/search" id="search-form" class="cf">
                <input type="text" name="q" id="search-box"/>
                <input type="submit"/>
                <div class="result scroll" id="search-result">
                </div>
            </form>
        </div>

        <nav class="main-nav">
            <ul>
                @foreach($pages->where('navigation', 'top') as $thePage)
                    <li class="{{ activeIf($thePage->uri, $localePrefix) }}">
                        <a href="{{ url(prefixUrl($thePage->uri)) }}">{{ $thePage->title }}</a>
                    </li>
                @endforeach
            </ul>


            <ul class="language-switch">
                <li class="{{ $locale=='ar'?'active':'' }}"><a href="/ar/home">العربية</a></li>
                <li class="{{ $locale=='en'?'active':'' }}"><a href="/">Eng</a></li>
            </ul>

        </nav>

        <nav class="secondary-nav">
            <ul>
                @foreach($pages->where('navigation', 'secondary') as $thePage)
                    <li class="{{ activeIf($thePage->uri, $localePrefix) }}"><a href="{{ url(prefixUrl($thePage->uri)) }}">{{ $thePage->title }}</a></li>
                @endforeach
            </ul>
        </nav>

    </header>
</div>
<!-- Header: END -->


<!-- Content : START -->
<div class="main-container">
    @yield('content')
</div>

<!-- Content : END -->


<!-- Footer : START -->
<div class="footer-container">
    <footer class="">
        <nav>
            <ul>
                @foreach($pages->where('navigation', 'footer') as $thePage)
                    <li><a href="{{ url(prefixUrl($thePage->uri)) }}">{{ $thePage->title }}</a></li>
                @endforeach
            </ul>
        </nav>
        <div class="copyrights">
            <p>&copy; {{ date('Y') }} @lang('front.IHS'). @lang('front.All Rights Reserved.')</p>
        </div>
        <div class="developed-by">
            Web Design by <a href="http://www.bluebeetle.ae" target="_blank" title="Dubai Web Design Agency - Blue Beetle">Blue Beetle</a>
        </div>
    </footer>
</div>
<!-- Footer : END -->


<script src="{{ cdnAsset('/js/plugins.js') }}"></script>
<script src="{{ cdnAsset('/js/scripts.js') }}"></script>


<script>
    @if(env('GOOGLE_MAP_API_KEY'))
    $.gmap3({
        key : '{{ env('GOOGLE_MAP_API_KEY') }}'
    });
    @endif
</script>


@yield('scripts.footer')

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/120635.js"></script>
<!-- End of HubSpot Embed Code -->

</body>

</html>