<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{{ $locale }}"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="{{ $locale }}"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="{{ $locale }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ $locale }}"> <!--<![endif]-->

<head>
    @if (url(prefixUrl($page->uri)) === url('/ar'))
        <script type="text/javascript">
            window.location.href = "{{URL::to('/')}}"
        </script>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--<title>@yield('title') :: @lang('IHS')</title>--}}

    <title>{{ MetaTag::get('title') }}</title>

    {!! MetaTag::tag('description') !!}
    {!! MetaTag::tag('keywords') !!}
    {!! MetaTag::tag('image') !!}

    {!! MetaTag::canonical() !!}

    {!! MetaTag::openGraph() !!}

    {!! MetaTag::twitterCard() !!}

    {{--Set default share picture after custom section pictures--}}
    {!! MetaTag::tag('image', asset('images/social.jpg')) !!}

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="ec9oxyf_JdTRiijFjiuLmYZY6Hl2EPWeqJyVW66KNx8" />

    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link rel="apple-touch-icon" href="favicon.png">

    <link href="{{ cdnAsset('/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ cdnAsset('/css/ie10.css') }}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.0/css/iziModal.css" rel="stylesheet">


    <!--[if lt IE 9]><script src="/js/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="/js/respond.js"></script><![endif]-->

    {{--If the current language is other than default (Englis /en),
    Then include the styles in bellow if clause--}}
    @if($localePrefix !== "")
        <link rel="stylesheet" href="{{ cdnAsset('/css/styles-' . $localePrefix . '.css') }}">
    @endif
    <script>
        @if(env('GOOGLE_TAG_MANAGER_TRACKING_ID'))
        /* Google Tag Manager */
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','{{  env("GOOGLE_TAG_MANAGER_TRACKING_ID") }}');
        /* End Google Tag Manager */
        @endif
    </script>
</head>


<body  class="home layout_changer" dir="{{ $locale == 'ar' ? 'rtl' : 'ltr' }}">
@include('inc.acquiringNotification')
@if(env('GOOGLE_TAG_MANAGER_TRACKING_ID'))
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{  env('GOOGLE_TAG_MANAGER_TRACKING_ID') }}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
@endif
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId={{ env('FACEBOOK_APP_ID') }}";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<div class="page-wrapper body_wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style One-->

    <header class="main-header header-style-one">
        <div class="nav">

            <div class="main-nav">
                <a class="hamburger-nav"></a>
                <ul class="menu">
                    @foreach($pages->where('navigation', 'secondary') as $page)
                        <li class="{{ activeIf($page->uri, $localePrefix) }}"><a href="{{ url(prefixUrl($page->uri)) }}">{{ $page->title }}</a></li>
                    @endforeach
                    @foreach($pages->where('navigation', 'top') as $page)
                        <li class="{{ activeIf($page->uri, $localePrefix) }}"><a href="{{ url(prefixUrl($page->uri)) }}">{{ $page->title }}</a></li>
                    @endforeach
                        <li class="{{ $locale=='ar'?'active':'' }} lang"><a href="/ar/home">العربية</a></li>
                        <li class="{{ $locale=='en'?'active':'' }} lang"><a href="/">Eng</a></li>
                </ul>
            </div>
        </div>
        <!-- Header Top One-->
        <div class="header-top-one">
            <div class="auto-container">
                <div class="clearfix">

                    <!--Top Right-->
                    <div class="top-right">
                        <nav class="main-nav">
                            <ul class="language-switch">
                                <li class="{{ $locale=='ar'?'active':'' }}"><a href="/ar/home">العربية</a></li>
                                <li class="{{ $locale=='en'?'active':'' }}"><a href="/">Eng</a></li>
                            </ul>
                        </nav>
                        <nav class="secondary-nav">
                            <ul>
                                @foreach($pages->where('navigation', 'top') as $page)
                                    <li class="{{ activeIf($page->uri, $localePrefix) }}"><a href="{{ url(prefixUrl($page->uri)) }}">{{ $page->title }}</a></li>
                                @endforeach
                                <li>
                                    <a href="tel:+97143902778">
                                        <span class="fa fa-phone"></span>
                                        <span class="phone">+97143902778</span>
                                    </a>
                                </li>
                                <li>
                                    <!--Search Box-->
                                    <div class="search-box-outer">
                                        <div id="search">
                                            <a href="#" id="search-trigger"  class="sprite-icon">@lang('front.Search')</a>
                                            <form action="{{ $localePrefix != '' ? '/' . $localePrefix : '' }}/search" id="search-form" class="cf">
                                                <input type="text" name="q" id="search-box"/>
                                                <input type="submit"/>
                                                <div class="result scroll" id="search-result">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>

                </div>

            </div>
        </div><!-- Header Top One End -->


        <!-- Header Lower -->
        <div class="header-lower">
            <div class="main-box">
                <div class="auto-container">
                    <div class="outer-container clearfix">
                        <!--Logo Box-->
                        <div class="logo-box">
                            <div class="logo"><a href="/{{ $localePrefix }}"></a></div>
                        </div>

                        <!--Nav Outer-->
                        <div class="nav-outer clearfix">
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <ul class="navigation clearfix">
                                    @foreach($pages->where('navigation', 'secondary') as $page)
                                        <li class="{{ activeIf($page->uri, $localePrefix) }}"><a href="{{ url(prefixUrl($page->uri)) }}">{{ $page->title }}</a></li>
                                    @endforeach
                                </ul>
                            </nav><!-- Main Menu End-->
                        </div><!--Nav Outer End-->


                    </div>
                </div>
            </div>
        </div>

        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="/{{ $localePrefix }}"></a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <ul class="navigation clearfix">
                            @foreach($pages->where('navigation', 'top') as $page)
                                <li class="{{ activeIf($page->uri, $localePrefix) }}"><a href="{{ url(prefixUrl($page->uri)) }}">{{ $page->title }}</a></li>
                            @endforeach
                        </ul>
                    </nav><!-- Main Menu End-->

                </div>

            </div>
        </div><!--End Sticky Header-->

    </header>
    <!--End Main Header -->

    <!-- Content : START -->
    <div class="main-container">
        @yield('content')
    </div>
    <!-- Content : END -->

    <footer class="main-footer">

        <!--Footer Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-sm-12 col-xs-12 column">
                                <div class="footer-widget about-widget">
                                    @include('inc.socialBarWidget')
                                    {{--@include('inc.newsletterHomeWidget')--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!--Footer Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="footer-nav clearfix">
                            <ul class="pull-left clearfix">
                                @foreach($pages->where('navigation', 'footer') as $page)
                                    <li><a href="{{ url(prefixUrl($page->uri)) }}">{{ $page->title }}</a></li>
                                @endforeach

                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright-text pull-right">&copy; {{ date('Y') }} @lang('front.IHS'). @lang('front.All Rights Reserved.')</div>
                    </div>
                </div>
            </div>
        </div>

    </footer>

</div>
<!--End pagewrapper-->

<script src="{{ cdnAsset('/js/home.js') }}" defer></script>
<script src="{{ cdnAsset('/js/script.js') }}" defer></script>

</body>
@yield('scripts.footer')

</body>

</html>