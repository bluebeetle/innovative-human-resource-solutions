@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="page">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div id="page-header">

                <div id="banner">

                    @if($customFields->get('banner'))
                        <img class="lozad" data-src="{{ cdnAsset($customFields->get('banner')) }}"/>
                    @endif


                </div>

            @if($children->count())
                <div id="side-menu">
                    <ul>
                        @foreach($children as $childPage)
                            <li class="{{ activeIf($childPage->uri, $localePrefix) }}"><a
                                        href="{{ ($childPage->alias == '1')?addReferrerToCourseLink(url(prefixUrl($childPage->uri)),$page->id):url(prefixUrl($childPage->uri)) }} ">{{ $childPage->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>

        <div id="page-body">
            <h1 class="text-uppercase customHeading">
                {{ $page->title }}
            </h1>

            @if($customFields->get('rightColumn') == "yes")
                <div class="right-column">
                    <div id="side-menu">
                        <div class="right-widget-content">
                            <h4>{{$customFields->get('right-heading-title')}}</h4>
                            <img class="lozad" data-src="{{ cdnAsset($customFields->get('right-heading-logo')) }}" />
                            <p class="link">{!! $customFields->get('right-heading-link') !!}</p>
                        </div>

                        @include('inc.contactForm')
                    </div>
                </div>
            @endif
            {!! $page->content !!}


        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">    
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
