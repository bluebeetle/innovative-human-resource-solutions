@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="page">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div id="team-dropdown">

            <div class="dropdown">
                <a href="#">Explore
                    <i class="sprite-icon"></i>
                </a>
                <ul>
                    @foreach($children as $child)
                        <li><a href="{{ url(prefixUrl($child->uri)) }}">{{ $child->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div id="page-header">

            <div id="banner">

                @if(isset($media['banner']))
                    <img class="lozad" data-src="{{ cdnAsset('/' . $media['banner'][0]['path']) }}"
                         alt="{{ $media['banner'][0]['caption'] ?? $page->title . ' banner' }}"/>
                @endif

                <div class="blur-bg-title">

                    <div class="bg"
                         style="{{ isset($media['banner'])?"background-image: url('" . cdnAsset('/' . $media['banner'][0]['path']) ."');":"background-color:#CCC;" }}"></div>

                    <h1>
                        {{ $page->title }}
                    </h1>
                </div>



                <div class="cta-banners">
                    @foreach($children as $child)
                        <div class="cta">
                            <a href="{{ url(prefixUrl($child->uri)) }}">
                                @if(isset($child->mediaArray['thumb']))
                                    <img class="lozad" data-src="{{ imageThumb($child->mediaArray['thumb'][0]['path'], 210) }}"
                                         alt="{{ $child->title }}"/>
                                @endif
                                <div class="overlay">
                                    <h2 class="text-uppercase">{{ $child->title }}</h2>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>

        <div id="page-body">
            <div class="left-column">

                {!! $page->content !!}

                @foreach($page->media()->ofType('pdf')->get() as $file)
                    <a href="/downloadFile?path={{ $file->path }}&name={{ $file->title }}"
                       class="underline">{{ $file->title }}</a> <br>
                @endforeach

            </div>
        </div>

        <div class="cta-banners">
            @if(isset($post_cta->title))
            <div class="post-cta">
                <a href="{{ route('article', [$post_cta->id, $post_cta->slug], false) }}">
                    <h2 class="text-uppercase"><?php echo $post_cta->title; ?></h2>
                </a>
            </div>
            @endif
            @foreach($children as $child)
                <div class="cta">
                    <a href="{{ url(prefixUrl($child->uri)) }}">
                        @if(isset($child->mediaArray['thumb']))
                            <img class="lozad" data-src="{{ cdnAsset('/' . $child->mediaArray['thumb'][0]['path']) }}" alt="{{ $child->title }}"/>
                        @endif
                        <div class="overlay">
                            <h2 class="text-uppercase">{{ $child->title }}</h2>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
