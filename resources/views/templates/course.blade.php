@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="occasion">

        @include('inc.breadcrumbs', $breadcrumbs)


        <div id="occasion-header">

            <div id="banner">

                @if(isset($course->mediaArray['banner']))
                    <img class="lozad" data-src="{{ cdnAsset($course->mediaArray['banner'][0]['path']) }}"
                         alt="banner"/>
                @else
                    <div id="shadow">
                    </div>
                @endif

                <div class="occasion-title">

                    <div class="table">

                        <div class="bg"
                             style="{{ isset($course->mediaArray['banner'])?"background-image: url('/".$course->mediaArray['banner'][0]['path']."');":"background-color:#CCC;" }}"></div>

                        <div class="logo">
                            <img class="lozad" data-src="{{ cdnAsset($course->logo) }}"
                                 alt="{{ $course->title }} logo"/>
                        </div>

                        <div class="title">
                            @if($course->title)
                                <h1 class="h2">
                                    {!! $course->title !!}
                                </h1>
                            @endif
                        </div>

                        <div class="button">
                            <a href="{{ route('courseBooking', ['course', $course->id, $course->slug], false) }}">@lang('front.Book Now')</a>
                        </div>

                    </div>

                </div>
            </div>

        </div>

        <div id="occasion-body">

            <div class="schedule">

                <div class="cf">
                    <h3>@lang('front.Available Dates:')</h3>

                    <div class="dropdown-menu {{ count($years)==1?'hidden':'' }}">
                        <div class="placeholder-container ">
                            <div class="placeholder"></div>
                        </div>
                        <ul class="options">
                            @foreach($years as $key => $year)
                                <li class="{{ ($key==0)?'active':'' }}" data-slug-value="year-{{ $year }}"><a
                                            href="#">{{ $year }} @lang('front.Accreditation Calendar')</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="tables">
                    <div class="table">
                        @foreach($months as $key => $month)
                            <div class="month">
                                <div class="name">
                                    @lang('front.'.$month)
                                </div>
                            </div>
                        @endforeach
                    </div>

                    @foreach($years as $year)
                        <div id="year-{{ $year }}" class="hidden">
                            @if(isset($dates[$year]) && is_array($dates[$year]))
                                <div class="table">
                                    @foreach($dates[$year] as $key => $date)
                                        <div class="month span-{{ is_array($date)?$date['monthsDiff']+1:'1' }}">
                                            @if(is_array($date))
                                                <div class="dates">
                                                    {{ presentOccasionDates($date['start_date'], $date['end_date'], '-', null) }}
                                                    <br/>
                                                    {{ $date['location'] }}
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>


            @if($course->media()->ofType('pdf')->count())
                <div class="flyer">
                    @foreach($course->media()->ofType('pdf')->get() as $file)
                        <a href="/downloadFile?path={{ $file->path }}&name={{ $file->title }}"
                           class="underline">@lang('front.Download')
                            - {{ $file->title }}
                        </a> <br>
                    @endforeach
                </div>
            @endif

            <div class="left-column">
                {!! $course->content !!}
            </div>

            @if($course->pages->count())

                <div class="relevant-items">
                    <h3>
                        @lang('front.Relevant Assessments')
                    </h3>
                    <div class="cf">
                        @foreach($course->pages->take(2) as $assessment)
                            <div class="item">
                                <a href="{{ url(prefixUrl($assessment->uri)) }}">{{ $assessment->title }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>

            @endif

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
