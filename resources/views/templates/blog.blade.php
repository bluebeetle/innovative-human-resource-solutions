@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="article-list">

        @include('inc.breadcrumbs', $breadcrumbs)

        @foreach($posts as $post)
            <article>
                <a href="{{ route('article', [$post->id, $post->slug], false) }}">

                    @if(isset($post->mediaArray['banner']))
                        <img class="lozad" data-src="{{ cdnAsset($post->mediaArray['banner'][0]['path']) }}" alt=""/>
                    @endif

                    <div class="overlay">
                        @if($post->title)
                            <h2>{!! $post->title !!}</h2>
                        @endif

                        <div class="text-center">
                            {{--@foreach($post->categories as $category)
                                <span class="category {{ $category->slug }}">
                                {{ $category->title }}
                                </span>
                            @endforeach--}}
                            <span class="date">
                        {{ $post->published_at->diffForHumans() }}
                    </span>
                        </div>
                    </div>
                </a>
            </article>
        @endforeach

        @if($posts->lastPage() > 1)
            <div class="pagination page-{{ ($posts->currentPage() == $posts->lastPage())?'last':$posts->currentPage() }}">

                @if($posts->currentPage() > 1 && $posts->currentPage() != $posts->lastPage())
                    <a class="prev" href="{{ $posts->previousPageUrl() }}">
                        @lang('Prev')
                    </a>
                @endif

                <ul>
                    @for($p=1; $p<=$posts->lastPage(); $p++)
                        <li class="{{ ($posts->currentPage()==$p)?'active':'' }}"><a
                                    href="{{ ($p==1) ? route('news&Events'):$posts->url($p) }}">{{ $p }}</a></li>
                    @endfor
                </ul>

                @if($posts->currentPage() != $posts->lastPage())
                    <a class="next" href="{{ $posts->nextPageUrl() }}">
                        @lang('Next')
                    </a>
                @endif

                @if($posts->currentPage() == $posts->lastPage())
                    <a class="prev" href="{{ $posts->previousPageUrl() }}">
                        @lang('Prev')
                    </a>
                @endif

            </div>
        @endif

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>
@stop
