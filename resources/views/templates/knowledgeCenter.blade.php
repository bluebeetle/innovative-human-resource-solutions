@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="knowledge-center">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div class="cta-banners">
            @foreach($events as $k => $event)
            {{-- {{ $k == 0 ? 'blue':'purple'}} --}}
                <div class="cta azure-white">
                    <a href="{{ route('event', [$event->id, $event->slug], false) }}">

                        {{-- @if(isset($event->mediaArray['banner']))
                            <img class="lozad" data-src="{{ cdnAsset($event->mediaArray['banner'][0]['path']) }}" alt="{{ $event->title }}"/>
                        @endif --}}

                        <div class="overlay">
                            <h2>{{ $event->title }}</h2>
                            {{-- {{ $k == 0 ? 'purple':'blue'}} --}}
                            <span class="category events ">
                                @lang('front.Event')
                            </span>
                        </div>
                    </a>
                </div>
            @endforeach

            <div class="cta blue">
                <a href="{{ route('casestudies', [], false) }}">
                    {{-- <img class="lozad" data-src="{{ cdnAsset('images/casestudies-cta-banner.jpg') }}" alt="Case Studies"/> --}}

                    <div class="overlay">
                        <h2>@lang('front.Local Case Studies')</h2>
                    </div>
                </a>
            </div>

            <div class="cta purple">
                <a href="https://www.psionline.com/en-gb/customer-stories/" target="_blank">
                    {{-- <img class="lozad" data-src="{{ cdnAsset('images/casestudies-cta-banner.jpg') }}" alt="Case Studies"/> --}}

                    <div class="overlay">
                        <h2>@lang('front.Global Case Studies')</h2>
                    </div>
                </a>
            </div>

            <div class="cta blue">
                <a href="https://www.psionline.com/en-gb/resources/" target="_blank">
                    {{-- <img class="lozad" data-src="{{ cdnAsset('images/casestudies-cta-banner.jpg') }}" alt="Case Studies"/> --}}

                    <div class="overlay">
                        <h2>@lang('front.Resources')</h2>
                    </div>
                </a>
            </div>

            <div class="cta purple">
                <a href="https://blog.psionline.com/talent" target="_blank">
                    {{-- <img class="lozad" data-src="{{ cdnAsset('images/casestudies-cta-banner.jpg') }}" alt="Case Studies"/> --}}

                    <div class="overlay">
                        <h2>@lang('front.Blog')</h2>
                    </div>
                </a>
            </div>

            {{-- <div class="cta news">
                <a href="{{ route('news&Events', [], false) }}">

                    @if(isset($latestPost->mediaArray['banner']))
                        <img class="lozad" data-src="{{ cdnAsset($latestPost->mediaArray['banner'][0]['path']) }}" alt="{{ $latestPost->title }}"/>
                    @endif

                    <div class="overlay">
                        <h4 class="text-uppercase">@lang('front.Latest News & Events')</h4>
                        @if($latestPost->title)
                        <h3>{{ $latestPost->title }}</h3>
                        @endif
                    </div>
                </a>
            </div> --}}

            

        </div>

        {{-- <div id="knowledge-center-body">

            @if(isset($sortedMediaArray['']))

                <h2 class="text-uppercase">@lang('front.Photo Gallery')</h2>

                <div id="photo-gallery" class="masonry" data-item-selector=".photo">
                    @foreach($sortedMediaArray[''] as $key => $photo)

                        <div class="photo {{ $column = $galleryColumns[($key>5)?($key%6):$key] }}">
                            <img class="load-lazy" data-src="{{ imageThumb($photo['path'], $column=='half'?625:300) }}"
                                 alt="{{ $photo['caption'] ?? $page->title.' gallery' }}"/>
                        </div>

                    @endforeach
                </div>
            @endif

        </div> --}}

    </div>
</div>

    @include('inc.newsletterWidget')
    
<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>
@stop
