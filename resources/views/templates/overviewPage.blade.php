@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="page">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div id="page-header">

            @if(isset($media['banner']))
            <div id="banner">

                @if($media['banner'])
                <img class="lozad" data-src="{{ cdnAsset($media['banner'][0]['path']) }}"
                    alt="{{ $media['banner'][0]['caption'] ?? $page->title . ' banner' }}" />
                @endif

                <div class="blur-bg-title">
                    <div class="bg lozad" data-background="{{ cdnAsset($media['banner'][0]['path']) }}"></div>
                    <h1>
                        {{ $page->title }}
                    </h1>
                </div>
            </div>
            @else
            <div id="shadow">
            </div>
            @endif

            @if($children->count())
            <div id="side-menu" class="has-{{ $children->count() }}-items">
                <ul>
                    @foreach($children as $childPage)
                    <li class="{{ activeIf($childPage->uri, $localePrefix) }}"><a
                            href="{{ ($childPage->alias == '1')?addReferrerToCourseLink(url(prefixUrl($childPage->uri)),$page->id):url(prefixUrl($childPage->uri)) }}">{{ $childPage->title }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif

        </div>

        <div id="page-body">
            <div class="right-column">
            </div>
            <div class="left-column">

                @if(!isset($media['banner']))
                <h1 class="text-uppercase">
                    {{ $page->title }}
                </h1>
                @endif

                {!! $page->content !!}
            </div>

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop