@extends('layouts.home')

@section('title', $page->title)

@section('content')

<!--Main Slider-->
<section class="main-slider" data-start-height="900" data-slide-overlay="yes">

    <div class="youtube-video embed-responsive embed-responsive-16by9"
        style="background-image: url('/uploads/images/f62cb6ba52b8b58ce5ef6af59e18190320c0ac03.png');background-size: cover;">
        <div class="video" data-video-id="{{ $customFields->get('banner-video') }}" data-mute="1" data-loop="1"
            data-autoplay="1">
        </div>
        <div class="overlay"></div>
    </div>
    <div class="main-heading">
        <h2>{!! $customFields->get('banner-cta-text') !!}</h2>
    </div>
    <div class="main-call-out">
        <div class="tp-caption sfb sfb tp-resizeme start">
            <a href="{!! $customFields->get('banner-cta-button-link-1')  !!}" class="theme-btn btn-style-one">{!!
                $customFields->get('banner-cta-button-text-1') !!}</a> &ensp;&ensp;
            <a href="{!! $customFields->get('banner-cta-button-link-2')  !!}" class="theme-btn btn-style-two">{!!
                $customFields->get('banner-cta-button-text-2') !!}</a>
        </div>
    </div>
    <div class="location">
        @include('inc.locationsHomeWidget')
    </div>
</section>

<!--Default Section-->
<section class="default-section">
    <div class="auto-container">
        {!! $page->content !!}
    </div>
</section>

<section class="clients-section">
    <div class="auto-container">
        <div class="clearfix">
            <div class="sec-title centered">
                <h2>A Few of Our Inspired Clients</h2>
            </div>

            <div id="client-slider">
                @foreach($clients as $client)
                <div class="item">
                    <a>
                        <img class="lozad" data-src="{{ imageThumb($client->logo, 320) }}" alt="{{ $client->title }}" />
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="default-section-one">
    <div class="auto-container">
        <!--Title-->
        <div class="clearfix">
            <div class="sec-title centered">
                <h2>Globally Recognised Products and Accreditation Programmes</h2>
            </div>

            <div id="logo-slider">
                @foreach($logoSliders as $logoSlider)
                <div class="item">
                    <a href="{{ $logoSlider->link }}">
                        <img class="lozad" data-src="{{ imageThumb($logoSlider->logo, 180) }}"
                            alt="{{ $logoSlider->name }}" />
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@include('inc.newsletterWidget')

<section class="testimonial-bottom-section">
    <div class="auto-container">
        <div class="clearfix">
            <div class="sec-title centered">
                <h2>{{ $customFields->get('testimonial-heading-bottom') }}</h2>
                <p>{{ $customFields->get('testimonial-description-bottom') }}</p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-12 centered">
                    <div class="col-md-8 col-sm-12 col-xs-12 center-block">
                        <div id="testi-slider">
                            @foreach($testimonials as $testimonial)
                            <div class="item">
                                <div class="testimonial-wrap">
                                    <div class="testimonial-text">{!! $testimonial->content !!}</div>
                                    <div class="testimonial-author">{{ $testimonial->name }}</div>
                                    <div class="testimonial-company">{{ $testimonial->title }}</div>
                                </div>
                                <div class="author-image">
                                    <img class="lozad" data-src="{{ imageThumb($testimonial->logo, 180) }}"
                                        alt="{{ $testimonial->name }}" />
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="testimonial-video col-md-6 col-sm-6 col-xs-12">

                    <div class="youtube-video embed-responsive embed-responsive-16by9">
                        <div class="video" data-video-id="{{ $customFields->get('testimonial-video-bottom') }}"
                            data-mute="1" data-loop="1" data-autoplay="0">
                        </div>
                        <div class="overlay"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="awards-section">
    <div class="auto-container">
        <div class="clearfix">
            <div class="sec-title centered">
                <h2>{!! $customFields->get('awards-heading') !!}</h2>
                <p>{!! $customFields->get('awards-description') !!}</p>
                <div id="awards-wrap" class="col-md-10 col-sm-12 col-xs-12 center-block">
                    @foreach($awards as $award)
                    <div class="item col-md-4 col-sm-4 col-xs-12">
                        <a href="#">
                            <img class="lozad" data-src="{{ imageThumb($award->logo, 320) }}"
                                alt="{{ $award->title }}" />
                        </a>
                        <p class="award-title">{{ $award->title }}</p>
                        {!! $award->content !!}
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section class="callback-section"
    style="background: url('{{ $customFields->get('callback-bg-image') }}') no-repeat right bottom;">
    <div class="auto-container">
        <div class="clearfix">
            <div class="sec-title centered">
                <h2>{{ $customFields->get('callback-heading') }}</h2>
                <p>{{ $customFields->get('callback-description') }}</p>
                <a href="{{ $customFields->get('callback-btn-link') }}"
                    class="btn">{{ $customFields->get('callback-btn-text') }}</a>
            </div>
        </div>
    </div>
</section>



@if(isset($media['popup-mobile']) && isset($media['popup-desktop']) && $localePrefix != "en")
<div class="modal fade" id="home-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <a href="#" class="close"></a>
                <a href="<?php echo $customFields->get('popup-link')?>" target="_blank">
                    <picture>
                        <source media="(max-width:700px)" src="{{ cdnAsset('/' . $media['popup-mobile'][0]['path']) }}">
                        <img src="{{ cdnAsset('/' . $media['popup-desktop'][0]['path']) }}" alt="Popup">
                    </picture>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

@stop