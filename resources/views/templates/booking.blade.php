@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="booking">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div id="shadow">
        </div>

        <div id="booking-body">
            <div class="read-more-box">
            <div class="logo">
                <img class="lozad" data-src="{{ cdnAsset($occasion->logo) }}" alt="{{ $occasion->title }}"/>
            </div>
                <div class="details">
                    <h1 class="h2">{{ $occasion->title }}</h1>

                    {!! $occasion->content !!}

                    <div class="cf">

                        @foreach($occasion->media()->ofType('pdf')->get() as $file)
                            <a href="/downloadFile?path={{ $file->path }}&name={{ $file->title }}" class="flyer pdf">
                                <span class="sprite-icon"></span>
                                @lang('front.Download') - {{ $file->title }}
                            </a>
                        @endforeach

                        @if($type == 'course')
                            <a href="{{ route('calendar', [], false) }}" class="button">@lang('front.Back to Calendar')</a>
                        @endif

                    </div>

                </div>
                <p class="read-more"><a href="#" class="button">Read More</a></p>
            </div>

            @if(count($errors))
                <div class="alert alert-danger">
                    @if($errors->has('bookingError'))
                        @lang('front.Your booking is not successful, Pleas try again !')
                    @else
                        @lang('front.Please fill in the required fields')
                    @endif
                </div>
            @endif

            @include('inc.' . $type . 'BookingForm')


        </div>
    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
