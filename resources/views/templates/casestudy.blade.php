@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="page">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div id="page-header">

            @if(isset($casestudy->mediaArray['banner']))
                <div id="banner">
                    <img class="lozad" data-src="{{ cdnAsset($casestudy->mediaArray['banner'][0]['path']) }}" alt="banner"/>
                </div>
            @else
                <div id="shadow">
                </div>
            @endif

        </div>

        <div id="page-body" class="article-body">

            <div class="left-column">
                <h1>
                    {!! $casestudy->title !!}
                </h1>

                <div class="sub-title">{{ $casestudy->sector->title }} / {{ $casestudy->category->title }}</div>

                {!! $casestudy->content !!}
            </div>

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
