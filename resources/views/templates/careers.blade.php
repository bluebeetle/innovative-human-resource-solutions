@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="careers">

        @include('inc.breadcrumbs', $breadcrumbs)


        <div id="shadow">
        </div>


        <div id="careers-body" class="show-job">

            <div id="careers-slider">
                <div class="overview-content">

                    <div class="right-column">
                        <div id="side-menu">
                            <h4 class="text-uppercase">@lang('front.Open Positions')</h4>
                            <ul>
                                @foreach($jobs as $job)
                                    <li><a href="#job-{{ $job->id }}"
                                           class="{{ Str::slug($job->name) }}">{{ $job->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <a href="#" class="close"></a>
                    </div>

                    <div class="left-column">
                        <div class="overview">
                            <h1>{{ $page->title }}</h1>

                            {!! $page->content !!}
                        </div>
                    </div>

                </div>
                <div class="jobs">
                    @foreach($jobs as $job)
                        <div class="job content-container" id="job-{{ $job->id }}">
                            @if($job->title)
                                <h1>
                                    {{ $job->title }}
                                </h1>
                            @endif

                            {!! $job->content !!}

                            @if($job->apply_link)
                                <a href="{{ $job->apply_link }}" target="_blank" class="apply">@lang('front.Apply')</a>
                            @endif

                        </div>
                    @endforeach

                </div>
            </div>

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
