@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="team">

        <div id="shadow">
        </div>

        <div id="team-dropdown">
            <a href="{{ route('aboutPSIMiddleEast') }}" class="back">
                <i class="sprite-icon"></i>
            </a>

            <div class="dropdown">
                <a href="#">{{ $page->title }}
                    <i class="sprite-icon"></i>
                </a>
                <ul>
                    @foreach($siblings as $sibling)
                    <li><a href="{{ url(prefixUrl($sibling->uri)) }}">{{ $sibling->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="cf"></div>
        <div id="team-body">
            <div class="left-column">
                <h1>
                    {{ $page->title }}
                </h1>

                {!! $page->content !!}

            </div>

            <div id="team-slider">
                <div class="thumbs masonry" data-item-selecto=".thumb">

                    @foreach($employees as $employee)
                    <div class="thumb">
                        <a href="#" class="{{ Str::slug($employee->name) }}">
                            <img class="load-lazy" data-src="{{ imageThumb($employee->dp, 140) }}"
                                alt="{{ $employee->title }} picture" />
                        </a>
                    </div>
                    @endforeach

                </div>

                <div class="details">
                    @foreach($employees as $employee)
                    <div class="detail">
                        <img class="lozad" data-src="{{ imageThumb($employee->dp, 566) }}"
                            alt="{{ $employee->title }} picture" />

                        <h2>{{ $employee->title }}</h2>

                        <h3>{{ $employee->designation }}</h3>

                        <div class="content-container">
                            {!! $employee->content !!}
                        </div>

                        <ul class="social-handles">
                            @if($employee->facebook)
                            <li><a href="{{ $employee->facebook }}" class="facebook"><i class="sprite-icon"></i></a>
                            </li>
                            @endif
                            @if($employee->twitter)
                            <li><a href="{{ $employee->twitter }}" class="twitter"><i class="sprite-icon"></i></a></li>
                            @endif
                            @if($employee->linkedin)
                            <li><a href="{{ $employee->linkedin }}" class="linkedin"><i class="sprite-icon"></i></a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop