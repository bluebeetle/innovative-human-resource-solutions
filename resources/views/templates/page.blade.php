@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">

    <div id="page">

        @include('inc.breadcrumbs', $breadcrumbs)


        <div id="page-header">

            @if(isset($media['banner']))
            <div id="banner">

                @if($media['banner'])
                <img class="lozad" data-src="{{ cdnAsset($media['banner'][0]['path']) }}"
                    alt="{{ $media['banner'][0]['caption'] ?? $page->title . ' banner' }}" />
                @endif

                <div class="blur-bg-title">
                    <div class="bg lozad" data-background="{{ cdnAsset($media['banner'][0]['path']) }}"></div>
                    <h1>
                        {{ $page->title }}
                    </h1>
                </div>
            </div>
            @else
            <div id="shadow">
            </div>
            @endif

        </div>

        <div id="page-body">

            @if($siblings->count())
            <div class="right-column">
                <div id="side-menu">
                    <ul>
                        @foreach($siblings as $siblingPage)
                        <li class="{{ activeIf($siblingPage->uri, $localePrefix) }}"><a
                                href="{{ ($siblingPage->alias == '1')?addReferrerToCourseLink(url(prefixUrl($siblingPage->uri)), $page->id):url(prefixUrl($siblingPage->uri)) }}">{{ $siblingPage->title }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif

            <div class="left-column">

                @if(!isset($media['banner']))
                <h1 class="text-uppercase">
                    {{ $page->title }}
                </h1>
                @endif

                {!! $page->content !!}

                @foreach($page->children as $child)
                <div class="sub-page content-container">

                    @if($child->title)
                    <h2>{{ $child->title }}</h2>
                    @endif
                    {!! $child->content !!}

                    @include('inc.tabsWrapper', ['page' => $child])


                    @if($child->children->count())

                    @foreach($child->children as $grandChild)

                    <div class="sub-page content-container">

                        @if($grandChild->title)
                        <h2>{{ $grandChild->title }}</h2>
                        @endif
                        {!! $grandChild->content !!}

                        @include('inc.tabsWrapper', ['page' => $grandChild])

                    </div>

                    @endforeach

                    @endif
                </div>
                @endforeach
            </div>

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop