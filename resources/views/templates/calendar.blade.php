@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="calendar">
        <div class="headers">
            <div class="name">
                <div class="dropdown-menu year-{{ count($years) }}">
                    <div class="placeholder-container ">
                        <div class="placeholder"></div>
                    </div>

                    <ul class="options">
                        @foreach($years as $key => $year)
                            <li class="{{ ($key==0)?'active':'' }}" data-slug-value="year-{{ $year }}"><a href="#">{{ $year }} @lang('front.Accreditation Calendar')</a></li>
                        @endforeach
                    </ul>
                </div>

            </div>
            <div class="months">
                @foreach($months as $key => $month)
                    <div class="month">
                        @lang('front.'.$month)
                    </div>
                @endforeach
            </div>
        </div>
        @foreach($years as $year)

            <div id="year-{{ $year }}" class="hidden">

                @foreach($courses as $course)

                    @if(isset($dates[$course->id][$year]) && is_array($dates[$course->id][$year]))

                        <div class="row">
                        <div class="name">
                            <a href="{{ route('course', [$course->id, $course->slug], false) }}">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <img class="lozad" data-src="{{ cdnAsset($course->logo) }}" alt="{{ $course->title }}"/>
                                    </div>
                                    <div class="media-body media-middle">
                                        {{ $course->title }}
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="close"></a>
                        </div>
                        <div class="scroll-wrapper">
                            <div class="scroll">
                                <div class="months">
                                    @foreach($dates[$course->id][$year] as $key => $date)
                                        <div class="month span-{{ is_array($date)?$date['monthsDiff']+1:'1' }}">
                                            @if(is_array($date))
                                                <a href="{{ route('course', [$course->id, $course->slug], false) }}">
                                                    {{ presentOccasionDates($date['start_date'], $date['end_date'], '-', null) }}
                                                    <br/>
                                                    {{ $date['location'] }}
                                                </a>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('course', [$course->id, $course->slug], false) }}" class="more-details">@lang('front.More Details')</a>
                    </div>

                        <div class="expand">
                        <div class="logo">
                            <img class="lozad" data-src="{{ cdnAsset($course->logo) }}" alt="{{ $course->title }}"/>
                        </div>
                        <div class="details">
                            @if($course->title)
                                <h3>{{ $course->title }}</h3>
                            @endif

                            {!! $course->content !!}

                            <div class="cf">
                                <a href="{{ route('courseBooking', ['course', $course->id, $course->slug], false) }}" class="button">@lang('front.Book Now')</a>

                                @foreach($course->media()->ofType('pdf')->get() as $file)
                                    <a href="/downloadFile?path={{ $file->path }}&name={{ $file->title }}" class="flyer pdf">
                                        <span class="sprite-icon"></span>
                                        @lang('front.Download') - {{ $file->title }}
                                    </a>
                                @endforeach

                            </div>
                        </div>
                    </div>

                    @endif

                @endforeach

            </div>

        @endforeach
    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
