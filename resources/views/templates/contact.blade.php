@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="contact">

        @include('inc.breadcrumbs', $breadcrumbs)

            <div id="contact-form" class="{{ (count($errors) || Request::get('area_of_interest_intl')) ? 'show-form':'' }}">
            <div class="headoffice">
                <h3>@lang('front.To Speak With Us Now!')</h3>

                <div class="phone">
                    <i></i>
                    {{ ($locations->where('head_office', '1')->count()) ? $locations->where('head_office', '1')->first()->phone : '' }}
                </div>
                <h4>
                    @lang('front.Head office in Dubai')
                </h4>
            </div>

            <a href="#" id="" class="form-trigger">@lang('front.Contact Us')</a>

            @if(count($errors))
                <div class="alert alert-danger">
                    @if($errors->has('submissionError'))
                        @lang('front.Your request has not been successful, Pleas try again !')
                    @else
                        @lang('front.Please fill in the required fields')
                    @endif
                </div>
            @endif

            {{--<form action="{{ route('submitContact', [], false) }}" method="post" id="contactForm">
                {{ csrf_field() }}
                <input type="text" name="firstName" placeholder="@lang('front.First Name*')" value="{{ old('firstName') }}" class="{{ $errors->has('firstName') ? 'error' :  ''}}"/>
                <input type="text" name="lastName" placeholder="@lang('front.Last Name*')" value="{{ old('lastName') }}" class="{{ $errors->has('lastName') ? 'error' :  ''}}"/>
                <input type="email" name="email" placeholder="@lang('front.Your Email*')" value="{{ old('email') }}" class="{{ $errors->has('email') ? 'error' :  ''}}"/>
                <input type="tel" name="mobile" placeholder="@lang('front.Telephone Number...')" value="{{ old('mobile') }}"/>
                <input type="text" name="city" placeholder="@lang('front.City')" value="{{ old('city') }}"/>
                <input type="text" name="company" placeholder="@lang('front.Company')" value="{{ old('company') }}"/>
                --}}{{--<input type="text" name="hearFrom" placeholder="@lang('front.Where did you hear about us?')"/>--}}{{--
                <textarea name="message" id="" cols="30" rows="10"
                          placeholder="@lang('front.Type your message here.*')" class="{{ $errors->has('message') ? 'error' :  ''}}">{{ old('message') }}</textarea>
                --}}{{--<input type="submit" value="@lang('front.send')"/>--}}{{--

                <button class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}" data-callback='onSubmit'>@lang('front.send')</button>
            </form>--}}


            <div class="quote-form-embed w-embed w-script">
                <!--[if lte IE 8]>
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                <![endif]-->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                        portalId: "120635",
                        formId: "{{ $locale == 'en' ? '055fe9ed-7c78-42e8-9ce9-ac5f0cfec6bf':'50599570-2a2c-4f92-8d4c-346ae662da39' }}"
                    });
                </script>


            </div>

        </div>

        <div id="our-locations">
            <h4>@lang('front.We are present in...')</h4>

            <div class="locations">
                <ul>
                    @foreach($locations as $location)
                        <li class="{{ $location->head_office?'active':'' }}">{{ $location->title }}</li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div id="contact-map">

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop

@section('scripts.footer')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>

        function onSubmit(token) {
            document.getElementById("contactForm").submit();
        }

        var markers = [
                @foreach($locations as $location)
                    {
                'title': '{{ $location->title }}',
                'position': [{{ $location->coordinates }}]
            },
            @endforeach
    ]
    </script>
@stop
