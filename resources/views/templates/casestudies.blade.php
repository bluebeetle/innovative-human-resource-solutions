@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="article-list">

        @include('inc.breadcrumbs', $breadcrumbs)

        @foreach($casestudies as $casestudy)
            <article>
                <a href="{{ route('casestudy', [$casestudy->id, $casestudy->slug], false) }}">

                    @if(isset($casestudy->mediaArray['banner']))
                        <img class="lozad" data-src="{{ cdnAsset($casestudy->mediaArray['banner'][0]['path']) }}" alt=""/>
                    @endif

                    <div class="overlay">
                        <h2>{!! $casestudy->title !!}</h2>

                        <div class="sub-title">
                         {{ $casestudy->sector->title }}   / {{ $casestudy->category->title }}
                        </div>
                    </div>
                </a>
            </article>
        @endforeach

        @if($casestudies->lastPage() > 1)
            <div class="pagination page-{{ ($casestudies->currentPage() == $casestudies->lastPage())?'last':$casestudies->currentPage() }}">

                @if($casestudies->currentPage() > 1 && $casestudies->currentPage() != $casestudies->lastPage())
                    <a class="prev" href="{{ $casestudies->previousPageUrl() }}">
                        @lang('Prev')
                    </a>
                @endif

                <ul>
                    @for($p=1; $p<=$casestudies->lastPage(); $p++)
                        <li class="{{ ($casestudies->currentPage()==$p)?'active':'' }}"><a
                                    href="{{ $casestudies->url($p) }}">{{ $p }}</a></li>
                    @endfor
                </ul>

                @if($casestudies->currentPage() != $casestudies->lastPage())
                    <a class="next" href="{{ $casestudies->nextPageUrl() }}">
                        @lang('Next')
                    </a>
                @endif

                @if($casestudies->currentPage() == $casestudies->lastPage())
                    <a class="prev" href="{{ $casestudies->previousPageUrl() }}">
                        @lang('Prev')
                    </a>
                @endif

            </div>
        @endif

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
