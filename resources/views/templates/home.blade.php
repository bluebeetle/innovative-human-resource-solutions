@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="hero-wrapper">
        <div class="youtube-video embed-responsive embed-responsive-16by9" style="background-image: url('{{ cdnAsset('/' . $media['banner'][0]['path']) }}');">
            <div class="video" data-video-id="{{ $customFields->get('banner-video') }}" data-mute="1" data-loop="1"
                 data-autoplay="1">
            </div>
            <div class="overlay"></div>
        </div>

        <div class="cta">
            <p>
                {!! $customFields->get('banner-cta-text')?:"" !!}
            </p>
            <a href="{{ $customFields->get('banner-cta-button-link')?:"" }}">{!! $customFields->get('banner-cta-button-text')?:"" !!}</a>
        </div>

        <div id="upcoming-occasion-tabs">
            <ul class="tabs cf" data-tabgroup="upcoming-occasions">
                <li class="active"><a href="#upcoming-events">@lang('front.Upcoming Events')</a></li>
                <li><a href="#upcoming-programs">@lang('front.Upcoming Programmes')</a></li>
            </ul>
            <div id="upcoming-occasions" class="tabgroup">
                <div id="upcoming-events">

                    @foreach($events as $event)
                        <div class="occasion">
                            <div class="name">
                                <a href="{{ route('event', [$event->get('id'), $event->get('slug')], false) }}" title="{{ $event->get('title') }}">{{ $event->get('title') }}</a>
                            </div>
                            <div class="date">
                                {{ $event->get('dates') }}
                            </div>
                        </div>
                    @endforeach

                </div>
                <div id="upcoming-programs" class="programs">

                    @foreach($courses as $course)
                        <div class="occasion">
                            <div class="name">
                                <a href="{{ route('course', [$course->get('id'), $course->get('slug')], false) }}" title="{{ $course->get('title') }}">{{ $course->get('title') }}</a>
                            </div>
                            <div class="date">
                                {{ $course->get('dates') }}
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

    <div id="latest-news">
        <div class="cf">
            <h4>@lang('front.Latest news')</h4>
            <a href="{{ route('news&Events', [], false) }}" class="all">@lang('front.All News')</a>
        </div>
        <a href="{{route('article', [$latestNews->id, $latestNews->slug], false)}}" class="news">
            {!! $latestNews->title !!}
        </a>

    </div>

    <div id="logo-slider">
        @foreach($logoSliders as $logoSlider)
            <div class="item">
                <a href="{{ $logoSlider->link }}">
                    <img class="lozad" data-src="{{ imageThumb($logoSlider->logo, 180) }}" alt="{{ $logoSlider->name }}"/>
                </a>
            </div>
        @endforeach
    </div>

</div>
    @include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>
@stop
