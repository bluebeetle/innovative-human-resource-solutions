@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    @if($post->categories[0]->name == 'Events')
        @include('inc.eventArticle')
    @else
        @include('inc.newsArticle')
    @endif
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>

@stop
