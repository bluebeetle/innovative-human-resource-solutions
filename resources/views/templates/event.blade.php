@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="occasion">

        @include('inc.breadcrumbs', $breadcrumbs)

        <div id="occasion-header">

            <div id="banner">

                @if(isset($sortedMediaArray['banner']))
                    <img class="lozad" data-src="{{ cdnAsset($sortedMediaArray['banner'][0]['path']) }}" alt="banner"/>
                @else
                    <div id="shadow">
                    </div>
                @endif

                <div class="occasion-title">


                    <div class="table">
                        <div class="bg"
                             style="{{ isset($sortedMediaArray['banner'])?"background-image: url('/".$sortedMediaArray['banner'][0]['path']."');":"background-color:#CCC;" }}"></div>
                        <div class="logo">
                            <img class="lozad" data-src="{{ cdnAsset($event->logo) }}" alt="{{ $event->title }} logo"/>
                        </div>
                        <div class="title">
                            <h1>
                                {{ $event->title }}
                            </h1>

                            @if(!is_null($upcomingDate))
                            <div class="date">
                                {{ presentOccasionDates($upcomingDate->get('start_date'), $upcomingDate->get('end_date'), '-', null) }}
                                 - {{ $upcomingDate->get('location') }}
                            </div>
                            @endif
                        </div>
                        <div class="button">
                            <a href="{{ route('eventBooking', ['event', $event->id, $event->slug], false) }}">@lang('Book Now')</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="occasion-body">

            <div class="left-column">
                {!! $event->content !!}
            </div>

            @if(isset($sortedMediaArray['']))

                <div id="photo-gallery" class="masonry" data-item-selector=".photo">
                    @foreach($sortedMediaArray[''] as $key => $photo)

                        <div class="photo {{ $galleryColumns[($key>5)?($key%6):$key] }}">
                            <img class="lozad" data-src="{{ cdnAsset($photo['path']) }}" alt="{{ $photo['caption'] ?? $event->title.' gallery' }}"/>
                        </div>

                    @endforeach
                </div>

            @endif

        </div>
    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div

@stop
