@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="page">

        @include('inc.breadcrumbs', $breadcrumbs)


        <div id="page-header">

            @if(isset($media['banner']))
            <div id="banner">

                @if($media['banner'])
                <img class="lozad" data-src="{{ cdnAsset($media['banner'][0]['path']) }}"
                    alt="{{ $media['banner'][0]['caption'] ?? $page->title . ' banner' }}" />
                @endif

                <div class="blur-bg-title">
                    <div class="bg lozad" style="{{ cdnAsset($media['banner'][0]['path']) }}"></div>
                    <h1>
                        {{ $page->title }}
                    </h1>
                </div>
            </div>
            @else
            <div id="shadow">
            </div>
            @endif

        </div>

        <div id="page-body">

            <div class="page-tabs">

                <ul class="tabs cf" data-tabgroup="page-{{ $page->id }}">
                    @foreach($children as $key => $child)
                    <li class="{{ $key==0?"active":"" }}" style="width:{{ (100/$children->count()) }}%;"><a
                            href="#{{ Str::slug($child->name) }}">{{ $child->title }}</a></li>
                    @endforeach
                </ul>

            </div>

            <div class="right-column">
                <div id="side-menu">
                    <ul>
                        @foreach($siblings as $siblingPage)
                        <li class="{{ activeIf($siblingPage->uri, $localePrefix) }}"><a
                                href="{{ url(prefixUrl($siblingPage->uri)) }}">{{ $siblingPage->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="left-column">

                @if(!isset($media['banner']))
                <h1 class="text-uppercase">
                    {{ $page->title }}
                </h1>
                @endif

                {!! $page->content !!}

                <div id="page-{{ $page->id }}" class="tabgroup">
                    @foreach($children as $child)
                    <div id="{{ Str::slug($child->name) }}" class="content-container">
                        {!! $child->content !!}
                    </div>
                    @endforeach
                </div>
            </div>

        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>
@stop