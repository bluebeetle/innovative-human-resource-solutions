@extends('layouts.default')

@section('title', $page->title)

@section('content')

<div class="container">
    <div id="clients">

        <div id="shadow">
        </div>

        <div id="team-dropdown">
            <a href="{{ route('aboutPSIMiddleEast') }}" class="back">
                <i class="sprite-icon"></i>
            </a>

            <div class="dropdown">
                <a href="#">{{ $page->title }}
                    <i class="sprite-icon"></i>
                </a>
                <ul>
                    @foreach($siblings as $sibling)
                        <li><a href="{{ url(prefixUrl($sibling->uri)) }}">{{ $sibling->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div id="clients-body">
            <div class="left-column">
                <h1>
                    {{ $page->title }}
                </h1>

                {!! $page->content !!}

            </div>
        </div>

        <div id="clients-grid">
            @foreach($clients as $client)
                <article class="client {{ Str::slug($client->name) }}">
                    <a href="#">
                        <img class="lozad" data-src="{{ imageThumb($client->logo, 320) }}" alt="{{ $client->title }}"/>
                    </a>
                    <div class="description content-container">
                            {!!  $client->content !!}
                    </div>
                </article>
            @endforeach
        </div>

    </div>
</div>

@include('inc.newsletterWidget')

<div class="container">
    @include('inc.locationsWidget')

    @include('inc.socialBarWidget')
</div>
@stop
