<p>
    Hello,
</p>
<p> A new booking has been made for <a href="{{ route('course', [$data['course']->id, $data['course']->slug]) }}">{{ $data['course']->title }}</a> with bellow details : </p>

<p>
@foreach($data['parameters'] as $key => $value)
{{ $key }}: {{ $value }} <br/>
@endforeach
</p>
