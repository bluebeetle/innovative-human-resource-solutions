<p>
    Hello,
</p>
<p> A new booking has been made for <a href="{{ route('event', [$data['event']->id, $data['event']->slug]) }}">{{ $data['event']->title }}</a> with bellow details : </p>

<p>
@foreach($data['parameters'] as $key => $value)
{{ $key }}: {{ $value }} <br/>
@endforeach
</p>
