var util = require("../modules/util");

var masonry = {
    init : function(el) {

        this.element = $(el);

        var itemSelector = this.element.data('item-selector');
        var columnWidth = this.element.data('column-width');
        var options = {gutter: 0};

        if(util.isDefined(itemSelector)) {
            options.itemSelector = itemSelector;
        }

        if(util.isDefined(columnWidth)) {
            options.columnWidth = columnWidth;
        }


        //this.masonryHandle = new Masonry( el, options);


        this.masonryHandle = this.element.packery(options);

        this.masonryHandle.imagesLoaded().progress((function($grid) {
            return function() {
                    $grid.packery();
            }
        })(this.masonryHandle));



        /*(function($grid) {
            return function(el) {
                $grid.packery();
            }
        })(this.masonryHandle)*/

        let packeryTimeoutHandle;

        const lazy = lozad('.masonry .load-lazy', {
            loaded: (el) => {

                clearTimeout(packeryTimeoutHandle);

                packeryTimeoutHandle = setTimeout((function($grid) {
                    return function() {
                        $grid.packery();
                    }
                })(this.masonryHandle), 500);

            }
        });
        lazy.observe();

    }
}


module.exports = masonry;
