var swapColumnPositions = {
    init : function(el) {

        var resizeDebounceHandler = null;

        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(swap,300)
        });

        swap();

        /*---------------------*/

        function swap() {

            var windowWidth = $(window).width();

            if(windowWidth <= 960){
                $(".left-column", el).after($(".right-column", el));
            }
            else {
                $(".right-column", el).after($(".left-column", el));
            }

        }
    }
}

module.exports = swapColumnPositions;

