var responsivePageTabs = {
    init : function(el) {


        $(".tabs", el).on('click', 'a', function(e) {
            setTimeout(updateCurrentTab, 50);
        });

        var resizeDebounceHandler = null;

        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(updateCurrentTab,300)
        });

        updateCurrentTab();

        /*---------------------*/

        function updateCurrentTab() {

            var windowWidth = $(window).width();

            if(windowWidth <= 768 || $('body').attr('dir') == 'rtl'){

                if($('.current-tab', el).length == 0)
                {
                    var currentTab = $('<div />', {
                        class : 'current-tab',
                        text : $(".page-tabs .active a").text()
                    });

                    currentTab.prependTo(el);
                }
                else
                {
                    $('.current-tab', el).text($(".page-tabs .active a").text());
                }

                $('.page-tabs').off('click').on('click', '.current-tab', function(e) {
                    e.preventDefault();
                    $(this).closest('.page-tabs').toggleClass('hover');
                    return false;
                });

                $(document).click(function() {
                    $('.page-tabs').removeClass('hover');
                });

            }
            else {
                $('.current-tab', el).remove();
            }

        }
    }
}

module.exports = responsivePageTabs;

