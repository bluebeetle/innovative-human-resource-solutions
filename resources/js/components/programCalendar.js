var DropdownMenu = require('../modules/DropdownMenu');

var programCalendar = {
    init: function(el) {

        this.element = $(el);

        $('.row', el).on('click', '.months a, .name a', function(e) {

            var windowWidth = $(window).width();

            if(windowWidth<= 900 && $(this).parent().hasClass('month')) {
                return true;
            }

            e.preventDefault();
            $(this).closest('.row').toggleClass('expanded')
                .siblings().removeClass('expanded');
        });


        if($('.dropdown-menu').length > 0)
        {

            var dropDown = new DropdownMenu('.dropdown-menu');
                dropDown.onChange(function(value, slugValue) {
                    $("#" + slugValue).removeClass('hidden').siblings('[id^="year-"]').addClass('hidden');
                })
                dropDown.init();
        }
    }
}

module.exports = programCalendar;
