var overviewMinHeight = {
    init : function(el) {


        var sideMenuHeight = $("#side-menu").outerHeight();
        var headerHeight = $("#page-header").outerHeight();
        var minHeight = Math.abs(headerHeight - parseInt($("#side-menu").css('top')) - parseInt($("#page-body").css('padding-top')) - sideMenuHeight);

        $("#page-body .left-column").css('min-height', minHeight);
    }
}

module.exports = overviewMinHeight;

