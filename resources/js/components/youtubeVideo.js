var videos = {};
var util = require("../modules/util");
var youtubeVideo = {
    init: function (el) {

        this.element = $(el);

        this.playerDefaults = {
            autoplay: 0,
            autohide: 1,
            loop: 0,
            playlist : '',
            modestbranding: 1,
            rel: 0,
            showinfo: 0,
            controls: 0,
            disablekb: 1,
            enablejsapi: 0,
            iv_load_policy: 3
        };


        this.element.each(function() {
            var video = $(this).find('.video');
            var videoId = video.data('video-id');
            if (!video.attr('id'))
                video.attr("id", "id_" + videoId + util.getUniqueString());
        });


        this.addYoutubeAPI();

    },
    
    addYoutubeAPI: function () {

        var tag = document.createElement('script');
        tag.src = 'https://www.youtube.com/player_api';
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        
        // Add Youtube API ready function
        window['onYouTubePlayerAPIReady'] = function() {
            $('.youtube-video').each(function() {

                var video = $(this).find('.video');
                var elementId = video.attr('id');
                var playerVars = $.extend({}, youtubeVideo.playerDefaults);
                var options = {
                    'videoId' : video.data('video-id'),
                    'mute' :  video.data('mute') ? video.data('mute'):0,
                };
                $.each(playerVars, function(index, value) {
                    if(!util.isUndefined(video.data(index)))
                    {
                        playerVars[index] = video.data(index);
                        options[index] = video.data(index);
                    }
                });

                videos[elementId] = {};
                videos[elementId]['options'] = options;
                videos[elementId]['player'] = new YT.Player(elementId, {
                    videoId : options.videoId,
                    events: {'onReady': youtubeVideo.onPlayerReady, 'onStateChange': youtubeVideo.onPlayerStateChange},
                    playerVars: playerVars
                });

            });
        }

        $(".youtube-video").on('click', '.play', function(e) {
            e.preventDefault();
            var elementId  = $(this).closest('.youtube-video').find('.video').attr('id');
            var videoHandler = videos[elementId]['player'];
            videoHandler.playVideo();
        });

        $(".youtube-video").on('click', '.pause', function(e) {
            e.preventDefault();
            var elementId  = $(this).closest('.youtube-video').find('.video').attr('id');
            var videoHandler = videos[elementId]['player'];
            videoHandler.pauseVideo();
        });

    },
    
    onPlayerReady: function (e) {

        var elementId = e.target.getIframe().id;
        var videoHandler = videos[elementId]['player'];
        var mute = videos[elementId]['options']['mute'];
        var autoplay = videos[elementId]['options']['autoplay'];

        if(!util.isUndefined(autoplay) && autoplay)
        {
            videoHandler.playVideo();
        }

        if(!util.isUndefined(mute) && mute)
        {
            videoHandler.mute();
        }
    },
    
    onPlayerStateChange: function (e) {

        var elementId = e.target.getIframe().id;
        var element = $(e.target.getIframe());
        var loop = videos[elementId]['options']['loop'];
        var videoHandler = videos[elementId]['player'];
        var classes = ['video-unstarted', 'video-ended', 'video-playing', 'video-paused', 'video-buffering', 'video-cued'];
        classes.forEach(function(item) {
            element.parent().removeClass(item);
        })
        element.parent().addClass(classes[(e.data + 1)]);

        if (e.data === 0 && (!util.isUndefined(loop) && loop)) {
            videoHandler.seekTo(0);
        }
    }
}

module.exports = youtubeVideo;

