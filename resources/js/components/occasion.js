var DropdownMenu = require('../modules/DropdownMenu');

var occasion = {
    init: function(el) {

        this.element = $(el);

        if($('.dropdown-menu').length > 0)
        {

            var dropDown = new DropdownMenu('.dropdown-menu');
            dropDown.onChange(function(value, slugValue) {
                $("#" + slugValue).removeClass('hidden').siblings('[id^="year-"]').addClass('hidden');
            })
            dropDown.init();
        }
    }
}

module.exports = occasion;
