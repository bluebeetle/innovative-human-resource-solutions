var bookingForm = {
    init: function(el) {

        this.element = $(el);

        function toggleCompanyName() {
            if($("#booking-type-company").is(":checked"))
            {
                $('#company-name').addClass('show');
            }

            if($("#booking-type-individual").is(":checked"))
            {
                $('#company-name').removeClass('show');
            }
        }

        $('[name=bookingType]', el).on('change', function() {
            toggleCompanyName();
        });

        toggleCompanyName();



        function toggleParticipantFields() {
            if($("#behalf-of-participant-yes").is(":checked"))
            {
                $('#sponsor-details').addClass('show');
            }

            if($("#behalf-of-participant-no").is(":checked"))
            {
                $('#sponsor-details').removeClass('show');
            }
        }

        $('input[type=radio]', '#behalf-of-participant').on('change', function() {
            toggleParticipantFields();
        });

        toggleParticipantFields();

    }
}

module.exports = bookingForm;
