var logoSlider = {
    init : function(el) {
        $(el).slick({
            lazyLoad : 'progressive',
            slidesToShow : 6,
            swipeToSlide : true,
            rtl : ($('body').attr('dir') == 'rtl' ? true : false),
            responsive : [
                {
                    breakpoint : 1200,
                    settings : {
                        slidesToShow : 5,
                    }
                },
                {
                    breakpoint : 1024,
                    settings : {
                        slidesToShow : 4,
                    }
                },
                {
                    breakpoint : 960,
                    settings : {
                        slidesToShow : 3,
                    }
                },
                {
                    breakpoint : 640,
                    settings : {
                        slidesToShow : 2,
                    }
                },
                {
                    breakpoint : 480,
                    settings : {
                        slidesToShow : 1,
                    }
                }

            ]
        });
    }
}


module.exports = logoSlider;
