var overviewResponsiveSideMenu = {
    init : function(el) {

        var resizeDebounceHandler = null;

        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(swapNavPositions,300)
        });

        swapNavPositions();

        /*---------------------*/

        function swapNavPositions() {

            var windowWidth = $(window).width();

            if(windowWidth <= 960){

                $("#page-body .right-column").append($("#page-header #side-menu"));
            }
            else {

                $("#page-header").append($("#page-body .right-column #side-menu"));
            }

        }
    }
}

module.exports = overviewResponsiveSideMenu;

