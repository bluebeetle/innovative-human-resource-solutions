var teamSlider = {

    init : function(el) {

        this.element = $(el);

        // Details Slider
        this.detailsSlider = this.element.find('.details').slick({
            lazyLoad : 'progressive',
            rtl : ($('body').attr('dir') == 'rtl' ? true : false),
            swipe : true,
            touchMove : false,
            arrows : false,
            adaptiveHeight: true,
        });


        function goToMember(memberIndex) {
            teamSlider.detailsSlider.slick('slickGoTo', memberIndex);

            var windowWidth = $(window).width();
            if(windowWidth <= 640)
            {
                $('html, body').animate({
                    scrollTop: $(".details", el).offset().top
                }, 100);
            }
        }


        this.element.find('.thumb').on('click', 'a', (function(self) {
            return function(e) {
                e.preventDefault();

                goToMember($(this).parent('.thumb').index());
            }
        })(this));



         $(window).on('load', () => {
             var hashTag = window.location.hash;
             if(hashTag != ''){
                 hashTag = hashTag.replace(/^#/, '');

                 goToMember($('.' + hashTag).parent('.thumb').index());
             }
        });

    }
}

module.exports = teamSlider;

