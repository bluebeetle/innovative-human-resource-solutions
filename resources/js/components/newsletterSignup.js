var newsletterSignup = {
    init : function(el) {

        this.element = $(el);

        this.element.on('click', '.why-signup', (function(self) {
            return function(e) {
                e.preventDefault();
                self.element.toggleClass('signup-benefits');
            }
        })(this));


        var resizeDebounceHandler = null;

        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(swapPositions,300)
        });

        swapPositions();

        /*-----------------*/

        function swapPositions() {

            var windowWidth = $(window).width();

            if(windowWidth <= 960){
                $("form > .email", el).after($("form > .buttons", el));
            }
            else {
                $("form > .buttons", el).after($("form > .email", el));
            }

        }

    }
}

module.exports = newsletterSignup;
