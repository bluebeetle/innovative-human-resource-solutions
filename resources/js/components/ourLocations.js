var util = require("../modules/util");

var ourLocations = {
    init : function(el) {

        this.element = $(el);

        setBgImageFrom($("li.active > a", el));

        this.element.on('mouseenter', '.locations > ul li > a', (function(self) {
            return function(e) {
                e.preventDefault();

                $(this).parent().addClass('active')
                    .siblings().removeClass('active');

                setBgImageFrom($(this));

            }
        })(this));

        function setBgImageFrom(activeLocation) {

            var imageUrl = activeLocation.data('image');

            if(!util.isUndefined(imageUrl) && imageUrl)
            {
                $(el).css('background-image', 'url(' + imageUrl + ')');
            }
        }



        /*this.element.on('click', '.locations > ul li > a', function(e) {
            return false;
        });*/
    }
}

module.exports = ourLocations;
