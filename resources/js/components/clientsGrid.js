var clientsGrid = {

    init : function(el) {

        this.element = $(el);

        this.element.find('.client').on('click', 'a', (function(self) {
            return function(e) {
                e.preventDefault();


                var gridWidth = self.element.width();
                var clientBoxWidth = self.element.find('> .client').outerWidth();
                var boxesPerRow = Math.round(gridWidth / clientBoxWidth);


                self.element.find('.client').removeClass('active');
                self.element.find('.client + .cf + .description').remove();
                self.element.find('.client + .cf').remove();

                var index = $(this).parent('.client').addClass('active').index() + 1;
                var row = Math.ceil(index/boxesPerRow);


                var lastIndex = (row*boxesPerRow) - 1;
                var totalCount = self.element.find('.client').length - 1;

                if(lastIndex > totalCount)
                {
                    lastIndex = totalCount;
                }

                var lastInRow = self.element.find('.client').eq(lastIndex);

                var cf = $("<div />", {
                    'class' : 'cf'
                });

                cf.insertAfter(lastInRow);
                $(this).parent('.client').find('.description').clone().insertAfter(cf);


            }
        })(this));




        $(window).load(function () {
            var hashTag = window.location.hash;
            if(hashTag != ''){
                hashTag = hashTag.replace(/^#/, '');
                $('.' + hashTag).addClass('active');
                $('html, body').animate({
                    scrollTop: $('.' + hashTag).offset().top
                }, 200);
            }
        });

    }
}

module.exports = clientsGrid;

