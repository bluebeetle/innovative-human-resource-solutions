var careersSlider = {

    init : function(el) {


        this.element = $(el);

        this.jobsContainer = $('.jobs', el);
        this.jobsContainer.find(' > .job').hide();

        function openJob($jobLink) {
            var $this = $jobLink,
                others = $this.closest('li').siblings(),
                target = $this.attr('href');
            others.removeClass('active');
            $this.closest('li').addClass('active');
            careersSlider.jobsContainer.children('div').hide();
            $(target).show();

            var windowWidth = $(window).width();


            if(!careersSlider.element.hasClass('showing-job') && windowWidth>960)
            {
                var leftColumnWidth = careersSlider.element.find('.left-column').outerWidth();
                if( $("body").css('direction') == 'rtl') {
                    careersSlider.element.css('margin-right', (-1 * (leftColumnWidth - 30)));
                }
                else {
                    careersSlider.element.css('margin-left', (-1 * (leftColumnWidth - 30)));
                }
            }
            careersSlider.element.addClass('showing-job');
        }

        this.element.find('#side-menu').on('click', 'a', function(e) {
            e.preventDefault();
            var $this = $(this);
            openJob($this);
        });

        this.element.find('.right-column').on('click', 'a.close', (function(self) {
            return function (e) {
                e.preventDefault();
                if( $("body").css('direction') == 'rtl') {
                    self.element.css('margin-right', 0);
                }
                else {
                    self.element.css('margin-left', 0);
                }

                self.element.removeClass('showing-job');

                $("#side-menu li", el).removeClass('active');
                $('.jobs .job', el).hide();
            }
        })(this));


        var resizeDebounceHandler = null;
        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(swap,300)
        });
        swap();

        /*---------------------*/

        function swap() {

            var windowWidth = $(window).width();

            if(windowWidth <= 960){
                $(".left-column", el).after($(".right-column", el));
                $(el).css('margin-left', 0);
            }
            else {
                $(".right-column", el).after($(".left-column", el));
            }

        }


        $(window).load(() => {
            var hashTag = window.location.hash;
            if(hashTag != ''){
                hashTag = hashTag.replace(/^#/, '');
                openJob($('.' + hashTag));
            }
        });

    }
}

module.exports = careersSlider;

