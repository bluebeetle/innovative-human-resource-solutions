var responsiveCalendar = {
    init : function(el) {


        var resizeDebounceHandler = null;

        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(toggleHeaders,300)
        });

        toggleHeaders();

        /*---------------------*/

        function toggleHeaders() {

            var windowWidth = $(window).width();

            if(windowWidth <= 900){

                if($(".row .headers", el).length == 0) {

                    var headers = $('<div />', {
                        class : 'headers',
                        html : $(".headers .months", el).clone()
                    })
                    $(".row .scroll .months", el).before(headers);

                }

            }
            else {

                $(".row .headers", el).remove();

            }

        }
    }
}

module.exports = responsiveCalendar;

