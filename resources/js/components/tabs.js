var tabs = {
    init: function(el) {

        $('.tabgroup > div').hide();
        $('.tabgroup > div:first-of-type').show();
        $('.tabs a').closest('li:first-of-type').addClass('active');

        $('.tabs a').click(function (e) {
            e.preventDefault();
            var $this = $(this),
                tabgroup = '#' + $this.parents('.tabs').data('tabgroup'),
                others = $this.closest('li').siblings(),
                target = $this.attr('href');
            others.removeClass('active');
            $this.closest('li').addClass('active');
            $(tabgroup).children('div').hide();
            $(target).show();
        });
    }
}

module.exports = tabs;
