var contactForm = {

    init : function(el) {

        this.element = $(el);

        this.element.on('click', '.form-trigger', (function(self) {
            return function(e) {
                e.preventDefault();
                self.element.toggleClass('show-form');
            }
        })(this));

    }
}

module.exports = contactForm;

