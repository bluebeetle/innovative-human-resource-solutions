module.exports = {
    init: function (el) {

        initHeaderSearchForm();


        var resizeDebounceHandler = null;

        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(swapNavPositions,300)
        });

        swapNavPositions();


        $("#hamburger").click(function(e) {
            e.preventDefault();

            $(this).toggleClass('open');
            $("header").toggleClass('open-mobile-nav');

        })


        /*-----------------*/

        function swapNavPositions() {

            var windowWidth = $(window).width();

            if(windowWidth <= 960){
                $("nav.secondary-nav").after($("nav.main-nav"));
                $('#search').after($('#search-form'));
                $('#search-form').after($('#search-result'));
            }
            else {
                $("nav.main-nav").after($("nav.secondary-nav"));
                $('#search').append($('#search-form'));
                $('#search-form').append($('#search-result'));
            }

        }

        function initHeaderSearchForm() {

            if ($("#search #search-trigger").length > 0) {
                $("#search-trigger", "#search").on("click", function (e) {
                    e.preventDefault();
                    $("#search-form").toggleClass("show");
                    $("#search").toggleClass("show");
                })
            }

            $(document).mouseup(function (e) {
                var $click_outside = $("#search-form");
                if (!$click_outside.is(e.target) // if the target of the click isn't the container...
                    && $click_outside.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    $("#search-form").removeClass("show");
                    $("#search").removeClass("show");
                    $("#search-result").removeClass("show");
                }
            });




            /*----------------------------------------------*/


            /* START: Search Form */

            if($("#search-form").length > 0)
            {
                var searchEventHandler;
                $("#search-box").on('keyup', function(e)
                {
                    e.preventDefault();
                    clearTimeout(searchEventHandler);
                    var q = $(this).val();
                    if (e.keyCode == 13 && _q.length > 2) {
                        search(q);
                    }
                    if (q == "") {
                        resetSearchForm();
                    }
                    else if (q.length > 2) {
                        searchEventHandler = setTimeout(function()
                            {
                                search(q);
                            },
                            500);
                    }
                    else {
                        return;
                    }
                });
            }


            /* END: Search Form */


        }

        function resetSearchForm() {
            $("#search-box").val('');
            $("#search-result").removeClass("show");
        }

        function search(q) {

            if(q=='' || q==' ') {
                resetSearchForm();
                return;
            }

            $.get("/search", {'q':q}, function(data) {
                var result = $.parseJSON(data);
                var item_string = '';
                var ul = $('<ul />');

                $.each(result, function(i,item) {

                    var type = $('<div />', {
                        class: 'type',
                        text: item.type
                    });

                    var title = $('<div />', {
                        class: 'title',
                        text: item.title
                    });

                    if(item.link)
                    {
                        var anchor = $('<a />', {
                            href: item.link
                        }).append(title).append(type);
                    }
                    else {
                        var anchor = $('<div />')
                            .append(title).append(type);
                    }


                    var li = $('<li />', {
                        class: item.type
                    }).append(anchor);

                    ul.append(li);

                });

                var list = $('<div />', {
                    class: 'list'
                }).append(ul);


                $("#search-result").html(list);

                var _height = $(window).height() - $("#search-result").position().top - 20;

                $("#search-result").css('max-height',_height).addClass('show');

            });
        }

    }
}
