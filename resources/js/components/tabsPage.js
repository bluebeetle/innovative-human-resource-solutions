var tabsPage = {
    init: function(el) {

        var hash = location.hash;

        var $tab = $('.tabs a[href="' + hash + '"]'),
            tabgroup = '#' + $tab.parents('.tabs').data('tabgroup'),
            others = $tab.closest('li').siblings(),
            target = $tab.attr('href');

            others.removeClass('active');
            $tab.closest('li').addClass('active');
            $(tabgroup).children('div').hide();
            $(target).show();
    }
}

module.exports = tabsPage;
