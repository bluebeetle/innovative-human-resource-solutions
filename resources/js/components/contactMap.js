var contactMap = {

    init : function(el) {

        $(function() {

            this.element = $(el);

            /**
             * markers is set from the view
             */
                //var markers = this.element.data('markers');

            markers.forEach(function(marker) {
                marker['icon'] = '/images/marker.png'
            });

            /*Configuration*/
            /*$.gmap3({
                key : '...'
            });*/

            this.element.gmap3({
                center: markers[0]['position'],
                zoom: 5,
                mapTypeControl: false,
                navigationControl: false,
                scrollwheel: false,
                streetViewControl: false,
                zoomControl : false,
            })
              .marker(markers).fit();
        });

    }
}

module.exports = contactMap;

