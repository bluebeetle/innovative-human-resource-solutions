var selectBox = {
    init: function(el) {

        this.element = $(el);


        $('select', el).on('change', function(e) {
            e.preventDefault();
            $(this).closest(el).find('.placeholder').text($(this).find(":selected").text());
        });

        $('select', el).each(function() {
            $(this).closest(el).find('.placeholder').text($(this).find(":selected").text());
        });

    }
}

module.exports = selectBox;
