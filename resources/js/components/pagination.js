var pagination = {
    init : function(el) {

        this.element = $(el);

        var width = this.element.width();
        var ulWidh = this.element.find('ul').width();
        var newWidth = (width-ulWidh) / 2;

        this.element.find('.next, .prev').width(newWidth);
    }
}

module.exports = pagination;
