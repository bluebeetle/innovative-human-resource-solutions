var responsiveOccasionTitle = {
    init : function(el) {


        var resizeDebounceHandler = null;

        $(window).on("resize", function (e) {
            e.preventDefault();

            if (resizeDebounceHandler !== null) {
                clearTimeout(resizeDebounceHandler);
            }

            resizeDebounceHandler = setTimeout(changeLogoPosition,300)
        });

        changeLogoPosition();

        /*---------------------*/

        function changeLogoPosition() {

            var windowWidth = $(window).width();

            if(windowWidth <= 960){
                $(".table", el).before($(".logo", el));
            }
            else {
                $(".bg", el).after($(".logo", el));
            }

        }
    }
}

module.exports = responsiveOccasionTitle;

