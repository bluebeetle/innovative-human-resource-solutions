var common = {
    init : function() {
        this.avoidConsoleErrors();
        this.addUseragentToDoc();
        this.disableClick();
        this.detectTouchEvents();
        this.disableSubmitAfterClick();
        this.initAcquiringNotifications();
        

        /* START: Init Lozad */

        const observer = lozad(); // lazy loads elements with default selector as '.lozad'
        observer.observe();

        /* END: Init Lozad */
    },
    avoidConsoleErrors : function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
},
    addUseragentToDoc : function() {
        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    },
    disableClick : function() {
        $("body").on('click', '.no-click', function(e) {
            return false;
        })
    },
    detectTouchEvents : function() {
        var hasTouch = 'ontouchstart' in window;
        if(hasTouch){
            $('html').addClass('touch');
        }
    },
    disableSubmitAfterClick : function() {

        var allowSubmit = true;

        $('input[type=submit]').on('click', function(e) {

            if(! allowSubmit) {
                return false;
            }
            var form = $(this).closest('form')[0];
            var valid = true;

            for (var f = 0; f < form.elements.length; f++) {
                var field = form.elements[f];

                if(! field.validity.valid)
                {
                    valid = false;
                    break;
                }
            }

            if(valid) {
                allowSubmit = false;
                return true;
            }

        });
    },
    initAcquiringNotifications : function() {
        
        setTimeout(function() {
            $('#acquiring-notification').removeClass('close');
        }, 1000);
        
        $('#acquiring-notification a.close').on('click', function(e) {
            $(this).closest('#acquiring-notification').remove();
        })
    }
}

module.exports = common;
