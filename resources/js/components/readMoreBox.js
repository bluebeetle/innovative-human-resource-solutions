var readMoreBox = {
    init: function(el) {

        this.element = $(el);




        $(".read-more .button", el).click(function(e) {

            e.preventDefault();

            var $el, $innerElements, totalHeight;

            totalHeight = 0

            $el = $(this);
            $innerElements = $el.closest(el).find(" > div, > p:not('.read-more')");

            // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
            $innerElements.each(function() {
                totalHeight += $(this).outerHeight() + parseInt($(this).css('margin-top')) + parseInt($(this).css('margin-bottom'));
                // FAIL totalHeight += $(this).css("margin-bottom");
            });

            $el.closest(el).css({
                    "max-height": totalHeight
                }).addClass('expanded');



        });

    }
}

module.exports = readMoreBox;
