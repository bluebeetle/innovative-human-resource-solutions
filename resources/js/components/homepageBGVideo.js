var homepageBGVideo = {
    init: function (el) {

        if($("[data-homepage-bg-video]").length == 0)
        {
            return;
        }

        this.element = $("[data-homepage-bg-video]");


        this.playerDefaults = {
            autoplay: 1,
            autohide: 1,
            loop: 1,
            playlist : '',
            modestbranding: 0,
            rel: 0,
            showinfo: 0,
            controls: 0,
            disablekb: 1,
            enablejsapi: 0,
            iv_load_policy: 3
        };


        this.video = '';

        if (!this.element.attr('id'))
            this.element.attr("id", "id_" + new Date().getTime());

        this.containerId = $("[data-homepage-bg-video]").attr('id');

        this.videoId = $("[data-homepage-bg-video]").data('homepage-bg-video');
        this.playerDefaults.playlist = this.videoId;

        this.addYoutubeAPI();


    },
    addYoutubeAPI: function () {

        var tag = document.createElement('script');
        tag.src = 'https://www.youtube.com/player_api';
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        
        // Add Youtube API ready function
        window['onYouTubePlayerAPIReady'] = (function (self) {
            return function() {
                homepageBGVideo.video = new YT.Player(self.containerId, {
                    events: {'onReady': homepageBGVideo.onPlayerReady, 'onStateChange': homepageBGVideo.onPlayerStateChange},
                    playerVars: self.playerDefaults
                });

                $("#" + self.containerId).data('video', self.video);
            }
        })(this)
    },
    onPlayerReady: function (e) {
        homepageBGVideo.video.loadVideoById(homepageBGVideo.videoId);
        homepageBGVideo.video.mute();
    },
    onPlayerStateChange: function (e) {
        if (e.data === 1) {
            $('#' + homepageBGVideo.containerId).addClass('active');
            $('#' + homepageBGVideo.containerId).parent().addClass("video-loaded");
        } else if (e.data === 0) {
            homepageBGVideo.video.seekTo(0);
        }
    }
}

module.exports = homepageBGVideo;

