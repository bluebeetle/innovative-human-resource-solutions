(function () {
    "use strict";

    /**
     * TheApp object
     * It contains all the js for the applications divided into sub functions called based on need.
     */
    var TheApp = function () {

        var theApp = this;


        /* START: Common Section */
        /**
         * Here is all the common js code
         */

        /* START: Confirm action */

        $(".confirm-action").on('click',function(e) {
            e.preventDefault();

            var link = this;

            $.confirm({
                title: 'Are you sure',
                content: 'This action is critical and may not be undone.<br/>Click proceed if you still want to continue.',
                confirmButton: 'Proceed',
                confirmButtonClass: 'btn-info',
                icon: 'glyphicon glyphicon-question-sign',
                animation: 'scale',
                opacity: 0.5,
                confirm: function () {
                    window.location = link.href;
                }
            });

        });

        /* END: Confirm action */


        /*----------------------------------------------*/

        /* START: Alias Page */
        function toggleAliasFields($aliasEl) {

            if($aliasEl.is(":checked"))
            {
                $(".tab-pane > div:not(.alias-field)").hide();
            }
            else {
                $(".tab-pane > div:not(.alias-field)").show();
            }

        }

        $("input[type=checkbox]#alias").on('change', function(e) {

            e.preventDefault();

            toggleAliasFields($(this));

        });

        toggleAliasFields($("input[type=checkbox]#alias"));


        /* END: Alias Page */



        /* END: Common Section */

        /*----------------------------------------------*/

        /* START: TinyMCE */

        theApp.initializeTinyMCE = function() {
            tinymce.init({
                selector: 'textarea.tinymce-editor',
                plugins: [
                    "autolink link image lists preview hr anchor pagebreak",
                    "visualblocks visualchars code fullscreen media nonbreaking",
                    "table contextmenu template paste textpattern"
                ],
                toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect styleselect | bullist numlist link unlink image media table | removeformat nonbreaking  pagebreak | code",
                min_height : 500,
                max_height: 1000,
                menubar: false,
                formats : {
                    alignleft: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
                    aligncenter: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
                    alignright: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
                    alignfull: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
                    bold: {inline : 'span', 'classes' : 'bold'},
                    italic: {inline : 'span', 'classes' : 'italic'},
                    underline: {inline : 'span', 'classes' : 'underline', exact : true},
                    strikethrough: {inline : 'del'}
                },
                block_formats: 'Paragraph=p;Header 2=h2;Header 3=h3; Header 4=h4;Header 5=h5',
                style_formats: [
                /*    {title: 'Image Left', selector: 'img', styles: {
                        'float' : 'left',
                        'margin': '0 10px 0 10px'
                    }},
                    {title: 'Image Right', selector: 'img', styles: {
                        'float' : 'right',
                        'margin': '0 10px 0 10px'
                    }}*/
                ],
                content_css : '/css/cms/editor-content.css?' + new Date().getTime(),
                relative_urls : false

            });
        }
        if ($(".tinymce-editor").length>0)
        {
            theApp.initializeTinyMCE();
        }

        /* END: TinyMCE */

        /*----------------------------------------------*/

        /* START: Slugify */

        theApp.initializeSlug = function () {
            $(".make-slug").each(function(index, value) {

                new MakeSlug($(this), $(this).data('slug-to'));

            })
        }

        if($(".make-slug").length > 0)
        {
            theApp.initializeSlug();
        }

        /* END: Slugify */

        /*----------------------------------------------*/

        /* START: ToolTip */

        theApp.initializeToolTip = function() {
            $('[data-toggle="tooltip"]').tooltip();
        }

        if($('[data-toggle="tooltip"]').length > 0){
            theApp.initializeToolTip();
        }

        /* END: ToolTip */

        /*----------------------------------------------*/

        /* START: Media Grid */

        theApp.initializeMediaGrid = function() {

            /**
             * Isotope
             */
            theApp.$isotopeHandler = $('.media-grid').isotope({
                // set itemSelector so .grid-sizer is not used in layout
                itemSelector: '.media-file'
            });

            theApp.$isotopeHandler.imagesLoaded().progress( function() {
                theApp.$isotopeHandler.isotope('layout');
            });


            /**
             * Select media to attach to mediable resource
             */
            $('.select-media .media-file input[type=checkbox]').on('change', function(e) {
                if($(this).is(":checked")) {
                    $(this).closest('.media-file').addClass('selected');
                }
                else {
                    $(this).closest('.media-file').removeClass('selected');
                }

                theApp.$isotopeHandler.isotope('layout');
            });


            /**
             * Remove attached media from mediable resource
             */
            $('.manage-media .media-file > a.remove').on('click', function(e) {

                $(this).closest('.media-file').toggleClass('removed');
                var disabled = false;
                if($(this).closest('.media-file').hasClass('removed')) {
                    disabled =  true;
                }
                $(this).closest('.media-file').find('.media-data input[type=text]').prop('disabled', disabled);
                $(this).closest('.media-file').find('input[type=checkbox]').prop('checked', disabled);
            });

            $('.manage-media .media-file > label').on('click', function(e) {

                if($(this).closest('.media-file').hasClass('removed')) {
                    $(this).closest('.media-file').removeClass('removed');
                    $(this).closest('.media-file').find('.media-data input[type=text]').prop('disabled', false);
                    $(this).closest('.media-file').find('input[type=checkbox]').prop('checked', false);

                }
            });




        }

        if($('.media-grid').length > 0)
        {
           theApp.initializeMediaGrid();
        }

        /* END: Media Grid */

        /*----------------------------------------------*/

        /* START: DateTime Picker */

        theApp.initializeDateTimePicker = function ($parentContainer) {

            var $elements = $('input.datetimepicker');
            if(typeof $parentContainer != 'undefined')
            {
                $elements = $parentContainer.find('input.datetimepicker');
            }

            $elements.each(function() {

                var options = {format: 'YYYY-MM-DD LT'};

                if($(this).hasClass('only-time'))
                {
                    options.format = 'LT';
                }

                if($(this).hasClass('only-date'))
                {
                    options.format = 'YYYY-MM-DD';
                }

                $(this).datetimepicker(options);
            });

        }

        if($('input.datetimepicker').length > 0)
        {
            theApp.initializeDateTimePicker();
        }

        /* END: DateTime Picker */


        /*----------------------------------------------*/


        /* START: Tiers */

        theApp.initializeTiers = function() {

            $('.tiers').each(function() {
                new Tiers($(this), theApp.initializeDateTimePicker);
            })
        }

        if($('.tiers').length > 0)
        {
            theApp.initializeTiers();
        }

        /* END: Tiers */

        /*----------------------------------------------*/

        /* START: Data Tables */

        theApp.initializeDataTable = function() {
            $('[data-table]').each(function (index, element) {
                var dataTable = $(this).DataTable({
                        ordering : false,
                        pageLength : 100,
                        lengthMenu : [ [100, 150, 200, -1], [100, 150, 200, "All"] ]
                });
            });
        }

        if($('[data-table]').length > 0){
            theApp.initializeDataTable();
        }

        /* END: Data Tables */

        /*----------------------------------------------*/

        /* START: Sortable */

        theApp.initializeSortable = function() {
            $('[data-sortable]').each(function (index, element) {
                var sortableName = $(this).data('sortable');
                $(this).find('tbody').sortable({
                    items:'tr',
                    stop: function( e, ui ) {
                        var rowIdsArray = $(this).sortable('toArray');
                        var idsArray = rowIdsArray.map(function(value) {
                            return value.replace("row-", "");
                        });
                        $('[data-save-order-form="'+sortableName+'"]').find('#ids').val(idsArray.join('|'));
                    }
                });
            });

        }

        if($('[data-sortable]').length > 0){
            theApp.initializeSortable();
        }



        /* END: Sortable */

        /*----------------------------------------------*/

        /* START: Custom Fields */

        theApp.initializeCustomFieldsSorting = function() {


                $('#custom-field-tiers').sortable({
                    items:'.tier',
                    /*stop: function( e, ui ) {
                        var rowIdsArray = $(this).sortable('toArray');
                        var idsArray = rowIdsArray.map(function(value) {
                            return value.replace("row-", "");
                        });
                        $('[data-save-order-form="'+sortableName+'"]').find('#ids').val(idsArray.join('|'));
                    }*/
                });

        }

        if($("#custom-field-tiers").length > 0)
        {
            theApp.initializeCustomFieldsSorting();
        }

        /* END: Custom Fields */

        /*----------------------------------------------*/









    };


    /**
     * Modules
     */
    var MakeSlug = function($el, slugTo) {
        if (!$el || !$el.length) {
            throw new TypeError('Element is not defined!');
        }

        if(typeof slugTo == 'undefined')
        {
            slugTo = $el.attr('name') + '-slug';
        }

        $el.on('change', function(e) {

            var text = $(this).val();
            var slug = text.toString().toLowerCase().trim()
                .replace(/\s+/g, '-')           // Replace spaces with -
                .replace(/&/g, '-and-')         // Replace & with 'and'
                .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                .replace(/\-\-+/g, '-');        // Replace multiple - with single -

            $("input." + slugTo).val(slug);
        })
    }

    var Tiers = function($el, onTierAdded) {

        if (!$el || !$el.length) {
            throw new TypeError('Element is not defined!');
        }

        var self = this;

        self.addTierTrigger = $el.data('add-tier-trigger') || null
        self.tierContainer = $el.data('tier-container') || $el[0];
        self.tierSource = $el.data('tier-source') || null;
        self.tierIdCount = parseInt($el.data('tier-id-count')) || 1;


        $(self.tierContainer).off('click', '.remove-tier')
            .on('click', '.remove-tier', function(e) {
            e.preventDefault();
            $(this).closest('.tier').remove();
        })


        $(self.tierContainer).off('click', self.addTierTrigger)
            .on('click', self.addTierTrigger, function(e) {
            e.preventDefault();
            var $tierContent = $(self.tierSource).html();
            $tierContent = $($tierContent.replace(/\{tierId\}/g, ++self.tierIdCount));
            $el.data('tier-id-count', self.tierIdCount);
            $(self.tierContainer).append($tierContent);

            if(typeof onTierAdded=='function')
            {
                onTierAdded($tierContent);
            }
        })
    }

    /**
     * initapp plugin
     * @param options
     * @returns {*}
     */
    $.fn.initapp = function (options) {
        return this.each(
            function () {
                var element = $(this);
                // Return early if this element already has a plugin instance
                if (element.data('initapp')) {
                    return;
                }
                // pass options to plugin constructor
                var initapp = new TheApp(this, options);
                // Store plugin object in this element's data
                element.data('initapp', initapp);
            }
        );
    };

})();

/**
 * Document Ready
 */
$(function() {
    "use strict";
    $('body').initapp().data('initapp');
});



