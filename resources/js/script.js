(function ($) {
    
    "use strict";
    
    
    //Hide Loading Box (Preloader)
    function handlePreloader() {
        if ($('.preloader').length) {
            $('.preloader').delay(100).fadeOut(100);
        }
    }
    
    
    //Update Header Style and Scroll to Top
    function headerStyle() {
        if ($('.main-header').length) {
            var windowpos = $(window).scrollTop();
            var siteHeader = $('.main-header');
            var scrollLink = $('.scroll-to-top');
            if (windowpos >= 200) {
                siteHeader.addClass('fixed-header');
                scrollLink.fadeIn(300);
            } else {
                siteHeader.removeClass('fixed-header');
                scrollLink.fadeOut(300);
            }
        }
    }
    
    headerStyle();
    
    // Sticky header
    function stickyHeader() {
        if ($('.main-header').length) {
            var sticky = $('.main-header'),
                scroll = $(window).scrollTop();
            
            if (scroll >= 265) sticky.addClass('fixed-header');
            else sticky.removeClass('fixed-header');
            
        }
        ;
    }
    
    
    //Submenu Dropdown Toggle
    if ($('.main-header .navigation li.dropdown ul').length) {
        $('.main-header .navigation li.dropdown').append('<div class="dropdown-btn"><span class="fa fa-angle-down"></span></div>');
        
        //Dropdown Button
        $('.main-header .navigation li.dropdown .dropdown-btn').on('click', function () {
            $(this).prev('ul').slideToggle(500);
        });
        
        //Disable dropdown parent link
        $('.main-header .navigation li.dropdown > a,.hidden-bar .side-menu li.dropdown > a').on('click', function (e) {
            e.preventDefault();
        });
    }
    

    // Home popup 
    if($('#home-popup').length) {
        $('#home-popup').modal('show');
        $('#home-popup a.close').on('click', function(e) {
            $('#home-popup').modal('hide');
        });
    }
    
    //Revolution Slider
    /* if ($('.main-slider .tp-banner').length) {
        
        var MainSlider = $('.main-slider');
        var strtHeight = MainSlider.attr('data-start-height');
        var slideOverlay = "'" + MainSlider.attr('data-slide-overlay') + "'";
        
        $('.main-slider .tp-banner').show().revolution({
            dottedOverlay: slideOverlay,
            delay: 10000,
            startwidth: 1200,
            startheight: strtHeight,
            hideThumbs: 600,
            
            thumbWidth: 80,
            thumbHeight: 50,
            thumbAmount: 5,
            
            navigationType: "bullet",
            navigationArrows: "0",
            navigationStyle: "preview3",
            
            touchenabled: "on",
            onHoverStop: "off",
            
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,
            
            parallax: "mouse",
            parallaxBgFreeze: "on",
            parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
            
            keyboardNavigation: "off",
            
            navigationHAlign: "center",
            navigationVAlign: "bottom",
            navigationHOffset: 0,
            navigationVOffset: 40,
            
            soloArrowLeftHalign: "left",
            soloArrowLeftValign: "center",
            soloArrowLeftHOffset: 0,
            soloArrowLeftVOffset: 0,
            
            soloArrowRightHalign: "right",
            soloArrowRightValign: "center",
            soloArrowRightHOffset: 0,
            soloArrowRightVOffset: 0,
            
            shadow: 0,
            fullWidth: "on",
            fullScreen: "off",
            
            spinner: "spinner4",
            
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            
            shuffle: "off",
            
            autoHeight: "off",
            forceFullWidth: "on",
            
            hideThumbsOnMobile: "on",
            hideNavDelayOnMobile: 1500,
            hideBulletsOnMobile: "on",
            hideArrowsOnMobile: "on",
            hideThumbsUnderResolution: 0,
            
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            startWithSlide: 0,
            videoJsPath: "",
            fullScreenOffsetContainer: ""
        });
        
    } */
    
    //Accordion Box
    if ($('.accordion-box').length) {
        $(".accordion-box").on('click', '.acc-btn', function () {
            
            var outerBox = $(this).parents('.accordion-box');
            var target = $(this).parents('.accordion');
            
            if ($(this).hasClass('active') !== true) {
                $('.accordion .acc-btn').removeClass('active');
            }
            
            if ($(this).next('.acc-content').is(':visible')) {
                return false;
            } else {
                $(this).addClass('active');
                $(outerBox).children('.accordion').removeClass('active-block');
                $(outerBox).find('.accordion').children('.acc-content').slideUp(300);
                target.addClass('active-block');
                $(this).next('.acc-content').slideDown(300);
            }
        });
    }
    
    
    // Scroll to a Specific Div
    if ($('.scroll-to-target').length) {
        $(".scroll-to-target").on('click', function () {
            var target = $(this).attr('data-target');
            // animate
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 1500);
            
        });
    }
    
    $('#logo-slider, #client-slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 7,
        variableWidth: false,
        autoplay: true,
        autoplaySpeed: 3500,
        arrows: false,
        rtl: ($('body').attr('dir') == 'rtl' ? true : false),
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 599,
                settings: {
                    autoplay: false,
                    dots: false,
                    slidesToShow: 1,
                }
            }
        
        ]
    });
    $('#testi-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: false,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        dots: false,
        adaptiveHeight: true,
        rtl: ($('body').attr('dir') == 'rtl' ? true : false),
    });
    
    if ($(window).width() < 599) {
        $('#loc').slick({
            responsive: [
                {
                    breakpoint: 599,
                    settings: {
                        autoplay: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplaySpeed: 5000,
                        dots: false,
                        arrows: false,
                    }
                }
            
            ]
        });
    }
    
    var ihs = (function () {
        require("./components/common").init();
        var componentLoader = require('./modules/componentLoader');
        componentLoader
            .register("header", require("./components/header"))
            .register(".youtube-video", require('./components/youtubeVideo'))
            .register("#newsletter-signup", require('./components/newsletterSignup'))
            .load(document.body);
    })()
    /* ==========================================================================
     When document is ready, do
     ========================================================================== */
    
    $(document).ready(function () {
        
        handlePreloader();
        
        $(".hamburger-nav").on("click", function () {
            // $(".menu").fadeToggle("slow").toggleClass("menu-hide");
            $(".menu").animate({
                height: 'toggle'
            });
        });
    });
    
    /* ==========================================================================
     When document is Scrollig, do
     ========================================================================== */
    
    $(window).on('scroll', function () {
        stickyHeader();
        headerStyle();
        
    });
    
    /* ==========================================================================
     When document is loading, do
     ========================================================================== */
    
    $(window).on('load', function () {
        //new LazyLoad();


        const observer = lozad(); // lazy loads elements with default selector as '.lozad'
        observer.observe();
    });
    
})(window.jQuery);