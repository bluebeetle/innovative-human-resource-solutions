var util = require("./util");

/**
 * Registers and executes callbacks based on instances of an element
 *
 * @class ComponentLoader
 */
class ComponentLoader {

    constructor() {
        this.components = [];
    }

    /**
     * Register a component selector and callback
     *
     * @param  {string} sel - A CSS-like selector which will activate the component JS
     * @param  {function} componentCb - The function to invoke when the component is found on the page
     * @return {ComponentLoader}
     */
    register (sel, componentCb) {

        this.components.push({
            sel: sel,
            component: componentCb
        });

        return this;

    }

    /**
     * Activate the component JS code for each matched component element on the page
     *
     * @param  {HTMLElement} parentEl - the base element to find the component from
     * @return {ComponentLoader}
     */
    load (parentEl) {

            this.components.forEach((component) => {
                if($(component.sel, $(parentEl)).length)
                {
                    component.component.init(component.sel);
                }
            });


                /*function(component) {
                if($(component.sel, $(parentEl)).length)
                {
                    component.component.init(component.sel);
                }
            });*/

            /*(component) => {
            $(component.sel, $(parentEl)).each(function () {
                component.component.init(this);
            });
            }*/

        return this;

    }

};

module.exports = new ComponentLoader();
