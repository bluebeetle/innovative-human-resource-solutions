
class DropdownMenu {

    constructor(el, placeholder, options) {

        this.$dropdown = $(el);

        this.$placeholder = placeholder ? $(placeholder) : this.$dropdown.find('.placeholder');

        this.$options = options ? $(options) : this.$dropdown.find('.options > li');

        this.onChangeCb = null;

        this.val = '';

        this.index = -1;

    }

    init() {

        this.change();

        this.$dropdown.on('click', function(e){
            $(e.currentTarget).toggleClass('open');
            return false;
        });

        this.$options.on('click',(function(self){
            return function(e) {
                e.preventDefault();

                var $option = $(e.currentTarget);
                $option.addClass('active')
                    .siblings().removeClass('active');
                self.change($option);

                var href = $option.find("a").attr("href");
                if(href != '#' && href!='')
                {
                    window.location = href;
                }

            }
        })(this));

        $(document).click(() => this.$dropdown.removeClass('open'));

    }

    change($option) {
        if(! !! $option)
        {
            this.$options.each((index, el) =>  {
                if($(el).hasClass('active')) {
                    $option = $(el);
                }
            });
        }

        this.val = $option.text();

        this.index = $option.index();

        this.$placeholder.text(this.val);

        if(typeof this.onChangeCb === 'function') {
            this.onChangeCb.call(this.onChangeCb, this.val, $option.data('slug-value'));
        }
    }

    onChange(cb) {

        if(typeof cb !== 'function')
        {
            return false;
        }

        this.onChangeCb = cb;
    }
}

module.exports = DropdownMenu;
