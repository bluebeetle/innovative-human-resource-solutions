

// Helper methods to abstract and simplify code
module.exports = {
    isUndefined (arg) {
        return typeof arg === "undefined";
    },
    isDefined (arg) {
        return typeof arg !== "undefined";
    },
    isString (arg) {
        return typeof arg === "string";
    },
    isInteger (arg) {
        return arg === parseInt(arg);
    },
    isObject(arg) {
        if (typeof arg === "object") {
            var isObj = true;
            for (var prop in arg) {
                if (!arg.hasOwnProperty(prop)) {
                    isObj = false;
                }

                break;
            }
            return isObj;
        } else {
            return false;
        }
    },
    isNull (arg) {
        return arg === null;
    },
    isArray (arg) {
        return arg instanceof Array;
    },
    isFunction (arg) {
        return typeof arg === "function";
    },
    noop () {
        return false;
    },
    setDebugMode (el, isSet) {
        var classApplierMethod = isSet ? "addClass" : "removeClass";

        if (el) {
            $(el)[classApplierMethod]("debug");
        }
    },
    doIfExists (selector, ifExistsFn, ifNotExistsFn) {
        if (typeof selector !== "string" || !selector.length) {
            return false;
        }

        if ($(selector).length) {
            if (this.isFunction(ifExistsFn)) {
                ifExistsFn();
            }

            return true;
        } else {

            if (this.isFunction(ifNotExistsFn)) {
                ifNotExistsFn();
            }

            return false;
        }
    },
    getUniqueString() {
        return ("0000" + (Math.random()*Math.pow(36,4) << 0).toString(36)).slice(-4)
    },
    debounce(func, wait, immediate) {
    var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        }
    },
};
