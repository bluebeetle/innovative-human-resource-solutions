/*var components = [
    'AvoidConsoleErrors', 'Header', 'HeroVideo'
];

for(key in components)
{
    var component = components[key];
}*/
/*var avoidConsoleErrors = require('./components/avoidConsoleErrors.js');
var header = require('./components/header.js');
var homepageBGVideo = require('./components/homepageBGVideo.js');


$(function() {
    "use strict";

    avoidConsoleErrors.init();
    header.init();
    homepageBGVideo.init();


})*/
var ihs = (function() {

    require("./components/common").init();

    var componentLoader = require('./modules/componentLoader');

    componentLoader
        .register("header", require("./components/header"))
        //.register("[data-homepage-bg-video]", require('./components/homepageBGVideo'))
        .register(".tabs", require('./components/tabs'))
        .register("#logo-slider", require('./components/logoSlider'))
        .register(".youtube-video", require('./components/youtubeVideo'))
        .register("#newsletter-signup", require('./components/newsletterSignup'))
        .register("#our-locations", require('./components/ourLocations'))
        .register(".pagination:not(.page-1):not(.page-last)", require('./components/pagination'))
        .register(".masonry", require('./components/masonry'))
        .register("#team-slider", require('./components/teamSlider'))
        .register("#clients-grid", require('./components/clientsGrid'))
        .register("#careers-slider", require('./components/careersSlider'))
        .register("#contact-form", require('./components/contactForm'))
        .register("#contact-map", require('./components/contactMap'))
        .register("#calendar", require('./components/programCalendar'))
        .register(".select-box", require('./components/selectBox'))
        .register("#booking-form", require('./components/bookingForm'))
        .register(".read-more-box", require('./components/readMoreBox'))
        .register("#page-header #side-menu", require('./components/overviewMinHeight'))
        .register("#page-header #side-menu", require('./components/overviewResponsiveSideMenu'))
        .register("#page-body", require('./components/swapColumnsPositions'))
        .register(".page-tabs", require('./components/responsivePageTabs'))
        .register(".occasion-title", require('./components/responsiveOccasionTitle'))
        .register("#calendar", require('./components/responsiveCalendar'))
        .register(".page-tabs", require('./components/tabsPage'))
        .register("#occasion", require('./components/occasion'))
        .load(document.body);


})()
