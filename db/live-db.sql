-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 02, 2019 at 04:15 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `innovati_ihs_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_tinified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `awards`
--

INSERT INTO `awards` (`id`, `parent_id`, `name`, `lft`, `rgt`, `depth`, `logo`, `logo_tinified`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(9, NULL, 'DUBAI ONEHUNDRED SME Winner', 1, 2, 0, 'uploads/images/awards/0ade7c2cf97f75d009975f4d720d1fa6c19f4897.png', 0, 1, 4, 4, '2017-09-13 01:34:46', '2017-11-06 04:21:55'),
(10, NULL, 'Arabia fast growth 500 winner 2016', 3, 4, 0, 'uploads/images/awards/b1d5781111d84f7b3fe45a0852e59758cd7a87e5.png', 0, 1, 4, 4, '2017-09-13 01:41:35', '2017-11-05 07:10:38'),
(18, NULL, 'GulfCapital SME Award', 5, 6, 0, 'uploads/images/awards/9e6a55b6b4563e652a23be9d623ca5055c356940.png', 0, 1, 4, 2, '2017-09-13 05:27:38', '2018-05-23 03:46:29');

-- --------------------------------------------------------

--
-- Table structure for table `award_translations`
--

CREATE TABLE `award_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `award_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `award_translations`
--

INSERT INTO `award_translations` (`id`, `award_id`, `locale`, `title`, `content`) VALUES
(9, 9, 'en', 'DUBAI ONEHUNDRED SME Winner', ''),
(10, 10, 'en', 'Arabia fast growth 500 winner 2016', ''),
(18, 18, 'en', 'GulfCapital SME Award', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'News', 'news', '2016-07-12 08:58:40', '2016-07-12 08:58:56', 1, 1),
(2, 'Events', 'events', '2016-08-07 23:41:46', '2018-05-23 03:47:21', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_translations`
--

CREATE TABLE `blog_category_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_category_translations`
--

INSERT INTO `blog_category_translations` (`id`, `category_id`, `locale`, `title`) VALUES
(1, 1, 'en', 'News'),
(2, 1, 'ar', 'News Arabic'),
(3, 2, 'en', 'Events');

-- --------------------------------------------------------

--
-- Table structure for table `casestudies`
--

CREATE TABLE `casestudies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sector_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `casestudies`
--

INSERT INTO `casestudies` (`id`, `name`, `slug`, `category_id`, `sector_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'LEADing Hoteliers! Maximising Excellence in Rotana Leaders', 'leading-hoteliers-maximising-excellence-in-rotana-leaders', 3, 2, 1, 3, 2, '2016-08-29 03:28:50', '2016-11-29 12:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `casestudy_categories`
--

CREATE TABLE `casestudy_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `casestudy_categories`
--

INSERT INTO `casestudy_categories` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'News', '2016-07-10 10:04:25', '2016-07-12 09:20:31', 1, 1),
(2, 'Events', '2016-07-10 10:32:57', '2018-05-23 03:47:06', 1, 2),
(3, 'Case Study', '2016-08-29 03:26:42', '2016-11-29 12:51:12', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `casestudy_category_translations`
--

CREATE TABLE `casestudy_category_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `casestudy_category_translations`
--

INSERT INTO `casestudy_category_translations` (`id`, `category_id`, `locale`, `title`) VALUES
(1, 1, 'en', 'News'),
(2, 2, 'en', 'Events'),
(3, 1, 'ar', 'News'),
(4, 2, 'ar', 'Events'),
(5, 3, 'en', 'Case Study'),
(6, 3, 'ar', 'Case Study');

-- --------------------------------------------------------

--
-- Table structure for table `casestudy_sectors`
--

CREATE TABLE `casestudy_sectors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `casestudy_sectors`
--

INSERT INTO `casestudy_sectors` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Oil & Gas', '2016-07-10 10:40:47', '2016-07-12 09:19:55', 1, 1),
(2, 'Tourism & Hospitality', '2016-08-29 03:27:04', '2016-11-29 12:51:29', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `casestudy_sector_translations`
--

CREATE TABLE `casestudy_sector_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `sector_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `casestudy_sector_translations`
--

INSERT INTO `casestudy_sector_translations` (`id`, `sector_id`, `locale`, `title`) VALUES
(1, 1, 'en', 'Oil & Gas'),
(2, 1, 'ar', 'Oil & Gas'),
(3, 2, 'en', 'Tourism & Hospitality'),
(4, 2, 'ar', 'Tourism & Hospitality');

-- --------------------------------------------------------

--
-- Table structure for table `casestudy_translations`
--

CREATE TABLE `casestudy_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `casestudy_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `casestudy_translations`
--

INSERT INTO `casestudy_translations` (`id`, `casestudy_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(4, 4, 'en', 'LEADing Hoteliers! Maximising Excellence in Rotana Leaders', '<h3 style=\"text-align: justify;\"><strong>The Leadership Excellence and Development (LEAD) programme was formulated in recognition of the critical importance of outstanding leadership to the future growth of Rotana.</strong></h3>\r\n<p style=\"text-align: justify;\">Rotana is a leading hotel management company in the&nbsp;Middle East, African and South Asian&nbsp;regions. Founded in 1992, it has a portfolio of over 100 properties in 26 cities. Rotana values it staff and prides itself on working tirelessly to maintain and develop talent in all capacities.</p>\r\n<p style=\"text-align: justify;\">In 2011, IHS were approached to offer human capital services to map out the future development initiatives for Rotana leaders. The goal of this initiative was to install a self-managed internal selection and development process that adapted to high potential, talented individuals. This programme was to optimize the efficiency of Human Resource efforts, and manage the expectations of interested parties.</p>\r\n<p style=\"text-align: justify;\">Together, Rotana and IHS developed distinctive strategies across three job levels as part of the LEAD Series. &nbsp;The LEAD series is an all encompassed, yet simple approach, to Assessment and Development, inclusive of centres, Personal Development Plans, feedback reports, coaching elements and psychometrics for GMs, EAMs and Department Heads across leading Middle Eastern Hotel chains.</p>\r\n<p style=\"text-align: justify;\">The programme resulted in successfully identifying talented individuals and working with them to ensure long term commitment to personal growth and, in turn, success for Rotana. 50 people took part across all three levels of the Programme that ran across 4 years, in a series that arguably puts Rotana at the forefront of excellent Hotelier HR practice, not only in this region- but globally. IHS continues to offer support services to Rotana to continually enhance their HR practices across their hotel properties.</p>\r\n<p style=\"text-align: justify;\">&rdquo;Once again, a million &lsquo;Thank You&rsquo;s&rdquo;</p>\r\n<p style=\"text-align: justify;\"><strong>BUSINESS BENEFITS</strong></p>\r\n<ul>\r\n<li style=\"text-align: justify;\">Consistent, centralised and clarified approaches to selection and development activities.</li>\r\n<li>\r\n<p style=\"text-align: justify;\">Achieving whole person development &nbsp;by facilitating tailor made activities, enabling learning and carving appropriate channels for meaningful communication.</p>\r\n</li>\r\n</ul>\r\n<h2>&nbsp;</h2>', '', '', ''),
(6, 4, 'ar', 'LEADing Hoteliers! Maximising Excellence in Rotana Leaders', '<h3 style=\"text-align: justify;\"><strong>The Leadership Excellence and Development (LEAD) programme was formulated in recognition of the critical importance of outstanding leadership to the future growth of Rotana.</strong></h3>\r\n<p style=\"text-align: justify;\">Rotana is a leading hotel management company in the&nbsp;Middle East, African and South Asian&nbsp;regions. Founded in 1992, it has a portfolio of over 100 properties in 26 cities. Rotana values it staff and prides itself on working tirelessly to maintain and develop talent in all capacities.</p>\r\n<p style=\"text-align: justify;\">In 2011, IHS were approached to offer human capital services to map out the future development initiatives for Rotana leaders. The goal of this initiative was to install a self-managed internal selection and development process that adapted to high potential, talented individuals. This programme was to optimize the efficiency of Human Resource efforts, and manage the expectations of interested parties.</p>\r\n<p style=\"text-align: justify;\">Together, Rotana and IHS developed distinctive strategies across three job levels as part of the LEAD Series. &nbsp;The LEAD series is an all encompassed, yet simple approach, to Assessment and Development, inclusive of centres, Personal Development Plans, feedback reports, coaching elements and psychometrics for GMs, EAMs and Department Heads across leading Middle Eastern Hotel chains.</p>\r\n<p style=\"text-align: justify;\">The programme resulted in successfully identifying talented individuals and working with them to ensure long term commitment to personal growth and, in turn, success for Rotana. 50 people took part across all three levels of the Programme that ran across 4 years, in a series that arguably puts Rotana at the forefront of excellent Hotelier HR practice, not only in this region- but globally. IHS continues to offer support services to Rotana to continually enhance their HR practices across their hotel properties.</p>\r\n<p style=\"text-align: justify;\">&rdquo;Once again, a million &lsquo;Thank You&rsquo;s&rdquo;</p>\r\n<p style=\"text-align: justify;\"><strong>BUSINESS BENEFITS</strong></p>\r\n<ul style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">\r\n<li style=\"text-align: justify;\">Consistent, centralised and clarified approaches to selection and development activities.</li>\r\n<li>\r\n<p style=\"text-align: justify;\">Achieving whole person development &nbsp;by facilitating tailor made activities, enabling learning and carving appropriate channels for meaningful communication.</p>\r\n</li>\r\n</ul>\r\n<h2 style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">&nbsp;</h2>', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE `category_post` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_post`
--

INSERT INTO `category_post` (`category_id`, `post_id`) VALUES
(1, 4),
(1, 16),
(2, 16),
(1, 17),
(2, 17),
(1, 18),
(1, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(1, 26);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_tinified` int(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `parent_id`, `name`, `lft`, `rgt`, `depth`, `logo`, `logo_tinified`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(6, NULL, 'Abu Dhabi Aviation', 3, 4, 0, 'uploads/images/clients/c1dfd96eea8cc2b62785275bca38ac261256e278.png', 0, 1, 1, 2, '2016-07-11 10:35:22', '2016-11-24 15:06:23'),
(14, NULL, 'Etihad Airways', 5, 6, 0, 'uploads/images/clients/fa35e192121eabf3dabf9f5ea6abdbcbc107ac3b.png', 0, 1, 1, 4, '2016-07-11 12:27:13', '2018-12-31 08:35:53'),
(15, NULL, 'Airbus', 9, 10, 0, 'uploads/images/clients/f1abd670358e036c31296e66b3b66c382ac00812.png', 0, 1, 1, 4, '2016-08-09 05:55:45', '2018-12-31 08:34:36'),
(16, NULL, 'National Bank of Fujairah', 7, 8, 0, 'uploads/images/clients/1574bddb75c78a6fd2251d61e2993b5146201319.png', 0, 1, 1, 2, '2016-08-09 05:56:04', '2016-11-24 15:06:56'),
(17, NULL, 'Abu Dhabi Investment Authority', 11, 12, 0, 'uploads/images/clients/0716d9708d321ffb6a00818614779e779925365c.jpg', 0, 0, 1, 4, '2016-08-09 05:59:11', '2018-12-30 07:12:58'),
(18, NULL, 'Qatar Financial Centre Regulatory Authority', 13, 14, 0, 'uploads/images/clients/9e6a55b6b4563e652a23be9d623ca5055c356940.jpg', 0, 1, 2, 2, '2016-08-12 06:36:04', '2016-11-24 15:07:48'),
(19, NULL, 'Abu Dhabi Commercial Bank', 15, 16, 0, 'uploads/images/clients/b3f0c7f6bb763af1be91d9e74eabfeb199dc1f1f.png', 0, 1, 2, 4, '2016-08-12 06:36:57', '2018-12-31 08:36:59'),
(20, NULL, 'Alinma Bank', 17, 18, 0, 'uploads/images/clients/91032ad7bbcb6cf72875e8e8207dcfba80173f7c.jpg', 0, 1, 2, 2, '2016-08-12 06:39:31', '2016-11-24 15:08:20'),
(21, NULL, 'Dubai International Financial Centre', 19, 20, 0, 'uploads/images/clients/472b07b9fcf2c2451e8781e944bf5f77cd8457c8.png', 0, 1, 2, 4, '2016-08-12 06:41:14', '2018-12-31 08:37:59'),
(22, NULL, 'Emirates NDB', 21, 22, 0, 'uploads/images/clients/12c6fc06c99a462375eeb3f43dfd832b08ca9e17.png', 0, 1, 2, 2, '2016-08-12 06:41:54', '2016-11-24 15:08:53'),
(23, NULL, 'HSBC', 23, 24, 0, 'uploads/images/clients/d435a6cdd786300dff204ee7c2ef942d3e9034e2.png', 0, 1, 2, 2, '2016-08-12 06:42:55', '2016-11-24 15:09:09'),
(25, NULL, 'Mashreq', 25, 26, 0, 'uploads/images/clients/f6e1126cedebf23e1463aee73f9df08783640400.jpg', 0, 1, 2, 2, '2016-08-12 06:43:44', '2018-05-23 03:46:16'),
(26, NULL, 'Mastercard', 27, 28, 0, 'uploads/images/clients/887309d048beef83ad3eabf2a79a64a389ab1c9f.jpg', 0, 1, 2, 4, '2016-08-12 06:44:16', '2018-12-31 08:31:31'),
(27, NULL, 'Jumeirah Group', 29, 30, 0, 'uploads/images/clients/bc33ea4e26e5e1af1408321416956113a4658763.jpg', 0, 1, 2, 2, '2016-08-12 06:44:53', '2016-11-24 15:11:11'),
(28, NULL, 'Tecom Group', 31, 32, 0, 'uploads/images/clients/0a57cb53ba59c46fc4b692527a38a87c78d84028.png', 0, 1, 2, 2, '2016-08-12 06:45:38', '2016-11-24 15:11:26'),
(29, NULL, 'Gems Education', 33, 34, 0, 'uploads/images/clients/7719a1c782a1ba91c031a682a0a2f8658209adbf.png', 0, 1, 2, 4, '2016-08-12 06:46:11', '2018-12-31 08:34:18'),
(30, NULL, 'Besix', 35, 36, 0, 'uploads/images/clients/22d200f8670dbdb3e253a90eee5098477c95c23d.jpg', 0, 1, 2, 2, '2016-08-12 06:47:10', '2016-11-24 15:11:55'),
(31, NULL, 'Aujan Group Holding', 37, 38, 0, 'uploads/images/clients/632667547e7cd3e0466547863e1207a8c0c0c549.png', 0, 1, 2, 2, '2016-08-12 06:47:45', '2016-11-24 15:12:11'),
(32, NULL, 'Dubai World Trade Centre', 39, 40, 0, 'uploads/images/clients/cb4e5208b4cd87268b208e49452ed6e89a68e0b8.png', 0, 1, 2, 2, '2016-08-12 06:48:28', '2016-11-24 15:12:28'),
(33, NULL, 'Emaar', 41, 42, 0, 'uploads/images/clients/b6692ea5df920cad691c20319a6fffd7a4a766b8.jpg', 0, 1, 2, 2, '2016-08-12 06:49:07', '2016-11-24 15:12:43'),
(34, NULL, 'Dubai Customs', 43, 44, 0, 'uploads/images/clients/f1f836cb4ea6efb2a0b1b99f41ad8b103eff4b59.jpg', 0, 1, 2, 2, '2016-08-12 06:50:00', '2016-11-24 15:13:04'),
(35, NULL, 'Dubai Courts', 45, 46, 0, 'uploads/images/clients/972a67c48192728a34979d9a35164c1295401b71.png', 0, 1, 2, 2, '2016-08-12 06:50:37', '2016-11-24 15:13:23'),
(36, NULL, 'Government of Dubai', 47, 48, 0, 'uploads/images/clients/fc074d501302eb2b93e2554793fcaf50b3bf7291.jpg', 0, 1, 2, 2, '2016-08-12 06:51:15', '2016-11-24 15:13:40'),
(37, NULL, 'Ministry of Labour', 49, 50, 0, 'uploads/images/clients/cb7a1d775e800fd1ee4049f7dca9e041eb9ba083.jpg', 0, 1, 2, 2, '2016-08-12 06:51:50', '2016-11-24 15:14:00'),
(38, NULL, 'MOD KSA', 51, 52, 0, 'uploads/images/clients/5b384ce32d8cdef02bc3a139d4cac0a22bb029e8.png', 0, 1, 2, 2, '2016-08-12 06:53:24', '2016-11-24 15:17:40'),
(39, NULL, 'Atkins Group', 53, 54, 0, 'uploads/images/clients/ca3512f4dfa95a03169c5a670a4c91a19b3077b4.png', 0, 1, 2, 2, '2016-08-12 06:53:47', '2016-11-24 15:17:56'),
(40, NULL, 'Mondelez International', 55, 56, 0, 'uploads/images/clients/af3e133428b9e25c55bc59fe534248e6a0c0f17b.png', 0, 1, 2, 2, '2016-08-12 06:54:21', '2016-11-24 15:18:13'),
(41, NULL, 'The Coca-Cola Company', 57, 58, 0, 'uploads/images/clients/761f22b2c1593d0bb87e0b606f990ba4974706de.jpg', 0, 1, 2, 2, '2016-08-12 06:55:13', '2016-11-24 15:18:30'),
(42, NULL, 'Nestle', 59, 60, 0, 'uploads/images/clients/92cfceb39d57d914ed8b14d0e37643de0797ae56.png', 0, 1, 2, 2, '2016-08-12 06:55:36', '2016-11-24 15:18:44'),
(43, NULL, 'The Boston Consulting Group', 61, 62, 0, 'uploads/images/clients/0286dd552c9bea9a69ecb3759e7b94777635514b.jpg', 0, 1, 2, 2, '2016-08-12 06:56:10', '2016-11-24 15:19:02'),
(44, NULL, 'Aggreko', 63, 64, 0, 'uploads/images/clients/98fbc42faedc02492397cb5962ea3a3ffc0a9243.jpg', 0, 1, 2, 2, '2016-08-12 06:58:14', '2016-11-24 15:19:15'),
(45, NULL, 'Abu Dhabi Company for Onshore Petroleum Operations', 65, 66, 0, 'uploads/images/clients/fb644351560d8296fe6da332236b1f8d61b2828a.jpg', 0, 0, 2, 4, '2016-08-12 06:58:54', '2018-12-31 08:43:44'),
(46, NULL, 'Al Ain Distribution Company', 67, 68, 0, 'uploads/images/clients/fe2ef495a1152561572949784c16bf23abb28057.jpg', 0, 1, 2, 2, '2016-08-12 06:59:37', '2016-11-24 15:19:48'),
(47, NULL, 'RasGas', 69, 70, 0, 'uploads/images/clients/827bfc458708f0b442009c9c9836f7e4b65557fb.jpg', 0, 0, 2, 4, '2016-08-12 07:00:24', '2018-12-30 06:33:29'),
(48, NULL, 'Adgas', 71, 72, 0, 'uploads/images/clients/64e095fe763fc62418378753f9402623bea9e227.jpg', 0, 0, 2, 4, '2016-08-12 07:14:27', '2018-12-30 06:33:42'),
(49, NULL, 'Adma Opco', 73, 74, 0, 'uploads/images/clients/2e01e17467891f7c933dbaa00e1459d23db3fe4f.jpg', 0, 0, 2, 4, '2016-08-12 07:14:54', '2018-12-30 06:33:12'),
(50, NULL, 'Aecom', 75, 76, 0, 'uploads/images/clients/e1822db470e60d090affd0956d743cb0e7cdf113.png', 0, 1, 2, 4, '2016-08-12 07:15:38', '2018-12-31 08:44:58'),
(51, NULL, 'Adnoc', 77, 78, 0, 'uploads/images/clients/b7eb6c689c037217079766fdb77c3bac3e51cb4c.png', 0, 1, 2, 4, '2016-08-12 07:17:53', '2018-12-31 08:30:14'),
(52, NULL, 'Dusup', 79, 80, 0, 'uploads/images/clients/a9334987ece78b6fe8bf130ef00b74847c1d3da6.jpg', 0, 1, 2, 2, '2016-08-12 07:18:47', '2016-11-24 15:21:00'),
(53, NULL, 'Enoc', 81, 82, 0, 'uploads/images/clients/c5b76da3e608d34edb07244cd9b875ee86906328.jpg', 0, 1, 2, 2, '2016-08-12 07:19:24', '2016-11-24 15:21:30'),
(54, NULL, 'Fertil', 83, 84, 0, 'uploads/images/clients/80e28a51cbc26fa4bd34938c5e593b36146f5e0c.png', 0, 0, 2, 4, '2016-08-12 07:19:59', '2018-12-30 06:38:50'),
(55, NULL, 'Emarat', 85, 86, 0, 'uploads/images/clients/8effee409c625e1a2d8f5033631840e6ce1dcb64.jpg', 0, 1, 2, 2, '2016-08-12 07:20:33', '2016-11-24 15:21:45'),
(56, NULL, 'Ma aden', 87, 88, 0, 'uploads/images/clients/54ceb91256e8190e474aa752a6e0650a2df5ba37.jpg', 0, 1, 2, 2, '2016-08-12 07:21:23', '2016-11-24 15:53:57'),
(57, NULL, 'NDC', 89, 90, 0, 'uploads/images/clients/9109c85a45b703f87f1413a405549a2cea9ab556.jpg', 0, 1, 2, 2, '2016-08-12 07:21:46', '2016-11-24 15:54:08'),
(58, NULL, 'NPCC', 91, 92, 0, 'uploads/images/clients/667be543b02294b7624119adc3a725473df39885.jpg', 0, 1, 2, 2, '2016-08-12 07:22:39', '2016-11-24 15:54:19'),
(59, NULL, 'Qatar Foundation', 93, 94, 0, 'uploads/images/clients/5a5b0f9b7d3f8fc84c3cef8fd8efaaa6c70d75ab.jpg', 0, 1, 2, 2, '2016-08-12 07:23:09', '2016-11-24 15:54:30'),
(60, NULL, 'Qatargas', 95, 96, 0, 'uploads/images/clients/e6c3dd630428fd54834172b8fd2735fed9416da4.jpg', 0, 1, 2, 2, '2016-08-12 07:23:41', '2016-11-24 15:55:51'),
(61, NULL, 'Qatar Petroleum', 97, 98, 0, 'uploads/images/clients/6c1e671f9af5b46d9c1a52067bdf0e53685674f7.jpg', 0, 1, 2, 2, '2016-08-12 07:24:08', '2016-11-24 15:56:06'),
(62, NULL, 'Merck', 99, 100, 0, 'uploads/images/clients/511a418e72591eb7e33f703f04c3fa16df6c90bd.jpg', 0, 1, 2, 2, '2016-08-12 07:24:37', '2016-11-24 15:56:16'),
(63, NULL, 'Novo Nordisk', 101, 102, 0, 'uploads/images/clients/a17554a0d2b15a664c0e73900184544f19e70227.jpg', 0, 1, 2, 2, '2016-08-12 07:25:15', '2016-11-24 15:56:26'),
(64, NULL, 'Roche', 103, 104, 0, 'uploads/images/clients/c66c65175fecc3103b3b587be9b5b230889c8628.jpg', 0, 1, 2, 2, '2016-08-12 07:25:41', '2016-11-24 15:56:37'),
(65, NULL, 'Wyeth', 105, 106, 0, 'uploads/images/clients/2a459380709e2fe4ac2dae5733c73225ff6cfee1.png', 0, 1, 2, 2, '2016-08-12 07:26:22', '2016-11-24 15:56:50'),
(66, NULL, 'AstraZeneca', 107, 108, 0, 'uploads/images/clients/59129aacfb6cebbe2c52f30ef3424209f7252e82.png', 0, 1, 2, 2, '2016-08-12 07:27:00', '2016-11-24 17:52:02'),
(67, NULL, 'Aspetar', 109, 110, 0, 'uploads/images/clients/4d89d294cd4ca9f2ca57dc24a53ffb3ef5303122.jpg', 0, 1, 2, 2, '2016-08-12 07:27:41', '2016-11-24 17:52:11'),
(68, NULL, 'King Faisal Specialist Hospital & Research Centre', 111, 112, 0, 'uploads/images/clients/b4c96d80854dd27e76d8cc9e21960eebda52e962.jpg', 0, 1, 2, 2, '2016-08-12 07:28:19', '2016-11-24 17:52:24'),
(69, NULL, 'Lilly', 113, 114, 0, 'uploads/images/clients/a72b20062ec2c47ab2ceb97ac1bee818f8b6c6cb.png', 0, 1, 2, 2, '2016-08-12 07:28:47', '2016-11-24 17:52:33'),
(70, NULL, 'Hospira', 115, 116, 0, 'uploads/images/clients/b7103ca278a75cad8f7d065acda0c2e80da0b7dc.png', 0, 1, 2, 2, '2016-08-12 07:29:32', '2016-11-24 17:52:44'),
(71, NULL, 'Etisalat', 117, 118, 0, 'uploads/images/clients/d02560dd9d7db4467627745bd6701e809ffca6e3.png', 0, 1, 2, 2, '2016-08-12 07:30:06', '2016-11-24 17:54:09'),
(72, NULL, 'Ooredoo', 119, 120, 0, 'uploads/images/clients/c097638f92de80ba8d6c696b26e6e601a5f61eb7.jpg', 0, 1, 2, 2, '2016-08-12 07:30:37', '2016-11-24 17:54:21'),
(73, NULL, 'Du', 121, 122, 0, 'uploads/images/clients/35e995c107a71caeb833bb3b79f9f54781b33fa1.jpg', 0, 1, 2, 2, '2016-08-12 07:31:00', '2016-11-24 17:54:31'),
(74, NULL, 'Roads & Transport Authority', 123, 124, 0, 'uploads/images/clients/1f1362ea41d1bc65be321c0a378a20159f9a26d0.jpg', 0, 1, 2, 2, '2016-08-12 07:31:39', '2016-11-24 17:54:40'),
(75, NULL, 'Dubai Taxi Corporation', 125, 126, 0, 'uploads/images/clients/450ddec8dd206c2e2ab1aeeaa90e85e51753b8b7.jpg', 0, 1, 2, 2, '2016-08-12 07:32:19', '2016-11-24 17:55:00'),
(76, NULL, 'National Bank of Abu Dhabi', 127, 128, 0, 'uploads/images/clients/d54ad009d179ae346683cfc3603979bc99339ef7.jpg', 0, 1, 2, 4, '2016-08-12 07:33:02', '2018-12-30 06:38:02'),
(77, NULL, 'PAE', 1, 2, 0, 'uploads/images/clients/d321d6f7ccf98b51540ec9d933f20898af3bd71e.png', 0, 1, 3, 2, '2016-09-21 08:12:56', '2016-11-24 15:05:41');

-- --------------------------------------------------------

--
-- Table structure for table `client_translations`
--

CREATE TABLE `client_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client_translations`
--

INSERT INTO `client_translations` (`id`, `client_id`, `locale`, `title`, `content`) VALUES
(5, 6, 'en', 'Abu Dhabi Aviation', ''),
(20, 14, 'en', 'Etihad Airways', ''),
(21, 15, 'en', 'Airbus', ''),
(22, 16, 'en', 'National Bank of Fujairah', ''),
(23, 17, 'en', 'Abu Dhabi Investment Authority', ''),
(24, 18, 'en', 'Qatar Financial Centre Regulatory Authority', ''),
(25, 19, 'en', 'Abu Dhabi Commercial Bank', ''),
(26, 20, 'en', 'Alinma Bank', ''),
(27, 21, 'en', 'Dubai International Financial Centre', ''),
(28, 22, 'en', 'Emirates NDB', ''),
(29, 23, 'en', 'HSBC', ''),
(31, 25, 'en', 'Mashreq', ''),
(32, 26, 'en', 'Mastercard', ''),
(33, 27, 'en', 'Jumeirah Group', ''),
(34, 28, 'en', 'Tecom Group', ''),
(35, 29, 'en', 'Gems Education', ''),
(36, 30, 'en', 'Besix', ''),
(37, 31, 'en', 'Aujan Group Holding', ''),
(38, 32, 'en', 'Dubai World Trade Centre', ''),
(39, 33, 'en', 'Emaar', ''),
(40, 34, 'en', 'Dubai Customs', ''),
(41, 35, 'en', 'Dubai Courts', ''),
(42, 36, 'en', 'Government of Dubai', ''),
(43, 37, 'en', 'Ministry of Labour', ''),
(44, 38, 'en', 'MOD KSA', ''),
(45, 39, 'en', 'Atkins Group', ''),
(46, 40, 'en', 'Mondelez International', ''),
(47, 41, 'en', 'The Coca-Cola Company', ''),
(48, 42, 'en', 'Nestle', ''),
(49, 43, 'en', 'The Boston Consulting Group', ''),
(50, 44, 'en', 'Aggreko', ''),
(51, 45, 'en', 'Abu Dhabi Company for Onshore Petroleum Operations', ''),
(52, 46, 'en', 'Al Ain Distribution Company', ''),
(53, 47, 'en', 'RasGas', ''),
(54, 48, 'en', 'Adgas', ''),
(55, 49, 'en', 'Adma Opco', ''),
(56, 50, 'en', 'Aecom', ''),
(57, 51, 'en', 'Adnoc', ''),
(58, 52, 'en', 'Dusup', ''),
(59, 53, 'en', 'Enoc', ''),
(60, 54, 'en', 'Fertil', ''),
(61, 55, 'en', 'Emarat', ''),
(62, 56, 'en', 'Ma aden', ''),
(63, 57, 'en', 'NDC', ''),
(64, 58, 'en', 'NPCC', ''),
(65, 59, 'en', 'Qatar Foundation', ''),
(66, 60, 'en', 'Qatargas', ''),
(67, 61, 'en', 'Qatar Petroleum', ''),
(68, 62, 'en', 'Merck', ''),
(69, 63, 'en', 'Novo Nordisk', ''),
(70, 64, 'en', 'Roche', ''),
(71, 65, 'en', 'Wyeth', ''),
(72, 66, 'en', 'AstraZeneca', ''),
(73, 67, 'en', 'Aspetar', ''),
(74, 68, 'en', 'King Faisal Specialist Hospital & Research Centre', ''),
(75, 69, 'en', 'Lilly', ''),
(76, 70, 'en', 'Hospira', ''),
(77, 71, 'en', 'Etisalat', ''),
(78, 72, 'en', 'Ooredoo', ''),
(79, 73, 'en', 'Du', ''),
(80, 74, 'en', 'Roads & Transport Authority', ''),
(81, 75, 'en', 'Dubai Taxi Corporation', ''),
(82, 76, 'en', 'FAB First Abu Dhabi Bank', ''),
(83, 77, 'en', 'PAE', '\'You delivered the training perfectly - we now understand much more about ourselves, our attitude and our work styles. I think this has really helped us understand one another and how we can interact so much better. You really listened to us and gave us applicable examples of how we can use MBTI at work\' - MBTI Workshop, HR Department, PAE Government Services '),
(84, 77, 'ar', 'PAE', '<p><span style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; display: inline !important; float: none;\">\'You delivered the training perfectly - we now understand much more about ourselves, our attitude and our work styles. I think this has really helped us understand one another and how we can interact so much better. You really listened to us and gave us applicable examples of how we can use MBTI at work\' - MBTI Workshop, HR Department, PAE Government Services</span></p>'),
(85, 6, 'ar', 'Abu Dhabi Aviation', ''),
(86, 14, 'ar', 'Etihad Airways', ''),
(87, 16, 'ar', 'National Bank of Fujairah', ''),
(88, 15, 'ar', 'Airbus', ''),
(89, 17, 'ar', 'Abu Dhabi Investment Authority', ''),
(90, 18, 'ar', 'Qatar Financial Centre Regulatory Authority', ''),
(91, 19, 'ar', 'Abu Dhabi Commercial Bank', ''),
(92, 20, 'ar', 'Alinma Bank', ''),
(93, 21, 'ar', 'Dubai International Financial Centre', ''),
(94, 22, 'ar', 'Emirates NDB', ''),
(95, 23, 'ar', 'HSBC', ''),
(96, 25, 'ar', 'Mashreq', ''),
(97, 26, 'ar', 'Mastercard', ''),
(98, 27, 'ar', 'Jumeirah Group', ''),
(99, 28, 'ar', 'Tecom Group', ''),
(100, 29, 'ar', 'Gems Education', ''),
(101, 30, 'ar', 'Besix', ''),
(102, 31, 'ar', 'Aujan Group Holding', ''),
(103, 32, 'ar', 'Dubai World Trade Centre', ''),
(104, 33, 'ar', 'Emaar', ''),
(105, 34, 'ar', 'Dubai Customs', ''),
(106, 35, 'ar', 'Dubai Courts', ''),
(107, 36, 'ar', 'Government of Dubai', ''),
(108, 37, 'ar', 'Ministry of Labour', ''),
(109, 38, 'ar', 'MOD KSA', ''),
(110, 39, 'ar', 'Atkins Group', ''),
(111, 40, 'ar', 'Mondelez International', ''),
(112, 41, 'ar', 'The Coca-Cola Company', ''),
(113, 42, 'ar', 'Nestle', ''),
(114, 43, 'ar', 'The Boston Consulting Group', ''),
(115, 44, 'ar', 'Aggreko', ''),
(116, 45, 'ar', 'Abu Dhabi Company for Onshore Petroleum Operations', ''),
(117, 46, 'ar', 'Al Ain Distribution Company', ''),
(118, 47, 'ar', 'RasGas', ''),
(119, 48, 'ar', 'Adgas', ''),
(120, 49, 'ar', 'Adma Opco', ''),
(121, 50, 'ar', 'Aecom', ''),
(122, 51, 'ar', 'Adnoc', ''),
(123, 52, 'ar', 'Dusup', ''),
(124, 54, 'ar', 'Fertil', ''),
(125, 53, 'ar', 'Enoc', ''),
(126, 55, 'ar', 'Emarat', ''),
(127, 56, 'ar', 'Ma aden', ''),
(128, 57, 'ar', 'NDC', ''),
(129, 58, 'ar', 'NPCC', ''),
(130, 59, 'ar', 'Qatar Foundation', ''),
(131, 60, 'ar', 'Qatargas', ''),
(132, 61, 'ar', 'Qatar Petroleum', ''),
(133, 62, 'ar', 'Merck', ''),
(134, 63, 'ar', 'Novo Nordisk', ''),
(135, 64, 'ar', 'Roche', ''),
(136, 65, 'ar', 'Wyeth', ''),
(137, 66, 'ar', 'AstraZeneca', ''),
(138, 67, 'ar', 'Aspetar', ''),
(139, 68, 'ar', 'King Faisal Specialist Hospital & Research Centre', ''),
(140, 69, 'ar', 'Lilly', ''),
(141, 70, 'ar', 'Hospira', ''),
(142, 71, 'ar', 'Etisalat', ''),
(143, 72, 'ar', 'Ooredoo', ''),
(144, 73, 'ar', 'Du', ''),
(145, 74, 'ar', 'Roads & Transport Authority', ''),
(146, 76, 'ar', 'National Bank of Abu Dhabi', ''),
(147, 75, 'ar', 'Dubai Taxi Corporation', '');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_tinified` int(1) NOT NULL DEFAULT '0',
  `cost` decimal(8,2) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `parent_id`, `name`, `slug`, `lft`, `rgt`, `depth`, `logo`, `logo_tinified`, `cost`, `link`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 0, 'MBTI Certification Programme', 'mbti-certificate-program', 1, 2, 0, 'uploads/images/courses/1b6453892473a467d07372d45eb05abc2031647a.png', 0, '15000.00', 'calendar/course/4/mbti-certificate-program', 1, 1, 4, '2016-08-02 02:52:48', '2018-12-12 02:54:40'),
(6, 0, 'BPS Qualification in Occupational Testing ', 'bps-qualification-in-occupation-testing', 3, 4, 0, 'uploads/images/courses/c1dfd96eea8cc2b62785275bca38ac261256e278.png', 0, '13000.00', '/calendar/course/6/bps-qualification-in-occupation-testing', 1, 1, 4, '2016-08-03 07:17:48', '2018-12-12 02:55:15'),
(7, 0, 'BPS Qualification: Assistant Test User', 'bps-qualification-assistant-test-user', 24, 25, 0, 'uploads/images/courses/902ba3cda1883801594b6e1b452790cc53948fda.png', 0, '3500.00', '/calendar/course/6/bps-qualification-in-occupation-testing', 0, 2, 3, '2016-08-13 07:22:41', '2018-03-06 13:24:36'),
(8, 0, 'EQi 2.0 & EQ360 ', 'eqi-20-and-eq360', 7, 8, 0, 'uploads/images/courses/fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f.png', 0, '7000.00', '/calendar/course/8/eqi-20-and-eq360', 1, 2, 4, '2016-08-13 07:27:25', '2018-10-14 08:50:11'),
(10, 0, 'Strong Interest Inventory', 'strong-interest-inventory', 10, 11, 0, 'uploads/images/courses/b1d5781111d84f7b3fe45a0852e59758cd7a87e5.png', 0, '4000.00', '/calendar/course/10/strong-interest-inventory', 1, 2, 4, '2016-08-13 07:38:19', '2018-10-14 08:45:50'),
(11, 0, 'FIRO Business', 'firo-business', 14, 15, 0, 'uploads/images/courses/17ba0791499db908433b80f37c5fbc89b870084b.png', 0, '5500.00', 'calendar/course/11/firo-business', 1, 2, 4, '2016-08-13 07:40:56', '2018-10-14 08:48:37'),
(13, 0, 'AFC Executive Coaching Certification ', 'afc-executive-coaching-certification', 12, 13, 0, 'uploads/images/courses/bd307a3ec329e10a2cff8fb87480823da114f8f4.png', 0, '14750.00', 'calendar/course/13/afc-executive-coaching-certification', 1, 2, 4, '2016-08-22 09:03:48', '2018-10-14 08:51:35'),
(14, 0, 'Thomas Kilman Conflict Mode Instrument (TKI) Workshop ', 'thomas-kilman-conflict-mode-instrument-tki-workshop', 28, 29, 0, 'uploads/images/courses/fa35e192121eabf3dabf9f5ea6abdbcbc107ac3b.jpg', 0, '5000.00', '/calendar/course/14/thomas-kilman-conflict-mode-instrument-tki-workshop', 0, 2, 4, '2016-08-22 09:09:45', '2018-03-06 13:24:35'),
(15, 0, 'NEO', 'neo-conversion', 22, 23, 0, 'uploads/images/courses/f1abd670358e036c31296e66b3b66c382ac00812.png', 0, '8000.00', 'calendar/course/15/neo-conversion', 0, 2, 4, '2016-08-25 06:58:03', '2018-12-12 02:56:25'),
(16, 0, 'Centre Manager', 'centre-manager', 16, 17, 0, 'uploads/images/courses/1574bddb75c78a6fd2251d61e2993b5146201319.png', 0, '5750.00', '/calendar/course/16/centre-manager', 0, 3, 4, '2016-08-27 11:07:14', '2018-10-14 02:27:40'),
(17, 0, 'Assessor Skills and Centre Manager', 'assessor-skills', 9, 9, 0, 'uploads/images/courses/0716d9708d321ffb6a00818614779e779925365c.png', 0, '7765.00', '/calendar/course/17/assessor-skills', 1, 3, 4, '2016-08-28 05:20:00', '2018-12-12 02:55:54'),
(18, NULL, 'Test', 'test', 30, 31, 0, 'uploads/images/courses/9e6a55b6b4563e652a23be9d623ca5055c356940.jpg', 0, '4000.00', '', 0, 1, 2, '2016-10-04 16:25:27', '2018-05-23 03:46:47'),
(19, 0, 'The Leadership Code', 'the-leadership-code', 20, 21, 0, 'uploads/images/courses/b3f0c7f6bb763af1be91d9e74eabfeb199dc1f1f.jpg', 0, '12000.00', '', 1, 4, 4, '2017-10-15 12:09:22', '2018-03-06 13:24:36'),
(20, 0, 'Competency Based Interview Training', 'competency-based-interview-training', 18, 19, 0, 'uploads/images/courses/91032ad7bbcb6cf72875e8e8207dcfba80173f7c.jpg', 0, '4000.00', '', 1, 4, 4, '2017-10-15 12:15:07', '2018-03-06 13:24:36'),
(21, 0, 'Strengthscope', 'Strengthscope', 26, 27, 0, 'uploads/images/courses/472b07b9fcf2c2451e8781e944bf5f77cd8457c8.jpg', 0, '8000.00', '', 1, 4, 4, '2017-10-17 11:48:07', '2018-03-06 13:24:35'),
(22, 0, 'Coaching Skills for People Managers (ICF Recognised)', 'coaching-skills-for-line-managers-icf-recognised', 5, 6, 0, 'uploads/images/courses/12c6fc06c99a462375eeb3f43dfd832b08ca9e17.png', 0, '5000.00', '', 1, 4, 4, '2018-03-06 13:18:26', '2018-10-23 04:01:23');

-- --------------------------------------------------------

--
-- Table structure for table `course_dates`
--

CREATE TABLE `course_dates` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_dates`
--

INSERT INTO `course_dates` (`id`, `course_id`, `start_date`, `end_date`) VALUES
(88, 4, '2019-02-03 20:00:00', '2019-02-06 20:00:00'),
(175, 6, '2019-02-10 20:00:00', '2019-02-13 20:00:00'),
(208, 15, '2018-05-09 20:00:00', '2018-05-09 20:00:00'),
(224, 8, '2019-03-26 20:00:00', '2019-03-27 20:00:00'),
(227, 7, '2016-10-05 20:00:00', '2016-10-05 20:00:00'),
(228, 10, '2019-02-20 20:00:00', '2019-02-20 20:00:00'),
(234, 11, '2019-04-16 20:00:00', '2019-04-17 20:00:00'),
(253, 16, '2018-03-27 20:00:00', '2018-03-28 20:00:00'),
(260, 14, '2016-11-01 05:00:00', '2016-11-01 05:00:00'),
(262, 13, '2019-03-10 20:00:00', '2019-03-13 20:00:00'),
(281, 18, '2017-01-18 20:00:00', '2017-01-20 20:00:00'),
(282, 18, '2016-10-03 20:00:00', '2016-10-03 20:00:00'),
(285, 17, '2019-11-24 20:00:00', '2019-11-26 20:00:00'),
(343, 19, '2016-11-01 05:00:00', '2016-11-01 05:00:00'),
(345, 20, '2016-10-30 05:00:00', '2016-10-30 05:00:00'),
(350, 21, '2016-11-01 05:00:00', '2016-11-01 05:00:00'),
(351, 4, '2019-06-23 20:00:00', '2019-06-26 20:00:00'),
(352, 4, '2019-10-06 20:00:00', '2019-10-09 20:00:00'),
(354, 6, '2019-04-28 20:00:00', '2019-05-01 20:00:00'),
(356, 6, '2019-09-08 20:00:00', '2019-09-11 20:00:00'),
(357, 8, '2019-07-16 20:00:00', '2019-07-17 20:00:00'),
(362, 17, '2019-08-04 20:00:00', '2019-08-06 20:00:00'),
(364, 16, '2018-08-28 20:00:00', '2018-08-29 20:00:00'),
(365, 16, '2018-11-13 20:00:00', '2018-11-14 20:00:00'),
(366, 13, '2019-04-23 20:00:00', '2019-04-23 20:00:00'),
(367, 13, '2019-07-07 20:00:00', '2019-07-07 20:00:00'),
(369, 10, '2019-06-30 20:00:00', '2019-06-30 20:00:00'),
(371, 22, '2019-06-18 20:00:00', '2019-06-19 20:00:00'),
(377, 22, '2019-12-10 20:00:00', '2019-12-11 20:00:00'),
(378, 8, '2019-11-05 20:00:00', '2019-11-06 20:00:00'),
(379, 17, '2019-03-31 20:00:00', '2019-04-02 20:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_date_translations`
--

CREATE TABLE `course_date_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_date_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_date_translations`
--

INSERT INTO `course_date_translations` (`id`, `course_date_id`, `locale`, `location`) VALUES
(88, 88, 'en', 'Dubai'),
(175, 175, 'en', 'Dubai'),
(208, 208, 'en', 'Dubai'),
(224, 224, 'en', 'Dubai'),
(227, 227, 'en', 'Dubai'),
(228, 228, 'en', 'Dubai'),
(234, 234, 'en', 'Dubai'),
(253, 253, 'en', 'Dubai'),
(260, 260, 'en', 'Dubai'),
(262, 262, 'en', 'Days 1-3'),
(281, 281, 'en', 'Dubai'),
(282, 282, 'en', 'London'),
(285, 285, 'en', 'Dubai'),
(308, 88, 'ar', 'دبي'),
(315, 175, 'ar', 'دبي'),
(321, 224, 'ar', 'دبي'),
(324, 228, 'ar', 'دبي'),
(327, 234, 'ar', 'دبي'),
(330, 285, 'ar', 'دبي'),
(334, 253, 'ar', 'دبي'),
(337, 262, 'ar', 'دبي'),
(340, 208, 'ar', 'دبي'),
(342, 260, 'ar', 'دبي'),
(383, 343, 'en', 'Dubai'),
(385, 345, 'en', 'Dubai'),
(390, 350, 'en', 'Dubai'),
(391, 351, 'en', 'Dubai'),
(392, 352, 'en', 'Dubai'),
(394, 354, 'en', 'Dubai'),
(396, 356, 'en', 'Dubai'),
(397, 357, 'en', 'Dubai'),
(402, 362, 'en', 'Dubai'),
(404, 364, 'en', 'Dubai'),
(405, 365, 'en', 'Dubai'),
(406, 366, 'en', 'Dubai Day 4'),
(407, 367, 'en', 'Dubai Day 5'),
(409, 369, 'en', 'Dubai'),
(411, 371, 'en', 'Dubai'),
(413, 371, 'ar', 'Dubai'),
(419, 377, 'en', 'Dubai'),
(420, 378, 'en', 'Dubai'),
(421, 379, 'en', 'Dubai');

-- --------------------------------------------------------

--
-- Table structure for table `course_page`
--

CREATE TABLE `course_page` (
  `course_id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_page`
--

INSERT INTO `course_page` (`course_id`, `page_id`) VALUES
(10, 20),
(11, 20),
(6, 28),
(6, 29),
(6, 30),
(4, 41),
(6, 41),
(8, 41),
(15, 41),
(22, 44),
(16, 45),
(17, 45),
(8, 60),
(15, 60),
(6, 70),
(6, 81),
(6, 121),
(6, 122),
(6, 128),
(8, 129),
(6, 137);

-- --------------------------------------------------------

--
-- Table structure for table `course_translations`
--

CREATE TABLE `course_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_translations`
--

INSERT INTO `course_translations` (`id`, `course_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(5, 4, 'en', 'MBTI Certification Programme', '<p style=\"text-align: justify;\">PSI Middle East is the only offically licsenced MBTI distributor for&nbsp;the Middle East. We are proud partners of The Myers-Briggs Company, formerly CPP, and we look forward to introducing you to the worlds most widely used Type tool!</p>\r\n<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/5b89bb26e048d735d5d5de8f09068ec31253c199.pdf&amp;name=MBTI Accreditation.pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">The Myers-Briggs Type Indicator&reg; (MBTI&reg;) instrument continues to be the most trusted and widely used assessment in the world for understanding individual differences and uncovering new ways to work and interact with others.</p>\r\n<p style=\"text-align: justify;\">It is used exclusively in a development setting to improve individual and team performance, develop and retain top talent and leaders lending depth to the coaching experience, improve communication and reduce conflict in teams as well as explore the world of work and careers.</p>\r\n<p style=\"text-align: justify;\">Those who successfully complete the course will be accredited to administer, interpret and feedback the MBTI instrument (Step I &amp; II). You will also be invited to join the Middle East MBTI Practitioners forum which is a free bi-annual learning and development event for MBTI practitioners to share experiences and continue to develop their skills further with the MBTI.</p>\r\n<p style=\"text-align: justify;\">The course comprises of some pre-course reading and taking the MBTI questionnaire, home work and daily multiple choice assessments.</p>', '', '', ''),
(7, 6, 'en', 'BPS Qualification in Occupational Testing', '<p style=\"text-align: justify;\"><a title=\"BPS Course Flyer\" href=\"/downloadFile?path=uploads/documents/77a4dcbcf3e00bd724d4099b2a50959eab9322c8.pdf&amp;name=BPS Level A &amp; P.pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">Formerly known as the BPS Level A&amp;B (P), this 4-day course qualifies delegates to use a wide variety of ability tests and personality questionnaires from a number of major test publishers including Saville Assessment, a Willis Towers Watson Company, psychometric&nbsp;products.</p>\r\n<p style=\"text-align: justify;\">The qualification is internationally recognised demanding the best practice in the use of psychometrics at work, and is suitable for those involved in selection, development or any other application where psychometric assessments are useful. Upon successful completion of the course delegates will be able to become registered as&nbsp;an international Occupational Test&nbsp;User (Abilty and&nbsp;Personality), with the ability&nbsp;to&nbsp;administer, analyse and feedback a variety of psychometric tools including the Saville Assessment&nbsp;range of products. Upon successful application to the BPS, conversion courses to a number of further products will also be made available.</p>\r\n<p style=\"text-align: justify;\">Pre-course work, reading and assessments are a requirement, two open book exams during the 4-day course and post course work, should you wish to apply to the BPS upon completion. Included in our pricing is one-to-one support with your post course&nbsp;work and free applications of the Saville Assessment Wave.</p>\r\n<p style=\"text-align: justify;\"><strong>Why British Psychological Society (BPS)?</strong></p>\r\n<p>The British Psychological Society promotes excellence and ethical practice in the science, education and practical applications of psychology. They do this by setting standards in&nbsp;<a href=\"http://www.bps.org.uk/node/10/\">psychological testing</a> and raising levels of&nbsp;<a href=\"http://www.bps.org.uk/node/11/\">education, training and practice</a> through their recognised international course providers. More information about Saville Assessment, A Willis Towers Watson Company and the BPS is available through IHS.<br /><a href=\"/html/course-details.php\">http://www.bps.org.uk/what-we-do/bps/bps</a></p>\r\n<p>&nbsp;</p>\r\n<p>We run this as a public course but this can be run as an in-house course for your teams! So feel free to get in touch to ask us more about this option on 04 390 2778.</p>', '', '', ''),
(8, 7, 'en', 'BPS Qualification: Assistant Test User', '<p><a title=\"BPS Assistant Test User\" href=\"/uploads/documents/08be263178d53863022b2a49c26bc5aed75f7190.pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">The British Psychological Society (BPS) Qualification in Occupational testing - Assistant test User is a 1-day course which allows those qualified to administer Aptitude and Ability Tests from a number of major test publishers, principally Saville Consulting.</p>\r\n<p style=\"text-align: justify;\">It is&nbsp;an essential qualification for&nbsp;administrators wishing to administer tests and handle test results. This course unlike the BPS Qualification&nbsp;in&nbsp;Occupational Testing (Ability &amp; Personality) does not qualify delegates to interpret and feedback results.</p>', '', '', ''),
(9, 8, 'en', 'EQi 2.0 & EQ360 ', '<p style=\"text-align: justify;\"><a title=\"EQi Flyer\" href=\"/downloadFile?path=uploads/documents/3c07ab7f5e3e802781d8afe3ec90cfac939b1147.pdf&amp;name=EQi 2.0 &amp; EQ360.pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">The EQi is particularly useful for development activity and in certain circumstances for selection. Emotional Intelligence has been identified as one the leading predictors of leadership performance and success, and is helpful in identifying how we can understand and manage ourselves and our relationships with others.</p>\r\n<p style=\"text-align: justify;\">The EQi 2-day Accreditation Course enables those who complete, pass the post online assessment and conduct an observed feedback session, to become certified to use the EQi-2.0 model of Emotional Intelligence tools; both individual reports and 360 surveys.</p>', '', '', ''),
(11, 10, 'en', 'Strong Interest Inventory', '<p><a title=\"Strong Accreditation\" href=\"/downloadFile?path=uploads/documents/573a85b5172083bb9c6ca28eee4d005c6df7e8b8.pdf&amp;name=Strong Interest Inventory.pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">The Strong Interest Inventory instrument is the most widely used and respected career development instrument in the world. It has guided thousands of individuals, from high school and college students to mid-career workers seeking a change in their search for a rich and fulfilling career. It is a powerful tool for anyone considering a career change, starting a new career, looking for career enrichment, or a better work life balance. The Strong Interest Inventory generates an in depth assessment of an individual&rsquo;s interests in a broad range of occupations, work and leisure activities, and educational subjects. The tool is the gold standard for career exploration and development, providing time-tested and research-validated insights that foster successful career counselling relationships. The Strong can be used in conjunction with other reports such as the Myers Briggs Type Indicator (MBTI) for a detailed look at personality and career.</p>\r\n<p>The Strong Interest Inventory&reg; is ideal for:</p>\r\n<ul>\r\n<li>Choosing a college major.</li>\r\n<li>Career exploration.</li>\r\n<li>Career development.</li>\r\n<li>Re-integration.</li>\r\n</ul>\r\n<p>There is some pre-course work (online assessment) required.&nbsp;</p>', '', '', ''),
(12, 11, 'en', 'FIRO Business', '<p style=\"text-align: justify;\"><a title=\"Firo Business Accreditation\" href=\"/downloadFile?path=uploads/documents/bd1608338fad89fe6199c172e0b2b39182458a7e.pdf&amp;name=FIRO-Business.pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">The FIRO-Business (Fundamental Interpersonal Relations Orientation - Business) instrument helps individuals understand their need for inclusion, control and affection and how it can shape their behaviour and interactions with others at work or in their personal life. As an integral part of your leadership and coaching, team building and conflict management initiatives, the&nbsp;FIRO-B assessment can be used in a variety of settings and in combination with other solutions to improve organisational performance. It provides the skills to administer and interpret the assessment competently and ethically.</p>\r\n<p style=\"text-align: justify;\">There is some pre-course work (online assessment) and homework.</p>', '', '', ''),
(14, 13, 'en', 'Accredited Award in Coach Training (AACT)', '<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/c0dd2fc0e475f4a9533c3069904058ee4e529dea.pdf&amp;name=Accredited Award in Coach Training (AACT).pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">A fast paced, highly interactive programme&nbsp;recognised by the Association of Coaching UK, introduces the practical techniques to coach individuals for professional development. Real life case studies, group discussions, personal feedback and continuous practice are all used to build the framework for professional coaching skills. The workshop particularly focuses on the GROW model.</p>\r\n<p style=\"text-align: justify;\">The workshop is designed to create an assured coach, confident to tackle work and behavioural issues and help their coachees achieve set goals. This includes:</p>\r\n<ul>\r\n<li style=\"text-align: justify;\">Understand the purpose, ethics and power of Coaching.</li>\r\n<li>Gaining commitment in times of change.</li>\r\n<li>Tapping into people&rsquo;s potential and their desire to succeed and improve.</li>\r\n<li>Solving technical and organisational problems.</li>\r\n<li>Dealing with difficult people and dramatically enhance their own and other&rsquo;s performance.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Innovative HR Solutions has aligned this programme with the <strong data-redactor-tag=\"strong\">Association for Coaching (UK</strong>) and the standards they set to help participants gain Associate and Member Level Membership with the Association following the workshop.</p>\r\n<p style=\"text-align: justify;\">There is some homework and post-course work &ndash; practical and theoretical work required.</p>\r\n<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/images/719f2eebb06aaff55076b6649baca074c18fbcaa.jpg&amp;name=10_THINGS.jpg\" target=\"_blank\">10 Reasons to get certified with Association for Coaching</a></p>', '', '', ''),
(15, 14, 'en', 'Thomas Kilman Conflict Mode Instrument (TKI) Workshop ', '<p style=\"text-align: justify;\">The Thomas Kilman Conflict Mode Instrument (TKI) is the world&rsquo;s best-selling tool for helping people understand how different conflict-handling styles affect interpersonal and group dynamics&mdash;and for empowering them to choose the appropriate style for any situation.</p>\r\n<p style=\"text-align: justify;\">Delegates will learn how to apply the TKI tool to assess an individual&rsquo;s typical behaviour in conflict situations along two dimensions: assertiveness and cooperativeness and how individuals can effectively use five different conflict-handling modes, or styles.</p>\r\n<p style=\"text-align: justify;\">There is some pre-course work (online assessment) required.</p>', '', '', ''),
(16, 15, 'en', 'NEO Conversion', '<p style=\"text-align: justify;\">The NEO PI-3 assesses the Big Five personality traits: Extraversion, Agreeableness, Conscientiousness, Neuroticism, and Openness to Experience. This thorough and collaborative one-day conversion course is designed for users who hold a BPS TUOP/Level B certificate, have experience using psychometrics, and would like to gain international access to the NEO suite of tools. Our highly skilled team will guide you through the fascinating world of NEO and allow you to become recognised at successfully interpreting and feeding back the psychometric, long referred to as the &lsquo;gold standard of personality assessment&rsquo;.</p>', '', '', ''),
(17, 16, 'en', 'Centre Manager', '<p><a title=\"Centre Manager Flyer\" href=\"/downloadFile?path=uploads/documents/c24f50381bbcd402e567ec7ce4728b82499c6a2f.pdf&amp;name=Centre Manager.pdf\">Course Overview</a></p>\r\n<p>This British Psychological Society (BPS) accredited 2-day Workshop enables delegates to manage the process of designing and facilitating Assessment and Development Centres. The 2-day Assessor Skills Course is a pre-requisite to attend this course.</p>\r\n<p>The course enables delegates to develop the knowledge and skills needed to become a qualified Centre Manager in a range of typical Assessment Centre events. You will be shown how to select exercises and design a matrix for an assessment event to gain a robust measure of the behaviours at the level in question. It will also instruct in the art of running an efficient and robust post centre &lsquo;wash-up&rsquo; session and guide consistency and quality throughout an event. Upon successful completion, delegates will be able to purchase business simulation exercises from two key test publishers; Global Leader and A&amp;DC.</p>\r\n<p>There is some homework and a multiple choice exam to finalise your certificati<span class=\"italic\">on.</span></p>', '', '', ''),
(18, 17, 'en', 'Assessor Skills and Centre Manager', '<p><a href=\"/downloadFile?path=uploads/documents/74c370d58b803726f249923259df0c9995784be8.pdf&amp;name=Assessor Skills.pdf\">Course Overview</a></p>\r\n<p style=\"text-align: justify;\">This British Psychological Society (BPS) recognised 3-day Global Leader Assessor Skills and Centre Manager Course enables delegates to develop the knowledge and skills needed to become a qualified Assessor and Centre Manager in a range of typical Assessment Centre/ Development Centre events.It is designed for HR Professionals, management and non-management staff who are required to be part of an assessment function.</p>\r\n<p style=\"text-align: justify;\">You will be shown how to select exercises and design a matrix for an assessment event to gain a robust measure of the behaviours at the level in question. We will also delve into the art of recording, classifying and rating behavioural evidence across a range of observable exercises including group exercises, role plays and analysis exercises / case studies and presentations. It will also instruct in the art of running an efficient and robust post centre &lsquo;wash-up&rsquo; session and guide consistency and quality throughout an event.&nbsp;</p>\r\n<p style=\"text-align: justify;\">There is no pre-coursework required. However, you can expect&nbsp;homework, an observed feedback session and a multiple choice exam at the end of the programme.</p>\r\n<p style=\"text-align: justify;\">We run this as a public course but this can be run as an in-house course for your teams! So feel free to get in touch to ask us more about this option on 04 390 2778.</p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/ade68d18f691d7f3ac06954028bba93b510eccec.pdf&amp;name=Assessor Skills.pdf\">&nbsp;Programme Flyer</a></p>', '', '', ''),
(19, 18, 'en', 'Strong Interest Inventories', '<p>Tes</p>', '', '', ''),
(20, 4, 'ar', 'برنامج اعتماد مؤشر أنماط مايرز بريجيز MBTI', '<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/5b89bb26e048d735d5d5de8f09068ec31253c199.pdf&amp;name=MBTI Accreditation.pdf\">منشور الدورة </a></p>\r\n<p style=\"text-align: justify;\">إن مؤشر أنماط مايرز بريجيز (MBTI) يمثل أكثر الأدوات المستخدمة مصداقية في العالم لفهم الاختلافات الفردية وكشف طرق جديدة للتواصل مع الآخرين.</p>\r\n<p>يُستخدم مؤشر MBTI حصراً في عمليات التطوير لتحسين أداء الفرق والأفراد، تطوير واستبقاء المواهب المتميزة والقادة، تطوير عملية التواصل، تقليل النزاع بين فرق العمل بالإضافة إلى استكشاف عالم الأعمال والوظائف.</p>\r\n<p>يتمكن الأفراد الذين يكملون الدورة بنجاح من الحصول على الترخيص لإدارة وشرح واعطاء افادة حول الأداء فيما يختص بأداة MBTI (الخطوة 1 و2). كما يمكنهم أيضاً الالتحاق بمنتدى MBTI في الشرق الأوسط، وهو إفطار نصف سنوي مجاني لممارسي أداة MBTI لتبادل الخبرات والتطوير المستمر وصقل المهارات.</p>\r\n<p>يتعين على المشارك القيام بالدراسة الذاتية لمدة ١٠ ساعات قبل الدورة (القراءة وإجراء اختبار عبر الانترنت) بجانب أداء بعض الأعمال المنزلية وعدد من اختبارات الخيارات المتعددة.</p>', '', '', ''),
(21, 6, 'ar', 'اعتماد جمعية علم النفس البريطانية في الاختبار المهني ', '<p style=\"text-align: justify;\"><a title=\"BPS Course Flyer\" href=\"/downloadFile?path=uploads/documents/77a4dcbcf3e00bd724d4099b2a50959eab9322c8.pdf&amp;name=BPS Level A &amp; P.pdf\">منشور الدورة </a></p>\r\n<p style=\"text-align: justify;\">تقوم هذه الدورة (سابقاً الجمعية البريطانية لعلم النفس المستوى \"أ\" و\"ب\") بتأهيل المشاركين لاستخدام مجموعة واسعة من اختبارات القدرة واستبيانات الشخصية من قبل عدد من كبار ناشري الاختبارات بما في ذلك منتجات شركة ساڤيل للاستشارات، إحدى شركات ويلس تاورز واتسون لمنتجات أدوات القياس السيكولوجية.</p>\r\n<p style=\"text-align: justify;\">تتضمن هذه الشهادة المعترف بها عالمياً استخدام أفضل الممارسات عند تطبيق اختبارات علم النفس في نطاق العمل، وهي مناسبة للمستخدمين العاملين في مجال التوظيف والتطوير أو التطبيقات الأخرى التي تستخدم تقييمات علم النفس. يتم تسجيل المشاركين عقب الانتهاء من الدورة كمستخدمين معتمدين للاختبارات المهنية (للقدرة والشخصية)، بجانب القدرة على الإدارة والتحليل وتقديم الملاحظات بشأن مجموعة من أدوات القياس النفسي بما في ذلك منتجات شركة ساڤيل للاستشارات. عند التطبيق الناجح للشهادة، من الممكن المشاركة في العديد من دورات الخاصة بالأدوات الأخرى.</p>\r\n<p style=\"text-align: justify;\">تتطلب هذه الدورة القيام ببعض التحضير قبل الدورة مثل القراءة واكمال التقييمات وإجراء اختبارين (كتاب مفتوح) خلال الأيام الأربعة للدورة. كما سيكون هناك بعض الأعمال التي يتعين القيام بها بعد الدورة (مثل: القيام بعدد ١٣ مهمة، بما في ذلك تقرير مكتوب وجلسات ملاحظات حول الأداء) وهي من المتطلبات التي يتعين على الراغبين بالحصول على الترخيص إكمالها. تتضمن الكلفة الحصول على الدعم الشخصي لأداء العمل المطلوب بعد الدورة بالإضافة إلى تطبيق مجاني من ساڤيل وايف (Saville Wave).</p>\r\n<p style=\"text-align: justify;\"><strong>لماذا تحصل على اعتماد جمعية علم النفس البريطانية؟ </strong> <br /><a href=\"/html/course-details.php\">http://www.bps.org.uk/what-we-do/bps/bps</a></p>', '', '', ''),
(22, 8, 'ar', 'إي كيو آي 2.0 وإي كيو ٣٦٠ (EQi 2.0-EQ360)', '<p style=\"text-align: justify;\"><a title=\"EQi Flyer\" href=\"/downloadFile?path=uploads/documents/3c07ab7f5e3e802781d8afe3ec90cfac939b1147.pdf&amp;name=EQi 2.0 &amp; EQ360.pdf\">منشور الدورة </a></p>\r\n<p style=\"text-align: justify;\">تُستخدم أداة EQi من أجل التطوير وفي بعض الظروف المحددة يتم استخدامها في عملية الاختيار (التوظيف). إن الذكاء العاطفي هو أحد أقوى المُحددات للأداء القيادي، كما يفيد في تحديد كيفية الاستفادة العظمى من قدراتنا وقدرات الآخرين. تقدم أداة EQi نظرة مفصلة حول كيفية فهم وإدارة الذات، بالإضافة إلى إدارة العلاقات مع الآخرين والتعامل مع ضغوط ومتطلبات الحياة.</p>\r\n<p style=\"text-align: justify;\">تُمكِّن دورة (EQi 2.0) المعتمدة التي تمتد على مدار يومين الأفراد الذين يقوموا بإكمال الاختبار عبر الانترنت وإجراء جلسة ملاحظات الأداء، من أن يصبحوا مرخصين لاستخدام أدوات نموذج إي كيو آي 2.0 للذكاء العاطفي؛ أي كلٍ من التقرير الفردي واستبيان ٣٦٠.</p>', '', '', ''),
(23, 10, 'ar', 'أداة Strong Interest Inventory ', '<p><a title=\"Strong Accreditation\" href=\"/downloadFile?path=uploads/documents/573a85b5172083bb9c6ca28eee4d005c6df7e8b8.pdf&amp;name=Strong Interest Inventory.pdf\">منشور الدورة </a></p>\r\n<p style=\"text-align: justify;\">تُعدّ أداة سترونغ من أكثر الأدوات استخداماً وتقديراً في مجال التطوير الوظيفي بالعالم. قامت الأداة بتوجيه آلاف الأفراد من طلاب المدارس الثانوية والجامعات والموظفين في منتصف المسار الوظيفي الذين يسعون إلى تغيير مساراتهم الوظيفية. تعتبر أداة سترونغ من الأدوات القوية لأي شخص يفكر بالتغيير الوظيفي أو بدء وظيفة جديدة أو يسعى إلى الإثراء الوظيفي أو الموازنة بين الحياة الخاصة والعمل.</p>\r\n<p style=\"text-align: justify;\">تقوم أداة سترونغ بإجراء تقييم متعمق لرغبات الفرد فيما يتعلق بمجموعة واسعة من أنشطة المهن والعمل والاستجمام والمواضيع التعليمية. تمثل هذه الأداة الأساس في عملية الاستكشاف الوظيفي والتطوير، من خلال تقديم نصائح زمنية مدروسة تساعد في عملية الإرشاد الوظيفي الناجحة.</p>\r\n<p style=\"text-align: justify;\">يمكن استخدام أداة سترونغ مع تقارير أخرى مثل مؤشر MBTI من أجل الحصول على نظرة مفصلة عن الشخصية والمسار الوظيفي.</p>\r\n<p>تُعدّ أداة سترونغ مثالية في العمليات التالية:</p>\r\n<ul>\r\n<li>&nbsp;اختيار تخصص ما في&nbsp;الكلية أو الجامعة</li>\r\n<li>الاستكشاف المهني</li>\r\n<li>التطوير الوظيفي</li>\r\n<li>إعادة الانضمام</li>\r\n</ul>\r\n<p>هناك بعض الأعمال المطلوب اكمالها قبل الدورة (مثال: التقييم عبر الانترنت)</p>', '', '', ''),
(24, 11, 'ar', 'فايرو للأعمال - Firo Business', '<p style=\"text-align: justify;\"><a title=\"Firo Business Accreditation\" href=\"/downloadFile?path=uploads/documents/bd1608338fad89fe6199c172e0b2b39182458a7e.pdf&amp;name=FIRO-Business.pdf\">منشور الدورة </a></p>\r\n<p style=\"text-align: justify;\">تساعد أداة فايرو للأعمال (FIRO-Business) الأفراد على فهم حاجاتهم فيما يختص بالاحتواء والسيطرة والألفة وأهميتها في تحديد سلوكهم وتواصلهم مع الآخرين في العمل والحياة الخاصة. وكجزء هام من عملية القيادة والتدريب الشخصي ومبادرات بناء الفرق وإدارة الخلاف، يمكن استخدام تقييم فايرو للأعمال في العديد من البيئات ودمجه مع الكثير من الحلول لتحسين الأداء المؤسسي. توفر أداة فايرو المهارات اللازمة لإدارة وتفسير التقييم بجدارة وأخلاقية عالية.</p>\r\n<p style=\"text-align: justify;\">هناك بعض المتطلبات التي يتعين القيام بها قبل الدورة (اختبار عبر الانترنت) وبعض الأعمال المنزلية المسبقة.</p>', '', '', ''),
(25, 17, 'ar', 'دورة مهارات المُقيِّمين ', '<p><a href=\"/downloadFile?path=uploads/documents/74c370d58b803726f249923259df0c9995784be8.pdf&amp;name=Assessor Skills.pdf\">منشور الدورة </a></p>\r\n<p style=\"text-align: justify;\">تقوم دورة مهارات المُقيِّمين التي تمتد على مدار يومين والمعترف بها من قبل جمعية علم النفس البريطانية، بمساعدة المشاركين على تطوير المعارف والمهارات المطلوبة ليصحبوا مقيميِّن خبراء في مجال مراكز التقييم والتطوير بشكل خاص. تم تصميم الدورة لخبراء إدارة الموارد البشرية والمدراء والموظفين غير الإداريين الذين يُطلب منهم القيام بإجراء التقييمات.</p>\r\n<p style=\"text-align: justify;\">تقوم دورة مهارات المُقيِّمين بتعليم المشاركين فن تسجيل وتحديد وتقييم الأدلة السلوكية عبر مجموعة من الأنشطة المختلفة مثل التدريبات الجماعية وأنشطة تمثيل الأدوار والتحليل أو دراسة الحالة والعرض التقديمي.</p>\r\n<p style=\"text-align: justify;\">هناك بعض المتطلبات التي يتعين القيام بها قبل الدورة (تقرير مكتوب) وبعض الأعمال المنزلية وجلسة ملاحظات حول الأداء بجانب اختبار ذو الاسئلة المتعددة الأجوبة في نهاية البرنامج.</p>\r\n<p>&nbsp;</p>', '', '', ''),
(26, 16, 'ar', 'دورة مدير المركز', '<p><a title=\"Centre Manager Flyer\" href=\"/downloadFile?path=uploads/documents/c24f50381bbcd402e567ec7ce4728b82499c6a2f.pdf&amp;name=Centre Manager.pdf\">منشور الدورة </a></p>\r\n<p>تقوم هذه الدورة التي تمتد على مدار يومين والمعترف بها من قبل جمعية علم النفس البريطانية، بمساعدة المشاركين على إدارة عملية تصميم وإدارة مراكز التقييم والتطوير. ومن متطلبات هذه الدورة حضور دورة مهارات المُقيِّمين لمدة يومين.</p>\r\n<p>تمكِّن هذه الدورة المشارك من تطوير المعارف والمهارات المتطلبة لكي يصبح مدير مركز محترف من خلال مجموعة من أنشطة مراكز التقييم. خلال الدورة، يتعلم المشارك كيفية اختيار الأنشطة المناسبة وتصميم مصفوفة التقييم من أجل الحصول على مقياس قوي للسلوكيات في المستوى المعني. كما سيتعلم أيضاً فن إدارة جلسات الإفادة والمتابعة الفعالة التي تُعقد عقب انتهاء المركز، من أجل ضمان الجودة والتناسق خلال المركز. عند اكمال متطلبات الدورة بنجاح، سيتمكن المشارك من شراء منتجات التقييم من اثنين من ناشري الاختبارات الأساسيين، جلوبال ليدر واي اند دي سي.</p>\r\n<p>تتطلب الدورة إنهاء بعض الأعمال المنزلية وإتمام اختبار الخيارات المتعددة لإنهاء متطلبات الترخيص.</p>', '', '', ''),
(27, 13, 'ar', 'دورة التدريب التنفيذي من جمعية AFC ', '<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/c0dd2fc0e475f4a9533c3069904058ee4e529dea.pdf&amp;name=Accredited Award in Coach Training (AACT).pdf\">منشور الدورة </a></p>\r\n<p style=\"text-align: justify;\">هي ورشة عمل سريعة وتفاعلية للغاية معترف بها من قبل جمعية التوجيه في المملكة المتحدة. تُقدِّم ورشة العمل التقنيات العملية لتوجيه الأشخاص بغرض التطوير المهني. تتضمن ورشة العمل عدة أنشطة مختلفة مثل دراسات الحالة الحقيقية، النقاشات الجماعية، مناقشة الأداء الشخصي والممارسة المستمرة، وذلك من أجل بناء إطار عمل لمهارات التدريب المهني. تركز الورشة بشكل خاص على استخدام نموذج GROW.</p>\r\n<p style=\"text-align: justify;\">تم تصميم الورشة من أجل المساهمة في تكوين مدرب متمرس، يتعامل مع معوّقات العمل السلوكية بثقة ويساعد متدربيه على الوصول إلى أهدافهم. ويشمل ذلك التالي:</p>\r\n<ul>\r\n<li>فهم غرض وأخلاقيات وقوة التوجيه.</li>\r\n<li>اكتساب الالتزام خلال أوقات التغيير.</li>\r\n<li>الاستفادة من إمكانيات الأفراد ورغباتهم بالنجاح والتطور.</li>\r\n<li>حل المشكلات التقنية والمؤسسية.</li>\r\n<li>التعامل مع الأفراد صعبي المراس وتغيير أدائهم وأداء الآخرين تغييراً جذرياً.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">قامت شركة انوڤاتيڤ إتش آر سوليوشنز بوضع هذا البرنامج&nbsp;ليتماشى مع مقاييس جمعية التدريب (بالمملكة المتحدة) الموضوعة لمساعدة المشاركين في الحصول على عضوية بدرجة منتسب وعضو بالجمعية بعد إكمال الدورة.</p>\r\n<p style=\"text-align: justify;\">يتعين القيام ببعض الأعمال المنزلية والمتطلبات العملية والنظرية بعد الدورة.</p>', '', '', ''),
(28, 15, 'ar', 'دورة نيو(NEO Conversion)  ', '<p style=\"text-align: justify;\">تقوم أداة (NEO PI-R) بتقييم سمات الشخصية الرئيسية الخمس: الانبساط، الموافقة، الوعي، العصابية والانفتاح. تم تصميم هذه الدورة المتكاملة والشاملة والتي تستغرق يوماً واحداً للمستخدمين الذين يحملون شهادة جمعية علماء النفس البريطانية: المستوى \"أ\" و\"ب\"، والذين يتمتعون بالخبرة في مجال استخدام أدوات القياس النفسي، ويرغبون في استخدام&nbsp;مجموعة منتجات نيو العالمية.</p>\r\n<p style=\"text-align: justify;\">سيقوم فريقنا المتخصص بتوجيهكم وإرشادكم عبر عالم نيو الساحر ويساعدكم على أن تصبحوا مرخصين لتفسير وإعطاء الملاحظات باستخدام أداة القياس بنجاح، والتي يشار إليها باسم \"المقياس الذهبي لأدوات القياس النفسي\".</p>', '', '', ''),
(29, 14, 'ar', 'دورة مؤشر توماس كيلمان لأنماط النزاع (TKI)', '<p style=\"text-align: justify;\">تُعدّ أداة توماس كيلمان (TKI) من أفضل الأدوات مبيعاً، حيث تساعد الأفراد على فهم الأساليب المختلفة لمواجهة النزاع، وتأثير كلٍ من هذه الأساليب على الديناميكيات الشخصية والجماعية. كما تساعد الأداة على اختيار أفضل الأساليب للتعامل مع كل موقف.</p>\r\n<p style=\"text-align: justify;\">سيتعلم المشاركون كيفية استخدام الأداة لتقييم سلوكيات الفرد التلقائية في التعامل مع حالات النزاع في نطاقين: الإصرار والتعاون وكيف يمكن للأفراد استخدام خمسة أساليب فعالة عند التعامل مع النزاع.</p>\r\n<p style=\"text-align: justify;\">هناك بعض المتطلبات التي يتعين القيام بها قبل الدورة (التقييم عبر الانترنت)</p>', '', '', ''),
(30, 19, 'en', 'The Leadership Code', '<p>Leaders need to be able to develop a clear vision of the way forward, communicate that vision effectively, adapt their approach to the needs and motivations of others and drive results efficiently.&nbsp; Mastering the Leadership Code is built around Dave Ulrich&rsquo;s, Norm Smallwood&rsquo;s and Kate Sweetman&rsquo;s; &lsquo;The Leadership Code: Five Rules to Lead By&rsquo;. Drawing on decades of research experience, the authors conducted extensive interviews with a variety of respected CEOs, academics, experienced executives, seasoned consultants - and determined the same five essentials repeated again and again. &lsquo;Mastering the Leadership Code&rsquo; is a 5-part modular programme interspersed with a pre and post-assessment, Executive Coaching and psychometrics to deepen self-awareness. Flexed across two levels; First-Line to Mid-Managers and Senior Leaders, this programme allows us to bring out the best in&nbsp;the future of your organisation and people.&nbsp;</p>', '', '', ''),
(31, 20, 'en', 'Competency Based Interview Training', '<p>This one day intensive workshop is designed for HR and Recruitment Specialists requiring formal training for interviewing. The course allows delegates to deepen their expertise in the purpose and best approach to competency based interviews (CBI), know how to develop and ask competency related questions, and how to appropriately use open and probing questions to gather STAR-E evidence. Topics covered on the workshop include but are not limited to, Frameworks, objective evaluations, evidence collation hiring recommendations and the importance of professional, positive impressions and expectations.</p>', '', '', ''),
(32, 21, 'en', 'Strengthscope', '<p>Strengthscope&reg; is the world&rsquo;s most complete and innovative strengths profiling system that helps energize peak performance at work. Underpinned by 10 years of testing and research, Strengthscope&reg; will help you to energize peak performance by releasing the power of employees&rsquo; strengths.&nbsp;</p>\r\n<p>Strengthscope&reg; is designed to be the first step in helping you optimize your strengths to improve your performance and engagement at work. When used as part of a strengths-based development program, the profiler and supporting tools and resources can significantly improve your performance, motivation and confidence, during good times and in the face of pressure.&nbsp;</p>\r\n<p>The Strengthscope&reg; profile report provides information on:&nbsp;</p>\r\n<ul>\r\n<li>Unique strengths and how to optimize these to achieve exceptional results</li>\r\n<li>Risk areas to peak performance together with powerful ways to reduce the impact of these</li>\r\n<li>Positive ways of working that will improve confidence, motivation and success in any situation</li>\r\n<li>How to strengthen relationships and work more effectively with people whose strengths are different from yours&nbsp;</li>\r\n</ul>', '', '', ''),
(33, 22, 'en', 'Coaching Skills for Line Managers', '<p>The workshop provides an overview of coaching methodology and best practice, as well as an opportunity for delegates to practice and develop their skills as a coach, confident to tackle work and behavioural issues and help their coachees achieve set goals.</p>\r\n<p>For Managers who want to:<br />&bull; Gain commitment in times of change<br />&bull; Motivate and support their team(s)<br />&bull; Understand people&rsquo;s potential<br />&bull; Dealing with difficult people, constructively and with empathy<br />&bull; Dramatically enhance their own and other&rsquo;s performance</p>\r\n<p>Coaching can occur in the workplace informal one-to-one sessions such as Performance Reviews or informal &lsquo;on-the-spot&rsquo; sessions, which requires considerable human insight into how to get the best from your people. The workshop will equip your leaders with the know-how and passion to ensure your culture thrives.</p>\r\n<p>We run this as a public course but this can be run as an in-house course for your teams! So feel free to get in touch to ask us more about this option on 04 390 2778.</p>\r\n<p>CCE credits are available for those&nbsp;delegates already an ICF recognised professional coach.&nbsp;</p>\r\n<p>Core Competence Hours Credits: 9</p>\r\n<p>Resource Development Credits: 3</p>\r\n<p>Resource Development includes training formerly called Personal Development, Business Development, or Other Skills and Tools.</p>', '', '', ''),
(34, 22, 'ar', 'مهارات التوجية للمدراء المباشرين - معتمد من الاتحاد العالمي للكوتشنج', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `model_id`, `model_type`, `name`, `order`) VALUES
(25, 2, 'App\\Page', '', '1'),
(47, 18, 'App\\Page', '', '1'),
(48, 19, 'App\\Page', '', '1'),
(50, 22, 'App\\Page', '', '1'),
(54, 23, 'App\\Page', '', '1'),
(56, 24, 'App\\Page', '', '1'),
(57, 25, 'App\\Page', '', '1'),
(58, 26, 'App\\Page', '', '1'),
(93, 36, 'App\\Page', '', '1'),
(267, 38, 'App\\Page', '', '1'),
(292, 107, 'App\\Page', '', '1'),
(293, 108, 'App\\Page', '', '1'),
(318, 51, 'App\\Page', '', '1'),
(319, 52, 'App\\Page', '', '1'),
(320, 53, 'App\\Page', '', '1'),
(321, 54, 'App\\Page', '', '1'),
(322, 55, 'App\\Page', '', '1'),
(323, 56, 'App\\Page', '', '1'),
(324, 57, 'App\\Page', '', '1'),
(325, 58, 'App\\Page', '', '1'),
(326, 82, 'App\\Page', '', '1'),
(329, 89, 'App\\Page', '', '1'),
(341, 62, 'App\\Page', '', '1'),
(342, 63, 'App\\Page', '', '1'),
(343, 64, 'App\\Page', '', '1'),
(344, 65, 'App\\Page', '', '1'),
(345, 66, 'App\\Page', '', '1'),
(382, 102, 'App\\Page', '', '1'),
(435, 50, 'App\\Page', '', '1'),
(437, 84, 'App\\Page', '', '1'),
(500, 132, 'App\\Page', '', '1'),
(565, 134, 'App\\Page', '', '1'),
(566, 135, 'App\\Page', '', '1'),
(627, 130, 'App\\Page', '', '1'),
(658, 91, 'App\\Page', '', '1'),
(659, 92, 'App\\Page', '', '1'),
(660, 93, 'App\\Page', '', '1'),
(661, 88, 'App\\Page', '', '1'),
(686, 5, 'App\\Page', '', '1'),
(687, 6, 'App\\Page', '', '1'),
(689, 35, 'App\\Page', '', '1'),
(703, 87, 'App\\Page', '', '1'),
(798, 34, 'App\\Page', '', '1'),
(816, 37, 'App\\Page', '', '1'),
(817, 83, 'App\\Page', '', '1'),
(852, 105, 'App\\Page', '', '1'),
(1018, 33, 'App\\Page', '', '1'),
(1028, 28, 'App\\Page', '', '1'),
(1143, 119, 'App\\Page', '', '1'),
(1144, 1, 'App\\Page', '', '1'),
(1145, 42, 'App\\Page', '', '1'),
(1146, 43, 'App\\Page', '', '1'),
(1147, 11, 'App\\Page', '', '1'),
(1148, 46, 'App\\Page', '', '1'),
(1149, 47, 'App\\Page', '', '1'),
(1150, 48, 'App\\Page', '', '1'),
(1151, 49, 'App\\Page', '', '1'),
(1152, 12, 'App\\Page', '', '1'),
(1153, 40, 'App\\Page', '', '1'),
(1154, 41, 'App\\Page', '', '1'),
(1155, 121, 'App\\Page', '', '1'),
(1156, 122, 'App\\Page', '', '1'),
(1173, 123, 'App\\Page', '', '1'),
(1174, 125, 'App\\Page', '', '1'),
(1175, 124, 'App\\Page', '', '1'),
(1176, 126, 'App\\Page', '', '1'),
(1177, 32, 'App\\Page', '', '1'),
(1178, 29, 'App\\Page', '', '1'),
(1179, 30, 'App\\Page', '', '1'),
(1180, 31, 'App\\Page', '', '1'),
(1181, 59, 'App\\Page', '', '1'),
(1182, 45, 'App\\Page', '', '1'),
(1183, 60, 'App\\Page', '', '1'),
(1184, 61, 'App\\Page', '', '1'),
(1185, 127, 'App\\Page', '', '1'),
(1186, 128, 'App\\Page', '', '1'),
(1187, 129, 'App\\Page', '', '1'),
(1188, 131, 'App\\Page', '', '1'),
(1189, 90, 'App\\Page', '', '1'),
(1190, 67, 'App\\Page', '', '1'),
(1191, 21, 'App\\Page', '', '1'),
(1192, 20, 'App\\Page', '', '1'),
(1193, 68, 'App\\Page', '', '1'),
(1194, 70, 'App\\Page', '', '1'),
(1195, 69, 'App\\Page', '', '1'),
(1196, 27, 'App\\Page', '', '1'),
(1197, 71, 'App\\Page', '', '1'),
(1198, 72, 'App\\Page', '', '1'),
(1199, 73, 'App\\Page', '', '1'),
(1200, 74, 'App\\Page', '', '1'),
(1201, 75, 'App\\Page', '', '1'),
(1202, 76, 'App\\Page', '', '1'),
(1203, 77, 'App\\Page', '', '1'),
(1204, 78, 'App\\Page', '', '1'),
(1205, 79, 'App\\Page', '', '1'),
(1206, 80, 'App\\Page', '', '1'),
(1207, 81, 'App\\Page', '', '1'),
(1208, 13, 'App\\Page', '', '1'),
(1209, 94, 'App\\Page', '', '1'),
(1210, 117, 'App\\Page', '', '1'),
(1211, 95, 'App\\Page', '', '1'),
(1212, 96, 'App\\Page', '', '1'),
(1213, 109, 'App\\Page', '', '1'),
(1214, 110, 'App\\Page', '', '1'),
(1215, 111, 'App\\Page', '', '1'),
(1216, 112, 'App\\Page', '', '1'),
(1217, 113, 'App\\Page', '', '1'),
(1218, 114, 'App\\Page', '', '1'),
(1219, 115, 'App\\Page', '', '1'),
(1220, 116, 'App\\Page', '', '1'),
(1221, 118, 'App\\Page', '', '1'),
(1222, 14, 'App\\Page', '', '1'),
(1223, 97, 'App\\Page', '', '1'),
(1224, 98, 'App\\Page', '', '1'),
(1225, 99, 'App\\Page', '', '1'),
(1226, 100, 'App\\Page', '', '1'),
(1227, 101, 'App\\Page', '', '1'),
(1228, 136, 'App\\Page', '', '1'),
(1229, 103, 'App\\Page', '', '1'),
(1230, 104, 'App\\Page', '', '1'),
(1231, 133, 'App\\Page', '', '1'),
(1232, 137, 'App\\Page', ' ', '1'),
(1233, 106, 'App\\Page', '', '1'),
(1234, 15, 'App\\Page', '', '1'),
(1235, 16, 'App\\Page', '', '1'),
(1236, 39, 'App\\Page', '', '1'),
(1237, 120, 'App\\Page', '', '1'),
(1239, 10, 'App\\Page', '', '1'),
(1246, 12, 'App\\Post', '', '1'),
(1247, 3, 'App\\Post', 'date', '1'),
(1248, 9, 'App\\Post', '', '1'),
(1249, 13, 'App\\Post', '', '1'),
(1250, 1, 'App\\Post', '', '1'),
(1251, 4, 'App\\Post', '', '1'),
(1252, 15, 'App\\Post', '', '1'),
(1253, 15, 'App\\Post', '', '2'),
(1254, 14, 'App\\Post', '', '1'),
(1255, 2, 'App\\Post', 'date', '1'),
(1256, 5, 'App\\Post', '', '1'),
(1257, 10, 'App\\Post', '', '1'),
(1258, 6, 'App\\Post', '', '1'),
(1259, 11, 'App\\Post', '', '1'),
(1260, 7, 'App\\Post', '', '1'),
(1261, 8, 'App\\Post', '', '1'),
(1262, 141, 'App\\Page', '', '1'),
(1264, 142, 'App\\Page', '333', '3'),
(1265, 85, 'App\\Page', '', '1'),
(1266, 138, 'App\\Page', '', '1'),
(1267, 139, 'App\\Page', '', '1'),
(1269, 86, 'App\\Page', '', '1'),
(1270, 140, 'App\\Page', '', '1'),
(1271, 44, 'App\\Page', '', '1'),
(1272, 3, 'App\\Page', '', '1'),
(1274, 17, 'App\\Page', 'video', '1'),
(1303, 4, 'App\\Page', 'banner-video', '1'),
(1304, 4, 'App\\Page', 'banner-cta-text', '2'),
(1305, 4, 'App\\Page', 'banner-cta-button-text', '3'),
(1306, 4, 'App\\Page', 'banner-cta-button-link', '4'),
(1309, 142, 'App\\Page', '222', '2'),
(1310, 16, 'App\\Post', '', '1'),
(1311, 17, 'App\\Post', '', '1'),
(1312, 18, 'App\\Post', '', '1'),
(1313, 143, 'App\\Page', 'banner-video', '1'),
(1314, 143, 'App\\Page', 'banner-cta-button-link-2', '2'),
(1315, 143, 'App\\Page', 'banner-cta-button-text-1', '3'),
(1316, 143, 'App\\Page', 'banner-cta-button-link-1', '4'),
(1317, 143, 'App\\Page', 'banner-cta-button-text-2', '5'),
(1318, 143, 'App\\Page', 'banner-cta-button-link-2', '6'),
(1319, 143, 'App\\Page', 'callback-heading', '7'),
(1320, 143, 'App\\Page', 'callback-description', '8'),
(1321, 143, 'App\\Page', 'callback-btn-text', '9'),
(1322, 143, 'App\\Page', 'callback-btn-link', '10'),
(1323, 143, 'App\\Page', 'callback-bg-image', '11'),
(1324, 143, 'App\\Page', 'awards-heading', '12'),
(1326, 143, 'App\\Page', 'testimonial-video-bottom', '13'),
(1328, 143, 'App\\Page', 'testimonial-heading-bottom', '14'),
(1329, 143, 'App\\Page', 'banner-cta-text', '15'),
(1330, 144, 'App\\Page', 'banner', '1'),
(1331, 144, 'App\\Page', 'rightColumn', '2'),
(1332, 144, 'App\\Page', 'right-heading-title', '3'),
(1333, 144, 'App\\Page', 'right-heading-link', '4'),
(1334, 144, 'App\\Page', 'right-heading-logo', '5'),
(1335, 19, 'App\\Post', '', '1'),
(1336, 20, 'App\\Post', '', '1'),
(1337, 145, 'App\\Page', '', '1'),
(1338, 21, 'App\\Post', '', '1'),
(1339, 22, 'App\\Post', '', '1'),
(1340, 23, 'App\\Post', '', '1'),
(1341, 24, 'App\\Post', '', '1'),
(1342, 25, 'App\\Post', '', '1'),
(1343, 26, 'App\\Post', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_translations`
--

CREATE TABLE `custom_field_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `custom_field_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_field_translations`
--

INSERT INTO `custom_field_translations` (`id`, `custom_field_id`, `locale`, `value`) VALUES
(25, 25, 'en', ''),
(47, 47, 'en', ''),
(48, 48, 'en', ''),
(50, 50, 'en', ''),
(54, 54, 'en', ''),
(56, 56, 'en', ''),
(57, 57, 'en', ''),
(58, 58, 'en', ''),
(93, 93, 'en', ''),
(267, 267, 'en', ''),
(292, 292, 'en', ''),
(293, 293, 'en', ''),
(318, 318, 'en', ''),
(319, 319, 'en', ''),
(320, 320, 'en', ''),
(321, 321, 'en', ''),
(322, 322, 'en', ''),
(323, 323, 'en', ''),
(324, 324, 'en', ''),
(325, 325, 'en', ''),
(326, 326, 'en', ''),
(329, 329, 'en', ''),
(341, 341, 'en', ''),
(342, 342, 'en', ''),
(343, 343, 'en', ''),
(344, 344, 'en', ''),
(345, 345, 'en', ''),
(382, 382, 'en', ''),
(435, 435, 'en', ''),
(437, 437, 'en', ''),
(500, 500, 'en', ''),
(565, 565, 'en', ''),
(566, 566, 'en', ''),
(627, 627, 'en', ''),
(658, 658, 'en', ''),
(659, 659, 'en', ''),
(660, 660, 'en', ''),
(661, 661, 'en', ''),
(686, 686, 'en', ''),
(687, 687, 'en', ''),
(689, 689, 'en', ''),
(703, 703, 'en', ''),
(798, 798, 'en', ''),
(816, 816, 'en', ''),
(817, 817, 'en', ''),
(852, 852, 'en', ''),
(1018, 1018, 'en', ''),
(1028, 1028, 'en', ''),
(1143, 1143, 'ar', ''),
(1144, 1144, 'ar', ''),
(1145, 1145, 'ar', ''),
(1146, 1146, 'ar', ''),
(1147, 1147, 'ar', ''),
(1148, 1148, 'ar', ''),
(1149, 1149, 'ar', ''),
(1150, 1150, 'ar', ''),
(1151, 1151, 'ar', ''),
(1152, 1152, 'ar', ''),
(1153, 1153, 'ar', ''),
(1154, 1154, 'ar', ''),
(1155, 1155, 'ar', ''),
(1156, 1156, 'ar', ''),
(1173, 1173, 'ar', ''),
(1174, 1174, 'ar', ''),
(1175, 1175, 'ar', ''),
(1176, 1176, 'ar', ''),
(1177, 1177, 'ar', ''),
(1178, 1178, 'ar', ''),
(1179, 1179, 'ar', ''),
(1180, 1180, 'ar', ''),
(1181, 1181, 'ar', ''),
(1182, 1182, 'ar', ''),
(1183, 1183, 'ar', ''),
(1184, 1184, 'ar', ''),
(1185, 1185, 'ar', ''),
(1186, 1186, 'ar', ''),
(1187, 1187, 'ar', ''),
(1188, 1188, 'ar', ''),
(1189, 1189, 'ar', ''),
(1190, 1190, 'ar', ''),
(1191, 1191, 'ar', ''),
(1192, 1192, 'ar', ''),
(1193, 1193, 'ar', ''),
(1194, 1194, 'ar', ''),
(1195, 1195, 'ar', ''),
(1196, 1196, 'ar', ''),
(1197, 1197, 'ar', ''),
(1198, 1198, 'ar', ''),
(1199, 1199, 'ar', ''),
(1200, 1200, 'ar', ''),
(1201, 1201, 'ar', ''),
(1202, 1202, 'ar', ''),
(1203, 1203, 'ar', ''),
(1204, 1204, 'ar', ''),
(1205, 1205, 'ar', ''),
(1206, 1206, 'ar', ''),
(1207, 1207, 'ar', ''),
(1208, 1208, 'ar', ''),
(1209, 1209, 'ar', ''),
(1210, 1210, 'ar', ''),
(1211, 1211, 'ar', ''),
(1212, 1212, 'ar', ''),
(1213, 1213, 'ar', ''),
(1214, 1214, 'ar', ''),
(1215, 1215, 'ar', ''),
(1216, 1216, 'ar', ''),
(1217, 1217, 'ar', ''),
(1218, 1218, 'ar', ''),
(1219, 1219, 'ar', ''),
(1220, 1220, 'ar', ''),
(1221, 1221, 'ar', ''),
(1222, 1222, 'ar', ''),
(1223, 1223, 'ar', ''),
(1224, 1224, 'ar', ''),
(1225, 1225, 'ar', ''),
(1226, 1226, 'ar', ''),
(1227, 1227, 'ar', ''),
(1228, 1228, 'ar', ''),
(1229, 1229, 'ar', ''),
(1230, 1230, 'ar', ''),
(1231, 1231, 'ar', ''),
(1232, 1232, 'ar', ''),
(1233, 1233, 'ar', ''),
(1234, 1234, 'ar', ''),
(1235, 1235, 'ar', ''),
(1236, 1236, 'ar', ''),
(1237, 1237, 'ar', ''),
(1239, 1239, 'ar', ''),
(1246, 1246, 'ar', ''),
(1247, 1247, 'ar', ''),
(1248, 1248, 'ar', ''),
(1249, 1249, 'ar', ''),
(1250, 1250, 'ar', ''),
(1251, 1251, 'ar', ''),
(1252, 1252, 'ar', ''),
(1253, 1253, 'ar', ''),
(1254, 1254, 'ar', ''),
(1255, 1255, 'ar', ''),
(1256, 1256, 'ar', ''),
(1257, 1257, 'ar', ''),
(1258, 1258, 'ar', ''),
(1259, 1259, 'ar', ''),
(1260, 1260, 'ar', ''),
(1261, 1261, 'ar', ''),
(1262, 1262, 'ar', ''),
(1264, 1264, 'en', 'ccc'),
(1265, 1265, 'ar', ''),
(1266, 1266, 'ar', ''),
(1267, 1267, 'ar', ''),
(1269, 1269, 'ar', ''),
(1270, 1270, 'ar', ''),
(1271, 1271, 'ar', ''),
(1272, 1272, 'ar', ''),
(1274, 1274, 'en', '_pkMRC0Qjt4'),
(1303, 1303, 'en', '_pkMRC0Qjt4'),
(1304, 1304, 'en', ' Inspiring People.<br/>Shaping Success.'),
(1305, 1305, 'en', 'About Us'),
(1306, 1306, 'en', '/about'),
(1307, 1303, 'ar', '_pkMRC0Qjt4'),
(1308, 1304, 'ar', 'إلهام الأفراد<br/> تشكيل النجاح'),
(1309, 1305, 'ar', 'من نحن'),
(1310, 1306, 'ar', '/ar/about'),
(1311, 1028, 'ar', ''),
(1312, 1252, 'en', ''),
(1313, 1253, 'en', ''),
(1314, 1271, 'en', ''),
(1315, 1222, 'en', ''),
(1317, 1264, 'ar', 'Test'),
(1320, 1309, 'en', 'bbb'),
(1321, 1269, 'en', ''),
(1322, 1270, 'en', ''),
(1323, 1265, 'en', ''),
(1324, 1179, 'en', ''),
(1325, 1175, 'en', ''),
(1326, 1310, 'en', ''),
(1327, 1310, 'ar', ''),
(1328, 1156, 'en', ''),
(1329, 1224, 'en', ''),
(1330, 1173, 'en', ''),
(1331, 1153, 'en', ''),
(1332, 1182, 'en', ''),
(1333, 1311, 'en', ''),
(1334, 1144, 'en', ''),
(1335, 1312, 'en', ''),
(1336, 1178, 'en', ''),
(1337, 1189, 'en', ''),
(1338, 1186, 'en', ''),
(1339, 1313, 'en', '_pkMRC0Qjt4'),
(1340, 1314, 'en', '/about'),
(1341, 1315, 'en', 'Free Consultation'),
(1342, 1316, 'en', '/contact'),
(1343, 1317, 'en', 'Learn More'),
(1344, 1318, 'en', '/about'),
(1345, 1319, 'en', 'Request a Call Back'),
(1346, 1320, 'en', 'One of our team will be in touch to understand your needs. We look forward to hearing from you!'),
(1347, 1321, 'en', 'Contact US'),
(1348, 1322, 'en', '/contact'),
(1349, 1323, 'en', '/uploads/images/bfec160006c49cc8ebc0dc8ac7f03ef029264648.jpg'),
(1350, 1324, 'en', 'Awards & Recognition of PSI Middle East'),
(1352, 1326, 'en', '_pkMRC0Qjt4'),
(1354, 1328, 'en', 'But Don\'t Take Our Word For It'),
(1355, 1329, 'en', 'Partnering with organisations to<br />identify, develop and energerise talent '),
(1356, 1330, 'en', 'uploads/images/istartstrong@2x.jpg'),
(1357, 1331, 'en', 'yes'),
(1358, 1332, 'en', 'As heard on Dubaieye 103.8'),
(1359, 1333, 'en', 'Listen here <a href=\"https://t.co/A1bszYWdu7\" target=\"_blank\">https://t.co/A1bszYWdu7</a>'),
(1360, 1334, 'en', '/uploads/images/a69fa63d06152b06a0778fecf1826c112e998e51.png'),
(1361, 1145, 'en', ''),
(1362, 1335, 'ar', ''),
(1363, 1335, 'en', ''),
(1364, 1239, 'en', ''),
(1365, 1336, 'en', ''),
(1366, 1337, 'en', ''),
(1367, 1338, 'en', ''),
(1368, 1251, 'en', ''),
(1369, 1272, 'en', ''),
(1370, 1339, 'en', ''),
(1371, 1340, 'en', ''),
(1372, 1143, 'en', ''),
(1373, 1341, 'en', ''),
(1374, 1342, 'en', ''),
(1375, 1343, 'en', ''),
(1376, 1209, 'en', ''),
(1377, 1155, 'en', ''),
(1378, 1194, 'en', ''),
(1379, 1207, 'en', ''),
(1380, 1232, 'en', ''),
(1381, 1236, 'en', ''),
(1382, 1208, 'en', ''),
(1383, 1235, 'en', ''),
(1384, 1234, 'en', ''),
(1385, 1147, 'en', '');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `dp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dp_tinified` int(1) NOT NULL DEFAULT '0',
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `parent_id`, `name`, `lft`, `rgt`, `depth`, `dp`, `dp_tinified`, `facebook`, `twitter`, `linkedin`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 0, 'Amanda White', 1, 2, 0, 'uploads/images/employees/1b6453892473a467d07372d45eb05abc2031647a.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/amanda-white-5bbba78?authType=NAME_SEARCH&authToken=2liG&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A26845343%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474173516%2Ctas%3Aamanda%20whi', 1, 1, 4, '2016-08-01 05:35:53', '2018-11-27 02:36:02'),
(5, 0, 'Humaira Anwer', 15, 16, 0, 'uploads/images/employees/ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/hanwer?authType=NAME_SEARCH&authToken=9IoY&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A77347276%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474280125%2Ctas%3Ahum', 0, 2, 4, '2016-08-12 08:46:09', '2018-09-20 07:38:59'),
(6, 0, 'Jonathan Rook', 13, 14, 0, 'uploads/images/employees/c1dfd96eea8cc2b62785275bca38ac261256e278.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/jonathan-rook-cpsychol-afbpss-6112502?authType=NAME_SEARCH&authToken=1BTC&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A6574189%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474313746%2Cta', 0, 2, 4, '2016-08-12 08:53:26', '2018-09-20 07:38:43'),
(7, 0, 'Sharan Gohel', 3, 4, 0, 'uploads/images/employees/902ba3cda1883801594b6e1b452790cc53948fda.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/sharangohel?authType=NAME_SEARCH&authToken=3K0s&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A43212514%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474204325%2Ctas%3Asharan%20', 1, 2, 2, '2016-08-12 08:55:07', '2018-07-31 07:54:04'),
(8, 0, 'Liesl Connolly', 17, 18, 0, 'uploads/images/employees/fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f.PNG', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/liesldup?authType=NAME_SEARCH&authToken=0pol&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A143056157%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474410256%2Ctas%3Alie', 0, 2, 2, '2016-08-12 09:07:19', '2018-07-31 07:55:21'),
(9, 0, 'Abrar Al Mubarak', 19, 20, 0, 'uploads/images/employees/0ade7c2cf97f75d009975f4d720d1fa6c19f4897.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/abraralmubarak?authType=NAME_SEARCH&authToken=5VXM&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A278410317%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474343865%2Ctas%3Aabra', 1, 2, 4, '2016-08-12 09:08:16', '2018-12-18 03:50:28'),
(10, 0, 'Sam Day', 43, 44, 0, 'uploads/images/employees/b1d5781111d84f7b3fe45a0852e59758cd7a87e5.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/samantha-day-62338b38?authType=NAME_SEARCH&authToken=PBHJ&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A132315579%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474481803%2Ctas%3Asam', 1, 2, 2, '2016-08-12 09:16:08', '2018-07-31 07:56:37'),
(11, 0, 'Joanna Rodrigues', 45, 46, 0, 'uploads/images/employees/17ba0791499db908433b80f37c5fbc89b870084b.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/joanna-rodrigues-32b8a2116?authType=NAME_SEARCH&authToken=nFUG&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A485931347%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474521347%2Ctas%3Ajoann', 1, 2, 2, '2016-08-12 09:18:01', '2018-07-31 07:56:46'),
(12, 0, 'Pushpa Mani', 47, 48, 0, 'uploads/images/employees/7b52009b64fd0a2a49e6d8a939753077792b0554.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/pushpa-mani-2b03671b?authType=NAME_SEARCH&authToken=UPy4&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A69561060%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474547332%2Ctas%3Apushpa', 1, 2, 4, '2016-08-12 09:19:31', '2018-11-27 08:50:07'),
(13, 0, 'Catherine Riya', 49, 50, 0, 'uploads/images/employees/bd307a3ec329e10a2cff8fb87480823da114f8f4.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/catherine-riya-joseph-86093372?authType=NAME_SEARCH&authToken=agyV&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A259102728%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474567732%2Ctas%3Ac', 1, 2, 2, '2016-08-12 09:20:22', '2018-07-31 07:57:00'),
(14, 0, 'Abi Krimeed (White)', 5, 6, 0, 'uploads/images/employees/fa35e192121eabf3dabf9f5ea6abdbcbc107ac3b.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/abi-white-2792617?authType=NAME_SEARCH&authToken=44LD&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A21526077%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474241056%2Ctas%3Aabi', 1, 2, 2, '2016-08-15 02:45:47', '2018-07-31 07:54:15'),
(16, 0, 'Kristina Beggs', 39, 40, 0, 'uploads/images/employees/1574bddb75c78a6fd2251d61e2993b5146201319.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', 'https://www.linkedin.com/in/kristinabeggs?authType=NAME_SEARCH&authToken=8pFk&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A31684191%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1472474429593%2Ctas%3Akris', 1, 2, 4, '2016-08-15 07:52:38', '2018-11-27 08:58:14'),
(17, 0, 'Suzanne Hart', 37, 38, 0, 'uploads/images/employees/0716d9708d321ffb6a00818614779e779925365c.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', '', 1, 2, 4, '2016-08-15 10:37:30', '2018-11-27 08:53:35'),
(18, 0, 'Joe Bruce', 23, 24, 0, 'uploads/images/employees/9e6a55b6b4563e652a23be9d623ca5055c356940.jpg', 0, '', '', '', 0, 2, 4, '2016-08-15 10:54:07', '2018-09-26 03:29:43'),
(19, 0, 'Ayman Nadim', 51, 52, 0, 'uploads/images/employees/b3f0c7f6bb763af1be91d9e74eabfeb199dc1f1f.png', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', '', 0, 2, 2, '2016-08-18 02:18:04', '2018-07-31 07:57:23'),
(20, 0, 'Aref Al Mubarak', 21, 22, 0, 'uploads/images/employees/91032ad7bbcb6cf72875e8e8207dcfba80173f7c.jpg', 0, 'https://www.facebook.com/innovativehr/', 'https://twitter.com/IHSDubai', '', 1, 3, 2, '2016-09-04 02:21:14', '2018-07-31 07:55:35'),
(21, 0, 'Simon Boag', 9, 10, 0, 'uploads/images/employees/472b07b9fcf2c2451e8781e944bf5f77cd8457c8.jpg', 1, '', '', '', 1, 4, 4, '2017-06-20 03:30:31', '2018-12-18 03:49:41'),
(22, 0, 'Simon Tither', 25, 26, 0, 'uploads/images/employees/12c6fc06c99a462375eeb3f43dfd832b08ca9e17.jpg', 0, '', '', '', 1, 4, 4, '2017-06-20 03:37:54', '2018-12-18 03:54:30'),
(23, 0, 'Tracy Stodart', 7, 8, 0, 'uploads/images/employees/d435a6cdd786300dff204ee7c2ef942d3e9034e2.jpg', 0, '', '', '', 1, 4, 4, '2017-08-07 05:31:01', '2018-12-18 03:49:15'),
(24, 0, 'Louise Kidd', 29, 30, 0, 'uploads/images/employees/4d134bc072212ace2df385dae143139da74ec0ef.jpg', 0, '', '', '', 1, 4, 2, '2017-08-07 05:42:39', '2018-05-23 03:43:37'),
(25, 0, 'Inas Farid', 27, 28, 0, 'uploads/images/employees/f6e1126cedebf23e1463aee73f9df08783640400.jpg', 0, '', '', '', 1, 4, 4, '2017-08-07 05:56:18', '2017-11-13 11:11:44'),
(26, 0, 'Waleed Mowafy', 31, 32, 0, 'uploads/images/employees/887309d048beef83ad3eabf2a79a64a389ab1c9f.jpg', 0, '', '', '', 0, 4, 4, '2017-08-07 06:08:33', '2018-09-26 03:30:17'),
(27, 0, 'Madeleine Anne Yorke Harling', 35, 36, 0, 'uploads/images/employees/bc33ea4e26e5e1af1408321416956113a4658763.jpg', 0, '', '', '', 1, 4, 4, '2017-08-10 07:44:48', '2018-02-19 16:34:19'),
(28, 0, 'Majid Sharif', 33, 34, 0, 'uploads/images/employees/0a57cb53ba59c46fc4b692527a38a87c78d84028.jpg', 0, '', '', '', 1, 4, 4, '2017-08-13 02:42:01', '2017-11-13 11:11:44'),
(29, 0, 'Saeed Al Yaqoub', 41, 42, 0, 'uploads/images/employees/7719a1c782a1ba91c031a682a0a2f8658209adbf.jpg', 0, '', '', '', 1, 4, 4, '2017-08-16 02:26:00', '2018-11-27 08:52:12'),
(30, 0, 'Marais Bester', 11, 12, 0, 'uploads/images/employees/22d200f8670dbdb3e253a90eee5098477c95c23d.jpg', 0, '', '', '', 1, 4, 2, '2017-11-13 11:11:44', '2018-08-13 03:25:03');

-- --------------------------------------------------------

--
-- Table structure for table `employee_translations`
--

CREATE TABLE `employee_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_translations`
--

INSERT INTO `employee_translations` (`id`, `employee_id`, `locale`, `title`, `designation`, `content`) VALUES
(4, 4, 'en', 'Amanda White - Managing Director', 'MBA, BA (Hons) Education (Psychology & Sociology)', '<p>Amanda is the Managing Director of&nbsp;PSI Middle East and her passion lies in the design of leadership assessment and development programmes, organisation cultural change, succession planning and talent management. Her personal assignments have seen her working across the GCC in sectors as diverse as government, oil and gas and engineering to hospitality and agriculture.</p>\r\n<p>Prior to coming to the region, Amanda established and ran a consulting business in the UK and wider Europe providing services largely to the Citi investment banking and professional services sector for 12 years, before its acquisition by New York Stock-Listed Company RR Donnelley &amp; Sons (RRD) in 2004.</p>\r\n<p>Today Amanda is a regular guest on Dubai Eye Radio lending her human capital insights to current global affairs. She has formerly held positions on the Executive Committee of the Recruitment and Employment Confederation (UK) as well as a member of the Bank of England City Advisory Committee from 2001 to 2008. Her MBTI Type is ENTP.</p>'),
(5, 5, 'en', 'Humaira Anwer - Senior Occupational Psychologist', 'MSc Work Psychology and Business, Masters in Human Resource Management (CIPD), BPsych, Chartered Occupational Psychologist', '<p>Humaira is an experienced Occupational Psychologist with a passion for developing Competency Frameworks and developing customised personality psychometrics, Leadership Assessment and Development programmes and designing and running 360 degree feedback programmes. Humaira has worked with West Midlands Police (UK), Accenture Consulting, the London Home Office and Ministry of Defence in various specialist human capital consulting roles. For the Ministry of Defence Humaira led a project within the Defence Medical Services, mapping the Defence Leadership Framework with the NHS Leadership Framework and providing training solutions to bridge the competency gaps.</p>\r\n<p>She holds a Masters in Work Psychology and Business, Masters in Human Resource Management, Bachelor in Psychology, Chartered Member of the British Psychological Society, Registered Psychologist with the Health and Care Professions Council, Trained Career Coach; Career Counselling Services. Humaira is an Associate Fellow of the British Psychological Society and is a member of the International Society for Coaching Psychology (ISCP). Member of the British Psychological Society, Registered Psychologist with the Health and Care Professions Council, Level A and B Occupational Testing Accredited, Trained Career Coach; Career Counselling Services and Trained Assessor &amp; Centre Manager. Her MBTI type is ESFJ.</p>'),
(6, 6, 'en', 'Jonathan Rook - Senior Occupational Psychologist', 'MSc Applied Psychology, PGC Occupational Psychology, Chartered Occupational Psychologist', '<p>Jonathan is a Chartered Occupational Psychologist with a broad based human capital consulting experience working across public and private sectors, including Central Government, Financial Services, Retail, Pharmaceuticals and Telecommunications. Aside from his in-depth experience across the GCC, he has worked in UK, Portugal, Switzerland and India.</p>\r\n<p>His particular passion lies in psychometric test development, talent analytics, organisational change management and HR consulting with key experience in selection and assessment, leadership development, executive assessment, job analysis &amp; competency development, career counselling and coaching.</p>\r\n<p>Jonathan is an Associate Fellow of the British Psychological Society, Full Member of the Division of Occupational Psychology, holds a Certificate in Coaching with the Centre for Coaching, member of the International Society for Coaching Psychology (ISCP) and Registered Psychologist with the Health Professions Council.</p>\r\n<p>Jonathan&rsquo;s publications include; &lsquo;Enabling Organisational Culture Change within Aviva: Case Study&rsquo;, British Psychological Society 2010, Division of Occupational Psychology Annual Conference; Zijlstra, F.R.H. &amp; Rook. J.W. (2005) &lsquo;The Weekly Cycle of Work and Rest&rsquo;; R.A. Roe, M.J. Waller and S. Clegg (Eds.) &lsquo;Time in organizations&rsquo; &ndash; Approaches and methods, London: Routledge; Rook, J.W. &amp; Zijlstra, F.R.H. &lsquo;The contribution of various types of activities to recovery&rsquo;, European Journal of Work and Organisational Psychology (2005), Vol. 15 (2); &lsquo;Defining Moments&rsquo;, The Psychologist (2002), Vol.15 (11), Letter to the Editor; &lsquo;Stress &amp; Recovery: A review of the literature&rsquo;, IPCD (2015);and most recently &lsquo;The Cost of Organizational Stress&rsquo;, CEO Magazine, June 2016, Opinion piece.</p>'),
(7, 7, 'en', 'Sharan Gohel - Director of Consulting Services', 'MSc Work Psychology & Business, BSc, Chartered Occupational Psychologist', '<p>Sharan Gohel leads the Innovative HR Solutions consulting team and psychometric test library, managing a vast array of assessment and development initiatives across a broad spectrum of clients and industry-sectors. Her key expertise lies in the application of psychology to assessment and development, Executive Coaching and Organisational Development such as change management, cultural analysis and performance management.</p>\r\n<p>Sharan has previously worked in the UK in the telecommunications and manufacturing industries as an in-house Occupational Psychologist. She also worked for the UK Government focusing on the assessment and development of individuals, teams and organisational development. In this capacity Sharan conducted an extensive research project looking at organisational culture and gender before developing an online entrepreneurial assessment tool.</p>\r\n<p>Sharan is an Associate Fellow of the British Psychological Society (AFBPsS), Registered Practitioner with the Health Professionals Council (HPC), Member of the Society of Coaching Psychology, Full Member of the Division of Occupational Psychology, Member of The Psychological Testing Centre (PTC). Her MBTI Type is ENTP.</p>'),
(8, 8, 'en', 'Liesl Connolly - Senior Occupational Psychologist', 'M.Com (Industrial Psychology), B.Com (Hons) HRM, Registered Industrial Psychologist', '<p>Liesl is a confident and successful Senior Occupational Psychologist with a strong HR background gained over the past few years in the GCC as well as South Africa. Her personal assignments to date includes the successful delivery of multiple large-scale talent assessment and development programmes in Government, Oil &amp; Gas and Construction.</p>\r\n<p>In the past Liesl has directed International HR Operations and worked as a consultant in the Industrial Psychologist, Human Resources and HRIS fields. She has been involved in Talent Acquisition, Employee Benefit Management, Change Management, Talent Development, Succession Planning, Coaching and Mentoring, as well as Performance Management. She is a Registered Industrial Psychologist and Accredited Brain-based Certified Coach. Her MBTI type is ENFJ.</p>'),
(9, 9, 'en', 'Abrar Al Mubarak - Senior Consultant', 'BSc Mass Communications and Psychology', '<p>Abrar is originally from Saudi Arabia and is bilingual English and Arabic. She is a licensed ICF Coach and a trained Assessor and Centre Manager and participates across both our English and Arabic programmes. Today she has leveraged her skills and learning and become integral in the development of our Arabic products and services, and project managing our Saudi-based projects. She has worked on various translations of psychometric and exercise tools in Arabic and is currently leading the development of a major career interest tool for the Saudi Government.</p>\r\n<p>Abrar holds a Bachelor&rsquo;s Degree in Mass Communication and Political Science with a minor in Psychology, gained internationally in 3 Universities across 3 countries; a programme designed to promote a global perspective. Her first year was in Bangalore, India (1 year), then Christ Church, New Zealand (2 years) before completing the programme in Pittsburgh, in the United States.</p>\r\n<p>She commenced her career as a Reporter with the Saudi Gazette Newspaper whilst representing her country as an Ambassador and Mediator between the University and The Saudi Arabian Cultural Mission. Abrar has set up and manages our Riyadh office and Saudi clients. Her MBTI Type is ENTP.</p>'),
(10, 10, 'en', 'Sam Day - Planning & Project Support', 'English Language and Literature Dip', '<p>Sam comes from a background in logistics, administration and customer service and has brought these project management skills and experience to Innovative HR Solutions in our Psychometric Test Library. Sam is dedicated, creative and detail-focused and passionate about finding a win-win for both the business and customer.</p>\r\n<p>In her role as Accreditation Programme Manager she liaises with our International Test Publishers and our prospective clients to ensure we are able to provide up-to-date information on accreditation and qualifications requirements, support and encouragement. Her responsibilities include planning of our course calendar across 3 countries, courseware development and certification process, as well as delegate registration and welfare. She also provides invaluable support to the Managing Director and Director of Operations overseeing operational requirements and managing ad hoc client projects across the GCC. Her MBTI type is ENTJ.</p>'),
(11, 11, 'en', 'Joanna Rodrigues - Project Support Coordinator', 'MBA (Marketing and HR), BA', '<p>As Project Support Coordinator Joanna&rsquo;s role is to set up and coordinate all our Human Capital projects and Assessment and Development Centres activities. Working with Manager of Planning and Project Support to book resources, arrange venues and materials, through to ensuring individual reports are prepared on time and coaching session logs are maintained. Joanna first joined us in 2012 and has made a big difference as she goes about making sure everyone is in the right place at the right time! Joanna&rsquo;s background was previously in administration in the advertising sector in Dubai and Comtech Computer Academy in India. Her MBTI Type is ESTJ</p>'),
(12, 12, 'en', 'Pushpa Mani - Psychometric Test Library Manager', 'Bachelors in Commerce', '<p>As Psychometric Test Library Manager, Pushpa has been instrumental in the growth and development of our test library product offering over the 17 years she has been working with PSI Middle East.</p>\r\n<p>Pushpa oversees our team of psychometric administrators and supports clients as they select the appropriate tools, exercises and norm groups and consider Accreditation Courses to support their talent programmes. She liaises with our International Test Publishers and manages over 10 online platforms. Pushpa is a source of tremendous support for all clients. Her MBTI Type is ESTJ.</p>'),
(13, 13, 'en', 'Catherine Riya - Library Coordinator', 'PGDip Business Administration (Marketing & Event Management)', '<p>Catherine&rsquo;s area of expertise lies in project management and stakeholder engagement in the HR field. She manages and supports a number of our human capital and assessment processes with clients and supports with psychometric tools and exercise selection and administration. She is an experienced trainer of new staff, devising training programmes for new employees and clients when they purchase new platforms for the first time. She liaises extensively with clients and candidates to ensure testing requirements have been met. Her MBTI Type is ISTJ.</p>'),
(14, 14, 'en', 'Abi Krimeed (White) - Principal Consultant,  Learning Solutions ', 'BSc (Hons) Business Management', '<p>Abi is a highly-experienced Senior Learning and Development Specialist and training practitioner, with a strong background in designing and facilitating leadership development workshops, assessing on Assessment and Development Centres and developing Human Capital solutions. She has led our development activities as both architect of the programmes we run, conducting train the trainer programmes to ensure consistency across both our English and Arabic speaking Facilitators and undertaking a role in the ongoing quality assessment.</p>\r\n<p>She is an ICF Certified Coach (International Coaching Federation), a Qualified Assessment Centre Designer, Manager and Assessor, a Licensed Facilitator of Stephen Covey&rsquo;s work, a Certified Facilitator of Emotional Intelligence Leadership Programme and a Licensed Facilitator of Myers Briggs. She has facilitated customised sessions across a broad range of programmes including our Mastering the Leadership Code 5-day Modular Programme, Coaching Skills for Senior Managers, Emotional Intelligence, Team Work, Collaboration, Managing Change, Leading High Performing Teams and Communication.</p>\r\n<p>Abi has previously worked across various industries working with the Jumeirah Hotel Group and Dubai Holding (TECOM Investments). Her role as Director of Performance, Talent and Training for TECOM Investments involved designing the TECOM Succession Planning Framework and running organisation-wide Development Centres across the leadership team. Her MBTI type is INTP.</p>'),
(16, 16, 'en', 'Kristina Beggs - Business Development & Marketing Manager', 'BA Hons European Languages and Management Studies', '<p>Kristina leads our business development efforts to enhance clients navigation of our services, and build talent solutions with the input of our consulting team. She is passionate about client service and can be regularly seen preparing proposals and presentations, meeting with clients and ensuring our clients have the right solution and are delighted with the end result.</p>\r\n<p>She began her career in the United Kingdom in a Big Data organisation working with PR/Marketing and Digital teams with some of the most prominent brands across multiple industries; including McLaren Automotive, Unilever, Danone Nutricia and Johnson &amp; Johnson. During these 7 years, Kristina developed client relations team from 2 people to over 25 while growing her personal portfolio of business to over $3m USD. Kristina has a keen interest in languages and cultures, reflective in her learning of Arabic since she has been in the region with PSI Middle East for three years. Her MBTI type is ESFJ.</p>'),
(17, 17, 'en', 'Suzanne Hart - Planning and Project Support Manager', 'Dip Human Resources & Dip Management', '<p>Suzanne has over 30 years international operational and business management experience, primarily in the Education and Hospitality sectors before coming to Dubai in 2014. Her strengths lie in her organisation skills and her ability to build client and staff relationships. Suzanne&rsquo;s role is often referred to as \"mission control&rdquo; in PSI Middle East! She oversees all project set ups and manages the ongoing logistics and co-ordination for all consulting projects ensuring that people and supporting resources are in place and we are able to deliver our services in line with our Values and Client Commitments.</p>'),
(18, 18, 'en', 'Joe Bruce - Senior Occupational Psychologist', 'MSc, BSc (Hons) Psychology', '<p>Joe is an Occupational Psychologist specialising in leadership assessment, development, training and team facilitation. He has a wide range of experience with different tools and methodologies. Having now lived and worked in the UAE for seven years, Joe has developed a good cultural understanding of the region, working with clients in Saudi Arabia, Qatar, Oman and Kuwait.</p>\r\n<p>He joined Innovative HR Solutions in 2009 as a Consultant Psychologist and by 2012 he had been appointed Manager of the Psychometric Tools Library having built experience and expertise as a trainer and practitioner across a number of world-leading psychometric tools.</p>\r\n<p>In 2014-2015 Joe spent a year working with Hay Group Middle East, where he gained experience of delivering a range of leadership development workshops as well as an opportunity to broaden his more generalist HR capabilities working on an organisation restructure. He has also built extensive experience in conducting behavioural observational assessment interviews at leadership level.</p>\r\n<p>Over the last few years Joe has designed and delivered a range of workshops using the MBTI, EQi and other tools to deliver effective learning to a range of audiences and for purposes including team building, leadership effectiveness and career enhancement. His MBTI is ENTP.</p>'),
(19, 19, 'en', 'Ayman Nadim - Senior Consultant', 'BA Business Administration, Executive Dip Management, Professional Dip HRM', '<p>Ayman is bilingual English and Arabic and has enjoyed a 20 year career in senior Human Resources roles before choosing to specialise with us in the talent sphere. He is a trained Assessor and leads on a number of Assessment and Development Centres, he is also an accomplished trainer and has recently completed a substantial Government project to train all Human Resources professional across multiple Government Agencies on the power and use of their newly launched Behavioural Competency Framework.</p>\r\n<p>Prior to Innovative HR Solutions he has enjoyed diversified experience across Oil and Gas, Retail, Manufacturing, Construction, FMCG and Consultancy in Senior positions across the GCC and wider Middle East. Ayman began his career quickly excelling through the ranks of BP in Cairo to the Head of Administration before moving into an integral HR role in retail as Corporate Recruitment Manager of Carrefour Egypt. He then worked with PwC before joining as a HR Director for Al Jaber Group 2006-2014, a family owned business with 30 subsidiaries and over 68,000 employees.</p>'),
(20, 20, 'en', 'Aref Al Mubarak - Senior Executive Coach', 'MBA (Human resources), BSc (Sociology)', '<p>Aref is a seasoned Senior Executive Coach and Team Facilitator who has enjoyed a long and varied career across many different verticals before choosing coaching and learning and development as a career. Aref actually began his professional life in a technical operations capacity in Quality Assurance and then as an Aircraft Senior Production Planner before he joined the Training and Assessment Development Centre team with Saudi Airlines, were he was from 1982 &ndash; 2009. As a member of the Training and Assessment Team he was involved in conducting Assessment Centres, Training Needs Analysis, creating IDPs and Coaching. It was then here that he decided to pursue his love for personal and professional development of both himself and others by leaving to become a full time Senior-Level Executive Coach. Aref holds an Organisation Developer Certificate (ODC), is an International Certified Trainer (HRDA) and PCC Certified Executive Coach.</p>'),
(21, 4, 'ar', 'أماندا وايت -  المدير العام ', 'ماجستير إدارة الأعمال , بكلوريوس (مرتبة الشرف)  في التعليم -علم النفس و علم الاجتماع', '<p class=\"right\">اماندا هي المدير العام لانوڤيتڤ اتش آر سلوشنز. تعشق أماندا تصميم مراكز التقييم والتطوير القيادي، التغيير المؤسسي ـ الثقافي، التعاقب الوظيفي وتخطيط ادارة المواهب. اخذتها مهماتها الشخصية للعمل في مختلف دول الخليج العربي وفي قطاعات عديدة مثل القطاع الحكومي، النفط والغاز، الهندسة، السياحة والزراعة. قبل القدوم الى منطقة الخليج اسست اماندا وادارت شركة استشارات في المملكة المتحدة ومدن اوربية اخرى وقامت بتقديم خدماتها بالدرجة الاولى لاستثمارات بنك سيتي وقطاع الخدمات المهنية لمدة ١٢ سنة، قبل ان يتم الاستحواذ عليها من قبل شركة آر آر دونلي اند سنز المدرجة في نيويورك&nbsp;</p>\r\n<p class=\"right\">اماندا اليوم ضيف متكرر في راديو عين دبي وتقوم باعارة خبراتها في مجال الموارد البشرية للشؤون العالمية اليومية. قامت فيما سبق بتسنم مناصب في الهيئة التنفيذية للتوظيف في المملكة المتحدة بالاضافة الى كونها عضو في الهيئة الاستشارية لبنك اينقلاند ستي للفترة من ٢٠٠١ &ndash; ٢٠٠٨.</p>\r\n<p class=\"right\">MBTI: ENTP</p>'),
(22, 7, 'ar', 'شيرن قوهل -  مدير الخدمات الاستشارية ', 'ماجستير علم نفس الأعمال، بكالوريوس، اخصائية علم نفس مهني معتمده ', '<p class=\"right\">&nbsp;تقود شيرن قوهل&nbsp;فريق الاستشارات ومكتبة اختبارات القياس النفسي في انوڤيتڤ اتش آر سولوشنز، وتدير مجموعة واسعة من مبادارات التقييم والتطوير للعديد من العملاء والصناعات. تقع خبراتها الاساسية في تطبيق علم النفس المهني في مراكز التقييم والتطوير، التوجيه التنفيذي، والتطوير المؤسسي مثل مبادرات التغيير وتحليل بيئة المؤسسة وادارة الاداء&nbsp;</p>\r\n<p class=\"right\">عملت شيرن&nbsp;سابقاً في المملكة المتحدة في قطاعات الاتصالات والتصنيع كاخصائية علم نفس مهني . كما انها عملت لحكومة المملكة المتحدة حيث ركزت على تقييم وتطوير الافراد والفرق والتطوير المؤسسي. وفي عملها هذا قامت شيرن&nbsp;باجراء مشاريع بحوث مكثفة حول ثقافة المؤسسة والأختلاط في بيئة العمل قبل ان تقوم بتطوير اداة تقييم عبر الانترنت</p>\r\n<p class=\"right\">شيرن&nbsp;حاصلة على شهادة الزمالة من جمعية علم النفس البريطانية، و هي مرخصة للمارسة مع مجلس المهن الصحية، بالاضافة الى كونها عضو في مجلس علم النفس للتوجية ، عضو في &nbsp;قسم علم النفس المهني، عضو في مركز الاختبارات السيكولوجي&nbsp;</p>\r\n<p class=\"right\">MBTI :ENTP</p>'),
(23, 14, 'ar', 'آبي وايت -  اختصاصي أول - التطوير والتعليم ', 'بكالوريوس (مع مرتبة الشرف) في إدارة الأعمال ', '<p>تعمل آبي كاختصاصي أول للتعليم والتطوير ومدربة ممارسة ذات خبرة طويلة. تتمتع آبي بخبرة كبيرة في مجال تصميم وتقديم دورات التطوير القيادية، مراكز التقييم والتطوير ووضع حلول الموارد البشرية. تقوم آبي بقيادة أنشطة التطوير بالشركة من خلال دورها كمصمم برامج، وتقديم برامج تدريب المدربين من أجل ضمان التناسق بين مدربينا الذين يتحدثون العربية والانكليزية، بالإضافة إلى القيام بمهام تقييم الجودة.</p>\r\n<p>آبي هي مدرب معتمد من قبل اتحاد التدريب العالمي &nbsp;مصمم مراكز، تقييم معتمد، مدير ومقيِّم، مدرب مرخص من قبل ستيفن كوفي، مدرب معتمد من قبل برنامج قيادة الذكاء العاطفي ومدرب معتمد لمؤشر MBTI.</p>\r\n<p>قامت آبي بتقديم جلسات متخصصة ضمن العديد من البرامج التي تشمل: برنامج \"نُظم&nbsp;القيادة\" (لمدة خمس أيام)، مهارات التدريب لكبار المدراء، الذكاء العاطفي، فرق العمل، التعاون، قيادة التغيير، قيادة فرق الأداء المتقدم، والتواصل.</p>\r\n<p>عملت آبي سابقاً في مجالات&nbsp;متعددة مثل مجموعة جميرا للفنادق ودبي القابضة (استثمارات تيكوم). تضمن دورها كمدير للأداء والمواهب والتدريب في تيكوم للاستثمارات و تصميم إطار عمل التعاقب الوظيفي وإدارة مراكز التطوير ضمن المؤسسة وفريق القيادة.</p>\r\n<p>&nbsp;MBTI : INTP.</p>'),
(24, 5, 'ar', 'حميراء أنور -  اختصاصي أول - علم نفس مهني ', 'ماجستير علم نفس مهني، ماجستير في إدارة الموارد البشرية (CIPD)، بكالوريوس علم النفس، اختصاصي علم نفس مهني مجاز', '<p>حميراء هي اختصاصية علم نفس مهني ذات خبرة عالية وهي شغوفة بتطوير أطر الكفاءات، تطوير اختبارات علم النفس المخصصة، برامج التقييم والتطوير القيادي، وتصميم وإدارة برامج أداة ٣٦٠. عملت حميراء في مختلف أدوار استشارات الموارد البشرية في عدة مجالات من ضمنها شرطة ميدلاندس الغربية في المملكة المتحدة، استشارات اكسنتشر، ووزارة الدفاع. قادت حميراء مشروعاً لخدمات الدفاع الطبية في وزارة الدفاع لملائمة إطار القيادة في الوزارة مع إطار القيادة في إن أتش إس (خدمة الصحة الوطنية) ووفرت حلول التدريب من أجل سد فجوة الكفاءات.</p>\r\n<p>تحمل حميراء ماجستير في علم نفس المهني، ماجستير إدارة الموارد البشرية، بكالوريوس علم النفس، وهي عضوة مسجلة في جمعية علم النفس البريطانية، وعضو مسجل مع مجلس المختصين بالصحة، موجه&nbsp;مهني متدرب في مجال خدمات الإشراف الوظيفي. كما أن حميراء تعمل كعضو منتسب في جمعية علم النفس البريطانية وعضو في الجمعية العالمية للتوجيه في&nbsp;علم النفس.</p>\r\n<p>حميراء حاصلة على ترخيص المستوى (أ) و (ب) للاختبارات المهنية، بالإضافة إلى تدريب الموجه&nbsp;الوظيفي، متدرب في مجال خدمات الإشراف الوظيفي، ومقيم ومدير مركز خدمات الاستشارات.مؤشر&nbsp;</p>\r\n<p>MBTI : ESFJ</p>'),
(25, 6, 'ar', 'جوناثان رووك -  اختصاصي أول - علم نفس مهني', 'ماجستير في علم النفس التطبيقي، دبلوم في علم النفس المهني، اختصاصي علم نفس مهني مجاز ', '<p>يعمل جوناثان كاختصاصي علم نفس مجاز وهو يتمتع بخبرة واسعة في مجال استشارات الموارد البشرية في القطاعين العام والخاص، بما في ذلك الحكومة المركزية والخدمات المالية والتسويق والصيدلة والاتصالات. إضافة إلى خبرته الواسعة في دول الخليج، عمل جوناثان في المملكة المتحدة والبرتغال وسويسرا والهند.</p>\r\n<p>جوناثان شغوف&nbsp;بتطوير أدوات القياس النفسي وتحليل المواهب والتغيير المؤسسي واستشارات الموارد البشرية مع خبرية واسعة في مجال التقييم والتوظيف، والتطوير القيادي والتقييم التنفيذي والتحليل الوظيفي وتطوير الكفاءات والإرشاد الوظيفي والتوجيه.</p>\r\n<p>جوناثان هو زميل مشارك في الجمعية البريطانية لعلم النفس، عضو منتظم&nbsp;في قسم علم النفس المهني، يحمل شهادة التوجيه&nbsp;من مركز التوجيه، عضو في الجمعية العالمية للتوجيه&nbsp;الفسي، واختصاصي علم نفس مسجل مع مجلس المهن الصحية.</p>\r\n<p>تشمل منشورات جوناثان التالي:</p>\r\n<p>&lsquo;Enabling Organisational Culture Change within Aviva: Case Study&rsquo;, British Psychological Society 2010, Division of Occupational Psychology Annual Conference; Zijlstra, F.R.H. &amp; Rook. J.W. (2005) &lsquo;The Weekly Cycle of Work and Rest&rsquo;; R.A. Roe, M.J. Waller and S. Clegg (Eds.) &lsquo;Time in organizations&rsquo; &ndash; Approaches and methods, London: Routledge; Rook, J.W. &amp; Zijlstra, F.R.H. &lsquo;The contribution of various types of activities to recovery&rsquo;, European Journal of Work and Organisational Psychology (2005), Vol. 15 (2); &lsquo;Defining Moments&rsquo;, The Psychologist (2002), Vol.15 (11), Letter to the Editor; &lsquo;Stress &amp; Recovery: A review of the literature&rsquo;, IPCD (2015); and most recently &lsquo;The Cost of Organizational Stress&rsquo;, CEO Magazine, June 2016, Opinion piece.</p>'),
(26, 9, 'ar', 'أبرار المبارك -  مستشار ', 'ماجستير الاتصال الجماهيري وعلم النفس ', '<p>أبرار سعودية الجنسية تتحدث اللغتين العربية والانجليزية. وهي موجه معتمد من قبل اتحاد التدريب العالمي ومقيِّم مُدرَّب ومدير مراكز، كما تشارك في كلٍ من برامجنا باللغة العربية والانجليزية. تلعب أبرار اليوم دوراً اساسياً في تطوير أدواتنا وخدماتنا باللغة العربية، وفي إدارة المشاريع في المملكة العربية السعودية. قامت بالعمل على العديد من ترجمات أدوات القياس النفسي والتمارين باللغة العربية، وحالياً تقود عملية وضع أداة تطوير وظيفي أساسية في الحكومة السعودية.</p>\r\n<p>تحمل أبرار شهادة البكالوريوس في الاتصال الجماهيري والعلوم السياسية مع الاختصاص في علم النفس، وقد حصلت على الشهادة عالمياً من ثلاثة جامعات في ثلاث دول ضمن برنامج صمم لتطوير و نشر المنظور عالمي. كانت سنتها الأولى في بانقالور في الهند (سنة واحدة) ثم في كرايست جيرج في نيوزيلندا (سنتين) قبل انهاء البرنامج في الولايات المتحدة في بتسبرغ.</p>\r\n<p>بدأت أبرار مشوارها الوظيفي كمراسلة صحفية مع جريدة جازيت السعودية بينما كانت تمثل بلدها كسفير ووسيط بين الجامعة والملحقية&nbsp;الثقافية للمملكة السعودية العربية. عملت أبرار بعد ذلك في خدمات تدريب اللغة الانجليزية لغير الناطقين بها.</p>\r\n<p>&nbsp;MBTI : ENTP.</p>'),
(27, 20, 'ar', 'عارف المبارك -  موجه تنفيذي أول ', 'ماجستير إدارة الأعمال (في الموارد البشرية)، بكالوريوس (علم الاجتماع) ', '<p>يعمل عارف كموجه&nbsp;تنفيذي أول ومدرب فرق. يتمتع عارف بمشوار وظيفي طويل ومنوَّع في مختلف المستويات قبل اختيار التوجيه&nbsp;التنفيذي والتعلم والتطوير كمسار وظيفي. بدأ عارف مشواره المهني في مجال العمليات التقنية بضمان الجودة، ثم عمل بعد ذلك كمنظم أول لمنتجات الطيران قبل أن ينضم إلى فريق التدريب ومراكز التقييم في الطيران السعودي، حيث عمل بها للفترة بين ١٩٨٢ وحتى ٢٠٠٩. وكعضو في فريق التدريب والتقييم شارك عارف في اجراء مراكز التقييم، تحليل الاحتياجات التدريبية، تصميم خطط التدريب الشخصية والتوجيه. وفي ذلك الوقت قرر أن يتجاوب مع شغفه بالتطوير الذاتي وتطوير الآخرين وأصبح مدرب تنفيذي أول بدوام كامل.</p>\r\n<p>يحمل عارف شهادة مطور مؤسسي (ODC) وهو مدرب عالمي معتمد (HRDA) وموجه&nbsp;تنفيذي معتمد من قبل (PCC).</p>'),
(28, 8, 'ar', 'ليزل دو پليسيه - اختصاصي أول - علم النفس المهني ', 'ماجستير تجارة (علم النفس الصناعي)، بكالوريوس (مع مرتبة الشرف)، اختصاصي علم نفس صناعي مسجل', '<p>تعمل ليزل كاختصاصي أول بعلم النفس المهني. تتمتع ليزل بالثقة والخبرة الواسعة في مجال الموارد البشرية والتي اكتسبتها من خلال سنوات من العمل في الخليج وجنوب افريقيا. قامت ليزل بعدة مهام حتى الآن، ومن أبرزها قيادة عدد من برامج التقييم والتطوير الكبيرة بنجاح في قطاعات الحكومة , النفط والغاز والبناء.</p>\r\n<p>قامت ليزل سابقاً بإدارة عمليات الموارد البشرية عالمياً وعملت كمستشار في مجالات علم النفس المهني، إدارة الموارد البشرية، ونظم معلومات الموارد البشرية. كما عملت في مجالات متعددة من ضمنها: اكتساب المواهب، إدارة منافع الموظفين، إدارة التغيير، تطوير المواهب، التعاقب الوظيفي، التدريب والتوجيه، بالإضافة إلى إدارة الأداء. إن ليزل اختصاصي علم نفس مهني مسجل ومرخص في التوجيه المتخصص في محاكاة العقل و الجهاز العصبي.</p>\r\n<p>&nbsp;MBTI: ENFJ.</p>'),
(29, 18, 'ar', 'جو بروس -  اختصاصي أول - علم النفس المهني ', 'ماجستير، بكالوريوس (مع مرتبة الشرف) في علم النفس ', '<p>إن جو هو اختصاصي علم نفس مهني في التقييم القيادي والتطوير وتدريب الفرق. يتمتع جو بخبرة واسعة في تطبيق العديد من الأدوات والمنهجيات. استقر جو في دول الخليج لأكثر من سبع سنين وعمل خلالها على تطوير فهماً جيداً لثقافة المنطقة من خلال عمله مع مختلف العملاء في السعودية وقطر وعمان والكويت.</p>\r\n<p>انضم جو لشركة انوڤاتيڤ إتش آر سوليوشنز في عام ٢٠٠٩ كمستشار علم نفس وفي عام ٢٠١٢ تم تعيينه كمدير لمكتبة أدوات القياس النفسي حيث قام بتطوير الخبرات والمهارات كمدرب وممارس للعديد من أدوات القياس النفسي.</p>\r\n<p>في عام ٢٠١٤ &ndash; ٢٠١٥ عمل جو لمدة سنة مع هاي جروب الشرق الاوسط، حيث اكتسب الخبرة في تقديم العديد من دورات التطوير القيادي بالإضافة إلى فرصة توسيع دوره كاختصاصي عام في الموارد البشرية والعمل على إعادة بناء الهيكل المؤسسي. كما اكتسب خبرات واسعة في اجراء مقابلات المراقبة السلوكية على مستوى القيادة.</p>\r\n<p>على مدى السنوات الماضية، قام جو بتصميم وتقديم العديد من الدورات باستخدام أدوات MBTI وEQI وغيرها من أجل تقديم أنشطة التدريب الفعالة للمشاركين ولأغراض أخرى تشمل بناء الفرق والقيادة الفعالة والتحسين الوظيفي.</p>'),
(31, 17, 'ar', 'سوزان هارت -  مدير التخطيط ودعم المشاريع ', 'دبلوم في إدارة الموارد البشرية ودبلوم في الإدارة ', '<p>تتمتع سوزان بأكثر من ٣٠ سنة من الخبرة في إدارة الأعمال والعمليات العالمية، وبالأخص في قطاعات التعليم والفندقة قبل أن تستقر بدبي في عام ٢٠١٤. تكمن نقاط قوتها في خبراتها التنظيمية وقدرتها على إنشاء العلاقات مع العملاء والموظفين.</p>\r\n<p>يُشار عادة إلى دور سوزان في الشركة على أنه دور \"مركز التحكم\"، حيث تقوم سوزان بالإشراف على تخطيط جميع المشاريع وإدارة الأمور اللوجستية بجانب تنسيق جميع مشاريع الاستشارات والتأكد من توفر الأفراد والموارد المساعدة، لضمان تقديم خدماتنا في ضوء القيم والالتزام الذي تعتمده انيوفيتف اتش ار سلوشنز تجاه العملاء.</p>'),
(32, 16, 'ar', 'كريستينا بيگز -  مدير تطوير الأعمال ', 'بكالوريوس (مع مرتبة الشرف) في دراسات اللغات الأوربية والإدارة ', '<p>تقود كريستينا عمليات تطوير الأعمال من أجل تحسين تجربة العملاء لخدماتنا، وبناء حلول المواهب بالاستعانة بالمعلومات من فريق الاستشارات. تحب كريستينا العمل في مجال خدمة العملاء وتقوم عادة بإعداد العروض الفنية والتقديمية ومقابلة العملاء لضمان حصولهم على الحلول المناسبة بجانب رضاهم بالنتائج.</p>\r\n<p>بدأت كريستينا عملها في مجال العلاقات العامة والتسويق وفرق البيانات في المملكة المتحدة في منظمة بيغ داتا، حيث عملت مع أشهر العلامات في العديد من الصناعات مثل سيارات مكلارين، يونيليفر، دانون نوتريشيا، وجونسن أند جونسن. خلال هذه السنوات السبع، قامت كريستينا بإنشاء فريق لعلاقات العملاء يتكون من ٢٥ شخصاً بجانب تطوير قيمة المشاريع تحت قيادتها إلى أكثر من ٣ ملايين دولار. تهتم كريستينا كثيراً باللغات والثقافات، وقد بدأت بتعلم اللغة العربية منذ قدومها إلى المنطقة للعمل مع شركة إينوفاتيف إتش آر سوليوشنز منذ ثلاثة سنوات.</p>\r\n<p>MBTI : ESFJ.</p>'),
(33, 10, 'ar', 'سام داي -  مدير برامج الاعتماد ', 'دبلوم في اللغة الانجليزية والآداب ', '<p>تتمتع سام بخبرة واسعة في مجال الإدارة اللوجستية والإدارة العامة وخدمة العملاء، وقد ساهمت سام بخبرتها ومهاراتها بإدارة المشاريع في تطوير مكتبة اختبارات القياس النفسي لشركة انوڤاتيڤ إتش آر سوليوشنز.</p>\r\n<p>تمتاز سام بالالتزام والابداع والانتباه للتفاصيل وشغفها بإيجاد حلول مُرضية لجميع الأطراف بما في ذلك العملاء والعمل.</p>\r\n<p>وفي دورها كمدير لبرامج الاعتماد، تقوم بالتنسيق مع ناشري الاختبارات العالميين وعملائنا لضمان تقديم أحدث المعلومات حول برامج الاعتماد ومتطلبات التأهيل والدعم والتشجيع. تتضمن مسؤولياتها التخطيط لمسار الدورات في دول الخليج العربي، تطوير المناهج، بالإضافة إلى عمليات الاعتماد وتسجيل المشاركين. كما تقوم سام بتقديم خدمات قيِّمة للمدير التنفيذي ومدير العمليات من خلال الإشراف على متطلبات العمليات وإدارة طلبات العملاء عند الحاجة في مختلف دول الخليج.</p>\r\n<p>&nbsp;MBTI : ENTJ.&nbsp;</p>'),
(34, 11, 'ar', 'جوانا رودريجز - منسق دعم المشاريع ', 'ماجستير إدارة الأعمال (التسويق والموارد البشرية)، بكالوريوس ', '<p>يتضمن دور جوانا كمنسق دعم المشاريع القيام بتنسيق جميع مشاريع الموارد البشرية ومراكز التقييم والتطوير. وكذلك معاونة مدير دعم وتخطيط المشاريع بحجز الموارد والقاعات والمواد، التأكد من كتابة التقارير الشخصية وتحضيرها في الوقت اللازم، وتدوين جلسات التدريب. انضمت جوانا إلى الشركة في عام ٢٠١٢ وقد أحدثت فرقاً كبيراً خلال تلك الفترة من خلال عملها على ضمان توفر جميع الأفراد في المكان المناسب والوقت المناسب.</p>\r\n<p>عملت جوانا سابقاً في قطاع التسويق في دبي وفي أكاديمية كومتك للكومبيوتر في الهند. إن نمطها في مؤشر</p>\r\n<p>MBTI : ESTJ.</p>'),
(35, 12, 'ar', 'بوشبا ماني -  مديرة مكتبة أدوات القياس النفسي ', 'بكالوريوس في التجارة ', '<p>تعمل بوشبا كمديرة مكتبة أدوات القياس النفسي، وقد قامت بدور أساسي في تطوير منتجات مكتبة الاختبارات خلال خمسة عشر عاماً من العمل بشركة انوڤاتيڤ إتش آر سوليوشنز.</p>\r\n<p>تشرف بوشبا على فريق الإداريين بمكتبة أدوات القياس النفسي وتقدم الدعم للعملاء عند اختيار الأدوات والأنشطة المناسبة والمجموعات المعيارية ودورات الاعتماد لدعم برامج المواهب الخاصة بهم. كما تقوم بالتنسيق مع ناشري الأبحاث العالميين وتقوم بإدارة أكثر من ١٠ منصات عبر الانترنت. تعتبر بوشبا مصدر دعم كبير&nbsp;للكثير من العملاء.</p>\r\n<p>MBTI : ESTJ.</p>'),
(36, 13, 'ar', 'كاثرن ريا -  منسق المكتبة ', 'دبلوم عالي في الإدارة (التسويق وإدارة الفعاليات) ', '<p>تتمتع كاثرين بخبرات واسعة في مجال إدارة المشاريع وإشراك المساهمين في مجال الموارد البشرية. تقوم كاثرين بإدارة ودعم العديد من عمليات الموارد البشرية والتقييم عبر تزويد أدوات القياس النفسي واختيار التمارين والإدارة.</p>\r\n<p>كما تعمل كمدرب خبير للموظفين الجدد، وتقوم بتخطيط برامج التدريب للموظفين الجدد والعملاء عند شراء المنصات الجديدة لأول مرة. تقوم كاثرين بالتنسيق المستمر مع العملاء والمشاركين لضمان توفير متطلبات الاختبارات.</p>\r\n<p>MBTI : ISTJ.</p>'),
(37, 19, 'ar', 'أيمن نديم -  مستشار أول', 'بكالوريوس إدارة الأعمال، دبلوم تنفيذي في الإدارة، دبلوم مهني في إدارة الموارد البشرية ', '<p>يعمل أيمن كمستشار أول ويتحدث اللغتين العربية والانجليزية، حصل أيمن على خبرة واسعة تتجاوز العشرين عاماً في أدوار الموارد البشرية قبل اختياره العمل معنا في نطاق المواهب. إن أيمن مقيِّم مدرَّب وقام بقيادة العديد من مراكز التطوير والتقييم، كما أنه مدرب محترف وقد قام مؤخراً بإكمال مشروع مهم لتدريب جميع مختصي الموارد البشرية في الحكومة في مختلف المؤسسات حول قوة واستخدام إطار عمل الكفاءات السلوكية.</p>\r\n<p>قبل الانضمام إلى شركة انوڤاتيڤ إتش آر سوليوشنز، تولى أيمن عدة مناصب عليا في العديد من القطاعات مثل النفط والغاز,التسويق والتصنيع والبناء,المواد الاستهلاكية والاستشارات بدول الخليج والشرق الأوسط.</p>\r\n<p>تطور أيمن بمشواره الوظيفي بشكل سريع، حيث بدأ كموظف مبتدئ في القاهرة حتى أصبح مدير القسم الإداري وذلك قبل انتقاله إلى العمل بمجال الموارد البشرية كمدير للتوظيف المؤسسي في شركة&nbsp;كارفور بمصر. بعد ذلك عمل أيمن مع پي دبليو سي قبل أن يصبح مدير عام الموارد البشرية لمجموعة الجابر بين ٢٠٠٦ &ndash; ٢٠١٤، وهي مجموعة أعمال مملوكة عائلياً وتمتلك ٣٠ شركة وأكثر من ٦٨ ألف موظف.</p>'),
(38, 21, 'en', 'Simon Boag - Senior Business Psychologist', 'MSc Occupational Psychology,  BSc (Hons) Psychology	', '<p>Simon is a Chartered Psychologist with over fifteen years consultancy experience at an international level (Europe, Middle East, Asia Pacific) with an excellent track record of working with diverse client groups using a strong client-focused approach. Simon has designed and delivered bespoke training and development programmes, providing high-impact solutions to international blue chip and public sector organisations and experienced in large scale bid processes, including PFI, and PPP.&nbsp;</p>\r\n<p>Before joining the team at PSI Middle East, formerly Innovative HR Solutions, Simon held several Organizational Development roles for Technip FMC (2012 to 2017) including L&amp;D Manager for the Middle East Region, and PMO for the HR Integration in APAC Region. Prior to those assignments he was responsible to implement Graduate and Management Development programmes at NPCC, and was a lead assessor for the GSEC leadership development programme. A specialist in selection, assessment, coaching, leadership and talent management using inventories published by Saville, SHL, Hogan, Psytech and AQR.</p>\r\n<p>Graduated with a Bachelor&lsquo;s degree in Psychology from the University of Hull and a Master&rsquo;s degree in Occupational Psychology from the internationally acclaimed Institute of Work Psychology, University of Sheffield. A member of the British Psychological Society (BPS), Division of Occupational Psychology, special group in Coaching Psychology, and accredited to BPS level A and B standard in psychometric testing.&nbsp;</p>'),
(39, 22, 'en', 'Simon Tither - Organisational Psychologist', 'MSc in Applied Psychology, BSc in Psychology ', '<p>Simon is originally from New Zealand but brings a global perspective to&nbsp; PSI Middle East, formerly Innovative HR Solutions, having previously lived in the Gulf and spent time working in Europe. He holds a Masters Degree in Applied Psychology (Industrial and Organisational) as well as a Bachelors Degree majoring in Psychology and Geography. His Master&rsquo;s research focused on the Employee Value Proposition put forward by organisations and the effect of this EVP on organizational outcomes such as job satisfaction and employee motivation.</p>\r\n<p>A background in the social sciences and organizational psychology, he has experience primarily working in the coordination of centres and talent assessment as well as facilitation and support for workshops, trend analysis and assessment design. Simon has experience in administration, interpretation and feedback of psychometric tools, and is a qualified test user for tools such as but not limited to the full Saville suite and Psytech GeneSys suite.&nbsp;</p>'),
(40, 23, 'en', 'Tracy Stodart - Director of Psychometric Test Library ', ' Bachelor of Arts (Psychology) & Masters in Business Administration ', '<p>Tracy joined PSI Middle East, formerly Innovative HR Solutions in 2003 and is both a Senior Psychologist and Director of our Psychometric Test Library. In this role she builds relationships with our Test Publishers, providing specialised and tailored solutions to meet the assessment and development needs of the Middle East region. Tracy is presently heavily involved in translating many of our most valid and reliable tools into Arabic and helping create local norm groups ensuring the best tools are available for our client base.&nbsp;&nbsp;</p>\r\n<p>Tracy came to the UAE in 2001 where she was appointed HR Manager for Presidential Flight based in Abu Dhabi.&nbsp; This role entailed the provision of specialist advice regarding a wide range of HR issues, with a key focus on assessment, performance management training and development.&nbsp; Tracy has held HR management roles in the banking and aviation industry and she has considerable multi-cultural experience, having lived and worked in New Zealand, Hong Kong, Dubai and Papua New Guinea.&nbsp; Whilst working with United Nations, she travelled extensively to various UN outstations to deliver customised training solutions.&nbsp; When in New Zealand, Tracy worked&nbsp; for SHL as part of its management team where she gained comprehensive experience in the management, implementation and validation of assessment tools. One of her responsibilities was conducting Occupational Testing Qualification Workshops on behalf of SHL.&nbsp;</p>\r\n<p>Tracy&nbsp; places a great deal of emphasis on the practical application of psychology in the world of work. Her approach incorporates a focus on cross cultural&nbsp; integration of individual\'s and teams within the workplace to achieve strong business results using the best tools on the market.</p>'),
(41, 24, 'en', 'Louise Kidd - Senior Organisational Psychologist ', 'MSc in Organisational Psychology & BA in Psychology and Education', '<p>An Organisational Psychologist by trade, Louise has 18 years experience in the field of psychology, from teaching to working with public and private sector organisations on a multitude of projects.&nbsp; In addition to working with clients on their leadership development programmes, Louise is an assessor and a coach to delegates on our British Psychological Society Qualification in Occupational Testing &ndash; Ability and Personality programme.&nbsp;</p>\r\n<p>A true psychometrician, Louise&rsquo;s expertise lies within evaluating and selecting appropriate tests for projects, administering and scoring assessments, interpreting these results and feeding sometimes difficult information back to Senior Executives, Board Executives and CEO&rsquo;s. Louise has extensive experience conducting assessment and development centres with clients ranging from IT &amp; Telecoms, Government Departments and Multinationals.</p>\r\n<p>Louise has vast experience in conducting interviews, whether competency based for recruitment purposes, or senior interviews to establish project requirements or to identify needs within the business. Louise is a skilled report writer and highly competent at feeding identified information back to participants. (<strong>BPS </strong><strong>RQTU&nbsp; Registration Number 424144)&nbsp;</strong></p>'),
(42, 25, 'en', 'Inas Farid- Senior Leadership Development Specialist', 'MBA in Leadership & B.A (Hons) English ', '<p>Inas has more than 20 years&rsquo; experience and been a senior management consultant since 2009.&nbsp; Her assignments include working extensively as an Assessor on Assessment and Development centres, conducting employee interviews and psychometric assessments for public and private sector clients including Dubai Roads and Transport Authority, Dubai Prime Ministers&rsquo; Office, Abu Dhabi Centre of Excellence, UAE Ministry of Health, Al Ain Municipality, Executive Directors of the Abu Dhabi government, Pfizer, MBC, Jaguar&amp; Land Cruiser, Dubai World Central.&nbsp; More recently Inas was involved in one of our largest projects to date with Al Jazeera where she was part of team who assessed in both English and Arabic and provided feedback to over 300 people.</p>\r\n<p>Inas commenced her career in the UAE in recruitment and talent management with Emirates Airline from 1997 &ndash; 2004 a prestigious role during a period of substantial growth and change for the airline before being approached to become HR Manager for Nakeel.&nbsp; Here her role again was managing the core functions of the company&rsquo;s significant recruitment drive during a period of unprecedented growth.&nbsp; This included oversight of manpower planning, running assessment centres, taking a lead in Employer Branding and establishing HR policy.</p>'),
(43, 26, 'en', 'Waleed Mowafy- HR Talent Management Specialist ', 'BA Economic Sciences ', '<p>Waleed has been a senior consultant since March 2009 where he has been working predominately in leadership development identifying areas of strength and areas for development and providing coaching and mentoring.&nbsp; He has also been instrumental in designing Personal Development Plans, designing training programmes and managing development and assessment centers.</p>\r\n<p>Prior to working with Innovative HR Solutions, Waleed worked with Mashreq Bank in the UAE for 4 years in a senior operations role managing a large team across 4 functional areas. It was here he developed a passion for developing talent and managing people.&nbsp;&nbsp;&nbsp;</p>\r\n<p>Waleed is originally from Egypt where he enjoyed a prestigious career with the Hilton, the leading hospitality group and the American Educational Services (7 years) where he taught languages (Arabic and English).</p>'),
(44, 27, 'en', 'Madeleine Anne Yorke Harling - L&D Co-ordinator', 'BSc in Psychology', '<p>Madeleine Anne Yorke Harling has joined the team as an Organisational Psychologist in training. Currently studying for her Masters, with a BSc in Psychology from the University of Kent, Madeleine has been managing assessment and development centres, coaching projects and recently assessing.</p>\r\n<p>Madeleine is certified in BPS Assessor Skills, BPS Centre Manager, BPS Qualification in Occupational Psychology- Level A and P as well as Strong Interest Inventory.&nbsp;</p>\r\n<p>Madeleine\'s background is a mix of her British heritage while growing up in Saudi Arabia, Singapore, England and Dubai which has enabled her to respond well&nbsp;to our diverse range of clients in the region. Madeleine can often be found liaising with and scheduling candidates for large centres as well as facilitating and assessing graduate level candidates.</p>'),
(45, 28, 'en', 'Majid Sharif - Finance Director', 'CPA, MBA (Finance), B.Com (Accounting)', '<p style=\"margin: 0in; margin-bottom: .0001pt;\"><span style=\"font-size: 12.0pt; color: #404040;\">Majid is a Certified Public Accountant with career span of more than 17 years in accountancy. Majid has acquired wide range of experience in financial audits, business advisories, accountancy, finance and management. In the capacity as Head of finance of Innovative HR Solutions, he is responsible for all finance and administrative functions while ensuring the effective internal control system. Prior joining IHS team, he served IHS as an external audit partner for more than 8 years.</span></p>\r\n<p style=\"margin: 0in; margin-bottom: .0001pt; min-height: 20.3px;\"><span style=\"font-size: 12.0pt; color: #404040;\">&nbsp;</span></p>\r\n<p style=\"margin: 0in; margin-bottom: .0001pt;\"><span style=\"font-size: 12.0pt; color: #404040;\">Majid holds MBA degree in Finance and is a Member of Institute of Public Accountants of Australia (MIPA), Associate of Institute of Financial Accountants of UK (AFA) and Associate of Institute of Cost Accountants of India (ACMA). Majid has expertise in International Financial Reporting Standards (IFRS), International Standards on Auditing (ISA), Company Laws, Tax Laws and Anti Money Laundering Laws. His special interests include financial modeling, reporting, internal controls and corporate governance.</span></p>'),
(46, 29, 'en', 'Saeed Al Yaqoub- Business Development Manager', 'Bachelors in Entrepreneurship ', '<p>Born in the United States to Saudi parents, Saeed was raised in Saudi Arabia to represent the best of both his cultures, returning to the US from the age of 11 to complete his education.</p>\r\n<p>&nbsp;Saeed holds a Bachelor&rsquo;s Degree in Entrepreneurship and Business Management with a minor in Social Sciences from the University of North Texas. While completing his studies, Saeed ran the Saudi Student organization in his university. Working together with over 120 members, the Saudi Student Organization represented the Kingdom&rsquo;s culture as well as empowered the organization to become a contributing member of the International Student Associations community within the southern states; winning the <em>&lsquo;Best International Student Organization&rsquo; </em>award in the year of 2011-2012.</p>\r\n<p>While in university; Saeed had the great opportunity to work with several multinational organizations and businesses which added to his knowledge and experience in the behaviors and norms of working with successful international teams, enabling him to become a finalist in the 2015 Northwestern Mutual Integrated Business Case Competition where students apply their business knowledge and practical skills.</p>\r\n<p>Upon graduating in 2015, Saeed returned to the Kingdom of Saudi Arabia to begin work at a reputable family construction company. During that time, Saeed gained a lot of practical experience; his role ranged from coordinating HR and Finance activities, managing projects,&nbsp;setting up new business meetings with prospective clients, as has been liaising as the Client Relations manager on all existing projects.</p>\r\n<p>Saeed&rsquo;s role within PSI will be spear-heading our Saudi&nbsp;office&nbsp;and bringing our world class tools and consultancy to the country to support the Kings 2030 Vision.</p>'),
(47, 30, 'en', 'Marais Bester - Senior Organisational Psychologist, Head of Education & Research', 'MSc Organisational Psychology, BSc Hons Psychology', '<p>Marais is a Chartered and Registered Occupational Psychologist with a strong background in Talent Management gained over the Middle East and Africa over the past number of years. His personal assignments up to date include the successful delivery of multiple large-scale Talent Assessment and Talent Development projects across multiple geographies, disciplines and industries.&nbsp;</p>\r\n<p>In the past Marais has worked with large multinational clients in the areas of Talent Acquisition, Employee Benefit Management, Employee Wellbeing, Change Management, Organisational Development, Talent Development, Talent Audits, Succession Planning, Coaching and Mentoring as well as Performance Management. He is a Chartered Occupational Psychologist registered with both the British Psychological Society and the Health Professions Council of South Africa and holds a Masters Degree in Occupational Psychology.&nbsp; Marais is passionate about assisting organisations to achieve their strategic outcomes by helping their employees optimise within their careers.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_tinified` int(1) NOT NULL DEFAULT '0',
  `recursive` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `slug`, `logo`, `logo_tinified`, `recursive`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'MBTI Forum', 'mbti', 'uploads/images/events/356a192b7913b04c54574d18c28d46e6395428ab.png', 0, 1, 1, 1, 4, '2016-07-12 09:46:31', '2018-10-28 07:47:21'),
(2, 'Business Breakfast', 'professional-development-breakfastbreakfast', 'uploads/images/events/da4b9237bacccdf19c0760cab7aec4a8359010b0.png', 1, 0, 1, 1, 4, '2016-08-03 04:53:11', '2018-09-26 07:13:30'),
(5, 'IPCD', 'ipcd', 'uploads/images/events/ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4.png', 0, 0, 0, 3, 2, '2016-08-27 13:11:16', '2017-09-03 12:15:35'),
(6, 'HR Summit & Expo', 'hr-summit-and-expo', 'uploads/images/events/c1dfd96eea8cc2b62785275bca38ac261256e278.png', 0, 0, 0, 3, 2, '2016-08-30 08:57:31', '2017-09-03 12:15:13');

-- --------------------------------------------------------

--
-- Table structure for table `event_dates`
--

CREATE TABLE `event_dates` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_dates`
--

INSERT INTO `event_dates` (`id`, `event_id`, `start_date`, `end_date`) VALUES
(1, 6, '2016-11-14 06:00:00', '2016-11-16 06:00:00'),
(5, 5, '2016-10-21 05:00:00', '2016-10-22 05:00:00'),
(8, 2, '2018-03-07 20:00:00', '2018-03-07 20:00:00'),
(10, 1, '2019-07-31 20:00:00', '2019-07-31 20:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `event_date_translations`
--

CREATE TABLE `event_date_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_date_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_date_translations`
--

INSERT INTO `event_date_translations` (`id`, `event_date_id`, `locale`, `location`) VALUES
(1, 1, 'en', 'Dubai'),
(5, 5, 'en', 'Dubai'),
(11, 8, 'en', 'Dubai'),
(13, 10, 'en', 'Dubai');

-- --------------------------------------------------------

--
-- Table structure for table `event_translations`
--

CREATE TABLE `event_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_translations`
--

INSERT INTO `event_translations` (`id`, `event_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 'en', 'MBTI Forum', '<h3 style=\"text-align: justify;\">A complimentary session exclusively for MBTI Practitioners!</h3>\r\n<p style=\"text-align: justify;\">The Myers Briggs Type Indicator (MBTI) Forum was launched in April 2011 and with over 500 members the use and expertise of the MBTI instrument continues to grow in the Middle East. The Forum is exclusively for qualified MBTI Practitioners and is an opportunity to grow the global awareness of the MBTI tool and strengthen the skills and effectiveness of the practitioners who use it in the GCC region. The Forums are currently held bi-annually in Dubai and we also have a LinkedIn group, MBTI Middle East, exclusive for MBTI practitioners to join to stay up to date and informed with the latest MBTI news.</p>\r\n<p style=\"text-align: justify;\">The MBTI Forum provides practitioners with the opportunity to:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>Network with local and international practitioners</li>\r\n<li>Develop skills through sharing experiences and listening to expert speakers</li>\r\n<li>Receive exclusive MBTI Forum news</li>\r\n<li>Receive special offers on products and training</li>\r\n<li>Online discussions hosted by accredited trainers</li>\r\n<li>Become a member of the MBTI Middle East Linkedin Page</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Previous Forum topics have included:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>\'How to Run an MBTI Workshop\' Workshop</li>\r\n<li>Temperament over Type</li>\r\n<li>Managing Conflict through Type</li>\r\n<li>Using Type to improve the Innovation process</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">&nbsp;If you are a certified MBTI practitioner and would like to join the Middle East MBTI Forum Book now!</p>', '', '', ''),
(2, 1, 'ar', 'منتدى  MBTI ', '<p>جلسة مجانية خاصة لممارسي &nbsp;MBTI.</p>\r\n<p>تم إطلاق منتدى مؤشر (MBTI) في ابريل ٢٠١١ بمشاركة أكثر من ٥٠٠ عضو. حيث يتزايد بإستمرار استخدام وخبرات أداة مؤشر أنماط MBTI في الشرق الأوسط.</p>\r\n<p>هذا المنتدى خاص حصراً بالممارسين الخبراء في استخدام أداة مؤشر MBTI ويمثل فرصة لتوسيع المعرفة العالمية بالأداة، وتقوية خبرات وفعالية الممارسين المستخدمين له في منطقة الخليج العربي. يعقد المنتدى حالياً مرتين سنوياً في دبي، كما أن لدينا صفحة على موقع لينكد إن (MBTI الشرق الاوسط) تهدف إلى انضمام ممارسي مؤشر MBTI من أجل الاطلاع الدائم على آخر أخبار الأداة.</p>\r\n<p>يوفر منتدى مؤشر MBTI للممارسين الفرص التالية:</p>\r\n<ul>\r\n<li>التعارف على الممارسين المحليين والعالميين.</li>\r\n<li>تطوير المهارات من خلال تبادل الخبرات والاستماع إلى المتحدثين المختصين.</li>\r\n<li>استلام اخبار منتدى مؤشر MBTI الخاصة.</li>\r\n<li>استلام العروض الخاصة حول المنتجات والتدريب.</li>\r\n<li>النقاش عبر الانترنت باستضافة مدربين معتمدين.</li>\r\n<li>امكانية أن تصبح عضواً في صفحة MBTI الشرق الأوسط على حساب لينكد إن (LinkedIn).</li>\r\n</ul>\r\n<p>ومن ضمن المواضيع التي تم مناقشتها خلال المنتديات السابقة:</p>\r\n<ul>\r\n<li>الطبيعة السلوكية والأنماط</li>\r\n<li>إدارة الخلاف عن طريق الأنماط</li>\r\n<li>استخدام الأنماط لتحسين عملية الابداع</li>\r\n</ul>\r\n<pre>إذا كنت ممارساً معتمداً بمؤشر MBTI وترغب في الانضمام لمنتدانا، احجز الآن. </pre>', '', '', ''),
(3, 2, 'en', 'Business Breakfast', '<h3>A complimentary session for our&nbsp;PSI Middle East&nbsp;Community!</h3>\r\n<p>Launched in 2001, our Professional Development Breakfasts are an opportunity for our community to come together and attend an engaging and thought provoking interactive workshop on a topic of interest. The topics we choose are informed by your&nbsp;feedback. The sessions are delivered&nbsp;by our team of Occupational / Business&nbsp;Psychologists or guest experts in the field. Professional Development Breakfasts are held across four&nbsp;locations; Dubai, Abu Dhabi, Doha and Riyadh.</p>\r\n<p style=\"text-align: justify;\">The Professional Development Breakfast provides our clients with the opportunity to:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>Stay informed on the latest talent thinking.</li>\r\n<li>Network and meet likeminded professionals in the field of HR and talent.</li>\r\n<li>Gain new found skills to use in your business environment.</li>\r\n<li>Receive session resources and takeaways to use in your business environment.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Previous breakfast sessions have included:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>How to Facilitate Teams in Creativity and Innovation</li>\r\n<li>Developing Resilience in Your Business Leaders.</li>\r\n<li>Developing a Feedback Culture.</li>\r\n<li>Leading in a Volatile, Uncertain, Complex and Ambiguous (VUCA) world.</li>\r\n<li>360 Feedback Systems.</li>\r\n<li>Using strengths to accelerate peak performance and improve feedback effectiveness.</li>\r\n<li>Driving Performance through Emotional Intelligence.</li>\r\n<li>Creating a Coaching Culture.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">If you would like to attend our upcoming Professional Development Breakfast book now!</p>', '', '', ''),
(6, 5, 'en', 'IPCD', '<h2 class=\"center\"><strong>A unique event for professional psychology and allied fields&nbsp;in the GCC</strong></h2>\r\n<p><strong>Heriot Watt University,&nbsp;Dubai, UAE<br /></strong><span style=\"text-align: justify; line-height: 1.6;\">As an expert in our industry we are delighted to invite you to attend the second IPCD event to be held later this year.</span></p>\r\n<p style=\"text-align: justify;\">The inaugural IPCD (2013), organised by a team of Dubai psychologists, brought together over 200&nbsp; psychology and related professionals from around the region. In 2016 we build on this success with fantastic international keynote speakers, professional development workshops and greater opportunity for networking and discussion of best practice.</p>\r\n<p style=\"text-align: justify;\"><strong>Four key streams:</strong></p>\r\n<ul style=\"text-align: justify;\">\r\n<li>clinical/counselling psychology</li>\r\n<li>organisational/occupational psychology</li>\r\n<li>coaching, positive psychology&nbsp;</li>\r\n<li>child/education psychology</li>\r\n</ul>\r\n<p style=\"text-align: justify;\"><strong>Registration and Programme<br /></strong>Our registration fees offer great value for money. Access to full 2 day event, including catering, for only&nbsp;<strong>700AED!</strong></p>\r\n<p style=\"text-align: justify;\"><strong>Occupational Psychology:<br /></strong>This year\'s keynote in occupational psychology&nbsp;<strong>Dr Alex Haslam</strong>, Professor of Social and Organizational Psychology and Australian Laureate Fellow at the University of Queensland.&nbsp; Dr Haslam will discuss the new psychology of leadership focusing on identity, influence and power.&nbsp; IPCD 2016 also features a workshop on the power of values and two panel discussions&nbsp; on the future of leadership development and the future of assessment. Presentations include looking at mobile assessment trends,&nbsp; leading during times of uncertainty and&nbsp; the care of welfare workers in occupational settings.&nbsp;</p>\r\n<p style=\"text-align: justify;\">Many more talks by local and international experts are on the agenda. Don\'t miss out!</p>\r\n<p style=\"text-align: justify;\"><strong>Full details of Programme and registration are now available on the IPCD website.</strong></p>\r\n<p style=\"text-align: justify;\"><strong>Registrations<br /></strong>Register and pay through the website&nbsp;<a href=\"http://r20.rs6.net/tn.jsp?f=0012VEqkOodukjJ7OtkZvayNV9mZQLa_DwOh0fvQrs89JxBW30AE1GPhZWWrxdVjCbiN8BCwSUrGf2Qc-9TsA8Z1ZgGRd4zW1q6yqAeDUtownwCBQCiEu3BBrtxYB65Ua112T8ROktVp9_5vpYH1WIRvf85XL-hihMuFn0ZSli293Y=&amp;c=LtCirTYLGifTvROSHQ5DVkeFKFXZ5QMMQ3ei6ZZA0B0YTzc5JQoy7w==&amp;ch=bcmPNzJB2gZXYIZQQiDRzOdzPAly1SBbh7Q2dLt9v0A8abetUMHcZw==\"><strong>www.psych-me.com</strong></a></p>\r\n<p style=\"text-align: justify;\">Registration fee:</p>\r\n<ul>\r\n<li style=\"text-align: justify;\">700AED for 2 day event&nbsp;for professionals</li>\r\n<li style=\"text-align: justify;\">250AED student stream</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">IPCD runs a <u>free bus transport service</u> from several locations in Dubai and has partner hotels for discounted rates. See full details on the website.</p>\r\n<p style=\"text-align: justify;\"><strong>General Information<br /></strong><strong><a href=\"mailto:info@psych-me.com\">info@psych-me.com</a></strong></p>\r\n<h3><strong>Gold Sponsors</strong></h3>\r\n<p><strong><u><img class=\"center\" title=\"IPCD\" src=\"/uploads/images/23d38140e32b6d48ddc809dacbdb1b93b74a1df3.png\" width=\"742\" height=\"91\" /></u></strong></p>', '', '', ''),
(7, 6, 'en', 'HR Summit & Expo', '<h1>Meet Us at the HR Summit &amp; Expo!</h1>\r\n<h3><strong>Engagement Theatre</strong>, Halls 5 &ndash; 6, Dubai International Exhibition Centre</h3>\r\n<p>If you&rsquo;re heading to the HR Summit &amp; Expo, drop by and say hello at <span class=\"bold\">Stand D20</span>, where we can have a chat about how we can help you with your HR Solutions!</p>\r\n<p>We are also delighted to be presenting as a <strong>keynote</strong> for the HR Engagement Summit (part of the HR Summit &amp; Expo)</p>\r\n<p>&nbsp;<img src=\"/cms/uploads/images/cc8beb982e60aac8c6729b6844dcc7c13d209983.png\" width=\"533\" height=\"551\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/cms/uploads/images/c9edee720b490a250ffea4db82b46c8ee09e47e7.jpg\" width=\"160\" height=\"204\" /></p>\r\n<p><strong>Amanda White<br /></strong>Managing Director<br />Innovative HR Solutions</p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/cms/uploads/images/c845b8f4ce80933e0ba9c5c9d545ce8860b6e092.jpg\" width=\"159\" height=\"159\" /></p>\r\n<p><strong>Dr Steven J Stein<br /></strong>CEO<br />Multi Health Systems (MHS)</p>\r\n<p>To purchase tickets to the HR Summit and Expo and attend our keynote session on Emotional Intelligence in Engagement at the HR Engagement Summit visit</p>\r\n<p><a href=\"http://www.hrsummitexpo.com/hr-engagement-summit/\">http://www.hrsummitexpo.com/hr-engagement-summit/</a></p>\r\n<p><strong>Register NOW!</strong></p>\r\n<p>Call: +971 4 335 2437<br />Email:<a href=\"http://%20register-mea@informa.com/\"> register-mea@informa.com<br /></a>Web: <a href=\"http://www.hrsummitexpo.com/\">www.hrsummitexpo.com</a></p>', '', '', ''),
(8, 2, 'ar', 'فطور العمل ', '<p>جلسة مجانية لمجتمع&nbsp;انوڤاتيڤ إتش آر سوليوشنز!</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>أبو ظبي &ndash; الثلاثاء ١٥ نوفمبر ٨:٣٠ صباحاً &ndash; ١١:٣٠ صباحاً</p>\r\n<p>الرياض &ndash; الأربعاء ١٦ نوفمبر ١٢:٠٠ ظهراً &ndash; ٣:٠٠ مساءاً</p>\r\n<p>الدوحة &ndash; الخميس ١٧ نوفمبر ٨:٣٠ صباحاً &ndash; ١١:٣٠ صباحاً</p>\r\n<p>&nbsp;</p>\r\n<p>منذ إطلاق جلسات فطور التطوير المهني في عام ٢٠٠٧، أصبحت فرصة لعملائنا للتجمع والتعارف وحضور فطور ممتع ومحفز للفكر حول مواضيع مشوقة. يتم اختيار المواضيع وفقاً لآراء عملائنا، وتُصمم الجلسات بعد ذلك عن طريق المختصين في مجال علم النفس المهني والخبراء في المجالات المختلفة. تُعقد جلسات فطور التطوير المهني مرتين في السنة بأربعة&nbsp;مواقع مختلفة؛ دبي وأبو ظبي و الدوحة و الرياض.</p>\r\n<p>توفر جلسات فطور التطوير المهني الفرصة لعملائنا لتحقيق التالي:</p>\r\n<ul>\r\n<li>الاطلاع على آخر الأفكار والتطورات في مجال المواهب من خلال جلسات مشوقة ومواضيع ممتعة.</li>\r\n<li>التعرف إلى مهنيين عاملين في مجالات مماثلة كالموارد البشرية والمواهب.</li>\r\n<li>اكتساب مهارات جديدة لاستخدامها في مجال بيئة العمل.</li>\r\n<li>الحصول على بعض الموارد والمنتجات التي يمكن استخدامها في بيئة العمل.</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>تضمنت جلسات الفطور السابقة المواضيع التالية:</p>\r\n<ul>\r\n<li>تطوير مستوى المرونة لدى قادة الأعمال</li>\r\n<li>تطوير ثقافة الإفادة حول الأداء مع SCARF</li>\r\n<li>القيادة والمواهب في عالم متقلب وغير واضح ومعقد وغامض (VUCA)</li>\r\n<li>نظام ملاحظات الأداء ٣٦٠</li>\r\n<li>استخدام نقاط القوة لتسريع تقدم الأداء وتحسين عملية الإفادة حول الأداء</li>\r\n<li>تحسين&nbsp;الأداء من خلال الذكاء العاطفي</li>\r\n<li>إنشاء ثقافة التدريب</li>\r\n</ul>\r\n<p>إذا رغبت في حضور جلسة فطور التطوير المهني القادمة قم بالحجز الآن.</p>', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apply_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `apply_link`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Senior Consultant Occupational Psychologist', 'http://www.innovative-hr.com/contact', 1, 1, NULL, '2016-07-05 08:50:07', '2018-11-25 02:09:06');

-- --------------------------------------------------------

--
-- Table structure for table `job_translations`
--

CREATE TABLE `job_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `job_translations`
--

INSERT INTO `job_translations` (`id`, `job_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 'en', 'Senior Consultant Occupational Psychologist', '<p><span class=\"bold\"><strong>Responsibilities</strong>&nbsp;</span></p>\r\n<ul>\r\n<li>Assist in leading and coordinating Assessment and Development Centres<br /><br /></li>\r\n<li>Deliver accredited training programmes in the use of psychometric assessments<br /><br /></li>\r\n<li>Assist in the delivery of other consulting projects across our consultancy services (IDPs, 360 assessments, executive coaching, leading and behaviourial skills training etc.)<br /><br /></li>\r\n<li>Account Manage key clients, coordinating all elements of delivery and maintaining the ongoing client relationship.<br /><br /></li>\r\n<li>Support&nbsp;in the development of new products or services.<br /><br /></li>\r\n<li>Represent&nbsp;PSI&nbsp;in public forums as required (eg. events, seminars etc.)</li>\r\n</ul>\r\n<p><span class=\"bold\"><strong>Qualifications and Experience</strong>&nbsp;</span></p>\r\n<ul>\r\n<li>Masters in Organisational Psychology or related psychology discipline.<br /><br /></li>\r\n<li>Registered as Psychologist in Home Country.<br /><br /></li>\r\n<li>5-8 years in assessment role.<br /><br /></li>\r\n<li>Qualified on one of the major personality profiles.<br /><br /></li>\r\n<li>Significant experience giving feedback on profiles and of assessment/development centre data.<br /><br /></li>\r\n<li>Ability to work extended hours on large assessment centre project as required.<br /><br /></li>\r\n<li>Ability to work with a multicultural client base and travel throughout the GCC.</li>\r\n</ul>\r\n<p><span class=\"bold\"><strong>Competencies</strong></span></p>\r\n<ul>\r\n<li>Strong communication Skills (Written and Oral)<br /><br /></li>\r\n<li>Planning &amp; Organising<br /><br /></li>\r\n<li>Commitment to Achieve<br /><br /></li>\r\n<li>Problem Solving &amp; Decision Making<br /><br /></li>\r\n<li>Adaptability<br /><br /></li>\r\n<li>Positive, high energy outlook<br /><br /></li>\r\n<li>Customer Commitment</li>\r\n</ul>\r\n<p>To apply <a href=\"/contact\">contact us</a> at&nbsp;PSI Middle East</p>', 'a', 'b', 'c'),
(2, 1, 'ar', 'Occupational Psychologist', '<p style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><span class=\"bold\" style=\"font-weight: bold;\"><strong>Responsibilities</strong>&nbsp;</span></p>\r\n<ul style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">\r\n<li>Assist in leading and coordinating Assessment and Development Centres<br /><br /></li>\r\n<li>Deliver accredited training programmes in the use of psychometric assessments<br /><br /></li>\r\n<li>Assist in the delivery of other consulting projects across our consultancy services (IDPs, 360 assessments, executive coaching, leading and behaviourial skills training etc.)<br /><br /></li>\r\n<li>Account Manage key clients, coordinating all elements of delivery and maintaining the ongoing client relationship.<br /><br /></li>\r\n<li>Support&nbsp;in the development of new products or services.<br /><br /></li>\r\n<li>Represent IHS in public forums as required (eg. events, seminars etc.)</li>\r\n</ul>\r\n<p style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><span class=\"bold\" style=\"font-weight: bold;\"><strong>Qualifications and Experience</strong>&nbsp;</span></p>\r\n<ul style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">\r\n<li>Masters in Organisational Psychology or related psychology discipline.<br /><br /></li>\r\n<li>Registered as Psychologist in Home Country.<br /><br /></li>\r\n<li>5-8 years in assessment role.<br /><br /></li>\r\n<li>Qualified on one of the major personality profiles.<br /><br /></li>\r\n<li>Significant experience giving feedback on profiles and of assessment/development centre data.<br /><br /></li>\r\n<li>Ability to work extended hours on large assessment centre project as required.<br /><br /></li>\r\n<li>Ability to work with a multicultural client base and travel throughout the GCC.</li>\r\n</ul>\r\n<p style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><span class=\"bold\" style=\"font-weight: bold;\"><strong>Competencies</strong></span></p>\r\n<ul style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">\r\n<li>Strong communication Skills (Written and Oral)<br /><br /></li>\r\n<li>Planning &amp; Organising<br /><br /></li>\r\n<li>Commitment to Achieve<br /><br /></li>\r\n<li>Problem Solving &amp; Decision Making<br /><br /></li>\r\n<li>Adaptability<br /><br /></li>\r\n<li>Positive, high energy outlook<br /><br /></li>\r\n<li>Customer Commitment</li>\r\n</ul>\r\n<p style=\"color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">To apply<span class=\"Apple-converted-space\">&nbsp;</span><a href=\"/contact\">contact us</a><span class=\"Apple-converted-space\">&nbsp;</span>at Innovative HR Solutions</p>', 'a', 'dfsdf', 'sf');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `landscape` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landscape_tinified` int(1) NOT NULL DEFAULT '0',
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `coordinates` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head_office` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `parent_id`, `name`, `landscape`, `landscape_tinified`, `lft`, `rgt`, `depth`, `coordinates`, `head_office`, `phone`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, NULL, 'Abu Dhabi', NULL, 0, 1, 2, 0, '24.299184, 54.696983', 0, '', 1, 1, 2, '2016-07-13 04:10:38', '2016-11-20 15:05:04'),
(3, NULL, 'Manama', NULL, 0, 13, 14, 0, '26.2285, 50.5860', 0, '', 1, 1, 2, '2016-07-13 04:20:32', '2016-11-20 15:07:31'),
(5, NULL, 'Doha', 'uploads/images/locations/ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4.png', 0, 5, 6, 0, '25.287932, 51.532890', 0, '', 1, 1, 2, '2016-08-04 03:04:45', '2016-11-20 15:06:05'),
(6, NULL, 'Dubai', 'uploads/images/locations/c1dfd96eea8cc2b62785275bca38ac261256e278.png', 0, 7, 8, 0, '25.213330, 55.284375', 1, '97143902778+', 1, 1, 5, '2016-08-04 03:08:25', '2017-01-22 19:47:06'),
(7, NULL, 'Amman', NULL, 0, 3, 4, 0, '31.9454, 35.9284', 0, '', 1, 1, 2, '2016-08-04 03:08:58', '2016-11-20 15:05:16'),
(8, NULL, 'Kuwait', NULL, 0, 11, 12, 0, '29.391908, 47.980505', 0, '', 1, 1, 2, '2016-08-04 03:09:29', '2016-11-20 15:06:52'),
(9, NULL, 'Muscat', NULL, 0, 15, 16, 0, '23.589308, 58.403751', 0, '', 1, 1, 2, '2016-08-04 03:09:55', '2016-11-20 15:07:52'),
(10, NULL, 'Riyadh', NULL, 0, 17, 18, 0, '24.733663, 46.683791', 0, '', 1, 1, 2, '2016-08-04 03:10:21', '2018-05-23 03:46:00'),
(11, NULL, 'Jeddah', NULL, 0, 9, 10, 0, '21.286848, 39.237060', 0, '', 1, 1, 2, '2016-08-17 01:17:07', '2016-11-20 15:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `location_translations`
--

CREATE TABLE `location_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `location_translations`
--

INSERT INTO `location_translations` (`id`, `location_id`, `locale`, `title`) VALUES
(3, 2, 'ar', 'أبو ظبي'),
(4, 3, 'ar', 'المنامة'),
(5, 2, 'en', 'Abu Dhabi'),
(6, 3, 'en', 'Manama'),
(8, 5, 'en', 'Doha'),
(9, 6, 'en', 'Dubai'),
(10, 7, 'en', 'Amman'),
(11, 8, 'en', 'Kuwait'),
(12, 9, 'en', 'Muscat'),
(13, 10, 'en', 'Riyadh'),
(14, 11, 'en', 'Jeddah'),
(15, 7, 'ar', 'عَمَّان'),
(16, 5, 'ar', 'الدوحة'),
(17, 6, 'ar', 'دبي'),
(18, 11, 'ar', 'جدة'),
(19, 8, 'ar', 'الكويت'),
(20, 9, 'ar', 'مسقط'),
(21, 10, 'ar', 'الرياض');

-- --------------------------------------------------------

--
-- Table structure for table `logo_sliders`
--

CREATE TABLE `logo_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo_tinified` int(1) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logo_sliders`
--

INSERT INTO `logo_sliders` (`id`, `parent_id`, `name`, `lft`, `rgt`, `depth`, `logo`, `logo_tinified`, `link`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(7, NULL, 'Global Leader', 9, 10, 0, 'uploads/images/logoSliders/902ba3cda1883801594b6e1b452790cc53948fda.jpg', 0, 'http://www.innovative-hr.com/assessment/assessment-centre-exercises', 1, 3, 2, '2016-10-24 11:08:56', '2016-12-19 19:06:51'),
(8, NULL, 'AACT', 13, 14, 0, 'uploads/images/logoSliders/fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f.jpg', 0, 'http://www.innovative-hr.com/calendar/course/13/afc-executive-coaching-certification?referrer=14', 1, 3, 2, '2016-10-24 11:09:57', '2016-12-19 19:09:07'),
(9, NULL, 'BPS', 15, 16, 0, 'uploads/images/logoSliders/0ade7c2cf97f75d009975f4d720d1fa6c19f4897.jpg', 0, 'http://www.innovative-hr.com/calendar/course/6/bps-qualification-in-occupation-testing?referrer=14', 1, 3, 2, '2016-10-24 11:10:51', '2016-12-19 19:09:54'),
(10, NULL, 'EQi 2.0', 5, 6, 0, 'uploads/images/logoSliders/b1d5781111d84f7b3fe45a0852e59758cd7a87e5.jpg', 0, 'http://www.innovative-hr.com/assessment/personality-assessment', 1, 3, 2, '2016-10-24 11:11:46', '2016-12-19 19:06:17'),
(12, NULL, 'EQ 360', 7, 8, 0, 'uploads/images/logoSliders/7b52009b64fd0a2a49e6d8a939753077792b0554.jpg', 0, 'http://www.innovative-hr.com/assessment/360-surveys', 1, 3, 2, '2016-10-24 11:12:21', '2016-12-19 19:06:35'),
(13, NULL, 'Hogan', 27, 28, 0, 'uploads/images/logoSliders/bd307a3ec329e10a2cff8fb87480823da114f8f4.jpg', 0, 'http://www.innovative-hr.com/assessment/personality-assessment', 1, 3, 2, '2016-10-24 11:13:52', '2018-12-30 09:02:20'),
(14, NULL, 'Saville', 3, 4, 0, 'uploads/images/logoSliders/fa35e192121eabf3dabf9f5ea6abdbcbc107ac3b.png', 1, 'http://www.innovative-hr.com/assessment/ability-assessments', 1, 3, 4, '2016-10-24 11:14:53', '2017-08-07 05:13:59'),
(15, NULL, 'StrengthScope', 29, 30, 0, 'uploads/images/logoSliders/f1abd670358e036c31296e66b3b66c382ac00812.jpg', 0, NULL, 1, 3, NULL, '2016-10-24 11:15:23', '2018-12-30 09:02:20'),
(16, NULL, 'Firo Business', 17, 18, 0, 'uploads/images/logoSliders/1574bddb75c78a6fd2251d61e2993b5146201319.jpg', 0, 'http://www.innovative-hr.com/assessment/specialised-assessment', 1, 3, 2, '2016-10-24 11:16:26', '2016-12-19 19:10:36'),
(17, NULL, 'Strong', 11, 12, 0, 'uploads/images/logoSliders/0716d9708d321ffb6a00818614779e779925365c.jpg', 0, 'http://www.innovative-hr.com/assessment/specialised-assessment', 1, 3, 2, '2016-10-24 11:16:57', '2016-12-19 19:07:19'),
(18, NULL, 'MBTI', 1, 2, 0, 'uploads/images/logoSliders/9e6a55b6b4563e652a23be9d623ca5055c356940.png', 0, 'http://www.innovative-hr.com/assessment/personality-assessment', 1, 3, 2, '2016-10-24 11:17:16', '2016-12-19 19:04:44'),
(19, NULL, 'NEO', 21, 22, 0, 'uploads/images/logoSliders/b3f0c7f6bb763af1be91d9e74eabfeb199dc1f1f.png', 0, 'http://www.innovative-hr.com/assessment/personality-assessment', 1, 3, 2, '2016-10-24 11:18:49', '2018-12-30 09:02:20'),
(20, NULL, 'TKI', 19, 20, 0, 'uploads/images/logoSliders/91032ad7bbcb6cf72875e8e8207dcfba80173f7c.jpg', 0, 'http://www.innovative-hr.com/assessment/specialised-assessment', 1, 3, 2, '2016-10-24 11:21:03', '2018-12-30 09:02:20'),
(21, NULL, 'Ravens', 23, 24, 0, 'uploads/images/logoSliders/472b07b9fcf2c2451e8781e944bf5f77cd8457c8.jpg', 0, 'http://www.innovative-hr.com/assessment/ability-assessments', 1, 3, 2, '2016-10-24 11:23:28', '2018-12-30 09:02:20'),
(22, NULL, 'MTQ48', 25, 26, 0, 'uploads/images/logoSliders/12c6fc06c99a462375eeb3f43dfd832b08ca9e17.jpg', 0, 'http://www.innovative-hr.com/assessment/specialised-assessment', 1, 3, 2, '2016-10-24 12:29:51', '2018-12-30 09:02:20'),
(25, NULL, '16PF', 31, 32, 0, 'uploads/images/logoSliders/f6e1126cedebf23e1463aee73f9df08783640400.png', 0, NULL, 1, 4, NULL, '2018-12-31 02:34:11', '2018-12-31 02:34:11'),
(26, NULL, 'EIP3', 33, 34, 0, 'uploads/images/logoSliders/887309d048beef83ad3eabf2a79a64a389ab1c9f.jpg', 0, NULL, 1, 4, NULL, '2018-12-31 02:35:42', '2018-12-31 02:35:42'),
(27, NULL, 'Am I Job Ready?', 35, 36, 0, 'uploads/images/logoSliders/bc33ea4e26e5e1af1408321416956113a4658763.png', 0, NULL, 1, 4, NULL, '2018-12-31 02:36:29', '2018-12-31 02:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `logo_slider_translations`
--

CREATE TABLE `logo_slider_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo_slider_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logo_slider_translations`
--

INSERT INTO `logo_slider_translations` (`id`, `logo_slider_id`, `locale`, `link`) VALUES
(1, 18, 'en', 'http://www.innovative-hr.com/assessment/personality-assessment'),
(2, 14, 'en', 'http://www.innovative-hr.com/assessment/ability-assessments'),
(3, 10, 'en', 'http://www.innovative-hr.com/assessment/personality-assessment'),
(4, 12, 'en', 'http://www.innovative-hr.com/assessment/360-surveys'),
(5, 7, 'en', 'http://www.innovative-hr.com/assessment/assessment-centre-exercises'),
(6, 17, 'en', 'http://www.innovative-hr.com/assessment/specialised-assessment'),
(7, 8, 'en', 'http://www.innovative-hr.com/calendar/course/13/afc-executive-coaching-certification?referrer=14'),
(8, 9, 'en', 'http://www.innovative-hr.com/calendar/course/6/bps-qualification-in-occupation-testing?referrer=14'),
(9, 16, 'en', 'http://www.innovative-hr.com/assessment/specialised-assessment'),
(11, 20, 'en', 'http://www.innovative-hr.com/assessment/specialised-assessment'),
(12, 21, 'en', 'http://www.innovative-hr.com/assessment/ability-assessments'),
(13, 22, 'en', 'http://www.innovative-hr.com/assessment/specialised-assessment'),
(14, 13, 'en', 'http://www.innovative-hr.com/assessment/personality-assessment'),
(16, 19, 'en', 'http://www.innovative-hr.com/assessment/personality-assessment'),
(17, 18, 'ar', 'http://www.innovative-hr.com/ar/assessment/personality-assessment'),
(18, 14, 'ar', 'http://www.innovative-hr.com/ar/assessment/ability-assessments'),
(19, 10, 'ar', 'http://www.innovative-hr.com/ar/assessment/personality-assessment'),
(20, 12, 'ar', 'http://www.innovative-hr.com/ar/assessment/360-surveys'),
(21, 7, 'ar', 'http://www.innovative-hr.com/ar/assessment/assessment-centre-exercises'),
(22, 17, 'ar', 'http://www.innovative-hr.com/ar/assessment/specialised-assessment'),
(23, 8, 'ar', 'http://www.innovative-hr.com/ar/calendar/course/13/afc-executive-coaching-certification?referrer=14'),
(24, 9, 'ar', 'http://www.innovative-hr.com/ar/calendar/course/6/bps-qualification-in-occupation-testing?referrer=14'),
(25, 16, 'ar', 'http://www.innovative-hr.com/ar/assessment/specialised-assessment'),
(27, 20, 'ar', 'http://www.innovative-hr.com/ar/assessment/specialised-assessment'),
(28, 19, 'ar', 'http://www.innovative-hr.com/ar/assessment/personality-assessment'),
(29, 21, 'ar', 'http://www.innovative-hr.com/ar/assessment/ability-assessments'),
(30, 22, 'ar', 'http://www.innovative-hr.com/ar/assessment/specialised-assessment'),
(31, 13, 'ar', 'http://www.innovative-hr.com/ar/assessment/personality-assessment'),
(33, 25, 'en', 'https://middleeast.psionline.com/assessment'),
(34, 26, 'en', 'https://middleeast.psionline.com/assessment'),
(35, 27, 'en', 'https://middleeast.psionline.com/assessment');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tinified` int(1) DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `title`, `name`, `type`, `path`, `caption`, `description`, `tinified`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'homepage-banner.png', 'f62cb6ba52b8b58ce5ef6af59e18190320c0ac03.png', 'image', 'uploads/images/f62cb6ba52b8b58ce5ef6af59e18190320c0ac03.png', NULL, NULL, 0, 1, NULL, '2016-08-03 01:50:57', '2016-08-03 01:50:57'),
(6, 'doc.pdf', '69dd5d19c29974221714e1eabfcbaede2704d4c1.pdf', 'pdf', 'uploads/documents/69dd5d19c29974221714e1eabfcbaede2704d4c1.pdf', NULL, NULL, 0, 1, 1, '2016-08-07 06:37:49', '2016-08-07 06:37:58'),
(10, 'gallery-1.png', '2279a0a482d8ed5abad9a85699ee20823dd17b6e.png', 'image', 'uploads/images/2279a0a482d8ed5abad9a85699ee20823dd17b6e.png', NULL, NULL, 1, 1, NULL, '2016-08-08 05:55:55', '2017-06-21 06:08:04'),
(11, 'gallery-2.png', '7fe6982033c2ef91a784103cdf03f9cf4127bce0.png', 'image', 'uploads/images/7fe6982033c2ef91a784103cdf03f9cf4127bce0.png', NULL, NULL, 0, 1, NULL, '2016-08-08 05:55:55', '2016-08-08 05:55:55'),
(13, 'iStock_88230227_XXXLARGE (1).jpg', 'c66ac42b3805072e8ce3bc2177263f2be08bfd9f.jpg', 'image', 'uploads/images/c66ac42b3805072e8ce3bc2177263f2be08bfd9f.jpg', NULL, NULL, 0, 2, NULL, '2016-08-10 07:25:36', '2016-08-10 07:25:36'),
(14, 'iStock_88230227_XXXLARGE (1).jpg', '953ac867bea5f545e69b0a2dc83d2015c4f36e81.jpg', 'image', 'uploads/images/953ac867bea5f545e69b0a2dc83d2015c4f36e81.jpg', NULL, NULL, 0, 2, NULL, '2016-08-10 07:26:00', '2016-08-10 07:26:00'),
(15, 'PersonalReport.pdf', '9080a5a3e037fe87b2636ed8a837d2d4c811535b.pdf', 'pdf', 'uploads/documents/9080a5a3e037fe87b2636ed8a837d2d4c811535b.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:08:02', '2016-08-10 09:44:17'),
(16, 'Line Manager Report.pdf', 'deea17520034e7409473029655c73d154f985fe7.pdf', 'pdf', 'uploads/documents/deea17520034e7409473029655c73d154f985fe7.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:08:03', '2016-08-10 09:44:30'),
(17, 'Expert Report.pdf', '897c88225d110f11446f761c4ce7838699b7cdc8.pdf', 'pdf', 'uploads/documents/897c88225d110f11446f761c4ce7838699b7cdc8.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:08:07', '2016-08-10 09:44:39'),
(18, 'Line Manager Report.pdf', '392bc8a600870cff87ec4e16a9e58d4b7381daa6.pdf', 'pdf', 'uploads/documents/392bc8a600870cff87ec4e16a9e58d4b7381daa6.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:08:36', '2016-08-10 09:44:50'),
(19, 'Expert Report.pdf', 'd2f55fcfde534164636f51314545b406de26c208.pdf', 'pdf', 'uploads/documents/d2f55fcfde534164636f51314545b406de26c208.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:13:27', '2016-08-10 09:45:00'),
(20, 'Personal Report.pdf', 'fe654cae809d8a8ae1422e00f5d0ffe3cd335edd.pdf', 'pdf', 'uploads/documents/fe654cae809d8a8ae1422e00f5d0ffe3cd335edd.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:13:33', '2016-08-10 09:45:08'),
(21, 'Environmental Fit Report.pdf', '6fcb54f0e9f3510d6794639aa78842d21ed7b18a.pdf', 'pdf', 'uploads/documents/6fcb54f0e9f3510d6794639aa78842d21ed7b18a.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:57:19', '2016-08-10 09:45:18'),
(22, 'Behavioural Profile.pdf', 'b849648492fa2a03a287f729cdedd8dd021747ec.pdf', 'pdf', 'uploads/documents/b849648492fa2a03a287f729cdedd8dd021747ec.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 08:57:20', '2016-08-10 09:45:28'),
(23, 'Customer Strengths Environment Fit Report.pdf', '8b2ae778e2ba1302e074aae8e4ac1c70d7d7f9ce.pdf', 'pdf', 'uploads/documents/8b2ae778e2ba1302e074aae8e4ac1c70d7d7f9ce.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 09:09:20', '2016-08-10 09:45:48'),
(24, 'Customer Strengths Behaviour Reporr', 'a96aa5c2386a23366053ebbe77c3766bc4d64d51.pdf', 'pdf', 'uploads/documents/a96aa5c2386a23366053ebbe77c3766bc4d64d51.pdf', NULL, NULL, 0, 2, 3, '2016-08-10 09:09:21', '2016-09-07 02:47:24'),
(25, 'Operational Strengths Environment Fit Report.pdf', '872cba058a9b9ca2366b7a6ec524bfbdab79db08.pdf', 'pdf', 'uploads/documents/872cba058a9b9ca2366b7a6ec524bfbdab79db08.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 09:28:09', '2016-08-10 09:46:34'),
(26, 'Operational Strengths Report.pdf', '4e6f1128c020f524afa1b0aa8b45dd13b4a70f46.pdf', 'pdf', 'uploads/documents/4e6f1128c020f524afa1b0aa8b45dd13b4a70f46.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 09:28:10', '2016-08-10 09:46:47'),
(28, 'SC_Administrative_Strengths_Environment_Fit_Report.pdf', '43ef91a9ef2de6cfda487a0f8280227417dc9856.pdf', 'pdf', 'uploads/documents/43ef91a9ef2de6cfda487a0f8280227417dc9856.pdf', NULL, NULL, 0, 2, NULL, '2016-08-10 09:34:08', '2016-08-10 09:34:08'),
(29, 'Administrative Strengths Report.pdf', 'd24ae4115d5db0ab9f1c40d1a4311771c8464796.pdf', 'pdf', 'uploads/documents/d24ae4115d5db0ab9f1c40d1a4311771c8464796.pdf', NULL, NULL, 0, 2, 2, '2016-08-10 09:34:09', '2016-08-10 09:47:04'),
(32, 'SC_My_Self_Report.pdf', '6edf9242327a76660b85c79e45929cbce4e28cb4.pdf', 'pdf', 'uploads/documents/6edf9242327a76660b85c79e45929cbce4e28cb4.pdf', NULL, NULL, 0, 2, NULL, '2016-08-10 09:39:09', '2016-08-10 09:39:09'),
(33, '360_Report_Ahmed_Al_Ali_-_English_Sample.pdf', '5ed8ad517e4d7fd2d9d6c9559e312facb5f2b1a3.pdf', 'pdf', 'uploads/documents/5ed8ad517e4d7fd2d9d6c9559e312facb5f2b1a3.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 05:28:44', '2016-08-11 05:28:44'),
(34, 'Jo_Wilson__Performance_360.pdf', '1f2fe6ed35660fa610ef9d723aaff498fd3dbd39.pdf', 'pdf', 'uploads/documents/1f2fe6ed35660fa610ef9d723aaff498fd3dbd39.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 05:34:40', '2016-08-11 05:34:40'),
(35, 'Jack_Doe_EQ360_Feedback_Standard_Client_-_update_Nov_15_RF.pdf', 'b7b84de431654b16ae80d2c470ed67eea58567e9.pdf', 'pdf', 'uploads/documents/b7b84de431654b16ae80d2c470ed67eea58567e9.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 05:39:30', '2016-08-11 05:39:30'),
(39, 'ST-284108_-_Strong_Profile.pdf', '94ba444dd25fa2347493f982231e2e5f103fb042.pdf', 'pdf', 'uploads/documents/94ba444dd25fa2347493f982231e2e5f103fb042.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 07:49:53', '2016-08-11 07:49:53'),
(41, 'FIRO-B-220170_-_FIRO_Business_Profile.pdf', 'd5e8bb7a5f3ee4a9491e28cb87aa26ed698c6c29.pdf', 'pdf', 'uploads/documents/d5e8bb7a5f3ee4a9491e28cb87aa26ed698c6c29.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 08:18:36', '2016-08-11 08:18:36'),
(42, 'TKI_Profile_and_Interpretive_Report.pdf', '290b4bb9dbf115f06e9d50c1524217185f31853c.pdf', 'pdf', 'uploads/documents/290b4bb9dbf115f06e9d50c1524217185f31853c.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 08:21:18', '2016-08-11 08:21:18'),
(43, 'safety_report.pdf', '5916c3fe71f9e1c0a51673bab75cfda34eec453a.pdf', 'pdf', 'uploads/documents/5916c3fe71f9e1c0a51673bab75cfda34eec453a.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:13:52', '2016-08-11 09:13:52'),
(44, 'Me2_Development_Report.pdf', '3799cc9dfeda367414a125155a2ca7dcfa474ab4.pdf', 'pdf', 'uploads/documents/3799cc9dfeda367414a125155a2ca7dcfa474ab4.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:16:54', '2016-08-11 09:16:54'),
(45, 'MTQ48_Development_Report.pdf', '307d9ec90054104e39201280a97671d3382d0394.pdf', 'pdf', 'uploads/documents/307d9ec90054104e39201280a97671d3382d0394.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:20:31', '2016-08-11 09:20:31'),
(46, 'workplace_english_-_customer_service_report_032011.sflb.pdf', '38cd6490e87dace68e877cb5e9c9c5fc7ba14efa.pdf', 'pdf', 'uploads/documents/38cd6490e87dace68e877cb5e9c9c5fc7ba14efa.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:23:26', '2016-08-11 09:23:26'),
(47, 'MBTI_Step_I_Profile.pdf', 'd60ac00311f2751572dab3cfd0dc149d3960549b.pdf', 'pdf', 'uploads/documents/d60ac00311f2751572dab3cfd0dc149d3960549b.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:47:55', '2016-08-11 09:47:55'),
(48, 'MBTI_Step_II_Profile (1).pdf', '254f73d2c8419a9e03820074f43d6ad437cee7dd.pdf', 'pdf', 'uploads/documents/254f73d2c8419a9e03820074f43d6ad437cee7dd.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:48:36', '2016-08-11 09:48:36'),
(49, 'MBTI_Step_I_Interpretive_Report.pdf', 'e68d9ec82b2ba1851bc014959ef4606af4891c79.pdf', 'pdf', 'uploads/documents/e68d9ec82b2ba1851bc014959ef4606af4891c79.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:48:37', '2016-08-11 09:48:37'),
(50, 'MBTI_Step_II_Interpretive_Report.pdf', '01380a643916a3f9e3d4df0ef82ff9c67421576b.pdf', 'pdf', 'uploads/documents/01380a643916a3f9e3d4df0ef82ff9c67421576b.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:50:13', '2016-08-11 09:50:13'),
(51, 'EQ-i_2.0_Client_Report.pdf', '248bfe18588abf65db4cc7619b3c067fb60edae2.pdf', 'pdf', 'uploads/documents/248bfe18588abf65db4cc7619b3c067fb60edae2.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:57:39', '2016-08-11 09:57:39'),
(52, 'EQ-i_2.0_Coach_Report.pdf', 'bb8dd99ddb2c69387330ddbf3443cd619e07e7d2.pdf', 'pdf', 'uploads/documents/bb8dd99ddb2c69387330ddbf3443cd619e07e7d2.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 09:59:07', '2016-08-11 09:59:07'),
(54, 'Hogan_Development_Survey__HDS__Report.pdf', '9066cdf012448477601bc72556d6e7a027e0acd4.pdf', 'pdf', 'uploads/documents/9066cdf012448477601bc72556d6e7a027e0acd4.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 10:25:54', '2016-08-11 10:25:54'),
(55, 'Hogan_Personality_Inventory__HPI__Report.pdf', 'f804541a5f58f102bdf20a9cbc5094f7c32edf30.pdf', 'pdf', 'uploads/documents/f804541a5f58f102bdf20a9cbc5094f7c32edf30.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 10:28:44', '2016-08-11 10:28:44'),
(57, 'Hogan_Motives__Values_and_Preferences_Inventory__MVPI__Report.pdf', 'fe2b810132b553bf45fb46b46ea0f8f2fc88eab5.pdf', 'pdf', 'uploads/documents/fe2b810132b553bf45fb46b46ea0f8f2fc88eab5.pdf', NULL, NULL, 0, 2, NULL, '2016-08-11 10:31:28', '2016-08-11 10:31:28'),
(58, 'The_Ledership_Code.jpg', '88121b14a3dedc0ed9986d48208c43758b10a5f3.jpg', 'image', 'uploads/images/88121b14a3dedc0ed9986d48208c43758b10a5f3.jpg', NULL, NULL, 0, 2, NULL, '2016-08-12 04:46:43', '2016-08-12 04:46:43'),
(68, 'SavilleConsulting_Wave_Accreditation.pdf', 'dc343836d4f21d56fd7ee358b9541e34d2612c30.pdf', 'pdf', 'uploads/documents/dc343836d4f21d56fd7ee358b9541e34d2612c30.pdf', NULL, NULL, 0, 2, NULL, '2016-08-12 06:01:01', '2016-08-12 06:01:01'),
(69, 'gallery-3', 'cb18c96c022a951d6c0d3f5112facb54438489cf.jpg', 'image', 'uploads/images/cb18c96c022a951d6c0d3f5112facb54438489cf.jpg', NULL, NULL, 0, 2, 2, '2016-08-13 05:58:20', '2016-08-13 06:06:03'),
(70, '616499_593302850705567_2096283974_o.jpg', '4c7cd0831bacd731789719d626e1b0020d81fefa.jpg', 'image', 'uploads/images/4c7cd0831bacd731789719d626e1b0020d81fefa.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:58:36', '2016-08-13 05:58:36'),
(72, '1012280_732138856821965_3269469239380054709_n.jpg', '6dd6dd653c6c0d88acf62a89cfbbfc221367aac2.jpg', 'image', 'uploads/images/6dd6dd653c6c0d88acf62a89cfbbfc221367aac2.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:58:44', '2016-08-13 05:58:44'),
(73, '1016568_732091390160045_1513557596966853573_n.jpg', '6913477b9eefb50fd23be2e3fb399f86a881fa81.jpg', 'image', 'uploads/images/6913477b9eefb50fd23be2e3fb399f86a881fa81.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:58:51', '2016-08-13 05:58:51'),
(74, '1012672_733316170037567_8777016947309129367_n.jpg', 'f430fd0e7a81edb115ec09f733915820cc5d76e5.jpg', 'image', 'uploads/images/f430fd0e7a81edb115ec09f733915820cc5d76e5.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:58:52', '2016-08-13 05:58:52'),
(75, '1384228_732132766822574_2467929618670340109_n.jpg', '2d458e76d01fc19ae1636f7227f7618c00eb7c92.jpg', 'image', 'uploads/images/2d458e76d01fc19ae1636f7227f7618c00eb7c92.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:58:57', '2016-08-13 05:58:57'),
(76, '1268807_960675150635000_3265494904961436868_o.jpg', '2a1f50ea8baa0a5c0723dcd10d788af478c8dfba.jpg', 'image', 'uploads/images/2a1f50ea8baa0a5c0723dcd10d788af478c8dfba.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:03', '2016-08-13 05:59:03'),
(77, '1450926_593242570711595_2113487806_n.jpg', '62bc17085001a6fc96c5adddda744f266a899dc5.jpg', 'image', 'uploads/images/62bc17085001a6fc96c5adddda744f266a899dc5.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:10', '2016-08-13 05:59:10'),
(78, '1511534_732132756822575_2612248436667912972_n.jpg', '77e0979ee8f619c6abec64300b0c17fb53a164e2.jpg', 'image', 'uploads/images/77e0979ee8f619c6abec64300b0c17fb53a164e2.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:19', '2016-08-13 05:59:19'),
(79, '1397794_593302854038900_1128719949_o.jpg', '1b9d548fdc09b14796e1af53a7e8e3e8d0e41efd.jpg', 'image', 'uploads/images/1b9d548fdc09b14796e1af53a7e8e3e8d0e41efd.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:25', '2016-08-13 05:59:25'),
(80, '1622599_734967573205760_5544565190584998472_n.jpg', '01b7b57daeb1bd43d4db1ac40984b2d48f84e8c1.jpg', 'image', 'uploads/images/01b7b57daeb1bd43d4db1ac40984b2d48f84e8c1.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:27', '2016-08-13 05:59:27'),
(81, '1917515_960085714027277_6453706042860050642_n.jpg', '0e22223efe02be610bcc5f74e948c0eb74eb0ee4.jpg', 'image', 'uploads/images/0e22223efe02be610bcc5f74e948c0eb74eb0ee4.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:31', '2016-08-13 05:59:31'),
(82, '1937501_734497559919428_7661679918698477502_n.jpg', '0792ad4472c84bc67b1bb441faf422572aec8db2.jpg', 'image', 'uploads/images/0792ad4472c84bc67b1bb441faf422572aec8db2.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:31', '2016-08-13 05:59:31'),
(83, '10006417_644669282235590_726139694_n.jpg', 'd2476ff5c4423b9ab25ac9b5caa3bea364a51de9.jpg', 'image', 'uploads/images/d2476ff5c4423b9ab25ac9b5caa3bea364a51de9.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:35', '2016-08-13 05:59:35'),
(84, '10258193_659534447415740_9185186917090154625_n.jpg', '31b1a748b1aba3cfa1b7911af8f803b2205fb1f4.jpg', 'image', 'uploads/images/31b1a748b1aba3cfa1b7911af8f803b2205fb1f4.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:40', '2016-08-13 05:59:40'),
(85, '10298733_664660190236499_4141363762799627436_n.jpg', 'e4bc4e54b4a7dc6edb16082812cd277a8050bf58.jpg', 'image', 'uploads/images/e4bc4e54b4a7dc6edb16082812cd277a8050bf58.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:43', '2016-08-13 05:59:43'),
(86, '10293577_913178882051294_2195199745825472620_o.jpg', 'dd136c281245cf7efcf3b9b14e1500e6112f065f.jpg', 'image', 'uploads/images/dd136c281245cf7efcf3b9b14e1500e6112f065f.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:54', '2016-08-13 05:59:54'),
(88, '10516623_720822687953582_6802580764141428701_n.jpg', 'af67ef9837c2097ad62b97c2a6ec2fab17387b9e.jpg', 'image', 'uploads/images/af67ef9837c2097ad62b97c2a6ec2fab17387b9e.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 05:59:59', '2016-08-13 05:59:59'),
(89, '10339325_682563875112797_752515727670335412_o.jpg', '4b8772ad0ddd8c6096c98e760d3efb7271b1bf01.jpg', 'image', 'uploads/images/4b8772ad0ddd8c6096c98e760d3efb7271b1bf01.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:06', '2016-08-13 06:00:06'),
(90, '10540877_760352447333939_23576739173191249_o.jpg', 'bab10051960f354ec3400f311cebc8b81d0a1c85.jpg', 'image', 'uploads/images/bab10051960f354ec3400f311cebc8b81d0a1c85.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:11', '2016-08-13 06:00:11'),
(91, '10599427_720822614620256_1325619568488240270_n.jpg', 'df5e054929228b3e4b992b23d72e57da8756ebe1.jpg', 'image', 'uploads/images/df5e054929228b3e4b992b23d72e57da8756ebe1.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:13', '2016-08-13 06:00:13'),
(92, '10606178_731885973513920_5922234926112784399_n.jpg', 'dd122540992180b3aeedcd6a3336d18bad89dcb8.jpg', 'image', 'uploads/images/dd122540992180b3aeedcd6a3336d18bad89dcb8.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:16', '2016-08-13 06:00:16'),
(93, '10600390_732091403493377_6360986948976474087_n.jpg', '012055f1bd9d7314876323859d8032f80b3d1e5e.jpg', 'image', 'uploads/images/012055f1bd9d7314876323859d8032f80b3d1e5e.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:19', '2016-08-13 06:00:19'),
(94, '10608791_720822634620254_1100563561523263537_o.jpg', 'e16a8c63275396d7775f0e326536d79512ef4581.jpg', 'image', 'uploads/images/e16a8c63275396d7775f0e326536d79512ef4581.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:25', '2016-08-13 06:00:25'),
(95, '10612547_732545386781312_6938292929971761112_n.jpg', '2f5081903bdf012ceeb7a70ce850d8e8bb030a79.jpg', 'image', 'uploads/images/2f5081903bdf012ceeb7a70ce850d8e8bb030a79.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:25', '2016-08-13 06:00:25'),
(96, '10616370_732107486825102_3568047303859305567_n.jpg', '312f46e3955ce53e575ad9a1327dc646481bab3b.jpg', 'image', 'uploads/images/312f46e3955ce53e575ad9a1327dc646481bab3b.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:31', '2016-08-13 06:00:31'),
(97, '11037633_802211566481360_1459238573445919784_n.jpg', 'ae9aebcedad1f0a8e8c0792ef36c5cf6effebd94.jpg', 'image', 'uploads/images/ae9aebcedad1f0a8e8c0792ef36c5cf6effebd94.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:33', '2016-08-13 06:00:33'),
(98, '11060258_801110599924790_1092043520152173107_n.jpg', '5ec39bd5dcdde5df93c2644ad27dd124ec12d252.jpg', 'image', 'uploads/images/5ec39bd5dcdde5df93c2644ad27dd124ec12d252.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:38', '2016-08-13 06:00:38'),
(99, '11059637_964576786911503_2399667126418670001_n.jpg', 'dacf9f32fe874e34cc040e091f9eb3d679d5d535.jpg', 'image', 'uploads/images/dacf9f32fe874e34cc040e091f9eb3d679d5d535.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:44', '2016-08-13 06:00:44'),
(100, '11070007_804358929599957_7504269567832187108_n.jpg', 'dc1a6ad7ea6ab948be47de355c3f354eef6642ba.jpg', 'image', 'uploads/images/dc1a6ad7ea6ab948be47de355c3f354eef6642ba.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:46', '2016-08-13 06:00:46'),
(101, '11229985_824218754280641_1871486665340998917_n.jpg', '08b16bfceecbbd073b008a15352fdb48712b96f4.jpg', 'image', 'uploads/images/08b16bfceecbbd073b008a15352fdb48712b96f4.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:53', '2016-08-13 06:00:53'),
(102, '11951351_893460260689823_6282437913160767135_n.jpg', '9b783a282298b173f0bfe2033b03f345dfd6a9f2.jpg', 'image', 'uploads/images/9b783a282298b173f0bfe2033b03f345dfd6a9f2.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:00:58', '2016-08-13 06:00:58'),
(103, '12049305_884275811608268_4318518749345970895_n.jpg', 'f73c56a000f6e0a2cb636ea329b3c9585d6bac5e.jpg', 'image', 'uploads/images/f73c56a000f6e0a2cb636ea329b3c9585d6bac5e.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:01', '2016-08-13 06:01:01'),
(104, '12043145_883263648376151_1735650410167355937_n.jpg', 'e7b258ba70b4c42865f034fee58f45868a5ad01c.jpg', 'image', 'uploads/images/e7b258ba70b4c42865f034fee58f45868a5ad01c.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:03', '2016-08-13 06:01:03'),
(105, '12063676_964576823578166_8377145958808684018_n.jpg', 'd2725753c552fbd00a16da3199d63b46f20533dc.jpg', 'image', 'uploads/images/d2725753c552fbd00a16da3199d63b46f20533dc.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:10', '2016-08-13 06:01:10'),
(106, '12063888_883263638376152_9166946041235809723_n.jpg', 'c929b1031fb51081f2158d4dd365439c70b29367.jpg', 'image', 'uploads/images/c929b1031fb51081f2158d4dd365439c70b29367.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:15', '2016-08-13 06:01:15'),
(107, '12065536_893795120656337_5191115826606109174_n.jpg', '1a243df9ff3bd35e624773eb4ad39b321ca4094f.jpg', 'image', 'uploads/images/1a243df9ff3bd35e624773eb4ad39b321ca4094f.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:19', '2016-08-13 06:01:19'),
(108, '12066014_894520130583836_5837809062175406953_n.jpg', 'b7d6d5f108595f392f008f9c4644acc4e1405349.jpg', 'image', 'uploads/images/b7d6d5f108595f392f008f9c4644acc4e1405349.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:25', '2016-08-13 06:01:25'),
(109, '12190099_894837370552112_3078356593610230369_n.jpg', 'f40c65a987aaddb62ee025b25dc4de559328fbc7.jpg', 'image', 'uploads/images/f40c65a987aaddb62ee025b25dc4de559328fbc7.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:33', '2016-08-13 06:01:33'),
(110, '12185121_895967153772467_7002126959648993541_o.jpg', '7764d2165022080f61572e13d524a057bc1f126a.jpg', 'image', 'uploads/images/7764d2165022080f61572e13d524a057bc1f126a.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:38', '2016-08-13 06:01:38'),
(111, '12193436_894837343885448_2621243792922064207_n.jpg', '599d1e8d1dc5765048e1e9c1372d3fd2f523d04c.jpg', 'image', 'uploads/images/599d1e8d1dc5765048e1e9c1372d3fd2f523d04c.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:44', '2016-08-13 06:01:44'),
(112, '12238147_898990663470116_706672516269082568_o.jpg', '453c18f4e2eb9416d2f84ec98bbde61b14295f3a.jpg', 'image', 'uploads/images/453c18f4e2eb9416d2f84ec98bbde61b14295f3a.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:45', '2016-08-13 06:01:45'),
(113, '12322460_913179752051207_2548907007538327415_o.jpg', '7a815caa62d39a09d1b8a5df24cbb22bd0083808.jpg', 'image', 'uploads/images/7a815caa62d39a09d1b8a5df24cbb22bd0083808.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:55', '2016-08-13 06:01:55'),
(114, '12374797_913179725384543_6499484215229803816_o.jpg', '94aea5181ba35213c8f3efc8e0f2016cbcc9e70a.jpg', 'image', 'uploads/images/94aea5181ba35213c8f3efc8e0f2016cbcc9e70a.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:01:56', '2016-08-13 06:01:56'),
(115, '12640370_933881316647717_2560007119448679648_o.jpg', 'cbe09d153f490523521651b3d3d4ed595ad2ff5f.jpg', 'image', 'uploads/images/cbe09d153f490523521651b3d3d4ed595ad2ff5f.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:04', '2016-08-13 06:02:04'),
(116, '12512246_964576806911501_4550066977659348311_n.jpg', 'afc613f488669c715d2a426f587b0166e665a662.jpg', 'image', 'uploads/images/afc613f488669c715d2a426f587b0166e665a662.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:06', '2016-08-13 06:02:06'),
(117, '12804683_947512531951262_8585336193363022823_n.jpg', '0ecd8506ad611f31d2400f872d3dca5b41e05e71.jpg', 'image', 'uploads/images/0ecd8506ad611f31d2400f872d3dca5b41e05e71.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:08', '2016-08-13 06:02:08'),
(118, '12888610_966149183420930_5138187558943477749_o.jpg', 'c27150c63bb74ca860b101629370f48f9d4a560e.jpg', 'image', 'uploads/images/c27150c63bb74ca860b101629370f48f9d4a560e.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:17', '2016-08-13 06:02:17'),
(119, '13179440_990584184310763_565182083623735364_n.jpg', '7878bee28a66ad2e6abefb5dd23e0e4afb93d72e.jpg', 'image', 'uploads/images/7878bee28a66ad2e6abefb5dd23e0e4afb93d72e.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:31', '2016-08-13 06:02:31'),
(120, '12938141_966149043420944_6505359821483814947_n.jpg', '5be02ad094125b12c3e344ac9da061fddc796eac.jpg', 'image', 'uploads/images/5be02ad094125b12c3e344ac9da061fddc796eac.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:36', '2016-08-13 06:02:36'),
(121, '13221507_990584397644075_5423984167799318474_n.jpg', 'edfc3331482779c5c77d735ca44572d7e8d37ca5.jpg', 'image', 'uploads/images/edfc3331482779c5c77d735ca44572d7e8d37ca5.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:40', '2016-08-13 06:02:40'),
(122, '13230111_990565027646012_5822779939330185647_n.jpg', '78fc9e9ac383baebe2132e7b86e7689ce7da43bc.jpg', 'image', 'uploads/images/78fc9e9ac383baebe2132e7b86e7689ce7da43bc.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:42', '2016-08-13 06:02:42'),
(123, '13230190_990584064310775_7824544680761592748_n.jpg', '3d574046b36cef2cecadd32d7fc40ca1b62df595.jpg', 'image', 'uploads/images/3d574046b36cef2cecadd32d7fc40ca1b62df595.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:47', '2016-08-13 06:02:47'),
(124, '13239922_990536497648865_1812167668956174195_n.jpg', '98893428af17a27e801b590b9f40c81e5a2826e1.jpg', 'image', 'uploads/images/98893428af17a27e801b590b9f40c81e5a2826e1.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:48', '2016-08-13 06:02:48'),
(125, 'IMG-20160323-WA0012.jpg', '5a419356c78ffa578fc2c99c1b00c0d2ccb5f92c.jpg', 'image', 'uploads/images/5a419356c78ffa578fc2c99c1b00c0d2ccb5f92c.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:54', '2016-08-13 06:02:54'),
(126, '13245236_990564960979352_3963264700770920849_n.jpg', 'e30feb5e19546b96236dc479b764ac682a235ef3.jpg', 'image', 'uploads/images/e30feb5e19546b96236dc479b764ac682a235ef3.jpg', NULL, NULL, 0, 2, NULL, '2016-08-13 06:02:56', '2016-08-13 06:02:56'),
(127, 'iStock_88229731_XXXLARGE.jpg', 'd2e282fbc66f99a7e12c751db99841ca97c017dc.jpg', 'image', 'uploads/images/d2e282fbc66f99a7e12c751db99841ca97c017dc.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 05:08:12', '2016-08-14 05:08:12'),
(128, '3.jpg', '667690c260a8cd12cbbe7d49968464a0969632ed.jpg', 'image', 'uploads/images/667690c260a8cd12cbbe7d49968464a0969632ed.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 07:49:15', '2016-08-14 07:49:15'),
(129, '4.jpg', '2a6e98124718eddcf027319f138f75c4db2de252.jpg', 'image', 'uploads/images/2a6e98124718eddcf027319f138f75c4db2de252.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 07:56:55', '2016-08-14 07:56:55'),
(130, '2.jpg', '3ea5eda55c312e2de73b4bb34ecd502b8927d535.jpg', 'image', 'uploads/images/3ea5eda55c312e2de73b4bb34ecd502b8927d535.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 08:52:41', '2016-08-14 08:52:41'),
(131, '6.jpg', 'b0662b8250c69a3fb33b25ab42b883ae9e317fa1.jpg', 'image', 'uploads/images/b0662b8250c69a3fb33b25ab42b883ae9e317fa1.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 10:01:43', '2016-08-14 10:01:43'),
(132, '1.jpg', 'd404eca76951dd30d49baa3e49bf94d420149b09.jpg', 'image', 'uploads/images/d404eca76951dd30d49baa3e49bf94d420149b09.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 10:30:44', '2016-08-14 10:30:44'),
(133, '5.jpg', '6b0afe9def8809314440d2f1319873f40241470a.jpg', 'image', 'uploads/images/6b0afe9def8809314440d2f1319873f40241470a.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 10:30:51', '2016-08-14 10:30:51'),
(134, '7.jpg', '39a6ecb9251c48885f1376343c2e1b54ebcf4ec6.jpg', 'image', 'uploads/images/39a6ecb9251c48885f1376343c2e1b54ebcf4ec6.jpg', NULL, NULL, 0, 2, NULL, '2016-08-14 10:30:55', '2016-08-14 10:30:55'),
(135, 'EQ-i_2.0_Client_Report.pdf', 'ae8e1847102b84ddd64c892665e7bf34c87a09f1.pdf', 'pdf', 'uploads/documents/ae8e1847102b84ddd64c892665e7bf34c87a09f1.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:45:03', '2016-08-19 02:45:03'),
(138, 'EQ-i_2.0_Coach_Report.pdf', '43436d716b35f0d1aa2ee11e836bbcae0e306c07.pdf', 'pdf', 'uploads/documents/43436d716b35f0d1aa2ee11e836bbcae0e306c07.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:45:36', '2016-08-19 02:45:36'),
(140, 'MBTI_Step_I_Profile.pdf', 'b5905787ddcf27aa75d77ac0480ce3d372995fd0.pdf', 'pdf', 'uploads/documents/b5905787ddcf27aa75d77ac0480ce3d372995fd0.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:45:49', '2016-08-19 02:45:49'),
(141, 'MBTI_Step_I_Interpretive_Report.pdf', '4fb6819a7cc32f153c701ebf4eb806daa192b355.pdf', 'pdf', 'uploads/documents/4fb6819a7cc32f153c701ebf4eb806daa192b355.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:45:53', '2016-08-19 02:45:53'),
(142, 'MBTI_Step_II_Profile.pdf', 'a2f662e6ba5918504b94460164166f0fb079d54e.pdf', 'pdf', 'uploads/documents/a2f662e6ba5918504b94460164166f0fb079d54e.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:04', '2016-08-19 02:46:04'),
(143, 'MBTI_Step_II_Interpretive_Report.pdf', 'ce1a1c973cee0bc34ad870ce97c1f1010281aad9.pdf', 'pdf', 'uploads/documents/ce1a1c973cee0bc34ad870ce97c1f1010281aad9.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:04', '2016-08-19 02:46:04'),
(146, 'SC_Administrative_Strengths_Environment_Fit_Report.pdf', '50f9aebe9cb6fa835801cce571ac2219d136a419.pdf', 'pdf', 'uploads/documents/50f9aebe9cb6fa835801cce571ac2219d136a419.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:12', '2016-08-19 02:46:12'),
(147, 'SC_Administrative_Strengths_Report.pdf', '6438378de031e81fb82b88b7c39bd49db302cb93.pdf', 'pdf', 'uploads/documents/6438378de031e81fb82b88b7c39bd49db302cb93.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:17', '2016-08-19 02:46:17'),
(148, 'SC_Commercial_Strengths_Environment_Fit_Report.pdf', 'a0eedb80aacd3461ed3490faaf93b3353d33a7bf.pdf', 'pdf', 'uploads/documents/a0eedb80aacd3461ed3490faaf93b3353d33a7bf.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:18', '2016-08-19 02:46:18'),
(149, 'SC_Commercial_Strengths_Report.pdf', '43f866f11a7e72d0a01b47022f51b20ae736551c.pdf', 'pdf', 'uploads/documents/43f866f11a7e72d0a01b47022f51b20ae736551c.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:20', '2016-08-19 02:46:20'),
(156, 'SW_FS_Line_Manager_Report.pdf', 'cd340a4588178f97d6f63084a99d745faf1367db.pdf', 'pdf', 'uploads/documents/cd340a4588178f97d6f63084a99d745faf1367db.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:36', '2016-08-19 02:46:36'),
(157, 'SW_FS_Expert_Report.pdf', '9e470d5076ea8f8f6146d28c1731df9d5f427005.pdf', 'pdf', 'uploads/documents/9e470d5076ea8f8f6146d28c1731df9d5f427005.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:36', '2016-08-19 02:46:36'),
(158, 'SW_FS_Personal_Report.pdf', 'f966c9df87dc96cab1d2990fb0fda37be41ecc35.pdf', 'pdf', 'uploads/documents/f966c9df87dc96cab1d2990fb0fda37be41ecc35.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:39', '2016-08-19 02:46:39'),
(159, 'SW_PS_Expert_Report.pdf', '61df0c94dab75dfaa389206df9955b792c7a52b8.pdf', 'pdf', 'uploads/documents/61df0c94dab75dfaa389206df9955b792c7a52b8.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:47', '2016-08-19 02:46:47'),
(160, 'SW_PS_Line_Manager_Report.pdf', '724d146ea9a197fb3677400cf7af1505d7a11dd1.pdf', 'pdf', 'uploads/documents/724d146ea9a197fb3677400cf7af1505d7a11dd1.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:49', '2016-08-19 02:46:49'),
(161, 'SW_WS_Behavioural_Profile.pdf', '45b98eac07f7f328975aefa5dc4b8f2189e8c98a.pdf', 'pdf', 'uploads/documents/45b98eac07f7f328975aefa5dc4b8f2189e8c98a.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:51', '2016-08-19 02:46:51'),
(162, 'SW_WS_Environmental_Fit_Report.pdf', 'a2ce747fd4aa6d2399415ef8f207caf8053221c4.pdf', 'pdf', 'uploads/documents/a2ce747fd4aa6d2399415ef8f207caf8053221c4.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:53', '2016-08-19 02:46:53'),
(163, 'SW_PS_Personal_Report.pdf', 'f91008fe5ae12e6f47080b19ce0c1030bbac2ae8.pdf', 'pdf', 'uploads/documents/f91008fe5ae12e6f47080b19ce0c1030bbac2ae8.pdf', NULL, NULL, 0, 2, NULL, '2016-08-19 02:46:55', '2016-08-19 02:46:55'),
(164, 'GettyImages-470724638.jpg', '3d0a77ea85be85143aa730f0e0c71570e3e41fb5.jpg', 'image', 'uploads/images/3d0a77ea85be85143aa730f0e0c71570e3e41fb5.jpg', NULL, NULL, 0, 3, NULL, '2016-08-27 10:06:50', '2016-08-27 10:06:50'),
(166, 'BPS.pdf', '08be263178d53863022b2a49c26bc5aed75f7190.pdf', 'pdf', 'uploads/documents/08be263178d53863022b2a49c26bc5aed75f7190.pdf', NULL, NULL, 0, 3, NULL, '2016-08-27 10:30:13', '2016-08-27 10:30:13'),
(167, 'Jack_Doe_EQ360_Feedback_Standard_Coach_-_update_Nov_15_RF.pdf', '3ad1f87d749d380d14b9d20cfca9bacb73568302.pdf', 'pdf', 'uploads/documents/3ad1f87d749d380d14b9d20cfca9bacb73568302.pdf', NULL, NULL, 0, 3, NULL, '2016-08-28 04:04:28', '2016-08-28 04:04:28'),
(169, 'GL png.png', '5d319d59417fabb745666614ebe04929401a5dd8.png', 'image', 'uploads/images/5d319d59417fabb745666614ebe04929401a5dd8.png', NULL, NULL, 0, 3, NULL, '2016-08-28 05:50:02', '2016-08-28 05:50:02'),
(170, 'MBTI Forum Logo JPEG.JPG', '6c59be6fedfb3eed99fa1d70b6b759b91e033839.JPG', 'image', 'uploads/images/6c59be6fedfb3eed99fa1d70b6b759b91e033839.JPG', NULL, NULL, 0, 3, NULL, '2016-08-29 02:29:41', '2016-08-29 02:29:41'),
(171, 'AW HR Summit Pic.jpg', 'c9edee720b490a250ffea4db82b46c8ee09e47e7.jpg', 'image', 'uploads/images/c9edee720b490a250ffea4db82b46c8ee09e47e7.jpg', NULL, NULL, 0, 3, NULL, '2016-08-29 04:00:07', '2016-08-29 04:00:07'),
(172, 'SS HR Summit Pic.jpg', 'c845b8f4ce80933e0ba9c5c9d545ce8860b6e092.jpg', 'image', 'uploads/images/c845b8f4ce80933e0ba9c5c9d545ce8860b6e092.jpg', NULL, NULL, 0, 3, NULL, '2016-08-29 04:00:12', '2016-08-29 04:00:12'),
(177, 'Capture.JPG', '88900d1b1031ca30aaaff7da77e7b7b99061297d.JPG', 'image', 'uploads/images/88900d1b1031ca30aaaff7da77e7b7b99061297d.JPG', NULL, NULL, 0, 3, NULL, '2016-08-30 05:20:27', '2016-08-30 05:20:27'),
(178, 'AS banner.jpg', 'ae274f9a3849339f94333691e506776fdd2e054b.jpg', 'image', 'uploads/images/ae274f9a3849339f94333691e506776fdd2e054b.jpg', NULL, NULL, 0, 3, NULL, '2016-08-30 05:21:24', '2016-08-30 05:21:24'),
(179, 'coaching banner.jpg', '5ab31bd44b1c59a47f0df130f009bf907364748e.jpg', 'image', 'uploads/images/5ab31bd44b1c59a47f0df130f009bf907364748e.jpg', NULL, NULL, 0, 3, NULL, '2016-08-30 05:22:46', '2016-08-30 05:22:46'),
(180, 'Strong banner.jpg', '0477a53d5c9c11533ef97358e430c2a5f5c3ddc3.jpg', 'image', 'uploads/images/0477a53d5c9c11533ef97358e430c2a5f5c3ddc3.jpg', NULL, NULL, 0, 3, NULL, '2016-08-30 05:25:00', '2016-08-30 05:25:00'),
(181, 'BPS banner.jpg', '13146619a71c729131b55a984f4f2c84d71560e4.jpg', 'image', 'uploads/images/13146619a71c729131b55a984f4f2c84d71560e4.jpg', NULL, NULL, 0, 3, NULL, '2016-08-30 05:25:43', '2016-08-30 05:25:43'),
(184, 'BPS A Banner.jpg', 'bcc8b91987f86c2663414f095181bd6e1c20ec2c.jpg', 'image', 'uploads/images/bcc8b91987f86c2663414f095181bd6e1c20ec2c.jpg', NULL, NULL, 0, 3, NULL, '2016-08-30 05:39:33', '2016-08-30 05:39:33'),
(185, 'slide01.png', '05c216d42035b6ba6a05cad47db4d01454caca4b.png', 'image', 'uploads/images/05c216d42035b6ba6a05cad47db4d01454caca4b.png', NULL, NULL, 0, 3, NULL, '2016-08-30 08:33:22', '2016-08-30 08:33:22'),
(186, 'ipcd pic.png', '23d38140e32b6d48ddc809dacbdb1b93b74a1df3.png', 'image', 'uploads/images/23d38140e32b6d48ddc809dacbdb1b93b74a1df3.png', NULL, NULL, 0, 3, NULL, '2016-08-30 08:37:10', '2016-08-30 08:37:10'),
(187, 'Day 1 HR Summit', 'cc8beb982e60aac8c6729b6844dcc7c13d209983.png', 'image', 'uploads/images/cc8beb982e60aac8c6729b6844dcc7c13d209983.png', NULL, NULL, 0, 3, 3, '2016-08-30 08:52:12', '2016-08-30 08:53:56'),
(188, 'HRSE16_social_media_banner2-exhibitor.jpg', 'cc7858bdb12b56efdb8e302fd160c67ab16b4080.jpg', 'image', 'uploads/images/cc7858bdb12b56efdb8e302fd160c67ab16b4080.jpg', NULL, NULL, 0, 3, NULL, '2016-08-30 08:59:42', '2016-08-30 08:59:42'),
(190, 'MBTI banner.jpg', 'f9af2e1c20eff25a78082713a5e9dbe0a48cab37.jpg', 'image', 'uploads/images/f9af2e1c20eff25a78082713a5e9dbe0a48cab37.jpg', NULL, NULL, 0, 3, NULL, '2016-09-06 04:36:26', '2016-09-06 04:36:26'),
(191, 'FIRO-B-220160 - FIRO B Profile.pdf', '40016b2f57f60f77eb621d2c217b88694aee4322.pdf', 'pdf', 'uploads/documents/40016b2f57f60f77eb621d2c217b88694aee4322.pdf', NULL, NULL, 0, 3, NULL, '2016-09-06 04:58:39', '2016-09-06 04:58:39'),
(192, 'tn-600-30e0352471ef2be2f92917d95d53c85e25ddc9ef.jpg', '4072c967404475300708324c80e93f222e7062f1.jpg', 'image', 'uploads/images/4072c967404475300708324c80e93f222e7062f1.jpg', NULL, NULL, 0, 3, NULL, '2016-09-06 05:32:10', '2016-09-06 05:32:10'),
(195, '2017 Accreditation Calendar.pdf', 'c58b576b3aeff33574e989df5742e6f8ac8297ea.pdf', 'pdf', 'uploads/documents/c58b576b3aeff33574e989df5742e6f8ac8297ea.pdf', NULL, NULL, 0, 3, 3, '2016-09-07 09:25:55', '2016-12-12 18:29:14'),
(197, 'MBTI.png', '79176094856874398328dfd25f5a1a1fcd4d230c.png', 'image', 'uploads/images/79176094856874398328dfd25f5a1a1fcd4d230c.png', NULL, NULL, 0, 3, NULL, '2016-09-08 08:26:07', '2016-09-08 08:26:07'),
(198, 'Firo-Business-Logo.png', 'a408a5382f2cd9b3ae401fef734f2fb3f172c1d5.png', 'image', 'uploads/images/a408a5382f2cd9b3ae401fef734f2fb3f172c1d5.png', NULL, NULL, 0, 3, NULL, '2016-09-08 08:26:41', '2016-09-08 08:26:41'),
(199, 'Strong-Interest-Inventory-Logo.png', 'd8fffd3062e6cabda6faee2c4006dea9d063a915.png', 'image', 'uploads/images/d8fffd3062e6cabda6faee2c4006dea9d063a915.png', NULL, NULL, 0, 3, NULL, '2016-09-08 08:27:37', '2016-09-08 08:27:37'),
(200, 'EQ360-logo-w-tag.jpg', '38773a6d93e72738713d6724b753063c172f806e.jpg', 'image', 'uploads/images/38773a6d93e72738713d6724b753063c172f806e.jpg', NULL, NULL, 0, 3, NULL, '2016-09-08 08:28:00', '2016-09-08 08:28:00'),
(201, 'EQi-2.0-logo_w-tag.jpg', 'f60539ac8eb5b3db08bf21a58e8288d0f0bd5640.jpg', 'image', 'uploads/images/f60539ac8eb5b3db08bf21a58e8288d0f0bd5640.jpg', NULL, NULL, 0, 3, NULL, '2016-09-08 08:28:00', '2016-09-08 08:28:00'),
(202, 'Hogan_logo.jpg', '95e8a1572ba7a7b5afc3bcac5dc441681a4ff6e0.jpg', 'image', 'uploads/images/95e8a1572ba7a7b5afc3bcac5dc441681a4ff6e0.jpg', NULL, NULL, 0, 3, NULL, '2016-09-08 08:29:37', '2016-09-08 08:29:37'),
(203, 'Accredited_Award_CT.jpg', 'cf6301404d65174198bd1b01f5f4d6bbc20d68bb.jpg', 'image', 'uploads/images/cf6301404d65174198bd1b01f5f4d6bbc20d68bb.jpg', NULL, NULL, 0, 3, NULL, '2016-09-08 08:30:00', '2016-09-08 08:30:00'),
(205, 'Pearson-Logo.jpg', '0c7bf33fe8676a4c9884918cf84939fda9275017.jpg', 'image', 'uploads/images/0c7bf33fe8676a4c9884918cf84939fda9275017.jpg', NULL, NULL, 0, 3, NULL, '2016-09-08 08:31:11', '2016-09-08 08:31:11'),
(207, 'innovative360_logo.png', 'c9e218597b004492d90c1cdcc646df50a0557094.png', 'image', 'uploads/images/c9e218597b004492d90c1cdcc646df50a0557094.png', NULL, NULL, 0, 3, NULL, '2016-09-08 09:09:18', '2016-09-08 09:09:18'),
(211, 'banner 1.jpg', 'ffb23c858ad65316e0738450d428885999f12d2e.jpg', 'image', 'uploads/images/ffb23c858ad65316e0738450d428885999f12d2e.jpg', NULL, NULL, 0, 3, NULL, '2016-09-14 04:34:02', '2016-09-14 04:34:02'),
(212, '5 flipped.jpg', '72ebd8e7f8d2d05a33454e299ff88a9508c818f9.jpg', 'image', 'uploads/images/72ebd8e7f8d2d05a33454e299ff88a9508c818f9.jpg', NULL, NULL, 0, 3, NULL, '2016-09-14 04:43:56', '2016-09-14 04:43:56'),
(217, 'IHS 2016 Calendar', '9d4e7a397d29289b5e9be88487a4a2959e8fb306.pdf', 'pdf', 'uploads/documents/9d4e7a397d29289b5e9be88487a4a2959e8fb306.pdf', NULL, NULL, 0, 3, 3, '2016-10-04 17:57:57', '2016-10-04 17:58:21'),
(219, 'NEO_badge_2.png', 'cd50be1857a81567021e76e063cb16a7f7d54603.png', 'image', 'uploads/images/cd50be1857a81567021e76e063cb16a7f7d54603.png', NULL, NULL, 0, 3, NULL, '2016-10-19 17:53:55', '2016-10-19 17:53:55'),
(220, 'ECW NEO PI-3 Personal Insight Report (Managerial and Professional Norm) (2).pdf', '7457aa91e6d7372eaf0a3b2d17f1517d0bd36a0a.pdf', 'pdf', 'uploads/documents/7457aa91e6d7372eaf0a3b2d17f1517d0bd36a0a.pdf', NULL, NULL, 0, 3, NULL, '2016-10-20 17:29:00', '2016-10-20 17:29:00'),
(223, 'wb 1584x856 Winners SME Awards2016.jpg', '78f7bcfa4eb819094132ec5601f23664fa3c426d.jpg', 'image', 'uploads/images/78f7bcfa4eb819094132ec5601f23664fa3c426d.jpg', NULL, NULL, 0, 3, NULL, '2016-10-31 11:46:28', '2016-10-31 11:46:28'),
(224, 'Picture3.png', '8493824cdd8d36c645317e423b1dedfabd335a23.png', 'image', 'uploads/images/8493824cdd8d36c645317e423b1dedfabd335a23.png', NULL, NULL, 0, 3, NULL, '2016-10-31 11:46:44', '2016-10-31 11:46:44'),
(225, '20161027_094457.jpg', '7b78cf0af16a3f11c662abdd5cef2a6c4ca19fc9.jpg', 'image', 'uploads/images/7b78cf0af16a3f11c662abdd5cef2a6c4ca19fc9.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:29', '2016-11-03 12:54:29'),
(226, 'IMG-20161026-WA0081.jpg', 'd5e96b5e31503bf554f4d6332f6ee3931f606960.jpg', 'image', 'uploads/images/d5e96b5e31503bf554f4d6332f6ee3931f606960.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:31', '2016-11-03 12:54:31'),
(227, 'InnovativeHRsolutions.jpg', '780c56a05680896c99b36e5227c74bc93847d10b.jpg', 'image', 'uploads/images/780c56a05680896c99b36e5227c74bc93847d10b.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:33', '2016-11-03 12:54:33'),
(228, 'SME Awards-1.jpg', '770562fc38a5dbc81fd9bbe73b9f7cefd67e3ac1.jpg', 'image', 'uploads/images/770562fc38a5dbc81fd9bbe73b9f7cefd67e3ac1.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:35', '2016-11-03 12:54:35'),
(229, 'SME Awards-144.jpg', '75e960128aed7bc408d3466bebd1acbb14cf7477.jpg', 'image', 'uploads/images/75e960128aed7bc408d3466bebd1acbb14cf7477.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:49', '2016-11-03 12:54:49'),
(230, 'SME Awards-4.jpg', '5188d76fb3a55ca395b29e549eea5f9642677e78.jpg', 'image', 'uploads/images/5188d76fb3a55ca395b29e549eea5f9642677e78.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:49', '2016-11-03 12:54:49'),
(231, 'SME Awards-2.jpg', '6a1fa6d84cae19898a00b80e3b785aacc76b7497.jpg', 'image', 'uploads/images/6a1fa6d84cae19898a00b80e3b785aacc76b7497.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:51', '2016-11-03 12:54:51'),
(232, 'IMG-20161026-WA0066.jpg', '2f5afec5d1fd2a27b5ef8c91c9ec6e7aa7d645ad.jpg', 'image', 'uploads/images/2f5afec5d1fd2a27b5ef8c91c9ec6e7aa7d645ad.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:54:55', '2016-11-03 12:54:55'),
(233, 'Winners_Walid.jpg', '7309d341f0193d4a03f98e85ae24ba403f4f1d16.jpg', 'image', 'uploads/images/7309d341f0193d4a03f98e85ae24ba403f4f1d16.jpg', NULL, NULL, 0, 3, NULL, '2016-11-03 12:55:03', '2016-11-03 12:55:03'),
(234, 'NEO PI-3 Technical Sample Report (Managerial and Professional Norm).pdf', '2b3b6679fe6097328e269c1864c9c91938d6740c.pdf', 'pdf', 'uploads/documents/2b3b6679fe6097328e269c1864c9c91938d6740c.pdf', NULL, NULL, 0, 3, NULL, '2016-11-03 13:23:15', '2016-11-03 13:23:15'),
(235, 'NEO PI-3 Primary Colours Leadership Sample Report.pdf', '1cd75bd05f14057d02b144f9a9731a3f5c318845.pdf', 'pdf', 'uploads/documents/1cd75bd05f14057d02b144f9a9731a3f5c318845.pdf', NULL, NULL, 0, 3, NULL, '2016-11-03 13:23:15', '2016-11-03 13:23:15'),
(236, 'IHS Corporate Brochure - Arabic.pdf', '68c28c65257a0c81aa180166002d6baccbcf9128.pdf', 'pdf', 'uploads/documents/68c28c65257a0c81aa180166002d6baccbcf9128.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:33:51', '2016-11-07 16:33:51'),
(237, 'IHS Corporate Brochure - English.pdf', '4553e9b37f36934a8eda2805f1d3edb98b8dfe7d.pdf', 'pdf', 'uploads/documents/4553e9b37f36934a8eda2805f1d3edb98b8dfe7d.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:33:51', '2016-11-07 16:33:51'),
(238, 'EQi 2.0 & EQ360.pdf', '3c07ab7f5e3e802781d8afe3ec90cfac939b1147.pdf', 'pdf', 'uploads/documents/3c07ab7f5e3e802781d8afe3ec90cfac939b1147.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:16', '2016-11-07 16:43:16'),
(239, 'Centre Manager.pdf', 'c24f50381bbcd402e567ec7ce4728b82499c6a2f.pdf', 'pdf', 'uploads/documents/c24f50381bbcd402e567ec7ce4728b82499c6a2f.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:16', '2016-11-07 16:43:16'),
(240, 'MBTI Accreditation.pdf', '5b89bb26e048d735d5d5de8f09068ec31253c199.pdf', 'pdf', 'uploads/documents/5b89bb26e048d735d5d5de8f09068ec31253c199.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:18', '2016-11-07 16:43:18'),
(241, 'FIRO-Business.pdf', 'bd1608338fad89fe6199c172e0b2b39182458a7e.pdf', 'pdf', 'uploads/documents/bd1608338fad89fe6199c172e0b2b39182458a7e.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:19', '2016-11-07 16:43:19'),
(242, 'Strong Interest Inventory.pdf', '573a85b5172083bb9c6ca28eee4d005c6df7e8b8.pdf', 'pdf', 'uploads/documents/573a85b5172083bb9c6ca28eee4d005c6df7e8b8.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:20', '2016-11-07 16:43:20'),
(243, 'Accredited Award in Coach Training (AACT).pdf', 'c0dd2fc0e475f4a9533c3069904058ee4e529dea.pdf', 'pdf', 'uploads/documents/c0dd2fc0e475f4a9533c3069904058ee4e529dea.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:20', '2016-11-07 16:43:20'),
(244, 'BPS Level A & P.pdf', '77a4dcbcf3e00bd724d4099b2a50959eab9322c8.pdf', 'pdf', 'uploads/documents/77a4dcbcf3e00bd724d4099b2a50959eab9322c8.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:21', '2016-11-07 16:43:21'),
(245, 'Assessor Skills.pdf', '74c370d58b803726f249923259df0c9995784be8.pdf', 'pdf', 'uploads/documents/74c370d58b803726f249923259df0c9995784be8.pdf', NULL, NULL, 0, 3, NULL, '2016-11-07 16:43:21', '2016-11-07 16:43:21'),
(246, '2017 Accreditation Calendar.pdf', '9ddea65d502b4908e4b81eeb57f5596d4ac9d073.pdf', 'pdf', 'uploads/documents/9ddea65d502b4908e4b81eeb57f5596d4ac9d073.pdf', NULL, NULL, 0, 3, NULL, '2016-12-12 18:28:40', '2016-12-12 18:28:40'),
(248, '2017 Accreditation Calendar.pdf', 'eb3320b9bc6b5c9b5c0aafc5c797e87d04512197.pdf', 'pdf', 'uploads/documents/eb3320b9bc6b5c9b5c0aafc5c797e87d04512197.pdf', NULL, NULL, 0, 3, NULL, '2016-12-29 18:25:16', '2016-12-29 18:25:16'),
(249, 'AAGroup shot low res.jpg', '125dd67c57cd5b11a677be7745ddcae4f257fd58.jpg', 'image', 'uploads/images/125dd67c57cd5b11a677be7745ddcae4f257fd58.jpg', 'IHS Team with Lean In Pakistan 2017', NULL, 0, 3, 3, '2017-01-22 17:34:21', '2017-01-22 18:18:32'),
(250, 'Speakers on stage.jpg', 'b59e6b8ef49769e546a3439c880c04a107ac3f63.jpg', 'image', 'uploads/images/b59e6b8ef49769e546a3439c880c04a107ac3f63.jpg', NULL, NULL, 0, 3, NULL, '2017-01-22 17:34:57', '2017-01-22 17:34:57'),
(251, 'IMG_3966.JPG', '56c698147659e21f7d93b26ece35d7d8c245e037.JPG', 'image', 'uploads/images/56c698147659e21f7d93b26ece35d7d8c245e037.JPG', NULL, NULL, 0, 3, NULL, '2017-01-22 17:35:31', '2017-01-22 17:35:31'),
(252, 'Sharan Gohel.JPG', '54bb4f63ffe4860a91a8e9b2c582fcc0c3130b15.JPG', 'image', 'uploads/images/54bb4f63ffe4860a91a8e9b2c582fcc0c3130b15.JPG', NULL, NULL, 0, 3, NULL, '2017-01-22 17:35:40', '2017-01-22 17:35:40'),
(253, 'IMG_3956.JPG', '15ced09feccf2339de8c36a695fce7dd0685aca9.JPG', 'image', 'uploads/images/15ced09feccf2339de8c36a695fce7dd0685aca9.JPG', NULL, NULL, 1, 3, NULL, '2017-01-22 17:35:42', '2017-06-21 06:13:23'),
(254, 'sheryl.jpg', '062f088bc15d68cfb0a79cf56db1dcad57698d7c.jpg', 'image', 'uploads/images/062f088bc15d68cfb0a79cf56db1dcad57698d7c.jpg', NULL, NULL, 1, 3, NULL, '2017-01-22 17:35:44', '2017-06-21 06:13:19'),
(255, '2017-01-18_1438.png', '542156fc8e837d9f63b12352d1bbb28ba9697aa6.png', 'image', 'uploads/images/542156fc8e837d9f63b12352d1bbb28ba9697aa6.png', NULL, NULL, 1, 3, NULL, '2017-01-22 17:35:48', '2017-06-21 06:13:15'),
(256, 'Amanda White.JPG', '1842c32463fa9ee0e2f9b229ee0db4f3b704c4c5.JPG', 'image', 'uploads/images/1842c32463fa9ee0e2f9b229ee0db4f3b704c4c5.JPG', NULL, NULL, 1, 3, NULL, '2017-01-22 17:35:55', '2017-06-21 06:13:12'),
(257, 'Speakers on stage.jpg', '40c0ad4802aeccab3e1c7105fda7ca722887eadb.jpg', 'image', 'uploads/images/40c0ad4802aeccab3e1c7105fda7ca722887eadb.jpg', NULL, NULL, 1, 3, NULL, '2017-01-22 17:36:44', '2017-06-21 06:13:05'),
(258, 'Sharan Susie Amanda Humaira & Liesel.JPG', '21979e0ccf28bd1f7c5474c9c3c991c4dd5e9cfd.JPG', 'image', 'uploads/images/21979e0ccf28bd1f7c5474c9c3c991c4dd5e9cfd.JPG', NULL, NULL, 1, 3, NULL, '2017-01-22 17:37:13', '2017-06-21 06:13:00'),
(259, 'Sharan Liessel Humaira & Amanda.JPG', '3293df863f3cb8687fe3133f4f2fcc9f800d0be8.JPG', 'image', 'uploads/images/3293df863f3cb8687fe3133f4f2fcc9f800d0be8.JPG', NULL, NULL, 1, 3, NULL, '2017-01-22 17:37:28', '2017-06-21 06:12:58'),
(260, 'AAGroup shot low res.jpg', '5a4ddc9a43c9e0f02e4ca8ddd67fc3dba952055e.jpg', 'image', 'uploads/images/5a4ddc9a43c9e0f02e4ca8ddd67fc3dba952055e.jpg', NULL, NULL, 1, 3, NULL, '2017-01-22 18:29:20', '2017-06-21 06:12:49'),
(261, '2017-01-22_1651.png', '1cf836f331c8ffc812a75350323ba988a737a869.png', 'image', 'uploads/images/1cf836f331c8ffc812a75350323ba988a737a869.png', NULL, NULL, 1, 3, NULL, '2017-01-22 18:42:39', '2017-06-21 06:12:45'),
(262, '2017-01-22_1709ff.png', '59d4ba33e9de1b013115ea404cde6b90572f5bc2.png', 'image', 'uploads/images/59d4ba33e9de1b013115ea404cde6b90572f5bc2.png', NULL, NULL, 1, 3, NULL, '2017-01-22 19:00:15', '2017-06-21 06:12:42'),
(263, 'Accredited-Partener-of-Saville-Assessment.png', '31b5f239562b84eefdc5b19b4a52ef2a76be3a8e.png', 'image', 'uploads/images/31b5f239562b84eefdc5b19b4a52ef2a76be3a8e.png', NULL, NULL, 1, 3, NULL, '2017-02-01 11:57:52', '2017-06-21 06:12:38'),
(264, 'Jo Wilson, Focus Styles Personal Report V4, INT.Pdf', '286485042a00a471519cc6c57d5e1dd8487f84ac.Pdf', 'pdf', 'uploads/documents/286485042a00a471519cc6c57d5e1dd8487f84ac.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:06:55', '2017-02-02 14:06:55'),
(265, 'Jo-Wilson, Focus Styles Line Manager Report V4, INT.Pdf', '7dfa2651e52043d52e39e2210bdba0dac4ef1b07.Pdf', 'pdf', 'uploads/documents/7dfa2651e52043d52e39e2210bdba0dac4ef1b07.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:06:57', '2017-02-02 14:06:57'),
(266, 'Jo Wilson, Focus Styles Onboarding Report, INT.Pdf', '67f5ee4b85d33cea9d7bc9634c36e7153013a97d.Pdf', 'pdf', 'uploads/documents/67f5ee4b85d33cea9d7bc9634c36e7153013a97d.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:06:58', '2017-02-02 14:06:58'),
(267, 'Chris Park, Professional Styles Onboarding Report, INT.Pdf', 'ee76b90f03739fb0f10f4453d104120b7b16f427.Pdf', 'pdf', 'uploads/documents/ee76b90f03739fb0f10f4453d104120b7b16f427.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:06:59', '2017-02-02 14:06:59'),
(268, 'Jo Wilson, Focus Styles Expert Report V4, INT.Pdf', '2047a8e82a089c6fd536405530881ff305557832.Pdf', 'pdf', 'uploads/documents/2047a8e82a089c6fd536405530881ff305557832.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:07:01', '2017-02-02 14:07:01'),
(269, 'Chris Park, Professional Styles Line Manager Report V4, INT.Pdf', '16013b17fa5c6e61742763c548c52f0f9aea41af.Pdf', 'pdf', 'uploads/documents/16013b17fa5c6e61742763c548c52f0f9aea41af.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:07:03', '2017-02-02 14:07:03'),
(270, 'Chris Park, Professional Styles Line Manager Report V4, INT.Pdf', '2feb1044d869eb7a06dd0234f6047c7548f43eae.Pdf', 'pdf', 'uploads/documents/2feb1044d869eb7a06dd0234f6047c7548f43eae.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:07:06', '2017-02-02 14:07:06'),
(271, 'Chris Park, Professional Styles Personal Report V4, INT.Pdf', '836860c05221ae66354f409a2205154299b4b894.Pdf', 'pdf', 'uploads/documents/836860c05221ae66354f409a2205154299b4b894.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:07:09', '2017-02-02 14:07:09'),
(272, 'Chris Park, Professional Styles Expert Report V4, INT.Pdf', '9635ddefe8c5c42b9a8b9a354195533ba2519544.Pdf', 'pdf', 'uploads/documents/9635ddefe8c5c42b9a8b9a354195533ba2519544.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:07:11', '2017-02-02 14:07:11'),
(273, 'Chris Park, Professional Styles Work Roles Report, INT.Pdf', '1d516d03731e199aaff266c88fc39c6ddcc8ee23.Pdf', 'pdf', 'uploads/documents/1d516d03731e199aaff266c88fc39c6ddcc8ee23.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:07:14', '2017-02-02 14:07:14'),
(274, 'Chris Park, Professional Styles Leadership Impact Expert Report, INT.Pdf', '04588546764b9de796b0f83fd35ffce57d0b8ef5.Pdf', 'pdf', 'uploads/documents/04588546764b9de796b0f83fd35ffce57d0b8ef5.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:07:15', '2017-02-02 14:07:15'),
(275, '261145 Arabic MBTI Step I Form M Profile.pdf', 'c5f0bfa1b1df8d86e555af777cbd9b663568456a.pdf', 'pdf', 'uploads/documents/c5f0bfa1b1df8d86e555af777cbd9b663568456a.pdf', NULL, NULL, 0, 3, NULL, '2017-02-02 14:55:02', '2017-02-02 14:55:02'),
(276, 'Chris Park, Professional Styles Work Roles Report, INT.pdf', '24e8cc80460db46d83f56275637cc4f37fa21011.pdf', 'pdf', 'uploads/documents/24e8cc80460db46d83f56275637cc4f37fa21011.pdf', NULL, NULL, 0, 3, NULL, '2017-02-05 13:18:31', '2017-02-05 13:18:31'),
(277, 'Arabic Professional Styles Expert Report V5.Pdf', '747db1c17bd08bc19cc572cafc7b4ddc88e1e29d.Pdf', 'pdf', 'uploads/documents/747db1c17bd08bc19cc572cafc7b4ddc88e1e29d.Pdf', NULL, NULL, 0, 3, NULL, '2017-02-05 16:23:49', '2017-02-05 16:23:49'),
(278, 'MBTI Product Catalogue.pdf', '218b4656e39b3ff67a5ef000de9f78bc692191b2.pdf', 'pdf', 'uploads/documents/218b4656e39b3ff67a5ef000de9f78bc692191b2.pdf', NULL, NULL, 0, 4, NULL, '2017-05-15 02:14:03', '2017-05-15 02:14:03');
INSERT INTO `media` (`id`, `title`, `name`, `type`, `path`, `caption`, `description`, `tinified`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(279, 'Arabic Psychometric Tools 2017.pdf', '18e990b9b0b206413bf85a7cfb48618b9dcf5a5f.pdf', 'pdf', 'uploads/documents/18e990b9b0b206413bf85a7cfb48618b9dcf5a5f.pdf', NULL, NULL, 0, 4, NULL, '2017-05-15 04:38:44', '2017-05-15 04:38:44'),
(280, 'Global Leader_Client Brochure.pdf', 'cc615c467b3b8e59b382ed7de1bb96bfe1e5bff3.pdf', 'pdf', 'uploads/documents/cc615c467b3b8e59b382ed7de1bb96bfe1e5bff3.pdf', NULL, NULL, 0, 4, NULL, '2017-05-15 08:47:58', '2017-05-15 08:47:58'),
(281, '2017-05-16_1003.png', '15b390fd24964c305f6a57175f4ecb3ef07eefaf.png', 'image', 'uploads/images/15b390fd24964c305f6a57175f4ecb3ef07eefaf.png', NULL, NULL, 1, 4, NULL, '2017-05-16 05:25:55', '2017-06-21 06:12:15'),
(282, '2017-05-16_1004.png', '885b9da65a1387e56866e9a74b66f513df81808a.png', 'image', 'uploads/images/885b9da65a1387e56866e9a74b66f513df81808a.png', NULL, NULL, 1, 4, NULL, '2017-05-16 05:25:58', '2017-06-21 06:12:06'),
(283, '18515990_10155250080851549_480522362_n.jpg', '0a0bb4220e32da5d8295c99d156211ff0b74bd62.jpg', 'image', 'uploads/images/0a0bb4220e32da5d8295c99d156211ff0b74bd62.jpg', NULL, NULL, 1, 4, NULL, '2017-05-16 05:25:59', '2017-06-21 06:12:01'),
(284, '18516149_10155250081626549_1055376318_n.jpg', '089506965c6c4ab90730df5edcf762bc61c58956.jpg', 'image', 'uploads/images/089506965c6c4ab90730df5edcf762bc61c58956.jpg', NULL, NULL, 1, 4, NULL, '2017-05-16 05:26:01', '2017-06-21 06:08:32'),
(285, 'PSI_ADC_Transition_Logo_v2.jpg', '2d90239164f4f9358d0750ea019da43a0b547bd7.jpg', 'image', 'uploads/images/2d90239164f4f9358d0750ea019da43a0b547bd7.jpg', NULL, NULL, 0, 4, NULL, '2017-06-28 02:57:27', '2017-06-28 02:57:27'),
(286, '10_THINGS.jpg', '719f2eebb06aaff55076b6649baca074c18fbcaa.jpg', 'image', 'uploads/images/719f2eebb06aaff55076b6649baca074c18fbcaa.jpg', NULL, NULL, 0, 4, NULL, '2017-07-06 04:04:13', '2017-07-06 04:04:13'),
(287, 'finalist2017.png', '066578affd940027a4c538caa671ae8ff2e02c7a.png', 'image', 'uploads/images/066578affd940027a4c538caa671ae8ff2e02c7a.png', NULL, NULL, 0, 4, NULL, '2017-07-10 04:32:13', '2017-07-10 04:32:13'),
(288, 'Gulf Capital SME Finalists Logo 2017.jpg', '02f6cff99b1cefd3a208ea7ed34d5939972f378b.jpg', 'image', 'uploads/images/02f6cff99b1cefd3a208ea7ed34d5939972f378b.jpg', NULL, NULL, 0, 4, NULL, '2017-07-10 04:52:50', '2017-07-10 04:52:50'),
(289, 'NEO_badge_2.png', 'b081e783c7d18418aead5fb901c72a611e9523dd.png', 'image', 'uploads/images/b081e783c7d18418aead5fb901c72a611e9523dd.png', NULL, NULL, 0, 4, NULL, '2017-08-07 05:02:23', '2017-08-07 05:02:23'),
(290, 'saville.png', '4bcf1155263433231dd282c5e6ee8145bc328165.png', 'image', 'uploads/images/4bcf1155263433231dd282c5e6ee8145bc328165.png', NULL, NULL, 0, 4, NULL, '2017-08-07 05:11:54', '2017-08-07 05:11:54'),
(291, 'Abrar.jpg', '605c77906c3774573b34fdfc2ad4de5ec78ef495.jpg', 'image', 'uploads/images/605c77906c3774573b34fdfc2ad4de5ec78ef495.jpg', NULL, NULL, 0, 4, NULL, '2017-08-10 01:00:38', '2017-08-10 01:00:38'),
(292, 'maddie.jpg', '4e99689e3fe2478db216f6e51982da7db61fa3d1.jpg', 'image', 'uploads/images/4e99689e3fe2478db216f6e51982da7db61fa3d1.jpg', NULL, NULL, 0, 4, NULL, '2017-08-10 01:01:10', '2017-08-10 01:01:10'),
(293, 'simonb.jpg', 'b47ac937a7b3faeb3f1314b299ea5a373027ba66.jpg', 'image', 'uploads/images/b47ac937a7b3faeb3f1314b299ea5a373027ba66.jpg', NULL, NULL, 0, 4, NULL, '2017-08-10 01:01:18', '2017-08-10 01:01:18'),
(294, 'inas.jpg', '26a65641f26ef8363fd9b971e7b923051ba397a6.jpg', 'image', 'uploads/images/26a65641f26ef8363fd9b971e7b923051ba397a6.jpg', NULL, NULL, 0, 4, NULL, '2017-08-10 01:01:31', '2017-08-10 01:01:31'),
(295, 'inas.jpg', '7e2a56f43972d528ca59f9e785799dcf08d41e9a.jpg', 'image', 'uploads/images/7e2a56f43972d528ca59f9e785799dcf08d41e9a.jpg', NULL, NULL, 0, 4, NULL, '2017-08-10 01:30:23', '2017-08-10 01:30:23'),
(296, 'MHS_EQi_Brochure.pdf', '553e418c0e75340e5b9cfb5e63b700372bc86c06.pdf', 'pdf', 'uploads/documents/553e418c0e75340e5b9cfb5e63b700372bc86c06.pdf', NULL, NULL, 0, 4, NULL, '2017-08-13 00:45:59', '2017-08-13 00:45:59'),
(297, 'Primary Colours Leadership 360 Example Report.pdf', 'ac38a250f08b2e9533ca4ea2e7bd33d9345c9789.pdf', 'pdf', 'uploads/documents/ac38a250f08b2e9533ca4ea2e7bd33d9345c9789.pdf', NULL, NULL, 0, 4, NULL, '2017-08-13 04:41:39', '2017-08-13 04:41:39'),
(298, 'sme awards 2017.jpg', 'd933b219da6384c28395c4692b7c76b247c56981.jpg', 'image', 'uploads/images/d933b219da6384c28395c4692b7c76b247c56981.jpg', NULL, NULL, 0, 4, NULL, '2017-10-12 08:57:14', '2017-10-12 08:57:14'),
(299, 'award fun 2017.jpg', 'e87686d06a68c7a4caa8fb207dd548269eb4ba58.jpg', 'image', 'uploads/images/e87686d06a68c7a4caa8fb207dd548269eb4ba58.jpg', NULL, NULL, 0, 4, NULL, '2017-10-12 16:37:44', '2017-10-12 16:37:44'),
(300, 'association.for.coaching.NEW.logo.png', '993e218f3594243cf1bd8db4407b62ff0458e46b.png', 'image', 'uploads/images/993e218f3594243cf1bd8db4407b62ff0458e46b.png', NULL, NULL, 0, 4, NULL, '2017-10-15 12:28:59', '2017-10-15 12:28:59'),
(301, 'ihs.ipcd.2017.jpg', '4d9479a0e15e4a490d72f5f0f8cff5226b5b4041.jpg', 'image', 'uploads/images/4d9479a0e15e4a490d72f5f0f8cff5226b5b4041.jpg', NULL, NULL, 0, 4, NULL, '2017-10-22 11:16:04', '2017-10-22 11:16:04'),
(302, 'ipcd.2017.jpg', '0f1a1524fc619103fd889bb36aeb17d051751bea.jpg', 'image', 'uploads/images/0f1a1524fc619103fd889bb36aeb17d051751bea.jpg', NULL, NULL, 0, 4, NULL, '2017-10-22 11:16:04', '2017-10-22 11:16:04'),
(303, 'sr.mark batey2017.jpg', '8121c58ac660fa630d04466cb79ac2c4ad14d003.jpg', 'image', 'uploads/images/8121c58ac660fa630d04466cb79ac2c4ad14d003.jpg', NULL, NULL, 0, 4, NULL, '2017-10-22 11:16:05', '2017-10-22 11:16:05'),
(304, 'ipcd.2017.ihs.jpg', '3b24a7be1b56b6c24e6d9be8f2e6cc8b83a5091a.jpg', 'image', 'uploads/images/3b24a7be1b56b6c24e6d9be8f2e6cc8b83a5091a.jpg', NULL, NULL, 0, 4, NULL, '2017-10-22 11:18:39', '2017-10-22 11:18:39'),
(305, 'full room1.jpg', '265ceabd0de735f36b98daeb6ec920c39a0f0dce.jpg', 'image', 'uploads/images/265ceabd0de735f36b98daeb6ec920c39a0f0dce.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 16:59:34', '2017-10-24 16:59:34'),
(306, 'candid.in.motion.jpg', 'a40a85d5089d0e58dabced76736cfc46ad6a5319.jpg', 'image', 'uploads/images/a40a85d5089d0e58dabced76736cfc46ad6a5319.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 16:59:40', '2017-10-24 16:59:40'),
(307, 'fullroom2.jpg', '5b5c8afd4cf128f38ad01adfa659fa957b2554d4.jpg', 'image', 'uploads/images/5b5c8afd4cf128f38ad01adfa659fa957b2554d4.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 16:59:44', '2017-10-24 16:59:44'),
(308, 'markbatey1.jpg', '2550366e3d7d05245663c1bd95eea13a45965f77.jpg', 'image', 'uploads/images/2550366e3d7d05245663c1bd95eea13a45965f77.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 16:59:48', '2017-10-24 16:59:48'),
(309, 'practical wshop.jpg', 'c47f74d5b00b86c8d5ab2d3a5c9ff3a80051b2f5.jpg', 'image', 'uploads/images/c47f74d5b00b86c8d5ab2d3a5c9ff3a80051b2f5.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 16:59:50', '2017-10-24 16:59:50'),
(310, 'practical.wskills.jpg', '2e8b424fbb624a1c393d8f2aac1c421c707e6604.jpg', 'image', 'uploads/images/2e8b424fbb624a1c393d8f2aac1c421c707e6604.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 16:59:54', '2017-10-24 16:59:54'),
(311, 'practicalskills.jpg', '0c2d005f82aa8d9a8d61a86ab6fd0330e31bc252.jpg', 'image', 'uploads/images/0c2d005f82aa8d9a8d61a86ab6fd0330e31bc252.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 16:59:58', '2017-10-24 16:59:58'),
(312, 'setup.jpg', 'f0d3dda22ff87a723679b8f896c1b66e6697ed74.jpg', 'image', 'uploads/images/f0d3dda22ff87a723679b8f896c1b66e6697ed74.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 17:00:01', '2017-10-24 17:00:01'),
(313, 'slides.jpg', '97dc4a8e052a2df35dea4118c623be8533b46f58.jpg', 'image', 'uploads/images/97dc4a8e052a2df35dea4118c623be8533b46f58.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 17:00:03', '2017-10-24 17:00:03'),
(314, 'view.jpg', '47caa1f60a5daea1ab16859686a57cf721dc12aa.jpg', 'image', 'uploads/images/47caa1f60a5daea1ab16859686a57cf721dc12aa.jpg', NULL, NULL, 0, 4, NULL, '2017-10-24 17:00:06', '2017-10-24 17:00:06'),
(315, '36dfba2a472b095b9dd6bf0503c195eec277f88d.jpg', 'bfec160006c49cc8ebc0dc8ac7f03ef029264648.jpg', 'image', 'uploads/images/bfec160006c49cc8ebc0dc8ac7f03ef029264648.jpg', NULL, NULL, 0, 4, NULL, '2017-11-20 13:44:07', '2017-11-20 13:44:07'),
(316, 'use1.jpg', '0f3e693251ba241998ea98521c0e4c18a5740280.jpg', 'image', 'uploads/images/0f3e693251ba241998ea98521c0e4c18a5740280.jpg', NULL, NULL, 0, 4, NULL, '2017-11-21 18:32:44', '2017-11-21 18:32:44'),
(317, 'use2.jpg', 'a0575bff1f2028977b8e69fea677516cef88f589.jpg', 'image', 'uploads/images/a0575bff1f2028977b8e69fea677516cef88f589.jpg', NULL, NULL, 0, 4, NULL, '2017-11-21 18:32:44', '2017-11-21 18:32:44'),
(318, 'use3.jpg', '45c83815f4fc90fa8d0acf6c22eab0f53f6cdb01.jpg', 'image', 'uploads/images/45c83815f4fc90fa8d0acf6c22eab0f53f6cdb01.jpg', NULL, NULL, 0, 4, NULL, '2017-11-21 18:32:46', '2017-11-21 18:32:46'),
(319, 'sample.jpg', '1b961ddf71f1b4d9e6ffb861775b257034f44884.jpg', 'image', 'uploads/images/1b961ddf71f1b4d9e6ffb861775b257034f44884.jpg', NULL, NULL, 1, 4, NULL, '2017-12-22 17:35:29', '2017-12-22 17:36:05'),
(320, '2018 Certification Calendar IHS.pdf', '80a70b3ae8b601e5b364ca13591425091e94a6b2.pdf', 'pdf', 'uploads/documents/80a70b3ae8b601e5b364ca13591425091e94a6b2.pdf', NULL, NULL, 0, 4, NULL, '2018-03-06 13:20:54', '2018-03-06 13:20:54'),
(321, 'lightbulb.png', '5dc3fcef46c8b8785cb6f82ebdfc2dfd2effbc71.png', 'image', 'uploads/images/5dc3fcef46c8b8785cb6f82ebdfc2dfd2effbc71.png', NULL, NULL, 0, 4, NULL, '2018-04-09 11:29:42', '2018-04-09 11:29:42'),
(322, 'dd4a0af3-1134-47a6-baf6-ffe8755cef55-thumbnail.jpg', '2bcf1ddad41dcfac8b3cd73a949acdf03a8370ce.jpg', 'image', 'uploads/images/2bcf1ddad41dcfac8b3cd73a949acdf03a8370ce.jpg', NULL, NULL, 0, 4, NULL, '2018-04-09 11:32:48', '2018-04-09 11:32:48'),
(323, 'lightbulb_50.png', 'deb034265342c78616ebe1a7d0dbd3e1fc992108.png', 'image', 'uploads/images/deb034265342c78616ebe1a7d0dbd3e1fc992108.png', NULL, NULL, 0, 4, NULL, '2018-04-09 12:09:50', '2018-04-09 12:09:50'),
(324, 'lightbulb_1_400x400.png', 'edf2e6a698888d9c965999c1c83114b1ff4bbfe3.png', 'image', 'uploads/images/edf2e6a698888d9c965999c1c83114b1ff4bbfe3.png', NULL, NULL, 0, 4, NULL, '2018-04-09 12:13:08', '2018-04-09 12:13:08'),
(326, 'facebook-cover-828x315 (1).jpg', '8098d56c90566386697d4df5aa2efc912abbb5e3.jpg', 'image', 'uploads/images/8098d56c90566386697d4df5aa2efc912abbb5e3.jpg', NULL, NULL, 0, 4, NULL, '2018-04-09 12:27:17', '2018-04-09 12:27:17'),
(327, 'webinar banner design 2 (1).png', 'cbdc8663355e2f914966f060b72f0158864688ed.png', 'image', 'uploads/images/cbdc8663355e2f914966f060b72f0158864688ed.png', NULL, NULL, 0, 4, NULL, '2018-04-09 15:45:16', '2018-04-09 15:45:16'),
(328, 'Copy of webinar banner wide design 2.png', '2ab75783162cd19063c3630c4d5cdde7049e35f5.png', 'image', 'uploads/images/2ab75783162cd19063c3630c4d5cdde7049e35f5.png', NULL, NULL, 0, 4, NULL, '2018-04-09 15:51:49', '2018-04-09 15:51:49'),
(329, 'webinar banner wide design 2.png', 'dd3f8657dfd9bfb53243ac19b01a1fa9d383b494.png', 'image', 'uploads/images/dd3f8657dfd9bfb53243ac19b01a1fa9d383b494.png', NULL, NULL, 0, 4, NULL, '2018-04-09 16:19:48', '2018-04-09 16:19:48'),
(330, 'webinar banner wide design 2.png', 'f0872453382938aadec5fd235be2a374a2428912.png', 'image', 'uploads/images/f0872453382938aadec5fd235be2a374a2428912.png', NULL, NULL, 0, 4, NULL, '2018-04-09 16:20:37', '2018-04-09 16:20:37'),
(331, 'webinar banner wide design 2 (1).png', '6eb5a6bd64cd07f80c426b14ea9da1ef9dffe344.png', 'image', 'uploads/images/6eb5a6bd64cd07f80c426b14ea9da1ef9dffe344.png', NULL, NULL, 0, 4, NULL, '2018-04-09 16:21:56', '2018-04-09 16:21:56'),
(332, 'SQ-Sample-Employer-SQ-2017.pdf', 'cc873d828b3a05538aa5cd503caf543291e4f246.pdf', 'pdf', 'uploads/documents/cc873d828b3a05538aa5cd503caf543291e4f246.pdf', NULL, NULL, 0, 4, NULL, '2018-04-17 11:05:28', '2018-04-17 11:05:28'),
(333, 'SQ-Sample-Participant-SQ-2017.pdf', '67a82bf709e7ef6272d372842b697e1e8df351d5.pdf', 'pdf', 'uploads/documents/67a82bf709e7ef6272d372842b697e1e8df351d5.pdf', NULL, NULL, 0, 4, NULL, '2018-04-17 11:05:34', '2018-04-17 11:05:34'),
(334, 'Group Report Sample.pdf', '0c903ecded10c8a1c28d5baf7ac5f464f88f5cdd.pdf', 'pdf', 'uploads/documents/0c903ecded10c8a1c28d5baf7ac5f464f88f5cdd.pdf', NULL, NULL, 0, 4, NULL, '2018-04-17 11:05:37', '2018-04-17 11:05:37'),
(335, '1 Safety Culture Perception Survey - What is Measured.pdf', 'ef7b2f6c7d0dbf642ac519c87fbf3c852ba7016a.pdf', 'pdf', 'uploads/documents/ef7b2f6c7d0dbf642ac519c87fbf3c852ba7016a.pdf', NULL, NULL, 0, 4, NULL, '2018-04-17 11:05:44', '2018-04-17 11:05:44'),
(336, '4a87e665-b226-4955-b269-bb3321e290c5.jpg', '8310fa1d4ed327f79964a5dd2549e9a0c8a4675a.jpg', 'image', 'uploads/images/8310fa1d4ed327f79964a5dd2549e9a0c8a4675a.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:34', '2018-04-17 17:31:34'),
(337, 'use3.jpg', '7c981ee7540810330e5d169993ac6a31781e75bb.jpg', 'image', 'uploads/images/7c981ee7540810330e5d169993ac6a31781e75bb.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:40', '2018-04-17 17:31:40'),
(338, 'use2.jpg', '57081fd70ac7dabad0ee8092369f90ef913ccdd0.jpg', 'image', 'uploads/images/57081fd70ac7dabad0ee8092369f90ef913ccdd0.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:41', '2018-04-17 17:31:41'),
(339, 'uae1.jpg', '5d3a32ff49458aeb6d5f4c2e53f1d20c892a11e4.jpg', 'image', 'uploads/images/5d3a32ff49458aeb6d5f4c2e53f1d20c892a11e4.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:44', '2018-04-17 17:31:44'),
(340, 'f6cf75c8-daa7-42f8-b1f9-94b5fa89553d.jpg', '15876eff38f9c7457b78a018dff6ae66808efa11.jpg', 'image', 'uploads/images/15876eff38f9c7457b78a018dff6ae66808efa11.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:46', '2018-04-17 17:31:46'),
(341, '9629a60a-57b3-4bc7-b977-ae6f42af2932.jpg', 'ed8be08ae1284fc6a768b58d27888428223f64c8.jpg', 'image', 'uploads/images/ed8be08ae1284fc6a768b58d27888428223f64c8.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:50', '2018-04-17 17:31:50'),
(342, 'BIBF1.jpg', '6164650ace835d68530e4c6322117aabe34253cd.jpg', 'image', 'uploads/images/6164650ace835d68530e4c6322117aabe34253cd.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:52', '2018-04-17 17:31:52'),
(343, 'd11434e9-3a25-4ed1-a57f-a0349c41a348.jpg', 'f61eb9e1ac35f6b24d0d3c6f9718935c92f36223.jpg', 'image', 'uploads/images/f61eb9e1ac35f6b24d0d3c6f9718935c92f36223.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:31:55', '2018-04-17 17:31:55'),
(344, 'use1.jpg', 'bc1a47f69bae306c50ea9c43b048eba029d76b19.jpg', 'image', 'uploads/images/bc1a47f69bae306c50ea9c43b048eba029d76b19.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:32:15', '2018-04-17 17:32:15'),
(345, 'use2.jpg', '0b6a5fb55ac289196a16293412e8c639da8247f1.jpg', 'image', 'uploads/images/0b6a5fb55ac289196a16293412e8c639da8247f1.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:32:17', '2018-04-17 17:32:17'),
(346, 'use3.jpg', '55e7ea484d56373eff5c95066bf019806dbcff43.jpg', 'image', 'uploads/images/55e7ea484d56373eff5c95066bf019806dbcff43.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:32:18', '2018-04-17 17:32:18'),
(347, 'the-palace-downtown-dubai.jpg', '8ca5a5ce89f54483de07d9393ec00006e5b1d5fb.jpg', 'image', 'uploads/images/8ca5a5ce89f54483de07d9393ec00006e5b1d5fb.jpg', NULL, NULL, 0, 4, NULL, '2018-04-17 17:32:21', '2018-04-17 17:32:21'),
(348, 'composition.MB.130418.png', 'e2061397126a88b4b019945b5aff818b81c31f68.png', 'image', 'uploads/images/e2061397126a88b4b019945b5aff818b81c31f68.png', NULL, NULL, 0, 4, NULL, '2018-04-17 17:32:28', '2018-04-17 17:32:28'),
(349, 'MB.HW.160318.3.png', '8c4a963649df7ad6c6397cbd9f54ad9106c779fc.png', 'image', 'uploads/images/8c4a963649df7ad6c6397cbd9f54ad9106c779fc.png', NULL, NULL, 0, 4, NULL, '2018-04-17 17:32:31', '2018-04-17 17:32:31'),
(350, 'PSI_innovative_shelf_with_strap_AW TRANSPARENT.png', 'f517915b323c8229661380fcbf5a87c0b450714f.png', 'image', 'uploads/images/f517915b323c8229661380fcbf5a87c0b450714f.png', NULL, NULL, 0, 4, NULL, '2018-04-26 09:05:04', '2018-04-26 09:05:04'),
(351, 'PSI_innovative_shelf_with_strap_AW.jpg', 'cc48e28d782b449f4dacf6f78c932425b54cbd9c.jpg', 'image', 'uploads/images/cc48e28d782b449f4dacf6f78c932425b54cbd9c.jpg', NULL, NULL, 0, 4, NULL, '2018-04-26 10:34:49', '2018-04-26 10:34:49'),
(353, 'MBTI Accreditation Programme.pdf', '1b42984079d3d223d60761ce17bf23c9292421e1.pdf', 'pdf', 'uploads/documents/1b42984079d3d223d60761ce17bf23c9292421e1.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 02:13:14', '2018-05-24 02:13:14'),
(354, 'Accreditation Calendar 2018.pdf', '733f2dfdf1b758d379087d7a5808a71384ebf86c.pdf', 'pdf', 'uploads/documents/733f2dfdf1b758d379087d7a5808a71384ebf86c.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:12', '2018-05-24 03:08:12'),
(355, 'Assessor Skills.pdf', 'ade68d18f691d7f3ac06954028bba93b510eccec.pdf', 'pdf', 'uploads/documents/ade68d18f691d7f3ac06954028bba93b510eccec.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:16', '2018-05-24 03:08:16'),
(356, 'Centre Manager.pdf', '699b147f9af38e68f1940b73fa829c3f8ec22cfb.pdf', 'pdf', 'uploads/documents/699b147f9af38e68f1940b73fa829c3f8ec22cfb.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:19', '2018-05-24 03:08:19'),
(357, 'BPS Level A and P.pdf', 'e122cac77296062c4f6d71c3414950f6489de07b.pdf', 'pdf', 'uploads/documents/e122cac77296062c4f6d71c3414950f6489de07b.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:30', '2018-05-24 03:08:30'),
(358, 'EQi 2.0 & EQ360.pdf', '0b3adf3ea46979bef32228bec57252a6a9e2158d.pdf', 'pdf', 'uploads/documents/0b3adf3ea46979bef32228bec57252a6a9e2158d.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:33', '2018-05-24 03:08:33'),
(359, 'FIRO - B.pdf', '38ce9b2aad0cbcd3d2b30c7fe6676f0b93aaa1f9.pdf', 'pdf', 'uploads/documents/38ce9b2aad0cbcd3d2b30c7fe6676f0b93aaa1f9.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:39', '2018-05-24 03:08:39'),
(360, 'MBTI Accreditation Programme.pdf', 'fe5df80f28baf66a458ca3b10cfcfd2d2d530625.pdf', 'pdf', 'uploads/documents/fe5df80f28baf66a458ca3b10cfcfd2d2d530625.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:44', '2018-05-24 03:08:44'),
(361, 'NEO Conversion.pdf', '2b594a4ae6496f28f89b1c5d532efffad542f086.pdf', 'pdf', 'uploads/documents/2b594a4ae6496f28f89b1c5d532efffad542f086.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:50', '2018-05-24 03:08:50'),
(362, 'Strong Interest Inventory®.pdf', '25e6374be8561598082b6f279a6d38414c34b7e5.pdf', 'pdf', 'uploads/documents/25e6374be8561598082b6f279a6d38414c34b7e5.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:08:54', '2018-05-24 03:08:54'),
(363, 'CPP - MBTI Product Catalogue 2018.pdf', 'af4aae15e28698a5e0c0dbfa65ac1e071005470e.pdf', 'pdf', 'uploads/documents/af4aae15e28698a5e0c0dbfa65ac1e071005470e.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:10:41', '2018-05-24 03:10:41'),
(364, 'CPP - FIRO B (CPP) 2018 Products and Pricing.pdf', '63d31abd186e219e0809ee09cc1d323a257e38da.pdf', 'pdf', 'uploads/documents/63d31abd186e219e0809ee09cc1d323a257e38da.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:10:47', '2018-05-24 03:10:47'),
(365, 'CPP - Strong and IStart Strong (CPP) 2018 Products and Pricing.pdf', '1821770a513d8cbbf59581b5a9c1ef24981d24b2.pdf', 'pdf', 'uploads/documents/1821770a513d8cbbf59581b5a9c1ef24981d24b2.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:10:52', '2018-05-24 03:10:52'),
(366, 'MHS - EQi2.0 and EQ360 2018.pdf', 'f25762c85f885ca0fd3a6aaa5aca960ef61b2ca9.pdf', 'pdf', 'uploads/documents/f25762c85f885ca0fd3a6aaa5aca960ef61b2ca9.pdf', NULL, NULL, 0, 4, NULL, '2018-05-24 03:11:05', '2018-05-24 03:11:05'),
(367, 'TalentAnalytics.jpg', '177785bcea038711e1529fdf539c8a93283d1aae.jpg', 'image', 'uploads/images/177785bcea038711e1529fdf539c8a93283d1aae.jpg', NULL, NULL, 0, 4, NULL, '2018-06-21 06:00:37', '2018-06-21 06:00:37'),
(368, 'TalentAnalytics.jpg', '8f38aad46099d065117bbf8b0d7b68082f780f0c.jpg', 'image', 'uploads/images/8f38aad46099d065117bbf8b0d7b68082f780f0c.jpg', NULL, NULL, 0, 4, NULL, '2018-06-21 06:01:26', '2018-06-21 06:01:26'),
(369, 'Banner-UNICOM-Engineering-Business-Analytics.jpg', 'ba3ac3a2f55cbe44ade950d87151e8c592be48d0.jpg', 'image', 'uploads/images/ba3ac3a2f55cbe44ade950d87151e8c592be48d0.jpg', NULL, NULL, 0, 4, NULL, '2018-06-24 02:00:28', '2018-06-24 02:00:28'),
(370, 'peak performance.jpg', '0c88ad419febe8da0b223f4097e81140e874f0b1.jpg', 'image', 'uploads/images/0c88ad419febe8da0b223f4097e81140e874f0b1.jpg', NULL, NULL, 0, 4, NULL, '2018-07-22 07:46:42', '2018-07-22 07:46:42'),
(371, 'PSI Innovative Corporate Bro Ara.pdf', '40407b9fa901bd5843a3a570e4b4035b643c230e.pdf', 'pdf', 'uploads/documents/40407b9fa901bd5843a3a570e4b4035b643c230e.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:15:08', '2018-08-01 07:15:08'),
(372, 'PSI Innovative Corporate Bro Eng.pdf', 'fc455251b12078f8ef89642cd55f8339ada8a9ac.pdf', 'pdf', 'uploads/documents/fc455251b12078f8ef89642cd55f8339ada8a9ac.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:15:11', '2018-08-01 07:15:11'),
(373, 'Assessor Skills.pdf', '79777ec5673121a4aa8f1cb758509a4408cb8e09.pdf', 'pdf', 'uploads/documents/79777ec5673121a4aa8f1cb758509a4408cb8e09.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:06', '2018-08-01 07:19:06'),
(374, 'Centre Manager.pdf', '0a2c254d690c3286183b2af4823ee7a26d2aa508.pdf', 'pdf', 'uploads/documents/0a2c254d690c3286183b2af4823ee7a26d2aa508.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:09', '2018-08-01 07:19:09'),
(375, 'BPS Level A and P.pdf', '22a0728e4f14b73eaa26128ffc948ddffbd1bebc.pdf', 'pdf', 'uploads/documents/22a0728e4f14b73eaa26128ffc948ddffbd1bebc.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:14', '2018-08-01 07:19:14'),
(376, 'EQi 2.0 & EQ360.pdf', '3dbd24b8068f629c5ac31a5d2aed9333c90e66ae.pdf', 'pdf', 'uploads/documents/3dbd24b8068f629c5ac31a5d2aed9333c90e66ae.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:22', '2018-08-01 07:19:22'),
(377, 'FIRO - B.pdf', 'e9ea3bbb64976765cdf4df435213eb0671ba9a9f.pdf', 'pdf', 'uploads/documents/e9ea3bbb64976765cdf4df435213eb0671ba9a9f.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:30', '2018-08-01 07:19:30'),
(378, 'MBTI Accreditation Programme.pdf', '2f295b63096270bceff271bcae8967042e5f97c7.pdf', 'pdf', 'uploads/documents/2f295b63096270bceff271bcae8967042e5f97c7.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:36', '2018-08-01 07:19:36'),
(379, 'NEO Conversion.pdf', 'ab48f9da46786d05fa2e652c26d757bdb790d703.pdf', 'pdf', 'uploads/documents/ab48f9da46786d05fa2e652c26d757bdb790d703.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:43', '2018-08-01 07:19:43'),
(380, 'Strong Interest Inventory®.pdf', '6640e20c8d9446ac0b2268e5dcfd513e594226e7.pdf', 'pdf', 'uploads/documents/6640e20c8d9446ac0b2268e5dcfd513e594226e7.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:19:50', '2018-08-01 07:19:50'),
(381, 'Accreditation Calendar 2018.pdf', '1b975e7f0c0c271618888a8f7c842903546e525e.pdf', 'pdf', 'uploads/documents/1b975e7f0c0c271618888a8f7c842903546e525e.pdf', NULL, NULL, 0, 4, NULL, '2018-08-01 07:40:24', '2018-08-01 07:40:24'),
(382, 'Marais Bester Profile Pic New.jpg', 'ee58cc1a3d68160500ac3fead17b9217cc3d315d.jpg', 'image', 'uploads/images/ee58cc1a3d68160500ac3fead17b9217cc3d315d.jpg', NULL, NULL, 0, 4, NULL, '2018-08-13 00:43:42', '2018-08-13 00:43:42'),
(383, 'JORDAN_JONES.png', '053e208ca6e595e3e818074b1594858573c91efe.png', 'image', 'uploads/images/053e208ca6e595e3e818074b1594858573c91efe.png', NULL, NULL, 0, 4, NULL, '2018-09-26 03:45:52', '2018-09-26 03:45:52'),
(384, 'triad.png', 'fdd2d449b046f61f591d1f1277ba5504c51f34d3.png', 'image', 'uploads/images/fdd2d449b046f61f591d1f1277ba5504c51f34d3.png', NULL, NULL, 0, 4, NULL, '2018-09-26 05:00:19', '2018-09-26 05:00:19'),
(385, 'esteem.psychology._260918.png', '0e6ce4210ee7977e60883e48e9ed7dd1c61eb1c1.png', 'image', 'uploads/images/0e6ce4210ee7977e60883e48e9ed7dd1c61eb1c1.png', NULL, NULL, 0, 4, NULL, '2018-09-26 06:42:12', '2018-09-26 06:42:12'),
(386, 'PDB.png', '64ddd588a0263699614aa2f34e70277a169c92ef.png', 'image', 'uploads/images/64ddd588a0263699614aa2f34e70277a169c92ef.png', NULL, NULL, 0, 4, NULL, '2018-09-26 07:04:33', '2018-09-26 07:04:33'),
(387, 'ABIKRIMEED.IPCD..jpg', '00c291bf46d19d8a54c87a5fc83c955a87a51e85.jpg', 'image', 'uploads/images/00c291bf46d19d8a54c87a5fc83c955a87a51e85.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:05', '2018-10-09 01:30:05'),
(388, 'alia annie.jpg', 'b0ea082df52b48a7396b40bd106e195124c5beff.jpg', 'image', 'uploads/images/b0ea082df52b48a7396b40bd106e195124c5beff.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:05', '2018-10-09 01:30:05'),
(389, 'binnakandola.jpg', 'f6b1c4296631a1fdde8c993d519114fa912a94fe.jpg', 'image', 'uploads/images/f6b1c4296631a1fdde8c993d519114fa912a94fe.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:06', '2018-10-09 01:30:06'),
(390, 'ipcdready!.jpg', 'f09f7bc4dd16678d19023e0553fd8ee41deab523.jpg', 'image', 'uploads/images/f09f7bc4dd16678d19023e0553fd8ee41deab523.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:06', '2018-10-09 01:30:06'),
(391, 'MARAISBESTER.jpg', '9a93102178c368a3b656e8f0af350565ffa5d787.jpg', 'image', 'uploads/images/9a93102178c368a3b656e8f0af350565ffa5d787.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:06', '2018-10-09 01:30:06'),
(392, 'MB.jpg', '7a67a44f4187e30b8604e051306fe74fec37716a.jpg', 'image', 'uploads/images/7a67a44f4187e30b8604e051306fe74fec37716a.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:07', '2018-10-09 01:30:07'),
(393, 'pic.png', 'a5149b02d4dce815a98c969cf52678a161ee7308.png', 'image', 'uploads/images/a5149b02d4dce815a98c969cf52678a161ee7308.png', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:07', '2018-10-09 01:30:07'),
(394, 'saeed.psi.jpg', '29c55cca476e453a9e25474bd7772e50effe679f.jpg', 'image', 'uploads/images/29c55cca476e453a9e25474bd7772e50effe679f.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:08', '2018-10-09 01:30:08'),
(395, 'twitter post.jpg', '7a66a40bebdd97897c29074cd124fd294ca83eea.jpg', 'image', 'uploads/images/7a66a40bebdd97897c29074cd124fd294ca83eea.jpg', NULL, NULL, 0, 4, NULL, '2018-10-09 01:30:08', '2018-10-09 01:30:08'),
(397, 'Screen Shot 2018-10-09 at 10.02.14 AM.png', 'c455b7403f99719cb7815eaab14e2b27cafea4da.png', 'image', 'uploads/images/c455b7403f99719cb7815eaab14e2b27cafea4da.png', NULL, NULL, 0, 2, NULL, '2018-10-09 02:07:33', '2018-10-09 02:07:33'),
(398, 'BPS Class of October 2018 (1).png', '64a077d9aceba04ca242c2127b2662ef18e46746.png', 'image', 'uploads/images/64a077d9aceba04ca242c2127b2662ef18e46746.png', NULL, NULL, 0, 4, NULL, '2018-10-09 03:37:37', '2018-10-09 03:37:37'),
(399, 'SII Class of September 2018.png', '7ba402b7dc489f9ad8d05269bda0b462a929788d.png', 'image', 'uploads/images/7ba402b7dc489f9ad8d05269bda0b462a929788d.png', NULL, NULL, 0, 4, NULL, '2018-10-09 03:37:37', '2018-10-09 03:37:37'),
(400, '2019 PSI Accreditation and Workshop Catalogue.pdf', '8637fa17c7e50f7c77c09359a4ee2b99a8c60829.pdf', 'pdf', 'uploads/documents/8637fa17c7e50f7c77c09359a4ee2b99a8c60829.pdf', NULL, NULL, 0, 4, NULL, '2018-10-15 02:45:28', '2018-10-15 02:45:28'),
(401, 'the-british-psychological-society59FA82DD1B59BE25E2C867D7.png', '13072ee511f3cfce032a3d158688d4abe6f19104.png', 'image', 'uploads/images/13072ee511f3cfce032a3d158688d4abe6f19104.png', NULL, NULL, 0, 4, NULL, '2018-10-17 01:05:21', '2018-10-17 01:05:21'),
(402, '2019 PSI Accreditation and Workshop Catalogue.pdf', 'da19d0f7c96981b2769dfc45515b40e5f1d9362e.pdf', 'pdf', 'uploads/documents/da19d0f7c96981b2769dfc45515b40e5f1d9362e.pdf', NULL, NULL, 0, 4, NULL, '2018-10-18 07:57:34', '2018-10-18 07:57:34'),
(403, '2019 Calendar only.pdf', '8a50974231101f4193aa19a4c3529e6abc8f0c28.pdf', 'pdf', 'uploads/documents/8a50974231101f4193aa19a4c3529e6abc8f0c28.pdf', NULL, NULL, 0, 4, NULL, '2018-10-23 04:02:19', '2018-10-23 04:02:19'),
(404, '2019 Training Catalogue.pdf', '5afe06ebb3c9291dbc8f196b877a52ce7a287da8.pdf', 'pdf', 'uploads/documents/5afe06ebb3c9291dbc8f196b877a52ce7a287da8.pdf', NULL, NULL, 0, 4, NULL, '2018-10-23 04:02:26', '2018-10-23 04:02:26'),
(405, 'Logo - PSI Middle East [72dpi] (png).png', '9854313ad44459e0db7206c54410f76136f26b54.png', 'image', 'uploads/images/9854313ad44459e0db7206c54410f76136f26b54.png', NULL, NULL, 0, 4, NULL, '2018-11-21 01:50:15', '2018-11-21 01:50:15'),
(406, 'PSI Middle East Corporate Folder.pdf', '1ba33088ff94d371567f25ded3f24338a7172d48.pdf', 'pdf', 'uploads/documents/1ba33088ff94d371567f25ded3f24338a7172d48.pdf', NULL, NULL, 0, 4, NULL, '2018-11-27 08:47:10', '2018-11-27 08:47:10'),
(407, 'cover.banner..png', '56391c6c0bfee62bf00e9f04efa68757414f1707.png', 'image', 'uploads/images/56391c6c0bfee62bf00e9f04efa68757414f1707.png', NULL, NULL, 0, 4, NULL, '2018-11-28 02:17:57', '2018-11-28 02:17:57'),
(408, 'fab.jpg', '1a43783c58eaeba66543a3fdc6926e6ff020593c.jpg', 'image', 'uploads/images/1a43783c58eaeba66543a3fdc6926e6ff020593c.jpg', NULL, NULL, 0, 4, NULL, '2018-12-30 06:36:17', '2018-12-30 06:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `mediables`
--

CREATE TABLE `mediables` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_id` int(10) UNSIGNED NOT NULL,
  `mediable_id` int(11) NOT NULL,
  `mediable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collection` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mediables`
--

INSERT INTO `mediables` (`id`, `media_id`, `mediable_id`, `mediable_type`, `collection`) VALUES
(15, 4, 4, 'App\\Page', 'banner'),
(20, 6, 25, 'App\\Page', NULL),
(46, 10, 1, 'App\\Page', NULL),
(47, 11, 1, 'App\\Page', NULL),
(51, 13, 1, 'App\\Page', NULL),
(52, 6, 38, 'App\\Page', NULL),
(54, 18, 51, 'App\\Page', NULL),
(55, 17, 51, 'App\\Page', NULL),
(56, 15, 51, 'App\\Page', NULL),
(57, 16, 52, 'App\\Page', NULL),
(58, 20, 52, 'App\\Page', NULL),
(59, 19, 52, 'App\\Page', NULL),
(60, 22, 53, 'App\\Page', NULL),
(61, 21, 53, 'App\\Page', NULL),
(62, 24, 54, 'App\\Page', NULL),
(63, 23, 54, 'App\\Page', NULL),
(64, 26, 55, 'App\\Page', NULL),
(66, 29, 56, 'App\\Page', NULL),
(67, 28, 56, 'App\\Page', NULL),
(70, 32, 58, 'App\\Page', NULL),
(71, 33, 62, 'App\\Page', NULL),
(73, 34, 63, 'App\\Page', NULL),
(75, 35, 64, 'App\\Page', NULL),
(80, 39, 38, 'App\\Page', NULL),
(94, 50, 84, 'App\\Page', NULL),
(95, 49, 84, 'App\\Page', NULL),
(96, 48, 84, 'App\\Page', NULL),
(97, 47, 84, 'App\\Page', NULL),
(99, 51, 88, 'App\\Page', NULL),
(104, 52, 88, 'App\\Page', NULL),
(107, 54, 91, 'App\\Page', NULL),
(109, 57, 93, 'App\\Page', NULL),
(110, 58, 94, 'App\\Page', NULL),
(113, 126, 10, 'App\\Page', NULL),
(114, 125, 10, 'App\\Page', NULL),
(115, 124, 10, 'App\\Page', NULL),
(116, 123, 10, 'App\\Page', NULL),
(117, 122, 10, 'App\\Page', NULL),
(118, 121, 10, 'App\\Page', NULL),
(119, 120, 10, 'App\\Page', NULL),
(120, 119, 10, 'App\\Page', NULL),
(121, 118, 10, 'App\\Page', NULL),
(122, 117, 10, 'App\\Page', NULL),
(123, 116, 10, 'App\\Page', NULL),
(124, 115, 10, 'App\\Page', NULL),
(125, 114, 10, 'App\\Page', NULL),
(126, 113, 10, 'App\\Page', NULL),
(127, 112, 10, 'App\\Page', NULL),
(128, 111, 10, 'App\\Page', NULL),
(129, 109, 10, 'App\\Page', NULL),
(130, 108, 10, 'App\\Page', NULL),
(131, 107, 10, 'App\\Page', NULL),
(132, 106, 10, 'App\\Page', NULL),
(133, 105, 10, 'App\\Page', NULL),
(134, 104, 10, 'App\\Page', NULL),
(135, 103, 10, 'App\\Page', NULL),
(136, 102, 10, 'App\\Page', NULL),
(137, 101, 10, 'App\\Page', NULL),
(138, 100, 10, 'App\\Page', NULL),
(139, 99, 10, 'App\\Page', NULL),
(140, 98, 10, 'App\\Page', NULL),
(141, 97, 10, 'App\\Page', NULL),
(142, 96, 10, 'App\\Page', NULL),
(143, 94, 10, 'App\\Page', NULL),
(144, 95, 10, 'App\\Page', NULL),
(145, 93, 10, 'App\\Page', NULL),
(146, 92, 10, 'App\\Page', NULL),
(147, 91, 10, 'App\\Page', NULL),
(148, 90, 10, 'App\\Page', NULL),
(149, 89, 10, 'App\\Page', NULL),
(150, 88, 10, 'App\\Page', NULL),
(152, 86, 10, 'App\\Page', NULL),
(153, 85, 10, 'App\\Page', NULL),
(154, 84, 10, 'App\\Page', NULL),
(155, 83, 10, 'App\\Page', NULL),
(156, 82, 10, 'App\\Page', NULL),
(157, 81, 10, 'App\\Page', NULL),
(158, 80, 10, 'App\\Page', NULL),
(159, 79, 10, 'App\\Page', NULL),
(160, 78, 10, 'App\\Page', NULL),
(161, 77, 10, 'App\\Page', NULL),
(162, 76, 10, 'App\\Page', NULL),
(163, 75, 10, 'App\\Page', NULL),
(164, 74, 10, 'App\\Page', NULL),
(165, 73, 10, 'App\\Page', NULL),
(166, 72, 10, 'App\\Page', NULL),
(168, 70, 10, 'App\\Page', NULL),
(169, 69, 10, 'App\\Page', NULL),
(170, 127, 1, 'App\\Page', NULL),
(171, 128, 1, 'App\\Page', 'banner'),
(172, 129, 42, 'App\\Page', 'thumb'),
(181, 129, 40, 'App\\Page', 'banner'),
(182, 129, 41, 'App\\Page', 'banner'),
(185, 131, 50, 'App\\Page', 'banner'),
(191, 129, 20, 'App\\Page', 'banner'),
(193, 134, 94, 'App\\Page', 'banner'),
(208, 134, 102, 'App\\Page', 'banner'),
(211, 134, 105, 'App\\Page', 'banner'),
(213, 130, 2, 'App\\Event', 'banner'),
(214, 130, 28, 'App\\Page', NULL),
(217, 134, 115, 'App\\Page', 'banner'),
(220, 132, 11, 'App\\Page', 'banner'),
(221, 132, 46, 'App\\Page', 'banner'),
(222, 132, 47, 'App\\Page', 'banner'),
(223, 132, 48, 'App\\Page', 'banner'),
(227, 134, 117, 'App\\Page', 'banner'),
(228, 134, 116, 'App\\Page', 'banner'),
(229, 134, 118, 'App\\Page', 'banner'),
(230, 131, 14, 'App\\Page', 'banner'),
(231, 131, 97, 'App\\Page', 'banner'),
(232, 131, 98, 'App\\Page', 'banner'),
(233, 131, 99, 'App\\Page', 'banner'),
(234, 131, 100, 'App\\Page', 'banner'),
(236, 131, 136, 'App\\Page', 'banner'),
(237, 131, 103, 'App\\Page', 'banner'),
(238, 131, 104, 'App\\Page', 'banner'),
(239, 131, 133, 'App\\Page', 'banner'),
(240, 131, 106, 'App\\Page', 'banner'),
(242, 129, 28, 'App\\Page', 'banner'),
(243, 129, 59, 'App\\Page', 'banner'),
(244, 129, 45, 'App\\Page', 'banner'),
(245, 129, 67, 'App\\Page', 'banner'),
(246, 129, 21, 'App\\Page', 'banner'),
(247, 129, 60, 'App\\Page', 'banner'),
(249, 133, 49, 'App\\Page', 'banner'),
(251, 133, 96, 'App\\Page', 'banner'),
(252, 133, 109, 'App\\Page', 'banner'),
(253, 133, 110, 'App\\Page', 'banner'),
(255, 133, 111, 'App\\Page', 'banner'),
(257, 133, 112, 'App\\Page', 'banner'),
(258, 133, 113, 'App\\Page', 'banner'),
(259, 133, 114, 'App\\Page', 'banner'),
(262, 132, 1, 'App\\Event', 'banner'),
(263, 164, 101, 'App\\Page', 'banner'),
(265, 131, 17, 'App\\Course', NULL),
(266, 131, 16, 'App\\Course', NULL),
(267, 131, 15, 'App\\Course', 'banner'),
(268, 131, 14, 'App\\Course', 'banner'),
(269, 131, 13, 'App\\Course', NULL),
(270, 131, 11, 'App\\Course', 'banner'),
(277, 177, 8, 'App\\Course', 'banner'),
(278, 178, 17, 'App\\Course', 'banner'),
(279, 178, 16, 'App\\Course', 'banner'),
(280, 179, 13, 'App\\Course', 'banner'),
(281, 180, 10, 'App\\Course', 'banner'),
(282, 181, 7, 'App\\Course', 'banner'),
(285, 184, 9, 'App\\Course', 'banner'),
(286, 185, 5, 'App\\Event', 'banner'),
(287, 188, 6, 'App\\Event', 'banner'),
(288, 177, 9, 'App\\Post', 'banner'),
(289, 188, 14, 'App\\Post', 'banner'),
(290, 170, 13, 'App\\Post', 'banner'),
(291, 128, 4, 'App\\Post', 'banner'),
(292, 129, 3, 'App\\Post', 'banner'),
(293, 180, 12, 'App\\Post', 'banner'),
(295, 180, 2, 'App\\Post', 'banner'),
(296, 132, 1, 'App\\Post', 'banner'),
(299, 190, 4, 'App\\Course', 'banner'),
(301, 184, 137, 'App\\Page', 'banner'),
(304, 179, 7, 'App\\Post', 'banner'),
(306, 179, 8, 'App\\Post', 'banner'),
(307, 179, 11, 'App\\Post', 'banner'),
(308, 179, 6, 'App\\Post', 'banner'),
(309, 179, 5, 'App\\Post', 'banner'),
(310, 179, 10, 'App\\Post', 'banner'),
(316, 211, 12, 'App\\Page', 'banner'),
(317, 134, 13, 'App\\Page', 'banner'),
(318, 133, 43, 'App\\Page', 'thumb'),
(319, 212, 95, 'App\\Page', 'banner'),
(320, 223, 15, 'App\\Post', 'banner'),
(322, 223, 10, 'App\\Page', NULL),
(323, 233, 10, 'App\\Page', NULL),
(324, 232, 10, 'App\\Page', NULL),
(325, 231, 10, 'App\\Page', NULL),
(326, 229, 10, 'App\\Page', NULL),
(327, 230, 10, 'App\\Page', NULL),
(328, 228, 10, 'App\\Page', NULL),
(329, 227, 10, 'App\\Page', NULL),
(330, 226, 10, 'App\\Page', NULL),
(331, 225, 10, 'App\\Page', NULL),
(335, 249, 16, 'App\\Post', 'banner'),
(336, 259, 10, 'App\\Page', NULL),
(337, 258, 10, 'App\\Page', NULL),
(338, 256, 10, 'App\\Page', NULL),
(339, 254, 10, 'App\\Page', NULL),
(340, 253, 10, 'App\\Page', NULL),
(341, 252, 10, 'App\\Page', NULL),
(342, 251, 10, 'App\\Page', NULL),
(343, 250, 10, 'App\\Page', NULL),
(344, 249, 10, 'App\\Page', NULL),
(345, 132, 6, 'App\\Course', 'banner'),
(346, 284, 10, 'App\\Page', NULL),
(347, 283, 10, 'App\\Page', NULL),
(348, 282, 10, 'App\\Page', NULL),
(349, 281, 10, 'App\\Page', NULL),
(350, 282, 17, 'App\\Post', 'banner'),
(351, 281, 17, 'App\\Post', NULL),
(352, 298, 10, 'App\\Page', NULL),
(353, 299, 10, 'App\\Page', NULL),
(354, 304, 10, 'App\\Page', NULL),
(355, 303, 10, 'App\\Page', NULL),
(356, 301, 10, 'App\\Page', NULL),
(357, 302, 10, 'App\\Page', NULL),
(358, 314, 10, 'App\\Page', NULL),
(359, 313, 10, 'App\\Page', NULL),
(360, 312, 10, 'App\\Page', NULL),
(361, 311, 10, 'App\\Page', NULL),
(362, 310, 10, 'App\\Page', NULL),
(363, 309, 10, 'App\\Page', NULL),
(364, 308, 10, 'App\\Page', NULL),
(365, 306, 10, 'App\\Page', NULL),
(366, 305, 10, 'App\\Page', NULL),
(367, 318, 10, 'App\\Page', NULL),
(368, 316, 10, 'App\\Page', NULL),
(369, 317, 10, 'App\\Page', NULL),
(370, 287, 18, 'App\\Post', 'banner'),
(380, 331, 20, 'App\\Post', 'banner'),
(383, 349, 21, 'App\\Post', 'banner'),
(385, 369, 22, 'App\\Post', 'banner'),
(386, 370, 23, 'App\\Post', 'banner'),
(387, 383, 24, 'App\\Post', 'banner'),
(388, 384, 25, 'App\\Post', 'banner'),
(389, 385, 26, 'App\\Post', 'banner'),
(391, 398, 10, 'App\\Page', NULL),
(392, 399, 10, 'App\\Page', NULL),
(393, 394, 10, 'App\\Page', NULL),
(394, 395, 10, 'App\\Page', NULL),
(395, 392, 10, 'App\\Page', NULL),
(396, 393, 10, 'App\\Page', NULL),
(397, 389, 10, 'App\\Page', NULL),
(398, 391, 10, 'App\\Page', NULL),
(399, 388, 10, 'App\\Page', NULL),
(400, 387, 10, 'App\\Page', NULL),
(401, 385, 10, 'App\\Page', NULL),
(403, 407, 19, 'App\\Post', 'banner');

-- --------------------------------------------------------

--
-- Table structure for table `mediable_translations`
--

CREATE TABLE `mediable_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `mediable_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mediable_translations`
--

INSERT INTO `mediable_translations` (`id`, `mediable_id`, `locale`, `caption`) VALUES
(17, 15, 'en', NULL),
(40, 46, 'en', NULL),
(41, 47, 'en', NULL),
(42, 51, 'en', NULL),
(43, 56, 'en', 'Personal Report'),
(44, 55, 'en', 'Expert Report'),
(45, 54, 'en', 'Line Manager Report'),
(46, 57, 'en', NULL),
(47, 59, 'en', NULL),
(48, 58, 'en', NULL),
(49, 61, 'en', NULL),
(50, 60, 'en', NULL),
(51, 64, 'en', NULL),
(53, 67, 'en', NULL),
(54, 66, 'en', NULL),
(57, 70, 'en', NULL),
(58, 71, 'en', NULL),
(59, 73, 'en', 'Saville Performance 360'),
(60, 75, 'en', 'Standard Client'),
(64, 52, 'en', NULL),
(66, 80, 'en', 'Strong Interest Inventory'),
(75, 97, 'en', 'MBTI Step 1'),
(76, 96, 'en', 'MBTI Step 2'),
(77, 95, 'en', 'Step 1 Interpretive Report'),
(78, 94, 'en', 'Step 2 Interpretive Report'),
(79, 99, 'en', 'Client Report'),
(81, 104, 'en', 'Coach Report'),
(83, 107, 'en', 'Hogan Development Survey'),
(85, 109, 'en', 'Hogan Motives, Values, Preferences'),
(86, 110, 'en', 'Leadership Development'),
(87, 113, 'en', NULL),
(88, 114, 'en', NULL),
(89, 115, 'en', NULL),
(90, 116, 'en', NULL),
(91, 117, 'en', NULL),
(92, 118, 'en', NULL),
(93, 119, 'en', NULL),
(94, 120, 'en', NULL),
(95, 121, 'en', NULL),
(96, 122, 'en', NULL),
(97, 123, 'en', NULL),
(98, 124, 'en', NULL),
(99, 125, 'en', NULL),
(100, 126, 'en', NULL),
(101, 127, 'en', NULL),
(102, 128, 'en', NULL),
(103, 129, 'en', NULL),
(104, 130, 'en', NULL),
(105, 131, 'en', NULL),
(106, 132, 'en', NULL),
(107, 133, 'en', NULL),
(108, 134, 'en', NULL),
(109, 135, 'en', NULL),
(110, 136, 'en', NULL),
(111, 137, 'en', NULL),
(112, 138, 'en', NULL),
(113, 139, 'en', NULL),
(114, 140, 'en', NULL),
(115, 141, 'en', NULL),
(116, 142, 'en', NULL),
(117, 143, 'en', NULL),
(118, 144, 'en', NULL),
(119, 145, 'en', NULL),
(120, 146, 'en', NULL),
(121, 147, 'en', NULL),
(122, 148, 'en', NULL),
(123, 149, 'en', NULL),
(124, 150, 'en', NULL),
(126, 152, 'en', NULL),
(127, 153, 'en', NULL),
(128, 154, 'en', NULL),
(129, 155, 'en', NULL),
(130, 156, 'en', NULL),
(131, 157, 'en', NULL),
(132, 158, 'en', NULL),
(133, 159, 'en', NULL),
(134, 160, 'en', NULL),
(135, 161, 'en', NULL),
(136, 162, 'en', NULL),
(137, 163, 'en', NULL),
(138, 164, 'en', NULL),
(139, 165, 'en', NULL),
(140, 166, 'en', NULL),
(142, 168, 'en', NULL),
(143, 169, 'en', NULL),
(144, 170, 'en', NULL),
(145, 171, 'en', NULL),
(146, 172, 'en', NULL),
(158, 181, 'en', 'Assessment'),
(159, 182, 'en', 'Assessment'),
(162, 185, 'en', NULL),
(168, 191, 'en', 'Assessment'),
(170, 193, 'en', NULL),
(185, 208, 'en', NULL),
(188, 211, 'en', NULL),
(190, 213, 'en', NULL),
(191, 214, 'en', NULL),
(194, 217, 'en', NULL),
(197, 220, 'en', 'Consulting'),
(198, 221, 'en', 'Consulting'),
(199, 222, 'en', NULL),
(200, 223, 'en', 'Consulting'),
(204, 227, 'en', 'Development'),
(205, 228, 'en', 'Development'),
(206, 229, 'en', 'Development'),
(207, 230, 'en', 'Accreditation Programmes'),
(208, 231, 'en', 'Accreditation Programmes'),
(209, 232, 'en', 'Accreditation Programmes'),
(210, 233, 'en', 'Accreditation Programmes'),
(211, 234, 'en', 'Accreditation Programmes'),
(213, 236, 'en', 'Accreditation Programmes'),
(214, 237, 'en', 'Accreditation Programmes'),
(215, 238, 'en', 'Accreditation Programmes'),
(216, 239, 'en', 'Accreditation Programmes'),
(217, 240, 'en', 'Accreditation Programmes'),
(219, 242, 'en', 'Assessment'),
(220, 243, 'en', 'Assessment'),
(221, 244, 'en', 'Assessment'),
(222, 245, 'en', 'Assessment'),
(223, 246, 'en', 'Assessment'),
(224, 247, 'en', 'Assessment'),
(226, 249, 'en', 'Nationalisation'),
(228, 251, 'en', 'Performance Management'),
(229, 252, 'en', 'Motivating and Engaging Others'),
(230, 253, 'en', 'Competency Based Interviews'),
(231, 255, 'en', 'Coaching Skills'),
(232, 257, 'en', 'Managing Change'),
(233, 258, 'en', 'Resilence'),
(234, 259, 'en', 'Managing Conflict'),
(236, 262, 'en', NULL),
(237, 263, 'en', NULL),
(239, 265, 'en', NULL),
(240, 266, 'en', NULL),
(241, 267, 'en', NULL),
(242, 268, 'en', NULL),
(243, 269, 'en', NULL),
(244, 270, 'en', NULL),
(251, 277, 'en', NULL),
(252, 278, 'en', NULL),
(253, 279, 'en', NULL),
(254, 280, 'en', NULL),
(255, 281, 'en', NULL),
(256, 282, 'en', NULL),
(259, 285, 'en', NULL),
(260, 286, 'en', NULL),
(261, 287, 'en', NULL),
(262, 288, 'en', NULL),
(263, 289, 'en', NULL),
(264, 290, 'en', NULL),
(265, 291, 'en', NULL),
(266, 292, 'en', NULL),
(267, 293, 'en', NULL),
(268, 295, 'en', NULL),
(269, 296, 'en', NULL),
(272, 299, 'en', NULL),
(274, 301, 'en', NULL),
(277, 304, 'en', NULL),
(279, 307, 'en', NULL),
(280, 306, 'en', NULL),
(281, 308, 'en', NULL),
(282, 309, 'en', NULL),
(283, 310, 'en', NULL),
(288, 316, 'en', NULL),
(289, 317, 'en', NULL),
(290, 318, 'en', NULL),
(291, 319, 'en', NULL),
(292, 320, 'en', NULL),
(293, 322, 'en', NULL),
(294, 323, 'en', NULL),
(295, 324, 'en', NULL),
(296, 325, 'en', NULL),
(297, 326, 'en', NULL),
(298, 327, 'en', NULL),
(299, 328, 'en', NULL),
(300, 329, 'en', NULL),
(301, 330, 'en', NULL),
(302, 331, 'en', NULL),
(306, 335, 'en', NULL),
(307, 345, 'en', NULL),
(308, 350, 'en', NULL),
(309, 351, 'en', NULL),
(310, 336, 'en', NULL),
(311, 337, 'en', NULL),
(312, 338, 'en', NULL),
(313, 339, 'en', NULL),
(314, 340, 'en', NULL),
(315, 341, 'en', NULL),
(316, 342, 'en', NULL),
(317, 343, 'en', NULL),
(318, 344, 'en', NULL),
(319, 346, 'en', NULL),
(320, 347, 'en', NULL),
(321, 348, 'en', NULL),
(322, 349, 'en', NULL),
(323, 352, 'en', 'SME Awards 2017'),
(324, 353, 'en', 'awards fun 2017!'),
(325, 354, 'en', NULL),
(326, 355, 'en', NULL),
(327, 356, 'en', NULL),
(328, 357, 'en', NULL),
(329, 370, 'en', NULL),
(339, 380, 'en', NULL),
(342, 383, 'en', NULL),
(344, 385, 'en', NULL),
(345, 386, 'en', 'banner'),
(346, 387, 'en', 'banner'),
(347, 388, 'en', 'banner'),
(348, 389, 'en', 'banner'),
(349, 358, 'en', NULL),
(350, 359, 'en', NULL),
(351, 360, 'en', NULL),
(352, 361, 'en', NULL),
(353, 362, 'en', NULL),
(354, 363, 'en', NULL),
(355, 364, 'en', NULL),
(356, 365, 'en', NULL),
(357, 366, 'en', NULL),
(358, 367, 'en', NULL),
(359, 368, 'en', NULL),
(360, 369, 'en', NULL),
(362, 403, 'en', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_10_052213_create_roles_and_permissions_tables', 1),
('2016_06_10_110410_create_pages_table', 1),
('2016_06_19_112221_create_media_table', 1),
('2016_06_20_110353_create_mediable_table', 2),
('2016_06_26_093454_create_blog_tables', 2),
('2016_06_28_104016_create_events_tables', 2),
('2016_06_29_095920_create_courses_tables', 3),
('2016_06_29_141548_create_clients_tables', 4),
('2016_07_05_132819_create_jobs_tables', 5),
('2016_07_10_053135_create_locations_tables', 6),
('2016_07_10_114434_create_casestudies_tables', 6),
('2016_07_12_152013_media_translation', 7),
('2016_08_01_074355_create_team_tables', 8),
('2016_08_01_121208_create_custom_field_tables', 9),
('2016_08_02_065418_create_course_page_table', 10),
('2016_08_02_104736_add_navigation_name_to_pages', 11),
('2016_08_02_151210_add_link_to_courses', 12),
('2016_08_04_072053_add_landscape_image_to_locations', 13),
('2016_09_06_141410_add_order_to_courses', 14);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `navigation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `not_searchable` tinyint(1) NOT NULL DEFAULT '0',
  `alias` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `name`, `uri`, `template`, `lft`, `rgt`, `depth`, `navigation`, `hidden`, `not_searchable`, `alias`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 'About PSI Middle East', 'about', 'about', 7, 12, 0, 'top', 0, 0, 0, 1, NULL, 5, NULL, '2018-11-28 10:17:59'),
(3, NULL, 'Contact', 'contact', 'contact', 41, 44, 0, 'top', 0, 0, 0, 1, NULL, 2, NULL, '2018-05-23 03:47:42'),
(4, NULL, 'Home', '/home', 'home', 1, 6, 0, '', 0, 0, 0, 1, NULL, 4, NULL, '2018-11-28 00:18:23'),
(5, 10, 'News & Events', 'knowledge-centre/news-events', 'blog', 14, 17, 1, '', 0, 0, 0, 1, NULL, 3, NULL, '2018-02-04 19:19:07'),
(6, 5, 'Article', 'knowledge-centre/news-events/article/{id}/{slug}', 'article', 15, 16, 2, '', 1, 0, 0, 1, NULL, 3, NULL, '2018-02-04 19:19:07'),
(10, NULL, 'Knowledge Centre', 'knowledge-centre', 'knowledgeCenter', 13, 30, 0, 'top', 0, 0, 0, 1, 1, 4, '2016-08-02 07:58:22', '2018-04-08 15:18:20'),
(11, NULL, 'Consulting', 'consulting', 'overviewPage', 45, 54, 0, 'secondary', 0, 0, 0, 1, 1, 2, '2016-08-02 08:07:12', '2018-12-11 06:05:40'),
(12, NULL, 'Assessment', 'assessment', 'overviewPage', 55, 142, 0, 'secondary', 0, 0, 0, 1, 1, 5, '2016-08-02 08:07:36', '2018-04-17 11:01:17'),
(13, NULL, 'Development', 'development', 'overviewPage', 143, 170, 0, 'secondary', 0, 0, 0, 1, 1, 5, '2016-08-02 08:07:56', '2018-11-28 10:08:51'),
(14, NULL, 'Accreditation Programme', 'accreditation-programme', 'overviewPage', 171, 194, 0, 'secondary', 0, 0, 0, 1, 1, 5, '2016-08-02 08:08:23', '2018-11-28 10:10:41'),
(15, NULL, 'Careers', 'careers', 'careers', 195, 196, 0, 'footer', 0, 0, 0, 1, 1, 4, '2016-08-02 08:19:53', '2018-11-29 02:06:40'),
(16, NULL, 'Legal', 'legal', 'page', 197, 198, 0, 'footer', 0, 0, 0, 1, 1, 4, '2016-08-02 08:22:33', '2018-11-27 02:25:05'),
(17, 4, 'Newsletter', 'homepage-newsletter', 'page', 2, 3, 1, '', 1, 1, 0, 1, 1, 4, '2016-08-03 02:06:16', '2018-09-26 07:46:11'),
(20, 12, 'Specialised Assessment', 'assessment/specialised-assessment', 'page', 108, 141, 1, '', 0, 0, 0, 1, 1, 5, '2016-08-07 05:04:56', '2018-04-17 11:01:17'),
(21, 12, 'CBi Smart', 'assessment/cbi', 'page', 106, 107, 1, '', 0, 0, 0, 1, 1, 5, '2016-08-07 05:05:16', '2018-02-04 19:19:07'),
(27, 20, 'Interpersonal Relationships & Conflict Management', 'assessment/specialised-assessment/interpersonal-relationships-and-conflict-management', 'page', 115, 122, 2, '', 0, 0, 0, 1, 1, 2, '2016-08-07 06:15:28', '2018-02-04 19:19:07'),
(28, 12, 'Ability Assessments', 'assessment/ability-assessments', 'tabsPage', 76, 85, 1, '', 0, 0, 0, 1, 1, 5, '2016-08-07 08:18:51', '2018-02-04 19:19:07'),
(29, 28, 'Saville ', 'assessment/ability-assessments#SWIFT', 'page', 79, 80, 2, '', 0, 0, 0, 1, 1, 4, '2016-08-07 08:19:35', '2018-10-16 07:22:46'),
(30, 28, 'Pearson ', 'assessment/ability-assessments#pearson', 'page', 81, 82, 2, '', 0, 0, 0, 1, 1, 4, '2016-08-07 08:20:11', '2018-10-16 07:21:10'),
(31, 28, 'PSYTECH General ', 'assessment/ability-assessments#psytech-general ', 'page', 83, 84, 2, '', 0, 0, 0, 1, 1, 5, '2016-08-07 08:21:17', '2018-02-04 19:19:07'),
(32, 28, 'Overview', 'assessment/ability-assessments#overview', 'page', 77, 78, 2, '', 0, 0, 0, 1, 1, 5, '2016-08-07 08:21:32', '2018-02-04 19:19:07'),
(33, 10, 'Casestudies', 'knowledge-centre/casestudies', 'casestudies', 20, 25, 1, '', 0, 0, 0, 1, 1, 2, '2016-08-08 01:58:07', '2018-02-04 19:19:07'),
(34, 33, 'LEADing Hoteliers! Maximising Excellence in Rotana Leaders', 'knowledge-centre/casestudies/casestudy/4/leading-hoteliers-maximising-excellence-in-rotana-leaders', 'casestudy', 21, 22, 2, '', 0, 0, 1, 1, 1, 1, '2016-08-08 01:59:06', '2018-02-04 19:19:07'),
(35, 10, 'Event', 'knowledge-centre/event/{id}/{slug}', 'event', 18, 19, 1, '', 0, 0, 0, 1, 1, 3, '2016-08-08 05:08:28', '2018-02-04 19:19:07'),
(37, NULL, 'Programme', '/programme/{id}/{slug}', 'course', 199, 200, 0, '', 0, 0, 0, 1, 1, 1, '2016-08-08 07:09:59', '2018-04-17 11:01:17'),
(39, NULL, 'Saville Myself', 'saville-myself', 'page', 201, 202, 0, '', 0, 0, 0, 1, 1, 4, '2016-08-08 07:50:18', '2018-10-16 07:27:52'),
(40, 12, 'Assessment Services', 'assessment/assessment-services', 'page', 60, 61, 1, '', 0, 0, 0, 1, 1, 4, '2016-08-08 07:52:27', '2018-10-16 07:30:52'),
(41, 12, 'Personality Assessment', 'assessment/personality-assessment', 'tabsPage', 62, 75, 1, '', 0, 0, 0, 1, 1, 2, '2016-08-08 08:09:28', '2018-02-04 19:19:07'),
(42, 1, 'Team', 'about/team', 'team', 8, 9, 1, '', 0, 0, 0, 1, 1, 2, '2016-08-09 02:03:17', '2018-03-08 14:41:04'),
(43, 1, 'Clients', 'about/clients', 'clients', 10, 11, 1, '', 0, 0, 0, 1, 1, 2, '2016-08-09 02:03:44', '2018-02-04 19:19:07'),
(44, NULL, 'Calendar', 'calendar', 'calendar', 31, 38, 0, 'top', 0, 0, 0, 1, 1, 4, '2016-08-09 06:46:56', '2018-12-12 02:57:10'),
(45, 12, 'Assessment Centre Exercises', 'assessment/assessment-centre-exercises', 'page', 88, 89, 1, '', 0, 0, 0, 1, 1, 4, '2016-08-10 06:56:20', '2018-02-04 19:19:07'),
(46, 11, 'Competency Framework Design', 'consulting/competency-framework-design', 'page', 46, 47, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-10 07:32:34', '2018-02-04 19:19:07'),
(47, 11, 'Performance Management', 'consulting/performance-management', 'page', 48, 49, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-10 07:41:56', '2018-02-04 19:19:07'),
(48, 11, 'Succession Planning & Talent Management', 'consulting/succession-and-talent-management', 'page', 50, 51, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-10 07:43:44', '2018-02-04 19:19:07'),
(49, 11, 'Nationalisation Programmes', 'consulting/nationalisation-programmes', 'page', 52, 53, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-10 07:44:41', '2018-02-04 19:19:07'),
(59, 12, 'Situational Judgement', 'assessment/situational-judgement', 'page', 86, 87, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-11 03:22:24', '2018-02-04 19:19:07'),
(60, 12, '360 Surveys', 'assessment/360-surveys', 'tabsPage', 90, 103, 1, '', 0, 0, 0, 1, 2, 2, '2016-08-11 04:45:13', '2018-02-04 19:19:07'),
(61, 60, 'Overview ', 'assessment/360-surveys#overview', 'page', 91, 92, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-11 05:09:24', '2018-02-04 19:19:07'),
(67, 12, 'Organisational Engagement Surveys', 'assessment/organisational-engagement-surveys', 'page', 104, 105, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-11 06:57:15', '2018-02-04 19:19:07'),
(68, 20, 'Career Planning ', 'assessment/specialised-assessment/career-planning', 'page', 109, 114, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-11 07:37:02', '2018-02-04 19:19:07'),
(69, 68, 'Strong Interest Inventory', 'assessments/specialised-assessment/career-planning/strong-interest-inventory', 'page', 112, 113, 3, '', 0, 0, 0, 1, 2, 5, '2016-08-11 07:46:31', '2018-02-04 19:19:07'),
(70, 68, 'Saville MySelf', 'assessment/specialised-assessment/career-planning/myself', 'page', 110, 111, 3, '', 0, 0, 0, 1, 2, 4, '2016-08-11 08:07:58', '2018-10-16 07:24:36'),
(71, 27, 'FIRO-B', 'assessment/specialised-assessment/interpersonal-relationships-and-conflict-management/firo-b', 'page', 116, 117, 3, '', 0, 0, 0, 1, 2, 5, '2016-08-11 08:14:51', '2018-02-04 19:19:07'),
(72, 27, 'FIRO-Business', 'assessment/specialised-assessment/interpersonal-relationships-and-conflict-management/firo-business', 'page', 118, 119, 3, '', 0, 0, 0, 1, 2, 5, '2016-08-11 08:17:44', '2018-02-04 19:19:07'),
(73, 27, 'Thomas Kilman Conflict Mode Instrument (TKI)', 'assessment/specialised-assessment/interpersonal-relationships-and-conflict-management/thomas-killman-conflict-mode-instrument-tki', 'page', 120, 121, 3, '', 0, 0, 0, 0, 2, 5, '2016-08-11 08:20:18', '2018-02-04 19:19:07'),
(74, 20, 'Safety in the Workplace', 'assessment/specialised-assessment/safety-in-the-workplace', 'page', 123, 128, 2, '', 0, 0, 0, 1, 2, 2, '2016-08-11 09:11:19', '2018-04-17 11:58:43'),
(75, 74, 'Hogan Safety ', 'assessment/specialised-assessment/safety-in-the-workplace/hogan-safety', 'page', 126, 127, 3, '', 0, 0, 0, 1, 2, 5, '2016-08-11 09:12:51', '2018-04-17 11:58:43'),
(76, 20, 'Innovation and Change', 'assessment/specialised-assessment/innovation-and-change', 'page', 129, 132, 2, '', 0, 0, 0, 1, 2, 2, '2016-08-11 09:15:02', '2018-04-17 11:01:17'),
(77, 76, 'ME2 ', 'assessment/specialised-assessment/innovation-and-change/me2', 'page', 130, 131, 3, '', 0, 0, 0, 1, 2, 5, '2016-08-11 09:16:20', '2018-04-17 11:01:17'),
(78, 20, 'Mental Toughness and Resilience ', 'assessment/specialised-assessment/mental-toughness-and-resilience', 'page', 133, 136, 2, '', 0, 0, 0, 1, 2, 2, '2016-08-11 09:18:21', '2018-04-17 11:01:17'),
(79, 78, 'MTQ 48', 'assessment/specialised-assessment/mental-toughness-and-resilience/mtq-48', 'page', 134, 135, 3, '', 0, 0, 0, 1, 2, 5, '2016-08-11 09:19:44', '2018-04-17 11:01:17'),
(80, 20, 'Workplace Language ', 'assessment/specialised-assessment/workplace-language', 'page', 137, 140, 2, '', 0, 0, 0, 1, 2, 2, '2016-08-11 09:21:47', '2018-04-17 11:01:17'),
(81, 80, 'Saville Workplace English', 'assessment/specialised-assessment/workplace-language/workplace-english', 'page', 138, 139, 3, '', 0, 0, 0, 1, 2, 4, '2016-08-11 09:22:51', '2018-10-16 07:25:41'),
(83, 44, 'Course', '/calendar/course/{id}/{slug}', 'course', 32, 33, 1, '', 0, 0, 0, 1, 1, 2, '2016-08-11 09:45:31', '2018-02-04 19:19:07'),
(85, 44, 'Course Booking', 'calendar/book/{type}/{id}/{slug}', 'booking', 34, 35, 1, '', 0, 0, 0, 1, 1, 2, '2016-08-11 09:46:36', '2018-02-04 19:19:07'),
(86, 44, 'Course Booking Thank you', 'calendar/book/thank-you', 'pageWithNoSidebar', 36, 37, 1, '', 0, 0, 0, 1, 1, 2, '2016-08-11 09:47:22', '2018-02-04 19:19:07'),
(87, 3, 'Contact Thank You', 'contact/thank-you', 'page', 42, 43, 1, '', 0, 0, 0, 1, 1, 4, '2016-08-11 09:51:45', '2018-11-27 00:56:30'),
(90, 60, 'NEO Primary Colours', 'assessment/360-surveys#neo-primary-colours', 'page', 101, 102, 2, '', 0, 0, 0, 1, 2, 2, '2016-08-11 10:22:45', '2018-07-31 08:30:11'),
(94, 13, 'Leadership Development', 'development/leadership-development', 'page', 144, 145, 1, '', 0, 0, 0, 1, 2, 4, '2016-08-12 04:46:05', '2018-10-15 02:48:24'),
(95, 13, 'Management Skills', 'development/management-skills', 'overviewPage', 148, 163, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-12 04:56:38', '2018-11-28 10:09:57'),
(96, 95, 'Performance Management Skills', 'development/management-skills/performance-management-skills', 'page', 149, 150, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-12 04:58:02', '2018-04-17 11:01:17'),
(97, 14, 'Myers-Briggs Type Indicator® (MBTI®)', 'calendar/course/4/mbti-certificate-program', 'page', 172, 173, 1, '', 0, 0, 1, 1, 2, 2, '2016-08-12 05:10:42', '2018-04-17 11:01:17'),
(98, 14, 'BPS Qualification in Occupational Testing (Ability & Personality) ', 'calendar/course/6/bps-qualification-in-occupation-testing', 'page', 174, 175, 1, '', 0, 0, 1, 1, 2, 3, '2016-08-12 05:20:10', '2018-04-17 11:01:17'),
(99, 14, 'EQi 2.0 & EQ360 ', 'calendar/course/8/eqi-20-and-eq360', 'page', 176, 177, 1, '', 0, 0, 1, 1, 2, 2, '2016-08-12 05:25:31', '2018-04-17 11:01:17'),
(100, 14, 'Strong Interest Inventory ', 'calendar/course/10/strong-interest-inventory', 'page', 178, 179, 1, '', 0, 0, 1, 1, 2, 2, '2016-08-12 05:29:48', '2018-04-17 11:01:17'),
(101, 14, 'FIRO Business ', 'calendar/course/11/firo-business', 'page', 180, 181, 1, '', 0, 0, 1, 1, 2, 2, '2016-08-12 05:38:30', '2018-04-17 11:01:17'),
(103, 14, 'Centre Manager ', 'calendar/course/16/centre-manager', 'page', 184, 185, 1, '', 0, 0, 1, 1, 2, 2, '2016-08-12 05:52:01', '2018-04-17 11:01:17'),
(104, 14, 'AFC Executive Coaching Certification ', 'calendar/course/13/afc-executive-coaching-certification', 'page', 186, 187, 1, '', 0, 0, 1, 1, 2, 2, '2016-08-12 05:59:18', '2018-04-17 11:01:17'),
(106, 14, 'Thomas Kilman Conflict Mode Instrument (TKI) Workshop', 'calendar/course/14/thomas-kilman-conflict-mode-instrument-tki-workshop', 'page', 190, 191, 1, '', 1, 0, 0, 0, 2, 2, '2016-08-12 06:03:22', '2018-04-17 11:01:17'),
(109, 95, 'Motivating & Engaging Others ', 'development/management-skills/motivating-and-engaging-others', 'page', 151, 152, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:35:12', '2018-04-17 11:01:17'),
(110, 95, 'Competency Based Interviewing', 'development/management-skills/competency-based-interviewing', 'page', 153, 154, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:36:04', '2018-04-17 11:01:17'),
(111, 95, 'Coaching Skills for Line Managers ', 'development/management-skills/coaching-skills-for-line-managers', 'page', 155, 156, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:40:17', '2018-04-17 11:01:17'),
(112, 95, 'Managing Change ', 'development/management-skills/managing-change', 'page', 157, 158, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:41:13', '2018-04-17 11:01:17'),
(113, 95, 'Resilence and Emotional Intelligence ', 'development/management-skills/resilence-and-emotional-intelligence', 'page', 159, 160, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:41:56', '2018-04-17 11:01:17'),
(114, 95, 'Managing Conflict ', 'development/management-skills/managing-conflict', 'page', 161, 162, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:42:45', '2018-04-17 11:01:17'),
(115, 13, 'Team Facilitation ', 'development/team-facilitation', 'page', 164, 165, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:44:12', '2018-04-17 11:01:17'),
(116, 13, 'Executive Coaching & Mentoring ', 'development/executive-coaching-and-mentoring', 'page', 166, 167, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:44:57', '2018-04-17 11:01:17'),
(117, 13, 'Development Centres ', 'development/development-centres', 'page', 146, 147, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:46:03', '2018-04-17 11:01:17'),
(118, 13, 'Individual Development Plans ', 'development/individual-development-plans', 'page', 168, 169, 1, '', 0, 0, 0, 1, 2, 5, '2016-08-13 05:46:42', '2018-04-17 11:01:17'),
(119, 4, 'Thank you for Subscription', 'thank-you-for-subscription', 'page', 4, 5, 1, '', 0, 0, 0, 1, 1, 4, '2016-08-14 08:14:18', '2018-11-27 00:51:39'),
(120, NULL, 'Not Found', 'not-found', 'page', 203, 204, 0, '', 0, 0, 0, 1, 1, 2, '2016-08-15 02:04:18', '2018-04-17 11:01:17'),
(121, 41, 'Overview', 'assessment/personality-assessment#overview', 'page', 63, 64, 2, '', 0, 0, 0, 1, 2, 4, '2016-08-19 02:23:52', '2018-10-16 07:31:32'),
(122, 41, 'Assessment', '/assessment/personality-assessment#Wave', 'page', 65, 66, 2, '', 0, 0, 0, 1, 2, 4, '2016-08-19 02:34:39', '2018-10-17 00:27:24'),
(123, 41, 'MBTI (Myers Briggs Type Indicator)', '/assessment/personality-assessment#MBTi', 'page', 67, 68, 2, '', 0, 0, 0, 1, 2, 2, '2016-08-19 03:06:49', '2018-07-31 07:34:56'),
(124, 41, 'NEO ', '/assessment/personality-assessment#neo', 'page', 71, 72, 2, '', 0, 0, 0, 1, 2, 4, '2016-08-19 03:10:27', '2018-02-04 19:19:07'),
(125, 41, 'EQ', 'assessment/personality-assessment#eq', 'page', 69, 70, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-19 03:18:36', '2018-02-04 19:19:07'),
(126, 41, 'Hogan', 'assessment/personality-assessment#hogan', 'page', 73, 74, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-19 03:21:51', '2018-02-04 19:19:07'),
(127, 60, 'Innovative 360', 'assessment/360-surveys#innovative-360', 'page', 93, 94, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-21 10:37:00', '2018-02-04 19:19:07'),
(128, 60, 'Saville Performance 360', 'assessment/360-surveys#performance-360', 'page', 95, 96, 2, '', 0, 0, 0, 1, 2, 4, '2016-08-21 10:51:33', '2018-10-16 07:20:31'),
(129, 60, 'EQ 360', 'assessment/360-surveys#eq-360', 'page', 97, 98, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-21 10:55:49', '2018-02-04 19:19:07'),
(131, 60, 'Hogan 360', 'assessment/360-surveys#hogan-360', 'page', 99, 100, 2, '', 0, 0, 0, 1, 2, 5, '2016-08-21 11:02:35', '2018-02-04 19:19:07'),
(133, 14, 'NEO Conversion', 'calendar/course/15/neo-conversion', 'page', 188, 189, 1, '', 0, 0, 1, 1, 2, 2, '2016-08-25 07:06:14', '2018-04-17 11:01:17'),
(136, 14, 'Assessor Skills', '/calendar/course/17/assessor-skills ', 'page', 182, 183, 1, '', 0, 0, 1, 1, 3, 2, '2016-08-28 05:21:19', '2018-04-17 11:01:17'),
(137, 14, 'Saville Conversion Online', 'saville-conversion-online', 'page', 192, 193, 1, '', 0, 0, 0, 1, 3, 4, '2016-08-29 02:12:35', '2018-10-16 07:27:03'),
(138, 33, 'Casestudy', 'knowledge-center/casestudies/casestudy/{id}/{slug}', 'casestudy', 23, 24, 2, '', 0, 0, 0, 1, 1, 2, '2016-09-04 07:02:33', '2018-02-04 19:19:07'),
(139, 10, 'Event Booking', 'knowledge-centre/book/{type}/{id}/{slug}', 'booking', 26, 27, 1, '', 0, 0, 0, 1, 2, 2, '2016-11-02 14:06:04', '2018-02-04 19:19:07'),
(140, 10, 'Event Booking Thank you', 'knowledge-centre/book/thank-you', 'pageWithNoSidebar', 28, 29, 1, '', 0, 0, 0, 1, 2, 4, '2016-11-02 14:08:43', '2018-02-04 19:19:07'),
(141, NULL, 'General Error', 'general-error', 'page', 205, 206, 0, '', 1, 0, 0, 1, 2, 2, '2016-11-07 13:52:31', '2018-04-17 11:01:17'),
(142, NULL, 'Search Result', 'search-result', 'page', 207, 208, 0, '', 1, 0, 0, 1, 2, 2, '2016-12-06 17:40:11', '2018-04-17 11:01:17'),
(143, NULL, 'Homev2', '/', 'homev2', 211, 212, 0, '', 0, 0, 0, 1, 4, 2, '2017-08-14 08:57:45', '2018-12-11 06:42:11'),
(144, NULL, 'Your Career Guidance Hero', 'your-career-guidance-hero', 'FullWidth', 209, 210, 0, 'secondary', 1, 0, 0, 1, 4, 4, '2017-12-22 16:56:28', '2018-04-17 11:01:17'),
(145, 74, 'Safety Quotient™', 'assessment/specialised-assessment/safety-in-the-workplace/safety-quotient', 'page', 124, 125, 3, '', 0, 0, 0, 1, 4, 4, '2018-04-17 11:01:17', '2018-04-17 11:58:43');

-- --------------------------------------------------------

--
-- Table structure for table `page_translations`
--

CREATE TABLE `page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_translations`
--

INSERT INTO `page_translations` (`id`, `page_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(3, 4, 'en', 'Homepage', '', 'PSI Middle East', '', 'Talent Management solutions from qualified occupational psychologists & learning & development specialists across the Middle East. Contact us today & see how we can improve your organisation. '),
(4, 1, 'en', 'About PSI Middle East', '<h3 style=\"text-align: justify;\">PSI Middle East is one of the largest teams of Occupational&nbsp;Psychologists and learning and development specialists in the Gulf.&nbsp;<strong>We specialise in individual, team and organisational assessment and development</strong>.</h3>\r\n<p style=\"text-align: justify;\">We work with organisations to select, assess and develop talent and teams, and support organisational development through the design and implementation of world-class Strategic HR Frameworks.</p>\r\n<p style=\"text-align: justify;\">PSI Middle East has been headquartered in the United Arab Emirates since 1999. Today we represent 12 International Test Publishers in the GCC, and have brought to the region world-renowned and highly-respected psychometric tools and leadership assessments to support effective individual and organisational development.</p>\r\n<p style=\"text-align: justify;\">Our team work in English and Arabic with Young Talent through to Senior Executives in the United Arab Emirates, Saudi Arabia, Oman, Bahrain, Kuwait and Jordan as well as &nbsp;Europe. &nbsp;We bring international expertise and deep regional experience to ensure our products and services are in line with global best practices and achieve enduring business outcomes.</p>\r\n<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/1ba33088ff94d371567f25ded3f24338a7172d48.pdf&amp;name=PSI Middle East Corporate Folder.pdf\">PSI Middle East Brochure</a></p>\r\n<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/40407b9fa901bd5843a3a570e4b4035b643c230e.pdf&amp;name=PSI Innovative Corporate Bro Ara.pdf\">PSI Middle East Brochure - Arabic</a></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>', 'About Us', '', 'We work with organisations to select, assess and develop talent and teams, and support organisational development through the design and implementation of world-class Strategic HR Frameworks.'),
(5, 1, 'ar', 'عن PSI Middle East ', '<h3 class=\"right\">تعد شركة PSI الشرق الأوسط&nbsp;واحدة من أكبر فرق المختصين بعلم النفس المهني و التعليم و التطوير في الخليج العربي&nbsp;</h3>\r\n<p class=\"left\">&nbsp;نقوم بالعمل مع المؤسسات من أجل اختيار المواهب والفرق وتقييمها وتطويرها، بجانب دعم التطوير المؤسسي من خلال تصميم وتنفيذ أطر للموارد البشرية الاستراتيجية ذات معايير عالمية. يقع مقر شركة&nbsp;PSI االشرق الأوسط في الإمارات العربية المتحدة منذ أكثر من ١٥ سنة، كما نقوم بتمثيل ١٢ ناشراً للإختبارات العالمية في منطقة الشرق الأوسط، وقد قمنا بتوفير أدوات قياس سيكولوجية وأدوات تقييم قيادية مرموقة في منطقة الخليج بهدف دعم التطوير الفردي والمؤسسي الفعال.</p>\r\n<p class=\"left\">يعمل فريقنا في منطقة دول الخليج لتنفيذ مجموعة من مبادرات الموارد البشرية الاستراتيجية. تتضمن مجالات خبرتنا المتخصصة في الأنشطة التالية:</p>\r\n<ul class=\"right\">\r\n<li class=\"left\">مراكز التقييم والتطوير لأغراض التوظيف والتطوير والترقية</li>\r\n<li class=\"left\">استراتيجيات الموارد البشرية للتعاقب الوظيفي، إدارة المواهب، ارتباط الموظفين، تقييم الأداء وأطر الكفاءات السلوكية</li>\r\n<li class=\"left\">اعتماد أدوات القياس السلوكية العالمية</li>\r\n<li class=\"left\">التدريب التنفيذي والإرشاد</li>\r\n<li class=\"left\">تدريب مهارات الإدارة</li>\r\n<li class=\"left\">برامج التوطين</li>\r\n</ul>\r\n<p class=\"right\">&nbsp;</p>', '', '', ''),
(6, 5, 'en', 'News & Events', '', '', '', ''),
(13, 3, 'en', 'Contact', '', 'Contact', '', 'Click here and get in touch with us today!'),
(14, 10, 'en', 'Knowledge Centre', '<p>Knowledge Centre</p>', 'Knowledge Centre Case Studies', '', 'Explore our case studies and meet us at different exhibitions, expo\'s and events. '),
(15, 11, 'en', 'Consulting', '<p style=\"text-align: justify;\">To ensure competitive advantage it is vital the right Talent Strategies and Frameworks are in place to drive your business outcomes. We will work closely with you to develop, communicate and embed comprehensive and integrated talent strategies to deliver business results.</p>', 'Consultancy Competency & Talent Management', '', 'We develop talent strategies to deliver performance management & competency improvement by succession planning & talent management. '),
(16, 12, 'en', 'Assessment', '<p style=\"text-align: justify;\">As Occupational Psychologists we are licensed to provide world-class assessment services to support recruitment, selection for promotion, identification of high potential talent and succession planning. We are able to run individual assessments for critical hires, through to large-scale assessment programmes.</p>', 'Employee Assessments & Performance Services', '', 'We are a licensed provider of employee assessments including personality & ability assessments. Visit us today to see how we can help you. '),
(17, 13, 'en', 'Development', '<p style=\"text-align: justify;\">Having identified the learning and development requirements across your organisation,&nbsp;PSI Middle East offer a range of learning and development options to optimise your team performance. We seek to create and deliver powerful, experiential interventions to help teams flourish and share a heartfelt commitment to organisational goals.</p>', 'Development Programs & Courses', '', 'We offer a range of learning and development options to optimise your team performance. We create and deliver powerful, experiential interventions.'),
(18, 14, 'en', 'Accreditation Programmes', '<p style=\"text-align: justify;\">We are qualified and licensed to run a broad range of Internationally Accredited Programmes to help you transfer assessment and development services in-house. We are proud to represent 12 International Test Publishers across the Gulf to deliver a suite of world-class programmes and products. All of our Facilitators are licensed and experienced to deliver the programmes and subject to ongoing quality review.</p>\r\n<h3 style=\"text-align: justify;\">DOWNLOADS</h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/1b975e7f0c0c271618888a8f7c842903546e525e.pdf&amp;name=Accreditation Calendar 2018.pdf\">2018 Accreditation Calendar</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/da19d0f7c96981b2769dfc45515b40e5f1d9362e.pdf&amp;name=2019 PSI Accreditation and Workshop Catalogue.pdf\">2019 Accreditation and Workshop Calendar</a></p>\r\n<p>&nbsp;</p>', 'International Licensed Accreditation Programmes', '', 'We are qualified and licensed to run a broad range of Internationally Accredited Programmes to help you transfer assessment and development services in-house. '),
(19, 15, 'en', 'Careers with PSI Middle East', '<p style=\"text-align: justify;\">Welcome to PSI Middle East Careers! The quality of our people is a key driver and contributor to our commercial performance and business success! We believe it is the quality, enthusiasm and commitment of our people that gives us our leading edge. We are committed to the ongoing personal and professional development of all our staff.</p>\r\n<p style=\"text-align: justify;\">Are you passionate about helping organisations across the GCC realise the power of their people through customised, expert talent management solutions? If yes, then take a look at our current vacancies and if there&rsquo;s nothing here that matches your skill set right now, do come back and visit us regularly to see if new opportunities have arisen.</p>\r\n<p style=\"text-align: justify;\">We are all committed to our Vision and Mission and living by our Values. We value Team, Integrity, Customer Focus and Innovation.</p>\r\n<p style=\"text-align: justify;\"><span class=\"bold\">Team</span>: We value each other, we work collaboratively and never forget to have fun</p>\r\n<p style=\"text-align: justify;\"><span class=\"bold\">Integrity</span>: We work with integrity, we are transparent, ethical and respectful</p>\r\n<p style=\"text-align: justify;\"><span class=\"bold\">Customer Focus</span>: Our clients can expect us to be responsive, flexible, proactive, focused on quality and passionate about what we do</p>\r\n<p style=\"text-align: justify;\"><span class=\"bold\">Innovation</span>: We value creativity, being expert and informed and thoughtful about our work</p>', 'Careers with us', '', 'We work with integrity, we are transparent, ethical and respectful. We value creativity and are thoughtful about our work.'),
(20, 16, 'en', 'Legal', '<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Terms of Use</strong></p>\r\n<p style=\"text-align: justify;\">The purpose of this website is to publicise the various products and services provided to all customers of&nbsp;PSI Middle East.</p>\r\n<p style=\"text-align: justify;\">The information contained on this website and its sub-sites are for general information purposes only. The information is provided by PSI Middle East and while PSI Middle East continually strive to keep the information up to date and correct, PSI Middle East makes no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>\r\n<p style=\"text-align: justify;\">In no event will PSI Middle East be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of or in connection with the use of this website.</p>\r\n<p style=\"text-align: justify;\">Through this website you are able to link to other websites which are not under the control of PSI Middle East. PSI Middle East have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorsement of the views expressed within them.</p>\r\n<p style=\"text-align: justify;\">Every effort is made to keep the website up and running smoothly. However, PSI Middle East takes no responsibility for and will not be liable for the website being temporarily unavailable due to technical issues beyond PSI Middle East control.</p>\r\n<p style=\"text-align: justify;\">PSI Middle East is committed to respecting your privacy and protecting your personal information. We recognise our obligation to keep sensitive information secure and have created this statement to share and explain the current information management practices on our websites. The handling of all personal information by&nbsp;PSI Middle East is governed by the UAE and Dubai privacy and security acts.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Privacy Policy</strong></p>\r\n<p style=\"text-align: justify;\">We are committed to protecting your privacy whether you are browsing for information or conducting business electronically.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Collection and use of online information</strong></p>\r\n<p style=\"text-align: justify;\">When you visit our websites, we will not collect your personal information unless you choose to use and receive online products and services that require it. All data capture uses up-to-date security mechanisms to ensure the integrity and confidentiality of information and systems.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Collection of IP address</strong></p>\r\n<p style=\"text-align: justify;\">Our web server(s) may automatically collects your IP address when you visit our websites (your IP address is your computer\'s unique address that lets other computers attached to the Internet know where to send data, but does not identify you individually). We use your IP address to help diagnose problems with our server and to compile statistics on site usage.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Use of cookies</strong></p>\r\n<p style=\"text-align: justify;\">PSI Middle East websites may use cookies. Cookies are generally used to make it more convenient for users to move around our websites. If you choose to, they may be used to \'remember\' your password and make it easier and faster to log-in to certain sites. These types of cookies need to be stored on your computer\'s hard drive.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Protection of personal information</strong></p>\r\n<p style=\"text-align: justify;\">The personal information you enter on our websites is only available to the authorised PSI Middle East employees who have a need to know it. It will not be available for public inspection without your consent. Also, no website user-information will be shared, sold, or transferred to any third party without your prior consent.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Important notice</strong></p>\r\n<p style=\"text-align: justify;\">Some of our websites link to other sites created and maintained by other public and/or private sector organizations. We provide these links solely for your information and convenience. When you link to an outside website, you are leaving the PSI Middle East website and our information management policies no longer apply.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Third Party Website Disclaimer</strong></p>\r\n<p style=\"text-align: justify;\">PSI Middle East website may periodically provide links to third-party websites (Third-Party Sites). This Agreement governs this Site only and not any Third-Party Sites. Our decision to provide a link to a Third-Party Site is not an endorsement of the content or services in that linked Third-Party Site. PSI Middle East does not operate or control any Third-Party Sites or any information contained therein. PSI Middle East makes no representation or warranty as to the availability or accuracy on information contained in any Third-Party Site and expressly disclaims any responsibility for the content and the accuracy of the information provided in any Third Party Sites. You agree to hold PSI Middle East harmless and absolve PSI Middle East from any and all liability, losses, damages and/or expenses incurred in connection with your use of any Third Party Site. You further agree that PSI Middle East shall not be liable or responsible to you in any way for any products or services available on any Third-Party Sites or any transaction carried out on or via a Third-Party Site. If you decide to access linked Third-Party Sites, you do so at your own risk. You agree that PSI Middle East is not liable for any information obtained, viruses or malware, operational failures, interruptions of service or connection failures experienced due to your use of a Third-Party Site. You should direct any concerns regarding any Third-Party Sites to the administrator of the applicable Third-Party Site. You expressly acknowledge that the security and privacy policies of any Third-Party Site may be different to PSI Middle East policies. You should refer to the separate terms of use, privacy policies and other rules posted on Third-Party Sites before you use any Third-Party Site.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">External links from PSI Middle East Website</strong></p>\r\n<p style=\"text-align: justify;\">A \"link\" from the PSI Middle East website may be defined as:</p>\r\n<p style=\"text-align: justify;\">- Hyperlinks activated by clicking on an image or text, leading to a web page that does not reside on the Innovative HR Solutions (IHS) website \"explicit\"</p>\r\n<p style=\"text-align: justify;\">- Mention of URL address providing users with the address of a web page that does not reside on the Innovative HR Solutions (IHS) website \"implicit\"</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">External links To PSI Middle East website</strong></p>\r\n<p style=\"text-align: justify;\">A site that links to the&nbsp;PSI Middle East website should not misrepresent its relationship with PSI Middle East. This means:</p>\r\n<p style=\"text-align: justify;\">- A browser or border environment should not be created around PSI Middle East content</p>\r\n<p style=\"text-align: justify;\">- The PSI Middle East logo should not be used without the permission of PSI Middle East</p>\r\n<p style=\"text-align: justify;\">- The site should not present false/inaccurate information about PSI Middle East services</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Governing Law and Jurisdiction</strong></p>\r\n<p style=\"text-align: justify;\">All matters regarding the use of this website and or any related matter shall be governed by and construed in accordance with the laws and regulations of, and applicable in, the emirate of Dubai and federal laws of the United Arab Emirates.</p>\r\n<p style=\"text-align: justify;\">The courts of Dubai will have exclusive jurisdiction over any claim arising from the use of this website and or any related matter, or related to, a visit to the website but we retain the right to bring proceedings against you for breach of the use of the website in your country of residence or any other relevant country.</p>', '', '', ''),
(21, 17, 'en', 'Newsletter', '<p>Stay informed on the latest thinking &amp; talent updates.</p>\r\n<p>Attend free professional development breakfasts, webinars &amp; events.</p>\r\n<p>Find out the latest about our upcoming accreditation programmes.</p>', 'Innovative HR Solutions | Middle East', '', 'HR solutions from qualified occupational psychologists & learning & development specialists across the Middle East. Contact us today & see how we can improve your organisation. '),
(24, 20, 'en', 'Specialised Assessment', '<p style=\"text-align: justify;\">At times our scope of work is more specific and we need assessments for a particular purpose. Perhaps it is to support Graduates and Young Talent with their career choices. Or there could be a special challenge for the business such as a team that is in conflict. All the following tools have been selected on the basis of their validity, reliability, ease of use, cost efficiency and objectivity.</p>', 'Specialised Assessment Career Analysis', '', 'We provide assessments and career planning tools which measures an individual\'s potential talents across a range of areas in the work place.'),
(25, 21, 'en', 'CBi Smart', '<p style=\"text-align: justify;\">CBI-Smart&trade; is an online Competency-Based Interview question builder that enables interviewers to quickly create Competency-Based Interview Guides from a database of over 1,000 proven CBI questions, assessing 35 popular competencies.</p>\r\n<p style=\"text-align: justify;\">Alternatively, we can customise CBI-Smart&trade; to include your competencies and design the questions to your Competency Framework in English and Arabic at different levels.</p>\r\n<p style=\"text-align: justify;\">CBI-Smart&trade; ensures that all your Competency Based Interviews are structured, questions relate directly to the essential competencies that have been identified as key to effective performance in a given role. Structured interviews, such as CBIs, have been found to be more than twice as effective as unstructured interviews at predicting a candidate&rsquo;s performance.</p>\r\n<p style=\"text-align: justify;\">To add to this, we offer a complete range of competency based interview solutions including training in competency design and interviewing skills.</p>', 'CBi Smart', '', 'CBI-Smart™ is an online Competency-Based Interview question builder that enables interviewers to quickly create Competency-Based Interview Guides from a database of over 1,000 proven CBI questions, assessing 35 popular competencies. '),
(31, 27, 'en', 'Interpersonal Relationships & Conflict Management', '', '', '', ''),
(32, 28, 'en', 'Ability Assessments', '', 'Ability Assessments & Tests', '', 'Our ability tests include measures of Abstract Reasoning, General Aptitudes and Practical Aptitudes. '),
(33, 29, 'en', 'Saville', '<h2><img class=\"center\" title=\"Saville\" src=\"/uploads/images/tn-600-31b5f239562b84eefdc5b19b4a52ef2a76be3a8e.png\" width=\"309\" height=\"99\" />Swift Analysis Aptitude (V, N, D) Combination</h2>\r\n<p>Measures critical reasoning through short verbal (6 min), numerical (6 min) and diagrammatic (6 min) sub-tests.</p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p>Directors, Managers, Professionals, Graduates &amp; Management Trainees</p>\r\n<h2>Swift Executive Aptitude (V, N, A) Combination</h2>\r\n<p style=\"text-align: justify;\">Measures critical reasoning through short verbal (6 min), numerical (6 min) and abstract (6 min) sub-tests.</p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p>Directors, Managers, Professionals, Graduates &amp; Management Trainees</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Swift Analysis Aptitude (V &amp; N) Combination</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Measures critical reasoning through short verbal (12 min) and numerical (12 min) sub-tests and is suitable for all high-level roles.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Directors, Managers, Professionals, Graduates &amp; Management Trainees</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Abstract Aptitudes</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">Measures abstract critical reasoning across all roles.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Analysis Aptitudes (V, N, D or A) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Verbal Analysis assesses the ability to evaluate complex written information. Numerical Analysis assesses the ability to evaluate numerical data. Diagrammatic Analysis assesses the ability to evaluate processes represented through diagrams.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Directors, Managers, Professionals, Graduates &amp; Management Trainees</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Professional Aptitudes (V, N, D) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Verbal Analysis assesses the ability to evaluate complex written information. Numerical Analysis assesses the ability to evaluate numerical data. Diagrammatic Analysis assesses the ability to evaluate processes represented through diagrams.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Directors, Managers &amp; Professionals</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Work Aptitudes (V, N, D) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Verbal Analysis assesses the ability to evaluate complex written information. Numerical Analysis assesses the ability to evaluate numerical data. Diagrammatic Analysis assesses the ability to evaluate processes represented through diagrams.</span></p>\r\n<h3 style=\"text-align: justify;\">TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Graduates &amp; Management Trainees</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Swift Comprehension Aptitude (V, N, E) Combination</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">Measures general reasoning through short verbal (4 mins), numerical (4 mins) and checking (90 secs) sub-tests.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Operational, Commercial, Customer &amp; Administrative Staff at apprentice or entry-level</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Comprehension Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Verbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Service Sector Apprentices, Operational, Commercial, Customer &amp; Administrative Staff</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Operational Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Verbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Operational Staff in Manufacturing, Engineering, Construction &amp; Transport</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Commercial Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">Verbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Commercial Staff in Sales, Marketing, Business Development &amp; Financial Services</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Customer Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Verbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Customer Staff in Call Centres, Hospitality, Leisure, Health &amp; Education</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Administrative Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Verbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Administrative Staff in Private &amp; Public Sector Offices</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Swift Apprentice Aptitude (V, N, E, S, M, D) Combination</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Measures both technical and comprehension reasoning through short verbal (4 mins), numerical (4 mins), checking (90 secs), spatial (3 mins), mechanical (3 mins) and diagrammatic (4 mins) sub-tests. </span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Operational, Technical, Engineering, Manufacturing &amp; Construction Apprentices </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\">Swift Technical Aptitude (S, M, D) Combination</h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Measures practical reasoning through short spatial (3 min), mechanical (3 min) and diagrammatic (4 min) sub-tests.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Technical Apprentices, Production, Construction, Engineering &amp; Scientific Staff e.g. production workers, apprentices, engineers, designers and scientists</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\">Technical Aptitudes (S, M, D) Single Tests</h2>\r\n<p style=\"text-align: justify;\"><span class=\"s1\">Spatial Reasoning assesses the ability to recognise shapes. Mechanical Reasoning assesses mechanical understanding through items that present a problem with a number of possible answers. Diagrammatic Reasoning assesses the ability to evaluate processes represented through diagrams.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Technical Apprentices, Production, Construction, Engineering &amp; Scientific Staff</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\">Practical Aptitudes (S, M, D) Single Tests</h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Spatial Reasoning assesses the ability to recognise shapes. Mechanical Reasoning assesses mechanical understanding through items that present a problem with a number of possible answers. Diagrammatic Reasoning assesses the ability to evaluate processes represented through diagrams.</span></p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p class=\"p1\"><span class=\"s1\">Production, Construction, Engineering &amp; Scientific Staff</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<p class=\"p1\"><span class=\"s1\">&nbsp;</span></p>', '', '', ''),
(34, 30, 'en', 'Pearson', '<h2 class=\"p1\"><span class=\"s1\">PEARSON Ravens Standard Progressive Matrices (SPM)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Non-verbal measure of mental ability, helping to identify individuals with advanced observation and clear thinking skills who can handle the complexity and ambiguity of the modern workplace. Ravens offers information about someone\'s capacity for analysing and solving problems, abstract reasoning, and the ability to learn &ndash; and reduces cultural bias with a nonverbal approach.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Supervisory/entry level management positions and mid-level individual contributor positions</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">PEARSON Ravens Advanced Progressive Matrices (APM)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Non-verbal measure of mental ability, helping to identify individuals with advanced observation and clear thinking skills who can handle the complexity and ambiguity of the modern workplace. Ravens offers information about someone\'s capacity for analysing and solving problems, abstract reasoning, and the ability to learn &ndash; and reduces cultural bias with a nonverbal approach.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Senior management positions and high level individual contributor positions </span></p>\r\n<p class=\"p1\">&nbsp;</p>', '', '', ''),
(35, 31, 'en', 'PSYTECH', '<h2 class=\"p1\"><span class=\"s1\">PSYTECH General Reasoning Test Battery (GRT2)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">A comprehensive, detailed and accurate measure of mental ability. This test is designed to assess reasoning power for those of general ability.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All levels of staff</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">PSYTECH Graduate Reasoning Test Battery (GRT1)</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">A comprehensive and in-depth measure of mental capacity designed to assess high level reasoning ability.&nbsp;Measure of Verbal, Numerical and Abstract reasoning.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Managers and Graduates&nbsp;</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">PSYTECH Critical Reasoning Test Battery (CRTB2)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Provides a detailed and accurate measure of critical reasoning (verbal and numerical). This test has been specifically designed for management assessment.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Managers</span></p>', '', '', ''),
(36, 32, 'en', 'Overview', '<p style=\"text-align: justify;\">Ability tests or aptitude tests are a standardised way of assessing how well people typically perform in varying work tasks, respond to problems, and are able to demonstrate learning agility and critical thinking skills. They have the advantage of measuring potential rather than simply academic performance, and are often used to make predictions about how people will perform in a work setting. Candidate results are scored by comparing their performance with the results of others who have completed the same assessments; known as a &lsquo;Norm Group&rsquo;. All of our aptitude and ability tests have a range of Norm Groups to ensure assessment at the appropriate level.</p>\r\n<p style=\"text-align: justify;\">We provide a range of ability tests for different purposes. These include measures of Abstract Reasoning, General Aptitudes (Verbal, Numerical and Perceptual Reasoning) and Practical Aptitudes (Spatial Ability and Mechanical Aptitude).</p>\r\n<p style=\"text-align: justify;\">To purchase ability tests you are required to be certified in <a href=\"/calendar/course/6/bps-qualification-in-occupation-testing\">BPS Qualification in Occupational Testing (Ability &amp; Personality).</a></p>', '', '', ''),
(37, 6, 'en', 'Article', '', '', '', ''),
(38, 33, 'en', 'Casestudies', '', 'Case Studies & Reviews', '', 'View our case studies and reviews from our clients. '),
(39, 34, 'en', 'LEADing Hoteliers! Maximising Excellence in Rotana Leaders', '', '', '', ''),
(40, 35, 'en', 'Event', '', '', '', ''),
(42, 37, 'en', 'Programme', '', '', '', ''),
(44, 39, 'en', ' Myself', '<p>Career planning tool for high school, college and University student as well as Young Talent and people in transition to make fulfilling career choices</p>\r\n<h3>TARGET CATEGORY</h3>\r\n<p>Fresh Graduates and Younger Talent</p>', '', '', ''),
(45, 40, 'en', 'Assessment Services', '<p style=\"text-align: justify;\">The cost of selecting the wrong person albeit for recruitment or selection for promotion or succession planning, can run into the hundreds of thousands or even millions of dollars; not to mention the potential impact to morale and productivity. We work with a number of world class organisations to assess key hires as part of the recruitment and selection process.</p>\r\n<p style=\"text-align: justify;\">When we are asked to assess on behalf of our clients we take a holistic approach. We seek demonstration of key leadership behaviours, capabilities i.e. critical thinking skills, learning agility, problem solving capacity, as well as values and environment fit to the organisation.</p>\r\n<p style=\"text-align: justify;\">We design our approach in line with our clients and the critical success factors of the role and organisation. On one end of the continuum we are able to gain valuable insights through the use of world-class personality and ability psychometrics combined with a Structured Interview. To support these findings we can also include a work sample or conduct a full Assessment Centre with observable assessment centre exercises (business simulation exercises). Most importantly, our clients can be sure that we will provide clear recommendations for selection and or development supported by action plans for the way forward.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">We have Arabic and English Assessment tools and materials for all kinds of Assessment requirements. Please reach out to our team for more info today! 00971 4 390 2778</p>', 'Professional Ability Assessment Services', '', 'We are licensed and qualified to provide several assessment services, like personality & ability assessments. '),
(46, 41, 'en', 'Personality Assessments', '', 'Personality Tests & Assessments', '', 'Personality assessments from IHS - licensed and qualified psychologists. Get in touch to see how we can help you today. '),
(47, 42, 'en', 'The team', '<p>Our Senior Team comprises an expert group of Organisational Psychologists, Leadership Development Professionals and Human Capital specialists. Our combination of skills, background and experience places us in a unique position to help your organisation design leading HR strategies and develop talent with an uncompromising focus on practical implementation and business results.</p>', 'The Team ', '', 'Meet the team of Organisational Psychologists, Leadership Development Professionals and Human Capital specialists at IHS.'),
(48, 43, 'en', 'Our Clients', '<p>Our teams have successfully designed and implemented a range of strategic HR initiatives with a number of organisations across various industries.</p>', 'Our Clients', '', 'Our teams have successfully designed and implemented a range of strategic HR initiatives with a number of organisations across various industries.'),
(49, 44, 'en', '2019 Calendar', '', '', '', ''),
(50, 45, 'en', 'Assessment Centre Exercises', '<p style=\"text-align: justify;\">Assessment Centre exercises are business simulations which are based on \'day in the life\' business problems and allow trained Assessors to assess leadership behaviours in action. Observing individuals evaluating business critical problems, taking decisions and communicating the way forward, provides us with an excellent opportunity to assess an individual&rsquo;s compatibility for a specific role or to determine their general leadership potential.</p>\r\n<p style=\"text-align: justify;\">These exercises enable the candidate to demonstrate various skills, behaviours and attitudes to achieve an optimum outcome to the task at hand. As with real life, business simulation exercises are designed for multiple settings; working independently, working one-to-one and working within a group. The exercises are chosen on the basis of their relevance to the assessment criteria; level of seniority, work context and job-critical competencies. Our library holds over 250 different exercises including:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>Assigned/Non-assigned Group Discussions</li>\r\n<li>Group Activities</li>\r\n<li>Internal/External Role Plays</li>\r\n<li>Structured Interviews</li>\r\n<li>Analysis Exercises/Case Studies culminating in a Report and /or Presentation</li>\r\n<li>In-Basket Exercise</li>\r\n<li>Fact Find Exercise</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Exercises are available to Certified Assessors who have been trained to observe and record candidate behaviour, classify and rate performance. We source from two of the international leaders in test publishing; A&amp;DC and Global Leader. We can guide you to choose the combination of exercises that is most suitable for your needs and train you to run your own Centres. Equally, we can manage the entire Assessment/Development Centre on your behalf.</p>\r\n<p style=\"text-align: justify;\">Want to become a certified Assessor and Centre Facilitator? Take the British Psychological Society recognised Assessor Skills and Centre Manager course with Innovative HR Solutions Today!</p>\r\n<p style=\"text-align: justify;\">Assessor Skills: Click here for <a href=\"/calendar/course/17/assessor-skills\">Course Details</a></p>\r\n<p style=\"text-align: justify;\">Centre Manager: Click here for <a href=\"/calendar/course/16/centre-manager\">Course Details</a></p>\r\n<p style=\"text-align: justify;\">Check out Global Leader Exercises: <a href=\"/downloadFile?path=uploads/documents/cc615c467b3b8e59b382ed7de1bb96bfe1e5bff3.pdf&amp;name=Global Leader_Client Brochure.pdf\">Click Here</a></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>', 'Assessment Centre Exercises Business Simulations', '', 'Assessment Centre Exercises enables candidates to demonstrate various skills, behaviors and attitudes to achieve an optimum outcome to the task at hand. '),
(51, 46, 'en', 'Competency Framework Design', '<p style=\"text-align: justify;\">Competencies are skills and abilities described in behavioural terms that are coachable, observable, measurable, and critical to effective organisational performance. They form a common language about successful behaviours within an organisation creating the foundation for effective and integrated HR processes that drive business performance. Competency Frameworks can be used to support recruitment, selection and promotion, employee development, performance management, career planning and succession planning activities.</p>\r\n<p style=\"text-align: justify;\"><span class=\"italic\">Does your Competency Framework integrate talent management and business strategy to provide employees with a clear understanding of the behaviours and skills required? Do they help individuals to understand the desired culture of your organisation which will in turn help them to realise the strategic vision?</span></p>\r\n<p style=\"text-align: justify;\">We start with visioning exercises and strategic conversations at the most senior leadership level to fully understand the strategy and how certain behaviours will drive and enable the plan. We will then work closely with you to design a creative and engaging implementation strategy across the organisation.</p>', 'Competency Improvement Framework Design', '', 'Competency Frameworks can be used to support recruitment, selection and promotion, employee development, performance management, career planning and succession planning activities.'),
(52, 47, 'en', 'Performance Management', '<p style=\"text-align: justify;\">A Performance Management process is intended to guide the expectations of individual, team and organisational performance. It provides a meaningful process by which employees can be rewarded for noteworthy contributions to the organisation, and provides a mechanism to improve individual and organisational performance. Establishing actionable, role-specific, and achievable performance goals is critical to the success of any performance initiative.</p>\r\n<p style=\"text-align: justify;\">Feedback is a natural part of performance management, and specific and timely feedback to employees about their performance against expectations provides the foundation for discussing developmental needs. Effective Performance Management will ensure employees&rsquo; goals are aligned with the overall organisation&rsquo;s business goals, mission, vision, and values. It will also enable a Personal Development Plan consistent with organisational goals and specific needs and ensure fairness and objectivity in reward and recognition.</p>\r\n<p style=\"text-align: justify;\">We work with organisations to design performance management strategy, policy, procedures and the accompanying templates. We can also support with effective communication templates, training programmes and implementation strategy.</p>', 'Performance Management Consulting Process', '', 'We support with effective communication templates, training programs and implementation strategy. Want to see more? Visit our website today.'),
(53, 48, 'en', 'Succession Planning & Talent Management', '<p style=\"text-align: justify;\">Succession Planning is the ongoing, systemised process of identifying, developing and retaining your organisations &lsquo;high potential&rsquo; individuals. The intent is to create a talent pipeline to ensure key leadership positions and critical roles are occupied by appropriately skilled people now and in the future. This process is fundamentally guided by the strategy, business performance and the requirements set at each level / job role.</p>\r\n<p style=\"text-align: justify;\">Who should be considered talent? What do you do to develop talent once identified? Who should stay in the talent pool? The key components of our programme strategies include;</p>\r\n<ul>\r\n<li style=\"text-align: justify;\">Determining what factors are critical for success in your organisation.</li>\r\n<li style=\"text-align: justify;\">Creating a competency model.</li>\r\n<li style=\"text-align: justify;\">Determining key roles and career paths throughout the organisation.</li>\r\n<li style=\"text-align: justify;\">Having a process to identify employees who possess required&nbsp;skills or can be coached and trained to attain the necessary&nbsp;skills in the future.</li>\r\n<li style=\"text-align: justify;\">Spending the necessary time and resources to prepare individuals for future roles.</li>\r\n</ul>', 'Succession Planning & Talent Management', '', 'Succession Planning is fundamentally guided by the strategy, business performance and the requirements set at each level / job role. Check out the key components of our program on our website today. '),
(54, 49, 'en', 'Nationalisation Programmes', '<p style=\"text-align: justify;\">\"The true wealth of a nation lies in its youth&hellip;one that is equipped with education and knowledge and which provides the means for building the nation and strengthening its principles to achieve progress on all levels.\" Sheikh Mohamed Bin Zayed Al Nahayan, Crown Prince of Abu Dhabi</p>\r\n<p style=\"text-align: justify;\">How an organisation attracts, retains and develops its &lsquo;National&rsquo; talent is a priority across the GCC region. Our focus is designing an overall people strategy aligned to business strategy, vision, mission and values. We help organisations define the way forward and develop SMART objectives and frameworks at individual, team and organisational-level.</p>', 'Nationalisation Programmes & Courses Consulting', '', 'We help organisations define the way forward and develop SMART objectives and frameworks at individual, team and organisational-level. '),
(64, 59, 'en', 'Situational Judgement Tests', '<p style=\"text-align: justify;\">Situational Judgement Tests (SJT) assesses judgment required for solving problems in work-related situations typically aligned to a specific role or job family. SJTs are used primarily as a way to sift multiple applications for a role and to provide a realistic job preview for prospective candidates. They are particularly helpful in attracting the best talent and are often used for graduate trainee programmes where they provide an opportunity for the organisation to &lsquo;show case&rsquo; their offering whilst simultaneously identifying candidates with the closest job fit.</p>\r\n<p style=\"text-align: justify;\">An SJT presents candidates with hypothetical and challenging situations that employees might encounter at work which may involve working with others as part of a team, interacting with others, and dealing with workplace problems. In response to each situation, candidates are presented with several possible actions in a multiple choice format. Most commonly, candidates are asked to select the most effective response to the situation described. However, some tests may require candidates to list the responses in order of effectiveness.</p>\r\n<p style=\"text-align: justify;\">Unlike most psychological tests, we strongly recommend that SJTs are not bought \'off-the-shelf\', but are in fact designed specifically to suit individual job role requirements and the culture and practices of your organisation.</p>\r\n<p style=\"text-align: justify;\">Thus we start by conducting a job analysis; reviewing Job Descriptions, observing performance in the workplace and conducting interviews. We typically design each test to consist of between 25 and 35 &lsquo;items&rsquo; or short descriptions of problem situations. Each description is usually followed by one, two or three questions and multiple possible responses which candidates are asked to rate.</p>\r\n<p style=\"text-align: justify;\">We can present the SJTs in either a static form or a multi-media format which help organisations attract and select the best possible talent.</p>', 'Situational Judgement (SJT) Tests', '', 'Our judgement test starts with conducting job analysis & descriptions, observing performance and conducting interviews. Start today and see how we can  help you. '),
(65, 60, 'en', '360 Surveys', '', '360 Survey & Performance Review', '', 'We provide 360 surveys & assessments to help employees and companies meet their goals. Visit our website today to see how we can help you.  '),
(66, 61, 'en', 'Overview', '<p style=\"text-align: justify;\">How you see yourself, and how others see you makes for an interesting development conversation. We have tried and tested 360 Survey tools both &lsquo;off the shelf&rsquo; where individuals are rated against an international competency model, and 360 surveys which are 100% customised to your Competency Framework. Unlike the traditional appraisal where a supervisor appraises the performance of their subordinate, 360 degree feedback incorporates multiple perspectives by using feedback from a variety of sources. These sources include self, line manager, peers, subordinates, and even key customers and suppliers if so desired. The majority of our tools allow for both a quantitative rating and qualitative comments to strengthen the developmental feedback process. 360 Surveys can be used to support training needs analysis, development planning, as well as part of the performance management framework.</p>', '', '', '');
INSERT INTO `page_translations` (`id`, `page_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(72, 67, 'en', 'Organisational Engagement Surveys', '<p style=\"text-align: justify;\">Measuring an organisations culture with a view to enhancing organisational performance is one of the most difficult leadership challenges. Culture comprises an interlocking set of goals, roles, processes, values, communications, practices, attitudes and assumptions. The elements fit together as a mutually reinforcing system and combine to prevent any attempt to change it! That is why single-fix changes may appear to make progress for a while, but eventually the interlocking elements of the organisational culture take over.</p>\r\n<p style=\"text-align: justify;\">Our diagnostic phase is a multi-faceted approach which includes a Climate Survey, Leadership Interviews and Focus Groups, as well as uniquely, Leadership Profiling. Surveys, Focus Groups and Interviews are not new; their success rests on the questions being asked and the way they are communicated upfront and actioned upon final analysis.</p>\r\n<p style=\"text-align: justify;\">What is unique to our way of working is that we have incorporated into our proposition Leadership Profiling as part of the diagnostic phase. We believe it is in the day-to-day behaviour of leaders that most people are informed about the &lsquo;way things are done around here&rsquo;; leaders shape culture and that culture shapes leadership behaviour. Leaders are both architects and the product.</p>\r\n<p style=\"text-align: justify;\">Thus our approach is a fully comprehensive diagnostic process, which culminated in thoughtful and detailed analysis and recommendations for the way forward.</p>', 'Organisation & Employee Engagement Surveys Performance', '', 'Our unique way of thinking in organisational engagement surveys providing insightful analysis and takeaways to improve your organisation. '),
(73, 68, 'en', 'Career Planning ', '', '', '', ''),
(74, 69, 'en', 'Strong Interest Inventory', '<p class=\"p1\"><span class=\"s1\">Career planning tool for high school, college and University students&mdash;as well as Young Talent and people in transition to make fulfilling career choices.</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">High<span class=\"Apple-converted-space\">&nbsp; </span>school, college and University students &amp; people in career transition</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/94ba444dd25fa2347493f982231e2e5f103fb042.pdf&amp;name=ST-284108_-_Strong_Profile.pdf\">Strong Interest Inventory Report</a></p>', '', '', ''),
(75, 70, 'en', 'My Self', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Career Guidance and Self-Development tool which measures an individual\'s potential talents across a range of areas in the workplace.</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">Fresh Graduates and Younger Talent</span></p>\r\n<h2 class=\"p1\">Contact us directly for a sample and demo.</h2>', '', '', ''),
(76, 71, 'en', 'FIRO-B', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Helps people understand their own behaviour and that of others in interpersonal situations.</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/40016b2f57f60f77eb621d2c217b88694aee4322.pdf&amp;name=FIRO-B-220160 - FIRO B Profile.pdf\">Firo-B Report</a></p>', '', '', ''),
(77, 72, 'en', 'FIRO-Business', '<p class=\"p1\">Designed for use with clients who want to understand their interpersonal communication needs and gain greater insight into how those needs influence their work behaviours and leadership style</p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/d5e8bb7a5f3ee4a9491e28cb87aa26ed698c6c29.pdf&amp;name=FIRO-B-220170_-_FIRO_Business_Profile.pdf\">Firo Business Report</a></p>', '', '', ''),
(78, 73, 'en', 'Thomas Kilman Conflict Mode Instrument (TKI)', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Helps individuals to learn to move beyond conflict and focus on achieving organisational goals and business objectives. Organisations can apply the TKI to such challenges as change management, team building, leadership development, stress management, negotiation, and communication.</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/290b4bb9dbf115f06e9d50c1524217185f31853c.pdf&amp;name=TKI_Profile_and_Interpretive_Report.pdf\">TKI Report</a></p>', '', '', ''),
(79, 74, 'en', 'Safety in the Workplace', '', '', '', ''),
(80, 75, 'en', 'Hogan Safety ', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Accidents at work cause unnecessary stress and business expenses.<span class=\"Apple-converted-space\">&nbsp; </span>Some people tend to engage in unsafe behaviour at work due to carelessness, recklessness, spite, and other reasons.</span></p>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Hogan&rsquo;s Safety Report helps alleviate some of these concerns by identifying risks that individuals possess that may lead to on-the-job accidents and other unsafe behaviours.</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">All levels</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/5916c3fe71f9e1c0a51673bab75cfda34eec453a.pdf&amp;name=safety_report.pdf\">Hogan Safety Report</a></p>', '', '', ''),
(81, 76, 'en', 'Innovation and Change', '', '', '', ''),
(82, 77, 'en', 'ME2 ', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Workplace diagnostic tool which assesses an individual&rsquo;s creative strengths and style which can then be applied to whole teams or organisations and how they fit and work together creatively. Creativity underpins innovation and&nbsp;problem-solving.</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/3799cc9dfeda367414a125155a2ca7dcfa474ab4.pdf&amp;name=Me2_Development_Report.pdf\">ME2 Report</a></p>', '', '', ''),
(83, 78, 'en', 'Mental Toughness and Resilience ', '', '', '', ''),
(84, 79, 'en', 'MTQ 48', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Assess mental toughness in terms of the four key components; Control, Commitment, Challenge, Confidence to help someone change their mental toughness or how to adopt the behaviours that a mentally tough person would require.</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/307d9ec90054104e39201280a97671d3382d0394.pdf&amp;name=MTQ48_Development_Report.pdf\">MTQ48 Report</a></p>', '', '', ''),
(85, 80, 'en', 'Workplace Language ', '', '', '', ''),
(86, 81, 'en', ' Workplace English', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Workplace English Tests assess an individual\'s understanding and use of English in the workplace. </span></p>\r\n<h2><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Applicable to a wide range of roles such as administrators, call centre personnel, sales assistants, hospitality staff and medical personnel.</span></p>\r\n<p class=\"p1\" style=\"text-align: justify;\">&nbsp;</p>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Sample reports are available. Please get in touch with our team for more info!</span></p>\r\n<h2 class=\"p1\">&nbsp;</h2>', '', '', ''),
(88, 83, 'en', 'Programme', '', '', '', ''),
(90, 85, 'en', 'Booking', '', '', '', ''),
(91, 86, 'en', 'Thank You', '<p><strong>Thank you for booking!</strong></p>\r\n<p><strong>One of our team members will be in touch with you shortly!&nbsp;</strong></p>\r\n<p><strong>If you have any questions regarding your Accreditation booking please contact us on +97143902778 or email&nbsp;<a href=\"mailto:info@ihsdubai.com\">course@innovative-hr.com</a></strong></p>\r\n<p><strong>If you have any questions regarding your event booking please&nbsp;contact us on +97143902778 or email&nbsp;<br /></strong><strong><a href=\"mailto:info@ihsdubai.com\">info@innovative-hr.com</a></strong></p>\r\n<p><strong>Please like us on Facebook and Twitter and follow us on LinkedIn for all the latest talent thinking updates and news!</strong><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>', '', '', ''),
(92, 87, 'en', 'Thank You', '<p><strong>Thank you for sending us your enquiry!&nbsp;</strong></p>\r\n<p><strong>One of our team members will be in touch with you shortly!&nbsp;</strong></p>\r\n<p><strong>If you have any questions regarding your enquiry&nbsp;please contact us on +97143902778 or email <a href=\"mailto:info@ihsdubai.com\">infome@psionline.com</a></strong></p>\r\n<p><strong>Please like us on Facebook and Twitter and follow us on LinkedIn for all the latest talent thinking updates and news!</strong><strong>&nbsp;</strong></p>', '', '', ''),
(95, 90, 'en', 'NEO', '<h2 class=\"p1\"><span class=\"s1\">NEO Primary Colours 360</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">The Primary Colours Model has been developed from 25 years of academic research and industry consulting experience.&nbsp;The Primary Colours Leadership Report merges the in-depth personality profile of the NEO PI-R with the proven Primary Colours Model of leadership to create a clear, comprehensive narrative report on an individual\'s leadership capability.&nbsp;</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/ac38a250f08b2e9533ca4ea2e7bd33d9345c9789.pdf&amp;name=Primary Colours Leadership 360 Example Report.pdf\">Primary Colours 360 Leadership Report</a></p>', '', '', ''),
(99, 94, 'en', 'Leadership Development', '<p style=\"text-align: justify;\">The world is arguably more volatile, uncertain, complex and ambiguous than ever before. What has not changed however, is that leaders need to be able to develop a clear vision of the way forward, be able to communicate that vision effectively, adapt their approach to the needs of others, motivate others and drive results efficiently.</p>\r\n<p style=\"text-align: justify;\">Due to popular demand, we have developed a Leadership Development Programme which can be delivered in English or Arabic. The programme is built around Dave Ulrich, Norm Smallwood and Kate Sweetman&rsquo;s; &lsquo;The Leadership Code: Five Rules to Lead By&rsquo;. Ulrich and his team set about seeking to &ldquo;simplify and synthesise&rdquo; and leverage the work of established experts from across the leadership development field. In fact as they report, there are literally tens of thousands of leadership studies, theories, frameworks, models, and recommended best practices. Drawing on decades of research experience, the authors conducted extensive interviews with a variety of respected CEOs, academics, experienced executives, and seasoned consultants - and determined the same five essentials repeated again and again. These five rules became The Leadership Code.</p>\r\n<p style=\"text-align: justify;\">The Innovative HR Solutions &lsquo;Mastering the Leadership Code&rsquo; is a 5-part modular programme interspersed with a pre and post assessment, Executive Coaching and psychometrics to deepen self-awareness. There is also the opportunity to customise the programme to bring learning into the workplace through on-the-job individual and / or team projects. We have designed two levels; First-Line to Mid-Managers and Senior Leaders. The programme can be flexed to accommodate time and budgetary requirements as well as the opportunity to co-create projects to bring value to your organisation.</p>\r\n<p style=\"text-align: justify;\">We have also launched the 2 day ICF-recognised Coaching Programme for People Managers, a highly practical workshop based on the GROW model to enable Managers to empower their people.&nbsp;</p>\r\n<p style=\"text-align: justify;\">Lastly based on client demand, we have designed The Power of Trust; a high impact Leadership Programme aimed at fostering high trust teams to produce great results.&nbsp;&nbsp;</p>\r\n<p style=\"text-align: justify;\"><a title=\"Accreditation and Workshop Catalogue\" href=\"/downloadFile?path=uploads/documents/8637fa17c7e50f7c77c09359a4ee2b99a8c60829.pdf&amp;name=2019 PSI Accreditation and Workshop Catalogue.pdf\">Accreditation and Workshop Catalogue</a></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>', 'Leadership Development & Talent Programme', '', 'The Innovative HR Solutions ‘Mastering the Leadership Code’ is a 5-part modular programme interspersed with a pre and post assessment, Executive Coaching and psychometrics to deepen self-awareness. See the different programmes at our website.'),
(100, 95, 'en', 'Management Skills', '<p style=\"text-align: justify;\">One of the most significant transitions we make in our careers is when we move from Managing Self to Managing Others. In this transition it is vital to equip Managers with the skills necessary to be effective in their roles. We offer the following programmes as discrete workshops or as a suite of modules.</p>', 'Management Skills & Competency', '', 'We offer several programmes to increase management skills such as coaching skills, performance skills and other comparable modules. '),
(101, 96, 'en', 'Performance Management Skills', '<p style=\"text-align: justify;\">A bad boss or supervisor is the number one reason people leave their employers. This could be due to the environment their boss creates and how they feel they have been treated, whether they feel listened to, are provided with support and the opportunity to grow and develop in their role. This module will cover elements such as:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>The importance of performance management</li>\r\n<li>Setting SMART goals</li>\r\n<li>Reviewing performance throughout the year</li>\r\n<li>Providing feedback</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">As with all our programmes we aim to provide a highly experiential learning opportunity with group exercise, role play, group discussion and individual reflection. We are able to conduct follow up performance management business simulation sessions to embed learning.</p>', 'Performance Management Skills', '', 'Performance management skills. Find out more about our program to get the best performance from your team. '),
(102, 97, 'en', 'Myers-Briggs Type Indicator® (MBTI®)', '<p>The Myers-Briggs Type Indicator&reg; (MBTI&reg;) instrument continues to be the most trusted and widely used assessment in the world for understanding individual differences and uncovering new ways to work and interact with others.</p>\r\n<p>It is used exclusively in the development setting to improve individual and team performance, develop and retain top talent and leaders lending depth to the coaching experience, improve communication and reduce conflict in teams as well as explore the world of work and careers.</p>\r\n<p>Those who successfully complete the course will be accredited to administer, interpret and feedback the MBTI instrument (Step I &amp; II). You will also be able to join the Middle East MBTI Forum which is a free bi-annual Professional Development Breakfast for MBTI practitioners to share experiences and continue to develop your skills further.</p>\r\n<p>The course comprises 10 hours pre-course work (reading and online assessment), home work and daily multiple choice exams.</p>\r\n<p>Click here for <a href=\"/uploads/documents/18b2b3bccbc60e5eb6f1492d4b096d4e51ae2d64.pdf\">Course Details</a>&nbsp;and the <a href=\"/calendar/book/course/4/mbti-certificate-program\">Registration Form</a></p>', '', '', ''),
(103, 98, 'en', 'BPS Qualification in Occupational Testing (Ability & Personality) ', '<p>Formerly known as the BPS Level A&amp;B this 4-day course qualifies delegates to use a wide variety of ability tests and personality questionnaires from a number of major test publishers including in this case, the Saville Consulting products.</p>\r\n<p>The qualification is internationally recognised demanding best practice in the use of psychometrics at work, and is suitable for those involved in selection, development or any other application where psychometric assessments are useful. Upon successful completion of the course delegates will be able to administer, analyse and feedback the Saville Wave selection of personality and ability tools, and apply to the BPS for Accreditation in Occupational Testing. The Accreditation then enables delegates to use a number of further products from other test publishers.</p>\r\n<p>There is 10 hours pre-course work required, two open book exams during the 4-day course, and post course work (13 Assignments in total, including written reports and feedback sessions) should you wish to apply to the BPS upon completion. Included in our pricing is one-to-one support post course to mark and make recommendations to support a successful BPS submission.</p>\r\n<p>Click here for <a href=\"/uploads/documents/449c0bc139627786e3356c4cfb876978fdef24f7.pdf\">Course Details</a>&nbsp;and <a href=\"/calendar/book/course/6/bps-qualification-in-occupation-testing\">Registration Form</a></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Why British Psychological Society\'s (BPS)?</strong> <br /><a href=\"/html/course-details.php\">http://www.bps.org.uk/what-we-do/bps/bps</a></p>\r\n<p><a href=\"/html/course-details.php\">British Psychological Society Assistant Test User</a></p>\r\n<p>The British Psychological society\'s (BPS) Qualification in Occupational testing - Assistant test User is a 1-day course which allows those qualified to administer Aptitude and Ability Tests from a number of major test publishers including Saville Consulting.</p>\r\n<p>It is principally for administrators wishing to administer tests and handle test results it does not qualify delegates to interpret or feedback results.</p>\r\n<p>Click here for <a href=\"/html/course-details.php\">Course Details</a> and <a href=\"/calendar/book/course/6/bps-qualification-in-occupation-testing\">Registration Form</a></p>', '', '', ''),
(104, 99, 'en', 'EQi 2.0 & EQ360 ', '<p>The EQi is particularly useful for development activity, and in certain circumstances for selection. Emotional Intelligence has been identified as one the leading predictors of leadership performance and success, and is helpful in identifying how we can understand and manage ourselves and our relationships with others.</p>\r\n<p>The EQi 2-day Accreditation Course enables those who complete, pass the post online assessment and conduct an observed feedback session, to become certified to use the EQi-2.0 model of Emotional Intelligence tools; both individual reports and 360 surveys.</p>\r\n<p>Click here for <a href=\"../../../uploads/documents/42a3f8dfebc8f7924fa7429ffd2a4f61b4f962ab.pdf\">Course Details</a>&nbsp;and the <a href=\"../../../calendar/book/course/8/eqi-20-and-eq360\">Registration Form</a></p>', '', '', ''),
(105, 100, 'en', 'Strong Interest Inventory ', '<p>The Strong Interest Inventory instrument is the most widely used and respected career development instrument in the world. It has guided thousands of individuals, from high school and college students to mid-career workers seeking a change in their search for a rich and fulfilling career. It is a powerful tool for anyone considering a career change, starting a new career, looking for career enrichment, or a better work life balance. The Strong Interest Inventory generates an in depth assessment of an individual&rsquo;s interests in a broad range of occupations, work and leisure activities, and educational subjects. The tool is the gold standard for career exploration and development, providing time-tested and research-validated insights that foster successful career counselling relationships. The Strong can be used in conjunction with other reports such as the Myers Briggs (MBTI) for a detailed look at personality and career.</p>\r\n<p>The Strong Interest Inventory&reg; is ideal for:</p>\r\n<ul>\r\n<li>Choosing a college major</li>\r\n<li>Career exploration</li>\r\n<li>Career development</li>\r\n<li>Re-integration</li>\r\n</ul>\r\n<p>There is some pre-course work (online assessment)</p>\r\n<p>Click here for <a href=\"../../../uploads/documents/006f8879407413a825a53d41c7899c83a823e251.pdf\">Course Details</a>&nbsp;and the <a href=\"../../../calendar/book/course/10/strong-interest-inventory\">Registration Form</a></p>', '', '', ''),
(106, 101, 'en', 'FIRO Business ', '<p>The FIRO-Business&reg; (Fundamental Interpersonal Relations Orientation - Business) instrument helps individuals understand their need for inclusion, control and affection and how it can shape their behaviour and interactions with others at work or in their personal life. As an integral part of your leadership and coaching, team building and conflict management initiatives, the FIRO-B&reg; assessment can be used in a variety of settings and in combination with other solutions to improve organisational performance. It provides the skills to administer and interpret the assessment competently and ethically.</p>\r\n<p>There is some pre-course work (online assessment) and homework.</p>\r\n<p>Click here for <a href=\"/uploads/documents/df2b3cac1a82cc3115522cc4ee23c66eab83c231.pdf\">Course Details</a>&nbsp;and the <a href=\"/calendar/book/course/11/firo-business\">Registration Form</a></p>', '', '', ''),
(108, 103, 'en', 'Centre Manager ', '<p>This British Psychological Society-accredited 2-day Workshop enables delegates to manage the process of designing and facilitating Assessment and Development Centres. The 2-day Assessor Skills Course is a pre-requisite to attending this course.</p>\r\n<p>The course enables delegates to develop the knowledge and skills needed to become a qualified Centre Manager in a range of typical Assessment Centre events. You will be shown how to select exercises and design a matrix for an assessment event to gain a robust measure of the behaviours at the level in question. It will also instruct in the art of running an efficient and robust post centre &lsquo;wash-up&rsquo; session and guide consistency and quality throughout an event.</p>\r\n<p>There is some homework and a multiple choice exam.</p>\r\n<p>Click here for <a href=\"/cms/uploads/documents/6dc99942fa5334a8f6976842022eeb67c3636974.pdf\">Course Details</a>&nbsp;and the <u data-redactor-tag=\"u\">Registration Form</u></p>', '', '', ''),
(109, 104, 'en', 'Accredited Award in Coach Training (AACT)', '<p>A fast paced, highly interactive workshop recognised by the Association of Coaching UK, introduces the practical techniques to coach individuals for professional development. Real life case studies, group discussions, personal feedback and continuous practice are all used to build the framework for professional coaching skills. The workshop particularly focuses on the GROW model.</p>\r\n<p>The workshop is designed to create an assured coach, confident to tackle work and behavioural issues and help their coachees achieve set goals. This includes:</p>\r\n<p>Understand the purpose, ethics and power of Coaching</p>\r\n<ul>\r\n<li>Gaining commitment in times of change</li>\r\n<li>Tapping into people&rsquo;s potential and their desire to succeed and improve</li>\r\n<li>Solving technical and organisational problems</li>\r\n<li>Dealing with difficult people and dramatically enhance their own and other&rsquo;s performance</li>\r\n</ul>\r\n<p>Innovative HR Solutions has aligned this programme with the <strong data-redactor-tag=\"strong\">Association for Coaching (UK</strong>) and the standards they set to help participants gain Associate and Member Level Membership with the Association following the workshop.</p>\r\n<p>There is some homework and post-course work &ndash; practical and theoretical work.</p>\r\n<p>Click here for <a href=\"/uploads/documents/2d6fe7b9d6a14aa5ffb357537a99901a5ef2ce9b.pdf\">Course Details</a>&nbsp;and the <u data-redactor-tag=\"u\">Registration Form</u></p>', '', '', ''),
(111, 106, 'en', 'Thomas Kilman Conflict Mode Instrument (TKI) Workshop', '<p>The Thomas Kilman Conflict Mode Instrument (TKI) is the world&rsquo;s best-selling tool for helping people understand how different conflict-handling styles affect interpersonal and group dynamics&mdash;and for empowering them to choose the appropriate style for any situation.</p>\r\n<p>Delegates will learn how to apply the TKI tool to assess an individual&rsquo;s typical behaviour in conflict situations along two dimensions: assertiveness and cooperativeness and how individuals can effectively use five different conflict-handling modes, or styles.</p>\r\n<p>There is some pre-course work (online assessment).</p>\r\n<p>Click here for <a href=\"/uploads/documents/290b4bb9dbf115f06e9d50c1524217185f31853c.pdf\">Course Details</a>&nbsp;and the <u data-redactor-tag=\"u\">Registration Form</u></p>', 'Thomas Kilman Conflict Mode Instrument (TKI) Workshop | IHS', '', 'TKI is the world’s best-selling tool for helping people understand how different conflict-handling styles affect interpersonal and group dynamics—and for empowering them to choose the appropriate style for any situation.'),
(114, 109, 'en', 'Motivating & Engaging Others ', '<p style=\"text-align: justify;\">As a recent Harvard Business Review article states; &ldquo;If your job involves leading others, the implications are clear: the most important thing you can do each day is to help your team members experience progress at meaningful work. To do so, you must understand what drives each person.&rdquo; Put simply, we should not leave the art of motivating and engaging others to chance! We leverage the work of Daniel Pink and others to explore strategies in a way which is practical and insightful.</p>\r\n<p style=\"text-align: justify;\">Then taking the theory to the next level we have the option to use <a href=\"/assessment/personality-assessment\">Myers Briggs Type Indicator (MBTI)</a> and we&nbsp;start to explore different personality and how that impacts each person in the day-to-day work setting. How we take in information around us, make decisions, communicate, structure tasks as individuals and teams, all plays an intrinsic role in our levels of satisfaction and motivation at work. It is important for individuals to find their own meaning in work, to set realistic and yet challenging goals to support their own intrinsic motivation whilst meeting corporate goals and objectives.</p>', 'Motivating & Engaging Others', '', 'The most important thing you can do each day is to help your team members experience progress at meaningful work. You must understand what drives each person. '),
(115, 110, 'en', 'Competency Based Interviewing ', '<p style=\"text-align: justify;\">We know the direct and indirect cost of poor recruitment when we hire the wrong person and equally that a poor candidate experience can impact our brand. Line Managers play a vital role in hiring decisions and the way in which we present our business to prospective talent. When interviewing candidates it is important to assess a candidates&rsquo; behavioural competence as well as their technical capabilities. Our workshops are designed to provide delegates with the skills and best practices essential to conducting competency based interviews. Our workshops are highly interactive; we build the skills and competence of individuals through role play, observation and reflection. Workshops can be customised to align with your Competency Framework and Job Requisition process and forms (if applicable).</p>\r\n<p style=\"text-align: justify;\">We can design interactive and engaging workshops to enable attendees to:</p>\r\n<ul>\r\n<li style=\"text-align: justify;\">Understand a competency based interview model and the advantages of questioning and probing competencies.</li>\r\n<li style=\"text-align: justify;\">Understand what type of information can be gathered and evaluated in an interview and how evidence can be rated according to the behavioural competencies being assessed at different levels of the organisation.</li>\r\n<li style=\"text-align: justify;\">Learn how to observe, record, classify and evaluate (ORCE) evidence.</li>\r\n<li style=\"text-align: justify;\">Learn how to structure an interview for best results. Developing competency-based questions and questioning techniques.</li>\r\n<li style=\"text-align: justify;\">Take part in practical interviewing skills session (to candidate / to hiring decision makers) and receive feedback from our qualified facilitator.</li>\r\n</ul>', 'Competency Based Interviewing Training Course', '', 'Our highly interactive workshops are designed to provide delegates with the skills and best practices essential to conducting competency based interviews. This involves role play, observation and reflection. '),
(116, 111, 'en', 'Coaching Skills for Line Managers ', '<p style=\"text-align: justify;\">Coaching Skills are intrinsic to establishing a high performance culture. To help individuals understand their role in reaching personal, team and corporate goals individuals require an understanding of how they can contribute clear communication around goals and expectations and timely feedback regarding their progress. According to Harvard Business School\'s recent research, &ldquo;the single most important managerial competency that separates highly effective managers from average ones is coaching&rdquo;. Our programmes includes;</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>What is Coaching and why is it important?</li>\r\n<li>Essential coaching principles and tools.</li>\r\n<li>Role of Manager in development.</li>\r\n<li>Coaching technique including the GROW model of coaching.</li>\r\n<li>Advanced listening and questioning techniques.</li>\r\n<li>Action planning.</li>\r\n<li>How to provide development feedback.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Want to become a certified Executive Coach? Take the AFC Executive Coaching Certification&nbsp;with Innovative HR Solutions today!</p>\r\n<p style=\"text-align: justify;\">AFC Executive Coaching Certification: Click here for&nbsp;<a title=\"Coaching Course Details\" href=\"/calendar/course/13/afc-executive-coaching-certification\">Course Details</a></p>', 'Coaching Skills for Line Managers', '', 'We created a programme to help individuals understand their role in reaching personal, team and corporate goals. Want to become a certified Executive Coach? Visit our website today.  '),
(117, 112, 'en', 'Managing Change ', '<p style=\"text-align: justify;\">Change is the only constant in this fast paced world, and no more so than in the Middle East region. Enabling change in the workplace to take advantage of opportunities as well as mitigate risks is now simply put, a &lsquo;101&rsquo; management skill.</p>\r\n<p style=\"text-align: justify;\">Our programmes are designed to provide insight into the change curve as we look to Kotter and others to understand the principles of change. Delegates will gain an understanding of managing their own emotional state during times of uncertainty and how different people experience change differently and have different stressors and coping strategies. We will examine the need for different forms of communication, holding true to vision and building strategies to build interim and long term plans in order for long term business goals to be&nbsp;met.</p>', 'Managing Change Training', '', 'Learn more about our managing change programs at IHS. Backed by research and best practices. Provided by licensed & qualified professionals.'),
(118, 113, 'en', 'Resilience and Emotional Intelligence ', '<p style=\"text-align: justify;\">We are now operating in a more volatile, uncertain, complex and ambiguous context than ever before. People fear change but they need it too. Change brings pressure. Pressure brings performance. But what happens when pressure outgrows the ability to perform? How do your employees maintain drive, focus and productivity when stretched beyond their means?</p>\r\n<p style=\"text-align: justify;\">One of the five core predictors of potential - resilience - is at the heart of our approach to coaching and development. Developing strategies to adjust to changing situations and higher levels of pressure is critical for today\'s successful leader and employee.</p>\r\n<p style=\"text-align: justify;\">Our work with individuals and teams has led to the creation of our Resilience Development programme which will enable your employees to thrive, rather than cope, under pressure.</p>', 'Resilience and Emotional Intelligence Training', '', 'Our work with individuals and teams has led to the creation of our Resilience Development programme which will enable your employees to thrive, rather than cope, under pressure. Visit our website today to see all the management skills we offer.'),
(119, 114, 'en', 'Managing Conflict ', '<p style=\"text-align: justify;\">Conflict in the workplace, while it is not desirable, is an inevitable element of the workplace. Colleagues with varying ideas who have different views may go head to head causing disruption in the workplace, subsequently impacting on productivity and workplace culture. Our workshop will enable delegates to learn approaches on how to diffuse potentially damaging conflict situations using the Thomas Kilman Conflict Mode Instrument (TKI).</p>\r\n<p style=\"text-align: justify;\">The TKI is the world&rsquo;s best-selling tool for helping people understand how different conflict-handling styles affect interpersonal and group dynamics&mdash;and for empowering them to choose the appropriate style for any situation. The TKI can be used effectively within an organisation to facilitate and improve team dynamics for development, leadership and coaching and subsequently enhance communication and improve productivity.</p>', 'Conflict Management Training', '', 'Learn more about our managing conflict program at IHS. Get in touch to enhance your organisation\'s communication & improve productivity. '),
(120, 115, 'en', 'Team Facilitation ', '<p style=\"text-align: justify;\">Psychologist Bruce Tuckman first came up with the memorable model \"forming, storming, norming, and performing\" in his 1965 article, \"Developmental Sequence in Small Groups.\" He used it to describe the path that most teams follow on their way to high performance. Unlocking the full potential of teams is critical to organisation performance. We have designed and implemented a range of powerful team development solutions and interventions aimed at increasing collective awareness of team success factors and enhancing the quality of communication and collaboration. We work with Boards, senior management teams as well as strategic business units who are attempting to raise their game, which are in the process of forming and storming, or undergoing change or indeed those that are in conflict. It is important to work together to achieve shared goals, resolve differences and appreciate others&rsquo; strengths.</p>', 'Team Building & Facilitation', '', 'Learn more about our team building and facilitation program to unlock the full potential of your organisation\'s performance. '),
(121, 116, 'en', 'Executive Coaching & Mentoring ', '<h3 style=\"text-align: justify;\">Executive Coaching</h3>\r\n<p style=\"text-align: justify;\">Executive Coaching is a dynamic, motivating and interactive relationship with a qualified and experienced Coach, assisting an individual or Coachee, to enhance their performance. It is an important component of a talent management strategy for high potential individuals and leaders, supporting individuals to continue to grow, learn and develop. During the coaching session we focus on enhancing overall performance whilst further developing behaviours and skills needed for success in their current, new or future role.&nbsp; Coaching provides a road map for behavioural change, offering fresh perspectives on personal and business challenges. The Coach assists and facilitates individuals to identify barriers to performance, whether linked to their own chosen behaviours and how they are perceived by others, or to current skill levels, knowledge and experience. Coaching assists the individual to increase their own awareness of what they need to do in order to become more effective, and to take responsibility for implementing the actions identified in order to arrive at solutions and realise success. Overall it leads to enhanced decision-making skills, greater interpersonal effectiveness, increased confidence, improved productivity, satisfaction with life and work and attainment goals.</p>\r\n<h3 style=\"text-align: justify;\"><span class=\"bold\">Mentoring</span></h3>\r\n<p style=\"text-align: justify;\">Mentoring is a relationship between two individuals based on a mutual desire for development towards career goals and objectives. &nbsp;Whilst both a Mentor and a Coach assume responsibility for the process during the development journey, the Mentor has a slightly different role in that they also assume a responsibility for content and knowledge transfer, whilst the coaching process is more facilitative in nature.&nbsp; The Mentor is therefore usually more experienced and qualified than the mentee and is more directive in style than a coach.&nbsp; The relationship is a non-reporting one and replaces none of the organisational structures in place.&nbsp; We can work with organisations to set up a Mentoring Framework in-house, identify in-house mentors and provide training to embed the process.&nbsp; Or we can provide Mentors to work with key individuals to expedite their development. &nbsp;&nbsp;&nbsp;</p>', 'Executive Coaching & Mentoring Grow Performance', '', 'Executive Coaching is a dynamic, motivating and interactive relationship with a qualified and experienced Coach. Our coaches & mentors focus on improving overall performance.'),
(122, 117, 'en', 'Development Centres ', '<p style=\"text-align: justify;\">A Development Centre is intended to support leadership and talent development. We typically run Centres over 1 or 2 days, and they comprise of a series of business simulations (for example; Analysis Exercise, Presentation, Role Play, Group Discussion, Structured Interview, In-Tray or Fact Find ), and personality and ability psychometrics. We are also able to add to this workplace assessments such as a 360 survey or line manager feedback.</p>\r\n<p style=\"text-align: justify;\">The emphasis of a Centre is to promote greater self-awareness around areas of strengths as well as areas for development. This is achieved on the day through a self-reflection exercise, and after the event, through a coaching-style Individual Feedback Session. In this session the observer or &lsquo;Assessor&rsquo; will provide evidence of what was said and done throughout the exercises, and will encourage the individual to consider the impact of the choices they made and how their performance could have been improved. Participants will be expected to &lsquo;own&rsquo; the development requirements as part of their continuous Professional Development and will be encouraged to form Individual Development Plans to capitalise on the insights gained.</p>', 'Professional Development Centres', '', 'We help you support leadership and talent development by a series of business simulations. The emphasis of a centre is to promote greater self-awareness around areas of strengths as well as areas for development.'),
(123, 118, 'en', 'Individual Development Plans ', '<p style=\"text-align: justify;\">An Individual Development Plan (IDP) is a structured process which&nbsp;enables an individual to create an individual plan to realise their development objectives. IDP&rsquo;s are specific to the individual and tailored to employees&rsquo; job function and department and align to the organisation&rsquo;s vision, mission and values. IDP&rsquo;s focus on enhancing an individual&rsquo;s skills to ensure effectiveness within their current job role and assist in developing the skills required for future roles. The key towards the success of an IDP is that the employee has ownership of the IDP, and receives support from their manager in implementing the plan. IDP&rsquo;s can be created following&nbsp;a different diagnostic process. For instance, they can follow on from a Development Centre, a performance conversation, a psychometric tools process or 360&deg; feedback. The creation of an IDP is a long-term investment and a cornerstone of any talent management process which motivates staff and conveys a clear organisational message that development is a priority.</p>', 'Individual Development Plans  (IDP)', '', 'An Individual Development Plan (IDP) is a structured process which enables an individual to create an individual plan to realise their development objectives. '),
(124, 119, 'en', 'Thank You', '<p><strong>Thank you for subscribing to our Newsletter! We are excited to have you as part of our community, bringing you the latest news and events in the Talent Management space! </strong></p>\r\n<p><strong>Do follow&nbsp;us on Facebook, Twitter and LinkedIn for all the latest talent thinking updates and news!</strong><strong>&nbsp;</strong></p>\r\n<p><strong>If you have any questions regarding how we can help you meet your business objectives please contact us on +97143902778 or email <a href=\"mailto:info@ihsdubai.com\">infome@psionline.com</a> </strong></p>', '', '', ''),
(125, 120, 'en', 'Page Not Found', '<p>The Page you are looking for is not found.</p>', '', '', ''),
(126, 121, 'en', 'Overview', '<p style=\"text-align: justify;\">How someone behaves at work has a considerable impact on how successful they are in their job role. There is a consistency in the way people behave across situations and over time. Personality tools help in identifying the typical way by which people relate to others, approach tasks and respond to situations. At PSI Middle East we have a range of personality tools all from the world&rsquo;s leading test publishers selected on the basis of their validity, reliability, ease of use, cost efficiency and objectivity.</p>', 'Personality Assessments Tools Overview', '', 'We have a range of personality tools all from the world’s leading test publishers selected on the basis of their validity, reliability, ease of use, cost efficiency and objectivity. Visit our website today. '),
(127, 122, 'en', 'Assessment, A Willis Towers Watson Company', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Saville\" src=\"/downloadFile?path=uploads/images/31b5f239562b84eefdc5b19b4a52ef2a76be3a8e.png&amp;name=Accredited-Partener-of-Saville-Assessment.png\" width=\"500\" height=\"167\" /></span></h2>\r\n<h2 class=\"p1\"><span class=\"s1\">Wave Professional Styles</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality Profile, highest Predictive Validity of any personality instrument on the market today.<span class=\"Apple-converted-space\">&nbsp; </span>For use in selection, development, talent management, identification of high potential, &nbsp; succession planning, leadership programs, coaching and career planning. GCC Norm Groups and Arabic capability available for some questionnaires!&nbsp;</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All - up to Senior Executives, available in Arabic and English</span></p>\r\n<h3 class=\"p1\">&nbsp;</h3>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">Wave Focus Styles</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality Profile used for volume recruitment, selection, development, talent management, succession planning and team effectiveness, &nbsp;identification of high potential.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All &ndash; up to Senior Executives</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">Wave Work Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality Profile questionnaire identifies the potential strengths of an individual against the successful behaviours required for a graduate, management trainee, manager or professional role. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All &ndash; up to Senior Managers</span></p>\r\n<h3 class=\"p1\">&nbsp;</h3>\r\n<h2 class=\"p1\">&nbsp;<span class=\"s1\">Customer Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality Profile used to identify the potential strengths of an individual against the successful behaviours required for customer oriented roles.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Customer Staff in Call Centres, Retail, Hospitality, Leisure, Health &amp; Education</span></p>\r\n<h3 class=\"p1\">&nbsp;</h3>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">&nbsp;Operational Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality Profile used to identify the potential strengths of an individual against the behaviours required for&nbsp;apprentice, technical and manufacturing type roles.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Technical Apprentices, Operational Staff in Manufacturing, Engineering, Construction and Transport</span></p>\r\n<h3 class=\"p1\">&nbsp;</h3>\r\n<h2 class=\"p1\"><span class=\"s1\">Administrative Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality Profile used to identify the potential strengths of an individual against the behaviours required for clerical and administrative roles.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Administrative Staff in Private &amp; Public Sector Offices</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">Commercial Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality Profile used to identify the potential strengths of an individual against the behaviours required for sales, marketing and business development roles.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Commercial Staff in Sales, Marketing, Business Development &amp; Financial Services</span></p>\r\n<h3 class=\"p1\">&nbsp;</h3>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">My Self</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Career Guidance And Self-Development tool which measures an individual\'s potential talents across a range of areas in the workplace.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Fresh Graduates and Younger Talent</span></p>\r\n<h3 class=\"p1\">&nbsp;</h3>', '', '', '');
INSERT INTO `page_translations` (`id`, `page_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(128, 123, 'en', 'MBTI', '<h2 class=\"p1\"><img class=\"center\" title=\"MBTI logo\" src=\"/uploads/images/79176094856874398328dfd25f5a1a1fcd4d230c.png\" width=\"146\" height=\"194\" /></h2>\r\n<h2 class=\"p1 center\">IHS are&nbsp;proud&nbsp;partners to CPP and the only company licensed to sell,use and train the world renowned, Myers Briggs Type Indicator (MBTI), in the region.&nbsp;</h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">The MBTI determines personality<span class=\"Apple-converted-space\">&nbsp;</span>type using four dichotomies: extraversion-introversion, sensing-intuition, thinking-feeling and judging-perceiving. The MBTI is used for Development only &ndash; individuals and teams. It allows for individuals and teams to gain a deeper understanding of themselves and their interactions with others, helping them improve how they communicate, learn and work.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/d60ac00311f2751572dab3cfd0dc149d3960549b.pdf&amp;name=MBTI_Step_I_Profile.pdf\">Step I Profile</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/254f73d2c8419a9e03820074f43d6ad437cee7dd.pdf&amp;name=MBTI_Step_II_Profile (1).pdf\">Step II Profile</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/e68d9ec82b2ba1851bc014959ef4606af4891c79.pdf&amp;name=MBTI_Step_I_Interpretive_Report.pdf\">Step I Interpretive Report</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/01380a643916a3f9e3d4df0ef82ff9c67421576b.pdf&amp;name=MBTI_Step_II_Interpretive_Report.pdf\">Step II Interpretive Report</a></p>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/c5f0bfa1b1df8d86e555af777cbd9b663568456a.pdf&amp;name=261145 Arabic MBTI Step I Form M Profile.pdf\">Arabic Step I Profile&nbsp;Report</a></p>\r\n<p class=\"p1\"><span class=\"bold\"><a href=\"/downloadFile?path=uploads/documents/af4aae15e28698a5e0c0dbfa65ac1e071005470e.pdf&amp;name=CPP - MBTI Product Catalogue 2018.pdf\" target=\"_blank\">&nbsp;MBTI Product Catalogue for Practitioners</a></span></p>', '', '', ''),
(129, 124, 'en', 'NEO', '<h2 class=\"p1\"><img class=\"centre center\" title=\"NEO\" src=\"/uploads/images/logoSliders/b3f0c7f6bb763af1be91d9e74eabfeb199dc1f1f.png\" width=\"135\" height=\"135\" />NEO PI-3</h2>\r\n<p style=\"text-align: justify;\"><span class=\"s1\">The NEO PI-3 provides a comprehensive approach to the assessment of adult personality and can be used in a wide range of business contexts, including:</span></p>\r\n<ul>\r\n<li>Candidate selection</li>\r\n<li>Leadership/Talent development</li>\r\n<li>Team building and effectiveness</li>\r\n<li>Coaching</li>\r\n</ul>\r\n<p>The NEO Personality Inventory Revised (NEO PI-R) is a concise measure of adult personality based upon the five factor model (FFM) and prominent work of Costa &amp; McRae (1985). The assessment embodies a conceptual model that distils decades of factor analytic research on the structure of personality. The NEO PI-R replaces the original NEO PI by adding the Agreeableness and Conscientiousness factors. The latest version with updated scales and items is the PI-3. The FFM can be easily remembered through the OCEAN acronym.</p>\r\n<ul>\r\n<li>Openness (to new experiences, feedback, and learning)</li>\r\n<li>Conscientiousness (organisation, reliability, drive and structure)</li>\r\n<li>Extraversion (assertiveness, sociability, and empathy)</li>\r\n<li>Agreeableness (trust, care for others, and compliance)</li>\r\n<li>Neuroticism (anxiety, sensitivity, and temperament)</li>\r\n</ul>\r\n<p>The NEO-PIR works best when face to face feedback is given and it focuses on 30 individual personality &lsquo;facet&rsquo; scales which are associated with different aspects of the FFM. NEO-PIR is suitable for use in occupational, clinical and research settings; it can be used for both selection and development of individuals in organisations. The Personal Insight reports split the results into four useful sections: problem solving and decision making, planning, organising and implementing, style of relating to others, and personal style. The Primary Colours Leadership report translates personality preferences into leadership potential based on the Primary Colours model of leadership (Pendleton &amp; Furnham, 2012).</p>\r\n<h3 class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">All - Up to Senior Executives</span></p>\r\n<h3 class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/7457aa91e6d7372eaf0a3b2d17f1517d0bd36a0a.pdf&amp;name=ECW NEO PI-3 Personal Insight Report (Managerial and Professional Norm) (2).pdf\">PR-I Personal Insight Report</a></p>\r\n<p style=\"text-align: justify;\"><a title=\"PI-3 Technical Report\" href=\"/downloadFile?path=uploads/documents/2b3b6679fe6097328e269c1864c9c91938d6740c.pdf&amp;name=NEO%20PI-3 Technical Sample Report (Managerial and Professional Norm).pdf\">PI-3 Technical Report</a></p>\r\n<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/1cd75bd05f14057d02b144f9a9731a3f5c318845.pdf&amp;name=NEO PI-3 Primary Colours Leadership Sample Report.pdf\">PI-3 Leadership Report</a></p>', '', '', ''),
(130, 125, 'en', 'EQi', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"EQi\" src=\"/uploads/images/f60539ac8eb5b3db08bf21a58e8288d0f0bd5640.jpg\" width=\"273\" height=\"88\" />EQi 2.0</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">For use in development, and in certain circumstances for selection. Emotional Intelligence has been identified as one of the strongest predictors of leadership performance and is helpful in identifying how to get the most out of ourselves and others. The EQi provides a detailed look at how we understand and manage ourselves, our relationships with others, and our reaction to the stresses and demands of everyday life.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/248bfe18588abf65db4cc7619b3c067fb60edae2.pdf&amp;name=EQ-i_2.0_Client_Report.pdf\">Client Report</a></p>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/bb8dd99ddb2c69387330ddbf3443cd619e07e7d2.pdf&amp;name=EQ-i_2.0_Coach_Report.pdf\">Coach Report</a></p>', '', '', ''),
(131, 126, 'en', 'Hogan', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Hogan\" src=\"/uploads/images/95e8a1572ba7a7b5afc3bcac5dc441681a4ff6e0.jpg\" width=\"123\" height=\"152\" />HOGAN Development Survey (HDS)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Recruitment &amp; Development &ndash; Identifies personality-based performance risks and&nbsp;derailers of interpersonal behaviour.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/9066cdf012448477601bc72556d6e7a027e0acd4.pdf&amp;name=Hogan_Development_Survey__HDS__Report.pdf\">Development Survey Report</a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">HOGAN Personality Inventory (HPI)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Ideal tool to help you strengthen your employee selection, leadership&nbsp;development, succession planning, and talent management processes.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/f804541a5f58f102bdf20a9cbc5094f7c32edf30.pdf&amp;name=Hogan_Personality_Inventory__HPI__Report.pdf\">Personality Inventory Report</a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">HOGAN Motives, Values and Preferences Inventory (MVPI)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Personality inventory that reveals a person&rsquo; score values, goals and interests. Results indicate which type of position, job and environment will be most motivating for the employee and when he/she will feel the most satisfied.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/fe2b810132b553bf45fb46b46ea0f8f2fc88eab5.pdf&amp;name=Hogan_Motives__Values_and_Preferences_Inventory__MVPI__Report.pdf\">Motives, Values and Preferences Inventory Report</a></p>', '', '', ''),
(132, 127, 'en', 'Innovative', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Innovative\" src=\"/uploads/images/c9e218597b004492d90c1cdcc646df50a0557094.png\" width=\"357\" height=\"50\" />Innovative 360</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Our most popular 360 Survey.<span class=\"Apple-converted-space\">&nbsp; </span>100% customised to your Competency Framework.<span class=\"Apple-converted-space\">&nbsp; </span>Ability to collect quantitative and qualitative feedback throughout. Ability to customise communication templates and rating scales to suit the culture and practices of your organisation.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/5ed8ad517e4d7fd2d9d6c9559e312facb5f2b1a3.pdf&amp;name=360_Report_Ahmed_Al_Ali_-_English_Sample.pdf\">Innovative 360&nbsp;Report</a></p>', '', '', ''),
(133, 128, 'en', 'Performance 360', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Saville\" src=\"/uploads/images/31b5f239562b84eefdc5b19b4a52ef2a76be3a8e.png\" width=\"305\" height=\"98\" />Performance 360</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Provides a multi-rater perspective against the Wave model of leadership. International normed and can be used for all levels.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All &ndash; up to Senior Executives </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<p class=\"p1\"><span class=\"s1\">Contact us for a sample report!</span></p>\r\n<h3 class=\"p1\">&nbsp;</h3>', '', '', ''),
(134, 129, 'en', 'EQ', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"EQ\" src=\"/uploads/images/38773a6d93e72738713d6724b753063c172f806e.jpg\" width=\"308\" height=\"96\" />EQ 360</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">Provides a multi-rater perspective on the emotional intelligence of the test taker factoring in the views of peers, managers, direct reports, and other groups as required.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">All- Up to Senior Executives</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/b7b84de431654b16ae80d2c470ed67eea58567e9.pdf&amp;name=Jack_Doe_EQ360_Feedback_Standard_Client_-_update_Nov_15_RF.pdf\">Feedback Standard Client Report</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/3ad1f87d749d380d14b9d20cfca9bacb73568302.pdf&amp;name=Jack_Doe_EQ360_Feedback_Standard_Coach_-_update_Nov_15_RF.pdf\">Feedback Standard Coach&nbsp;Report</a></p>', '', '', ''),
(136, 131, 'en', 'Hogan', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Hogan\" src=\"/uploads/images/95e8a1572ba7a7b5afc3bcac5dc441681a4ff6e0.jpg\" width=\"123\" height=\"152\" />Hogan 360</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">The Hogan 360&deg; is aligned with Hogan&rsquo;s core assessments. The Hogan 360&deg; provides a real-time look at an individual&rsquo;s attitude, behaviour, and performance. The report offers constructive feedback around leadership expectations and sets priorities for improvement.</span></p>\r\n<h3 class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">TARGET CATEGORY</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">Senior Executives and Leaders</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/49b302dfe6175d21df6ec38912b1819bd0ef699a.pdf&amp;name=Hogan_360_Sample_Report_with_Open_Ended_Questions.pdf\"><span class=\"s1\">Hogan 360&nbsp;Report</span></a></p>\r\n<p class=\"p1\">&nbsp;</p>', 'Hogan', '', ''),
(138, 133, 'en', 'NEO Conversion', '', '', '', ''),
(141, 136, 'en', 'Assessor Skills', '', '', '', ''),
(142, 137, 'en', 'Online Wave Conversion , Saville Assessment', '<p style=\"text-align: justify;\">This course is designed for those who hold the British Psychological Society Qualification in Occupational Testing Ability and Personality (formerly Level A &amp; P) and wish to start using Wave, Saville Assessment personality tools. The British Psychological Society Qualification in Occupational Testing Ability and Personality is a pre-requisite for this course. The course certifies delegates to achieve proficiency in interpreting and feeding back the Wave suite of personality tools. It also enables delegates to understand how to apply the Wave in selection and development. The&nbsp; Wave model combines personality, motivation and preferred culture and is regarded as the next generation in personality assessment. In order to qualify for this course you must present your current A&amp;P certificates to our team.</p>\r\n<p style=\"text-align: justify;\"><a href=\"/contact\"><span class=\"bold\">Contact Us</span> </a>and register your interest!</p>', 'Saville Conversion Online Course', '', 'This course certifies delegates to achieve proficiency in interpreting and feeding back the Saville Wave suite of personality tools.'),
(143, 138, 'en', 'Casestudy', '', 'Leading Hoteliers! Maximising Excellence in Rotana Leaders | IHS', '', ''),
(144, 139, 'en', 'Booking', '', '', '', ''),
(145, 140, 'en', 'Thank You', '<p><strong>Thank you for booking!</strong></p>\r\n<p><strong>One of our team members will be in touch with you shortly!&nbsp;</strong></p>\r\n<p><strong>If you have any questions regarding your Accreditation booking please contact us on +97143902778 or email&nbsp;<a href=\"mailto:info@ihsdubai.com\">courses@innovative-hr.com</a></strong></p>\r\n<p><strong>If you have any questions regarding your event booking please&nbsp;contact us on +97143902778 or email&nbsp;<br /></strong><strong><a href=\"mailto:info@ihsdubai.com\">info@innovative-hr.com</a></strong></p>\r\n<p><strong>Please like us on Facebook and Twitter and follow us on LinkedIn for all the latest talent thinking updates and news!</strong><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>', '', '', ''),
(146, 141, 'en', 'Something went wrong', '<p>Whoops! Looks like something went wrong. Please try again later or contact us on +97143902778.</p>', '', '', ''),
(147, 4, 'ar', 'الصفحة الرئيسية', '', '', '', ''),
(148, 17, 'ar', 'النشرة الإخبارية', '<p>ابقَ على اطلاع بآخر التطورات حول المواهب</p>\r\n<p>دعوة لحضور إفطار مجاني حول التطوير المهني والأنشطة</p>\r\n<p>تعرف على مستجدات برامج التدريب المعتمدة</p>', '', '', ''),
(149, 119, 'ar', 'شكراً على تسجيل البريد الالكتروني', '<p><strong>شكراً لاشتراكك بنشرتنا الاخبارية. نسعد بانضمامك إلى مجتمع الموارد البشرية الخاص بنا الذي يهدف إلى التبادل المعرفي والإطلاع على آخر الأخبار والأنشطة. </strong></p>\r\n<p><strong>اشترك بحسابنا على الفيسبوك وتويتر وقم بمتابعتنا على حساب LinkedIn الخاص بنا للحصول على آخر المستجدات والأخبار بخصوص المواهب. </strong></p>\r\n<p><strong>إذا كانت لديك أي اسئلة بشأن كيفية المساعدة في تحقيق أهداف عملك، يرجى الاتصال بنا على الرقم: +97143902778أو التواصل معنا عبر البريد الالكتروني : <a href=\"mailto:info@ihsdubai.com\">info@innovative-hr.com</a> </strong></p>', '', '', ''),
(150, 42, 'ar', 'فريقنا', '<p>يتكون فريقنا المتقدم من مجموعة من اختصاصيي علم النفس المهني، اختصاصي التطوير القيادي ورأس المال البشري. يضعنا تنوع مهاراتنا وخلفياتنا وخبراتنا في موقع فريد من اجل دعم تصميم الموارد البشرية في مؤسستك وتطوير المهارات مع تركيز ثابت على التنفيذ العملي والنتائج من العمل.</p>', '', '', ''),
(151, 43, 'ar', 'عملاءنا', '<p>قام فريقنا المتخصص بتنفيذ وتصميم مجموعة من مبادرات الموارد البشرية الاستراتيجية مع عدد من المؤسسات في مختلف الصناعات.</p>', '', '', ''),
(152, 11, 'ar', 'الإستشارات', '<p class=\"left\"><span class=\"bold\">&nbsp; &nbsp;من المهم جدا وضع استراتيجيات و أطر المواهب المناسبة لتحقيق نتائج العمل المرغوبة للنجاح بالإضافة إلى ضمان مميزات تنافسية في عملك أو مؤسستك</span></p>\r\n<p class=\"left\"><span class=\"bold\">يسرنا العمل معك بشكل مقرب لتطوير و توصيل و غرز استراتيجيات مواهب متناسقة لتحقيق النتائج العملية المرجوة&nbsp;</span></p>', '', '', ''),
(153, 46, 'ar', 'تصميم أطر الكفاءات ', '<p>الكفاءات هي القدرات والمهارات الأساسية للأداء الفعَال والتي يمكن تعريفها &nbsp;بصيغة سلوكية قابلة للتطوير والمراقبة والقياس. تؤلف هذه الكفاءات لغة مشتركة حول السلوكيات الناجحة في المؤسسة التي تستند عليها عمليات الموارد البشرية الفعالة والتي تدعم الأداء المؤسسي. يمكن استخدام أطر الكفاءات السلوكية لدعم أنشطة التوظيف، والترقيات، وتطوير الموظفين، وإدارة الأداء، والتخطيط والتعاقب الوظيفي.</p>\r\n<p>هل يتضمن إطار الكفاءة في مؤسستك ادارة المواهب واستراتيجية العمل ويوفر للموظفين مفهوم واضح للسلوكيات والمهارات المطلوبة؟ هل يساعدالإطارالأفراد على فهم بيئة العمل المرغوبة والتي ستساعدهم في&nbsp;ادراك الرؤية المؤسسية؟</p>\r\n<p>نبتدئ&nbsp;بتمارين الرؤية المؤسسية والمحادثة الاستراتيجية للمستوى الأعلى في قيادة المؤسسة من اجل الفهم الكامل للاستراتيجية وكيف يمكن لبعض السلوكيات ان تدعم الخطة المؤسسية. نقوم بعد ذلك بالعمل بصورة مباشرة معك من اجل تصميم استراتيجية تنفيذ مبتكرة ومشوقة شاملة للمؤسسة.</p>', '', '', ''),
(154, 47, 'ar', 'إدارة الأداء', '<p class=\"left\">تهدف عملية إدارة الأداء إلى ارشاد توقعات الأفراد والفرق والأداء المؤسسي. وتقوم بتوفير اجراء ذو معنى يمكّن عملية مكافأة الموظفين لمساهماتهم القيمة للمؤسسة، كما توفر آلية لتطوير الأداء الفردي والمؤسسي. انشاء اهداف محددة وقابلة للتنفيذ يعد امراً ضرورياً &nbsp;لنجاح اي مبادرة أداء.</p>\r\n<p class=\"left\">افادة الأداء عملية طبيعية من عمليات ادارة الأداء، حيث تسهل عمليات اعطاء النصح المحددة في الوقت الملاءم حول أداء الموظفين &nbsp;ضمن التوقعات المؤسسية ، &nbsp;كما تسهل تأسيس خطط التطوير الشخصي. وتضمن عملية افادة الأداء الفعالة ان أهداف الموظفين تتلائم مع أهداف المؤسسة الكبرى، والرؤية والمهمة والقيم. كما &nbsp;ستمكن من وضع خطة التطوير الفردي التي تتلائم مع الأهداف المؤسسية والاحتياجات الأساسية وتضمن العدل والحيادية في المكافأة والتميز.</p>\r\n<p class=\"left\">نقوم بالعمل مع المؤسسات &nbsp;لتصميم استراتيجيات ادارة الأداء، والسياسات، والإجراءات، والنماذج المطلوبة. كما يمكننا توفير&nbsp;نماذجالتواصل الفعال وبرامج التدريب واستراتيجيات التنفيذ.</p>', '', '', ''),
(155, 48, 'ar', 'التعاقب الوظيفي وإدارة الأداء', '<p>تعرف عملية التعاقب الوظيفي بتحديد واستبقاء وتطوير الأفراد ذوي الإمكانيات العالية. وتطمح هذه العملية الى خلق مسار للمواهب للتأكد من ان المناصب القيادية الأساسية والأدوار الرئيسية يتم شغلها عن طريق افراد مهرة في الوقت الحالي والمستقبلي. يتم قيادة هذه العملية بشكل أساسي عن طريق الاستراتيجية، والأداء المؤسسي ومتطلبات كل مستوى أو دور وظيفي.</p>\r\n<p>مَن مِن الموظفين يعتبر موهوب؟ ماذا ستفعل لتطوير المواهب بعد تحديدها؟ من يجب أن يبقى في دائرة المواهب؟ ان المحددات الرئيسية لاستراتيجية برنامجنا تتضمن:</p>\r\n<ul>\r\n<li>تحديد العوامل الرئيسية للنجاح في المؤسسة.</li>\r\n<li>تصميم نموذج الكفاءات.</li>\r\n<li>تحديد الأدوار الرئيسية ومسارات الوظائف في المؤسسة.</li>\r\n<li>وضع عملية لتحديد الموظفين الذين يمتلكون المهارات المطلوبة أو من الممكن تطويرهم وتدريبهم للحصول على المهارات المطلوبة في المستقبل.</li>\r\n<li>توفير الوقت والموارد المطلوبة لتحضير الأفراد لأدوار مستقبلية.</li>\r\n</ul>\r\n<p>&nbsp;</p>', '', '', ''),
(156, 49, 'ar', 'برامج التوطين', '<p class=\"left\">&ldquo;ان الثروة الحقيقية للأمة في شبابها &hellip; الشباب المجهز بالتعليم والمعارف والذي يوفر سبل بناء الأمة وتقوية اسسها لإحراز التطور على جميع المستويات.\"&nbsp;</p>\r\n<p class=\"left\"><span class=\"bold\">الشيخ محمد بن زايد ال نهيان، ولي عهد ابوظبي</span></p>\r\n<p class=\"left\">يعتبر استقطاب المؤسسة مواهبها &ldquo;الوطنية\" وابقاءهم وتطويرهم من أهم &nbsp;استراتيجيات دول الخليج العربي. نركز في منهجنا على تطوير استراتيجيات متكاملة شاملة للموارد البشرية بالإضافة إلى رؤية ومهمة وقيم المؤسسة. نساعد المؤسسات كذلك على تحديد مسارها وتطوير الأهداف الذكية &nbsp;وأطر عمل على مستوى&nbsp; الأفراد والفرق والمؤسسات.</p>', '', '', ''),
(157, 12, 'ar', 'التقييم', '<p class=\"left\">كوننا اختصاصيو علم نفس مهني، فاننا مرخصون لتقديم خدمات التقييم بمستوىعالمي لدعم عمليات التوظيف والترقية وتحديد المواهب ذوي الإمكانيات العالية والتعاقب الوظيفي. نحن قادرون على التقييم الفردي للوظائف الحساسة، بالإضافة إلى تصميم برامج التقييم الكبيرة.</p>', '', '', ''),
(158, 40, 'ar', 'خدمات التقييم', '<p>ان تكلفة اختيار الموظف الخاطئ اذا كان للتوظيف أو الترقية أو التعاقب الوظيفي، قد تصل إلى مئات الالآف أوالملايين من الدولارات، ناهيك عن التأثير على المعنويات والإنتاجية. نعمل مع عدد من المؤسسات العالمية لتقييم المتقدمين للوظائف الحساسة كجزء من عملية التوظيف.</p>\r\n<p>عندما يطلب منا العميل مساعدته في التقييم نتخذ منهجاً شاملاً. نبحث عن اظهار السلوكيات القيادية الأساسية، والإمكانيات مثل التفكير الناقد، المقدرة على التعلم السريع، حل المشكلات، بالإضافة الى القيم والتوافق الملائم &nbsp;للمؤسسة.&nbsp;</p>\r\n<p>نقوم بتصميم منهجنا في ضوء طلب عملائنا ومتطلبات النجاح الأساسية للدورالوظيفي المطلوب والمؤسسة. يمكننا في نهاية المطاف أن نحصل على معلومات قيمة عن طريق استخدام اختبارات الشخصية والقدرات ودمجها مع المقابلات المنظمة. ومن اجل أن ندعم هذه المخرجات يمكننا ان نضيف حالة عملية أو إجراء مركز تقييم متكامل مع اختبارات مراقبة &nbsp;(تمارين محاكاة الاعمال). الأهم في ذلك مقدرة&nbsp;عملائنا على الوثوق والإطمئنان &nbsp;بقدرتنا على توفير توصيات واضحة لدعم التوظيف أو التطوير والتي يمكن تعزيزها &nbsp;بخطط عملية للمضي قدماً.</p>', '', '', ''),
(159, 41, 'ar', 'تقييم الشخصية', '', '', '', ''),
(160, 121, 'ar', 'نبذة عامة', '<p>تؤثر طريقة تصرف الأفراد وسلوكياتهم في مكان العمل على نجاحهم الوظيفي، حيث أن &nbsp;الأفراد يتصرفون بطريقة ثابتة &nbsp;في مختلف الأوقات والمواقف. وتساهم أدوات الشخصية في تحديد الطريقة النموذجية التي يتواصل بها&nbsp;الأفراد مع بعضهم ويتعاملون مع المهام ويستجيبون للمواقف. وتتوفر لدينا مجموعة من أدوات قياس الشخصية &nbsp;من ناشري الإختبارات العالميين، والتي تم اختيارها على اساس الموضوعية ومصداقيتها وموثوقيتها وسهولة استخدامها وكلفتها القليلة</p>', '', '', ''),
(161, 122, 'ar', 'أداة ساڤيل (Saville)', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Saville Consuting\" src=\"/uploads/images/152ae1bbf398d73af614e2222c8eccb21407883f.png\" width=\"278\" height=\"89\" /></span><span class=\"s1\">أنماط اختبارات ساڤيل وايڤ المهنية (Saville Wave Professional Styles)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تُشير أداة ساڤيل إلى أحد أدوات تحليل الشخصية ذات المصداقية والموثوقية العالية في السوق الحالية. وتستخدم الأداة لاختيار المواهب وتطويرها وإدارتها بالإضافة إلى تحديد الأفراد ذوي الإمكانيات العالية، والتعاقب الوظيفي، وبرامج القيادة والتدريب، والتخطيط الوظيفي. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p class=\"p1\"><a title=\"PS Expert Report\" href=\"/downloadFile?path=uploads/documents/61df0c94dab75dfaa389206df9955b792c7a52b8.pdf&amp;name=SW_PS_Expert_Report.pdf\">تقرير المختص </a></p>\r\n<p class=\"p1\"><a title=\"PS Line Manager Report\" href=\"/downloadFile?path=uploads/documents/724d146ea9a197fb3677400cf7af1505d7a11dd1.pdf&amp;name=SW_PS_Line_Manager_Report.pdf\">تقرير المدير المباشر </a></p>\r\n<p class=\"p1\"><a title=\"PS Personal Report\" href=\"/downloadFile?path=uploads/documents/f91008fe5ae12e6f47080b19ce0c1030bbac2ae8.pdf&amp;name=SW_PS_Personal_Report.pdf\">التقرير الشخصي</a></p>\r\n<h2 class=\"p1\"><span class=\"s1\">أداة ساڤيل وايڤ &ndash; Saville Wave Focus Styles</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تستخدم هذه الأداة في عملية توظيف أعداد كبيرة، والإختيار، والتطوير، وإدارة المواهب، والتعاقب الوظيفي، وتقييم فعالية الفرق، وتحديد الأفراد ذوي الإمكانيات المتقدمة. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات - وصولاً إلى كبار&nbsp;المدراء</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a title=\"FS Expert Report\" href=\"/downloadFile?path=uploads/documents/9e470d5076ea8f8f6146d28c1731df9d5f427005.pdf&amp;name=SW_FS_Expert_Report.pdf\"><span class=\"s1\">تقرير المختص </span></a></p>\r\n<p><a title=\"FS Line Manager Report\" href=\"/downloadFile?path=uploads/documents/cd340a4588178f97d6f63084a99d745faf1367db.pdf&amp;name=SW_FS_Line_Manager_Report.pdf\">تقرير المدير المباشر </a></p>\r\n<p><a title=\"FS Personal Report\" href=\"/downloadFile?path=uploads/documents/f966c9df87dc96cab1d2990fb0fda37be41ecc35.pdf&amp;name=SW_FS_Personal_Report.pdf\">التقرير الشخصي</a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">(استبيان ساڤيل وايڤ (نقاط القوة بالعمل - Saville Wave Work Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">هو استبيان للشخصية يحدد نقاط القوة المحتملة للفرد مقارنة بسلوكيات النجاح المطلوبة من الخريجين، ومتدربي الإدارة، والمدراء والعاملين بالأدوار المهنية. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار المدراء </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/45b98eac07f7f328975aefa5dc4b8f2189e8c98a.pdf&amp;name=SW_WS_Behavioural_Profile.pdf\">الملف السلوكي </a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/a2ce747fd4aa6d2399415ef8f207caf8053221c4.pdf&amp;name=SW_WS_Environmental_Fit_Report.pdf\">ملف ملائمة بيئة العمل </a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">(أداة ساڤيل (خدمة العملاء - Saville Customer Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تستخدم هذه الأداة لتحديد نقاط القوة المحتملة للأفراد مقارنة بسلوكيات النجاح المطلوبة للأدوار المتعلقة بخدمة العملاء. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">موظفي خدمة العملاء بمراكز الاتصال، المتاجر، السياحة، الرفاهية، الصحة والتعليم. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/a96aa5c2386a23366053ebbe77c3766bc4d64d51.pdf&amp;name=Customer Strengths Report.pdf\">تقرير نقاط قوة خدمة العملاء</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/8b2ae778e2ba1302e074aae8e4ac1c70d7d7f9ce.pdf&amp;name=Customer Strengths Environment Fit Report.pdf\">تقرير نقاط قوة بيئة خدمة العملاء</a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">(أداة ساڤيل (موظفي التشغيل - Saville Operational Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تستخدم هذه الأداة لتحديد نقاط القوة المحتملة للأفراد مقارنة بالسلوكيات المطلوبة للعاملين في الأدوار التقنية والصناعية والمتدربين. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">المتدربين المهنيين وموظفي التشغيل في مجال التصنيع والهندسة والبناء والنقل. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/4e6f1128c020f524afa1b0aa8b45dd13b4a70f46.pdf&amp;name=Operational Strengths Report.pdf\">تقرير نقاط قوة التشغيل</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/872cba058a9b9ca2366b7a6ec524bfbdab79db08.pdf&amp;name=Operational Strengths Environment Fit Report.pdf\">تقرير نقاط قوة بيئة التشغيل</a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">(أداة ساڤيل (الموظفين الإداريين - Saville Administrative Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تستخدم هذه الأداة لتحديد نقاط القوة المحتملة للأفراد مقارنة بالسلوكيات المطلوبة للعاملين في الأدوار الإدارية والمكتبية. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">جميع الموظفين الإداريين في القطاع الحكومي والخاص </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/d24ae4115d5db0ab9f1c40d1a4311771c8464796.pdf&amp;name=Administrative Strengths Report.pdf\">تقرير نقاط قوة الموظفين الإداريين</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/43ef91a9ef2de6cfda487a0f8280227417dc9856.pdf&amp;name=SC_Administrative_Strengths_Environment_Fit_Report.pdf\">تقرير نقاط قوة البيئة الإدارية</a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">(أداة ساڤيل (المجال التجاري - Saville Commercial Strengths</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تستخدم هذه الأداة لتحديد نقاط القوة المحتملة للأفراد مقارنة بالسلوكيات المطلوبة للعاملين في أدوار المبيعات والتسويق وتطوير الأعمال.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">جميع موظفي المبيعات والتسويق وتطوير الأعمال والخدمات المالية. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/a4a1dee4214d4b0e1e181f2b90cc5a52828b64b7.pdf&amp;name=SC_Commercial_Strengths_Report.pdf\">تقرير نقاط قوة الموظفين في المجال التجاري</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/74a0d53055343caa68bfe71906d774c5c878580b.pdf&amp;name=SC_Commercial_Strengths_Environment_Fit_Report.pdf\">تقرير نقاط قوة البيئة التجارية</a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">أداة ساڤيل الذاتية - Saville My Self</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">هي أداة للإرشاد الوظيفي والتطوير الذاتي تساعد على قياس المواهب المحتملة للأفراد ضمن مجموعة من المجالات بمكان العمل. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">الخريجين الجدد والمواهب الشابة</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/511bfdf6a49139517a3337fd0cd5ab388e3e7fe7.pdf&amp;name=SC_My_Self_Report.pdf\">التقرير الذاتي</a></p>', '', '', ''),
(162, 123, 'ar', 'مؤشر أنماط مايرز بريجيز MBTI', '<h2 class=\"p1\"><img class=\"center\" title=\"MBTI logo\" src=\"/uploads/images/79176094856874398328dfd25f5a1a1fcd4d230c.png\" width=\"146\" height=\"194\" />Myers Briggs Type Indicator (MBTI)</h2>\r\n<p>تستخدم أداة &nbsp;الام بي تي آي لتحديد انماط الشخصية من خلال&nbsp;اربعة أنماط بأقطاب متضادة. نمط الإنفتاح أو الإنغلاق ، النمط الحسي أو الحدسي، نمط التفكير أم الشعور، نمط الحكم أو الإدراك. تستخدم الام بي تي آي لاغراض التطوير فقط &ndash; سواء كان لتطوير الأفراد ام الفرق. حيث تسهل الام بي تي آي للأفراد والفرق الوصول الى فهم اعمق لانفسهم وتواصلهم مع الآخرين، ومساعدتهم على تطوير كيفية التواصل والتعلم والعمل.</p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/d60ac00311f2751572dab3cfd0dc149d3960549b.pdf&amp;name=MBTI_Step_I_Profile.pdf\">ملف الخطوة 1 </a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/254f73d2c8419a9e03820074f43d6ad437cee7dd.pdf&amp;name=MBTI_Step_II_Profile (1).pdf\">ملف الخطوة 2</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/e68d9ec82b2ba1851bc014959ef4606af4891c79.pdf&amp;name=MBTI_Step_I_Interpretive_Report.pdf\">تقرير مفصل عن الخطوة 1</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/01380a643916a3f9e3d4df0ef82ff9c67421576b.pdf&amp;name=MBTI_Step_II_Interpretive_Report.pdf\">تقرير مفصل عن الخطوة 2</a></p>\r\n<p>لا تزال أداة الام بي تي آي &nbsp;من أكثر أدوات التقييم موثوقية واستخداماً لفهم اختلافات الشخصية وكشف طرق جديدة للتعامل والتواصل مع الآخرين.&nbsp;</p>\r\n<p>يستخدم تقرير الام بي تي آي حصراً في عمليات التطوير لتحسين أداء الفرق والأفراد، لتطوير واستبقاء المواهب والقيادات عن طريق تجربة التطوير الشخصي والتوجيه، وتطوير التواصل وتقليل النزاع بين الفرق بالإضافة الى استكشاف عالم الأعمال والوظائف.&nbsp;</p>\r\n<p>سيتمكن الأفراد الذين يكملون الدورة بنجاح من الحصول على الترخيص لإدارة وشرح وإعطاء جلسات إفادة الأداء فيما يختص بالام بي تي آي (خطوة 1 و2). سيمكنك كذلك الالتحاق بمنتدى الام بي تي آي في الشرق الاوسط، وهو افطار نصف سنوي مجاني لممارسي الام بي تي آي لتبادل الخبرات والتطوير المستمر وصقل المهارات.&nbsp;</p>\r\n<p>تتألف الدورة من 10 ساعات من الدراسة الذاتية (قراءة واكمال تقييم ام بي تي آي) والوظائف المنزلية واختبارات قصيرة خلال الدورة التدريبية.</p>\r\n<h2 class=\"p1\">&nbsp;</h2>', '', '', ''),
(163, 125, 'ar', 'أداة إي كيو (EQ)', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"EQi\" src=\"/uploads/images/f60539ac8eb5b3db08bf21a58e8288d0f0bd5640.jpg\" width=\"273\" height=\"88\" />أداة إي كيو آي 2.0 (EQi 2.0)</span></h2>\r\n<p>تستخدم هذه الأداة للتطوير، وفي بعض الظروف للاختيار (التوظيف). يعد الذكاء العاطفي أحد أقوى المحددات للأداء القيادي، كما انه يفيد في تعيين كيفية الاستفادة العظمى من انفسنا والآخرين. يقدم الإي كيو آي نظرة مفصلة لكيفية فهمنا وادارتنا مع ذواتنا، وعلاقاتنا مع الآخرين، وكيفية تعاملنا مع الضغوط ومتطلبات الحياة.</p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة</span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/248bfe18588abf65db4cc7619b3c067fb60edae2.pdf&amp;name=EQ-i_2.0_Client_Report.pdf\">تقرير العميل </a></p>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/bb8dd99ddb2c69387330ddbf3443cd619e07e7d2.pdf&amp;name=EQ-i_2.0_Coach_Report.pdf\">تقرير الموجه</a></p>', '', '', ''),
(164, 124, 'ar', 'أداة نيو NEO  ', '<h2 class=\"p1\"><img class=\"centre center\" title=\"NEO\" src=\"/uploads/images/tn-600-cd50be1857a81567021e76e063cb16a7f7d54603.png\" width=\"135\" height=\"135\" />أداة نيو برامري كلرز 360&nbsp;NEO Primary Colours 360</h2>\r\n<p>تم تطوير نموذج برامري كلرز&nbsp;عبر 25 سنة بالاستفادة من البحوث الاكاديمية في مجال الاستشارات. يمزج تقرير الالوان الاساسية للقيادة ما بين تقرير نيو المتعمق للشخصية، مع نموذج الالوان الاساسية للقيادة من اجل خلق تقرير واضح وشامل عن قابلية الشخص القيادية.</p>\r\n<h3 class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">كافة الفئات وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h3 class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تحميل </span></h3>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\"><a href=\"/downloadFile?path=uploads/documents/1cd75bd05f14057d02b144f9a9731a3f5c318845.pdf&amp;name=NEO PI-3 Primary Colours Leadership Sample Report.pdf\">360 تقرير الألوان الأساسية للقيادة</a></p>', '', '', ''),
(165, 126, 'ar', 'استبيان هوغان', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Hogan\" src=\"/uploads/images/95e8a1572ba7a7b5afc3bcac5dc441681a4ff6e0.jpg\" width=\"123\" height=\"152\" />استبيان هوغان للتطوير (HDS) </span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\">يستخدم للتوظيف والتطوير &ndash; يحدد مخاطر الأداء بناءً على نمط الشخصية ومعوقات السلوك الذاتي.</p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/9066cdf012448477601bc72556d6e7a027e0acd4.pdf&amp;name=Hogan_Development_Survey__HDS__Report.pdf\">تقرير استبيان التطوير </a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار هوغان للشخصية (HPI) </span></h2>\r\n<p>اختبار هوغان للشخصية (إتش بي آي)</p>\r\n<p>أداة مثالية للمساعدة في دعم عمليات اختيار الموظفين والتطوير القيادي والتعاقب الوظيفي وادارة المواهب.</p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/f804541a5f58f102bdf20a9cbc5094f7c32edf30.pdf&amp;name=Hogan_Personality_Inventory__HPI__Report.pdf\">تقرير لائحة الشخصية </a></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">لائحة هوغان للدوافع والقيم والتفضيلات (MVPI) </span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\">&nbsp;تستخدم لتوضيح القيم والأهداف والاهتمامات الخاصة بالشخص. تبين النتائج أي من الوظائف والأدوار والبيئات ستكون الأفضل لتحفيز الموظف، الى جانب توضيح متى يشعر الموظف بأكبر قدر من الإرتياح.</p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/fe2b810132b553bf45fb46b46ea0f8f2fc88eab5.pdf&amp;name=Hogan_Motives__Values_and_Preferences_Inventory__MVPI__Report.pdf\">تقرير الدوافع والقيم والتفضيلات </a></p>', '', '', ''),
(166, 32, 'ar', 'نبذة عامة ', '<p class=\"left\">اختبارات القدرات او الكفاءة هي طريقة تقييم موحدة لمستوى الكفاءة للأشخاصعند&nbsp;القيام بالمهمات المختلفة والإستجابة للمشاكل، وقدرتهم على التعلم بمرونة&nbsp;ومهارات التفكير الناقد. تمتلك اختبارات القدرات السبق في قياس الإمكانية خلافاً عن&nbsp;الأداء الأكاديمي، وعادة ما يتم استخدامها للتنبؤ&nbsp;بأداء الأفراد في بيئة العمل. يتم قياس نتائج الأفراد عن طريق مقارنة أدائهم مع نتائج الأفراد آخرين \"مجموعة مقارنة\" ممن قاموا بإنهاء هذه الاختبارات وفي نفس المستوى الوظيفي. هناك نطاق واسع من مجموعات المقارنة&nbsp;لإختبارات القدرات والتي تضمن التقييم بالدرجة الأمثل.</p>\r\n<p class=\"left\">نوفر نطاقاً واسعاً من اختبارات القدرات التي تناسب مع مختلف الاحتياجات. يشمل هذا اختبار القدرات على الإدراك التجريدي، الاختبارات العامة (الإدراك اللفظي والرقمي والتفكير الإدراكي) القدرات العملية (المكانية والميكانيكية)</p>\r\n<p class=\"left\">لشراء اختبارات القدرة يتعين عليك ان تكون مرخصاً من الجمعية البريطانية لعلم النفس الاختبارات المهنية (القدرات والشخصية)</p>', '', '', ''),
(167, 29, 'ar', 'أداة ساڤيل Saville ', '<h2><img class=\"center\" title=\"Saville\" src=\"/uploads/images/152ae1bbf398d73af614e2222c8eccb21407883f.png\" width=\"309\" height=\"99\" />اختبار ساڤيل &ndash; Swift Analysis Aptitude (V,N,D) Combination</h2>\r\n<p>يقيس الإدراك والتفكير النقدي من خلال اختبارات قصيرة:</p>\r\n<p>&nbsp;اللفظي 6 دقائق، والرقمي 6 دقائق، واختبار البياني 6 دقائق.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p>الرؤساء والمدراء والخبراء والخريجون ومتدربو الإدارة.</p>\r\n<p>&nbsp;</p>\r\n<h2>اختبار ساڤيل &ndash; Swift Executive Aptitude (V, N, A) Combination</h2>\r\n<p>يقيس الإدراك والتفكير النقدي من خلال اختبارات قصيرة</p>\r\n<p>: اللفظي 6 دقائق، والرقمي 6 دقائق، و التجريدي 6 دقائق.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p>الرؤساء والمدراء والخبراء والخريجون ومتدربو الإدارة.</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Swift Analysis Aptitude (V &amp; N) Combination</span></h2>\r\n<p>يقيس الإدراك والتفكير النقدي من خلال اختبارات قصيرة:</p>\r\n<p>&nbsp;اللفظي 12 دقيقة، والرقمي 12 دقيقة وبدرجة مناسبة لجميع الادوار العليا.</p>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">&nbsp;</span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">الرؤساء والمدراء والخبراء والخريجون ومتدربو الإدارة. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Abstract Aptitudes</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">يستخدم هذا الاختبار لقياس الإدراك المجرَّد بجميع الأدوار. </span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Analysis Aptitudes (V, N, D or A) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يقيس التحليل اللفظي القدرة على تقييم المعلومات المكتوبة المعقدة. يقيس التحليل الرقمي القدرة على تقييم المعلومات الرقمية والبيانات. يقيس التحليل البياني القدرة على تقييم العمليات الممثلة من خلال الرسوم و الاشكال البيانية. </span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">الرؤساء والمدراء والخبراء والخريجون ومتدربو الإدارة. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Professional Aptitudes (V, N, D) Single Tests الاختبارات الفردية للمؤهلات المهنية</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\">يقيس التحليل اللفظي القدرة على تقييم المعلومات المكتوبة المعقدة. يقيس التحليل الرقمي القدرة على تقييم المعلومات الرقمية والبيانات. يقيس التحليل البياني القدرة على تقييم العمليات الممثلة من خلال الرسوم و الاشكال البيانية.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">الرؤساء والمدراء والخبراء. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Work Aptitudes (V, N, D) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\">يقيس التحليل اللفظي القدرة على تقييم المعلومات المكتوبة المعقدة. يقيس التحليل الرقمي القدرة على تقييم المعلومات الرقمية والبيانات. يقيس التحليل البياني القدرة على تقييم العمليات الممثلة من خلال الرسوم و الاشكال البيانية.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">الخريجون ومتدربو الإدارة. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Swift Comprehension Aptitude (V, N, E) Combination</span></h2>\r\n<p>يقيس الادراك العام عن طريق اختبارات &nbsp;قصيرة:</p>\r\n<p>اللفظي 4 دقائق، والرقمي 4 دقائق، والتحقق 90 ثانية.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">موظفو أقسام التشغيل والأقسام التجارية وخدمة العملاء ومتدربو الإداريون والموظفون بالمستوى الأول. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Comprehension Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يقيس التحليل اللفظي القدرة على تقييم المعلومات المكتوبة المعقدة. يقيس التحليل الرقمي القدرة على تقييم المعلومات الرقمية والبيانات. يقيس التحقق من الأخطاء القدرة على التحقق من صحة المعلومات المعطاة. </span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">متدربو القطاعات الخدمية والتشغيلية والتجارية وخدمة العملاء وموظفو الإدارة. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Operational Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p>يقيس الاستيعاب اللفظي القدرة على استيعاب المعلومات المكتوبة. يقيس التحليل الرقمي القدرة على استيعاب المعلومات الرقمية والبيانات. يقيس التحقق من الأخطاء القدرة على التحقق من صحة المعلومات المعطاة.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">موظفو التصنيع والهندسة والبناء والنقل </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Commercial Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\">يقيس الاستيعاب اللفظي القدرة على استيعاب المعلومات المكتوبة. يقيس التحليل الرقمي القدرة على استيعاب المعلومات الرقمية والبيانات. يقيس التحقق من الأخطاء القدرة على التحقق من صحة المعلومات المعطاة.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">موظفو خدمات المبيعات والتسويق وتطوير الأعمال والمالية. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Customer Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\">يقيس الاستيعاب اللفظي القدرة على استيعاب المعلومات المكتوبة. يقيس التحليل الرقمي القدرة على استيعاب المعلومات الرقمية والبيانات. يقيس التحقق من الأخطاء القدرة على التحقق من صحة المعلومات المعطاة.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">موظفو خدمة العملاء في مراكز الاتصال والسياحة والرفاهية والصحة والتعليم. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Administrative Aptitudes (V, N, E) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يقيس الاستيعاب اللفظي القدرة على استيعاب المعلومات المكتوبة. يقيس التحليل الرقمي القدرة على استيعاب المعلومات الرقمية والبيانات. يقيس التحقق من الاخطاء القدرة على التحقق من صحة المعلومات المعطاة. </span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">الموظفون الإداريون في القطاعين الخاص والعام </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Swift Apprentice Aptitude (V, N, E, S, M, D) Combination</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يستخدم هذا الاختبار لقياس كلٍ من الإدراك التقني والاستيعابي عن طريق اختبارات قصيرة، اللفظي (4 دقائق)، الرقمي (4 دقائق)، التحقق من الاخطاء (90 ثانية)، المكاني (3 دقائق)، الميكانيكي (3 دقائق) والبياني (4 دقائق). </span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p>متدربو التشغيل والتصنيع والهندسة والبناء</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Swift Technical Aptitude (S, M, D) Combination</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يستخدم هذا الاختبار لقياس الإدراك التطبيقي عن طريق اختبارات فرعية قصيرة، المكاني (3 دقائق) والميكانيكي (3 دقائق) والبياني (4 دقائق). </span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">متدربو الأقسام التقنية والانتاج والبناء والهندسة وموظفو المجالات العلمية مثل الانتاج والعمال والمتدربون والمهندسون والمصممون والعلماء. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Technical Aptitudes (S, M, D) Single Tests</span></h2>\r\n<p style=\"text-align: justify;\">يقيس الإدراك المكاني القدرة على تمييز الأشكال. ويقييس الإدراك الميكانيكي القدرة على تقييم الفهم الميكانيكي عن طريق الأسئلة التي تطرح مشكلة مع عدد من الأجوبة المحتملة. يقيس الإدراك البياني باستخدام الاشكال والقدرة على تقييم العمليات المقدمة من خلال الرسوم البيانية.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">المتدربون التقنيون وموظفو الانتاج والبناء والهندسة وموظفو المجال العلمي. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار ساڤيل &ndash; Practical Aptitudes (S, M, D) Single Tests</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يقيس الإدراك المكاني القدرة على تمييز الاشكال. ويقيس الإدراك الميكانيكي القدرة على تقييم الفهم الميكانيكي عن طريق الاسئلة التي تطرح مشكلة مع عدد من الأجوبة المحتملة. يقيس الإدراك البياني باستخدام القدرة على تقييم العمليات المقدمة من خلال الرسوم البيانية.</span></p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p class=\"p1\"><span class=\"s1\">موظفو الانتاج والبناء والهندسة والمجال العلمي </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<p class=\"p1\"><span class=\"s1\">&nbsp;</span></p>', '', '', ''),
(168, 30, 'ar', 'بيرسون (PEARSON)', '<h2 class=\"p1\"><span class=\"s1\">اختبار بيرسون رايفينز &ndash; Pearson Ravens (SPM)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">هو مقياس غير لفظي للمقدرة الذهنية، يساعد على تحديد الأشخاص ذوي القدرات المتقدمة في المراقبة والتفكير السليم، والذين يستطيعون التعامل مع تعقيد وغموض بيئة العمل في العصر الحالي. يقدم اختبار رايفنز معلومات حول قدرات الأشخاص على تحليل وحل المشكلات، الاستيعاب التجريدي، والقدرة على التعلم، كما أن الاختبار يقلل التحيز الثقافي كونه لا يستخدم اختبارات لغوية أو لفظية. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">الوظائف الإشرافية والمدراء في المستوى الابتدائي والمتوسط. </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار بيرسون رايفينز - Pearson Ravens (APM)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">هو مقياس غير لفظي للمقدرة الذهنية، يساعد على تحديد الأشخاص ذوي القدرات المتقدمة في المراقبة والتفكير السليم، والذين يستطيعون التعامل مع تعقيد وغموض بيئة العمل في العصر الحالي. يقدم اختبار رايفنز معلومات حول قدرات الأشخاص على تحليل وحل المشكلات، الاستيعاب التجريدي، والقدرة على التعلم، كما يقلل الاختبار من التحيز الثقافي كونه لا يستخدم اختبارات لغوية أو لفظية. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">وظائف كبار المدراء والمشاركون الفرديون في المستويات العليا. </span></p>\r\n<p class=\"p1\">&nbsp;</p>', '', '', '');
INSERT INTO `page_translations` (`id`, `page_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(169, 31, 'ar', 'اختبارات سايتك PSYTECH', '<h2 class=\"p1\"><span class=\"s1\">اختبار سايتك للإستدلال العام (PSYTECH &ndash; GRT2)</span></h2>\r\n<p>مقياس متكامل ومفصل ودقيق للقدرة الذهنية. صمم هذا الإختبار لتقييم قوة الإدراك لأصحاب القدرة العامة.</p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">جميع مستويات الموظفين </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار سايتك للخريجين (PSYTECH &ndash; GRT1)</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">مقياس شامل ومتعمق للقدرة الذهنية صُمَّم لتقييم مستويات الاستيعاب العالية. يقوم الإختبار بقياس الإدراك اللفظي والرقمي والتجريدي. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">المدراء والخريجون </span></p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<h2 class=\"p1\"><span class=\"s1\">اختبار سايتك للإستدلال التحليلي (PSYTECH &ndash; CRTB2)</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يوفر هذا الإختبار قياس مفصل ودقيق للإستدلال النقدي (اللفظي والرقمي). صُمَّم هذا الإختبار خصيصاً لتقييم المدراء. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">المدراء</span></p>', '', '', ''),
(170, 59, 'ar', 'اختبارات الحُكم الظرفية', '<p class=\"left\">تقيم اختبارات الحكم الظرفية القدرة على الحكم بحسب&nbsp;مواقف العمل لدور او عائلة وظيفية معينة. تستخدم اختبارات الحكم الظرفية كطريقة للإختيار من مجموعات من المتقدمين للوظائف، وتقدم لهم نظرة واقعية عن بيئة العمل المرتقبة. عادة ما تكون مثالية لجذب أفضل المواهب وعادة ما تستخدم لبرامج الخريجين المتدربين، حيث توفر فرصة للمؤسسة لاستعراض مزاياها، وفي نفس الوقت تحديد أفضل المتقدمين من ناحية التناسب الوظيفي.</p>\r\n<p class=\"left\">يقدم هذا الإختبارمجموعة من الظروف الإفتراضية الصعبة والتي قد يواجهها الموظف في مكان العمل، والتي قد تستدعي العمل مع الآخرين في فريق، التواصل مع الآخرين، ومجابهة مصاعب العمل. للاستجابة&nbsp; لكل واحد من هذه الظروف، يتعين على المشاركين اختيار واحد من الخيارات المقترحة. الصيغة الأشهر للإختبار تستدعي ان يقوم المشاركون باختيارالإجابة الأكثر فاعلية من بين عدة اجابات ، ولكن هناك صيغ اخرى من الإختبار تستدعي المشاركون أن يضعوا الإختيارات في التسلسل الأمثل لحل المشكلة.</p>\r\n<p class=\"left\">وبعكس العديد من الإختبارات السيكولوجية، فإننا ننصح بان لا يتم شراء اختبارات الحكم الظرفية الجاهزة، بل ان يتم تصميمها خصيصاً لحاجات الوظيفة وبيئة وممارسات المؤسسة.</p>\r\n<p class=\"left\">لذلك &nbsp;نبتدئ بعملية التحليل الوظيفي، نراجع الوصف الوظيفي، نراقب الأداء في مكان العمل ونقوم بإجراء المقابلات. عادة ما نقوم بتصميم كل اختبار ليكون ما بين 25 &ndash; 35 سؤال أو وصف قصير لمشكلة ظرفية. ويلحق كل وصف بسؤالين أو ثلاثة، وعدد من الأجوبة المفترضة التي من الممكن للمشارك أن يقوم بتقييمها.</p>\r\n<p class=\"left\">يمكننا أن نصمم هذا الإختباربصيغة واحدة أو عدة صيغ&nbsp;لمساعدة المؤسسة على&nbsp; اختيار أفضل المواهب. &nbsp;</p>', '', '', ''),
(171, 45, 'ar', 'أنشطة وتمارين مراكز التقييم', '<p>تُشير أنشطة مركز التقييم إلى تمارين&nbsp;محاكاة بيئة العمل والتي تستند على&nbsp;أحد مشاكل \"يوم العمل العادي\" وتسمح للمقيمين بمراقبة وتقييم&nbsp;السلوكيات القيادية بصورة مباشرة. إن مراقبة الأفراد أثناء تقييمهم لمشاكل العمل المعقدة واتخاذ القرار والتوصل إلى الحلول للمشاكل يوفر لنا فرصة ممتازة لتقييم ملائمة الأفراد لدور معين أو تحديد امكانياتهم القيادية العامة.</p>\r\n<p>تُمكِّن هذه الأنشطة المشاركين من إظهار مهاراتهم المختلفة وسلوكياتهم وطرقهم الخاصة في تحقيق أفضل النتائج للمهام التي يقومون بها. وكما هو الحال في الحياة الواقعية، يتم تصميم أنشطة محاكاة الأعمال لمختلف بيئات العمل مثل العمل الفردي، أو مع شخص آخر، أو العمل ضمن مجموعة. يتم اختيار الأنشطة وفقاً لملاءمتها لمعايير التقييم والدرجة الوظيفية وبيئة العمل والكفاءات الأساسية للوظيفة. تحتوي مكتبتنا على أكثر من 250 نشاط مختلف بما في ذلك:</p>\r\n<ul>\r\n<li>تمارين&nbsp;المناقشات الجماعية</li>\r\n<li>الأنشطة الجماعية</li>\r\n<li>تمارين&nbsp;تمثيل الأدوار داخل أو خارج المؤسسة</li>\r\n<li>المقابلات المنظمة</li>\r\n<li>تمارين&nbsp;التحليل ودراسات الحالة التي تنتهي بتقرير أو عرض تقديمي</li>\r\n<li>تمارين&nbsp;التنظيم و الأولوية</li>\r\n<li>أنشطة استدلال الحقائق</li>\r\n</ul>\r\n<p>تتم هذه الأنشطة بالاستعانة بالمقيمين المعتمدين الذين تم تدريبهم لمراقبة وتسجيل سلوك المشاركين وتصنيف وتقييم أدائهم. ونقوم بتوفير الموارد من قبل اثنين من ناشري الإختبارات والتمارين الرائدين عالمياً، اي اند دي سي وجلوبال ليدر (A&amp;DC , Global Leader). يمكننا توجيهك لاختيار مجموعة الأنشطة الأفضل والأكثر ملائمة لاحتياجاتك، وتدريبك للقيام بإدارة مراكز التقييم الخاصة بك. كما يمكننا القيام بإدارة مركز التقييم بالكامل بالنيابة عنك.</p>\r\n<p>إذا كنت ترغب أن تصبح مقيِّم معتمد أو منسق مركز تقييم، قم بالتسجيل في دورات مهارات التقييم وإدارة المراكز المعتمدة من قبل جمعية علم النفس البريطانية.</p>\r\n<p>مهارات التقييم: اضغط هنا للمعلومات عن الدورة</p>\r\n<p>إدارة مراكز التقييم: اضغط هنا للمعلومات عن الدورة</p>', '', '', ''),
(172, 60, 'ar', 'استبيانات 360 ', '', '', '', ''),
(173, 61, 'ar', 'نبذة عامة', '<p style=\"text-align: justify;\">نظرتك لنفسك ونظرة الآخرين لك ستؤدي الى محادثة تطويرية مهمة. لقد قمنا بتجربة واختبار أدوات 360 بأنواعها ، سواء الجاهزة حيث يتم تقييم الموظفين بحسب إطار الكفاءات العالمي، أو تلك التي يتم تصميمها لإطار الكفاءات الخاص بك. وبعكس محادثات الأداء التقليدية، حيث يقوم المسؤول المباشر بتقييم أداء مرؤوسيه، فإن تقييم 360 يقوم باستخدام العديد من وجهات النظر من مصادر مختلفة. هذه المصادر تشمل الموظف ذاته، المدير المباشر، الزملاء، المرؤوسون، والعملاء والموردون الأساسيون عند الرغبة. معظم أدواتنا تسمح بالملاحظات الكمية والنوعية لتقوية وتطوير عملية إفادة الأداء. بالإمكان استخدام أدوات 360 لدعم تحليل متطلبات التدريب، تخطيط التطوير، بالإضافة إلى امكانية جعلها جزءاً من عملية إدارة الأداء.</p>', '', '', ''),
(174, 127, 'ar', 'أداة إينوفاتيف (INNOVATIVE) ', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Innovative\" src=\"/uploads/images/c9e218597b004492d90c1cdcc646df50a0557094.png\" width=\"357\" height=\"50\" />أداة إينوفاتيف 360 (Innovative 360) </span></h2>\r\n<p>تُعدّ من اشهر أدوات 360. حيث بالإمكان تصميمها&nbsp;100% لتلائم إطار الكفاءة الخاص بمؤسستك. تتميز أداة انوفيتف 360 &nbsp;بقابليتها لجمع معلومات إفادة الأداء الكمية والنوعية بشمولية. وتتيح الفرصة&nbsp;لتطوير نماذج التواصل ومعايير التقييم لملائمة بيئة وممارسات مؤسستك.</p>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/5ed8ad517e4d7fd2d9d6c9559e312facb5f2b1a3.pdf&amp;name=360_Report_Ahmed_Al_Ali_-_English_Sample.pdf\">تقرير أداة إينوفاتيف 360 </a></p>', '', '', ''),
(175, 128, 'ar', 'أداة 360 للأداء من ساڤيل - Saville Performance 360', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Sav\" src=\"/uploads/images/152ae1bbf398d73af614e2222c8eccb21407883f.png\" width=\"305\" height=\"98\" />أداة 360 للأداء من ساڤيل - Saville Performance 360</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تستخدم هذه الأداة لتقديم وجهات النظر من قبل عدة مقيمين ضمن&nbsp;إطار قيادة ساڤيل وايف (Saville Wave). وتتضمن مجموعة مقارنة&nbsp;عالمية يمكن استخدامها لمختلف المستويات. </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة المستويات &ndash; وصولاً إلى كبار الموظفين التنفيذيين </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/1f2fe6ed35660fa610ef9d723aaff498fd3dbd39.pdf&amp;name=Jo_Wilson__Performance_360.pdf\">تقرير أداة 360 للأداء </a></p>', '', '', ''),
(176, 129, 'ar', 'أداة 360 EQ ', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"EQ\" src=\"/uploads/images/38773a6d93e72738713d6724b753063c172f806e.jpg\" width=\"308\" height=\"96\" />أداة 360 EQ </span></h2>\r\n<p>تقدم وجة نظر عدد من المقيمين للأفراد الذي يكملون اختبار الذكاء العاطفي، وتشتمل على أراء الزملاء والمدراء والمرؤوسين المباشرين وأفراد مجاميع اخرى بحسب الرغبة.</p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/b7b84de431654b16ae80d2c470ed67eea58567e9.pdf&amp;name=Jack_Doe_EQ360_Feedback_Standard_Client_-_update_Nov_15_RF.pdf\">تقرير الافادة القياسي للعميل </a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/3ad1f87d749d380d14b9d20cfca9bacb73568302.pdf&amp;name=Jack_Doe_EQ360_Feedback_Standard_Coach_-_update_Nov_15_RF.pdf\">تقرير الافادة القياسي للموجه</a></p>', '', '', ''),
(177, 131, 'ar', 'أداة هوجان 360 ', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"Hogan\" src=\"/uploads/images/95e8a1572ba7a7b5afc3bcac5dc441681a4ff6e0.jpg\" width=\"123\" height=\"152\" />أداة هوجان 360 </span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تتماشى أداة هوجان 360 مع تقييمات هوجان الأساسية. توفر أداة هوجان 360 نظرة واقعية عن توجهات وسلوكيات وأداء المشارك. يشتمل التقرير على اقتراحات تطويرية بناءة حول توقعات القيادة بجانب وضع أولويات للتطوير. </span></p>\r\n<h3 class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\">&nbsp;كبار الموظفين التنفيذيين والقياديين</p>\r\n<h3 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/49b302dfe6175d21df6ec38912b1819bd0ef699a.pdf&amp;name=Hogan_360_Sample_Report_with_Open_Ended_Questions.pdf\"><span class=\"s1\">كبار الموظفين التنفيذيين والقادة </span></a></p>\r\n<p class=\"p1\">&nbsp;</p>', '', '', ''),
(178, 90, 'ar', 'أداة 360 للألوان الأساسية من نيو - NEO Primary Colours 360', '<h2 class=\"p1\"><span class=\"s1\"><img class=\"center\" title=\"NEO\" src=\"/uploads/images/bb4a5dd3a512bad9507f82c9a382a1337bbd6f0b.jpg\" width=\"135\" height=\"135\" />أداة 360 للألوان الأساسية من نيو - NEO Primary Colours 360</span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تم تطوير نموذج الألوان الأساسية من خلال البحوث الأكاديمية والخبرات الاستشارية على مدار 25 سنة. يقوم تقرير الألوان الأساسية بدمج كلٍ من تقرير NEO PI-R المتعمق للشخصية مع نموذج الألوان الأساسية للقيادة من أجل إنشاء تقرير واضح وشامل عن قدرات الشخص القيادية.</span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h3>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين </span></p>\r\n<h3 class=\"p1\"><span class=\"s1\">تحميل </span></h3>\r\n<p><a href=\"/downloadFile?path=uploads/documents/9ed9540952a8f31914fab64cd7c90ce1e84a0402.pdf&amp;name=Primary Colours Leadership 360 Example Report.pdf\">تقرير الألوان الأساسية 360 للقيادة</a></p>', '', '', ''),
(179, 67, 'ar', 'استبيانات الارتباط المؤسسي', '<p>يعد قياس البيئة المؤسسية مع نظرة على تحسين الاداء المؤسسي من اصعب التحديات التي تواجه القيادة. تشمل بيئة العمل على مجموعة مترابطة من الاهداف والادوار والعمليات والقيم وادواة التواصل والممارسات والتوجهات والافتراضات. تتناغم هذه العناصر مع بعضها كنظام متجانس يمنع اي محاولات لتغييره. ولذلك فان الحلول الموحدة من الممكن ان تظهر انها تحرز بعض التقدم لبعض الوقت، ولكن في النهاية فان العناصر المتجانسة تسيطر.</p>\r\n<p>ان عمليتنا التحليلة هي عملية مركبة تشتمل استفتاءات المناخ العملي ، مقابلات القيادة، مجموعات التركيز، بالاضافة الى التشخيص القيادي. ان الاستفتاءات ومجموعات التركيز والمقابلات ليست جديدة، ويتعمد نجاحها على الاسئلة التي يتم سؤالها وكيفية التواصل من البداية للوصول الى التحليل النهائي.</p>\r\n<p>ما يميز عمليتنا اننا نقوم باشمال التشخيص القيادي كجزء من عملية التحليل. نحن نؤمن بان اغلب الموظفين يتعلمون \"كيفية سير الامور\" عن طريق السلوكيات اليومية للقادة.&nbsp; يقوم القادة بصقل بيئة المؤسسة وهذه البيئة تصقل سلوكيات القيادة.</p>\r\n<p style=\"text-align: justify;\">ولذلك فان طريقتنا تشتمل على تحليل متكامل، يشتمل على التحليل الدقيق المدروس والتوصيات للمستقبل.</p>', '', '', ''),
(180, 21, 'ar', 'المقابلات القائمة على الكفاءات – سي بي آي سمارت (CBI Smart) ', '<p class=\"left\">لقد أثبتت البحوث أن المقابلات المنظمة تضاعف أحتمالية استقطاب و توظيف المرشح الكفئ بمرتين مقارنة بالمقابلات الأعتيادية و الغير منظمة.</p>\r\n<p class=\"left\">تتميز أداة سي بي آي سمارت بضمان تنظيم أسئلة تتوافق بشكل مباشر مع الكفاءات التي تحدد من اساسيات عوامل الأداء الفعال في مؤسستك.</p>\r\n<p class=\"left\">بالإضافة الى ذلك يوجد لدينا طيف من الحلو الموجهة إلى المقابلات المنظمة بما في ذلك التدريب على مهارات و تصميم المقابلات المنظمة لموظفي مؤسستك.</p>', '', '', ''),
(181, 20, 'ar', 'التقييمات المتخصصة', '<p style=\"text-align: justify;\">في بعض الاحيان يكون نطاق عملنا أكثر تخصصاً ونحتاج لتقييم غاية معينة. كدعم الخريجين والموهوبين الشباب لاختيار وظائفهم المستقبلية. او قد يكون هناك تحديات معينة لاحد الاعمال، كأن يكون هناك نزاع بين افراد الفريق. تم أختيار جميع هذه الأدوات على أساس مصداقيتها وموثوقيتها وسهولة استخدامها وكلفتها القليلة باللإضافة إلى حياديتها.</p>', '', '', ''),
(182, 68, 'ar', 'تخطيط المستقبل الوظيفي', '', '', '', ''),
(183, 70, 'ar', 'أداة ساڤيل الذاتية - Saville My self', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تستخدم هذه الأداة للتوجيه الوظيفي والتطوير الذاتي عن طريق قياس المواهب المحتملة للأفراد عبر طيف&nbsp;من مجالات بيئة العمل. </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">الخريجون الجدد والمواهب الشابة </span></p>\r\n<h2 class=\"p1\"><span class=\"s1 bold\">تحميل </span></h2>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/6edf9242327a76660b85c79e45929cbce4e28cb4.pdf&amp;name=SC_My_Self_Report.pdf\">التقرير الذاتي </a></p>', '', '', ''),
(184, 69, 'ar', 'أداة Strong Interest Inventory', '<p class=\"p1\"><span class=\"s1\">تستخدم هذه الأداة للتخطيط الوظيفي لطلبة المدارس الثانوية والكليات والجامعات &ndash; بالإضافة إلى المواهب الشابة والأفراد الراغبين في تغيير وظائفهم. </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">طلاب المدارس الثانوية والكليات والجامعات والأفراد الراغبين في تغيير وظائفهم. </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/94ba444dd25fa2347493f982231e2e5f103fb042.pdf&amp;name=ST-284108_-_Strong_Profile.pdf\">تقرير Strong Interest Inventory</a></p>', '', '', ''),
(185, 27, 'ar', 'إدارة العلاقات الشخصية والنزاعات ', '', '', '', ''),
(186, 71, 'ar', 'أداة فايرو بي – FIRO-B ', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تساعد هذه الأداة الأفراد على فهم سلوكهم الشخصي وسلوك الآخرين من خلال مواقف التعامل مع الآخرين. </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/40016b2f57f60f77eb621d2c217b88694aee4322.pdf&amp;name=FIRO-B-220160 - FIRO B Profile.pdf\">تقرير فايرو بي &ndash; FIRO-B </a></p>', '', '', ''),
(187, 72, 'ar', 'تقرير فايرو الأعمال – FIRO-Business', '<p class=\"p1\">تم تصميم هذه الأداة للاستخدام مع العملاء الراغبين بفهم احتياجات التواصل الشخصي الخاصة بهم والحصول على رؤية أفضل حول كيفية تأثير هذه الاحتياجات على سلوكهم المهني وأسلوب قيادتهم.</p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/d5e8bb7a5f3ee4a9491e28cb87aa26ed698c6c29.pdf&amp;name=FIRO-B-220170_-_FIRO_Business_Profile.pdf\">تقرير فايرو الأعمال &ndash; FIRO Business</a></p>', '', '', ''),
(188, 73, 'ar', 'أداة توماس كيلمان لأنماط النزاع (TKI) ', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تساعد هذه الأداة الأفراد على تعلم كيفية تجاوز النزاعات والتركيز على تحقيق الأهداف المؤسسية. يُمكن للمؤسسات تطبيق مؤشر توماس كيلمان عند مواجهة تحديات مثل التغيير الإداري&nbsp;وبناء الفرق والتطوير القيادي وإدارة الضغوط بالإضافة إلى مهارات التفاوض أو التواصل. </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/290b4bb9dbf115f06e9d50c1524217185f31853c.pdf&amp;name=TKI_Profile_and_Interpretive_Report.pdf\">تقرير مؤشر توماس كيلمان</a></p>', '', '', ''),
(189, 74, 'ar', 'السلامة في بيئة العمل ', '', '', '', ''),
(190, 75, 'ar', 'تقرير هوجان للسلامة ', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تؤدي حوادث العمل إلى تكبد الضغوط و النفقات غير الضرورية. مع ذلك يميل بعض الأفراد إلى اتباع سلوكيات غير آمنة في العمل بسبب الإهمال أو التهور أو أسباب أخرى. </span></p>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">يساعد تقرير هوجان للسلامة على تجنب بعض هذه المشاكل عن طريق تحديد السلوكيات الخطرة التي يتبعها الأشخاص والتي قد تؤدي إلى الحوادث بمكان العمل وغيرها من السلوكيات غير الآمنة. </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">جميع المستويات </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/5916c3fe71f9e1c0a51673bab75cfda34eec453a.pdf&amp;name=safety_report.pdf\">تقرير هوجان للسلامة </a></p>', '', '', ''),
(191, 76, 'ar', 'الابتكار والتغيير ', '', '', '', ''),
(192, 77, 'ar', 'أداة مي 2 – ME2', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">هي أداة تشخيصية تستخدم في مكان العمل من أجل تقييم نقاط القوة والأسلوب الإبداعي للأفراد و التي يمكن تطبيقها&nbsp;على فرق العمل أو المؤسسات بالكامل، وملاحظة كيف تتلاءم مع بعضها بشكل إبداعي. يدعم الإبداع كلٍ من التفكير الخلاق وحل المشكلات. </span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئات المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/3799cc9dfeda367414a125155a2ca7dcfa474ab4.pdf&amp;name=Me2_Development_Report.pdf\">تقرير مي 2 &ndash; ME2</a></p>', '', '', ''),
(193, 78, 'ar', 'القوة الذهنية والمرونة ', '', '', '', ''),
(194, 79, 'ar', 'أداة القوة الذهنية 48 (MTQ 48)', '<p>تقيم هذه الاداة القوة الذهنية فيما يختص بالمكونات الاساسية الاربعة: السيطرة، الالتزام، التحدي والثقة لكي تساعد الافراد على تغيير قواهم الذهنية او كيفية استخدام السلوكيات التي يتطلبها الاشخاص اصحاب الأذهان القهوية.</p>\r\n<h2 class=\"p1\"><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\"><span class=\"s1\">كافة الفئات &ndash; وصولاً إلى كبار الموظفين التنفيذيين</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/307d9ec90054104e39201280a97671d3382d0394.pdf&amp;name=MTQ48_Development_Report.pdf\">تقرير القوة الذهنية (MTQ 48)</a></p>', '', '', ''),
(195, 80, 'ar', 'لغة مكان العمل ', '', '', '', ''),
(196, 81, 'ar', 'اختبار ساڤيل للغة الانجليزية ', '<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">تـُقيِّم اختبارات اللغة الانجليزية قدرة الأشخاص على فهم واستخدام اللغة الانجليزية في مكان العمل. </span></p>\r\n<h2><span class=\"s1\">الفئة المستهدفة </span></h2>\r\n<p class=\"p1\" style=\"text-align: justify;\"><span class=\"s1\">ينطبق على مجموعة واسعة من الأدوار مثل الإداريين وموظفي مراكز الاتصال ومساعدي المبيعات وموظفي السياحة والموظفين في المجال الطبي</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">تحميل </span></h2>\r\n<p class=\"p1\"><a href=\"/downloadFile?path=uploads/documents/38cd6490e87dace68e877cb5e9c9c5fc7ba14efa.pdf&amp;name=workplace_english_-_customer_service_report_032011.sflb.pdf\">تقرير اللغة الانجليزية بمكان العمل </a></p>', '', '', ''),
(197, 13, 'ar', 'التطوير ', '<p class=\"right\">بعد القيام بتحديد متطلبات التطوير والتعليم في مؤسستك، تقوم شركة بي اس آي الشرق الأوسط&nbsp; بتقديم مجموعة من خيارات التعليم والتطوير من أجل تحسين أداء فرق العمل. نحن نسعى إلى إنتاج&nbsp;وتقديم أنشطة قوية وتطبيقية لمساعدة فرق العمل على التطور ومشاركة الالتزام بأهداف المؤسسة.</p>', '', '', ''),
(198, 94, 'ar', 'التطوير القيادي ', '<p class=\"left\">أصبح&nbsp;العالم اكثر تعقيداً وتغيراً وغموضاً من اي وقت قد مضى. ولكن ما لم يتغير لحد الآن هو ان القادة يحتاجون الى تطوير رؤية واضحة للمضي قدماً، و ايصال هذه الرؤية بفعالية، بالإضافة الى أن يغيروا طريقتهم لتناسب أحتياجات الآخرين، وان يحفزوهم لاحراز النتائج باسهاب.</p>\r\n<p class=\"left\">ونظراً للطلب المتزايد فقد قمنا بتطوير برنامج تطوير قيادي بالامكان تقديمه باللغتين العربية والانجليزية. هذا البرنامج مبني على نموذج ديف اولريك، نورم سمولوود، وكيت سوييتمانز المعروف<span class=\"bold\"> بنُظم القيادة : خمسة قوانين للقيادة.</span> قام اولريك وفريقه بمحاولة تبسيط وتجميع الاعمال المعروفة للخبراء من مختلف نطاقات حقل التطوير. وفي الواقع فانه تم &nbsp;العثور على عشرات الالاف من الدراسات والنظريات والاطر والنماذج والمقترحات وافضل الممارسات في القيادة. وباستخدام خبرات البحث لعشرات السنين، قام الكتاب بمقابلات كثيرة مع مجموعة من الرؤساء التنفيذيين والاكاديميين والخبراء التنفيذيين والمستشارين، وقاموا بتحديد الاساسيات الخمس نفسها مرة بعد اخرى. هذه القوانين الخمسة اصبحت نُظم القيادة.</p>\r\n<p class=\"left\">ان برنامج \"اتقان نُظم القيادة\" المرخص من قبل معهد القيادة والادارة هو برنامج يتضمن 5 مساقات، تشتمل على اختبار ما قبل وبعد البرنامج، توجيه شخصي &nbsp;وادوات قياس سايكولوجية لتعميق معرفة الذات. كما ان من الممكن تصميم البرنامج لجلب التعليم الى بيئة العمل من خلال مشاريع فردية وجماعية. لقد قمنا بتصميم مستويين: مدراء الصف الاول الى المدراء المتوسطين، والمدراء المتقدمين. من الممكن ان يتم توسيع البرنامج ليلائم وقت وميزانية مؤسستك، اضافة الى فرص التعاون للإنشاء مشاريع مشتركة من أجل تحقيق القيمة المرغوبة لمؤسستك.&nbsp;</p>', '', '', ''),
(199, 117, 'ar', 'مراكز التطوير ', '<p class=\"left\">يهدف مركز التطوير لدعم تطوير القيادة والمهارات. عادة ما تسير مراكزنا ليوم او يومين، وتتالف من مجموعة من تمارين محاكاة الاعمال (مثل التمرين التحليل، العرض التقديمي،تمثيل الادوار، النقاش الجماعي، المقابلات المنظمة، تمارين تنظيم الأولويات و استكشاف الحقائق) والاختبارات السيكولوجية للشخصية والقدرة. كما ان بامكاننا اضافة تقييمات مثل اداة ٣٦٠ او افادة اداء المدير المباشر. ان هدف المركز يقوم على تعزيز الوعي الذاتي حول مناطق القوة و مناطق التي تحتاج إلى التطوير. يتم اجراء ذلك عن طريق تمرين التأمل/التفكير الذاتي ومن ثم تقديم جلسات الإفادةحول &nbsp;الأداء. خلال جلسة الإفادة يقوم المراقب او \"المقيم\" بتزويد ادلة لما تم قوله وفعله خلال التمارين، ويقوم بتشجيع الافراد على التفكير في تأثير الخيارات التي قاموا بها وكيف كان من الممكن القيام بخيارات افضل. يتوقع من المشاركين ان يتحملوا مسؤولية المتطلبات التطويرية كجزء من التطوير المهني ويتم تشجيعهم لوضع خطط التطوير الفردية للاستفادة من الافكار المكتسبة.</p>', '', '', ''),
(200, 95, 'ar', 'مهارات الإدارة ', '<p class=\"right\">ان احد اهم التحولات التي تحدث في مساراتنا الوظيفية هي حين ننتقل من ادارة النفس الى ادارة الاخرين. في هذه المرحلة الانتقالية من المهم جداً ان نجهز المدراء بمهارات القيادة الضرورية ليكونوا فعالين في ادوارهم. نقوم بطرح البرامج التالية كدورات فردية او كمجموعة من المساقات.</p>', '', '', ''),
(201, 96, 'ar', 'مهارات إدارة الأداء ', '<p style=\"text-align: justify;\">يكمن السبب الرئيسي وراء ترك الموظفين لوظائفهم هو المدير أو المشرف السيء. ويمكن أن يرجع ذلك إلى البيئة التي يخلقها المدير وشعور الأفراد حول طريقة التعامل معهم، أو شعور الموظفين بعدم اهتمام المدير بهم أو عدم تزويدهم بالدعم وفرص النمو والتطوير. تغطي هذه الوحدة العناصر التالية:</p>\r\n<ul>\r\n<li>أهمية إدارة الأداء</li>\r\n<li>وضع أهداف سمارت SMART</li>\r\n<li>مراجعة الأداء خلال السنة</li>\r\n<li>تقديم الإفادة حول الأداء</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">وكما هو الحال بجميع برامجنا، نسعى إلى توفير فرصة تعليمية فريدة من خلال القيام بالأنشطة الجماعية، نشاط تمثيل الأدوار، النقاش الجماعي والتفكير الفردي. كما يمكننا إجراء جلسات محاكاة لمتابعة الأداء من أجل ترسيخ التعليم.</p>', '', '', ''),
(202, 109, 'ar', 'تحفيز وإشراك الآخرين', '<p>ذكرت إحدى مقالات مجلة هارفرد بزنز رفيو \"إذا كان عملك يتطلب قيادة الآخرين، فإن أهم شيء عليك فعله كل يوم هو مساعدة الفريق على التطور من خلال أداء عمل ذو معنى، ولتحقيق ذلك، عليك فهم ما الذي يحفز كل شخص للمشاركة\".</p>\r\n<p>وببساطة، يجب علينا ألا نترك فن تحفيز وإشراك الآخرين للصدفة. نحن نعمل على دعم عمل دانيل بينك والآخرين من أجل اكتشاف الاستراتيجيات بطريقة عملية وذات معنى.</p>\r\n<p>بعد ذلك ننتقل إلى المستوى التالي عن طريق استخدام مؤشر أنماط <a href=\"/assessment/personality-assessment\">MBTI</a>، حيث نبدأ باستكشاف الشخصيات المختلفة وكيفية تأثيرها على الأفراد داخل بيئة العمل&nbsp;اليومية. إن كيفية الاستفادة من المعلومات المحيطة بنا&nbsp;واتخاذ القرارات والتواصل ووضع المهام كأفراد وكفرق عمل، يلعب دوراً أساسياً فيما يتعلق بمستوى الرضا والتحفيز في العمل. من المهم للأفراد وضع الأهداف الواقعية و ذو تحدي&nbsp;لدعم التحفيز الذاتي وفي نفس الوقت تحقيق الأهداف والأغراض المؤسسية</p>', '', '', ''),
(203, 110, 'ar', 'المقابلات القائمة على الكفاءات', '<p class=\"left\">نحن نعرف الكلفة المباشرة وغير المباشرة للتوظيف الضعيف عندما نقوم بتعيين الشخص الغير مناسب، وبنفس الوقت فان الشخص قليل الخبرة سيؤثر على سمعة الشركة. يلعب المدراء المباشرون دوراً مهماً في قرارات التوظيف وفي الطريقة التي نقدم بها اعمالنا للمهارات المرتقب توظيفها. عند مقابلة المتقدمين للوظائف من الضروري تقييم كفاءاتهم السلوكية اضافة الى قابلياتهم التقنية. كما ان دوراتنا مصممة لتزود المشاركين بالمهارات وافضل الممارسات لاجراء المقابلة المستندة على الكفاءة. تتميز دوراتنا بالتفاعلية حيث نبني مهارات وكفاءة الافراد عن طريق تمارين تمثيل الادوار، المراقبة والتفكر. من الممكن تصميم الدورات لتتماشى مع اطار الكفاءة في شركتك والوصف الوظيفي ان كان ضرورياً.</p>\r\n<p class=\"left\">من الممكن تصميم ورش عمل تفاعلية وممتعة لتمكين الحضور من:</p>\r\n<ul class=\"left\">\r\n<li>فهم نموذج المقابلة المستندة الى الكفاءة وافضلية الاستفسار وسبر الكفاءات ومزايا السؤال والسبر في الكفاءات.</li>\r\n</ul>\r\n<ul class=\"left\">\r\n<li>فهم نوعية المعلومات التي من الممكن ان يتم جمعها وتقييمها في المقابلة وكيفية تقييم الدليل طبقاً للكفاءات التي يتم تقييمها في مختلف مستويات المؤسسة.</li>\r\n</ul>\r\n<ul class=\"left\">\r\n<li>تعلم كيفية مراقبة، وتسجيل، وتصنيف، وتقييم الادلة</li>\r\n</ul>\r\n<ul class=\"left\">\r\n<li>تعلم كيفية تنظيم المقابلة للنتائج الافضل. تطوير الاسئلة المستندة على الكفاءات و تقنيات طرح الاسئلة.</li>\r\n</ul>\r\n<ul>\r\n<li class=\"left\">المشاركة في جلسة إجراء المقابلات العملية (للمشاركين/لاتخاذ قرارات التعيين) والحصول اقتراحات التطوير من مدرب محترف.</li>\r\n</ul>', '', '', ''),
(204, 111, 'ar', 'دورة مهارات التدريب للمدراء ', '<p style=\"text-align: justify;\">تعد مهارات التدريب الشخصي من المهارات الجوهرية لخلق ثقافة الأداء المتميز. ومن أجل مساعدة الأفراد على فهم أدوارهم للوصول إلى أهداف الفرد والفريق والمؤسسة، يتعين تزويد الأفراد بالتدريب حول كيفية التواصل الواضح فيما يتعلق بالأهداف والتوقعات والملاحظات لتحقيق التقدم في الوقت المناسب. وطبقاً لبحث مدرسة هارفرد للأعمال \"إن الكفاءة الإدارية الوحيدة التي تميز بين المدير الفعال والمدير العادي هي التوجيه\". يشتمل برنامجنا على التالي:</p>\r\n<ul>\r\n<li>ما هو التوجيه&nbsp;وما أهميته؟</li>\r\n<li>مبادئ وأدوات التوجيه&nbsp;الأساسية.</li>\r\n<li>دور المدير في التطوير.</li>\r\n<li>تقنيات التوجيه&nbsp;والتي تشتمل على نموذج GROW للتوجيه.</li>\r\n<li>تقنيات الإصغاء وطرح الأسئلة المتقدمة.</li>\r\n<li>تخطيط العمل.</li>\r\n<li>كيفية تقديم جلسات إفادة تطويرية حول الأداء.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">هل تريد أن تصبح موجه&nbsp;تنفيذي معتمد؟ قم بالتسجيل للحصول على شهادة المدرب التنفيذي AFC مع شركة إينوفاتيف إتش آر سوليوشنز اليوم!&nbsp;</p>\r\n<p>شهادة المدرب التنفيذي AFC: <a href=\"/calendar/course/13/afc-executive-coaching-certification\">اضغط هنا للمعلومات حول الدورة</a>.</p>', '', '', ''),
(205, 112, 'ar', 'إدارة التغيير', '<p style=\"text-align: justify;\">إن التغيير هو العامل الثابت الوحيد في هذا العالم المتسارع، وتعد منطقة الشرق الأوسط من أكثر المناطق تغيراً بالعالم. ومن أساسيات مهارات الإدارة حالياً هو السماح للتغيير في مكان العمل من أجل الاستفادة من الفرص وتقليل المخاطر.</p>\r\n<p style=\"text-align: justify;\">تم تصميم برامجنا لتوفير نظرة ثاقبة حول منحنى التغيير من خلال نموذج كوتر ونماذج أخرى لفهم أساسيات التغيير.</p>\r\n<p style=\"text-align: justify;\">سوف يكتسب المشاركين بالدورة الفهم اللازم لإدارة مشاعرهم &nbsp;خلال أوقات الغموض، بجانب فهم كيفية اختلاف استجابات الأفراد للتغييرو اختلاف محفزات الضغوط واستراتيجيات الاستجابات له لكل فرد. كما نقوم خلال الدورة بالتأكيد على أهمية أنواع التواصل المختلفة، الولاء للرؤية، وبناء الاستراتيجيات لوضع الخطط قصيرة وطويلة المدى من أجل ضمان تحقيق الأهداف النهائية.</p>', '', '', ''),
(206, 113, 'ar', 'المرونة والذكاء العاطفي ', '<p>نحن نعمل الآن في&nbsp;بيئات متقلبة وغامضة ومعقدة وغير واضحة أكثر من أي وقت مضى. يخشى الأفراد التغيير ولكنهم يحتاجونه في نفس الوقت. إن التغيير يؤدي إلى الضغوط، وتؤدي الضغوط إلى تعزيز الأداء. ولكن ماذا يحدث إذا كانت هذه الضغوط أكثر من القدرة على الأداء؟ كيف سيتمكن الموظفين من المحافظة على الدافع والتركيز والإنتاجية حينما يتم الضغط عليهم أكثر من مستوى إمكانياتهم؟</p>\r\n<p>إن إحدى المؤشرات&nbsp;الخمسة الأساسية لإمكانيات الأفراد هي مستوى المرونة، والتي تعتبر صميم منهج التطوير والتوجيه الخاص بنا. إن تطوير الاستراتيجيات من أجل الاستجابة للظروف المتغيرة ومستويات الضغوط العالية يعتبر من العوامل المحورية لنجاح الموظفين والقادة.</p>\r\n<p>من خلال عملنا مع الأفراد وفرق العمل المختلفة، قمنا بإنشاء برنامج تطويري للمرونة والذي يُمكِّن الموظفين من الازدهار والنجاح عند التعرض للضغوط.</p>', '', '', ''),
(207, 114, 'ar', 'إدارة النزاع', '<p>على الرغم من أن النزاع أمر غير مرغوب، إلا أنه&nbsp;حتمي في بيئة العمل. قد تحدث الخلافات بين الزملاء نظراً لاختلاف أفكارهم ووجهات نظرهم لدرجة قد تصل إلى اضطراب أجواء العمل، وبالتالي التأثير على الإنتاجية وثقافة العمل. تهدف دورة إدارة النزاع إلى تمكين المشاركين من تعلم المنهجيات لتقليل الأضرار الناتجة عن مواقف الاختلاف باستخدام أداة توماس كيلمان (TKI).</p>\r\n<p>تُعدّ أداة توماس كيلمان (TKI) من أفضل الأدوات مبيعاً، حيث تساعد الأفراد على فهم الأساليب المختلفة لمواجهة النزاع، وتأثير كلٍ من هذه الأساليب على الديناميكيات الشخصية والجماعية. كما تساعد الأداة على اختيار أفضل الأساليب للتعامل مع كل موقف. يمكن استخدام الأداة بفعالية في المؤسسة من أجل تسهيل وتحسين ديناميكيات الفريق لتحقيق التطوير وتعزيز القيادة وبالتالي تحسين التواصل وتطوير الإنتاجية.</p>\r\n<p>دور أداة توماس كيلمان للنزاع (TKI): اضغط هنا للحصول على تفاصيل الدورة</p>', '', '', ''),
(208, 115, 'ar', 'تطوير الفرق ', '<p>قدم عالم النفس بروس تكمان النموذج الشهير \"التكوين، العصف، التعود، الأداء\" في مقالة \"التسلسل التطويري في الفرق الصغيرة\" عام ١٩٦٥. وقد استخدم هذا النموذج لوصف المسار المتبع لدى معظم الفرق في طريقها للأداء المتميز. يُعد تمكين الفرق من إظهار إمكانيتها الكاملة أمر ضروري لتحقيق الأداء المؤسسي.</p>\r\n<p>لقد قمنا بتصميم وتطوير نطاقاً من حلول تطوير فرق العمل والممارسات التي تطمح إلى رفع الوعي الجماعي بعوامل نجاح فرق العمل وتحسين جودة التواصل والتعاون. نقوم بالعمل مع مجالس الإدارة والإدارات العليا إضافة إلى وحدات العمل الاستراتيجية الذي يحاولون تحسين أدائهم، ومن هم في مرحلة التكوين والعصف، أو من يمرون بمرحلة التغيير والنزاع.</p>\r\n<p>من الضروري العمل معاً لتحقيق الأهداف المشتركة، حل الاختلافات واحترام نقاط قوة الآخرين.</p>', '', '', ''),
(209, 116, 'ar', 'التدريب والتوجيه التنفيذي ', '<h3 style=\"text-align: justify;\">التدريب التنفيذي</h3>\r\n<p style=\"text-align: justify;\">يمثل التدريب التنفيذي علاقة ديناميكية وتحفيزية وتفاعلية مع مدرب محترف ومتمرس. يساعد التدريب الأفراد أو المتدربين على تطوير أدائهم.</p>\r\n<p style=\"text-align: justify;\">إن عملية التدريب هي مكون أساسي من مكونات استراتيجية إدارة المواهب للأفراد والقادة ذوي الإمكانيات العالية، كما تساعد الأفراد على الاستمرار في النمو والتعلم والتطور. خلال جلسة التدريب نركز على تطوير الأداء العام وفي نفس الوقت تطوير السلوكيات والمهارات المطلوبة للنجاح في المناصب الحالية أو المستقبلية.</p>\r\n<p style=\"text-align: justify;\">يوفر التدريب خارطة طريق للتغيير السلوكي عن طريق طرح وجهات نظر جديدة للتحديات الشخصية والمهنية. يقوم المدرب بمساعدة الأفراد على تحديد معوقات الأداء سواء كانت مرتبطة بسلوكياتهم المختارة أو بمستويات مهاراتهم وخبراتهم ومعارفهم الحالية و كيف ينظر الأخرون لهم. كما يساعد التدريب الأفراد على زيادة الوعي باحتياجاتهم لتحقيق الكفاءة وتحمل مسؤولية الإجراءات المحددة للوصول إلى الحلول والنجاح. وبشكل عام تساهم عملية التدريب في تعزيز مهارات اتخاذ القرار وتحسين الأداء الشخصي، بالإضافة إلى زيادة الثقة وتحسين الإنتاجية والرضا وتحقيق الأهداف في الحياة العملية و الشخصية.</p>\r\n<h3 style=\"text-align: justify;\"><span class=\"bold\">التوجيه</span></h3>\r\n<p style=\"text-align: justify;\">إن التوجيه هو علاقة بين فردين مبنية على أساس الرغبة المتبادلة للتقدم نحو الأهداف والغايات الوظيفية. وبينما يتولى كلٍ من المدرب والمُوجِّه مسؤولية عملية التدريب خلال مرحلة التطوير، فإن المُوجِّه لديه دور مختلف قليلاً حيث أنه يتحمل مسؤولية نقل المحتوى والمعارف، بينما تكون عملية التدريب هي عملية تيسيرية&nbsp;بطبيعة الحال. ولذلك فان المُوجِّه عادة ما يكون أكثر خبرة وتأهيلاً من المتدرب ويكون أسلوبه أكثر توجيهاً من المدرب. إن العلاقة بين الاثنين ليست علاقة رئيس ومرؤوس ولا يحل أيٍ منهما محل الآخر في الهيكل التنظيمي.</p>\r\n<p style=\"text-align: justify;\">نستطيع العمل مع المؤسسات لتأسيس إطار عمل داخلي للتوجيه، نحدد الأشخاص الذين يمكن أن يقوموا بدور المُوجِّه، ونوفر لهم التدريب من أجل ترسيخ العملية. كما يمكننا توفير الموجِّهين للعمل مع الأفراد الرئيسين لتسريع تطويرهم.</p>', '', '', ''),
(210, 118, 'ar', 'خطط التطوير الفردية', '<p style=\"text-align: justify;\">إن خطة التطوير الفردية هي عملية منظمة تساعد الأفراد على وضع خطط فردية لتحقيق أهدافهم التطويرية. يتم تصميم خطط التطوير الفردية خصيصاً وفقاً للدور و القسم الذي يعمل &nbsp;به الفرد كما يتم وضعها لتتماشى مع رؤية ومهمة وقيم المؤسسة. تركز خطط التطوير الفردية على تطوير مهارات الفرد لضمان الكفاءة في الدور الحالي كما تساعد على تطوير المهارات المتطلبة للأدوار المستقبلية.</p>\r\n<p style=\"text-align: justify;\">إن العامل الأساسي لنجاح خطط التطوير الفردية هو قيام الفرد بتحمل مسؤولية الخطة الموضوعة بجانب الدعم من المدير المباشر لتنفيذ هذه الخطط. يمكن تصميم خطط التطوير الفردية من خلال اتباع ممارسات تشخيصية مختلفة، على سبيل المثال، يمكن وضع الخطط بعد حضور مركز للتطوير أو إجراء محادثة حول الأداء أو جلسة إفادة أداء (أداة ٣٦٠) أو بعد القيام باختبار قياس سيكولوجي.</p>\r\n<p style=\"text-align: justify;\">يعد وضع خطط التطوير الفردية استثماراً طويل الأمد وعامل أساسي في عملية إدارة المواهب من أجل تحفيز الموظفين ونشر رسالة المؤسسة التي تنطوي على أن التطوير هو أحد الأولويات الأساسية.</p>', '', '', ''),
(211, 14, 'ar', 'البرامج المعتمدة ', '<p class=\"right\">نحن مؤهلون ومرخصون لتقديم عدد من البرامج العالمية المعتمدة للمساعدة في نقل خدمات التقييم والتطوير إلى داخل مؤسستك. نفخر بتمثيلنا لـ ١٢ ناشراً للاختبارات العالمية في منطقة الخليج من أجل توفير مجموعة من البرامج والمنتجات عالمية المستوى. إن جميع المدربين العاملين لدينا معتمدين ومن ذوي الخبرات العالية في تقديم البرامج كما يخضعون جميعاً لمراجعة دورية لمستوى الجودة.</p>', '', '', ''),
(212, 97, 'ar', 'برنامج اعتماد مؤشر أنماط مايرز بريجيز MBTI', '', '', '', ''),
(213, 98, 'ar', 'اعتماد جمعية علم النفس البريطانية في الاختبار المهني (المستوى \"أ\" و \"ب\")', '', '', '', ''),
(214, 99, 'ar', 'إي كيو آي 2.0 وإي كيو ٣٦٠ (EQi 2.0 – EQ360)', '', '', '', ''),
(215, 100, 'ar', 'أداة Strong Interest Inventory ', '', '', '', ''),
(216, 101, 'ar', 'فايرو للأعمال - Firo Business', '', '', '', ''),
(217, 136, 'ar', 'دورة مهارات المُقيِّمين ', '', '', '', ''),
(218, 103, 'ar', 'دورة مدير المركز', '', '', '', ''),
(219, 104, 'ar', 'دورة التدريب التنفيذي من جمعية AFC ', '', '', '', ''),
(220, 133, 'ar', 'دورة نيو(NEO Conversion)  ', '', '', '', ''),
(221, 137, 'ar', 'دورة ساڤيل الالكترونية Saville Conversion Online', '<p style=\"text-align: justify;\">تم تصميم هذه الدورة الالكترونية للمستخدمين الذين يحملون ترخيص جمعية علم النفس البريطانية في الاختبارات النفسية للقدرة والشخصية (المعروفة سابقاً باسم المستوى أ و پ)، والذين يرغبون في بدء استخدام أدوات ساڤيل للاستشارات الخاصة باختبار الشخصية. يعتبر ترخيص الاختبار المهنية للقدرة والشخصية من جمعية علم النفس البريطانية متطلب أساسي لهذه الدورة. تمكِّن الدورة المتدربين من تحقيق الكفاءة في تفسير ومناقشة الأداء حول أدوات القياس الشخصية. كما أنها تؤهل المشاركين لفهم كيفية تطبيق أداة ساڤيل وايڤ (Saville Wave) خلال عملية التوظيف والتطوير. يقوم نموذج ساڤيل وايڤ بدمج سمات الشخصية والدوافع والبيئات المفضلة ويُعدّ من أدوات تقييم الشخصية من الجيل القادم. من أجل التأهل لهذه الدورة يجب عليك تقديم شهادة \"أ\" و \"پ\" خاصتك لفريقنا.</p>\r\n<p style=\"text-align: justify;\"><span class=\"bold\"><a href=\"/contact\">اتصل بنا</a></span> للتسجيل في هذه الدورة.</p>', '', '', ''),
(222, 106, 'ar', 'دورة مؤشر توماس كيلمان لأنماط النزاع (TKI)', '<p>ُعدّ أداة توماس كيلمان (TKI) من أفضل الأدوات مبيعاً، حيث تساعد الأفراد على فهم الأساليب المختلفة لمواجهة النزاع، وتأثير كلٍ من هذه الأساليب على الديناميكيات الشخصية والجماعية. كما تساعد الأداة على اختيار أفضل الأساليب للتعامل مع كل موقف.</p>\r\n<p>سيتعلم المشاركون كيفية استخدام الأداة لتقييم سلوكيات الفرد التلقائية في التعامل مع حالات النزاع في نطاقين: الإصرار والتعاون وكيف يمكن للأفراد استخدام خمسة أساليب فعالة عند التعامل مع النزاع.</p>\r\n<p>هناك بعض المتطلبات التي يتعين القيام بها قبل الدورة (التقييم عبر الانترنت)</p>', '', '', ''),
(223, 15, 'ar', 'الوظائف في  PSI Middle East ', '', '', '', ''),
(224, 16, 'ar', 'المسائل القانونية ', '<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">شروط الاستخدام </strong></p>\r\n<p style=\"text-align: justify;\">يهدف هذا الموقع إلى نشر مختلف المنتجات والخدمات المقدمة لكافة عملاء شركة انوڤاتيڤ إتش آر سوليوشنز.</p>\r\n<p style=\"text-align: justify;\">إن المعلومات الواردة في هذا الموقع وجميع المواقع الملحقة به هي معلومات للأغراض العامة فقط. تقدم هذه المعلومات من قبل شركة انوڤاتيڤ إتش آر سوليوشنز. وبينما تعمل الشركة على تحديث المحتوى وضمان الدقة، فإن انوڤاتيڤ إتش آر سوليوشنز لا تلتزم بأي تمثيل أو ضمانات من أي نوع، مصرحة أو متضمنة في المعنى، عن كمالية ودقة وموثوقية وملائمة أو توافر الموقع أو المعلومات والمنتجات والخدمات والصور ذات الصلة الواردة في الموقع لأي غرض من الأغراض. لذا فإن أي اعتماد على مثل هذه المعلومات سيكون على مسؤوليتك الخاصة.</p>\r\n<p style=\"text-align: justify;\">لن تكون شركة انوڤاتيڤ إتش آر سوليوشنز، في أي حال من الأحوال، مسؤولة عن أية أضرار أو خسائر بما في ذلك على سبيل المثال لا الحصر، الضرر أو الخسارة غير المباشرة أو التبعية، أو الذي ينشأ عن فقدان البيانات أو الأرباح الناتجة عن أو ذات الصلة باستخدام هذا الموقع.</p>\r\n<p style=\"text-align: justify;\">من خلال هذا الموقع يمكنك الربط مع مواقع أخرى والتي ليست تحت سيطرة شركة انوڤاتيڤ إتش آر سوليوشنز. لا تملك شركة انوڤاتيڤ إتش آر سوليوشنز السيطرة على طبيعة ومحتوى وتوافر هذه المواقع. إن تضمين أي رابط لا يعني بالضرورة توصية ا\\أو تأييد لوجهات النظر المتضمنة.</p>\r\n<p style=\"text-align: justify;\">نقوم ببذل كافة الجهود لضمان عمل الموقع بسلاسة وسهولة. ولكن لا تتحمل شركة انوڤاتيڤ إتش آر سوليوشنز أي مسؤولية عن عدم توافر الموقع بصورة وقتية بسبب المشاكل الفنية الخارجة عن سيطرة انوڤاتيڤ إتش آر سوليوشنز.</p>\r\n<p style=\"text-align: justify;\">تلتزم شركة انوڤاتيڤ إتش آر سوليوشنز باحترام الخصوصية وحماية المعلومات الشخصية. نحن نقر بالتزامنا بإبقاء المعلومات ذات الخصوصية آمنة، وقد وضعنا هذه الرسالة من أجل مشاركة وشرح نظام ممارسات المعلومات الحالية في موقعنا. يخضع التعامل مع جميع المعلومات من قبل شركة انوڤاتيڤ إتش آر سوليوشنز إلى قوانين الخصوصية والأمن في الإمارات العربية المتحدة ودبي.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">سياسة الخصوصية </strong></p>\r\n<p style=\"text-align: justify;\">نلتزم بحماية خصوصية العميل سواء أثناء التصفح للحصول على المعلومات أو خلال ممارسة الأعمال عبر الانترنت.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">جمع واستخدام المعلومات عبر الانترنت</strong></p>\r\n<p style=\"text-align: justify;\">عند القيام بزيارة موقعنا، لن نقوم بجمع المعلومات الشخصية الخاصة بك، الا إذا قررت استخدام واستلام المنتجات والخدمات التي تتطلب ذلك عبر الانترنت. تستخدم جميع عمليات جمع البيانات أحدث الآليات الأمنية لضمان نزاهة وسرية المعلومات والأنظمة.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">جمع معلومات عناوين البروتوكولات IP</strong></p>\r\n<p style=\"text-align: justify;\">من المحتمل أن يقوم خادم (خوادم) الويب الخاص بنا تلقائياً بجمع عنوان بروتوكول الانترنت (IP) الخاص بك عند زيارة موقعنا (ان عنوان بروتوكول الانترنت IP address الخاص بك هو العنوان الفريد لحاسوبك والذي يسمح للحواسيب الأخرى المتصلة بالشبكة أن تعرف عنوان إرسال البيانات، ولكن لا يتعرف عليك شخصياً). نحن نستخدم عنوان بروتوكول الانترنت للمساعدة في تشخيص مشاكل الخادم الخاص بنا وتجميع الاحصائيات عن استخدام الموقع.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">استخدام ملفات تعريف الارتباط (كوكيز) </strong></p>\r\n<p style=\"text-align: justify;\">من المحتمل أن يقوم موقع انوڤاتيڤ إتش آر سلوشنز باستخدام ملفات تعريف الارتباط. تستخدم ملفات تعريف الارتباط في العادة من أجل تسهيل حركة المستخدمين في المواقع. إذا اخترت ذلك، يتم استخدام ملفات تعريف الارتباط من أجل \"تذكر\" كلمة السر الخاصة بك لتسهيل وتسريع تسجيل الدخول إلى بعض المواقع. يجب تخزين هذه الانواع من ملفات تعريف الارتباط على القرص الصلب في حاسوبك.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">حماية المعلومات الشخصية </strong></p>\r\n<p style=\"text-align: justify;\">تتوفر المعلومات الشخصية التي تدخلها على موقعنا فقط للموظفين المخولين في شركة انوڤاتيڤ إتش آر سوليوشنز والذين يحتاجون إلى معرفة هذه المعلومات. لن تتوفر هذه المعلومات للآخرين بدون إذنك المسبق، كما لن يتم مشاركة معلومات المستخدمين في الموقع ولن تباع أو تحول إلى أطراف خارجية بدون الحصول على الموافقة المسبقة.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">ملاحظات هامة </strong></p>\r\n<p style=\"text-align: justify;\">ترتبط بعض المواقع الخاصة بنا بمواقع أخرى يتم إنشائها وإدارتها من قبل مؤسسات عامة أو خاصة. نقوم بتزويد تلك الروابط فقط لأغراض الاطلاع على المعلومات والتسهيل. عندما تقوم بالارتباط بموقع خارجي، فإنك تترك موقع انوڤاتيڤ إتش آر سوليوشنز ولا تطبق معلومات سياسات إدارة المعلومات الخاصة بنا بعد ذلك.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">قد يزود موقع انوڤاتيڤ إتش آر سلوشنز (HIS) بين الحين والآخر بعض الروابط لمواقع خارجية. تحكم هذه الاتفاقية عمل هذا الموقع فقط ولا تنطبق على أية مواقع خارجية أخرى. لا يجب اعتبار توفيرنا لموقع آخر على أنه تأييد لمحتوى أو خدمات ذلك الموقع. لا تقوم شركة انوڤايتڤ إتش آر سلوشنز بتشغيل أو التحكم بأي موقع خارجي أو أي من محتوياته. لا تمثل شركة انوڤاتيڤ إتش آر سلوشنز أو تضمن توفر أو صحة المحتويات الواردة في المواقع الأخرى ولا تلتزم بأي مسؤولية عن المحتوى أو صحة المعلومات المقدمة في أي موقع خارجي.</strong></p>\r\n<p style=\"text-align: justify;\">توافق على إعفاء شركة انوڤاتيڤ إتش آر سوليوشنز من أي وكل مسؤولية أو خسارة أو ضرر و/ أو نفقات ناتجة عن ارتباطك بأي موقع خارجي. كما أنك توافق أيضاً على أن شركة انوڤاتيڤ إتش آر سوليوشنز لن تكون مسؤولة بأي شكل من الأشكال عن أي من المنتجات أو الخدمات الموجودة بالمواقع الخارجية أو أي تعاملات مالية تمت في أو من خلال الموقع الخارجي. إذا قررت الدخول إلى روابط المواقع الخارجية، فإنك تقوم بذلك على مسؤوليتك الخاصة.</p>\r\n<p style=\"text-align: justify;\">توافق على أن شركة انوڤاتيڤ إتش آر سوليوشنز غير مسؤولة عن المعلومات التي يتم جمعها أو الفيروسات أو البرامج الخبيثة أو الاخفاقات التشغيلية التي قد تنتج عن استخدام المواقع الخارجية. يجب التواصل مباشرة مع المسؤولين بالمواقع الخارجية بخصوص أي مشاكل ناشئة. كما تُقر صراحة بأن سياسات الخصوصية والأمن بالمواقع الخارجية قد تختلف عن سياسات شركة انوڤاتيڤ إتش آر سلوشنز. يجب مراجعة شروط الاستخدام وسياسات الخصوصية والقوانين الأخرى المنشورة على المواقع الخارجية قبل استخدامها.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">الروابط الخارجية من موقع انوڤاتيڤ إتش آر سوليوشنز </strong></p>\r\n<p style=\"text-align: justify;\">تُعرَّف \"الروابط\" من موقع انوڤاتيڤ إتش آر سوليوشنز كالتالي:</p>\r\n<p style=\"text-align: justify;\">- روابط الكترونية يتم تفعيلها من خلال النقر على صورة أو نص، وتربط المستخدم بصفحة انترنت أخرى لا تقع ضمن موقع انوڤاتيڤ إتش آر سوليوشنز \"صراحة\"</p>\r\n<p style=\"text-align: justify;\">- الإشارة إلى عنوان لأحد الروابط يزود المستخدمين بعنوان صفحة على الانترنت لا توجد على موقع انوڤاتيڤ إتش آر سوليوشنز \"ضمناً\"</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">روابط خارجية لموقع انوڤاتيڤ إتش آر سوليوشنز </strong></p>\r\n<p style=\"text-align: justify;\">لا يجب على المواقع التي ترتبط بموقع شركة انوڤاتيڤ إتش آر سوليوشنز أن تسيء تمثيل علاقتها مع الشركة، مما يعني التالي:</p>\r\n<p style=\"text-align: justify;\">- لا ينبغي إنشاء متصفح أو حدود بيئية محيطة حول محتويات انوڤاتيڤ إتش آر سوليوشنز</p>\r\n<p style=\"text-align: justify;\">- لا يجب استخدام شعار شركة انوڤاتيڤ إتش آر سوليوشنز بدون الحصول على إذن مسبق من الشركة</p>\r\n<p style=\"text-align: justify;\">- لا يجب أن يقدم الموقع معلومات خاطئة أو غير دقيقة عن خدمات شركة انوڤاتيڤ إتش آر سوليوشنز</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">القوانين الحاكمة والسلطة القانونية </strong></p>\r\n<p style=\"text-align: justify;\">تخضع جميع الأمور المتعلقة باستخدام هذا الموقع وتفسر وفقاً للقوانين والأنظمة المعمول بها والمُطبَّقة في إمارة دبي والقوانين الاتحادية في الإمارات العربية المتحدة.</p>\r\n<p style=\"text-align: justify;\">محاكم دبي هي المختصة حصرياً بالنظر في أي دعاوى تصدر عن استخدام هذا الموقع و/ أو أي أمور أخرى متعلقة بزيارة الموقع. نحتفظ بالحق القضائي في حال انتهاك استخدام الموقع في بلد اقامتك أو أي دولة أخرى ذات صلة.</p>', '', '', ''),
(225, 39, 'ar', 'أداة ساڤيل الذاتية - Saville My self', '<p>تستخدم هذه الأداة للتخطيط الوظيفي لطلبة المدارس الثانوية والكليات والجامعات &ndash; بالإضافة إلى المواهب الشابة والأفراد الراغبين في تغيير وظائفهم.</p>\r\n<h3>الفئة المستهدفة</h3>\r\n<p>الخريجون الجدد والمواهب الشابة</p>', '', '', ''),
(226, 120, 'ar', 'الصفحة غير متوفرة', '<p>لم يتم العثور على الصفحة التي تبحث عنها</p>', '', '', ''),
(227, 141, 'ar', 'حدث خطأ ما', '<p>Whoops! Looks like something went wrong. Please try again later or contact us on +97143902778.</p>', '', '', ''),
(228, 10, 'ar', 'مركز المعرفة', '<p>مركز المعرفة</p>', '', '', ''),
(229, 142, 'ar', 'نتيجة البحث', '', '', '', ''),
(230, 142, 'en', 'Search Result', '', '', '', ''),
(231, 85, 'ar', 'حجز الدورة', '', '', '', ''),
(232, 138, 'ar', 'دراسة الحالة', '', '', '', ''),
(233, 139, 'ar', 'حجز الفعاليات', '', '', '', ''),
(234, 140, 'ar', 'شكراً لك ', '<p><span class=\"bold\">شكراً لك على الحجز!</span></p>\r\n<p><span class=\"bold\">سيقوم أحد أعضاء فريقنا بالتواصل معك في أقرب وقت!</span></p>\r\n<p><span class=\"bold\">إذا كان لديك أي أسئلة بخصوص حجز برنامج الاعتماد الخاص بك يرجى الاتصال بنا على الهاتف رقم:<strong>+97143902778 </strong><strong>&nbsp;</strong>أو البريد الإلكتروني التالي: <a href=\"mailto:info@ihsdubai.com\"><strong>course@innovative-hr.com</strong></a></span></p>\r\n<p><span class=\"bold\">إذا كان لديك أي أسئلة بخصوص حجز الفعاليات يرجى الاتصال بنا على الهاتف رقم:<strong>+97143902778 </strong>&nbsp;أو البريد الإلكتروني التالي: <a href=\"mailto:info@ihsdubai.com\"><strong>info@innovative-hr.com</strong></a></span></p>\r\n<p><span class=\"bold\">يرجى متابعة صفحتنا على الفايسبوك وتويتر ومتابعتنا على حساب LinkedIn من أجل الحصول على كافة التحديثات والأخبار المتعلقة بالمواهب!</span></p>', '', '', ''),
(235, 86, 'ar', 'شكراً لك ', '<p><span class=\"bold\">شكراً لك على الحجز!</span></p>\r\n<p><span class=\"bold\">سيقوم أحد أعضاء فريقنا بالتواصل معك في أقرب وقت!</span></p>\r\n<p><span class=\"bold\">إذا كان لديك أي أسئلة بخصوص حجز برنامج الاعتماد الخاص بك يرجى الاتصال بنا على الهاتف رقم:<strong>+97143902778 </strong><strong>&nbsp;</strong>أو البريد الإلكتروني التالي: <a href=\"mailto:info@ihsdubai.com\"><strong>course@innovative-hr.com</strong></a></span></p>\r\n<p><span class=\"bold\">إذا كان لديك أي أسئلة بخصوص حجز الفعاليات يرجى الاتصال بنا على الهاتف رقم:<strong>+97143902778 </strong>&nbsp;أو البريد الإلكتروني التالي: <a href=\"mailto:info@ihsdubai.com\"><strong>info@innovative-hr.com</strong></a></span></p>\r\n<p><span class=\"bold\">يرجى متابعة صفحتنا على الفايسبوك وتويتر ومتابعتنا على حساب LinkedIn من أجل الحصول على كافة التحديثات والأخبار المتعلقة بالمواهب!</span></p>', '', '', ''),
(236, 44, 'ar', 'التقويم', '', '', '', ''),
(237, 3, 'ar', 'اتصل بنا', '', '', '', ''),
(238, 28, 'ar', 'اختبارات القدرات', '', '', '', ''),
(239, 143, 'en', 'Home Page 2', '<div class=\"sec-title centered\">\r\n<h2>What we do</h2>\r\n<p style=\"font-size: 17px; line-height: 22px;\">We partner with organisations to identify, develop and energise talent, creating change through<br />business psychology whilst being focused on commercial success.</p>\r\n</div>\r\n<div class=\"services-outer center-block fadeInUp col-md-8 col-sm-12 col-xs-12\" data-wow-delay=\"0ms\" data-wow-duration=\"1500ms\">\r\n<div class=\"clearfix\"><!--Service Block-->\r\n<div class=\"default-service-block col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"inner-box\">\r\n<div class=\"icon-box\"><img src=\"/images/icons/wwd-icon-1.png\" /></div>\r\n<h3><a href=\"/assessment\">Selection</a></h3>\r\n<div class=\"text\">Why leave your selection decisions to chance?</div>\r\n</div>\r\n</div>\r\n<!--Service Block-->\r\n<div class=\"default-service-block col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"inner-box\">\r\n<div class=\"icon-box\"><img src=\"/images/icons/wwd-icon-3.png\" /></div>\r\n<h3><a href=\"/development\">Development</a></h3>\r\n<div class=\"text\">How can I take my people to the next level?</div>\r\n</div>\r\n</div>\r\n<!--Service Block-->\r\n<div class=\"default-service-block col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"inner-box\">\r\n<div class=\"icon-box\"><img src=\"/images/icons/wwd-icon-2.png\" /></div>\r\n<h3><a href=\"/accreditation-programme\">Accreditation</a></h3>\r\n<div class=\"text\">Let&rsquo;s bring it in-house!</div>\r\n</div>\r\n</div>\r\n<!--Service Block-->\r\n<div class=\"default-service-block col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"inner-box\">\r\n<div class=\"icon-box\"><img src=\"/images/icons/wwd-icon-3.png\" /></div>\r\n<h3><a href=\"/consulting\">Consulting</a></h3>\r\n<div class=\"text\">Where do I start?</div>\r\n</div>\r\n</div>\r\n<!--Service Block--></div>\r\n</div>', ' ', '', 'HR solutions from qualified occupational psychologists & learning & development specialists across the Middle East. Contact us today & see how we can improve your organisation. ');
INSERT INTO `page_translations` (`id`, `page_id`, `locale`, `title`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(240, 144, 'en', 'Your Career Guidance Hero', '<h3>Enable your students success!</h3>\r\n<p style=\"text-align: justify;\">For nearly 80 years, the Strong Interest Inventory&reg; assessment has provided time-tested, research-validated insights to help individuals in their search for a rich, fulfilling career.</p>\r\n<p style=\"text-align: justify;\">As one of the most respected and widely used career planning instruments in the world, it has been used extensively in schools (14yrs+), Colleges, Universities and places of work of all sizes.</p>\r\n<p style=\"text-align: justify;\">The Strong Interest Inventory is designed to help students to find a career that fits. How? By understanding their personality preferences and comparing their interests to the interests of people who happy and fulfilled in their jobs. The reports are fully interactive, enabling students to click and search over 1,000 positions* in the world today which are constantly being updated; from Web Designer to Dental Surgeon.</p>\r\n<p style=\"text-align: justify;\">*Special Unicorn is coming soon</p>\r\n<h3>Benefits of the Strong Interest Inventory</h3>\r\n<ol class=\"listing\">\r\n<li>Empowers students to discover their true interests so they can expand and explore various career options.</li>\r\n<li>Delivers user-friendly, fun, interactive reports allowing test takers to explore over 1,000 career options in more detail.</li>\r\n<li>Helps heighten self-awareness and a deeper understanding of individual strengths and blind spots.</li>\r\n</ol>', '', '', ''),
(241, 145, 'en', 'Safety Quotient', '<p>Safety Quotient&trade; is a new and innovative employee risk&nbsp;assessment technology that measures 6&nbsp;unique personality traits directly connected to risk tolerance and unsafe behaviors. Most companies using Safety Quotient&trade; have reduced workplace incidents by more than 25%.</p>\r\n<p>Years of research into workplace safety have uncovered 6 personality traits that are directly correlated to employee safety:</p>\r\n<ul>\r\n<li>Resistance</li>\r\n<li>Irritability</li>\r\n<li>Distractibility</li>\r\n<li>Impulsiveness</li>\r\n<li>Anxiousness</li>\r\n<li>Thrill-Seeking</li>\r\n</ul>\r\n<p>Through a 10-15 minute online assessment,&nbsp;<strong>Safety Quotient&trade;</strong>&nbsp;measures participants&rsquo; personality risk factors to predict which of your front-line employees are high-risk for these traits.</p>\r\n<p>The employee risk assessment outputs a detailed two part report outlining strategies to improve the participants&rsquo;&nbsp;<em>Safety Self-Awareness</em>&nbsp;and allows managers to predict preventable incidents through personalized training and coaching.</p>\r\n<h2 class=\"p1\"><span class=\"s1\">TARGET CATEGORY</span></h2>\r\n<p class=\"p1\"><span class=\"s1\">All levels</span></p>\r\n<h2 class=\"p1\"><span class=\"s1\">DOWNLOADS</span></h2>\r\n<p><a href=\"/downloadFile?path=uploads/documents/67a82bf709e7ef6272d372842b697e1e8df351d5.pdf&amp;name=SQ-Sample-Participant-SQ-2017.pdf\">Safety Quotient Participant</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/cc873d828b3a05538aa5cd503caf543291e4f246.pdf&amp;name=SQ-Sample-Employer-SQ-2017.pdf\">Safety Quotient&nbsp; Employer</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/0c903ecded10c8a1c28d5baf7ac5f464f88f5cdd.pdf&amp;name=Group Report Sample.pdf\">&nbsp;Safety Quotient Group Report</a></p>\r\n<p><a href=\"/downloadFile?path=uploads/documents/ef7b2f6c7d0dbf642ac519c87fbf3c852ba7016a.pdf&amp;name=1 Safety Culture Perception Survey - What is Measured.pdf\">&nbsp;Safety Culture Perception Survey</a></p>\r\n<p>&nbsp;</p>', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `slug`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'login-to-cms', 'Login to CMS', 'Can loging to CMS', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `name`, `slug`, `published_at`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 2, 'Innovation and Psychology', 'innovation-and-psychology', '2016-11-15 05:00:00', 1, 2, 2, '2016-08-21 10:08:52', '2018-05-23 03:47:28'),
(16, 4, 'IHS Brings MBTI to Lean In Pakistan 2017', 'ihs-brings-mbti-to-lean-in-pakistan-2017', '2017-01-22 06:00:00', 1, 3, 2, '2017-01-22 17:54:58', '2018-03-08 14:41:25'),
(17, 5, 'IHS raises awareness about the positive impact of coaching for businesses and professionals', 'ihs-raises-awareness-about-the-positive-impact-of-coaching-for-businesses-and-professionals', '2017-05-28 05:00:00', 1, 5, 4, '2017-05-28 05:27:22', '2017-05-28 05:49:37'),
(18, 4, 'SME Awards Press Release', 'sme-awards-press-release', '2017-07-09 20:00:00', 1, 4, 4, '2017-07-10 04:34:14', '2017-07-10 06:42:36'),
(19, 2, 'Launching PSI Middle East! ', 'launching-psi-middle-east', '2018-11-27 06:00:00', 1, 2, 2, '2018-04-03 16:08:07', '2018-11-28 05:43:31'),
(20, 4, 'Why Assessment is Crucial', 'webinar-why-assessment-is-crucial', '2018-04-09 05:00:00', 1, 4, 4, '2018-04-09 11:22:09', '2018-04-19 15:56:59'),
(21, 4, 'Cultural Intelligence- The Art of Understanding Emotions', 'cultural-intelligence-the-art-of-understanding-emotions', '2018-05-19 20:00:00', 1, 4, 4, '2018-05-20 08:17:52', '2018-10-30 02:56:21'),
(22, 4, 'Talent Analytics', 'talent-analytics', '2018-06-20 20:00:00', 1, 4, 4, '2018-06-21 05:45:42', '2018-10-30 02:57:41'),
(23, 4, 'Achieving Peak Performance in Challenging Times ', 'achieving-peak-performance-in-challenging-times', '2018-07-18 20:00:00', 1, 4, 4, '2018-07-19 07:07:34', '2018-10-30 02:58:54'),
(24, 4, 'How to Engage High Potential', 'how-to-engage-high-potential', '2018-09-11 20:00:00', 1, 4, 4, '2018-09-26 03:45:00', '2018-10-30 02:58:31'),
(25, 4, 'Snakes in Suits - Managing the Dark Triad in the Workplace', 'snakes-in-suits-managing-the-dark-triad-in-the-workplace', '2018-09-18 20:00:00', 1, 4, 4, '2018-09-26 03:58:23', '2018-11-01 00:53:22'),
(26, 4, 'GRIT...THE MISSING LINK', 'gritthe-missing-link', '2018-09-25 20:00:00', 1, 4, 4, '2018-09-26 06:41:35', '2018-09-26 06:45:54');

-- --------------------------------------------------------

--
-- Table structure for table `post_translations`
--

CREATE TABLE `post_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_text` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_translations`
--

INSERT INTO `post_translations` (`id`, `post_id`, `locale`, `title`, `banner_text`, `excerpt`, `content`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(4, 4, 'en', 'The Psychology of Innovation ', NULL, '', '<p style=\"text-align: justify;\"><strong>Sharan Gohel</strong></p>\r\n<p style=\"text-align: justify;\">In a recent survey conducted by Mckinsey more than 70 percent of the senior executives said that innovation will be one of the top three drivers of growth for their company in the next three to five years. Other executives reported innovation as the most important way for companies to accelerate the pace of change in today&rsquo;s global business environment.</p>\r\n<p style=\"text-align: justify;\">Innovation and Creativity are two words that we all use interchangeably and generally agree are positive. In most cases we want more, yet there seems to be less alignment when we try and define it and no real shared interpretation of innovation/creativity. Similarly Leadership is also a term which does not seem to have a commonly accepted definition or shared understanding. Being creative or innovative and Leadership are closely related as leadership always seems to have some focus on bringing about a change resulting in a better future.</p>\r\n<p style=\"text-align: justify;\">More often than not when you read books and articles on leadership, you will hear leadership defined in a number of different ways such as:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>the ability to create a vision</li>\r\n<li>anticipate trends</li>\r\n<li>being congruent in the way leaders live their values (value-congruence)</li>\r\n<li>being empowering</li>\r\n<li>understanding their own impact on others (self-understanding)</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Within the competencies mentioned above there is an assumption that the individual has the capacity to be more creative and consider more risks. However in order to be competent they will need to utilize and increase their capacity to be creative and take risks &ndash; having the capacity to develop new ideas and take risks to make the ideas a reality in the face of adversity. Therefore before leadership it is important to consider these two factors and the role they play. In this short article we will begin to explore the relationship between leadership and innovation or creativity and the impact risk taking can have on their success through each leadership competency.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Vision:&nbsp;</strong>It is widely accepted that effective leaders are able to create a vision for the organization&rsquo;s future; a picture that inspires people; a statement to action giving a shared purpose for all. Through such a vision, people in the organization are able to see the relationship they have to the larger realities/goals of the enterprise. But without creativity, how can you create a vision? And how can you make a vision happen if you&rsquo;re not willing to take risks to make it happen? A vision without creativity is dull and insignificant. A vision without risk taking never happens.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Value-Congruence:</strong> Leaders who are congruent in the way they live out their values are those that inspire people to follow them and essentially &lsquo;walk the talk&rsquo;. A leader who uses his or her values to guide and motivate others is able to provide meaning to people&rsquo;s lives within organizations. In extreme cases they become the standards by which choices are made. However it is easy to walk and talk your values when you&rsquo;re not faced with a challenge or when you &ldquo;fit in&rdquo;. But if your values run against the norm, then that&rsquo;s when creativity and risk taking are required. Leaders must take the risk to &ldquo;walk the talk&rdquo;. Or, they run the risk of losing their capacity to lead.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Anticipatory Skills:</strong> Foresight is fundamental to leadership. An effective leader intuitively and systematically scans the environment for potential areas of opportunities and risks. The leader&rsquo;s focus is on servicing customers (internal and external) in new ways, finding new advantages over competitors, and exploiting new company strengths. Being anticipating requires the leader to look outside your immediate view, being on alert for new upcoming ideas -looking for creative new trends that may impact you and your organization. At the same time a Leader must be willing to take the risk of thinking outside the box and their own paradigms - challenging their own mental models. It is the anticipation which drives an organization forward. Looking for opportunities to use creative abilities and taking the risk of thinking outside the realms of conventional methods -of what has been to is what will help create the new trends that are the keys to success.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Empowerment:</strong> This is not about blind trust, it is about giving those who you are leading responsibility for doing jobs and performing in ways that demonstrates your confidence in their abilities. The concept of empowerment embodies in it a level of trust in your employees, trusting that they can be creative with new ideas that come from them. This means that as a leader to be empowering you need to take the risk to let them determine and drive the ideas forward without you micromanaging them.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Self-Awareness:</strong> For a leader, understanding yourself is critical. Without it, leaders may do more harm than good. To be an effective Leader you must have introspective skills as well as frameworks with which to understand the impact on others. Leaders seek to understand themselves and what a risk that can pose, through understanding and making choices about yourself given that self-understanding. Knowing yourself and being able to share with others &ldquo;who you are and what your strengths and weaknesses are&rdquo; puts a leader in an extremely vulnerable, difficult but necessary position, probably one of the ultimate risks a leader faces.</p>\r\n<p style=\"text-align: justify;\">So I would like you to challenge you as leaders to think about whether we may have missed a key component in effective leadership? If we agree that leadership is about the capacity to identify new opportunities and drive forward change within our organizations; then without fostering leadership competencies around innovation/creativity and risk taking, how can we as leaders take advantage of the opportunities we have in a global marketplace?</p>', '', '', ''),
(21, 4, 'ar', 'The Psychology of Innovation ', NULL, '', '<p style=\"text-align: justify;\"><strong>Sharan Gohel</strong></p>\r\n<p style=\"text-align: justify;\">In a recent survey conducted by Mckinsey more than 70 percent of the senior executives said that innovation will be one of the top three drivers of growth for their company in the next three to five years. Other executives reported innovation as the most important way for companies to accelerate the pace of change in today&rsquo;s global business environment.</p>\r\n<p style=\"text-align: justify;\">Innovation and Creativity are two words that we all use interchangeably and generally agree are positive. In most cases we want more, yet there seems to be less alignment when we try and define it and no real shared interpretation of innovation/creativity. Similarly Leadership is also a term which does not seem to have a commonly accepted definition or shared understanding. Being creative or innovative and Leadership are closely related as leadership always seems to have some focus on bringing about a change resulting in a better future.</p>\r\n<p style=\"text-align: justify;\">More often than not when you read books and articles on leadership, you will hear leadership defined in a number of different ways such as:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>the ability to create a vision</li>\r\n<li>anticipate trends</li>\r\n<li>being congruent in the way leaders live their values (value-congruence)</li>\r\n<li>being empowering</li>\r\n<li>understanding their own impact on others (self-understanding)</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Within the competencies mentioned above there is an assumption that the individual has the capacity to be more creative and consider more risks. However in order to be competent they will need to utilize and increase their capacity to be creative and take risks &ndash; having the capacity to develop new ideas and take risks to make the ideas a reality in the face of adversity. Therefore before leadership it is important to consider these two factors and the role they play. In this short article we will begin to explore the relationship between leadership and innovation or creativity and the impact risk taking can have on their success through each leadership competency.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Vision:&nbsp;</strong>It is widely accepted that effective leaders are able to create a vision for the organization&rsquo;s future; a picture that inspires people; a statement to action giving a shared purpose for all. Through such a vision, people in the organization are able to see the relationship they have to the larger realities/goals of the enterprise. But without creativity, how can you create a vision? And how can you make a vision happen if you&rsquo;re not willing to take risks to make it happen? A vision without creativity is dull and insignificant. A vision without risk taking never happens.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Value-Congruence:</strong><span class=\"Apple-converted-space\">&nbsp;</span>Leaders who are congruent in the way they live out their values are those that inspire people to follow them and essentially &lsquo;walk the talk&rsquo;. A leader who uses his or her values to guide and motivate others is able to provide meaning to people&rsquo;s lives within organizations. In extreme cases they become the standards by which choices are made. However it is easy to walk and talk your values when you&rsquo;re not faced with a challenge or when you &ldquo;fit in&rdquo;. But if your values run against the norm, then that&rsquo;s when creativity and risk taking are required. Leaders must take the risk to &ldquo;walk the talk&rdquo;. Or, they run the risk of losing their capacity to lead.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Anticipatory Skills:</strong><span class=\"Apple-converted-space\">&nbsp;</span>Foresight is fundamental to leadership. An effective leader intuitively and systematically scans the environment for potential areas of opportunities and risks. The leader&rsquo;s focus is on servicing customers (internal and external) in new ways, finding new advantages over competitors, and exploiting new company strengths. Being anticipating requires the leader to look outside your immediate view, being on alert for new upcoming ideas -looking for creative new trends that may impact you and your organization. At the same time a Leader must be willing to take the risk of thinking outside the box and their own paradigms - challenging their own mental models. It is the anticipation which drives an organization forward. Looking for opportunities to use creative abilities and taking the risk of thinking outside the realms of conventional methods -of what has been to is what will help create the new trends that are the keys to success.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Empowerment:</strong><span class=\"Apple-converted-space\">&nbsp;</span>This is not about blind trust, it is about giving those who you are leading responsibility for doing jobs and performing in ways that demonstrates your confidence in their abilities. The concept of empowerment embodies in it a level of trust in your employees, trusting that they can be creative with new ideas that come from them. This means that as a leader to be empowering you need to take the risk to let them determine and drive the ideas forward without you micromanaging them.</p>\r\n<p style=\"text-align: justify;\"><strong data-redactor-tag=\"strong\">Self-Awareness:</strong><span class=\"Apple-converted-space\">&nbsp;</span>For a leader, understanding yourself is critical. Without it, leaders may do more harm than good. To be an effective Leader you must have introspective skills as well as frameworks with which to understand the impact on others. Leaders seek to understand themselves and what a risk that can pose, through understanding and making choices about yourself given that self-understanding. Knowing yourself and being able to share with others &ldquo;who you are and what your strengths and weaknesses are&rdquo; puts a leader in an extremely vulnerable, difficult but necessary position, probably one of the ultimate risks a leader faces.</p>\r\n<p style=\"text-align: justify;\">So I would like you to challenge you as leaders to think about whether we may have missed a key component in effective leadership? If we agree that leadership is about the capacity to identify new opportunities and drive forward change within our organizations; then without fostering leadership competencies around innovation/creativity and risk taking, how can we as leaders take advantage of the opportunities we have in a global marketplace?</p>', '', '', ''),
(31, 16, 'en', 'Innovative HR Solutions Brings MBTI to Lean In Pakistan 2017', NULL, '', '<p><img class=\"center\" title=\"Innovative HR Solutions Brings MBTI to Lean In Pakistan 2017\" src=\"/downloadFile?path=uploads/images/59d4ba33e9de1b013115ea404cde6b90572f5bc2.png&amp;name=2017-01-22_1709ff.png\" /></p>\r\n<p>Dawood Global Foundation (DGF) and the British Deputy High Commission, Karachi partnered to host <em>Lean In Pakistan</em> Circle Luncheon 2017 powered by LADIESFUND for women professionals and entrepreneurs at the British Deputy High Commission, Karachi.</p>\r\n<p>The Consultancy Team of Innovative HR Solutions was asked to fly to Karachi and to conduct an engaging and interactive Myers-Briggs MBTI session on &ldquo;How Understanding Your Personality Can Lead To Career Success.&rdquo;&nbsp;Leading these sessions were Senior Organisational Psychologists Sharan Gohel, Humaira Anwer, Liesl Connolly as well as Managing Director, Amanda White and Consultant Susie Kelt.</p>\r\n<p>&nbsp;&ldquo;Lean In&rdquo; which is the highly praised global movement by <span class=\"bold\">Sheryl Sandberg COO of Facebook</span>, was introduced to Pakistan and endorsed by Sheryl last year and has since become the biggest Lean In Circle meeting in the MENA region and South Asia. More than 100 women professionals and entrepreneurs from leading multi-nationals, retail, medical, accountancy, legal sectors, NGOs and youth programmes gathered to form professional friendships, meet potential clients and build/strengthen their network for career advancement purposes.</p>\r\n<p>President of Dawood Global Foundation, Tara Uzra Dawood said, &ldquo;<span class=\"italic\">On behalf of Dawood Global Foundation, thank you for &ldquo;leaning in together&rdquo; with us. Our theme is strengthening women\'s inclusion in the work force and encouraging women to pursue and achieve their ambitions</span>.\"</p>\r\n<p>The funds generated from this event have contributed towards the charitable efforts of DGF for women advancement. To date, DGF has educated 1000 deserving girls in Karachi and 250 in Lagos, Nigeria, to become journalists through its award-winning Educate a Girl project.</p>\r\n<p>&nbsp;</p>\r\n<p><span class=\"bold\">About LeanIn.Org<strong>&reg;</strong></span></p>\r\n<p>LeanIn.Org is a nonprofit organization founded by Facebook COO Sheryl Sandberg to empower all women in their ambitions. Leanin.Org offers inspiration and support through an online community, free expert lectures and Lean in Circles, small peer groups whose members meet regularly to share and learn together</p>\r\n<p>&nbsp;</p>\r\n<p><strong>About LADIESFUND&reg;</strong></p>\r\n<p>LADIESFUND is Pakistan&rsquo;s leading platform for urban women professionals and SME entrepreneurs with over 12,000 in its network. DGF and BDHC previously held the LADIESFUND Women of Influence Luncheon January 2011, LADIESFUND Speed Networking Luncheon January 2014, LADIESFUND Star Speed Networking Luncheon January 2015 and LADIESFUND Lean In Networking Luncheon January 2016</p>', '', '', ''),
(32, 16, 'ar', 'IHS Brings MBTI to Lean In Pakistan 2017', NULL, '', '<p>IHS Brings MBTI to Lean In Pakistan 2017</p>', '', '', ''),
(33, 17, 'en', 'IHS raises awareness about the positive impact of coaching for businesses and professionals', NULL, '', '<p style=\"text-align: justify;\">Senior Organisational Psychologist and Executive Coach, Liesl Connolly of Innovative HR Solutions was invited as a Guest Speaker as part of the panel of professional coaching experts including Vice President of the International Coaching Federation (ICF) David Ribott at an event in&nbsp;the Psychology Department of Heriot Watt University on 15 May 2017.&nbsp;</p>\r\n<p style=\"text-align: justify;\">The aim of the event was to raise awareness about the positive impact of coaching for businesses professionals and illustrate to students what it entails exactly to be a coach. The talks at the event ranged from the use of psychometric tools in business coaching to the connection between organisational psychology and coaching.</p>\r\n<p style=\"text-align: justify;\">Coaching in business is a growing profession in the region but with a lot of confusion about how it fits into organizations. Herriot Watt University and Liesl Connolly aof IHS aimed to bridge that gap for the public at this event.</p>\r\n<p style=\"text-align: justify;\">The evening was extremely well received and we at Innovative HR Solutions would like to thank Herriot Watt University as well as the International Coaching Federation for hosting such a great event and look forward to more exciting events in the future.</p>', '', '', ''),
(34, 18, 'en', 'We are finalists for the Gulf Capital SME Awards 2017!', NULL, '', '<p class=\"center\">&nbsp;<strong>PRESS RELEASE</strong></p>\r\n<p class=\"center\"><img title=\"IHS is delighted to announce that we are finalists for the Gulf Capital SME Awards 2017!\" src=\"/uploads/images/02f6cff99b1cefd3a208ea7ed34d5939972f378b.jpg\" width=\"851\" height=\"315\" /></p>\r\n<p class=\"center\"><strong>10<sup>th</sup> July 2017</strong></p>\r\n<p>Innovative HR Solutions is delighted to announce we are a finalist again this year in the Gulf Capital SME Awards 2017; the&nbsp;<strong>RSA Customer Focus of the Year</strong> award as well as the&nbsp;<strong>People &amp; Culture of the Year</strong> award.</p>\r\n<p>&nbsp;<strong><em>\"We are very excited to be the finalists for the RSA Customer Focus of the Year award and the People &amp; Culture of the Year award 2017. We are passionate about what we do, and delighted to be recognised by MEED and RSA for the positive contributions we have made for our own people, as well as for the growth and development of the talent and teams in our client organisations,</em></strong><strong><em>\"</em></strong>&nbsp; says Amanda White, Managing Director of Innovative HR Solutions.</p>\r\n<p>The&nbsp;2017 Gulf Capital SME Awards will be bigger and more exciting than ever as it continues to recognise the best small and medium businesses in the UAE. The Gulf Capital SME Awards Programme recognises success, growth and innovation in business.</p>\r\n<p><strong><em>&ldquo;This year, there are 56 more finalists than in the 2016 edition, when 47 were shortlisted in various categories. This means more and more SMEs are reporting growth and are getting on the radar for their business accomplishments and growth,&rdquo;</em></strong> says Karim el-Solh, co-founder and CEO of Gulf Capital, one of the largest alternative asset management firms in the Middle East and the headline sponsor of the awards. Winners will be announced on 11 October 2017 at The Westin Hotel, Dubai. For information and updates on the awards, please visit&nbsp;<a href=\"http://sme.meedevents.com\">http://sme.meedevents.com</a></p>\r\n<p><strong>For event enquiries, please contact:</strong></p>\r\n<p>Becky Crayman <br /> Head of Awards, MEED<br /> P O Box 25960, 20th Floor, Al Thuraya Tower 1, Dubai Media City, Dubai&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br /> Tel: 00971 (0) 4 8180314 | M&nbsp;: 00971 (0) 505590713 | E: <a href=\"mailto:becky.crayman@meed.com\">becky.crayman@meed.com</a></p>\r\n<p>Kristina Beggs<br /> Business Development Manager, Innovative HR Solutions<br /> PO Box 52271, Office, G05-08, Block 3, Ground Floor, Dubai Knowledge Village, Dubai&nbsp;&nbsp;&nbsp; <br /> Tel: 00971 (0) 4 3902778 | M&nbsp;: 00971 (0) 551378245| E: <a href=\"mailto:kristina@innovative-hr.com\">kristina@innovative-hr.com</a></p>', '', '', ''),
(35, 19, 'ar', 'إعلان إطلاق شركة PSI  الشرق الأوسط !', 'إعلان إطلاق شركة PSI  الشرق الأوسط !', '', '<p>يسر شركة إنوفيتيف لحلول الموارد البشرية (IHS) الرائدة في مجال تفييم وتطوير المواهب، الإعلان عن تغيير اسم مؤسستها لـ PSI Assessment Services FZ LLC و ستعرف بـ PSI الشرق الأوسط في جميع مكاتبها في الخليج من الان فصاعداً.</p>\r\n<p>بعد استحواذ شركة PSI Services Inc على إنوفيتيف لحلول الموارد البشرية في شهر يونيو من هذا العام، تم الاتفاق على تغيير الاسم ليعكس شعار شركتنا الأم، والتزامنا في هذه المنطقة بمواصلة تطوير وتوفير خدمات التكنولوجيا الخاصة بالتراخيص والشهادات المقدمة من قبل بشركة PSI .</p>\r\n<p>كما صرحت أماندا وايت، المديرة التنفيذية لشركة PSI الشرق الأوسط قائلة \" نحن متفائلون ونترقب الخطوة التالية من رحلتنا كـ PSI الشرق الأوسط بإفتتاح مكتبنا الجديد في الرياض في بداية العام المقبل. ان خبرتنا العميقة في المنطقة بالإضافة لتكنولوجيا التقييم ذات المعايير العالمية لـ PSI ستمكننا من مواجهة التحديات الرئيسية الخاصة بعملائنا، وستتيح لنا المجال لتقديم حلول تجمع بين المحتوى والتكنولوجيا مع الخبرة الإستشارية.\"</p>\r\n<p>عن شركةPSI الشرق الأوسط</p>\r\n<p>تقوم PSI الشرق الأوسط بالعمل مع المؤسسات من أجل اختيار المواهب والفرق وتقييمها وتطويرها. بجانب دعم التطوير المؤسسي من خلال تصميم وتنفيذ الأطر الاستراتيجية للموارد البشرية ذات المعايير العالمية. يقع مقرنا في الإمارات العربية المتحدة منذ عام 1999، ويعد فريقنا أحد&nbsp;أكبر فرق المختصين بعلم النفس المهني والتعليم والتطوير في الخليج. &nbsp;كما نقوم بتمثيل ١٢ ناشراً للإختبارات العالمية في منطقة الشرق الأوسط. يعمل الفريق باللغتين العربية والإنجليزية على مختلف المستويات، بدءا بالأفراد ذوي المواهب إلى مستوى المسؤولين التنفيذيين في الشرق الأوسط.</p>\r\n<p>وتعدPSI الشرق الأوسط جزء من PSI Services LLC (PSI)، التي تمتلك أكثر من 70 سنة من الخبرة في تقديم حلول التقييم عالمياً للشركات والمؤسسات الحكومية و هيئات التصديق والمؤسسات الأكاديمية . وتقدم PSI مناهج حلول شاملة من تطوير الاختبارات ومعالجة النتائج، مما يشمل اختبارات التقييم لما قبل التوظيف، وتقييم الإدارة، وإختبارات الترخيص والتصديق، وإختبارات الدراسة عن بُعد.</p>\r\n<p>للمزيد من المعلومات</p>\r\n<p>جانيت غارسيا | رئيسة شركة PSI International | jgarcia@psionline.com | الهاتف: <span style=\"white-space: nowrap; direction: ltr; display: inline-block;\">+44 78675580</span> | <a href=\"https://www.psionline.com/talent-measurement/\">https://www.psionline.com/talent-measurement/</a> أماندا وايت | المديرة التنفيذية PSI الشرق الأوسط | amandawhite@psionline.com | الهاتف المحمول: <span style=\"white-space: nowrap; direction: ltr; display: inline-block;\">+971 509110 772</span> | هاتف المكتب : <span style=\"white-space: nowrap; direction: ltr; display: inline-block;\">+971 4390 2779</span> | <a href=\"http://www.middleeast.psionline.com\">www.middleeast.psionline.com</a></p>', 'تستحوذ بي اس آي سيرفيسز على انيوفيتف اتش ار سوليوشنز خبراء التقييم و التطوير في الشرق الأوسط', '', 'معا نوحد القوى لتقديم الحلول المبتكرة للتقييم ولإستشارات رأس المال البشري على التكنولوجيا الرائدة في العالم\r\n\r\n'),
(36, 19, 'en', 'Launching PSI Middle East, formerly Innovative HR Solutions', 'Launching PSI Middle East, formerly Innovative HR Solutions - read the press release to find out more.', '', '<p><em>Joining forces to deliver innovative assessment solutions and human capital consulting on the world&rsquo;s leading technology</em></p>\r\n<p><strong>Dubai, UAE&nbsp;&mdash; 27th November, 2018 &mdash;&nbsp;</strong><strong>Innovative HR Solutions (IHS)</strong>, leaders in talent assessment and development, is delighted to announce it is changing its corporate name to PSI Assessment Services FZ LLC and will be known simply as <strong>PSI Middle East </strong>across all of our GCC offices henceforth.&nbsp;</p>\r\n<p>Following the acquisition of IHS by PSI Services LLC in June this year, the name change reflects our parent company brand, and our regional commitment to providing the leading technology Talent Measurement and eAssessment solutions of the wider PSI business.&nbsp;</p>\r\n<p>Steve Tapp, CEO at PSI said &ldquo;Our acquisition earlier this year, continues PSI&rsquo;s global growth strategy, further strengthening our offering with an established and very experienced team in the Middle East&rdquo;. &nbsp;</p>\r\n<p>Amanda White, Managing Director has commented; &ldquo;We look forward to the next stage of our journey as PSI Middle East and the opening of our Riyadh office in the New Year.&nbsp; Our deep regional expertise together with the world-class PSI assessment technology will enable us to address some of the key customer challenges providing solutions that merge content, technology and consulting expertise.&rdquo;</p>\r\n<p><strong>About PSI Middle East </strong></p>\r\n<p>PSI Middle East work with organisations to select, assess and develop talent and teams, and support organisational development through the design and implementation of world-class strategic HR frameworks. Headquartered in the UAE since 1999, PSI Middle East comprises one of the largest teams of Chartered Occupational Psychologists and Learning and Development specialists in the Gulf, representing 12 International Test Publishers in the region. The team work in English and Arabic with Young Talent through to Senior Executives across the GCC and Levant region.&nbsp; PSI Middle East is part of the PSI Services LLC (PSI).&nbsp; PSI has over 70 years of experience providing worldwide testing solutions to corporations, federal and state government agencies, professional associations, certifying bodies and leading academic institutions. PSI offers a comprehensive solutions approach from test development to delivery to results processing, including pre-hire employment selection, managerial assessments, licensing and certification tests, distance learning testing, license management services and professional services. More information is available at <a href=\"http://www.psionline.com\">www.psionline.com</a></p>\r\n<p><strong>Further information </strong></p>\r\n<p>Janet Garcia | President, PSI International | <a href=\"mailto:jgarcia@psionline.com\">jgarcia@psionline.com</a> | Mobile: +44 7867558041 | <a href=\"https://www.psionline.com/talent-measurement/\">https://www.psionline.com/talent-measurement/</a></p>\r\n<p>Amanda White | Managing Director, PSI Middle East | <a href=\"mailto:amandawhite@psionline.com\">amandawhite@psionline.com</a>&nbsp; Mobile:&nbsp; +971 509110 772 | Landline:&nbsp;&nbsp; +971 4390 2779 | <a href=\"http://www.middleeast.psionline.com\">www.middleeast.psionline.com</a></p>\r\n<p>&nbsp;</p>', 'IHS has been acquired by PSI Services LLC  ', '', 'Joining forces to deliver innovative assessment solutions and human capital consulting on the world’s leading technology\r\n\r\n'),
(37, 20, 'en', 'Why Assessment is Crucial', 'Why Assessment is Crucial', '', '<p>PSI Innovative Human Resource Solutions is proud to announce the first of a series of webinars, bringing the latest Thought Leadership in Talent Management across the GCC. All webinars will be recorded and available on demand.&nbsp;</p>\r\n<p>Join our live webinar to understand how assessments can help you predict employee performance to inform hiring decisions.&nbsp; How can we use assessments to identify high potential talent for Succession Planning, and finally leverage assessment insights to support talent and leadership development.&nbsp;This session will provide insight on the crucial role of assessment at each stage of the employee life cycle.</p>\r\n<p>Book your place now and understand how to implement assessment in your organisation today!&nbsp;</p>\r\n<p><strong>Learning Points :</strong></p>\r\n<ul>\r\n<li>Outlining the business case for assessments.</li>\r\n<li>Explain what assessment tools can help at each stage of the employee life cycle.</li>\r\n<li>Identify the right assessment tools to answer critical talent capital questions.</li>\r\n</ul>\r\n<p><strong>Speaker:</strong></p>\r\n<p>Simon Boag is a Senior Occupational Psychologist and Certified Executive Coach with a passion for Leadership Development. With close to 20 years consultancy experience at an international level he has built an excellent track record of working with diverse client groups using a strong client-focused approach in talent assessment and leadership development.</p>\r\n<p class=\"center\">&nbsp;</p>\r\n<p class=\"center\"><strong>Thursday, 19th April 2018&nbsp;&nbsp;</strong><strong>10:30am - 12:00pm (U.A.E)</strong></p>\r\n<p class=\"center\"><a href=\"https://zoom.us/webinar/register/WN_TKFbOARMS2qsrHsG0faZTQ\">WATCH NOW</a></p>', '', '', ''),
(38, 21, 'en', 'Cultural Intelligence- The Art of Understanding Emotions', '', '', '<p><span class=\"bold\">Topic: Cultural Intelligence- The Art of Understanding Emotions</span></p>\r\n<p><span class=\"bold\">Time and Date:&nbsp;Sunday 10th June 2018 from 10:00 - 11:15 GST</span>&nbsp;</p>\r\n<p>Join our live webinar to understand how your capability to relate and work effectively across cultures may impact your success as a professional, leader and organisation. The webinar explores how we can use assessments to identify the level of cultural intelligence within our organisations and enhance our competitive advantage by celebrating our diversity.</p>\r\n<p>&nbsp;<span class=\"bold\">Learning Points :&nbsp;</span></p>\r\n<ul>\r\n<li>Insights on the key mistakes that we make when attempting to understand other cultures.</li>\r\n<li>The business case for enhancing our cultural intelligence as organisations.</li>\r\n<li>Identify the right assessment tools to answer critical culture-related questions.</li>\r\n</ul>\r\n<p>&nbsp;<span class=\"bold\">Speaker:&nbsp;</span></p>\r\n<p>Marais Bester is a Senior Chartered Occupational Psychologist with a passion for talent management. With many years of international experience, Marais has developed a&nbsp;excellent track record of working with Middle Eastern clients from&nbsp;a range of industries in the fields of talent assessment and talent optimisation. Marais has just completed his PhD focusing on career psychology where he explored the different psychological resources and psychological drivers that typically contribute to career success, career satisfaction and career wellbeing.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class=\"bold\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href=\"https://zoom.us/webinar/register/WN_WGLPuMqLQfGaqHXsK-N7jQ\">WATCH HERE</a></span></p>', '', '', ''),
(39, 22, 'en', 'Talent Analytics ', 'Talent Analytics', '', '<p><span class=\"bold\">Topic:&nbsp;Talent Analytics</span></p>\r\n<p><span class=\"bold\">Time and Date: Thursday 19th July 2018 from 10:00 - 11:15 GST</span>&nbsp;</p>\r\n<p>A recent study by Deloitte found that only 10% of companies are using analytics to solve talent challenges and only 4% are using analytics to predict and forecast future talent needs and outcomes. The majority of companies (84%) that use data only for reporting are at a significant disadvantage as companies that use HR data <em>analytically</em>&nbsp;have higher than average stock returns, more effective HR practices and more credible HR teams.</p>\r\n<p>&nbsp;<span class=\"bold\">Learning Points :&nbsp;</span></p>\r\n<p>*&nbsp;&nbsp; &nbsp;Understand how and where to use analytics<br /> *&nbsp;&nbsp; &nbsp;Critique data that is presented<br /> *&nbsp;&nbsp; &nbsp;Build Business Cases for projects that involve analytics<br /> *&nbsp;&nbsp; &nbsp;Communicate data analysis to peers and clients&nbsp;</p>\r\n<p>&nbsp;<span class=\"bold\">Speaker:&nbsp;</span></p>\r\n<p>Simon Tither, an Occupational Psychologist&nbsp;holds a Masters Degree in Applied Psychology (Industrial and Organisational). Simon has gained a wide range of experience working across a number of sectors in the region, in analysing the data from Assessment and Development Centres to better advise his clients in their HR decisions and budget spend.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a href=\"https://zoom.us/webinar/register/WN_FwBAlz3yR8uviRAzCo3glA\">WATCH HERE</a></p>\r\n<p>&nbsp;</p>', '', '', ''),
(40, 23, 'en', 'Achieving Peak Performance in Challenging Times ', 'Achieving Peak Performance in Challenging Times ', '', '<p><strong>Achieving Peak Performance in Challenging Times&nbsp;</strong></p>\r\n<p>&nbsp;</p>\r\n<p>In these challenging economic times many staff feel under pressure whether it be because of more demanding targets, their own job maybe under threat or generally being asked to do more with less.</p>\r\n<p>&nbsp;</p>\r\n<p>Mental toughness is the quality, which determines in some part how people deal with these challenges, stressors, and the ever increasing demands of organisational life in challenging and turbulent times!.</p>\r\n<p>If you can get your staff to continue to perform even when under severe pressure then it could put you ahead of your competitors.</p>\r\n<p>This webinar is intended to create an understanding of what stressors are and how people and groups can best deal with them.</p>\r\n<p>We will explore:</p>\r\n<ul>\r\n<li>What causes one person to succumb and another to thrive in the same circumstances?</li>\r\n<li>Can we identify people&rsquo;s strengths and weaknesses in these areas?</li>\r\n<li>Can we develop individuals to enable them to handle stressors like change, pressure and challenge more effectively?</li>\r\n<li>How can we support individuals better with their specific needs?</li>\r\n</ul>\r\n<p><strong>Speaker:</strong></p>\r\n<p>Simon Boag is a Senior Occupational Psychologist and Certified Executive Coach with a passion for Leadership Development. With close to 20 years consultancy experience at an international level he has built an excellent track record of working with diverse client groups using a strong client-focused approach in talent assessment and leadership development.</p>\r\n<p class=\"center\">&nbsp;</p>\r\n<p class=\"center\"><strong>Thursday, 30th August 2018&nbsp;&nbsp;</strong><strong>10:30am - 11:15am GST</strong></p>\r\n<p class=\"center\"><a href=\"https://zoom.us/webinar/register/WN_ofsFgj8NT6WIUgB3bYYoxw\">WATCH NOW</a></p>\r\n<p>&nbsp;</p>', '', '', ''),
(41, 24, 'en', 'How to Engage High Potential', '', '', '<p><strong>How To Engage High Potential</strong></p>\r\n<p>&nbsp;</p>\r\n<div>Find out how innovative, immersive and multi-assessment solutions can support you to assess and develop your high potentials to drive behavioural change.</div>\r\n<div>&nbsp;</div>\r\n<div>During this 30 minute webinar, one of our Senior Consultants, Jordon Jones will share several case studies on how to create a multi assessment process designed to engage and develop high potentials.</div>\r\n<p>We will explore:</p>\r\n<ul>\r\n<li>How to identify your high potentials</li>\r\n<li>How to use technology to engage your high potential talent pool</li>\r\n<li>How to embed the learning</li>\r\n</ul>\r\n<p><strong>Speaker:</strong></p>\r\n<p>Jordon is a psychologist at the talent management consultancy a&amp;dc (Assessment &amp; Development Consultants). Alongside his MSc in Occupational and Organisational Psychology from the University of Surrey, Jordon is a member of the British Psychological Society (BPS), a registered specialist in psychometric test use (including Saville Wave, OPQ, MBTI, EIP and 15FQ+ among others).</p>\r\n<p class=\"center\">&nbsp;</p>\r\n<div>\r\n<div align=\"center\"><strong>Thursday 4th October 2018,&nbsp;</strong></div>\r\n</div>\r\n<div align=\"center\"><strong>15:00 - 15:30 GST</strong></div>\r\n<p class=\"center\"><a href=\"https://vimeo.com/293360592/6d0e0b28f6\">WATCH IT HERE</a></p>\r\n<p>&nbsp;</p>', '', '', ''),
(42, 25, 'en', 'Snakes in Suits - Managing the Dark Triad in the Workplace', '', '', '<div><strong>Snakes in Suits - Managing the Dark Triad in the Workplace</strong></div>\r\n<p>This webinar will focus on exploring the conceptualisation and diagnostic criteria for the personality disorders/traits of narcissism, Machiavellianism, and psychopathy (the Dark Triad)</p>\r\n<div>\r\n<div align=\"justify\">This webinar will show you:</div>\r\n<div align=\"justify\">\r\n<ul>\r\n<ul>\r\n<li>The influence on Leadership the Dark Triad will have in the workplace</li>\r\n<li>The impact on the organisational culture the Dark Triad wll have in the workplace</li>\r\n<li>Potential interventions for organisations who employ individuals with these personality disorders/traits will be explored</li>\r\n</ul>\r\n</ul>\r\n</div>\r\n</div>\r\n<p><strong>Speaker:</strong></p>\r\n<p>Marais Bester is a Senior Chartered Occupational Psychologist with a passion for talent management. With many years of international experience, Marais has developed a&nbsp;excellent track record of working with Middle Eastern clients from&nbsp;a range of industries in the fields of talent assessment and talent optimisation. Marais has just completed his PhD focusing on career psychology where he explored the different psychological resources and psychological drivers that typically contribute to career success, career satisfaction and career wellbeing.</p>\r\n<div>\r\n<div align=\"center\"><strong>Tuesday 30th October 2018,&nbsp;</strong></div>\r\n</div>\r\n<div align=\"center\"><strong>14:00 - 15:00 GST</strong></div>\r\n<p class=\"center\"><a href=\"https://zoom.us/webinar/register/WN_Zj5awkhUSVS1QcYcQ3ke5w\" target=\"_blank\">WATCH NOW</a></p>', '', '', ''),
(43, 26, 'en', 'GRIT...THE MISSING LINK', '', '', '<p><strong>GRIT...THE MISSING LINK</strong></p>\r\n<p><em>Sharpen your grit and watch your dreams turn into reality.</em></p>\r\n<p>By Dr MARAIS BESTER</p>\r\n<p>&nbsp;</p>\r\n<p>On a recent trip to Doha, Qatar, I met a Kenyan taxi driver named Jensen. For the past five years in Doha, he has worked 17-hour shifts every day and has not been home to see his family. His hard work has paid off and he has been able to buy a plot of land in Kenya on which he has three cows and a delivery truck. He has also sent his two sons to university. He has decided that he will be going home at the end of the month. To me, Jensen is a great example of someone with grit.</p>\r\n<p>The concept of grit can be seen as a positive psychological concept that focuses on an individual&rsquo;s perseverance and passion for a particular long-term objective or end-state. Individuals who have high levels of grit are typically courageous, conscientious, goal-orientated, resilient and always striving for excellence.</p>\r\n<p><strong>Courage</strong></p>\r\n<p>When it comes to courage, individuals who have high levels of grit are typically not scared of taking risks. They are also not scared of making mistakes. They believe that there are great lessons to be learnt from losing, messing up and overcoming one&rsquo;s fears. Psychologists describe the fear of failure as an abnormal and unhealthy aversion to risk. Fear of failure sees individuals develop a very strong resistance to change, coupled with feelings of ambiguity and vulnerability. Change or uncertainty will typically be met with feelings of anxiety, mental blocks and inability to perform. In the words of Eleanor Roosevelt: &ldquo;Do something that scares you every day.&rdquo; Consider your goals and break them down into bite-sized actions or solutions. Reflect on your own strengths and experience and consider how they can be utilized to overcome your</p>\r\n<p>fears. Don&rsquo;t be scared of making a fool of yourself or failing because courage is a muscle that needs to be exercised. The more you put yourself out there the more you will develop courage and, subsequently, grit.</p>\r\n<p><strong>Conscientiousness</strong></p>\r\n<p>Being conscientious is not just about showing up at work every day or being dependable and meticulous. Conscientiousness implies being able to align your actions towards a specific goal or outcome. Individuals with high levels of grit do not merely slave away at their tasks or responsibilities but take a long-term view of their work and align their activities accordingly. In Jensen&rsquo;s case, for example, he knew that he had to work 17-hour shifts every day in order to achieve his outcomes and he did it with a smile. How are the daily activities that you are busy with helping you achieve your goals? Some of these activities may be fun, some may be difficult or tedious, but as long as they are aligned to what you want to achieve, you will experience a lot of contentment when you engage in them.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Goal-orientated</strong></p>\r\n<p>As mentioned earlier, grit is all about perseverance and endurance. Individuals who have high levels of grit are like marathon runners who have the finish line in mind, but also know that they need to run at a persistent pace to reach the finish line. Being a good marathon runner is not necessarily about being talented in running, but rather about being headstrong, consistent in your training and staying focused on your goals. The difference between somebody who succeeds and somebody who is just spending a lot of time doing things is this: Practice must have purpose. Do not spend nine hours at work every day working on someone else&rsquo;s goals and come home and spend no time working on your own goals. Write down your goals, nurture them and spend time every day working toward them.</p>\r\n<p><strong>Resilience</strong></p>\r\n<p>Individuals who have high levels of grit are typically very resilient. Resilience has to do with the ability to bounce back from difficult circumstances, coping with pressures and dealing with ambiguity. Psychologists describe resilient individuals as being hardy, having the courage to grow from stressful or even traumatizing situations and being aware of the things that they can and those which they cannot control. Research has shown that resilience is ordinary, not extraordinary, and that it can be developed in anyone. A key trait that resilient individuals have is that they know the extent of their resilience and where their emotional cups can be refilled. To enhance your resilience, you should invest in mutually satisfactory relationships. This provides a support system. You must keep in mind that change is a part of life and that you need to be proactive and decisive about your future and to look for opportunities for self-discovery.</p>\r\n<p><strong>Excellence</strong></p>\r\n<p>Most individuals who have high levels of grit do not seek perfection, but rather strive for excellence. Perfectionists tend to be pedantic, stubborn, demanding and a real pain to be around. Perfectionism could lead to anxiety disorders, low self-esteem and even obsessive-compulsive disorder. Excellence is far more forgiving. It allows for mistakes and prioritizes progress over perfection. Excellence and grit have to do with an attitude of engaging in activities because you enjoy them, and it is an opportunity for you to explore, develop and optimise your talents and capabilities, rather than to prove a point to someone or yourself.</p>\r\n<p>People with grit are flexible, proactive and goal-orientated. They enjoy life, have positive relationships, are self-aware and never say no to a challenge.</p>\r\n<p>How gritty are you?</p>\r\n<p><em>&nbsp;</em></p>\r\n<p><em>Dr Marais Bester is a registered occupational psychologist who works for PSI Innovative HR Solutions in Dubai. As a consultant, speaker and author, he works across Africa and the Middle East to help people optimise their careers. Find him on:</em></p>\r\n<p><em>twitter: @marais_bester;</em></p>\r\n<p><em>LinkedIn-linkedin.com/in/maraisbester/;</em></p>\r\n<p><em>Website-www.innovative-hr.com</em></p>\r\n<p><em>This article was published in Esteem Psychology Magazine Oct - Dec 2018 issue.&nbsp;</em></p>', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Admin', 'Super Admin', '0', NULL, NULL),
(2, 'admin', 'Admin', 'Site Admin', '1', NULL, NULL),
(3, 'manager', 'Manager', 'Site Manager', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(3, 2),
(3, 4),
(3, 5),
(3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `search_index`
--

CREATE TABLE `search_index` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `search_index`
--

INSERT INTO `search_index` (`id`, `locale`, `type`, `type_id`, `url`, `title`, `content`) VALUES
(1, 'en', 'pages', '4', 'https://middleeast.psionline.com/home', 'Homepage', ''),
(2, 'en', 'pages', '119', 'https://www.innovative-hr.com/thank-you-for-subscription', 'Thank You', 'Thank you for subscribing to our Newsletter! We are excited to have you as part of our community, bringing you the latest news and events in the Talent Management space! \r\nDo follow&nbsp;us on Facebook, Twitter and LinkedIn for all the latest talent thinking updates and news!&nbsp;\r\nIf you have any questions regarding how we can help you meet your business objectives please contact us on +97143902778 or email infome@psionline.com '),
(3, 'ar', 'pages', '1', 'https://middleeast.psionline.com/about', 'عن PSI Middle East ', 'تعد شركة PSI الشرق الأوسط&nbsp;واحدة من أكبر فرق المختصين بعلم النفس المهني و التعليم و التطوير في الخليج العربي&nbsp;\r\n&nbsp;نقوم بالعمل مع المؤسسات من أجل اختيار المواهب والفرق وتقييمها وتطويرها، بجانب دعم التطوير المؤسسي من خلال تصميم وتنفيذ أطر للموارد البشرية الاستراتيجية ذات معايير عالمية. يقع مقر شركة&nbsp;PSI االشرق الأوسط في الإمارات العربية المتحدة منذ أكثر من ١٥ سنة، كما نقوم بتمثيل ١٢ ناشراً للإختبارات العالمية في منطقة الشرق الأوسط، وقد قمنا بتوفير أدوات قياس سيكولوجية وأدوات تقييم قيادية مرموقة في منطقة الخليج بهدف دعم التطوير الفردي والمؤسسي الفعال.\r\nيعمل فريقنا في منطقة دول الخليج لتنفيذ مجموعة من مبادرات الموارد البشرية الاستراتيجية. تتضمن مجالات خبرتنا المتخصصة في الأنشطة التالية:\r\n\r\nمراكز التقييم والتطوير لأغراض التوظيف والتطوير والترقية\r\nاستراتيجيات الموارد البشرية للتعاقب الوظيفي، إدارة المواهب، ارتباط الموظفين، تقييم الأداء وأطر الكفاءات السلوكية\r\nاعتماد أدوات القياس السلوكية العالمية\r\nالتدريب التنفيذي والإرشاد\r\nتدريب مهارات الإدارة\r\nبرامج التوطين\r\n\r\n&nbsp;'),
(4, 'en', 'pages', '42', 'https://www.innovative-hr.com/about/team', 'The team', 'Our Senior Team comprises an expert group of Organisational Psychologists, Leadership Development Professionals and Human Capital specialists. Our combination of skills, background and experience places us in a unique position to help your organisation design leading HR strategies and develop talent with an uncompromising focus on practical implementation and business results.'),
(5, 'en', 'pages', '43', 'https://www.innovative-hr.com/about/clients', 'Our Clients', 'Our teams have successfully designed and implemented a range of strategic HR initiatives with a number of organisations across various industries.'),
(6, 'en', 'pages', '10', 'https://www.innovative-hr.com/knowledge-centre', 'Knowledge Centre', 'Knowledge Centre'),
(7, 'en', 'pages', '5', 'https://www.innovative-hr.com/knowledge-centre/news-events', 'News & Events', ''),
(8, 'en', 'pages', '33', 'https://www.innovative-hr.com/knowledge-centre/casestudies', 'Casestudies', ''),
(9, 'en', 'pages', '34', 'https://www.innovative-hr.com/knowledge-centre/casestudies/casestudy/4/leading-hoteliers-maximising-excellence-in-rotana-leaders', 'LEADing Hoteliers! Maximising Excellence in Rotana Leaders', ''),
(10, 'en', 'pages', '140', 'https://www.innovative-hr.com/knowledge-centre/book/thank-you', 'Thank You', 'Thank you for booking!\r\nOne of our team members will be in touch with you shortly!&nbsp;\r\nIf you have any questions regarding your Accreditation booking please contact us on +97143902778 or email&nbsp;courses@innovative-hr.com\r\nIf you have any questions regarding your event booking please&nbsp;contact us on +97143902778 or email&nbsp;info@innovative-hr.com\r\nPlease like us on Facebook and Twitter and follow us on LinkedIn for all the latest talent thinking updates and news!&nbsp;\r\n&nbsp;'),
(11, 'en', 'pages', '44', 'https://middleeast.psionline.com/calendar', '2019 Calendar', ''),
(12, 'en', 'pages', '86', 'https://www.innovative-hr.com/calendar/book/thank-you', 'Thank You', 'Thank you for booking!\r\nOne of our team members will be in touch with you shortly!&nbsp;\r\nIf you have any questions regarding your Accreditation booking please contact us on +97143902778 or email&nbsp;course@innovative-hr.com\r\nIf you have any questions regarding your event booking please&nbsp;contact us on +97143902778 or email&nbsp;info@innovative-hr.com\r\nPlease like us on Facebook and Twitter and follow us on LinkedIn for all the latest talent thinking updates and news!&nbsp;\r\n&nbsp;'),
(13, 'en', 'pages', '3', 'https://www.innovative-hr.com/contact', 'Contact', ''),
(14, 'en', 'pages', '87', 'https://www.innovative-hr.com/contact/thank-you', 'Thank You', 'Thank you for sending us your enquiry!&nbsp;\r\nOne of our team members will be in touch with you shortly!&nbsp;\r\nIf you have any questions regarding your enquiry&nbsp;please contact us on +97143902778 or email infome@psionline.com\r\nPlease like us on Facebook and Twitter and follow us on LinkedIn for all the latest talent thinking updates and news!&nbsp;'),
(15, 'en', 'pages', '11', 'https://middleeast.psionline.com/consulting', 'Consulting', 'To ensure competitive advantage it is vital the right Talent Strategies and Frameworks are in place to drive your business outcomes. We will work closely with you to develop, communicate and embed comprehensive and integrated talent strategies to deliver business results.'),
(16, 'en', 'pages', '46', 'https://www.innovative-hr.com/consulting/competency-framework-design', 'Competency Framework Design', 'Competencies are skills and abilities described in behavioural terms that are coachable, observable, measurable, and critical to effective organisational performance. They form a common language about successful behaviours within an organisation creating the foundation for effective and integrated HR processes that drive business performance. Competency Frameworks can be used to support recruitment, selection and promotion, employee development, performance management, career planning and succession planning activities.\r\nDoes your Competency Framework integrate talent management and business strategy to provide employees with a clear understanding of the behaviours and skills required? Do they help individuals to understand the desired culture of your organisation which will in turn help them to realise the strategic vision?\r\nWe start with visioning exercises and strategic conversations at the most senior leadership level to fully understand the strategy and how certain behaviours will drive and enable the plan. We will then work closely with you to design a creative and engaging implementation strategy across the organisation.'),
(17, 'en', 'pages', '47', 'https://www.innovative-hr.com/consulting/performance-management', 'Performance Management', 'A Performance Management process is intended to guide the expectations of individual, team and organisational performance. It provides a meaningful process by which employees can be rewarded for noteworthy contributions to the organisation, and provides a mechanism to improve individual and organisational performance. Establishing actionable, role-specific, and achievable performance goals is critical to the success of any performance initiative.\r\nFeedback is a natural part of performance management, and specific and timely feedback to employees about their performance against expectations provides the foundation for discussing developmental needs. Effective Performance Management will ensure employees&rsquo; goals are aligned with the overall organisation&rsquo;s business goals, mission, vision, and values. It will also enable a Personal Development Plan consistent with organisational goals and specific needs and ensure fairness and objectivity in reward and recognition.\r\nWe work with organisations to design performance management strategy, policy, procedures and the accompanying templates. We can also support with effective communication templates, training programmes and implementation strategy.'),
(18, 'en', 'pages', '48', 'https://www.innovative-hr.com/consulting/succession-and-talent-management', 'Succession Planning & Talent Management', 'Succession Planning is the ongoing, systemised process of identifying, developing and retaining your organisations &lsquo;high potential&rsquo; individuals. The intent is to create a talent pipeline to ensure key leadership positions and critical roles are occupied by appropriately skilled people now and in the future. This process is fundamentally guided by the strategy, business performance and the requirements set at each level / job role.\r\nWho should be considered talent? What do you do to develop talent once identified? Who should stay in the talent pool? The key components of our programme strategies include;\r\n\r\nDetermining what factors are critical for success in your organisation.\r\nCreating a competency model.\r\nDetermining key roles and career paths throughout the organisation.\r\nHaving a process to identify employees who possess required&nbsp;skills or can be coached and trained to attain the necessary&nbsp;skills in the future.\r\nSpending the necessary time and resources to prepare individuals for future roles.\r\n'),
(19, 'en', 'pages', '49', 'https://www.innovative-hr.com/consulting/nationalisation-programmes', 'Nationalisation Programmes', '\"The true wealth of a nation lies in its youth&hellip;one that is equipped with education and knowledge and which provides the means for building the nation and strengthening its principles to achieve progress on all levels.\" Sheikh Mohamed Bin Zayed Al Nahayan, Crown Prince of Abu Dhabi\r\nHow an organisation attracts, retains and develops its &lsquo;National&rsquo; talent is a priority across the GCC region. Our focus is designing an overall people strategy aligned to business strategy, vision, mission and values. We help organisations define the way forward and develop SMART objectives and frameworks at individual, team and organisational-level.'),
(20, 'en', 'pages', '12', 'https://www.innovative-hr.com/assessment', 'Assessment', 'As Occupational Psychologists we are licensed to provide world-class assessment services to support recruitment, selection for promotion, identification of high potential talent and succession planning. We are able to run individual assessments for critical hires, through to large-scale assessment programmes.'),
(21, 'en', 'pages', '40', 'https://www.innovative-hr.com/assessment/assessment-services', 'Assessment Services', 'The cost of selecting the wrong person albeit for recruitment or selection for promotion or succession planning, can run into the hundreds of thousands or even millions of dollars; not to mention the potential impact to morale and productivity. We work with a number of world class organisations to assess key hires as part of the recruitment and selection process.\r\nWhen we are asked to assess on behalf of our clients we take a holistic approach. We seek demonstration of key leadership behaviours, capabilities i.e. critical thinking skills, learning agility, problem solving capacity, as well as values and environment fit to the organisation.\r\nWe design our approach in line with our clients and the critical success factors of the role and organisation. On one end of the continuum we are able to gain valuable insights through the use of world-class personality and ability psychometrics combined with a Structured Interview. To support these findings we can also include a work sample or conduct a full Assessment Centre with observable assessment centre exercises (business simulation exercises). Most importantly, our clients can be sure that we will provide clear recommendations for selection and or development supported by action plans for the way forward.\r\n&nbsp;\r\nWe have Arabic and English Assessment tools and materials for all kinds of Assessment requirements. Please reach out to our team for more info today! 00971 4 390 2778'),
(22, 'en', 'pages', '41', 'https://www.innovative-hr.com/assessment/personality-assessment', 'Personality Assessments', ''),
(23, 'en', 'pages', '121', 'https://www.innovative-hr.com/assessment/personality-assessment#overview', 'Overview', 'How someone behaves at work has a considerable impact on how successful they are in their job role. There is a consistency in the way people behave across situations and over time. Personality tools help in identifying the typical way by which people relate to others, approach tasks and respond to situations. At PSI Middle East we have a range of personality tools all from the world&rsquo;s leading test publishers selected on the basis of their validity, reliability, ease of use, cost efficiency and objectivity.'),
(24, 'en', 'pages', '122', 'https://www.innovative-hr.com/assessment/personality-assessment#Wave', 'Assessment, A Willis Towers Watson Company', '\r\nWave Professional Styles\r\nPersonality Profile, highest Predictive Validity of any personality instrument on the market today.&nbsp; For use in selection, development, talent management, identification of high potential, &nbsp; succession planning, leadership programs, coaching and career planning. GCC Norm Groups and Arabic capability available for some questionnaires!&nbsp;\r\nTARGET CATEGORY\r\nAll - up to Senior Executives, available in Arabic and English\r\n&nbsp;\r\n&nbsp;\r\nWave Focus Styles\r\nPersonality Profile used for volume recruitment, selection, development, talent management, succession planning and team effectiveness, &nbsp;identification of high potential.\r\nTARGET CATEGORY\r\nAll &ndash; up to Senior Executives\r\nWave Work Strengths\r\nPersonality Profile questionnaire identifies the potential strengths of an individual against the successful behaviours required for a graduate, management trainee, manager or professional role. \r\nTARGET CATEGORY\r\nAll &ndash; up to Senior Managers\r\n&nbsp;\r\n&nbsp;Customer Strengths\r\nPersonality Profile used to identify the potential strengths of an individual against the successful behaviours required for customer oriented roles.\r\nTARGET CATEGORY\r\nCustomer Staff in Call Centres, Retail, Hospitality, Leisure, Health &amp; Education\r\n&nbsp;\r\n&nbsp;\r\n&nbsp;Operational Strengths\r\nPersonality Profile used to identify the potential strengths of an individual against the behaviours required for&nbsp;apprentice, technical and manufacturing type roles.\r\nTARGET CATEGORY\r\nTechnical Apprentices, Operational Staff in Manufacturing, Engineering, Construction and Transport\r\n&nbsp;\r\nAdministrative Strengths\r\nPersonality Profile used to identify the potential strengths of an individual against the behaviours required for clerical and administrative roles.\r\nTARGET CATEGORY\r\nAdministrative Staff in Private &amp; Public Sector Offices\r\nCommercial Strengths\r\nPersonality Profile used to identify the potential strengths of an individual against the behaviours required for sales, marketing and business development roles.\r\nTARGET CATEGORY\r\nCommercial Staff in Sales, Marketing, Business Development &amp; Financial Services\r\n&nbsp;\r\n&nbsp;\r\nMy Self\r\nCareer Guidance And Self-Development tool which measures an individual\'s potential talents across a range of areas in the workplace.\r\nTARGET CATEGORY\r\nFresh Graduates and Younger Talent\r\n&nbsp;'),
(25, 'en', 'pages', '123', 'https://www.innovative-hr.com/assessment/personality-assessment#MBTi', 'MBTI', '\r\nIHS are&nbsp;proud&nbsp;partners to CPP and the only company licensed to sell,use and train the world renowned, Myers Briggs Type Indicator (MBTI), in the region.&nbsp;\r\nThe MBTI determines personality&nbsp;type using four dichotomies: extraversion-introversion, sensing-intuition, thinking-feeling and judging-perceiving. The MBTI is used for Development only &ndash; individuals and teams. It allows for individuals and teams to gain a deeper understanding of themselves and their interactions with others, helping them improve how they communicate, learn and work.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nStep I Profile\r\nStep II Profile\r\nStep I Interpretive Report\r\nStep II Interpretive Report\r\nArabic Step I Profile&nbsp;Report\r\n&nbsp;MBTI Product Catalogue for Practitioners'),
(26, 'en', 'pages', '125', 'https://www.innovative-hr.com/assessment/personality-assessment#eq', 'EQi', 'EQi 2.0\r\nFor use in development, and in certain circumstances for selection. Emotional Intelligence has been identified as one of the strongest predictors of leadership performance and is helpful in identifying how to get the most out of ourselves and others. The EQi provides a detailed look at how we understand and manage ourselves, our relationships with others, and our reaction to the stresses and demands of everyday life.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nClient Report\r\nCoach Report'),
(27, 'en', 'pages', '124', 'https://www.innovative-hr.com/assessment/personality-assessment#neo', 'NEO', 'NEO PI-3\r\nThe NEO PI-3 provides a comprehensive approach to the assessment of adult personality and can be used in a wide range of business contexts, including:\r\n\r\nCandidate selection\r\nLeadership/Talent development\r\nTeam building and effectiveness\r\nCoaching\r\n\r\nThe NEO Personality Inventory Revised (NEO PI-R) is a concise measure of adult personality based upon the five factor model (FFM) and prominent work of Costa &amp; McRae (1985). The assessment embodies a conceptual model that distils decades of factor analytic research on the structure of personality. The NEO PI-R replaces the original NEO PI by adding the Agreeableness and Conscientiousness factors. The latest version with updated scales and items is the PI-3. The FFM can be easily remembered through the OCEAN acronym.\r\n\r\nOpenness (to new experiences, feedback, and learning)\r\nConscientiousness (organisation, reliability, drive and structure)\r\nExtraversion (assertiveness, sociability, and empathy)\r\nAgreeableness (trust, care for others, and compliance)\r\nNeuroticism (anxiety, sensitivity, and temperament)\r\n\r\nThe NEO-PIR works best when face to face feedback is given and it focuses on 30 individual personality &lsquo;facet&rsquo; scales which are associated with different aspects of the FFM. NEO-PIR is suitable for use in occupational, clinical and research settings; it can be used for both selection and development of individuals in organisations. The Personal Insight reports split the results into four useful sections: problem solving and decision making, planning, organising and implementing, style of relating to others, and personal style. The Primary Colours Leadership report translates personality preferences into leadership potential based on the Primary Colours model of leadership (Pendleton &amp; Furnham, 2012).\r\nTARGET CATEGORY\r\nAll - Up to Senior Executives\r\nDOWNLOADS\r\nPR-I Personal Insight Report\r\nPI-3 Technical Report\r\nPI-3 Leadership Report'),
(28, 'en', 'pages', '126', 'https://www.innovative-hr.com/assessment/personality-assessment#hogan', 'Hogan', 'HOGAN Development Survey (HDS)\r\nRecruitment &amp; Development &ndash; Identifies personality-based performance risks and&nbsp;derailers of interpersonal behaviour.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nDevelopment Survey Report\r\n&nbsp;\r\nHOGAN Personality Inventory (HPI)\r\nIdeal tool to help you strengthen your employee selection, leadership&nbsp;development, succession planning, and talent management processes.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nPersonality Inventory Report\r\n&nbsp;\r\nHOGAN Motives, Values and Preferences Inventory (MVPI)\r\nPersonality inventory that reveals a person&rsquo; score values, goals and interests. Results indicate which type of position, job and environment will be most motivating for the employee and when he/she will feel the most satisfied.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nMotives, Values and Preferences Inventory Report'),
(29, 'en', 'pages', '28', 'https://www.innovative-hr.com/assessment/ability-assessments', 'Ability Assessments', ''),
(30, 'en', 'pages', '32', 'https://www.innovative-hr.com/assessment/ability-assessments#overview', 'Overview', 'Ability tests or aptitude tests are a standardised way of assessing how well people typically perform in varying work tasks, respond to problems, and are able to demonstrate learning agility and critical thinking skills. They have the advantage of measuring potential rather than simply academic performance, and are often used to make predictions about how people will perform in a work setting. Candidate results are scored by comparing their performance with the results of others who have completed the same assessments; known as a &lsquo;Norm Group&rsquo;. All of our aptitude and ability tests have a range of Norm Groups to ensure assessment at the appropriate level.\r\nWe provide a range of ability tests for different purposes. These include measures of Abstract Reasoning, General Aptitudes (Verbal, Numerical and Perceptual Reasoning) and Practical Aptitudes (Spatial Ability and Mechanical Aptitude).\r\nTo purchase ability tests you are required to be certified in BPS Qualification in Occupational Testing (Ability &amp; Personality).'),
(31, 'en', 'pages', '29', 'https://www.innovative-hr.com/assessment/ability-assessments#SWIFT', 'Saville', 'Swift Analysis Aptitude (V, N, D) Combination\r\nMeasures critical reasoning through short verbal (6 min), numerical (6 min) and diagrammatic (6 min) sub-tests.\r\nTARGET CATEGORY\r\nDirectors, Managers, Professionals, Graduates &amp; Management Trainees\r\nSwift Executive Aptitude (V, N, A) Combination\r\nMeasures critical reasoning through short verbal (6 min), numerical (6 min) and abstract (6 min) sub-tests.\r\nTARGET CATEGORY\r\nDirectors, Managers, Professionals, Graduates &amp; Management Trainees\r\n&nbsp;\r\nSwift Analysis Aptitude (V &amp; N) Combination\r\nMeasures critical reasoning through short verbal (12 min) and numerical (12 min) sub-tests and is suitable for all high-level roles.\r\nTARGET CATEGORY\r\nDirectors, Managers, Professionals, Graduates &amp; Management Trainees\r\n&nbsp;\r\nAbstract Aptitudes\r\nMeasures abstract critical reasoning across all roles.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\n&nbsp;\r\nAnalysis Aptitudes (V, N, D or A) Single Tests\r\nVerbal Analysis assesses the ability to evaluate complex written information. Numerical Analysis assesses the ability to evaluate numerical data. Diagrammatic Analysis assesses the ability to evaluate processes represented through diagrams.\r\nTARGET CATEGORY\r\nDirectors, Managers, Professionals, Graduates &amp; Management Trainees\r\n&nbsp;\r\nProfessional Aptitudes (V, N, D) Single Tests\r\nVerbal Analysis assesses the ability to evaluate complex written information. Numerical Analysis assesses the ability to evaluate numerical data. Diagrammatic Analysis assesses the ability to evaluate processes represented through diagrams.\r\nTARGET CATEGORY\r\nDirectors, Managers &amp; Professionals\r\n&nbsp;\r\nWork Aptitudes (V, N, D) Single Tests\r\nVerbal Analysis assesses the ability to evaluate complex written information. Numerical Analysis assesses the ability to evaluate numerical data. Diagrammatic Analysis assesses the ability to evaluate processes represented through diagrams.\r\nTARGET CATEGORY\r\nGraduates &amp; Management Trainees\r\n&nbsp;\r\nSwift Comprehension Aptitude (V, N, E) Combination\r\nMeasures general reasoning through short verbal (4 mins), numerical (4 mins) and checking (90 secs) sub-tests.\r\nTARGET CATEGORY\r\nOperational, Commercial, Customer &amp; Administrative Staff at apprentice or entry-level\r\n&nbsp;\r\nComprehension Aptitudes (V, N, E) Single Tests\r\nVerbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.\r\nTARGET CATEGORY\r\nService Sector Apprentices, Operational, Commercial, Customer &amp; Administrative Staff\r\n&nbsp;\r\nOperational Aptitudes (V, N, E) Single Tests\r\nVerbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.\r\nTARGET CATEGORY\r\nOperational Staff in Manufacturing, Engineering, Construction &amp; Transport\r\n&nbsp;\r\nCommercial Aptitudes (V, N, E) Single Tests\r\nVerbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.\r\nTARGET CATEGORY\r\nCommercial Staff in Sales, Marketing, Business Development &amp; Financial Services\r\n&nbsp;\r\nCustomer Aptitudes (V, N, E) Single Tests\r\nVerbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.\r\nTARGET CATEGORY\r\nCustomer Staff in Call Centres, Hospitality, Leisure, Health &amp; Education\r\n&nbsp;\r\nAdministrative Aptitudes (V, N, E) Single Tests\r\nVerbal Comprehension assesses the ability to comprehend written information. Numerical Comprehension assesses the ability to comprehend numerical data. Error Checking assesses the ability to check the correctness of transposed information.\r\nTARGET CATEGORY\r\nAdministrative Staff in Private &amp; Public Sector Offices\r\n&nbsp;\r\nSwift Apprentice Aptitude (V, N, E, S, M, D) Combination\r\nMeasures both technical and comprehension reasoning through short verbal (4 mins), numerical (4 mins), checking (90 secs), spatial (3 mins), mechanical (3 mins) and diagrammatic (4 mins) sub-tests. \r\nTARGET CATEGORY\r\nOperational, Technical, Engineering, Manufacturing &amp; Construction Apprentices \r\n&nbsp;\r\nSwift Technical Aptitude (S, M, D) Combination\r\nMeasures practical reasoning through short spatial (3 min), mechanical (3 min) and diagrammatic (4 min) sub-tests.\r\nTARGET CATEGORY\r\nTechnical Apprentices, Production, Construction, Engineering &amp; Scientific Staff e.g. production workers, apprentices, engineers, designers and scientists\r\n&nbsp;\r\nTechnical Aptitudes (S, M, D) Single Tests\r\nSpatial Reasoning assesses the ability to recognise shapes. Mechanical Reasoning assesses mechanical understanding through items that present a problem with a number of possible answers. Diagrammatic Reasoning assesses the ability to evaluate processes represented through diagrams.\r\nTARGET CATEGORY\r\nTechnical Apprentices, Production, Construction, Engineering &amp; Scientific Staff\r\n&nbsp;\r\nPractical Aptitudes (S, M, D) Single Tests\r\nSpatial Reasoning assesses the ability to recognise shapes. Mechanical Reasoning assesses mechanical understanding through items that present a problem with a number of possible answers. Diagrammatic Reasoning assesses the ability to evaluate processes represented through diagrams.\r\nTARGET CATEGORY\r\nProduction, Construction, Engineering &amp; Scientific Staff\r\n&nbsp;\r\n&nbsp;'),
(32, 'en', 'pages', '30', 'https://www.innovative-hr.com/assessment/ability-assessments#pearson', 'Pearson', 'PEARSON Ravens Standard Progressive Matrices (SPM)\r\nNon-verbal measure of mental ability, helping to identify individuals with advanced observation and clear thinking skills who can handle the complexity and ambiguity of the modern workplace. Ravens offers information about someone\'s capacity for analysing and solving problems, abstract reasoning, and the ability to learn &ndash; and reduces cultural bias with a nonverbal approach.\r\nTARGET CATEGORY\r\nSupervisory/entry level management positions and mid-level individual contributor positions\r\n&nbsp;\r\nPEARSON Ravens Advanced Progressive Matrices (APM)\r\nNon-verbal measure of mental ability, helping to identify individuals with advanced observation and clear thinking skills who can handle the complexity and ambiguity of the modern workplace. Ravens offers information about someone\'s capacity for analysing and solving problems, abstract reasoning, and the ability to learn &ndash; and reduces cultural bias with a nonverbal approach.\r\nTARGET CATEGORY\r\nSenior management positions and high level individual contributor positions \r\n&nbsp;'),
(33, 'en', 'pages', '31', 'https://www.innovative-hr.com/assessment/ability-assessments#psytech-general ', 'PSYTECH', 'PSYTECH General Reasoning Test Battery (GRT2)\r\nA comprehensive, detailed and accurate measure of mental ability. This test is designed to assess reasoning power for those of general ability.\r\nTARGET CATEGORY\r\nAll levels of staff\r\n&nbsp;\r\nPSYTECH Graduate Reasoning Test Battery (GRT1)\r\nA comprehensive and in-depth measure of mental capacity designed to assess high level reasoning ability.&nbsp;Measure of Verbal, Numerical and Abstract reasoning.\r\nTARGET CATEGORY\r\nManagers and Graduates&nbsp;\r\n&nbsp;\r\nPSYTECH Critical Reasoning Test Battery (CRTB2)\r\nProvides a detailed and accurate measure of critical reasoning (verbal and numerical). This test has been specifically designed for management assessment.\r\nTARGET CATEGORY\r\nManagers'),
(34, 'en', 'pages', '59', 'https://www.innovative-hr.com/assessment/situational-judgement', 'Situational Judgement Tests', 'Situational Judgement Tests (SJT) assesses judgment required for solving problems in work-related situations typically aligned to a specific role or job family. SJTs are used primarily as a way to sift multiple applications for a role and to provide a realistic job preview for prospective candidates. They are particularly helpful in attracting the best talent and are often used for graduate trainee programmes where they provide an opportunity for the organisation to &lsquo;show case&rsquo; their offering whilst simultaneously identifying candidates with the closest job fit.\r\nAn SJT presents candidates with hypothetical and challenging situations that employees might encounter at work which may involve working with others as part of a team, interacting with others, and dealing with workplace problems. In response to each situation, candidates are presented with several possible actions in a multiple choice format. Most commonly, candidates are asked to select the most effective response to the situation described. However, some tests may require candidates to list the responses in order of effectiveness.\r\nUnlike most psychological tests, we strongly recommend that SJTs are not bought \'off-the-shelf\', but are in fact designed specifically to suit individual job role requirements and the culture and practices of your organisation.\r\nThus we start by conducting a job analysis; reviewing Job Descriptions, observing performance in the workplace and conducting interviews. We typically design each test to consist of between 25 and 35 &lsquo;items&rsquo; or short descriptions of problem situations. Each description is usually followed by one, two or three questions and multiple possible responses which candidates are asked to rate.\r\nWe can present the SJTs in either a static form or a multi-media format which help organisations attract and select the best possible talent.'),
(35, 'en', 'pages', '45', 'https://www.innovative-hr.com/assessment/assessment-centre-exercises', 'Assessment Centre Exercises', 'Assessment Centre exercises are business simulations which are based on \'day in the life\' business problems and allow trained Assessors to assess leadership behaviours in action. Observing individuals evaluating business critical problems, taking decisions and communicating the way forward, provides us with an excellent opportunity to assess an individual&rsquo;s compatibility for a specific role or to determine their general leadership potential.\r\nThese exercises enable the candidate to demonstrate various skills, behaviours and attitudes to achieve an optimum outcome to the task at hand. As with real life, business simulation exercises are designed for multiple settings; working independently, working one-to-one and working within a group. The exercises are chosen on the basis of their relevance to the assessment criteria; level of seniority, work context and job-critical competencies. Our library holds over 250 different exercises including:\r\n\r\nAssigned/Non-assigned Group Discussions\r\nGroup Activities\r\nInternal/External Role Plays\r\nStructured Interviews\r\nAnalysis Exercises/Case Studies culminating in a Report and /or Presentation\r\nIn-Basket Exercise\r\nFact Find Exercise\r\n\r\nExercises are available to Certified Assessors who have been trained to observe and record candidate behaviour, classify and rate performance. We source from two of the international leaders in test publishing; A&amp;DC and Global Leader. We can guide you to choose the combination of exercises that is most suitable for your needs and train you to run your own Centres. Equally, we can manage the entire Assessment/Development Centre on your behalf.\r\nWant to become a certified Assessor and Centre Facilitator? Take the British Psychological Society recognised Assessor Skills and Centre Manager course with Innovative HR Solutions Today!\r\nAssessor Skills: Click here for Course Details\r\nCentre Manager: Click here for Course Details\r\nCheck out Global Leader Exercises: Click Here\r\n&nbsp;'),
(36, 'en', 'pages', '60', 'https://www.innovative-hr.com/assessment/360-surveys', '360 Surveys', ''),
(37, 'en', 'pages', '61', 'https://www.innovative-hr.com/assessment/360-surveys#overview', 'Overview', 'How you see yourself, and how others see you makes for an interesting development conversation. We have tried and tested 360 Survey tools both &lsquo;off the shelf&rsquo; where individuals are rated against an international competency model, and 360 surveys which are 100% customised to your Competency Framework. Unlike the traditional appraisal where a supervisor appraises the performance of their subordinate, 360 degree feedback incorporates multiple perspectives by using feedback from a variety of sources. These sources include self, line manager, peers, subordinates, and even key customers and suppliers if so desired. The majority of our tools allow for both a quantitative rating and qualitative comments to strengthen the developmental feedback process. 360 Surveys can be used to support training needs analysis, development planning, as well as part of the performance management framework.'),
(38, 'en', 'pages', '127', 'https://www.innovative-hr.com/assessment/360-surveys#innovative-360', 'Innovative', 'Innovative 360\r\nOur most popular 360 Survey.&nbsp; 100% customised to your Competency Framework.&nbsp; Ability to collect quantitative and qualitative feedback throughout. Ability to customise communication templates and rating scales to suit the culture and practices of your organisation.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nInnovative 360&nbsp;Report'),
(39, 'en', 'pages', '128', 'https://www.innovative-hr.com/assessment/360-surveys#performance-360', 'Performance 360', 'Performance 360\r\nProvides a multi-rater perspective against the Wave model of leadership. International normed and can be used for all levels.\r\nTARGET CATEGORY\r\nAll &ndash; up to Senior Executives \r\n&nbsp;\r\nContact us for a sample report!\r\n&nbsp;'),
(40, 'en', 'pages', '129', 'https://www.innovative-hr.com/assessment/360-surveys#eq-360', 'EQ', 'EQ 360\r\nProvides a multi-rater perspective on the emotional intelligence of the test taker factoring in the views of peers, managers, direct reports, and other groups as required.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nFeedback Standard Client Report\r\nFeedback Standard Coach&nbsp;Report'),
(41, 'en', 'pages', '131', 'https://www.innovative-hr.com/assessment/360-surveys#hogan-360', 'Hogan', 'Hogan 360\r\nThe Hogan 360&deg; is aligned with Hogan&rsquo;s core assessments. The Hogan 360&deg; provides a real-time look at an individual&rsquo;s attitude, behaviour, and performance. The report offers constructive feedback around leadership expectations and sets priorities for improvement.\r\nTARGET CATEGORY\r\nSenior Executives and Leaders\r\nDOWNLOADS\r\nHogan 360&nbsp;Report\r\n&nbsp;'),
(42, 'en', 'pages', '90', 'https://www.innovative-hr.com/assessment/360-surveys#neo-primary-colours', 'NEO', 'NEO Primary Colours 360\r\nThe Primary Colours Model has been developed from 25 years of academic research and industry consulting experience.&nbsp;The Primary Colours Leadership Report merges the in-depth personality profile of the NEO PI-R with the proven Primary Colours Model of leadership to create a clear, comprehensive narrative report on an individual\'s leadership capability.&nbsp;\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nPrimary Colours 360 Leadership Report'),
(43, 'en', 'pages', '67', 'https://www.innovative-hr.com/assessment/organisational-engagement-surveys', 'Organisational Engagement Surveys', 'Measuring an organisations culture with a view to enhancing organisational performance is one of the most difficult leadership challenges. Culture comprises an interlocking set of goals, roles, processes, values, communications, practices, attitudes and assumptions. The elements fit together as a mutually reinforcing system and combine to prevent any attempt to change it! That is why single-fix changes may appear to make progress for a while, but eventually the interlocking elements of the organisational culture take over.\r\nOur diagnostic phase is a multi-faceted approach which includes a Climate Survey, Leadership Interviews and Focus Groups, as well as uniquely, Leadership Profiling. Surveys, Focus Groups and Interviews are not new; their success rests on the questions being asked and the way they are communicated upfront and actioned upon final analysis.\r\nWhat is unique to our way of working is that we have incorporated into our proposition Leadership Profiling as part of the diagnostic phase. We believe it is in the day-to-day behaviour of leaders that most people are informed about the &lsquo;way things are done around here&rsquo;; leaders shape culture and that culture shapes leadership behaviour. Leaders are both architects and the product.\r\nThus our approach is a fully comprehensive diagnostic process, which culminated in thoughtful and detailed analysis and recommendations for the way forward.'),
(44, 'en', 'pages', '21', 'https://www.innovative-hr.com/assessment/cbi', 'CBi Smart', 'CBI-Smart&trade; is an online Competency-Based Interview question builder that enables interviewers to quickly create Competency-Based Interview Guides from a database of over 1,000 proven CBI questions, assessing 35 popular competencies.\r\nAlternatively, we can customise CBI-Smart&trade; to include your competencies and design the questions to your Competency Framework in English and Arabic at different levels.\r\nCBI-Smart&trade; ensures that all your Competency Based Interviews are structured, questions relate directly to the essential competencies that have been identified as key to effective performance in a given role. Structured interviews, such as CBIs, have been found to be more than twice as effective as unstructured interviews at predicting a candidate&rsquo;s performance.\r\nTo add to this, we offer a complete range of competency based interview solutions including training in competency design and interviewing skills.'),
(45, 'en', 'pages', '20', 'https://www.innovative-hr.com/assessment/specialised-assessment', 'Specialised Assessment', 'At times our scope of work is more specific and we need assessments for a particular purpose. Perhaps it is to support Graduates and Young Talent with their career choices. Or there could be a special challenge for the business such as a team that is in conflict. All the following tools have been selected on the basis of their validity, reliability, ease of use, cost efficiency and objectivity.'),
(46, 'en', 'pages', '68', 'https://www.innovative-hr.com/assessment/specialised-assessment/career-planning', 'Career Planning ', ''),
(47, 'en', 'pages', '70', 'https://www.innovative-hr.com/assessment/specialised-assessment/career-planning/myself', 'My Self', 'Career Guidance and Self-Development tool which measures an individual\'s potential talents across a range of areas in the workplace.\r\nTARGET CATEGORY\r\nFresh Graduates and Younger Talent\r\nContact us directly for a sample and demo.'),
(48, 'en', 'pages', '69', 'https://www.innovative-hr.com/assessments/specialised-assessment/career-planning/strong-interest-inventory', 'Strong Interest Inventory', 'Career planning tool for high school, college and University students&mdash;as well as Young Talent and people in transition to make fulfilling career choices.\r\nTARGET CATEGORY\r\nHigh&nbsp; school, college and University students &amp; people in career transition\r\nDOWNLOADS\r\nStrong Interest Inventory Report'),
(49, 'en', 'pages', '27', 'https://www.innovative-hr.com/assessment/specialised-assessment/interpersonal-relationships-and-conflict-management', 'Interpersonal Relationships & Conflict Management', ''),
(50, 'en', 'pages', '71', 'https://www.innovative-hr.com/assessment/specialised-assessment/interpersonal-relationships-and-conflict-management/firo-b', 'FIRO-B', 'Helps people understand their own behaviour and that of others in interpersonal situations.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nFiro-B Report'),
(51, 'en', 'pages', '72', 'https://www.innovative-hr.com/assessment/specialised-assessment/interpersonal-relationships-and-conflict-management/firo-business', 'FIRO-Business', 'Designed for use with clients who want to understand their interpersonal communication needs and gain greater insight into how those needs influence their work behaviours and leadership style\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nFiro Business Report'),
(52, 'en', 'pages', '73', 'https://www.innovative-hr.com/assessment/specialised-assessment/interpersonal-relationships-and-conflict-management/thomas-killman-conflict-mode-instrument-tki', 'Thomas Kilman Conflict Mode Instrument (TKI)', 'Helps individuals to learn to move beyond conflict and focus on achieving organisational goals and business objectives. Organisations can apply the TKI to such challenges as change management, team building, leadership development, stress management, negotiation, and communication.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nTKI Report'),
(53, 'en', 'pages', '74', 'https://www.innovative-hr.com/assessment/specialised-assessment/safety-in-the-workplace', 'Safety in the Workplace', ''),
(54, 'en', 'pages', '75', 'https://www.innovative-hr.com/assessment/specialised-assessment/safety-in-the-workplace/hogan-safety', 'Hogan Safety ', 'Accidents at work cause unnecessary stress and business expenses.&nbsp; Some people tend to engage in unsafe behaviour at work due to carelessness, recklessness, spite, and other reasons.\r\nHogan&rsquo;s Safety Report helps alleviate some of these concerns by identifying risks that individuals possess that may lead to on-the-job accidents and other unsafe behaviours.\r\nTARGET CATEGORY\r\nAll levels\r\nDOWNLOADS\r\nHogan Safety Report'),
(55, 'en', 'pages', '76', 'https://www.innovative-hr.com/assessment/specialised-assessment/innovation-and-change', 'Innovation and Change', ''),
(56, 'en', 'pages', '77', 'https://www.innovative-hr.com/assessment/specialised-assessment/innovation-and-change/me2', 'ME2 ', 'Workplace diagnostic tool which assesses an individual&rsquo;s creative strengths and style which can then be applied to whole teams or organisations and how they fit and work together creatively. Creativity underpins innovation and&nbsp;problem-solving.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nME2 Report'),
(57, 'en', 'pages', '78', 'https://www.innovative-hr.com/assessment/specialised-assessment/mental-toughness-and-resilience', 'Mental Toughness and Resilience ', ''),
(58, 'en', 'pages', '79', 'https://www.innovative-hr.com/assessment/specialised-assessment/mental-toughness-and-resilience/mtq-48', 'MTQ 48', 'Assess mental toughness in terms of the four key components; Control, Commitment, Challenge, Confidence to help someone change their mental toughness or how to adopt the behaviours that a mentally tough person would require.\r\nTARGET CATEGORY\r\nAll- Up to Senior Executives\r\nDOWNLOADS\r\nMTQ48 Report'),
(59, 'en', 'pages', '80', 'https://www.innovative-hr.com/assessment/specialised-assessment/workplace-language', 'Workplace Language ', ''),
(60, 'en', 'pages', '81', 'https://www.innovative-hr.com/assessment/specialised-assessment/workplace-language/workplace-english', ' Workplace English', 'Workplace English Tests assess an individual\'s understanding and use of English in the workplace. \r\nTARGET CATEGORY\r\nApplicable to a wide range of roles such as administrators, call centre personnel, sales assistants, hospitality staff and medical personnel.\r\n&nbsp;\r\nSample reports are available. Please get in touch with our team for more info!\r\n&nbsp;'),
(61, 'ar', 'pages', '13', 'https://middleeast.psionline.com/development', 'التطوير ', 'بعد القيام بتحديد متطلبات التطوير والتعليم في مؤسستك، تقوم شركة بي اس آي الشرق الأوسط&nbsp; بتقديم مجموعة من خيارات التعليم والتطوير من أجل تحسين أداء فرق العمل. نحن نسعى إلى إنتاج&nbsp;وتقديم أنشطة قوية وتطبيقية لمساعدة فرق العمل على التطور ومشاركة الالتزام بأهداف المؤسسة.'),
(62, 'en', 'pages', '94', 'https://www.innovative-hr.com/development/leadership-development', 'Leadership Development', 'The world is arguably more volatile, uncertain, complex and ambiguous than ever before. What has not changed however, is that leaders need to be able to develop a clear vision of the way forward, be able to communicate that vision effectively, adapt their approach to the needs of others, motivate others and drive results efficiently.\r\nDue to popular demand, we have developed a Leadership Development Programme which can be delivered in English or Arabic. The programme is built around Dave Ulrich, Norm Smallwood and Kate Sweetman&rsquo;s; &lsquo;The Leadership Code: Five Rules to Lead By&rsquo;. Ulrich and his team set about seeking to &ldquo;simplify and synthesise&rdquo; and leverage the work of established experts from across the leadership development field. In fact as they report, there are literally tens of thousands of leadership studies, theories, frameworks, models, and recommended best practices. Drawing on decades of research experience, the authors conducted extensive interviews with a variety of respected CEOs, academics, experienced executives, and seasoned consultants - and determined the same five essentials repeated again and again. These five rules became The Leadership Code.\r\nThe Innovative HR Solutions &lsquo;Mastering the Leadership Code&rsquo; is a 5-part modular programme interspersed with a pre and post assessment, Executive Coaching and psychometrics to deepen self-awareness. There is also the opportunity to customise the programme to bring learning into the workplace through on-the-job individual and / or team projects. We have designed two levels; First-Line to Mid-Managers and Senior Leaders. The programme can be flexed to accommodate time and budgetary requirements as well as the opportunity to co-create projects to bring value to your organisation.\r\nWe have also launched the 2 day ICF-recognised Coaching Programme for People Managers, a highly practical workshop based on the GROW model to enable Managers to empower their people.&nbsp;\r\nLastly based on client demand, we have designed The Power of Trust; a high impact Leadership Programme aimed at fostering high trust teams to produce great results.&nbsp;&nbsp;\r\nAccreditation and Workshop Catalogue\r\n&nbsp;'),
(63, 'en', 'pages', '117', 'https://www.innovative-hr.com/development/development-centres', 'Development Centres ', 'A Development Centre is intended to support leadership and talent development. We typically run Centres over 1 or 2 days, and they comprise of a series of business simulations (for example; Analysis Exercise, Presentation, Role Play, Group Discussion, Structured Interview, In-Tray or Fact Find ), and personality and ability psychometrics. We are also able to add to this workplace assessments such as a 360 survey or line manager feedback.\r\nThe emphasis of a Centre is to promote greater self-awareness around areas of strengths as well as areas for development. This is achieved on the day through a self-reflection exercise, and after the event, through a coaching-style Individual Feedback Session. In this session the observer or &lsquo;Assessor&rsquo; will provide evidence of what was said and done throughout the exercises, and will encourage the individual to consider the impact of the choices they made and how their performance could have been improved. Participants will be expected to &lsquo;own&rsquo; the development requirements as part of their continuous Professional Development and will be encouraged to form Individual Development Plans to capitalise on the insights gained.'),
(64, 'ar', 'pages', '95', 'https://middleeast.psionline.com/development/management-skills', 'مهارات الإدارة ', 'ان احد اهم التحولات التي تحدث في مساراتنا الوظيفية هي حين ننتقل من ادارة النفس الى ادارة الاخرين. في هذه المرحلة الانتقالية من المهم جداً ان نجهز المدراء بمهارات القيادة الضرورية ليكونوا فعالين في ادوارهم. نقوم بطرح البرامج التالية كدورات فردية او كمجموعة من المساقات.');
INSERT INTO `search_index` (`id`, `locale`, `type`, `type_id`, `url`, `title`, `content`) VALUES
(65, 'en', 'pages', '96', 'https://www.innovative-hr.com/development/management-skills/performance-management-skills', 'Performance Management Skills', 'A bad boss or supervisor is the number one reason people leave their employers. This could be due to the environment their boss creates and how they feel they have been treated, whether they feel listened to, are provided with support and the opportunity to grow and develop in their role. This module will cover elements such as:\r\n\r\nThe importance of performance management\r\nSetting SMART goals\r\nReviewing performance throughout the year\r\nProviding feedback\r\n\r\nAs with all our programmes we aim to provide a highly experiential learning opportunity with group exercise, role play, group discussion and individual reflection. We are able to conduct follow up performance management business simulation sessions to embed learning.'),
(66, 'en', 'pages', '109', 'https://www.innovative-hr.com/development/management-skills/motivating-and-engaging-others', 'Motivating & Engaging Others ', 'As a recent Harvard Business Review article states; &ldquo;If your job involves leading others, the implications are clear: the most important thing you can do each day is to help your team members experience progress at meaningful work. To do so, you must understand what drives each person.&rdquo; Put simply, we should not leave the art of motivating and engaging others to chance! We leverage the work of Daniel Pink and others to explore strategies in a way which is practical and insightful.\r\nThen taking the theory to the next level we have the option to use Myers Briggs Type Indicator (MBTI) and we&nbsp;start to explore different personality and how that impacts each person in the day-to-day work setting. How we take in information around us, make decisions, communicate, structure tasks as individuals and teams, all plays an intrinsic role in our levels of satisfaction and motivation at work. It is important for individuals to find their own meaning in work, to set realistic and yet challenging goals to support their own intrinsic motivation whilst meeting corporate goals and objectives.'),
(67, 'en', 'pages', '110', 'https://www.innovative-hr.com/development/management-skills/competency-based-interviewing', 'Competency Based Interviewing ', 'We know the direct and indirect cost of poor recruitment when we hire the wrong person and equally that a poor candidate experience can impact our brand. Line Managers play a vital role in hiring decisions and the way in which we present our business to prospective talent. When interviewing candidates it is important to assess a candidates&rsquo; behavioural competence as well as their technical capabilities. Our workshops are designed to provide delegates with the skills and best practices essential to conducting competency based interviews. Our workshops are highly interactive; we build the skills and competence of individuals through role play, observation and reflection. Workshops can be customised to align with your Competency Framework and Job Requisition process and forms (if applicable).\r\nWe can design interactive and engaging workshops to enable attendees to:\r\n\r\nUnderstand a competency based interview model and the advantages of questioning and probing competencies.\r\nUnderstand what type of information can be gathered and evaluated in an interview and how evidence can be rated according to the behavioural competencies being assessed at different levels of the organisation.\r\nLearn how to observe, record, classify and evaluate (ORCE) evidence.\r\nLearn how to structure an interview for best results. Developing competency-based questions and questioning techniques.\r\nTake part in practical interviewing skills session (to candidate / to hiring decision makers) and receive feedback from our qualified facilitator.\r\n'),
(68, 'en', 'pages', '111', 'https://www.innovative-hr.com/development/management-skills/coaching-skills-for-line-managers', 'Coaching Skills for Line Managers ', 'Coaching Skills are intrinsic to establishing a high performance culture. To help individuals understand their role in reaching personal, team and corporate goals individuals require an understanding of how they can contribute clear communication around goals and expectations and timely feedback regarding their progress. According to Harvard Business School\'s recent research, &ldquo;the single most important managerial competency that separates highly effective managers from average ones is coaching&rdquo;. Our programmes includes;\r\n\r\nWhat is Coaching and why is it important?\r\nEssential coaching principles and tools.\r\nRole of Manager in development.\r\nCoaching technique including the GROW model of coaching.\r\nAdvanced listening and questioning techniques.\r\nAction planning.\r\nHow to provide development feedback.\r\n\r\nWant to become a certified Executive Coach? Take the AFC Executive Coaching Certification&nbsp;with Innovative HR Solutions today!\r\nAFC Executive Coaching Certification: Click here for&nbsp;Course Details'),
(69, 'en', 'pages', '112', 'https://www.innovative-hr.com/development/management-skills/managing-change', 'Managing Change ', 'Change is the only constant in this fast paced world, and no more so than in the Middle East region. Enabling change in the workplace to take advantage of opportunities as well as mitigate risks is now simply put, a &lsquo;101&rsquo; management skill.\r\nOur programmes are designed to provide insight into the change curve as we look to Kotter and others to understand the principles of change. Delegates will gain an understanding of managing their own emotional state during times of uncertainty and how different people experience change differently and have different stressors and coping strategies. We will examine the need for different forms of communication, holding true to vision and building strategies to build interim and long term plans in order for long term business goals to be&nbsp;met.'),
(70, 'en', 'pages', '113', 'https://www.innovative-hr.com/development/management-skills/resilence-and-emotional-intelligence', 'Resilience and Emotional Intelligence ', 'We are now operating in a more volatile, uncertain, complex and ambiguous context than ever before. People fear change but they need it too. Change brings pressure. Pressure brings performance. But what happens when pressure outgrows the ability to perform? How do your employees maintain drive, focus and productivity when stretched beyond their means?\r\nOne of the five core predictors of potential - resilience - is at the heart of our approach to coaching and development. Developing strategies to adjust to changing situations and higher levels of pressure is critical for today\'s successful leader and employee.\r\nOur work with individuals and teams has led to the creation of our Resilience Development programme which will enable your employees to thrive, rather than cope, under pressure.'),
(71, 'en', 'pages', '114', 'https://www.innovative-hr.com/development/management-skills/managing-conflict', 'Managing Conflict ', 'Conflict in the workplace, while it is not desirable, is an inevitable element of the workplace. Colleagues with varying ideas who have different views may go head to head causing disruption in the workplace, subsequently impacting on productivity and workplace culture. Our workshop will enable delegates to learn approaches on how to diffuse potentially damaging conflict situations using the Thomas Kilman Conflict Mode Instrument (TKI).\r\nThe TKI is the world&rsquo;s best-selling tool for helping people understand how different conflict-handling styles affect interpersonal and group dynamics&mdash;and for empowering them to choose the appropriate style for any situation. The TKI can be used effectively within an organisation to facilitate and improve team dynamics for development, leadership and coaching and subsequently enhance communication and improve productivity.'),
(72, 'en', 'pages', '115', 'https://www.innovative-hr.com/development/team-facilitation', 'Team Facilitation ', 'Psychologist Bruce Tuckman first came up with the memorable model \"forming, storming, norming, and performing\" in his 1965 article, \"Developmental Sequence in Small Groups.\" He used it to describe the path that most teams follow on their way to high performance. Unlocking the full potential of teams is critical to organisation performance. We have designed and implemented a range of powerful team development solutions and interventions aimed at increasing collective awareness of team success factors and enhancing the quality of communication and collaboration. We work with Boards, senior management teams as well as strategic business units who are attempting to raise their game, which are in the process of forming and storming, or undergoing change or indeed those that are in conflict. It is important to work together to achieve shared goals, resolve differences and appreciate others&rsquo; strengths.'),
(73, 'en', 'pages', '116', 'https://www.innovative-hr.com/development/executive-coaching-and-mentoring', 'Executive Coaching & Mentoring ', 'Executive Coaching\r\nExecutive Coaching is a dynamic, motivating and interactive relationship with a qualified and experienced Coach, assisting an individual or Coachee, to enhance their performance. It is an important component of a talent management strategy for high potential individuals and leaders, supporting individuals to continue to grow, learn and develop. During the coaching session we focus on enhancing overall performance whilst further developing behaviours and skills needed for success in their current, new or future role.&nbsp; Coaching provides a road map for behavioural change, offering fresh perspectives on personal and business challenges. The Coach assists and facilitates individuals to identify barriers to performance, whether linked to their own chosen behaviours and how they are perceived by others, or to current skill levels, knowledge and experience. Coaching assists the individual to increase their own awareness of what they need to do in order to become more effective, and to take responsibility for implementing the actions identified in order to arrive at solutions and realise success. Overall it leads to enhanced decision-making skills, greater interpersonal effectiveness, increased confidence, improved productivity, satisfaction with life and work and attainment goals.\r\nMentoring\r\nMentoring is a relationship between two individuals based on a mutual desire for development towards career goals and objectives. &nbsp;Whilst both a Mentor and a Coach assume responsibility for the process during the development journey, the Mentor has a slightly different role in that they also assume a responsibility for content and knowledge transfer, whilst the coaching process is more facilitative in nature.&nbsp; The Mentor is therefore usually more experienced and qualified than the mentee and is more directive in style than a coach.&nbsp; The relationship is a non-reporting one and replaces none of the organisational structures in place.&nbsp; We can work with organisations to set up a Mentoring Framework in-house, identify in-house mentors and provide training to embed the process.&nbsp; Or we can provide Mentors to work with key individuals to expedite their development. &nbsp;&nbsp;&nbsp;'),
(74, 'en', 'pages', '118', 'https://www.innovative-hr.com/development/individual-development-plans', 'Individual Development Plans ', 'An Individual Development Plan (IDP) is a structured process which&nbsp;enables an individual to create an individual plan to realise their development objectives. IDP&rsquo;s are specific to the individual and tailored to employees&rsquo; job function and department and align to the organisation&rsquo;s vision, mission and values. IDP&rsquo;s focus on enhancing an individual&rsquo;s skills to ensure effectiveness within their current job role and assist in developing the skills required for future roles. The key towards the success of an IDP is that the employee has ownership of the IDP, and receives support from their manager in implementing the plan. IDP&rsquo;s can be created following&nbsp;a different diagnostic process. For instance, they can follow on from a Development Centre, a performance conversation, a psychometric tools process or 360&deg; feedback. The creation of an IDP is a long-term investment and a cornerstone of any talent management process which motivates staff and conveys a clear organisational message that development is a priority.'),
(75, 'ar', 'pages', '14', 'https://middleeast.psionline.com/accreditation-programme', 'البرامج المعتمدة ', 'نحن مؤهلون ومرخصون لتقديم عدد من البرامج العالمية المعتمدة للمساعدة في نقل خدمات التقييم والتطوير إلى داخل مؤسستك. نفخر بتمثيلنا لـ ١٢ ناشراً للاختبارات العالمية في منطقة الخليج من أجل توفير مجموعة من البرامج والمنتجات عالمية المستوى. إن جميع المدربين العاملين لدينا معتمدين ومن ذوي الخبرات العالية في تقديم البرامج كما يخضعون جميعاً لمراجعة دورية لمستوى الجودة.'),
(76, 'en', 'pages', '97', 'https://www.innovative-hr.com/calendar/course/4/mbti-certificate-program', 'Myers-Briggs Type Indicator® (MBTI®)', 'The Myers-Briggs Type Indicator&reg; (MBTI&reg;) instrument continues to be the most trusted and widely used assessment in the world for understanding individual differences and uncovering new ways to work and interact with others.\r\nIt is used exclusively in the development setting to improve individual and team performance, develop and retain top talent and leaders lending depth to the coaching experience, improve communication and reduce conflict in teams as well as explore the world of work and careers.\r\nThose who successfully complete the course will be accredited to administer, interpret and feedback the MBTI instrument (Step I &amp; II). You will also be able to join the Middle East MBTI Forum which is a free bi-annual Professional Development Breakfast for MBTI practitioners to share experiences and continue to develop your skills further.\r\nThe course comprises 10 hours pre-course work (reading and online assessment), home work and daily multiple choice exams.\r\nClick here for Course Details&nbsp;and the Registration Form'),
(77, 'en', 'pages', '98', 'https://www.innovative-hr.com/calendar/course/6/bps-qualification-in-occupation-testing', 'BPS Qualification in Occupational Testing (Ability & Personality) ', 'Formerly known as the BPS Level A&amp;B this 4-day course qualifies delegates to use a wide variety of ability tests and personality questionnaires from a number of major test publishers including in this case, the Saville Consulting products.\r\nThe qualification is internationally recognised demanding best practice in the use of psychometrics at work, and is suitable for those involved in selection, development or any other application where psychometric assessments are useful. Upon successful completion of the course delegates will be able to administer, analyse and feedback the Saville Wave selection of personality and ability tools, and apply to the BPS for Accreditation in Occupational Testing. The Accreditation then enables delegates to use a number of further products from other test publishers.\r\nThere is 10 hours pre-course work required, two open book exams during the 4-day course, and post course work (13 Assignments in total, including written reports and feedback sessions) should you wish to apply to the BPS upon completion. Included in our pricing is one-to-one support post course to mark and make recommendations to support a successful BPS submission.\r\nClick here for Course Details&nbsp;and Registration Form\r\n&nbsp;\r\nWhy British Psychological Society\'s (BPS)? http://www.bps.org.uk/what-we-do/bps/bps\r\nBritish Psychological Society Assistant Test User\r\nThe British Psychological society\'s (BPS) Qualification in Occupational testing - Assistant test User is a 1-day course which allows those qualified to administer Aptitude and Ability Tests from a number of major test publishers including Saville Consulting.\r\nIt is principally for administrators wishing to administer tests and handle test results it does not qualify delegates to interpret or feedback results.\r\nClick here for Course Details and Registration Form'),
(78, 'en', 'pages', '99', 'https://www.innovative-hr.com/calendar/course/8/eqi-20-and-eq360', 'EQi 2.0 & EQ360 ', 'The EQi is particularly useful for development activity, and in certain circumstances for selection. Emotional Intelligence has been identified as one the leading predictors of leadership performance and success, and is helpful in identifying how we can understand and manage ourselves and our relationships with others.\r\nThe EQi 2-day Accreditation Course enables those who complete, pass the post online assessment and conduct an observed feedback session, to become certified to use the EQi-2.0 model of Emotional Intelligence tools; both individual reports and 360 surveys.\r\nClick here for Course Details&nbsp;and the Registration Form'),
(79, 'en', 'pages', '100', 'https://www.innovative-hr.com/calendar/course/10/strong-interest-inventory', 'Strong Interest Inventory ', 'The Strong Interest Inventory instrument is the most widely used and respected career development instrument in the world. It has guided thousands of individuals, from high school and college students to mid-career workers seeking a change in their search for a rich and fulfilling career. It is a powerful tool for anyone considering a career change, starting a new career, looking for career enrichment, or a better work life balance. The Strong Interest Inventory generates an in depth assessment of an individual&rsquo;s interests in a broad range of occupations, work and leisure activities, and educational subjects. The tool is the gold standard for career exploration and development, providing time-tested and research-validated insights that foster successful career counselling relationships. The Strong can be used in conjunction with other reports such as the Myers Briggs (MBTI) for a detailed look at personality and career.\r\nThe Strong Interest Inventory&reg; is ideal for:\r\n\r\nChoosing a college major\r\nCareer exploration\r\nCareer development\r\nRe-integration\r\n\r\nThere is some pre-course work (online assessment)\r\nClick here for Course Details&nbsp;and the Registration Form'),
(80, 'en', 'pages', '101', 'https://www.innovative-hr.com/calendar/course/11/firo-business', 'FIRO Business ', 'The FIRO-Business&reg; (Fundamental Interpersonal Relations Orientation - Business) instrument helps individuals understand their need for inclusion, control and affection and how it can shape their behaviour and interactions with others at work or in their personal life. As an integral part of your leadership and coaching, team building and conflict management initiatives, the FIRO-B&reg; assessment can be used in a variety of settings and in combination with other solutions to improve organisational performance. It provides the skills to administer and interpret the assessment competently and ethically.\r\nThere is some pre-course work (online assessment) and homework.\r\nClick here for Course Details&nbsp;and the Registration Form'),
(81, 'en', 'pages', '136', 'https://www.innovative-hr.com/calendar/course/17/assessor-skills ', 'Assessor Skills', ''),
(82, 'en', 'pages', '103', 'https://www.innovative-hr.com/calendar/course/16/centre-manager', 'Centre Manager ', 'This British Psychological Society-accredited 2-day Workshop enables delegates to manage the process of designing and facilitating Assessment and Development Centres. The 2-day Assessor Skills Course is a pre-requisite to attending this course.\r\nThe course enables delegates to develop the knowledge and skills needed to become a qualified Centre Manager in a range of typical Assessment Centre events. You will be shown how to select exercises and design a matrix for an assessment event to gain a robust measure of the behaviours at the level in question. It will also instruct in the art of running an efficient and robust post centre &lsquo;wash-up&rsquo; session and guide consistency and quality throughout an event.\r\nThere is some homework and a multiple choice exam.\r\nClick here for Course Details&nbsp;and the Registration Form'),
(83, 'en', 'pages', '104', 'https://www.innovative-hr.com/calendar/course/13/afc-executive-coaching-certification', 'Accredited Award in Coach Training (AACT)', 'A fast paced, highly interactive workshop recognised by the Association of Coaching UK, introduces the practical techniques to coach individuals for professional development. Real life case studies, group discussions, personal feedback and continuous practice are all used to build the framework for professional coaching skills. The workshop particularly focuses on the GROW model.\r\nThe workshop is designed to create an assured coach, confident to tackle work and behavioural issues and help their coachees achieve set goals. This includes:\r\nUnderstand the purpose, ethics and power of Coaching\r\n\r\nGaining commitment in times of change\r\nTapping into people&rsquo;s potential and their desire to succeed and improve\r\nSolving technical and organisational problems\r\nDealing with difficult people and dramatically enhance their own and other&rsquo;s performance\r\n\r\nInnovative HR Solutions has aligned this programme with the Association for Coaching (UK) and the standards they set to help participants gain Associate and Member Level Membership with the Association following the workshop.\r\nThere is some homework and post-course work &ndash; practical and theoretical work.\r\nClick here for Course Details&nbsp;and the Registration Form'),
(84, 'en', 'pages', '133', 'https://www.innovative-hr.com/calendar/course/15/neo-conversion', 'NEO Conversion', ''),
(85, 'en', 'pages', '106', 'https://www.innovative-hr.com/calendar/course/14/thomas-kilman-conflict-mode-instrument-tki-workshop', 'Thomas Kilman Conflict Mode Instrument (TKI) Workshop', 'The Thomas Kilman Conflict Mode Instrument (TKI) is the world&rsquo;s best-selling tool for helping people understand how different conflict-handling styles affect interpersonal and group dynamics&mdash;and for empowering them to choose the appropriate style for any situation.\r\nDelegates will learn how to apply the TKI tool to assess an individual&rsquo;s typical behaviour in conflict situations along two dimensions: assertiveness and cooperativeness and how individuals can effectively use five different conflict-handling modes, or styles.\r\nThere is some pre-course work (online assessment).\r\nClick here for Course Details&nbsp;and the Registration Form'),
(86, 'en', 'pages', '137', 'https://www.innovative-hr.com/saville-conversion-online', 'Online Wave Conversion , Saville Assessment', 'This course is designed for those who hold the British Psychological Society Qualification in Occupational Testing Ability and Personality (formerly Level A &amp; P) and wish to start using Wave, Saville Assessment personality tools. The British Psychological Society Qualification in Occupational Testing Ability and Personality is a pre-requisite for this course. The course certifies delegates to achieve proficiency in interpreting and feeding back the Wave suite of personality tools. It also enables delegates to understand how to apply the Wave in selection and development. The&nbsp; Wave model combines personality, motivation and preferred culture and is regarded as the next generation in personality assessment. In order to qualify for this course you must present your current A&amp;P certificates to our team.\r\nContact Us and register your interest!'),
(87, 'en', 'pages', '15', 'https://middleeast.psionline.com/careers', 'Careers with PSI Middle East', 'Welcome to PSI Middle East Careers! The quality of our people is a key driver and contributor to our commercial performance and business success! We believe it is the quality, enthusiasm and commitment of our people that gives us our leading edge. We are committed to the ongoing personal and professional development of all our staff.\r\nAre you passionate about helping organisations across the GCC realise the power of their people through customised, expert talent management solutions? If yes, then take a look at our current vacancies and if there&rsquo;s nothing here that matches your skill set right now, do come back and visit us regularly to see if new opportunities have arisen.\r\nWe are all committed to our Vision and Mission and living by our Values. We value Team, Integrity, Customer Focus and Innovation.\r\nTeam: We value each other, we work collaboratively and never forget to have fun\r\nIntegrity: We work with integrity, we are transparent, ethical and respectful\r\nCustomer Focus: Our clients can expect us to be responsive, flexible, proactive, focused on quality and passionate about what we do\r\nInnovation: We value creativity, being expert and informed and thoughtful about our work'),
(88, 'en', 'pages', '16', 'https://www.innovative-hr.com/legal', 'Legal', 'Terms of Use\r\nThe purpose of this website is to publicise the various products and services provided to all customers of&nbsp;PSI Middle East.\r\nThe information contained on this website and its sub-sites are for general information purposes only. The information is provided by PSI Middle East and while PSI Middle East continually strive to keep the information up to date and correct, PSI Middle East makes no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.\r\nIn no event will PSI Middle East be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of or in connection with the use of this website.\r\nThrough this website you are able to link to other websites which are not under the control of PSI Middle East. PSI Middle East have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorsement of the views expressed within them.\r\nEvery effort is made to keep the website up and running smoothly. However, PSI Middle East takes no responsibility for and will not be liable for the website being temporarily unavailable due to technical issues beyond PSI Middle East control.\r\nPSI Middle East is committed to respecting your privacy and protecting your personal information. We recognise our obligation to keep sensitive information secure and have created this statement to share and explain the current information management practices on our websites. The handling of all personal information by&nbsp;PSI Middle East is governed by the UAE and Dubai privacy and security acts.\r\nPrivacy Policy\r\nWe are committed to protecting your privacy whether you are browsing for information or conducting business electronically.\r\nCollection and use of online information\r\nWhen you visit our websites, we will not collect your personal information unless you choose to use and receive online products and services that require it. All data capture uses up-to-date security mechanisms to ensure the integrity and confidentiality of information and systems.\r\nCollection of IP address\r\nOur web server(s) may automatically collects your IP address when you visit our websites (your IP address is your computer\'s unique address that lets other computers attached to the Internet know where to send data, but does not identify you individually). We use your IP address to help diagnose problems with our server and to compile statistics on site usage.\r\nUse of cookies\r\nPSI Middle East websites may use cookies. Cookies are generally used to make it more convenient for users to move around our websites. If you choose to, they may be used to \'remember\' your password and make it easier and faster to log-in to certain sites. These types of cookies need to be stored on your computer\'s hard drive.\r\nProtection of personal information\r\nThe personal information you enter on our websites is only available to the authorised PSI Middle East employees who have a need to know it. It will not be available for public inspection without your consent. Also, no website user-information will be shared, sold, or transferred to any third party without your prior consent.\r\nImportant notice\r\nSome of our websites link to other sites created and maintained by other public and/or private sector organizations. We provide these links solely for your information and convenience. When you link to an outside website, you are leaving the PSI Middle East website and our information management policies no longer apply.\r\nThird Party Website Disclaimer\r\nPSI Middle East website may periodically provide links to third-party websites (Third-Party Sites). This Agreement governs this Site only and not any Third-Party Sites. Our decision to provide a link to a Third-Party Site is not an endorsement of the content or services in that linked Third-Party Site. PSI Middle East does not operate or control any Third-Party Sites or any information contained therein. PSI Middle East makes no representation or warranty as to the availability or accuracy on information contained in any Third-Party Site and expressly disclaims any responsibility for the content and the accuracy of the information provided in any Third Party Sites. You agree to hold PSI Middle East harmless and absolve PSI Middle East from any and all liability, losses, damages and/or expenses incurred in connection with your use of any Third Party Site. You further agree that PSI Middle East shall not be liable or responsible to you in any way for any products or services available on any Third-Party Sites or any transaction carried out on or via a Third-Party Site. If you decide to access linked Third-Party Sites, you do so at your own risk. You agree that PSI Middle East is not liable for any information obtained, viruses or malware, operational failures, interruptions of service or connection failures experienced due to your use of a Third-Party Site. You should direct any concerns regarding any Third-Party Sites to the administrator of the applicable Third-Party Site. You expressly acknowledge that the security and privacy policies of any Third-Party Site may be different to PSI Middle East policies. You should refer to the separate terms of use, privacy policies and other rules posted on Third-Party Sites before you use any Third-Party Site.\r\nExternal links from PSI Middle East Website\r\nA \"link\" from the PSI Middle East website may be defined as:\r\n- Hyperlinks activated by clicking on an image or text, leading to a web page that does not reside on the Innovative HR Solutions (IHS) website \"explicit\"\r\n- Mention of URL address providing users with the address of a web page that does not reside on the Innovative HR Solutions (IHS) website \"implicit\"\r\nExternal links To PSI Middle East website\r\nA site that links to the&nbsp;PSI Middle East website should not misrepresent its relationship with PSI Middle East. This means:\r\n- A browser or border environment should not be created around PSI Middle East content\r\n- The PSI Middle East logo should not be used without the permission of PSI Middle East\r\n- The site should not present false/inaccurate information about PSI Middle East services\r\nGoverning Law and Jurisdiction\r\nAll matters regarding the use of this website and or any related matter shall be governed by and construed in accordance with the laws and regulations of, and applicable in, the emirate of Dubai and federal laws of the United Arab Emirates.\r\nThe courts of Dubai will have exclusive jurisdiction over any claim arising from the use of this website and or any related matter, or related to, a visit to the website but we retain the right to bring proceedings against you for breach of the use of the website in your country of residence or any other relevant country.'),
(89, 'en', 'pages', '39', 'https://www.innovative-hr.com/saville-myself', ' Myself', 'Career planning tool for high school, college and University student as well as Young Talent and people in transition to make fulfilling career choices\r\nTARGET CATEGORY\r\nFresh Graduates and Younger Talent'),
(90, 'en', 'pages', '120', 'https://www.innovative-hr.com/not-found', 'Page Not Found', 'The Page you are looking for is not found.'),
(91, 'en', 'pages', '141', 'https://www.innovative-hr.com/general-error', 'Something went wrong', 'Whoops! Looks like something went wrong. Please try again later or contact us on +97143902778.'),
(92, 'en', 'pages', '142', 'https://www.innovative-hr.com/search-result', 'Search Result', ''),
(93, 'en', 'news & events', '1', 'http://innovative-hr.com/knowledge-centre/news-events/article/1/can-resilience-be-taught-in-the-workplace', 'Can \'Resilience\' be taught in the workplace?', 'Humaira Anwer\r\n\"The only constant is change\" they say and in today\'s modern workplace, a truer phrase could not be spoken. In these uncertain times, challenges are ever-growing within organisations; increasing healthcare costs for employees on long term illness, high turnover and training costs, developing a \'can-do\' attitude among staff instead of reluctant \'that\'s-not-my-job\" culture and the list goes on. Now more than ever it\'s becoming evident just how important \'Resilience\' is within the workplace.\r\nBeing resilient does not mean that a person does not experience stress or difficulty, rather they have the ability to adapt when faced with tough challenges, sustain successful performance and positive well-being, and importantly, recover from or adjust easily to misfortune or change.\r\nAll is not lost- resilience can be learned and support easily set up within the organisation. Humaira Anwer, a Senior Organisational Psychologist from Innovative HR Solutions recently ran a workshop for clients on this very topic. The workshop has been designed using the Robertson Cooper Model of Resilience which suggests 4 elements which help build resilience; adaptability, social support, confidence and purposefulness.\r\n\r\nThis theory also explores the easy steps that can be taken by organisations to teach resilience and to support their staff- for employees to turn the negative into positives and look for the win-win solution, to proactively seek the opportunities, to widen their circle of influence and take better care of themselves.\r\nHow resilient are you? Take the questionnaire now to find out:\r\nFree&nbsp;Resilience Report\r\nFor more information or to attend future workshops at Innovative HR Solutions contact&nbsp;us today!'),
(94, 'en', 'news & events', '2', 'http://innovative-hr.com/knowledge-centre/news-events/article/2/unlock-talent-potential-with-coaching', 'Unlock Talent Potential with Coaching!', 'Liesl Du Plessis&nbsp;\r\n\r\nCoaching has become the leadership development phenomena of the century. Business leaders, around the world, acknowledge its unique power to unlock talent potential.\r\nExtensive research has been conducted in this field, validating the personal, professional and organisational benefits of coaching. In this day in age, the question is no longer: \"Is coaching beneficial?\" But rather;\r\n\"How do I ensure that the positive impact of coaching penetrates every area of the organisation?\"\r\nBy addressing this conundrum, the modern day organisation aims to ensure that no untapped potential is lost and that paradigm shifts are encouraged for every business challenge which employees may face.\r\nThe mobilisation of this cultural shift necessitates three primary drivers of change:\r\n\r\nKey Stakeholder buy-in - Widespread Executive support, acting as a catalyst for critical changes through vehicles such as corporate strategy.\r\n\r\n\r\nA strong Mobilisation Team - Change Leaders from various disciplines and levels within the organisation, are empowered to drive the desired change.\r\n\r\n\r\nShift in Leadership culture - A coaching leadership style is embraced. Business leaders practice and openly enable formal and informal coaching initiatives, thus initiating a shift in the organisational culture. The successful shift in leadership style relies heavily on exposure to coaching and the benefits thereof.\r\n\r\nIn accordance with John Kotter\'s Change philosophy, the momentum of change is only sustainable if one endeavours to embed it using both quick wins (where visible improvements are evident) and large scale project management, which aims to enable action over the long-term. (where organisations systematically chip away at bigger initiatives).\r\nThe selection of the most appropriate change interventions depends primarily on organisational context and may include initiatives such as:\r\n\r\nBuilding coaching capacity by expanding their pool of internal and/or external coaches.\r\nEmbedding coaching in formal processes using the coaching strategy as a springboard.\r\nUpskilling the coaching skills of the workforce through various training programmes.\r\nExploring creative avenues to embed coaching in a non-conventional manner.\r\n\r\nOne of the most vital elements of this cultural change journey is to ensure progress is monitored and measured throughout and that corrective action is taken as required, thus ensuring continuous development and accurate record of success metrics.\r\nIt may therefore be concluded that organisations\' aspiration for a coaching culture may be fulfilled using a broad range of methodologies and approaches, which tailors best to their context and unique circumstances.\r\nWhilst the road map to create a coaching culture may vary, most organisations report that it tends to yield insurmountable business results with a high ROI.\r\nWould you like to know how Innovative HR Solutions can help you unlock talent potential within your organisation? Contact us today!'),
(95, 'en', 'news & events', '3', 'http://innovative-hr.com/knowledge-centre/news-events/article/3/what-is-talent-management', 'What is Talent Management? ', 'Amanda White\r\nGreat question! This article asks; what is talent management? What do leading academics say? What do some of the leading international businesses here in the region do about it? Hopefully in the process, it will give you some great ideas to take back to your workforce.\r\nSince McKinsey coined the expression \'The War for Talent&rsquo; in 1997; the term \'Talent Management\' has become increasingly common in the world of human resources. However in preparing for this exploration it is increasingly clear that both in academia and in the real world &lsquo;talent management&rsquo; lacks consistent definition. What is certain however is that talent management is here to stay and considered vitally important to corporate performance and competitiveness. In a an article published by the Chartered Institute of Personnel and Development (CIPD) in 2009 &lsquo;Fighting Back Through Talent Management&rsquo; CIPD refer to talent management as &lsquo;engaging, developing and retaining talent&rsquo; and suggest that in &ldquo;these financially challenging times, organisations have increased their focus on talent to include amongst other things; employer brand and profile and keeping talent warm for the future&rdquo;.\r\nIs Talent Management simply another name for Succession Planning?\r\nSuccession planning is a critical piece of the puzzle for sure. When designing any corporate strategy, it is important to have the end game in mind. The essence of succession planning is the identification of an organisation&rsquo;s critical job roles now and in the future, as well as the career path, associated competencies, background and experience required to fill those roles. This then in turn informs both the recruitment and development strategies an organisation might choose to put in place to ensure critical positions are covered.\r\nIndeed succession planning is particularly vital in the UAE and wider Gulf as knowledge transfer becomes increasingly an organization&rsquo;s competitive advantage. Given the local workforce dynamics, demographic and recent re-structuring this will inevitably decrease the capacity of any organisation&rsquo;s decision-making, efficiency, productivity, innovation and growth and so succession planning is essential in the battle for competitiveness.\r\nWho is Talented?\r\nWhilst various academics such as Vorhauser-Smith (2001), Morton (2005), Garman and Glawe (2004) and others advocate the concept that &lsquo;talented&rsquo; people are not all employees but perhaps a select few, I fall on the side of Talent Management is for everyone. And I am not alone.\r\nFollowing up on their celebrated War for Talent, McKinsey acknowledge that 10 years ago they emphasized the recruitment and retention of a company&rsquo;s top 20% or so. 10 years later in &lsquo;Making Talent a Strategic Priority&rsquo; (2008), McKinsey states; &lsquo;the impact of the top talent on corporate performance hasn&rsquo;t diminished, but what&rsquo;s much clearer today&hellip;.is that organisations cannot afford to neglect the contributions of other employees&rsquo;. Indeed there is now industry evidence to suggest that exclusive focus on top players can damage morale of the rest of the organisation and as a result potentially impact the overall performance of the business and that a mix of people who are all motivated is a more effective environment to groom talent. See Subramaniam and Youndt (2005).\r\nAnd the industry experts agree. I undertook a study of the hospitality sector at the end of 2011 and interviewed 10 international hospitality groups with significant operations here in the Gulf. All confirmed that all employees were in scope of their talent management strategy. All reported talent management was an increasing force to be reckoned with and that it will have a determining impact on the way their businesses think, act manage and compete in the future. Further all of the participants believed the downturn had accelerated a shift toward a greater corporate focus on talent especially given recent budgetary restraints and the significant growth planned in their sector in the near to medium term.\r\nWhy is Talent Management Important?\r\nTalent Management is key to delivering the brand promise through people and the services they render, for business continuity and to achieve competitive advantage. According to Wright, Dunford, &amp; Snell, (2001) &lsquo;to have a sustainable competitive advantage a firm must first possess people with different and better skills and knowledge than its competitors or it must possess HR practices that allow for differentiation from competitors&rsquo;.\r\nSo what can HR professionals do to differentiate? Here at Innovative HR Solutions we have created a Talent Management Audit Tool to help our clients check the building blocks are in place first. Fundamentally the tool prompts organisations to consider whether critical HR processes are in place and have been consistently applied across the business. The tool looks at attraction and recruitment, on-boarding through to the first six months, learning and development, performance management, pay and reward, succession planning and retention.\r\nEmployer Brand and Employee Engagement\r\nThe next critical step is employee brand. As Fitz-enz stated in 1997; &ldquo;The idea behind employee engagement is to build a fierce employer brand equity.&rdquo; Once the building blocks are in place the objective is to communicate and capitalize on your hard work creating an environment &ldquo;where individuals have a passion for the business motivating them to optimize their performance, commitment and contribution such that it is difficult for competitors to copy.&rdquo; Indeed the way in which employers engage with employee hearts and minds is particularly important given the emergence of Generation Y in the workforce; individuals in the region of 30 years old in 2012.\r\nGen Y&rsquo;s are looking for the opportunity to learn and develop, good managers who understand them and trust them and a great company brand. They are also much more likely to leave their job if they think it&rsquo;s taking too long to get promoted. Thus there are some significant consequences for talent management, if correct, to attract and retain Gen Y, an organisation should proactively promote their brand and brand values, proactively communicate across multiple channels to provide a 360 brand experience. Organisations should also be transparent about career paths and promotions and support personal development through training and mentorship programmes. The hospitality businesses I spoke with agreed. One remarked there was a &ldquo;notable shift of expectations coming from Generation Y&rdquo; another said that Gen Y were &ldquo;more savvy.&rdquo; People are expecting better work life balance and better access to information and communication.\r\nWhat are the practical steps?\r\nAll interviewees stated that Employer Brand as a tangible representation of culture and values and as such all those interviewed had wrapped their employee branding around the reward system, peoples experience of recruitment, induction, management training, the working environment through to vision and leadership statements. All had produced an impressive array of tools and beautifully presented literature to support their various initiatives. All of the programme literature captured the essence of passing through an important rite of passage and detailed the new competencies required so that individuals could be clear about what was required. All those interviewed spoke of quarterly talent meetings to discuss the talent pipeline, learning and development for strong performers and other staffing related matters.\r\nOne of the global hotel groups interviewed focuses on promoting people from within to ensure continuity of brand, growth and customer satisfaction which has a positive effect on the motivation of people; &ldquo;it is critical to the corporation&rsquo;s success to have sufficient supply of high-calibre, motivated individuals to support the Company&rsquo;s growth.&rdquo; In addition there is also a cost benefit in recruiting from within. The cost of the one management development programme is USD 10,000 per person. To recruit a Director or General Manager externally costs the business on average USD 20,000. The number of Programmes and participants within the programme keeps increasing. The target is to have a minimum of 50% of the participants promoted or transferred internally within one year after completing the Programme to ensure retention. They have been consistently mobilizing 70%+ to meet growth.\r\nAnother consensus view is that employee communication is key. All had newsletters, monthly staff meetings, intranet site, notice boards, training seminars, in-house &lsquo;universities&rsquo; and brand statements posted everywhere and &ldquo;everyone is recruited and orientated against our values.&rdquo; The assessment and development centres also all included brand and people-related exercises to crystallise the connection.\r\nThe Role of the Manager\r\nMy final comment is around the role of managers across any organisation. All those interviewed acknowledged the significant role of managers in the talent management process and acknowledged that as employees were disbursed, they were heavily dependent on managers. A good manager is like gold dust and can &ldquo;notably attract, retain and develop more good people than an average manager&rdquo; said one.\r\nThus overall it is clear to me that whilst HR needs to design the talent management strategy and tools to support there is a significant strategic need to grow culturally agile managers with the required competencies to deliver a consistent Talent Management message to build and protect your brand positioning with existing and potential employees.');
INSERT INTO `search_index` (`id`, `locale`, `type`, `type_id`, `url`, `title`, `content`) VALUES
(96, 'en', 'news & events', '4', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/4/innovation-and-psychology', 'The Psychology of Innovation ', 'Sharan Gohel\r\nIn a recent survey conducted by Mckinsey more than 70 percent of the senior executives said that innovation will be one of the top three drivers of growth for their company in the next three to five years. Other executives reported innovation as the most important way for companies to accelerate the pace of change in today&rsquo;s global business environment.\r\nInnovation and Creativity are two words that we all use interchangeably and generally agree are positive. In most cases we want more, yet there seems to be less alignment when we try and define it and no real shared interpretation of innovation/creativity. Similarly Leadership is also a term which does not seem to have a commonly accepted definition or shared understanding. Being creative or innovative and Leadership are closely related as leadership always seems to have some focus on bringing about a change resulting in a better future.\r\nMore often than not when you read books and articles on leadership, you will hear leadership defined in a number of different ways such as:\r\n\r\nthe ability to create a vision\r\nanticipate trends\r\nbeing congruent in the way leaders live their values (value-congruence)\r\nbeing empowering\r\nunderstanding their own impact on others (self-understanding)\r\n\r\nWithin the competencies mentioned above there is an assumption that the individual has the capacity to be more creative and consider more risks. However in order to be competent they will need to utilize and increase their capacity to be creative and take risks &ndash; having the capacity to develop new ideas and take risks to make the ideas a reality in the face of adversity. Therefore before leadership it is important to consider these two factors and the role they play. In this short article we will begin to explore the relationship between leadership and innovation or creativity and the impact risk taking can have on their success through each leadership competency.\r\nVision:&nbsp;It is widely accepted that effective leaders are able to create a vision for the organization&rsquo;s future; a picture that inspires people; a statement to action giving a shared purpose for all. Through such a vision, people in the organization are able to see the relationship they have to the larger realities/goals of the enterprise. But without creativity, how can you create a vision? And how can you make a vision happen if you&rsquo;re not willing to take risks to make it happen? A vision without creativity is dull and insignificant. A vision without risk taking never happens.\r\nValue-Congruence: Leaders who are congruent in the way they live out their values are those that inspire people to follow them and essentially &lsquo;walk the talk&rsquo;. A leader who uses his or her values to guide and motivate others is able to provide meaning to people&rsquo;s lives within organizations. In extreme cases they become the standards by which choices are made. However it is easy to walk and talk your values when you&rsquo;re not faced with a challenge or when you &ldquo;fit in&rdquo;. But if your values run against the norm, then that&rsquo;s when creativity and risk taking are required. Leaders must take the risk to &ldquo;walk the talk&rdquo;. Or, they run the risk of losing their capacity to lead.\r\nAnticipatory Skills: Foresight is fundamental to leadership. An effective leader intuitively and systematically scans the environment for potential areas of opportunities and risks. The leader&rsquo;s focus is on servicing customers (internal and external) in new ways, finding new advantages over competitors, and exploiting new company strengths. Being anticipating requires the leader to look outside your immediate view, being on alert for new upcoming ideas -looking for creative new trends that may impact you and your organization. At the same time a Leader must be willing to take the risk of thinking outside the box and their own paradigms - challenging their own mental models. It is the anticipation which drives an organization forward. Looking for opportunities to use creative abilities and taking the risk of thinking outside the realms of conventional methods -of what has been to is what will help create the new trends that are the keys to success.\r\nEmpowerment: This is not about blind trust, it is about giving those who you are leading responsibility for doing jobs and performing in ways that demonstrates your confidence in their abilities. The concept of empowerment embodies in it a level of trust in your employees, trusting that they can be creative with new ideas that come from them. This means that as a leader to be empowering you need to take the risk to let them determine and drive the ideas forward without you micromanaging them.\r\nSelf-Awareness: For a leader, understanding yourself is critical. Without it, leaders may do more harm than good. To be an effective Leader you must have introspective skills as well as frameworks with which to understand the impact on others. Leaders seek to understand themselves and what a risk that can pose, through understanding and making choices about yourself given that self-understanding. Knowing yourself and being able to share with others &ldquo;who you are and what your strengths and weaknesses are&rdquo; puts a leader in an extremely vulnerable, difficult but necessary position, probably one of the ultimate risks a leader faces.\r\nSo I would like you to challenge you as leaders to think about whether we may have missed a key component in effective leadership? If we agree that leadership is about the capacity to identify new opportunities and drive forward change within our organizations; then without fostering leadership competencies around innovation/creativity and risk taking, how can we as leaders take advantage of the opportunities we have in a global marketplace?'),
(97, 'en', 'news & events', '5', 'http://innovative-hr.com/knowledge-centre/news-events/article/5/women-in-leadership', 'Women in Leadership', 'Amanda White on DubaiEye 103.8\r\nAmanda White, Managing Director at Innovative HR Solutions, shares her thoughts on Women in Leadership on Dubai Eye Radio 103.8\r\n'),
(98, 'en', 'news & events', '6', 'http://innovative-hr.com/knowledge-centre/news-events/article/6/what-is-stress-and-the-economic-cost', 'What is Stress and the economic cost?', '14 May 2015\r\nAmanda White on DubaiEye 103.8\r\nAmanda White, Managing Director at Innovative HR Solutions and Dr. Saliha Afridi, Managing Director at Lighthouse Arabia, share their insights into stress in the workplace and what to do to combat it.\r\n'),
(99, 'en', 'news & events', '7', 'http://innovative-hr.com/knowledge-centre/news-events/article/7/global-salary-trends', 'Global Salary Trends ', '10 September 2013&nbsp;\r\nAmanda White on DubaiEye 103.8\r\nUAE salaries are predicted to rise 5.5% this year (2013) - an Aon Hewitt annual survey paints a stable outlook for the GCC. Amanda White, Managing Director at Innovative HR Solutions, provides her insights into Global salary trends and looks at how best to go about getting a raise!\r\n'),
(100, 'en', 'news & events', '8', 'http://innovative-hr.com/knowledge-centre/news-events/article/8/what-is-talent-management-and-how-important-is-it', 'What is Talent Management and how important is it?', '31 October 2011\r\nAmanda White on DubaiEye 103.8\r\nWhat is Talent Management and how important is it? Amanda White, Managing Director at Innovative HR Solutions, provides her thoughts on what Talent Management is, why are people talking about it and why is it so important for staff morale and business strategy!\r\n\r\n&nbsp;'),
(101, 'en', 'news & events', '9', 'http://innovative-hr.com/knowledge-centre/news-events/article/9/emotional-intelligence-in-the-workplace', 'Emotional Intelligence in the Workplace', 'Abi White\r\nThe UAE Psychologist Newsletter, Vol 5. May-Dec. 2015\r\nFor many, Emotional Intelligence (EI) is perceived as a relatively new concept, however the importance of being aware of and having an ability to manage emotional responses has been around since the 1960&rsquo;s. Whilst there are many definitions of Emotional Intelligence it can generally be described as &lsquo;a set of emotional and social skills that influence the way we perceive and express ourselves, develop and maintain social relationships, cope with challenges, and use emotional information in an effective and meaningful way&rsquo;.\r\nA great deal of research has been conducted on the im-pact of EI in the workplace, in particular Leadership. With all the global, economical and generational changes in today&rsquo;s work place, being an emotionally intelligent leader is a key driver for success. Despite this perception, application of this approach is still less prevalent. Leaders are faced with an overwhelming number of challenging and conflicting approaches to adopt on how to become an effective leader. In pursuit of effective leadership many have attended workshops and received coaching in order to become emotionally intelligent leaders. However, despite the visibility and focus placed on its importance within organi-sations, the impact within the workplace is less prominent. The question is why?\r\nWhen considering organisations today, it could be argued that there are three challenges that stand out when trying to develop emotionally intelligent leaders.\r\nFirst, Organisational Culture. The foundation for most organisations today is still around results, the bottom line and policies and procedures, with little focus on people development. Within such a context embedding a leadership style that is emotionally intelligent is challenging; as organisational systems and mechanisms do not support or allow leaders to demonstrate these behaviours. The demand for financial success and quick results tends to override the implementation and effectiveness of any leader attempting to demonstrate new or learnt skills in this area.\r\nSecondly, the duration and type of development initiatives. Organisations who focus on short term development initiatives, such as an emotional intelligence workshop, will find getting traction around the learning outcomes a challenge and struggle to gain a Return on Investment. This may lead to lower levels of buyin to the concept due to less tangible results of the development activities.\r\nFinally, the Target Audience. Many organsiations appear to target specific audiences that are limited to either a specific level or section of the business. This limitation creates a small pocket of understanding, and fails to reach the wider population. Therefore, having little effect on leadership style or organisational culture.\r\nWhile the approach adopted by organisations may to have emotional intelligence at the core, it is important to have a long term focus on development initiatives with multiple interventions to ensure greater momentum and behavioural change. In addition, determining ways to impact larger audiences and creating mechanisms for cascading learning can ensure the learning is more pervasive.'),
(102, 'en', 'news & events', '10', 'http://innovative-hr.com/knowledge-centre/news-events/article/10/beware-the-dangers-of-occupational-stress', 'Beware the Dangers of Occupational Stress', 'Jonathan Rook\r\n\r\nStress costs organisations billions of dollars a year. But being aware of stress, and introducing a few new activities to your workday, could change all that\r\nAgain the concept of &lsquo;stress&rsquo; at work has appeared in the popular media, with authors linking stress with sleep problems and an increase in Karoshi (&lsquo;death from overwork&rsquo;).\r\nThe negative impact of &lsquo;stress&rsquo; isn\'t new but it continues to be something we need to manage as individuals and as leaders of organisations\r\n\'Occupational Stress\', the experienced incompatibility between the demands placed upon us through our work (such as quantity, cognitive requirements and interpersonal relationships) and our perceived ability to cope with these stressors, costs organisations and the economy billions of dollars each year.\r\nThese costs arise from sickness absence, lost productivity, reduced performance, compensation claims and staff attrition.\r\nThe individual costs are potentially greater and can include chronic fatigue, &lsquo;burnout&rsquo;, career derailment, depression and anxiety - and that&rsquo;s before we consider physical ailments such as heart disease, joint pain, stomach complaints and other more serious illnesses linked to harmful stress hormones, such as cancer.\r\nIt is common for &lsquo;stress&rsquo; to impact sleep negatively, if we are over-activated at the end of a work day and fail to recover during leisure time, an effect of this is reduced quality and efficiency of sleep.\r\nSleep is a restorative function and it is necessary for the optimal and fundamental performance of mental and physical tasks. Prevailing thought indicates we need a core amount of sleep for optimal functioning (five to eight hours for most individuals). Bottom line it makes sense for leaders to pay attention to stress.\r\nAll that said, we need a certain amount of &lsquo;healthy stress&rsquo; to function, but it depends on our psycho-physiological makeup as to the consequences and how we should offset adverse effects.\r\nThere are three tactics we can employ to reduce work stress: Remove the stressor, change our appraisal and use coping strategies.\r\nJonathan Rook is a chartered psychologist and associate fellow of the British Psychological Society. He is an expert at leadership assessment, development and c-level coaching\r\nRemoving the stressor could include ensuring work related activities are not undertaken outside of the &lsquo;working day&rsquo; - easier said than done in this technological age.\r\nIn extreme cases removing the stressor may include changing jobs. There is also the option of designing jobs so that they are less stressful, for example by giving employees (perceived) greater control over their work, or reducing their span of responsibility.\r\nOrganisations may also design ergonomic work spaces or modify shift patterns to ensure regular breaks and vacations.\r\nChanging our appraisal of stressful situations can include re-framing core beliefs and ideas we have about individual events.\r\nA coach may help us to change the way we think about &lsquo;stressors&rsquo;, so they no longer become stressful and activate the stress response. We may also use psychometrics to coach people to be more assertive, less emotional in their appraisals of stressors, to help them better align their preferences to the job they choose and to develop resilience.\r\nThere is also help in terms of time management and developing greater emotional intelligence.\r\nCoping strategies include ensuring that we recover properly during times of leisure or non-work time. Recovery is the process of replenishing the depleted resources or rebalancing suboptimal systems.\r\nIt is evident, from mine and others research that we should stimulate active engagement in leisure activities, particularly those which are different to our work tasks.\r\nPhysical activity is a great recovery activity because it acts as a stress reliever and dissipates harmful stress hormones. Something as simple as promoting physical activities during lunch breaks or ensuring employees reach closure before leaving the office is helpful.\r\nTechniques for healthy sleep include cognitive closure, lowering body-core temperature and modifying work schedules which promote balance.\r\nThere is also growing evidence that ancient techniques such as meditation, Yoga, Tai-Chi, Qi-Gong are helpful promoting &lsquo;mindfulness&rsquo;, deep breathing and mental focus.\r\nUltimately there are things we can do in our battle to reduce the individual and organisational costs of stress; things that don&rsquo;t cost the earth but would radically change our experience of living on it.'),
(103, 'en', 'news & events', '11', 'http://innovative-hr.com/knowledge-centre/news-events/article/11/how-to-be-a-good-boss', 'How to be a good boss', '16 April 2015\r\nAmanda White on DubaiEye 103.8\r\nAmanda White, Managing Director at Innovative HR Solutions, gives her expert commentary (5.52 minutes) on &lsquo;How to be a good boss&rsquo; and the top qualities that make employees stay in their job.\r\n\r\n&nbsp;\r\n&nbsp;'),
(104, 'en', 'news & events', '12', 'http://innovative-hr.com/knowledge-centre/news-events/article/12/building-a-great-coaching-relationship', 'Building a GREAT Coaching Relationship', 'Roisin O\'Kane\r\n&ldquo;Without a relationship, coaching cannot happen&rdquo; &ndash; Downey\r\nRelationships define what coaching is. If you as a coach are unable to form a strong relationship with your client the process will almost certainly fail to achieve a positive outcome. However if the coaching relationship is strong and healthy this provides your client with a safe space in which to become aware of the need or desire to grow and to explore the many options available and take the appropriate action to move forward.\r\nIn order to build a strong interpersonal relationship the coach must have a sufficient degree of interpersonal intelligence. Golemam in his book, &lsquo;Emotional Intelligence&rsquo; quotes the earlier work of Gardner who defines interpersonal intelligence as &ldquo;the ability to understand other people: what motivates them, how they work, how to work co-operatively with them. It also includes &ldquo;the capacities to discern and respond appropriately to the moods, temperaments, motivations and desires of other people&rdquo;.\r\nCoaches must therefore have a sufficient degree of intrapersonal intelligence &ndash; an understanding of self that is true and accurate, requiring a good deal of insight as well as the courage to face the truth. The basic skills involved in intrapersonal intelligence are insight and self &ndash;awareness, including the ability to recognize and name our emotions and to manage them. These skills are vital for us as coaches, to not only be aware of and manage our own feelings, but to be aware of the feelings of our clients and to respond appropriately.\r\nCentral to the development of the coaching relationship is the coach&rsquo;s ability to develop rapport with the client. Rapport in the coaching relationship includes a strong sense of trust and mutual respect that allows open and honest communication. Without trust the coaching relationship will falter, therefore from the initial meeting the coach must inspire trust. The coach can inspire trust by demonstrating their integrity and authenticity on an ongoing basis. It is extremely important that the coach is seen to be living his/her life in an authentic way, for example, do the coaches have their own coach? Is she/he setting goals for their own life?\r\nRespect is also an important characteristic of the coaching relationship, the coach and the client must establish the boundaries at the onset of the relationship so that both parties are clear about their role, the process and the expectations. As coaches, it is also imperative that we respect our clients&rsquo; values, beliefs and opinions even when we disagree.\r\nThe coaching relationship is always supportive of the client, however the coach will also, with the client&rsquo;s permission, challenge the client but always with the clients&rsquo; best interests in mind. For example, if the client is expressing a limiting belief the coach will challenge the client in order to overcome it and move forward.\r\nMany factors affect the coaching relationship, these include each person&rsquo;s emotional self- awareness, insight into human behavior, emotional resilience, the ability to trust and to inspire trust, authenticity, ability to communicate clearly, stresses, strains, health and well-being, hopes, fears, aspirations , and even moods. These factors may overlap, for example, without integrity and authenticity it may be difficult to build the trust needed for a coaching relationship.\r\nUnconscious processes such as transference and counter- transference, projection, and re-stimulation are very powerful and can often be damaging elements in the relationship. They must be identified and taken care of if the relationship is to flourish.\r\nAdditionally the intensity of the relationship may provide a challenge. The spectrum of relationships ranges from hostile to intimate at each extreme. For coaching to be effective and appropriate, the relationship should be friendly and warm. A hostile, remote or cool relationship will be destructive, inappropriate and ineffective. However, as we work together on very personal issues such as hopes and fears in a supportive environment it is critical that we bear in mind that allowing the relationship to develop along the spectrum to love and intimacy is completely inappropriate. The coach must at all times be able to maintain a professional detachment from their client and the outcome of the coaching. It would be highly inappropriate if the coach had an emotional reaction to their client not following through with an agreed action rather the coach would be using her skills of questioning, listening and clarifying to ascertain why the action had not taken place.'),
(105, 'en', 'news & events', '13', 'http://innovative-hr.com/knowledge-centre/news-events/article/13/mbti-forum', 'MBTI Forum', '\r\nA complimentary session exclusively for MBTI Practitioners!\r\nThe Myers Briggs Type Indicator (MBTI) Forum was launched in April 2011 and with over 500 members the use and expertise of the MBTI instrument continues to grow in the Middle East. The Forum is exclusively for qualified MBTI Practitioners and is an opportunity to grow the global awareness of the MBTI tool and strengthen the skills and effectiveness of the practitioners who use it in the GCC region. The Forums are currently held bi-annually in Dubai and we also have a LikedIn group, MBTI Middle East, exclusive for MBTI practitioners to join to stay up to date and informed with the latest MBTI news.\r\nThe MBTI Forum provides practitioners with the opportunity to:\r\n\r\nNetwork with local and international practitioners\r\nDevelop skills through sharing experiences and listening to expert speakers\r\nReceive exclusive MBTI Forum news\r\nReceive special offers on products and training\r\nOnline discussions hosted by accredited trainers\r\nBecome a member of the MBTI Middle East Linkedin Page\r\n\r\nPrevious Forum topics have included:\r\n\r\nTemperament over Type\r\nManaging Conflict through Type\r\nUsing Type to improve the Innovation process\r\n\r\n&nbsp;If you are a certified MBTI practitioner and would like to join the Middle East MBTI Forum Book now!'),
(106, 'en', 'news & events', '14', 'http://innovative-hr.com/knowledge-centre/news-events/article/14/meet-us-at-the-hr-summit-and-expo', 'Meet Us at the HR Summit & Expo!', '14 &ndash; 16 November 2016\r\nEngagement Theatre, Halls 5 &ndash; 6, Dubai International Exhibition Centre\r\nIf you&rsquo;re heading to the HR Summit &amp; Expo, drop by and say hello at Stand D20, where we can have a chat about how we can help you with your HR Solutions!\r\nWe are also delighted to be presenting as a keynote for the HR Engagement Summit (part of the HR Summit &amp; Expo)\r\n\r\nTo purchase tickets to the HR Summit and Expo and attend our keynote session on Emotional Intelligence in Engagement at the HR Engagement Summit visit\r\nhttp://www.hrsummitexpo.com/hr-engagement-summit/\r\nRegister NOW! \r\nCall: +971 4 335 2437\r\nEmail: register-mea@informa.com\r\nWeb: www.hrsummitexpo.com'),
(107, 'en', 'news & events', '15', 'http://innovative-hr.com/knowledge-centre/news-events/article/15/innovative-hr-solutions-winner-of-people-and-culture-of-the-year-award', 'Innovative HR Solutions winner of People & Culture of the Year Award!', 'Dubai, United Arab Emirates; 31 October 2016&nbsp;&ndash; Innovative HR Solutions won the People &amp; Culture of the Year Award at this year&rsquo;s edition of the Gulf Capital SME Awards, the prestigious recognition programme honouring the success, growth and innovation of small businesses, entrepreneurs and business leaders in the UAE.\r\n&lsquo;We are delighted to have won the People and Culture Award. Our people are passionate about what we do and this award is a testament to the hard work and commitment of our growing team&rsquo;\r\n\r\n\r\nInnovative HR Solutions was recognised for demonstrating a positive working environment and investing in developing its people. Innovative HR Solutions showcased how it focuses on motivating and engaging its Team in the company\'s values to &nbsp;shape its business\' commercial performance and competitive advantage.\r\n&ldquo;We congratulate all the winners for their achievements, and commend their efforts for steering their businesses to success. We hope that this recognition paves the way for greater growth, and become an inspiration for other small business owners to follow,&rdquo; says Becky Crayman, Head of Awards, MEED.\r\nThe winners of the 2016 Gulf Capital SME Awards were honoured at a glittering ceremony at the Conrad Dubai.\r\nAbout Gulf Capital SME Awards The Gulf Capital SME Awards recognises the UAE&rsquo;s SMEs, business leaders and entrepreneurs for innovation, growth and success.&nbsp;For more information about Gulf Capital&nbsp; please visit www.gulfcapital.com\r\nAbout Innovative HR Solutions Innovative HR Solutions (IHS) is one of the largest teams of Occupational&nbsp;Psychologists and learning and development specialists in the Gulf.&nbsp;We specialise in individual, team and organisational assessment and development.\r\nWe work with organisations to select, assess and develop talent and teams, and support organisational development through the design and implementation of world-class Strategic HR Frameworks.\r\nIHS has been headquartered in the United Arab Emirates since 1999. Today we represent 12 International Test Publishers in the GCC, and have brought to the region world-renowned and highly-respected psychometric tools and leadership assessments to support effective individual and organisational development.\r\nOur team work in English and Arabic with Young Talent through to Senior Executives in the United Arab Emirates, Saudi Arabia, Qatar, Oman, Bahrain, Kuwait and Jordan. &nbsp;We bring international expertise and deep regional experience to ensure our products and services are in line with global best practices and achieve enduring business outcomes.\r\nFor enquiries, please contact: Jehan Tabet Marketing Manager, Innovative HR Solutions PO Box 52271, Office, G05-08, Block 3, Ground Floor, Dubai Knowledge Village, Dubai&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tel: 00971 (0) 4 4461505 | M&nbsp;: 00971 (0) 561472854 | E:&nbsp;jtabet@innovative-hr.com'),
(108, 'en', 'case studies', '4', 'https://www.innovative-hr.com/knowledge-center/casestudies/casestudy/4/leading-hoteliers-maximising-excellence-in-rotana-leaders', 'LEADing Hoteliers! Maximising Excellence in Rotana Leaders', 'The Leadership Excellence and Development (LEAD) programme was formulated in recognition of the critical importance of outstanding leadership to the future growth of Rotana.\r\nRotana is a leading hotel management company in the&nbsp;Middle East, African and South Asian&nbsp;regions. Founded in 1992, it has a portfolio of over 100 properties in 26 cities. Rotana values it staff and prides itself on working tirelessly to maintain and develop talent in all capacities.\r\nIn 2011, IHS were approached to offer human capital services to map out the future development initiatives for Rotana leaders. The goal of this initiative was to install a self-managed internal selection and development process that adapted to high potential, talented individuals. This programme was to optimize the efficiency of Human Resource efforts, and manage the expectations of interested parties.\r\nTogether, Rotana and IHS developed distinctive strategies across three job levels as part of the LEAD Series. &nbsp;The LEAD series is an all encompassed, yet simple approach, to Assessment and Development, inclusive of centres, Personal Development Plans, feedback reports, coaching elements and psychometrics for GMs, EAMs and Department Heads across leading Middle Eastern Hotel chains.\r\nThe programme resulted in successfully identifying talented individuals and working with them to ensure long term commitment to personal growth and, in turn, success for Rotana. 50 people took part across all three levels of the Programme that ran across 4 years, in a series that arguably puts Rotana at the forefront of excellent Hotelier HR practice, not only in this region- but globally. IHS continues to offer support services to Rotana to continually enhance their HR practices across their hotel properties.\r\n&rdquo;Once again, a million &lsquo;Thank You&rsquo;s&rdquo;\r\nBUSINESS BENEFITS\r\n\r\nConsistent, centralised and clarified approaches to selection and development activities.\r\n\r\nAchieving whole person development &nbsp;by facilitating tailor made activities, enabling learning and carving appropriate channels for meaningful communication.\r\n\r\n\r\n&nbsp;'),
(109, 'en', 'events', '1', 'https://middleeast.psionline.com/knowledge-centre/event/1/mbti', 'MBTI Forum', 'A complimentary session exclusively for MBTI Practitioners!\r\nThe Myers Briggs Type Indicator (MBTI) Forum was launched in April 2011 and with over 500 members the use and expertise of the MBTI instrument continues to grow in the Middle East. The Forum is exclusively for qualified MBTI Practitioners and is an opportunity to grow the global awareness of the MBTI tool and strengthen the skills and effectiveness of the practitioners who use it in the GCC region. The Forums are currently held bi-annually in Dubai and we also have a LinkedIn group, MBTI Middle East, exclusive for MBTI practitioners to join to stay up to date and informed with the latest MBTI news.\r\nThe MBTI Forum provides practitioners with the opportunity to:\r\n\r\nNetwork with local and international practitioners\r\nDevelop skills through sharing experiences and listening to expert speakers\r\nReceive exclusive MBTI Forum news\r\nReceive special offers on products and training\r\nOnline discussions hosted by accredited trainers\r\nBecome a member of the MBTI Middle East Linkedin Page\r\n\r\nPrevious Forum topics have included:\r\n\r\n\'How to Run an MBTI Workshop\' Workshop\r\nTemperament over Type\r\nManaging Conflict through Type\r\nUsing Type to improve the Innovation process\r\n\r\n&nbsp;If you are a certified MBTI practitioner and would like to join the Middle East MBTI Forum Book now!'),
(110, 'en', 'events', '2', 'https://middleeast.psionline.com/knowledge-centre/event/2/professional-development-breakfastbreakfast', 'Business Breakfast', 'A complimentary session for our&nbsp;PSI Middle East&nbsp;Community!\r\nLaunched in 2001, our Professional Development Breakfasts are an opportunity for our community to come together and attend an engaging and thought provoking interactive workshop on a topic of interest. The topics we choose are informed by your&nbsp;feedback. The sessions are delivered&nbsp;by our team of Occupational / Business&nbsp;Psychologists or guest experts in the field. Professional Development Breakfasts are held across four&nbsp;locations; Dubai, Abu Dhabi, Doha and Riyadh.\r\nThe Professional Development Breakfast provides our clients with the opportunity to:\r\n\r\nStay informed on the latest talent thinking.\r\nNetwork and meet likeminded professionals in the field of HR and talent.\r\nGain new found skills to use in your business environment.\r\nReceive session resources and takeaways to use in your business environment.\r\n\r\nPrevious breakfast sessions have included:\r\n\r\nHow to Facilitate Teams in Creativity and Innovation\r\nDeveloping Resilience in Your Business Leaders.\r\nDeveloping a Feedback Culture.\r\nLeading in a Volatile, Uncertain, Complex and Ambiguous (VUCA) world.\r\n360 Feedback Systems.\r\nUsing strengths to accelerate peak performance and improve feedback effectiveness.\r\nDriving Performance through Emotional Intelligence.\r\nCreating a Coaching Culture.\r\n\r\nIf you would like to attend our upcoming Professional Development Breakfast book now!'),
(111, 'en', 'courses', '4', 'https://middleeast.psionline.com/calendar/course/4/mbti-certificate-program', 'MBTI Certification Programme', 'PSI Middle East is the only offically licsenced MBTI distributor for&nbsp;the Middle East. We are proud partners of The Myers-Briggs Company, formerly CPP, and we look forward to introducing you to the worlds most widely used Type tool!\r\nCourse Overview\r\nThe Myers-Briggs Type Indicator&reg; (MBTI&reg;) instrument continues to be the most trusted and widely used assessment in the world for understanding individual differences and uncovering new ways to work and interact with others.\r\nIt is used exclusively in a development setting to improve individual and team performance, develop and retain top talent and leaders lending depth to the coaching experience, improve communication and reduce conflict in teams as well as explore the world of work and careers.\r\nThose who successfully complete the course will be accredited to administer, interpret and feedback the MBTI instrument (Step I &amp; II). You will also be invited to join the Middle East MBTI Practitioners forum which is a free bi-annual learning and development event for MBTI practitioners to share experiences and continue to develop their skills further with the MBTI.\r\nThe course comprises of some pre-course reading and taking the MBTI questionnaire, home work and daily multiple choice assessments.'),
(112, 'en', 'courses', '6', 'https://middleeast.psionline.com/calendar/course/6/bps-qualification-in-occupation-testing', 'BPS Qualification in Occupational Testing', 'Course Overview\r\nFormerly known as the BPS Level A&amp;B (P), this 4-day course qualifies delegates to use a wide variety of ability tests and personality questionnaires from a number of major test publishers including Saville Assessment, a Willis Towers Watson Company, psychometric&nbsp;products.\r\nThe qualification is internationally recognised demanding the best practice in the use of psychometrics at work, and is suitable for those involved in selection, development or any other application where psychometric assessments are useful. Upon successful completion of the course delegates will be able to become registered as&nbsp;an international Occupational Test&nbsp;User (Abilty and&nbsp;Personality), with the ability&nbsp;to&nbsp;administer, analyse and feedback a variety of psychometric tools including the Saville Assessment&nbsp;range of products. Upon successful application to the BPS, conversion courses to a number of further products will also be made available.\r\nPre-course work, reading and assessments are a requirement, two open book exams during the 4-day course and post course work, should you wish to apply to the BPS upon completion. Included in our pricing is one-to-one support with your post course&nbsp;work and free applications of the Saville Assessment Wave.\r\nWhy British Psychological Society (BPS)?\r\nThe British Psychological Society promotes excellence and ethical practice in the science, education and practical applications of psychology. They do this by setting standards in&nbsp;psychological testing and raising levels of&nbsp;education, training and practice through their recognised international course providers. More information about Saville Assessment, A Willis Towers Watson Company and the BPS is available through IHS.http://www.bps.org.uk/what-we-do/bps/bps\r\n&nbsp;\r\nWe run this as a public course but this can be run as an in-house course for your teams! So feel free to get in touch to ask us more about this option on 04 390 2778.'),
(113, 'en', 'courses', '8', 'https://www.innovative-hr.com/calendar/course/8/eqi-20-and-eq360', 'EQi 2.0 & EQ360 ', 'Course Overview\r\nThe EQi is particularly useful for development activity and in certain circumstances for selection. Emotional Intelligence has been identified as one the leading predictors of leadership performance and success, and is helpful in identifying how we can understand and manage ourselves and our relationships with others.\r\nThe EQi 2-day Accreditation Course enables those who complete, pass the post online assessment and conduct an observed feedback session, to become certified to use the EQi-2.0 model of Emotional Intelligence tools; both individual reports and 360 surveys.'),
(114, 'en', 'courses', '10', 'https://www.innovative-hr.com/calendar/course/10/strong-interest-inventory', 'Strong Interest Inventory', 'Course Overview\r\nThe Strong Interest Inventory instrument is the most widely used and respected career development instrument in the world. It has guided thousands of individuals, from high school and college students to mid-career workers seeking a change in their search for a rich and fulfilling career. It is a powerful tool for anyone considering a career change, starting a new career, looking for career enrichment, or a better work life balance. The Strong Interest Inventory generates an in depth assessment of an individual&rsquo;s interests in a broad range of occupations, work and leisure activities, and educational subjects. The tool is the gold standard for career exploration and development, providing time-tested and research-validated insights that foster successful career counselling relationships. The Strong can be used in conjunction with other reports such as the Myers Briggs Type Indicator (MBTI) for a detailed look at personality and career.\r\nThe Strong Interest Inventory&reg; is ideal for:\r\n\r\nChoosing a college major.\r\nCareer exploration.\r\nCareer development.\r\nRe-integration.\r\n\r\nThere is some pre-course work (online assessment) required.&nbsp;'),
(115, 'en', 'courses', '11', 'https://www.innovative-hr.com/calendar/course/11/firo-business', 'FIRO Business', 'Course Overview\r\nThe FIRO-Business (Fundamental Interpersonal Relations Orientation - Business) instrument helps individuals understand their need for inclusion, control and affection and how it can shape their behaviour and interactions with others at work or in their personal life. As an integral part of your leadership and coaching, team building and conflict management initiatives, the&nbsp;FIRO-B assessment can be used in a variety of settings and in combination with other solutions to improve organisational performance. It provides the skills to administer and interpret the assessment competently and ethically.\r\nThere is some pre-course work (online assessment) and homework.'),
(116, 'en', 'courses', '17', 'https://middleeast.psionline.com/calendar/course/17/assessor-skills', 'Assessor Skills and Centre Manager', 'Course Overview\r\nThis British Psychological Society (BPS) recognised 3-day Global Leader Assessor Skills and Centre Manager Course enables delegates to develop the knowledge and skills needed to become a qualified Assessor and Centre Manager in a range of typical Assessment Centre/ Development Centre events.It is designed for HR Professionals, management and non-management staff who are required to be part of an assessment function.\r\nYou will be shown how to select exercises and design a matrix for an assessment event to gain a robust measure of the behaviours at the level in question. We will also delve into the art of recording, classifying and rating behavioural evidence across a range of observable exercises including group exercises, role plays and analysis exercises / case studies and presentations. It will also instruct in the art of running an efficient and robust post centre &lsquo;wash-up&rsquo; session and guide consistency and quality throughout an event.&nbsp;\r\nThere is no pre-coursework required. However, you can expect&nbsp;homework, an observed feedback session and a multiple choice exam at the end of the programme.\r\nWe run this as a public course but this can be run as an in-house course for your teams! So feel free to get in touch to ask us more about this option on 04 390 2778.\r\n&nbsp;Programme Flyer'),
(118, 'en', 'courses', '13', 'https://www.innovative-hr.com/calendar/course/13/afc-executive-coaching-certification', 'Accredited Award in Coach Training (AACT)', 'Course Overview\r\nA fast paced, highly interactive programme&nbsp;recognised by the Association of Coaching UK, introduces the practical techniques to coach individuals for professional development. Real life case studies, group discussions, personal feedback and continuous practice are all used to build the framework for professional coaching skills. The workshop particularly focuses on the GROW model.\r\nThe workshop is designed to create an assured coach, confident to tackle work and behavioural issues and help their coachees achieve set goals. This includes:\r\n\r\nUnderstand the purpose, ethics and power of Coaching.\r\nGaining commitment in times of change.\r\nTapping into people&rsquo;s potential and their desire to succeed and improve.\r\nSolving technical and organisational problems.\r\nDealing with difficult people and dramatically enhance their own and other&rsquo;s performance.\r\n\r\nInnovative HR Solutions has aligned this programme with the Association for Coaching (UK) and the standards they set to help participants gain Associate and Member Level Membership with the Association following the workshop.\r\nThere is some homework and post-course work &ndash; practical and theoretical work required.\r\n10 Reasons to get certified with Association for Coaching'),
(120, 'en', 'clients', '77', 'https://www.innovative-hr.com/about/clients#pae', 'PAE', '\'You delivered the training perfectly - we now understand much more about ourselves, our attitude and our work styles. I think this has really helped us understand one another and how we can interact so much better. You really listened to us and gave us applicable examples of how we can use MBTI at work\' - MBTI Workshop, HR Department, PAE Government Services '),
(122, 'en', 'clients', '14', 'https://middleeast.psionline.com/about/clients#etihad-airways', 'Etihad Airways', ''),
(245, 'en', 'clients', '19', 'https://middleeast.psionline.com/about/clients#abu-dhabi-commercial-bank', 'Abu Dhabi Commercial Bank', ''),
(123, 'en', 'clients', '16', 'https://middleeast.psionline.com/about/team#kristina-beggs', 'Kristina Beggs - Business Development & Marketing Manager', 'Kristina leads our business development efforts to enhance clients navigation of our services, and build talent solutions with the input of our consulting team. She is passionate about client service and can be regularly seen preparing proposals and presentations, meeting with clients and ensuring our clients have the right solution and are delighted with the end result.\r\nShe began her career in the United Kingdom in a Big Data organisation working with PR/Marketing and Digital teams with some of the most prominent brands across multiple industries; including McLaren Automotive, Unilever, Danone Nutricia and Johnson &amp; Johnson. During these 7 years, Kristina developed client relations team from 2 people to over 25 while growing her personal portfolio of business to over $3m USD. Kristina has a keen interest in languages and cultures, reflective in her learning of Arabic since she has been in the region with PSI Middle East for three years. Her MBTI type is ESFJ.'),
(124, 'en', 'clients', '15', 'https://middleeast.psionline.com/about/clients#airbus', 'Airbus', ''),
(234, 'ar', 'news & events', '19', 'https://middleeast.psionline.com/knowledge-centre/news-events/article/19/launching-psi-middle-east', 'إعلان إطلاق شركة PSI  الشرق الأوسط !', 'يسر شركة إنوفيتيف لحلول الموارد البشرية (IHS) الرائدة في مجال تفييم وتطوير المواهب، الإعلان عن تغيير اسم مؤسستها لـ PSI Assessment Services FZ LLC و ستعرف بـ PSI الشرق الأوسط في جميع مكاتبها في الخليج من الان فصاعداً.\r\nبعد استحواذ شركة PSI Services Inc على إنوفيتيف لحلول الموارد البشرية في شهر يونيو من هذا العام، تم الاتفاق على تغيير الاسم ليعكس شعار شركتنا الأم، والتزامنا في هذه المنطقة بمواصلة تطوير وتوفير خدمات التكنولوجيا الخاصة بالتراخيص والشهادات المقدمة من قبل بشركة PSI .\r\nكما صرحت أماندا وايت، المديرة التنفيذية لشركة PSI الشرق الأوسط قائلة \" نحن متفائلون ونترقب الخطوة التالية من رحلتنا كـ PSI الشرق الأوسط بإفتتاح مكتبنا الجديد في الرياض في بداية العام المقبل. ان خبرتنا العميقة في المنطقة بالإضافة لتكنولوجيا التقييم ذات المعايير العالمية لـ PSI ستمكننا من مواجهة التحديات الرئيسية الخاصة بعملائنا، وستتيح لنا المجال لتقديم حلول تجمع بين المحتوى والتكنولوجيا مع الخبرة الإستشارية.\"\r\nعن شركةPSI الشرق الأوسط\r\nتقوم PSI الشرق الأوسط بالعمل مع المؤسسات من أجل اختيار المواهب والفرق وتقييمها وتطويرها. بجانب دعم التطوير المؤسسي من خلال تصميم وتنفيذ الأطر الاستراتيجية للموارد البشرية ذات المعايير العالمية. يقع مقرنا في الإمارات العربية المتحدة منذ عام 1999، ويعد فريقنا أحد&nbsp;أكبر فرق المختصين بعلم النفس المهني والتعليم والتطوير في الخليج. &nbsp;كما نقوم بتمثيل ١٢ ناشراً للإختبارات العالمية في منطقة الشرق الأوسط. يعمل الفريق باللغتين العربية والإنجليزية على مختلف المستويات، بدءا بالأفراد ذوي المواهب إلى مستوى المسؤولين التنفيذيين في الشرق الأوسط.\r\nوتعدPSI الشرق الأوسط جزء من PSI Services LLC (PSI)، التي تمتلك أكثر من 70 سنة من الخبرة في تقديم حلول التقييم عالمياً للشركات والمؤسسات الحكومية و هيئات التصديق والمؤسسات الأكاديمية . وتقدم PSI مناهج حلول شاملة من تطوير الاختبارات ومعالجة النتائج، مما يشمل اختبارات التقييم لما قبل التوظيف، وتقييم الإدارة، وإختبارات الترخيص والتصديق، وإختبارات الدراسة عن بُعد.\r\nللمزيد من المعلومات\r\nجانيت غارسيا | رئيسة شركة PSI International | jgarcia@psionline.com | الهاتف: +44 78675580 | https://www.psionline.com/talent-measurement/ أماندا وايت | المديرة التنفيذية PSI الشرق الأوسط | amandawhite@psionline.com | الهاتف المحمول: +971 509110 772 | هاتف المكتب : +971 4390 2779 | www.middleeast.psionline.com'),
(128, 'en', 'clients', '20', 'https://www.innovative-hr.com/about/team#aref-al-mubarak', 'Aref Al Mubarak - Senior Executive Coach', 'Aref is a seasoned Senior Executive Coach and Team Facilitator who has enjoyed a long and varied career across many different verticals before choosing coaching and learning and development as a career. Aref actually began his professional life in a technical operations capacity in Quality Assurance and then as an Aircraft Senior Production Planner before he joined the Training and Assessment Development Centre team with Saudi Airlines, were he was from 1982 &ndash; 2009. As a member of the Training and Assessment Team he was involved in conducting Assessment Centres, Training Needs Analysis, creating IDPs and Coaching. It was then here that he decided to pursue his love for personal and professional development of both himself and others by leaving to become a full time Senior-Level Executive Coach. Aref holds an Organisation Developer Certificate (ODC), is an International Certified Trainer (HRDA) and PCC Certified Executive Coach.'),
(129, 'en', 'clients', '21', 'https://middleeast.psionline.com/about/clients#dubai-international-financial-centre', 'Dubai International Financial Centre', '');
INSERT INTO `search_index` (`id`, `locale`, `type`, `type_id`, `url`, `title`, `content`) VALUES
(242, 'en', 'news & events', '26', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/26/gritthe-missing-link', 'GRIT...THE MISSING LINK', 'GRIT...THE MISSING LINK\r\nSharpen your grit and watch your dreams turn into reality.\r\nBy Dr MARAIS BESTER\r\n&nbsp;\r\nOn a recent trip to Doha, Qatar, I met a Kenyan taxi driver named Jensen. For the past five years in Doha, he has worked 17-hour shifts every day and has not been home to see his family. His hard work has paid off and he has been able to buy a plot of land in Kenya on which he has three cows and a delivery truck. He has also sent his two sons to university. He has decided that he will be going home at the end of the month. To me, Jensen is a great example of someone with grit.\r\nThe concept of grit can be seen as a positive psychological concept that focuses on an individual&rsquo;s perseverance and passion for a particular long-term objective or end-state. Individuals who have high levels of grit are typically courageous, conscientious, goal-orientated, resilient and always striving for excellence.\r\nCourage\r\nWhen it comes to courage, individuals who have high levels of grit are typically not scared of taking risks. They are also not scared of making mistakes. They believe that there are great lessons to be learnt from losing, messing up and overcoming one&rsquo;s fears. Psychologists describe the fear of failure as an abnormal and unhealthy aversion to risk. Fear of failure sees individuals develop a very strong resistance to change, coupled with feelings of ambiguity and vulnerability. Change or uncertainty will typically be met with feelings of anxiety, mental blocks and inability to perform. In the words of Eleanor Roosevelt: &ldquo;Do something that scares you every day.&rdquo; Consider your goals and break them down into bite-sized actions or solutions. Reflect on your own strengths and experience and consider how they can be utilized to overcome your\r\nfears. Don&rsquo;t be scared of making a fool of yourself or failing because courage is a muscle that needs to be exercised. The more you put yourself out there the more you will develop courage and, subsequently, grit.\r\nConscientiousness\r\nBeing conscientious is not just about showing up at work every day or being dependable and meticulous. Conscientiousness implies being able to align your actions towards a specific goal or outcome. Individuals with high levels of grit do not merely slave away at their tasks or responsibilities but take a long-term view of their work and align their activities accordingly. In Jensen&rsquo;s case, for example, he knew that he had to work 17-hour shifts every day in order to achieve his outcomes and he did it with a smile. How are the daily activities that you are busy with helping you achieve your goals? Some of these activities may be fun, some may be difficult or tedious, but as long as they are aligned to what you want to achieve, you will experience a lot of contentment when you engage in them.\r\n&nbsp;\r\n&nbsp;\r\nGoal-orientated\r\nAs mentioned earlier, grit is all about perseverance and endurance. Individuals who have high levels of grit are like marathon runners who have the finish line in mind, but also know that they need to run at a persistent pace to reach the finish line. Being a good marathon runner is not necessarily about being talented in running, but rather about being headstrong, consistent in your training and staying focused on your goals. The difference between somebody who succeeds and somebody who is just spending a lot of time doing things is this: Practice must have purpose. Do not spend nine hours at work every day working on someone else&rsquo;s goals and come home and spend no time working on your own goals. Write down your goals, nurture them and spend time every day working toward them.\r\nResilience\r\nIndividuals who have high levels of grit are typically very resilient. Resilience has to do with the ability to bounce back from difficult circumstances, coping with pressures and dealing with ambiguity. Psychologists describe resilient individuals as being hardy, having the courage to grow from stressful or even traumatizing situations and being aware of the things that they can and those which they cannot control. Research has shown that resilience is ordinary, not extraordinary, and that it can be developed in anyone. A key trait that resilient individuals have is that they know the extent of their resilience and where their emotional cups can be refilled. To enhance your resilience, you should invest in mutually satisfactory relationships. This provides a support system. You must keep in mind that change is a part of life and that you need to be proactive and decisive about your future and to look for opportunities for self-discovery.\r\nExcellence\r\nMost individuals who have high levels of grit do not seek perfection, but rather strive for excellence. Perfectionists tend to be pedantic, stubborn, demanding and a real pain to be around. Perfectionism could lead to anxiety disorders, low self-esteem and even obsessive-compulsive disorder. Excellence is far more forgiving. It allows for mistakes and prioritizes progress over perfection. Excellence and grit have to do with an attitude of engaging in activities because you enjoy them, and it is an opportunity for you to explore, develop and optimise your talents and capabilities, rather than to prove a point to someone or yourself.\r\nPeople with grit are flexible, proactive and goal-orientated. They enjoy life, have positive relationships, are self-aware and never say no to a challenge.\r\nHow gritty are you?\r\n&nbsp;\r\nDr Marais Bester is a registered occupational psychologist who works for PSI Innovative HR Solutions in Dubai. As a consultant, speaker and author, he works across Africa and the Middle East to help people optimise their careers. Find him on:\r\ntwitter: @marais_bester;\r\nLinkedIn-linkedin.com/in/maraisbester/;\r\nWebsite-www.innovative-hr.com\r\nThis article was published in Esteem Psychology Magazine Oct - Dec 2018 issue.&nbsp;'),
(233, 'en', 'team', '29', 'https://www.innovative-hr.com/about/team#saeed-al-yaqoub', 'Saeed Al Yaqoub- Business Development Manager', 'Born in the United States to Saudi parents, Saeed was raised in Saudi Arabia to represent the best of both his cultures, returning to the US from the age of 11 to complete his education.\r\n&nbsp;Saeed holds a Bachelor&rsquo;s Degree in Entrepreneurship and Business Management with a minor in Social Sciences from the University of North Texas. While completing his studies, Saeed ran the Saudi Student organization in his university. Working together with over 120 members, the Saudi Student Organization represented the Kingdom&rsquo;s culture as well as empowered the organization to become a contributing member of the International Student Associations community within the southern states; winning the &lsquo;Best International Student Organization&rsquo; award in the year of 2011-2012.\r\nWhile in university; Saeed had the great opportunity to work with several multinational organizations and businesses which added to his knowledge and experience in the behaviors and norms of working with successful international teams, enabling him to become a finalist in the 2015 Northwestern Mutual Integrated Business Case Competition where students apply their business knowledge and practical skills.\r\nUpon graduating in 2015, Saeed returned to the Kingdom of Saudi Arabia to begin work at a reputable family construction company. During that time, Saeed gained a lot of practical experience; his role ranged from coordinating HR and Finance activities, managing projects,&nbsp;setting up new business meetings with prospective clients, as has been liaising as the Client Relations manager on all existing projects.\r\nSaeed&rsquo;s role within IHS will be spear-heading our Saudi&nbsp;office&nbsp;and bringing our world class tools and consultancy to the country to support the Kings 2030 Vision.'),
(130, 'en', 'clients', '22', 'https://middleeast.psionline.com/about/team#simon-tither', 'Simon Tither - Organisational Psychologist', 'Simon is originally from New Zealand but brings a global perspective to&nbsp; PSI Middle East, formerly Innovative HR Solutions, having previously lived in the Gulf and spent time working in Europe. He holds a Masters Degree in Applied Psychology (Industrial and Organisational) as well as a Bachelors Degree majoring in Psychology and Geography. His Master&rsquo;s research focused on the Employee Value Proposition put forward by organisations and the effect of this EVP on organizational outcomes such as job satisfaction and employee motivation.\r\nA background in the social sciences and organizational psychology, he has experience primarily working in the coordination of centres and talent assessment as well as facilitation and support for workshops, trend analysis and assessment design. Simon has experience in administration, interpretation and feedback of psychometric tools, and is a qualified test user for tools such as but not limited to the full Saville suite and Psytech GeneSys suite.&nbsp;'),
(232, 'en', 'team', '27', 'https://www.innovative-hr.com/about/team#madeleine-anne-yorke-harling', 'Madeleine Anne Yorke Harling - L&D Co-ordinator', 'Madeleine Anne Yorke Harling has joined the team as an Organisational Psychologist in training. Currently studying for her Masters, with a BSc in Psychology from the University of Kent, Madeleine has been managing assessment and development centres, coaching projects and recently assessing.\r\nMadeleine is certified in BPS Assessor Skills, BPS Centre Manager, BPS Qualification in Occupational Psychology- Level A and P as well as Strong Interest Inventory.&nbsp;\r\nMadeleine\'s background is a mix of her British heritage while growing up in Saudi Arabia, Singapore, England and Dubai which has enabled her to respond well&nbsp;to our diverse range of clients in the region. Madeleine can often be found liaising with and scheduling candidates for large centres as well as facilitating and assessing graduate level candidates.'),
(131, 'en', 'clients', '23', 'https://middleeast.psionline.com/about/team#tracy-stodart', 'Tracy Stodart - Director of Psychometric Test Library ', 'Tracy joined PSI Middle East, formerly Innovative HR Solutions in 2003 and is both a Senior Psychologist and Director of our Psychometric Test Library. In this role she builds relationships with our Test Publishers, providing specialised and tailored solutions to meet the assessment and development needs of the Middle East region. Tracy is presently heavily involved in translating many of our most valid and reliable tools into Arabic and helping create local norm groups ensuring the best tools are available for our client base.&nbsp;&nbsp;\r\nTracy came to the UAE in 2001 where she was appointed HR Manager for Presidential Flight based in Abu Dhabi.&nbsp; This role entailed the provision of specialist advice regarding a wide range of HR issues, with a key focus on assessment, performance management training and development.&nbsp; Tracy has held HR management roles in the banking and aviation industry and she has considerable multi-cultural experience, having lived and worked in New Zealand, Hong Kong, Dubai and Papua New Guinea.&nbsp; Whilst working with United Nations, she travelled extensively to various UN outstations to deliver customised training solutions.&nbsp; When in New Zealand, Tracy worked&nbsp; for SHL as part of its management team where she gained comprehensive experience in the management, implementation and validation of assessment tools. One of her responsibilities was conducting Occupational Testing Qualification Workshops on behalf of SHL.&nbsp;\r\nTracy&nbsp; places a great deal of emphasis on the practical application of psychology in the world of work. Her approach incorporates a focus on cross cultural&nbsp; integration of individual\'s and teams within the workplace to achieve strong business results using the best tools on the market.'),
(230, 'en', 'team', '26', 'https://www.innovative-hr.com/about/team#waleed-mowafy', 'Waleed Mowafy- HR Talent Management Specialist ', 'Waleed has been a senior consultant since March 2009 where he has been working predominately in leadership development identifying areas of strength and areas for development and providing coaching and mentoring.&nbsp; He has also been instrumental in designing Personal Development Plans, designing training programmes and managing development and assessment centers.\r\nPrior to working with Innovative HR Solutions, Waleed worked with Mashreq Bank in the UAE for 4 years in a senior operations role managing a large team across 4 functional areas. It was here he developed a passion for developing talent and managing people.&nbsp;&nbsp;&nbsp;\r\nWaleed is originally from Egypt where he enjoyed a prestigious career with the Hilton, the leading hospitality group and the American Educational Services (7 years) where he taught languages (Arabic and English).'),
(231, 'en', 'team', '28', 'https://www.innovative-hr.com/about/team#majid-sharif', 'Majid Sharif - Finance Director', 'Majid is a Certified Public Accountant with career span of more than 17 years in accountancy. Majid has acquired wide range of experience in financial audits, business advisories, accountancy, finance and management. In the capacity as Head of finance of Innovative HR Solutions, he is responsible for all finance and administrative functions while ensuring the effective internal control system. Prior joining IHS team, he served IHS as an external audit partner for more than 8 years.\r\n&nbsp;\r\nMajid holds MBA degree in Finance and is a Member of Institute of Public Accountants of Australia (MIPA), Associate of Institute of Financial Accountants of UK (AFA) and Associate of Institute of Cost Accountants of India (ACMA). Majid has expertise in International Financial Reporting Standards (IFRS), International Standards on Auditing (ISA), Company Laws, Tax Laws and Anti Money Laundering Laws. His special interests include financial modeling, reporting, internal controls and corporate governance.'),
(132, 'en', 'clients', '25', 'https://www.innovative-hr.com/about/clients#mashreq', 'Mashreq', ''),
(240, 'en', 'news & events', '24', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/24/how-to-engage-high-potential', 'How to Engage High Potential', 'How To Engage High Potential\r\n&nbsp;\r\nFind out how innovative, immersive and multi-assessment solutions can support you to assess and develop your high potentials to drive behavioural change.\r\n&nbsp;\r\nDuring this 30 minute webinar, one of our Senior Consultants, Jordon Jones will share several case studies on how to create a multi assessment process designed to engage and develop high potentials.\r\nWe will explore:\r\n\r\nHow to identify your high potentials\r\nHow to use technology to engage your high potential talent pool\r\nHow to embed the learning\r\n\r\nSpeaker:\r\nJordon is a psychologist at the talent management consultancy a&amp;dc (Assessment &amp; Development Consultants). Alongside his MSc in Occupational and Organisational Psychology from the University of Surrey, Jordon is a member of the British Psychological Society (BPS), a registered specialist in psychometric test use (including Saville Wave, OPQ, MBTI, EIP and 15FQ+ among others).\r\n&nbsp;\r\n\r\nThursday 4th October 2018,&nbsp;\r\n\r\n15:00 - 15:30 GST\r\nWATCH IT HERE\r\n&nbsp;'),
(229, 'en', 'team', '24', 'https://www.innovative-hr.com/about/team#louise-kidd', 'Louise Kidd - Senior Organisational Psychologist ', 'An Organisational Psychologist by trade, Louise has 18 years experience in the field of psychology, from teaching to working with public and private sector organisations on a multitude of projects.&nbsp; In addition to working with clients on their leadership development programmes, Louise is an assessor and a coach to delegates on our British Psychological Society Qualification in Occupational Testing &ndash; Ability and Personality programme.&nbsp;\r\nA true psychometrician, Louise&rsquo;s expertise lies within evaluating and selecting appropriate tests for projects, administering and scoring assessments, interpreting these results and feeding sometimes difficult information back to Senior Executives, Board Executives and CEO&rsquo;s. Louise has extensive experience conducting assessment and development centres with clients ranging from IT &amp; Telecoms, Government Departments and Multinationals.\r\nLouise has vast experience in conducting interviews, whether competency based for recruitment purposes, or senior interviews to establish project requirements or to identify needs within the business. Louise is a skilled report writer and highly competent at feeding identified information back to participants. (BPS RQTU&nbsp; Registration Number 424144)&nbsp;'),
(134, 'en', 'clients', '27', 'https://www.innovative-hr.com/about/clients#jumeirah-group', 'Jumeirah Group', ''),
(135, 'en', 'clients', '28', 'https://www.innovative-hr.com/about/clients#tecom-group', 'Tecom Group', ''),
(227, 'en', 'team', '30', 'https://www.innovative-hr.com/about/team#marais-bester', 'Marais Bester - Senior Organisational Psychologist, Head of Education & Research', 'Marais is a Chartered and Registered Occupational Psychologist with a strong background in Talent Management gained over the Middle East and Africa over the past number of years. His personal assignments up to date include the successful delivery of multiple large-scale Talent Assessment and Talent Development projects across multiple geographies, disciplines and industries.&nbsp;\r\nIn the past Marais has worked with large multinational clients in the areas of Talent Acquisition, Employee Benefit Management, Employee Wellbeing, Change Management, Organisational Development, Talent Development, Talent Audits, Succession Planning, Coaching and Mentoring as well as Performance Management. He is a Chartered Occupational Psychologist registered with both the British Psychological Society and the Health Professions Council of South Africa and holds a Masters Degree in Occupational Psychology.&nbsp; Marais is passionate about assisting organisations to achieve their strategic outcomes by helping their employees optimise within their careers.'),
(219, 'en', 'clients', '29', 'https://middleeast.psionline.com/about/clients#gems-education', 'Gems Education', ''),
(226, 'en', 'team', '23', 'https://www.innovative-hr.com/about/team#tracy-stodart', 'Tracy Stodart - Director of Psychometric Test Library ', 'Tracy joined Innovative HR Solutions in 2003 and is both a Senior Psychologist and Director of our Psychometric Test Library. In this role she builds relationships with our Test Publishers, providing specialised and tailored solutions to meet the assessment and development needs of the Middle East region. Tracy is presently heavily involved in translating many of our most valid and reliable tools into Arabic and helping create local norm groups ensuring the best tools are available for our client base.&nbsp;&nbsp;\r\nTracy came to the UAE in 2001 where she was appointed HR Manager for Presidential Flight based in Abu Dhabi.&nbsp; This role entailed the provision of specialist advice regarding a wide range of HR issues, with a key focus on assessment, performance management training and development.&nbsp; Tracy has held HR management roles in the banking and aviation industry and she has considerable multi-cultural experience, having lived and worked in New Zealand, Hong Kong, Dubai and Papua New Guinea.&nbsp; Whilst working with United Nations, she travelled extensively to various UN outstations to deliver customised training solutions.&nbsp; When in New Zealand, Tracy worked&nbsp; for SHL as part of its management team where she gained comprehensive experience in the management, implementation and validation of assessment tools. One of her responsibilities was conducting Occupational Testing Qualification Workshops on behalf of SHL.&nbsp;\r\nTracy&nbsp; places a great deal of emphasis on the practical application of psychology in the world of work. Her approach incorporates a focus on cross cultural&nbsp; integration of individual\'s and teams within the workplace to achieve strong business results using the best tools on the market.'),
(137, 'en', 'clients', '30', 'https://www.innovative-hr.com/about/team#marais-bester', 'Marais Bester - Senior Organisational Psychologist, Head of Education & Research', 'Marais is a Chartered and Registered Occupational Psychologist with a strong background in Talent Management gained over the Middle East and Africa over the past number of years. His personal assignments up to date include the successful delivery of multiple large-scale Talent Assessment and Talent Development projects across multiple geographies, disciplines and industries.&nbsp;\r\nIn the past Marais has worked with large multinational clients in the areas of Talent Acquisition, Employee Benefit Management, Employee Wellbeing, Change Management, Organisational Development, Talent Development, Talent Audits, Succession Planning, Coaching and Mentoring as well as Performance Management. He is a Chartered Occupational Psychologist registered with both the British Psychological Society and the Health Professions Council of South Africa and holds a Masters Degree in Occupational Psychology.&nbsp; Marais is passionate about assisting organisations to achieve their strategic outcomes by helping their employees optimise within their careers.'),
(239, 'en', 'news & events', '23', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/23/achieving-peak-performance-in-challenging-times', 'Achieving Peak Performance in Challenging Times ', 'Achieving Peak Performance in Challenging Times&nbsp;\r\n&nbsp;\r\nIn these challenging economic times many staff feel under pressure whether it be because of more demanding targets, their own job maybe under threat or generally being asked to do more with less.\r\n&nbsp;\r\nMental toughness is the quality, which determines in some part how people deal with these challenges, stressors, and the ever increasing demands of organisational life in challenging and turbulent times!.\r\nIf you can get your staff to continue to perform even when under severe pressure then it could put you ahead of your competitors.\r\nThis webinar is intended to create an understanding of what stressors are and how people and groups can best deal with them.\r\nWe will explore:\r\n\r\nWhat causes one person to succumb and another to thrive in the same circumstances?\r\nCan we identify people&rsquo;s strengths and weaknesses in these areas?\r\nCan we develop individuals to enable them to handle stressors like change, pressure and challenge more effectively?\r\nHow can we support individuals better with their specific needs?\r\n\r\nSpeaker:\r\nSimon Boag is a Senior Occupational Psychologist and Certified Executive Coach with a passion for Leadership Development. With close to 20 years consultancy experience at an international level he has built an excellent track record of working with diverse client groups using a strong client-focused approach in talent assessment and leadership development.\r\n&nbsp;\r\nThursday, 30th August 2018&nbsp;&nbsp;10:30am - 11:15am GST\r\nWATCH NOW\r\n&nbsp;'),
(138, 'en', 'clients', '31', 'https://www.innovative-hr.com/about/clients#aujan-group-holding', 'Aujan Group Holding', ''),
(139, 'en', 'clients', '32', 'https://www.innovative-hr.com/about/clients#dubai-world-trade-centre', 'Dubai World Trade Centre', ''),
(140, 'en', 'clients', '33', 'https://www.innovative-hr.com/about/clients#emaar', 'Emaar', ''),
(141, 'en', 'clients', '34', 'https://www.innovative-hr.com/about/clients#dubai-customs', 'Dubai Customs', ''),
(142, 'en', 'clients', '35', 'https://www.innovative-hr.com/about/clients#dubai-courts', 'Dubai Courts', ''),
(143, 'en', 'clients', '36', 'https://www.innovative-hr.com/about/clients#government-of-dubai', 'Government of Dubai', ''),
(144, 'en', 'clients', '37', 'https://www.innovative-hr.com/about/clients#ministry-of-labour', 'Ministry of Labour', ''),
(145, 'en', 'clients', '38', 'https://www.innovative-hr.com/about/clients#mod-ksa', 'MOD KSA', ''),
(146, 'en', 'clients', '39', 'https://www.innovative-hr.com/about/clients#atkins-group', 'Atkins Group', ''),
(147, 'en', 'clients', '40', 'https://www.innovative-hr.com/about/clients#mondelez-international', 'Mondelez International', ''),
(148, 'en', 'clients', '41', 'https://www.innovative-hr.com/about/clients#the-coca-cola-company', 'The Coca-Cola Company', ''),
(149, 'en', 'clients', '42', 'https://www.innovative-hr.com/about/clients#nestle', 'Nestle', ''),
(150, 'en', 'clients', '43', 'https://www.innovative-hr.com/about/clients#the-boston-consulting-group', 'The Boston Consulting Group', ''),
(151, 'en', 'clients', '44', 'https://www.innovative-hr.com/about/clients#aggreko', 'Aggreko', ''),
(153, 'en', 'clients', '46', 'https://www.innovative-hr.com/about/clients#al-ain-distribution-company', 'Al Ain Distribution Company', ''),
(157, 'en', 'clients', '50', 'https://middleeast.psionline.com/about/clients#aecom', 'Aecom', ''),
(158, 'en', 'clients', '51', 'https://middleeast.psionline.com/about/clients#adnoc', 'Adnoc', ''),
(159, 'en', 'clients', '52', 'https://www.innovative-hr.com/about/clients#dusup', 'Dusup', ''),
(160, 'en', 'clients', '53', 'https://www.innovative-hr.com/about/clients#enoc', 'Enoc', ''),
(162, 'en', 'clients', '55', 'https://www.innovative-hr.com/about/clients#emarat', 'Emarat', ''),
(163, 'en', 'clients', '56', 'https://www.innovative-hr.com/about/clients#ma-aden', 'Ma aden', ''),
(164, 'en', 'clients', '57', 'https://www.innovative-hr.com/about/clients#ndc', 'NDC', ''),
(165, 'en', 'clients', '58', 'https://www.innovative-hr.com/about/clients#npcc', 'NPCC', ''),
(166, 'en', 'clients', '59', 'https://www.innovative-hr.com/about/clients#qatar-foundation', 'Qatar Foundation', ''),
(167, 'en', 'clients', '60', 'https://www.innovative-hr.com/about/clients#qatargas', 'Qatargas', ''),
(168, 'en', 'clients', '61', 'https://www.innovative-hr.com/about/clients#qatar-petroleum', 'Qatar Petroleum', ''),
(169, 'en', 'clients', '62', 'https://www.innovative-hr.com/about/clients#merck', 'Merck', ''),
(170, 'en', 'clients', '63', 'https://www.innovative-hr.com/about/clients#novo-nordisk', 'Novo Nordisk', ''),
(171, 'en', 'clients', '64', 'https://www.innovative-hr.com/about/clients#roche', 'Roche', ''),
(172, 'en', 'clients', '65', 'https://www.innovative-hr.com/about/clients#wyeth', 'Wyeth', ''),
(173, 'en', 'clients', '66', 'https://www.innovative-hr.com/about/clients#astrazeneca', 'AstraZeneca', ''),
(174, 'en', 'clients', '67', 'https://www.innovative-hr.com/about/clients#aspetar', 'Aspetar', ''),
(175, 'en', 'clients', '68', 'https://www.innovative-hr.com/about/clients#king-faisal-specialist-hospital-research-centre', 'King Faisal Specialist Hospital & Research Centre', ''),
(176, 'en', 'clients', '69', 'https://www.innovative-hr.com/about/clients#lilly', 'Lilly', ''),
(177, 'en', 'clients', '70', 'https://www.innovative-hr.com/about/clients#hospira', 'Hospira', ''),
(178, 'en', 'clients', '71', 'https://www.innovative-hr.com/about/clients#etisalat', 'Etisalat', ''),
(179, 'en', 'clients', '72', 'https://www.innovative-hr.com/about/clients#ooredoo', 'Ooredoo', ''),
(180, 'en', 'clients', '73', 'https://www.innovative-hr.com/about/clients#du', 'Du', ''),
(181, 'en', 'clients', '74', 'https://www.innovative-hr.com/about/clients#roads-transport-authority', 'Roads & Transport Authority', ''),
(182, 'en', 'clients', '75', 'https://www.innovative-hr.com/about/clients#dubai-taxi-corporation', 'Dubai Taxi Corporation', ''),
(243, 'en', 'clients', '76', 'https://middleeast.psionline.com/about/clients#national-bank-of-abu-dhabi', 'FAB First Abu Dhabi Bank', ''),
(184, 'en', 'careers', '1', 'https://www.innovative-hr.com/careers#senior-consultant-occupational-psychologist', 'Senior Consultant Occupational Psychologist', 'Responsibilities&nbsp;\r\n\r\nAssist in leading and coordinating Assessment and Development Centres\r\nDeliver accredited training programmes in the use of psychometric assessments\r\nAssist in the delivery of other consulting projects across our consultancy services (IDPs, 360 assessments, executive coaching, leading and behaviourial skills training etc.)\r\nAccount Manage key clients, coordinating all elements of delivery and maintaining the ongoing client relationship.\r\nSupport&nbsp;in the development of new products or services.\r\nRepresent&nbsp;PSI&nbsp;in public forums as required (eg. events, seminars etc.)\r\n\r\nQualifications and Experience&nbsp;\r\n\r\nMasters in Organisational Psychology or related psychology discipline.\r\nRegistered as Psychologist in Home Country.\r\n5-8 years in assessment role.\r\nQualified on one of the major personality profiles.\r\nSignificant experience giving feedback on profiles and of assessment/development centre data.\r\nAbility to work extended hours on large assessment centre project as required.\r\nAbility to work with a multicultural client base and travel throughout the GCC.\r\n\r\nCompetencies\r\n\r\nStrong communication Skills (Written and Oral)\r\nPlanning &amp; Organising\r\nCommitment to Achieve\r\nProblem Solving &amp; Decision Making\r\nAdaptability\r\nPositive, high energy outlook\r\nCustomer Commitment\r\n\r\nTo apply contact us at&nbsp;PSI Middle East'),
(185, 'en', 'team', '4', 'https://www.innovative-hr.com/about/team#amanda-white', 'Amanda White - Managing Director', 'Amanda is the Managing Director of Innovative HR Solutions and her passion lies in the design of leadership assessment and development programmes, organisation cultural change, succession planning and talent management. Her personal assignments have seen her working across the GCC in sectors as diverse as government, oil and gas and engineering to hospitality and agriculture.\r\nPrior to coming to the region, Amanda established and ran a consulting business in the UK and wider Europe providing services largely to the Citi investment banking and professional services sector for 12 years, before its acquisition by New York Stock-Listed Company RR Donnelley &amp; Sons (RRD) in 2004.\r\nToday Amanda is a regular guest on Dubai Eye Radio lending her human capital insights to current global affairs. She has formerly held positions on the Executive Committee of the Recruitment and Employment Confederation (UK) as well as a member of the Bank of England City Advisory Committee from 2001 to 2008. Her MBTI Type is ENTP.'),
(186, 'en', 'team', '7', 'https://www.innovative-hr.com/about/team#sharan-gohel', 'Sharan Gohel - Director of Consulting Services', 'Sharan Gohel leads the Innovative HR Solutions consulting team and psychometric test library, managing a vast array of assessment and development initiatives across a broad spectrum of clients and industry-sectors. Her key expertise lies in the application of psychology to assessment and development, Executive Coaching and Organisational Development such as change management, cultural analysis and performance management.\r\nSharan has previously worked in the UK in the telecommunications and manufacturing industries as an in-house Occupational Psychologist. She also worked for the UK Government focusing on the assessment and development of individuals, teams and organisational development. In this capacity Sharan conducted an extensive research project looking at organisational culture and gender before developing an online entrepreneurial assessment tool.\r\nSharan is an Associate Fellow of the British Psychological Society (AFBPsS), Registered Practitioner with the Health Professionals Council (HPC), Member of the Society of Coaching Psychology, Full Member of the Division of Occupational Psychology, Member of The Psychological Testing Centre (PTC). Her MBTI Type is ENTP.'),
(187, 'en', 'team', '14', 'https://www.innovative-hr.com/about/team#abi-krimeed-white', 'Abi Krimeed (White) - Principal Consultant,  Learning Solutions ', 'Abi is a highly-experienced Senior Learning and Development Specialist and training practitioner, with a strong background in designing and facilitating leadership development workshops, assessing on Assessment and Development Centres and developing Human Capital solutions. She has led our development activities as both architect of the programmes we run, conducting train the trainer programmes to ensure consistency across both our English and Arabic speaking Facilitators and undertaking a role in the ongoing quality assessment.\r\nShe is an ICF Certified Coach (International Coaching Federation), a Qualified Assessment Centre Designer, Manager and Assessor, a Licensed Facilitator of Stephen Covey&rsquo;s work, a Certified Facilitator of Emotional Intelligence Leadership Programme and a Licensed Facilitator of Myers Briggs. She has facilitated customised sessions across a broad range of programmes including our Mastering the Leadership Code 5-day Modular Programme, Coaching Skills for Senior Managers, Emotional Intelligence, Team Work, Collaboration, Managing Change, Leading High Performing Teams and Communication.\r\nAbi has previously worked across various industries working with the Jumeirah Hotel Group and Dubai Holding (TECOM Investments). Her role as Director of Performance, Talent and Training for TECOM Investments involved designing the TECOM Succession Planning Framework and running organisation-wide Development Centres across the leadership team. Her MBTI type is INTP.'),
(188, 'en', 'team', '5', 'https://www.innovative-hr.com/about/team#humaira-anwer', 'Humaira Anwer - Senior Occupational Psychologist', 'Humaira is an experienced Occupational Psychologist with a passion for developing Competency Frameworks and developing customised personality psychometrics, Leadership Assessment and Development programmes and designing and running 360 degree feedback programmes. Humaira has worked with West Midlands Police (UK), Accenture Consulting, the London Home Office and Ministry of Defence in various specialist human capital consulting roles. For the Ministry of Defence Humaira led a project within the Defence Medical Services, mapping the Defence Leadership Framework with the NHS Leadership Framework and providing training solutions to bridge the competency gaps.\r\nShe holds a Masters in Work Psychology and Business, Masters in Human Resource Management, Bachelor in Psychology, Chartered Member of the British Psychological Society, Registered Psychologist with the Health and Care Professions Council, Trained Career Coach; Career Counselling Services. Humaira is an Associate Fellow of the British Psychological Society and is a member of the International Society for Coaching Psychology (ISCP). Member of the British Psychological Society, Registered Psychologist with the Health and Care Professions Council, Level A and B Occupational Testing Accredited, Trained Career Coach; Career Counselling Services and Trained Assessor &amp; Centre Manager. Her MBTI type is ESFJ.'),
(189, 'en', 'team', '6', 'https://www.innovative-hr.com/about/team#jonathan-rook', 'Jonathan Rook - Senior Occupational Psychologist', 'Jonathan is a Chartered Occupational Psychologist with a broad based human capital consulting experience working across public and private sectors, including Central Government, Financial Services, Retail, Pharmaceuticals and Telecommunications. Aside from his in-depth experience across the GCC, he has worked in UK, Portugal, Switzerland and India.\r\nHis particular passion lies in psychometric test development, talent analytics, organisational change management and HR consulting with key experience in selection and assessment, leadership development, executive assessment, job analysis &amp; competency development, career counselling and coaching.\r\nJonathan is an Associate Fellow of the British Psychological Society, Full Member of the Division of Occupational Psychology, holds a Certificate in Coaching with the Centre for Coaching, member of the International Society for Coaching Psychology (ISCP) and Registered Psychologist with the Health Professions Council.\r\nJonathan&rsquo;s publications include; &lsquo;Enabling Organisational Culture Change within Aviva: Case Study&rsquo;, British Psychological Society 2010, Division of Occupational Psychology Annual Conference; Zijlstra, F.R.H. &amp; Rook. J.W. (2005) &lsquo;The Weekly Cycle of Work and Rest&rsquo;; R.A. Roe, M.J. Waller and S. Clegg (Eds.) &lsquo;Time in organizations&rsquo; &ndash; Approaches and methods, London: Routledge; Rook, J.W. &amp; Zijlstra, F.R.H. &lsquo;The contribution of various types of activities to recovery&rsquo;, European Journal of Work and Organisational Psychology (2005), Vol. 15 (2); &lsquo;Defining Moments&rsquo;, The Psychologist (2002), Vol.15 (11), Letter to the Editor; &lsquo;Stress &amp; Recovery: A review of the literature&rsquo;, IPCD (2015);and most recently &lsquo;The Cost of Organizational Stress&rsquo;, CEO Magazine, June 2016, Opinion piece.'),
(190, 'en', 'team', '9', 'https://www.innovative-hr.com/about/team#abrar-al-mubarak', 'Abrar Al Mubarak - Senior Consultant', 'Abrar is originally from Saudi Arabia and is bilingual English and Arabic. She is a licensed ICF Coach and a trained Assessor and Centre Manager and participates across both our English and Arabic programmes. Today she has leveraged her skills and learning and become integral in the development of our Arabic products and services, and project managing our Saudi-based projects. She has worked on various translations of psychometric and exercise tools in Arabic and is currently leading the development of a major career interest tool for the Saudi Government.\r\nAbrar holds a Bachelor&rsquo;s Degree in Mass Communication and Political Science with a minor in Psychology, gained internationally in 3 Universities across 3 countries; a programme designed to promote a global perspective. Her first year was in Bangalore, India (1 year), then Christ Church, New Zealand (2 years) before completing the programme in Pittsburgh, in the United States.\r\nShe commenced her career as a Reporter with the Saudi Gazette Newspaper whilst representing her country as an Ambassador and Mediator between the University and The Saudi Arabian Cultural Mission. Abrar then worked with ELS Education Service. Her MBTI Type is ENTP.'),
(191, 'en', 'team', '20', 'https://www.innovative-hr.com/about/team#aref-al-mubarak', 'Aref Al Mubarak - Senior Executive Coach', 'Aref is a seasoned Senior Executive Coach and Team Facilitator who has enjoyed a long and varied career across many different verticals before choosing coaching and learning and development as a career. Aref actually began his professional life in a technical operations capacity in Quality Assurance and then as an Aircraft Senior Production Planner before he joined the Training and Assessment Development Centre team with Saudi Airlines, were he was from 1982 &ndash; 2009. As a member of the Training and Assessment Team he was involved in conducting Assessment Centres, Training Needs Analysis, creating IDPs and Coaching. It was then here that he decided to pursue his love for personal and professional development of both himself and others by leaving to become a full time Senior-Level Executive Coach. Aref holds an Organisation Developer Certificate (ODC), is an International Certified Trainer (HRDA) and PCC Certified Executive Coach.'),
(228, 'en', 'team', '25', 'https://www.innovative-hr.com/about/team#inas-farid', 'Inas Farid- Senior Leadership Development Specialist', 'Inas has more than 20 years&rsquo; experience and been a senior management consultant since 2009.&nbsp; Her assignments include working extensively as an Assessor on Assessment and Development centres, conducting employee interviews and psychometric assessments for public and private sector clients including Dubai Roads and Transport Authority, Dubai Prime Ministers&rsquo; Office, Abu Dhabi Centre of Excellence, UAE Ministry of Health, Al Ain Municipality, Executive Directors of the Abu Dhabi government, Pfizer, MBC, Jaguar&amp; Land Cruiser, Dubai World Central.&nbsp; More recently Inas was involved in one of our largest projects to date with Al Jazeera where she was part of team who assessed in both English and Arabic and provided feedback to over 300 people.\r\nInas commenced her career in the UAE in recruitment and talent management with Emirates Airline from 1997 &ndash; 2004 a prestigious role during a period of substantial growth and change for the airline before being approached to become HR Manager for Nakeel.&nbsp; Here her role again was managing the core functions of the company&rsquo;s significant recruitment drive during a period of unprecedented growth.&nbsp; This included oversight of manpower planning, running assessment centres, taking a lead in Employer Branding and establishing HR policy.'),
(193, 'en', 'team', '18', 'https://www.innovative-hr.com/about/team#joe-bruce', 'Joe Bruce - Senior Occupational Psychologist', 'Joe is an Occupational Psychologist specialising in leadership assessment, development, training and team facilitation. He has a wide range of experience with different tools and methodologies. Having now lived and worked in the UAE for seven years, Joe has developed a good cultural understanding of the region, working with clients in Saudi Arabia, Qatar, Oman and Kuwait.\r\nHe joined Innovative HR Solutions in 2009 as a Consultant Psychologist and by 2012 he had been appointed Manager of the Psychometric Tools Library having built experience and expertise as a trainer and practitioner across a number of world-leading psychometric tools.\r\nIn 2014-2015 Joe spent a year working with Hay Group Middle East, where he gained experience of delivering a range of leadership development workshops as well as an opportunity to broaden his more generalist HR capabilities working on an organisation restructure. He has also built extensive experience in conducting behavioural observational assessment interviews at leadership level.\r\nOver the last few years Joe has designed and delivered a range of workshops using the MBTI, EQi and other tools to deliver effective learning to a range of audiences and for purposes including team building, leadership effectiveness and career enhancement. His MBTI is ENTP.'),
(194, 'en', 'team', '15', 'http://innovative-hr.com/about/team#jehan-tabet', 'Jehan Tabet - Marketing Manager', 'Jehan is pivotal in the event management of our complimentary Professional Development Breakfasts, MBTI Forums, International Psychology Conference Dubai and all conferences and exhibitions across the GCC. She oversees the PR, branding, communication and sponsorship activities across IHS including managing our digital community. She is a highly experienced Marketing Manager with a strong background in marketing communications, event management, social media marketing, public relations and stakeholder engagement.\r\nWorking across a diverse range of sectors including banking, education, hospitality, retail and government she has previously managed key campaigns for Apple, [U] by Kotex and Business Review Weekly. She is renowned for her work in the Australian Federal Government managing key high profile marketing and event campaigns for the previous Prime Minister of Australia, the Hon Kevin Rudd, and the Parliamentary Secretary for Disabilities and Children\'s Services, the Hon Bill Shorten MP (current Leader for the Australian Labour party). Since joining Innovative HR Solutions in 2014, Jehan has implemented a number of high profile and key marketing initiatives to positively build Innovative HR Solutions\' brand presence in the region. Her MBTI type is ESFP.'),
(195, 'en', 'team', '17', 'https://www.innovative-hr.com/about/team#suzanne-hart', 'Suzanne Hart - Planning and Project Support Manager', 'Suzanne has over 30 years international operational and business management experience, primarily in the Education and Hospitality sectors before coming to Dubai in 2014. Her strengths lie in her organisation skills and her ability to build client and staff relationships. Suzanne&rsquo;s role is often referred to as \"mission control&rdquo; in IHS! She oversees all project set ups and manages the ongoing logistics and co-ordination for all consulting projects ensuring that people and supporting resources are in place and we are able to deliver our services in line with our Values and Client Commitments.'),
(196, 'en', 'team', '16', 'https://www.innovative-hr.com/about/team#kristina-beggs', 'Kristina Beggs - Business Development Manager', 'Kristina leads our business development efforts to enhance clients navigation of our services, and build talent solutions with the input of our consulting team. She is passionate about client service and can be regularly seen preparing proposals and presentations, meeting with clients and ensuring our clients have the right solution and are delighted with the end result.\r\nShe began her career in the United Kingdom in a Big Data organisation working with PR/Marketing and Digital teams with some of the most prominent brands across multiple industries; including McLaren Automotive, Unilever, Danone Nutricia and Johnson &amp; Johnson. During these 7 years, Kristina developed client relations team from 2 people to over 25 while growing her personal portfolio of business to over $3m USD. Kristina has a keen interest in languages and cultures, reflective in her learning of Arabic since she has been in the region with IHS for three years. Her MBTI type is ESFJ.'),
(197, 'en', 'team', '10', 'https://www.innovative-hr.com/about/team#sam-day', 'Sam Day - Planning & Project Support', 'Sam comes from a background in logistics, administration and customer service and has brought these project management skills and experience to Innovative HR Solutions in our Psychometric Test Library. Sam is dedicated, creative and detail-focused and passionate about finding a win-win for both the business and customer.\r\nIn her role as Accreditation Programme Manager she liaises with our International Test Publishers and our prospective clients to ensure we are able to provide up-to-date information on accreditation and qualifications requirements, support and encouragement. Her responsibilities include planning of our course calendar across 3 countries, courseware development and certification process, as well as delegate registration and welfare. She also provides invaluable support to the Managing Director and Director of Operations overseeing operational requirements and managing ad hoc client projects across the GCC. Her MBTI type is ENTJ.'),
(198, 'en', 'team', '11', 'https://www.innovative-hr.com/about/team#joanna-rodrigues', 'Joanna Rodrigues - Project Support Coordinator', 'As Project Support Coordinator Joanna&rsquo;s role is to set up and coordinate all our Human Capital projects and Assessment and Development Centres activities. Working with Manager of Planning and Project Support to book resources, arrange venues and materials, through to ensuring individual reports are prepared on time and coaching session logs are maintained. Joanna first joined us in 2012 and has made a big difference as she goes about making sure everyone is in the right place at the right time! Joanna&rsquo;s background was previously in administration in the advertising sector in Dubai and Comtech Computer Academy in India. Her MBTI Type is ESTJ'),
(199, 'en', 'team', '12', 'https://www.innovative-hr.com/about/team#pushpa-mani', 'Pushpa Mani - Psychometric Test Library Manager', 'As Psychometric Test Library Manager, Pushpa has been instrumental in the growth and development of our test library product offering over the 15 years she has been working with Innovative HR Solutions.\r\nPushpa oversees our team of psychometric administrators and supports clients as they select the appropriate tools, exercises and norm groups and consider Accreditation Courses to support their talent programmes. She liaises with our International Test Publishers and manages over 10 online platforms. Pushpa is a source of tremendous support for all clients. Her MBTI Type is ESTJ.'),
(200, 'en', 'team', '13', 'https://www.innovative-hr.com/about/team#catherine-riya', 'Catherine Riya - Library Coordinator', 'Catherine&rsquo;s area of expertise lies in project management and stakeholder engagement in the HR field. She manages and supports a number of our human capital and assessment processes with clients and supports with psychometric tools and exercise selection and administration. She is an experienced trainer of new staff, devising training programmes for new employees and clients when they purchase new platforms for the first time. She liaises extensively with clients and candidates to ensure testing requirements have been met. Her MBTI Type is ISTJ.');
INSERT INTO `search_index` (`id`, `locale`, `type`, `type_id`, `url`, `title`, `content`) VALUES
(202, 'en', 'clients', '10', 'https://www.innovative-hr.com/about/team#sam-day', 'Sam Day - Planning & Project Support', 'Sam comes from a background in logistics, administration and customer service and has brought these project management skills and experience to Innovative HR Solutions in our Psychometric Test Library. Sam is dedicated, creative and detail-focused and passionate about finding a win-win for both the business and customer.\r\nIn her role as Accreditation Programme Manager she liaises with our International Test Publishers and our prospective clients to ensure we are able to provide up-to-date information on accreditation and qualifications requirements, support and encouragement. Her responsibilities include planning of our course calendar across 3 countries, courseware development and certification process, as well as delegate registration and welfare. She also provides invaluable support to the Managing Director and Director of Operations overseeing operational requirements and managing ad hoc client projects across the GCC. Her MBTI type is ENTJ.'),
(203, 'en', 'clients', '4', 'https://www.innovative-hr.com/about/team#amanda-white', 'Amanda White - Managing Director', 'Amanda is the Managing Director of&nbsp;PSI Middle East and her passion lies in the design of leadership assessment and development programmes, organisation cultural change, succession planning and talent management. Her personal assignments have seen her working across the GCC in sectors as diverse as government, oil and gas and engineering to hospitality and agriculture.\r\nPrior to coming to the region, Amanda established and ran a consulting business in the UK and wider Europe providing services largely to the Citi investment banking and professional services sector for 12 years, before its acquisition by New York Stock-Listed Company RR Donnelley &amp; Sons (RRD) in 2004.\r\nToday Amanda is a regular guest on Dubai Eye Radio lending her human capital insights to current global affairs. She has formerly held positions on the Executive Committee of the Recruitment and Employment Confederation (UK) as well as a member of the Bank of England City Advisory Committee from 2001 to 2008. Her MBTI Type is ENTP.'),
(204, 'en', 'clients', '7', 'https://www.innovative-hr.com/about/team#sharan-gohel', 'Sharan Gohel - Director of Consulting Services', 'Sharan Gohel leads the Innovative HR Solutions consulting team and psychometric test library, managing a vast array of assessment and development initiatives across a broad spectrum of clients and industry-sectors. Her key expertise lies in the application of psychology to assessment and development, Executive Coaching and Organisational Development such as change management, cultural analysis and performance management.\r\nSharan has previously worked in the UK in the telecommunications and manufacturing industries as an in-house Occupational Psychologist. She also worked for the UK Government focusing on the assessment and development of individuals, teams and organisational development. In this capacity Sharan conducted an extensive research project looking at organisational culture and gender before developing an online entrepreneurial assessment tool.\r\nSharan is an Associate Fellow of the British Psychological Society (AFBPsS), Registered Practitioner with the Health Professionals Council (HPC), Member of the Society of Coaching Psychology, Full Member of the Division of Occupational Psychology, Member of The Psychological Testing Centre (PTC). Her MBTI Type is ENTP.'),
(241, 'en', 'news & events', '25', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/25/snakes-in-suits-managing-the-dark-triad-in-the-workplace', 'Snakes in Suits - Managing the Dark Triad in the Workplace', 'Snakes in Suits - Managing the Dark Triad in the Workplace\r\nThis webinar will focus on exploring the conceptualisation and diagnostic criteria for the personality disorders/traits of narcissism, Machiavellianism, and psychopathy (the Dark Triad)\r\n\r\nThis webinar will show you:\r\n\r\n\r\n\r\nThe influence on Leadership the Dark Triad will have in the workplace\r\nThe impact on the organisational culture the Dark Triad wll have in the workplace\r\nPotential interventions for organisations who employ individuals with these personality disorders/traits will be explored\r\n\r\n\r\n\r\n\r\nSpeaker:\r\nMarais Bester is a Senior Chartered Occupational Psychologist with a passion for talent management. With many years of international experience, Marais has developed a&nbsp;excellent track record of working with Middle Eastern clients from&nbsp;a range of industries in the fields of talent assessment and talent optimisation. Marais has just completed his PhD focusing on career psychology where he explored the different psychological resources and psychological drivers that typically contribute to career success, career satisfaction and career wellbeing.\r\n\r\nTuesday 30th October 2018,&nbsp;\r\n\r\n14:00 - 15:00 GST\r\nWATCH NOW'),
(207, 'en', 'clients', '9', 'https://middleeast.psionline.com/about/team#abrar-al-mubarak', 'Abrar Al Mubarak - Senior Consultant', 'Abrar is originally from Saudi Arabia and is bilingual English and Arabic. She is a licensed ICF Coach and a trained Assessor and Centre Manager and participates across both our English and Arabic programmes. Today she has leveraged her skills and learning and become integral in the development of our Arabic products and services, and project managing our Saudi-based projects. She has worked on various translations of psychometric and exercise tools in Arabic and is currently leading the development of a major career interest tool for the Saudi Government.\r\nAbrar holds a Bachelor&rsquo;s Degree in Mass Communication and Political Science with a minor in Psychology, gained internationally in 3 Universities across 3 countries; a programme designed to promote a global perspective. Her first year was in Bangalore, India (1 year), then Christ Church, New Zealand (2 years) before completing the programme in Pittsburgh, in the United States.\r\nShe commenced her career as a Reporter with the Saudi Gazette Newspaper whilst representing her country as an Ambassador and Mediator between the University and The Saudi Arabian Cultural Mission. Abrar has set up and manages our Riyadh office and Saudi clients. Her MBTI Type is ENTP.'),
(225, 'en', 'courses', '22', 'https://www.innovative-hr.com/calendar/course/22/coaching-skills-for-line-managers-icf-recognised', 'Coaching Skills for Line Managers', 'The workshop provides an overview of coaching methodology and best practice, as well as an opportunity for delegates to practice and develop their skills as a coach, confident to tackle work and behavioural issues and help their coachees achieve set goals.\r\nFor Managers who want to:&bull; Gain commitment in times of change&bull; Motivate and support their team(s)&bull; Understand people&rsquo;s potential&bull; Dealing with difficult people, constructively and with empathy&bull; Dramatically enhance their own and other&rsquo;s performance\r\nCoaching can occur in the workplace informal one-to-one sessions such as Performance Reviews or informal &lsquo;on-the-spot&rsquo; sessions, which requires considerable human insight into how to get the best from your people. The workshop will equip your leaders with the know-how and passion to ensure your culture thrives.\r\nWe run this as a public course but this can be run as an in-house course for your teams! So feel free to get in touch to ask us more about this option on 04 390 2778.\r\nCCE credits are available for those&nbsp;delegates already an ICF recognised professional coach.&nbsp;\r\nCore Competence Hours Credits: 9\r\nResource Development Credits: 3\r\nResource Development includes training formerly called Personal Development, Business Development, or Other Skills and Tools.'),
(235, 'en', 'news & events', '20', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/20/webinar-why-assessment-is-crucial', 'Why Assessment is Crucial', 'PSI Innovative Human Resource Solutions is proud to announce the first of a series of webinars, bringing the latest Thought Leadership in Talent Management across the GCC. All webinars will be recorded and available on demand.&nbsp;\r\nJoin our live webinar to understand how assessments can help you predict employee performance to inform hiring decisions.&nbsp; How can we use assessments to identify high potential talent for Succession Planning, and finally leverage assessment insights to support talent and leadership development.&nbsp;This session will provide insight on the crucial role of assessment at each stage of the employee life cycle.\r\nBook your place now and understand how to implement assessment in your organisation today!&nbsp;\r\nLearning Points :\r\n\r\nOutlining the business case for assessments.\r\nExplain what assessment tools can help at each stage of the employee life cycle.\r\nIdentify the right assessment tools to answer critical talent capital questions.\r\n\r\nSpeaker:\r\nSimon Boag is a Senior Occupational Psychologist and Certified Executive Coach with a passion for Leadership Development. With close to 20 years consultancy experience at an international level he has built an excellent track record of working with diverse client groups using a strong client-focused approach in talent assessment and leadership development.\r\n&nbsp;\r\nThursday, 19th April 2018&nbsp;&nbsp;10:30am - 12:00pm (U.A.E)\r\nWATCH NOW'),
(209, 'en', 'clients', '11', 'https://www.innovative-hr.com/about/team#joanna-rodrigues', 'Joanna Rodrigues - Project Support Coordinator', 'As Project Support Coordinator Joanna&rsquo;s role is to set up and coordinate all our Human Capital projects and Assessment and Development Centres activities. Working with Manager of Planning and Project Support to book resources, arrange venues and materials, through to ensuring individual reports are prepared on time and coaching session logs are maintained. Joanna first joined us in 2012 and has made a big difference as she goes about making sure everyone is in the right place at the right time! Joanna&rsquo;s background was previously in administration in the advertising sector in Dubai and Comtech Computer Academy in India. Her MBTI Type is ESTJ'),
(210, 'en', 'clients', '12', 'https://middleeast.psionline.com/about/team#pushpa-mani', 'Pushpa Mani - Psychometric Test Library Manager', 'As Psychometric Test Library Manager, Pushpa has been instrumental in the growth and development of our test library product offering over the 17 years she has been working with PSI Middle East.\r\nPushpa oversees our team of psychometric administrators and supports clients as they select the appropriate tools, exercises and norm groups and consider Accreditation Courses to support their talent programmes. She liaises with our International Test Publishers and manages over 10 online platforms. Pushpa is a source of tremendous support for all clients. Her MBTI Type is ESTJ.'),
(211, 'en', 'clients', '13', 'https://www.innovative-hr.com/about/team#catherine-riya', 'Catherine Riya - Library Coordinator', 'Catherine&rsquo;s area of expertise lies in project management and stakeholder engagement in the HR field. She manages and supports a number of our human capital and assessment processes with clients and supports with psychometric tools and exercise selection and administration. She is an experienced trainer of new staff, devising training programmes for new employees and clients when they purchase new platforms for the first time. She liaises extensively with clients and candidates to ensure testing requirements have been met. Her MBTI Type is ISTJ.'),
(212, 'en', 'news & events', '16', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/16/ihs-brings-mbti-to-lean-in-pakistan-2017', 'Innovative HR Solutions Brings MBTI to Lean In Pakistan 2017', '\r\nDawood Global Foundation (DGF) and the British Deputy High Commission, Karachi partnered to host Lean In Pakistan Circle Luncheon 2017 powered by LADIESFUND for women professionals and entrepreneurs at the British Deputy High Commission, Karachi.\r\nThe Consultancy Team of Innovative HR Solutions was asked to fly to Karachi and to conduct an engaging and interactive Myers-Briggs MBTI session on &ldquo;How Understanding Your Personality Can Lead To Career Success.&rdquo;&nbsp;Leading these sessions were Senior Organisational Psychologists Sharan Gohel, Humaira Anwer, Liesl Connolly as well as Managing Director, Amanda White and Consultant Susie Kelt.\r\n&nbsp;&ldquo;Lean In&rdquo; which is the highly praised global movement by Sheryl Sandberg COO of Facebook, was introduced to Pakistan and endorsed by Sheryl last year and has since become the biggest Lean In Circle meeting in the MENA region and South Asia. More than 100 women professionals and entrepreneurs from leading multi-nationals, retail, medical, accountancy, legal sectors, NGOs and youth programmes gathered to form professional friendships, meet potential clients and build/strengthen their network for career advancement purposes.\r\nPresident of Dawood Global Foundation, Tara Uzra Dawood said, &ldquo;On behalf of Dawood Global Foundation, thank you for &ldquo;leaning in together&rdquo; with us. Our theme is strengthening women\'s inclusion in the work force and encouraging women to pursue and achieve their ambitions.\"\r\nThe funds generated from this event have contributed towards the charitable efforts of DGF for women advancement. To date, DGF has educated 1000 deserving girls in Karachi and 250 in Lagos, Nigeria, to become journalists through its award-winning Educate a Girl project.\r\n&nbsp;\r\nAbout LeanIn.Org&reg;\r\nLeanIn.Org is a nonprofit organization founded by Facebook COO Sheryl Sandberg to empower all women in their ambitions. Leanin.Org offers inspiration and support through an online community, free expert lectures and Lean in Circles, small peer groups whose members meet regularly to share and learn together\r\n&nbsp;\r\nAbout LADIESFUND&reg;\r\nLADIESFUND is Pakistan&rsquo;s leading platform for urban women professionals and SME entrepreneurs with over 12,000 in its network. DGF and BDHC previously held the LADIESFUND Women of Influence Luncheon January 2011, LADIESFUND Speed Networking Luncheon January 2014, LADIESFUND Star Speed Networking Luncheon January 2015 and LADIESFUND Lean In Networking Luncheon January 2016'),
(213, 'en', 'news & events', '17', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/17/ihs-raises-awareness-about-the-positive-impact-of-coaching-for-businesses-and-professionals', 'IHS raises awareness about the positive impact of coaching for businesses and professionals', 'Senior Organisational Psychologist and Executive Coach, Liesl Connolly of Innovative HR Solutions was invited as a Guest Speaker as part of the panel of professional coaching experts including Vice President of the International Coaching Federation (ICF) David Ribott at an event in&nbsp;the Psychology Department of Heriot Watt University on 15 May 2017.&nbsp;\r\nThe aim of the event was to raise awareness about the positive impact of coaching for businesses professionals and illustrate to students what it entails exactly to be a coach. The talks at the event ranged from the use of psychometric tools in business coaching to the connection between organisational psychology and coaching.\r\nCoaching in business is a growing profession in the region but with a lot of confusion about how it fits into organizations. Herriot Watt University and Liesl Connolly aof IHS aimed to bridge that gap for the public at this event.\r\nThe evening was extremely well received and we at Innovative HR Solutions would like to thank Herriot Watt University as well as the International Coaching Federation for hosting such a great event and look forward to more exciting events in the future.'),
(217, 'en', 'team', '22', 'https://www.innovative-hr.com/about/team#simon-tither', 'Simon Tither - Organisational Psychologist', 'Simon is originally from New Zealand but brings a global perspective to Innovative HR Solutions, having previously lived in the Gulf and spent time working in Europe. He holds a Masters Degree in Applied Psychology (Industrial and Organisational) as well as a Bachelors Degree majoring in Psychology and Geography. His Master&rsquo;s research focused on the Employee Value Proposition put forward by organisations and the effect of this EVP on organizational outcomes such as job satisfaction and employee motivation.\r\nA background in the social sciences and organizational psychology, he has experience primarily working in the coordination of centres and talent assessment as well as facilitation and support for workshops, trend analysis and assessment design. Simon has experience in administration, interpretation and feedback of psychometric tools, and is a qualified test user for tools such as but not limited to the full Saville suite and Psytech GeneSys suite.&nbsp;'),
(218, 'en', 'clients', '24', 'https://www.innovative-hr.com/about/team#louise-kidd', 'Louise Kidd - Senior Organisational Psychologist ', 'An Organisational Psychologist by trade, Louise has 18 years experience in the field of psychology, from teaching to working with public and private sector organisations on a multitude of projects.&nbsp; In addition to working with clients on their leadership development programmes, Louise is an assessor and a coach to delegates on our British Psychological Society Qualification in Occupational Testing &ndash; Ability and Personality programme.&nbsp;\r\nA true psychometrician, Louise&rsquo;s expertise lies within evaluating and selecting appropriate tests for projects, administering and scoring assessments, interpreting these results and feeding sometimes difficult information back to Senior Executives, Board Executives and CEO&rsquo;s. Louise has extensive experience conducting assessment and development centres with clients ranging from IT &amp; Telecoms, Government Departments and Multinationals.\r\nLouise has vast experience in conducting interviews, whether competency based for recruitment purposes, or senior interviews to establish project requirements or to identify needs within the business. Louise is a skilled report writer and highly competent at feeding identified information back to participants. (BPS RQTU&nbsp; Registration Number 424144)&nbsp;'),
(216, 'en', 'team', '21', 'https://www.innovative-hr.com/about/team#simon-boag', 'Simon Boag - Senior Business Psychologist', 'Simon is a Chartered Psychologist with over fifteen years consultancy experience at an international level (Europe, Middle East, Asia Pacific) with an excellent track record of working with diverse client groups using a strong client-focused approach. Simon has designed and delivered bespoke training and development programmes, providing high-impact solutions to international blue chip and public sector organisations and experienced in large scale bid processes, including PFI, and PPP.&nbsp;\r\nBefore joining the team at IHS, Simon held several Organizational Development roles for Technip FMC (2012 to 2017) including L&amp;D Manager for the Middle East Region, and PMO for the HR Integration in APAC Region. Prior to those assignments he was responsible to implement Graduate and Management Development programmes at NPCC, and was a lead assessor for the GSEC leadership development programme. A specialist in selection, assessment, coaching, leadership and talent management using inventories published by Saville, SHL, Hogan, Psytech and AQR.\r\nGraduated with a Bachelor&lsquo;s degree in Psychology from the University of Hull and a Master&rsquo;s degree in Occupational Psychology from the internationally acclaimed Institute of Work Psychology, University of Sheffield. A member of the British Psychological Society (BPS), Division of Occupational Psychology, special group in Coaching Psychology, and accredited to BPS level A and B standard in psychometric testing.&nbsp;'),
(214, 'en', 'news & events', '18', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/18/sme-awards-press-release', 'We are finalists for the Gulf Capital SME Awards 2017!', '&nbsp;PRESS RELEASE\r\n\r\n10th July 2017\r\nInnovative HR Solutions is delighted to announce we are a finalist again this year in the Gulf Capital SME Awards 2017; the&nbsp;RSA Customer Focus of the Year award as well as the&nbsp;People &amp; Culture of the Year award.\r\n&nbsp;\"We are very excited to be the finalists for the RSA Customer Focus of the Year award and the People &amp; Culture of the Year award 2017. We are passionate about what we do, and delighted to be recognised by MEED and RSA for the positive contributions we have made for our own people, as well as for the growth and development of the talent and teams in our client organisations,\"&nbsp; says Amanda White, Managing Director of Innovative HR Solutions.\r\nThe&nbsp;2017 Gulf Capital SME Awards will be bigger and more exciting than ever as it continues to recognise the best small and medium businesses in the UAE. The Gulf Capital SME Awards Programme recognises success, growth and innovation in business.\r\n&ldquo;This year, there are 56 more finalists than in the 2016 edition, when 47 were shortlisted in various categories. This means more and more SMEs are reporting growth and are getting on the radar for their business accomplishments and growth,&rdquo; says Karim el-Solh, co-founder and CEO of Gulf Capital, one of the largest alternative asset management firms in the Middle East and the headline sponsor of the awards. Winners will be announced on 11 October 2017 at The Westin Hotel, Dubai. For information and updates on the awards, please visit&nbsp;http://sme.meedevents.com\r\nFor event enquiries, please contact:\r\nBecky Crayman  Head of Awards, MEED P O Box 25960, 20th Floor, Al Thuraya Tower 1, Dubai Media City, Dubai&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Tel: 00971 (0) 4 8180314 | M&nbsp;: 00971 (0) 505590713 | E: becky.crayman@meed.com\r\nKristina Beggs Business Development Manager, Innovative HR Solutions PO Box 52271, Office, G05-08, Block 3, Ground Floor, Dubai Knowledge Village, Dubai&nbsp;&nbsp;&nbsp;  Tel: 00971 (0) 4 3902778 | M&nbsp;: 00971 (0) 551378245| E: kristina@innovative-hr.com'),
(220, 'en', 'courses', '19', 'https://www.innovative-hr.com/calendar/course/19/the-leadership-code', 'The Leadership Code', 'Leaders need to be able to develop a clear vision of the way forward, communicate that vision effectively, adapt their approach to the needs and motivations of others and drive results efficiently.&nbsp; Mastering the Leadership Code is built around Dave Ulrich&rsquo;s, Norm Smallwood&rsquo;s and Kate Sweetman&rsquo;s; &lsquo;The Leadership Code: Five Rules to Lead By&rsquo;. Drawing on decades of research experience, the authors conducted extensive interviews with a variety of respected CEOs, academics, experienced executives, seasoned consultants - and determined the same five essentials repeated again and again. &lsquo;Mastering the Leadership Code&rsquo; is a 5-part modular programme interspersed with a pre and post-assessment, Executive Coaching and psychometrics to deepen self-awareness. Flexed across two levels; First-Line to Mid-Managers and Senior Leaders, this programme allows us to bring out the best in&nbsp;the future of your organisation and people.&nbsp;'),
(221, 'en', 'courses', '20', 'https://www.innovative-hr.com/calendar/course/20/competency-based-interview-training', 'Competency Based Interview Training', 'This one day intensive workshop is designed for HR and Recruitment Specialists requiring formal training for interviewing. The course allows delegates to deepen their expertise in the purpose and best approach to competency based interviews (CBI), know how to develop and ask competency related questions, and how to appropriately use open and probing questions to gather STAR-E evidence. Topics covered on the workshop include but are not limited to, Frameworks, objective evaluations, evidence collation hiring recommendations and the importance of professional, positive impressions and expectations.'),
(222, 'en', 'courses', '21', 'https://www.innovative-hr.com/calendar/course/21/Strengthscope', 'Strengthscope', 'Strengthscope&reg; is the world&rsquo;s most complete and innovative strengths profiling system that helps energize peak performance at work. Underpinned by 10 years of testing and research, Strengthscope&reg; will help you to energize peak performance by releasing the power of employees&rsquo; strengths.&nbsp;\r\nStrengthscope&reg; is designed to be the first step in helping you optimize your strengths to improve your performance and engagement at work. When used as part of a strengths-based development program, the profiler and supporting tools and resources can significantly improve your performance, motivation and confidence, during good times and in the face of pressure.&nbsp;\r\nThe Strengthscope&reg; profile report provides information on:&nbsp;\r\n\r\nUnique strengths and how to optimize these to achieve exceptional results\r\nRisk areas to peak performance together with powerful ways to reduce the impact of these\r\nPositive ways of working that will improve confidence, motivation and success in any situation\r\nHow to strengthen relationships and work more effectively with people whose strengths are different from yours&nbsp;\r\n'),
(223, 'en', 'pages', '143', 'https://middleeast.psionline.com', 'Home Page 2', '\r\nWhat we do\r\nWe partner with organisations to identify, develop and energise talent, creating change throughbusiness psychology whilst being focused on commercial success.\r\n\r\n\r\n\r\n\r\n\r\n\r\nSelection\r\nWhy leave your selection decisions to chance?\r\n\r\n\r\n\r\n\r\n\r\n\r\nDevelopment\r\nHow can I take my people to the next level?\r\n\r\n\r\n\r\n\r\n\r\n\r\nAccreditation\r\nLet&rsquo;s bring it in-house!\r\n\r\n\r\n\r\n\r\n\r\n\r\nConsulting\r\nWhere do I start?\r\n\r\n\r\n\r\n'),
(224, 'en', 'pages', '144', 'https://www.innovative-hr.com/your-career-guidance-hero', 'Your Career Guidance Hero', 'Enable your students success!\r\nFor nearly 80 years, the Strong Interest Inventory&reg; assessment has provided time-tested, research-validated insights to help individuals in their search for a rich, fulfilling career.\r\nAs one of the most respected and widely used career planning instruments in the world, it has been used extensively in schools (14yrs+), Colleges, Universities and places of work of all sizes.\r\nThe Strong Interest Inventory is designed to help students to find a career that fits. How? By understanding their personality preferences and comparing their interests to the interests of people who happy and fulfilled in their jobs. The reports are fully interactive, enabling students to click and search over 1,000 positions* in the world today which are constantly being updated; from Web Designer to Dental Surgeon.\r\n*Special Unicorn is coming soon\r\nBenefits of the Strong Interest Inventory\r\n\r\nEmpowers students to discover their true interests so they can expand and explore various career options.\r\nDelivers user-friendly, fun, interactive reports allowing test takers to explore over 1,000 career options in more detail.\r\nHelps heighten self-awareness and a deeper understanding of individual strengths and blind spots.\r\n'),
(236, 'en', 'pages', '145', 'https://www.innovative-hr.com/assessment/specialised-assessment/safety-in-the-workplace/safety-quotient', 'Safety Quotient', 'Safety Quotient&trade; is a new and innovative employee risk&nbsp;assessment technology that measures 6&nbsp;unique personality traits directly connected to risk tolerance and unsafe behaviors. Most companies using Safety Quotient&trade; have reduced workplace incidents by more than 25%.\r\nYears of research into workplace safety have uncovered 6 personality traits that are directly correlated to employee safety:\r\n\r\nResistance\r\nIrritability\r\nDistractibility\r\nImpulsiveness\r\nAnxiousness\r\nThrill-Seeking\r\n\r\nThrough a 10-15 minute online assessment,&nbsp;Safety Quotient&trade;&nbsp;measures participants&rsquo; personality risk factors to predict which of your front-line employees are high-risk for these traits.\r\nThe employee risk assessment outputs a detailed two part report outlining strategies to improve the participants&rsquo;&nbsp;Safety Self-Awareness&nbsp;and allows managers to predict preventable incidents through personalized training and coaching.\r\nTARGET CATEGORY\r\nAll levels\r\nDOWNLOADS\r\nSafety Quotient Participant\r\nSafety Quotient&nbsp; Employer\r\n&nbsp;Safety Quotient Group Report\r\n&nbsp;Safety Culture Perception Survey\r\n&nbsp;'),
(237, 'en', 'news & events', '21', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/21/cultural-intelligence-the-art-of-understanding-emotions', 'Cultural Intelligence- The Art of Understanding Emotions', 'Topic: Cultural Intelligence- The Art of Understanding Emotions\r\nTime and Date:&nbsp;Sunday 10th June 2018 from 10:00 - 11:15 GST&nbsp;\r\nJoin our live webinar to understand how your capability to relate and work effectively across cultures may impact your success as a professional, leader and organisation. The webinar explores how we can use assessments to identify the level of cultural intelligence within our organisations and enhance our competitive advantage by celebrating our diversity.\r\n&nbsp;Learning Points :&nbsp;\r\n\r\nInsights on the key mistakes that we make when attempting to understand other cultures.\r\nThe business case for enhancing our cultural intelligence as organisations.\r\nIdentify the right assessment tools to answer critical culture-related questions.\r\n\r\n&nbsp;Speaker:&nbsp;\r\nMarais Bester is a Senior Chartered Occupational Psychologist with a passion for talent management. With many years of international experience, Marais has developed a&nbsp;excellent track record of working with Middle Eastern clients from&nbsp;a range of industries in the fields of talent assessment and talent optimisation. Marais has just completed his PhD focusing on career psychology where he explored the different psychological resources and psychological drivers that typically contribute to career success, career satisfaction and career wellbeing.\r\n&nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; WATCH HERE'),
(238, 'en', 'news & events', '22', 'https://www.innovative-hr.com/knowledge-centre/news-events/article/22/talent-analytics', 'Talent Analytics ', 'Topic:&nbsp;Talent Analytics\r\nTime and Date: Thursday 19th July 2018 from 10:00 - 11:15 GST&nbsp;\r\nA recent study by Deloitte found that only 10% of companies are using analytics to solve talent challenges and only 4% are using analytics to predict and forecast future talent needs and outcomes. The majority of companies (84%) that use data only for reporting are at a significant disadvantage as companies that use HR data analytically&nbsp;have higher than average stock returns, more effective HR practices and more credible HR teams.\r\n&nbsp;Learning Points :&nbsp;\r\n*&nbsp;&nbsp; &nbsp;Understand how and where to use analytics *&nbsp;&nbsp; &nbsp;Critique data that is presented *&nbsp;&nbsp; &nbsp;Build Business Cases for projects that involve analytics *&nbsp;&nbsp; &nbsp;Communicate data analysis to peers and clients&nbsp;\r\n&nbsp;Speaker:&nbsp;\r\nSimon Tither, an Occupational Psychologist&nbsp;holds a Masters Degree in Applied Psychology (Industrial and Organisational). Simon has gained a wide range of experience working across a number of sectors in the region, in analysing the data from Assessment and Development Centres to better advise his clients in their HR decisions and budget spend.\r\n&nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;WATCH HERE\r\n&nbsp;'),
(244, 'en', 'clients', '26', 'https://middleeast.psionline.com/about/clients#mastercard', 'Mastercard', '');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_tinified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `parent_id`, `name`, `lft`, `rgt`, `depth`, `logo`, `logo_tinified`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, NULL, 'Peter Mitchell', 1, 2, 0, 'uploads/images/testimonial/da4b9237bacccdf19c0760cab7aec4a8359010b0.png', 0, 1, 4, 4, '2017-11-04 06:20:45', '2017-11-04 06:27:24'),
(3, NULL, 'Joanna Campbell', 3, 4, 0, 'uploads/images/testimonial/77de68daecd823babbb58edb1c8e14d7106e83bb.png', 0, 1, 4, NULL, '2017-11-04 06:30:16', '2017-11-04 06:30:16'),
(4, NULL, 'Tanya D’Antignac', 5, 6, 0, 'uploads/images/testimonial/1b6453892473a467d07372d45eb05abc2031647a.png', 0, 1, 4, NULL, '2017-11-04 06:31:52', '2017-11-04 06:31:52'),
(5, NULL, 'Matthew Bryan Cullen', 7, 8, 0, 'uploads/images/testimonial/ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4.png', 0, 1, 4, NULL, '2017-11-04 06:32:41', '2017-11-04 06:32:41'),
(6, NULL, 'Richard Nicholl', 9, 10, 0, 'uploads/images/testimonial/c1dfd96eea8cc2b62785275bca38ac261256e278.png', 0, 1, 4, NULL, '2017-11-04 06:33:47', '2017-11-04 06:33:47'),
(7, NULL, 'Salma Ali Al Aghbari', 11, 12, 0, 'uploads/images/testimonial/902ba3cda1883801594b6e1b452790cc53948fda.png', 0, 1, 4, NULL, '2017-11-04 06:37:26', '2017-11-04 06:37:26'),
(8, NULL, 'Fiona Busse', 13, 14, 0, 'uploads/images/testimonial/fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f.png', 0, 1, 4, NULL, '2017-11-04 06:38:47', '2017-11-04 06:38:47'),
(9, NULL, 'Ammar AlMadani ', 15, 16, 0, 'uploads/images/testimonial/0ade7c2cf97f75d009975f4d720d1fa6c19f4897.png', 0, 1, 4, 2, '2017-11-04 06:39:48', '2018-05-23 03:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial_translations`
--

CREATE TABLE `testimonial_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `testimonial_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `testimonial_translations`
--

INSERT INTO `testimonial_translations` (`id`, `testimonial_id`, `locale`, `title`, `content`) VALUES
(2, 2, 'en', 'VP Talent & Organisational Development', '<p><em>Thank you once again for all the support you provided us in the implementation of the Management Assessment Centre. We know how much work goes on in the background and we really appreciate the dedication that you and the team put in to deliver.</em></p>'),
(3, 3, 'en', 'Head of Leadership Development', '<p><em>Overall this is a comprehensive, practical course, that is well facilitated by people who know what they are doing and wanted every participant to leave as confident and competent Centre Managers and Assessors.&nbsp; I really enjoyed it and look forward to applying what I have learnt.</em></p>'),
(4, 4, 'en', 'HR Director', '<p><em>You delivered the training perfectly- we now understand much more about ourselves, our attitude and our workstyles. I think this has really helped us understand one another and how we can interact so much better. You really listened to us and gave us applicable examples of how we can MBTI at work.</em></p>'),
(5, 5, 'en', 'Sport Psychology and Personal Development Facilitator', '<p><em>I enjoy communicating with you and your team at IHS. As my Aussie mates would say, &lsquo;you are onto it&rsquo; in my language you are professional, timely and proactive.</em></p>'),
(6, 6, 'en', 'Resource Director, Middle East', '<p><em>Just a quick note to say thank you for making this happen. All that attended the session have said how useful it was and that though they only had a limited time they got a lot out of the session.</em></p>'),
(7, 7, 'en', 'Section Manager - Performance and People Development', '<p>\"It has been a great pleasure and a real joy working with Innovative HR. A great journey from the first meeting which we have conducted at a very short notice to address a very urgent request until we have received the final analysis and recommendations.\"</p>\r\n<p>What I have personally loved, is the level of flexibility the team had in addressing any challenges or obstacles we faced during the time of the project. </p>'),
(8, 8, 'en', 'Director of Learning and Development', '<p><em>We were just so grateful for the amazing training!</em></p>\r\n<p><em>Personally I learnt so much and it was such a pleasure to spend the week learning from you. You were fantastic and so accommodating, flexible and above all so knowledgeable</em></p>'),
(9, 9, 'en', 'Career Development Officer ', '<p><em>Let me first say, THANK YOU for the amazing workshop. Everyone walked out of the room said the same thing &ldquo;WE LOVED IT&rdquo;. I have met a couple of the students yesterday on campus and they were saying that they know MBTI by heart, but the way it was presented was brilliant.</em></p>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `award_translations`
--
ALTER TABLE `award_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `award_translations_award_id_locale_unique` (`award_id`,`locale`),
  ADD KEY `award_translations_locale_index` (`locale`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category_translations`
--
ALTER TABLE `blog_category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_category_translations_category_id_locale_unique` (`category_id`,`locale`),
  ADD KEY `blog_category_translations_locale_index` (`locale`);

--
-- Indexes for table `casestudies`
--
ALTER TABLE `casestudies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `casestudies_category_id_foreign` (`category_id`),
  ADD KEY `casestudies_sector_id_foreign` (`sector_id`);

--
-- Indexes for table `casestudy_categories`
--
ALTER TABLE `casestudy_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casestudy_category_translations`
--
ALTER TABLE `casestudy_category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `casestudy_category_translations_category_id_locale_unique` (`category_id`,`locale`),
  ADD KEY `casestudy_category_translations_locale_index` (`locale`);

--
-- Indexes for table `casestudy_sectors`
--
ALTER TABLE `casestudy_sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casestudy_sector_translations`
--
ALTER TABLE `casestudy_sector_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `casestudy_sector_translations_sector_id_locale_unique` (`sector_id`,`locale`),
  ADD KEY `casestudy_sector_translations_locale_index` (`locale`);

--
-- Indexes for table `casestudy_translations`
--
ALTER TABLE `casestudy_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `casestudy_translations_casestudy_id_locale_unique` (`casestudy_id`,`locale`),
  ADD KEY `casestudy_translations_locale_index` (`locale`);

--
-- Indexes for table `category_post`
--
ALTER TABLE `category_post`
  ADD PRIMARY KEY (`category_id`,`post_id`),
  ADD KEY `category_post_post_id_foreign` (`post_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_translations`
--
ALTER TABLE `client_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_translations_client_id_locale_unique` (`client_id`,`locale`),
  ADD KEY `client_translations_locale_index` (`locale`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_dates`
--
ALTER TABLE `course_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_dates_course_id_foreign` (`course_id`);

--
-- Indexes for table `course_date_translations`
--
ALTER TABLE `course_date_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `course_date_translations_course_date_id_locale_unique` (`course_date_id`,`locale`),
  ADD KEY `course_date_translations_locale_index` (`locale`);

--
-- Indexes for table `course_page`
--
ALTER TABLE `course_page`
  ADD PRIMARY KEY (`course_id`,`page_id`),
  ADD KEY `course_page_page_id_foreign` (`page_id`);

--
-- Indexes for table `course_translations`
--
ALTER TABLE `course_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `course_translations_course_id_locale_unique` (`course_id`,`locale`),
  ADD KEY `course_translations_locale_index` (`locale`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_translations`
--
ALTER TABLE `custom_field_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `custom_field_translations_custom_field_id_locale_unique` (`custom_field_id`,`locale`),
  ADD KEY `custom_field_translations_locale_index` (`locale`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_translations`
--
ALTER TABLE `employee_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_translations_employee_id_locale_unique` (`employee_id`,`locale`),
  ADD KEY `employee_translations_locale_index` (`locale`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_dates`
--
ALTER TABLE `event_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_dates_event_id_foreign` (`event_id`);

--
-- Indexes for table `event_date_translations`
--
ALTER TABLE `event_date_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `event_date_translations_event_date_id_locale_unique` (`event_date_id`,`locale`);

--
-- Indexes for table `event_translations`
--
ALTER TABLE `event_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `event_translations_event_id_locale_unique` (`event_id`,`locale`),
  ADD KEY `event_translations_locale_index` (`locale`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_translations`
--
ALTER TABLE `job_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `job_translations_job_id_locale_unique` (`job_id`,`locale`),
  ADD KEY `job_translations_locale_index` (`locale`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_translations`
--
ALTER TABLE `location_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `location_translations_location_id_locale_unique` (`location_id`,`locale`),
  ADD KEY `location_translations_locale_index` (`locale`);

--
-- Indexes for table `logo_sliders`
--
ALTER TABLE `logo_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo_slider_translations`
--
ALTER TABLE `logo_slider_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `logo_slider_translations_logo_slider_id_locale_unique` (`logo_slider_id`,`locale`),
  ADD KEY `logo_slider_translations_locale_index` (`locale`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mediables`
--
ALTER TABLE `mediables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mediables_media_id_foreign` (`media_id`);

--
-- Indexes for table `mediable_translations`
--
ALTER TABLE `mediable_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mediable_translations_mediable_id_locale_unique` (`mediable_id`,`locale`),
  ADD KEY `mediable_translations_locale_index` (`locale`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_uri_unique` (`uri`);

--
-- Indexes for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  ADD KEY `page_translations_locale_index` (`locale`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_author_id_foreign` (`author_id`);

--
-- Indexes for table `post_translations`
--
ALTER TABLE `post_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_translations_post_id_locale_unique` (`post_id`,`locale`),
  ADD KEY `post_translations_locale_index` (`locale`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `search_index`
--
ALTER TABLE `search_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `search_index_locale_index` (`locale`),
  ADD KEY `search_index_type_index` (`type`),
  ADD KEY `search_index_type_id_index` (`type_id`),
  ADD KEY `search_index_title_index` (`title`);
ALTER TABLE `search_index` ADD FULLTEXT KEY `fulltext_content` (`content`);
ALTER TABLE `search_index` ADD FULLTEXT KEY `fulltext_title` (`title`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial_translations`
--
ALTER TABLE `testimonial_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `testimonial_translations_testimonial_id_locale_unique` (`testimonial_id`,`locale`),
  ADD KEY `testimonial_translations_locale_index` (`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `award_translations`
--
ALTER TABLE `award_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blog_category_translations`
--
ALTER TABLE `blog_category_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `casestudies`
--
ALTER TABLE `casestudies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `casestudy_categories`
--
ALTER TABLE `casestudy_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `casestudy_category_translations`
--
ALTER TABLE `casestudy_category_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `casestudy_sectors`
--
ALTER TABLE `casestudy_sectors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `casestudy_sector_translations`
--
ALTER TABLE `casestudy_sector_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `casestudy_translations`
--
ALTER TABLE `casestudy_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `client_translations`
--
ALTER TABLE `client_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `course_dates`
--
ALTER TABLE `course_dates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=380;

--
-- AUTO_INCREMENT for table `course_date_translations`
--
ALTER TABLE `course_date_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=422;

--
-- AUTO_INCREMENT for table `course_translations`
--
ALTER TABLE `course_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1344;

--
-- AUTO_INCREMENT for table `custom_field_translations`
--
ALTER TABLE `custom_field_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1386;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `employee_translations`
--
ALTER TABLE `employee_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `event_dates`
--
ALTER TABLE `event_dates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `event_date_translations`
--
ALTER TABLE `event_date_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `event_translations`
--
ALTER TABLE `event_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_translations`
--
ALTER TABLE `job_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `location_translations`
--
ALTER TABLE `location_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `logo_sliders`
--
ALTER TABLE `logo_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `logo_slider_translations`
--
ALTER TABLE `logo_slider_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=409;

--
-- AUTO_INCREMENT for table `mediables`
--
ALTER TABLE `mediables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=404;

--
-- AUTO_INCREMENT for table `mediable_translations`
--
ALTER TABLE `mediable_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=363;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `post_translations`
--
ALTER TABLE `post_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `search_index`
--
ALTER TABLE `search_index`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `testimonial_translations`
--
ALTER TABLE `testimonial_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `award_translations`
--
ALTER TABLE `award_translations`
  ADD CONSTRAINT `award_translations_award_id_foreign` FOREIGN KEY (`award_id`) REFERENCES `awards` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_category_translations`
--
ALTER TABLE `blog_category_translations`
  ADD CONSTRAINT `blog_category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `casestudies`
--
ALTER TABLE `casestudies`
  ADD CONSTRAINT `casestudies_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `casestudy_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `casestudies_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `casestudy_sectors` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `casestudy_category_translations`
--
ALTER TABLE `casestudy_category_translations`
  ADD CONSTRAINT `casestudy_category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `casestudy_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `casestudy_sector_translations`
--
ALTER TABLE `casestudy_sector_translations`
  ADD CONSTRAINT `casestudy_sector_translations_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `casestudy_sectors` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `casestudy_translations`
--
ALTER TABLE `casestudy_translations`
  ADD CONSTRAINT `casestudy_translations_casestudy_id_foreign` FOREIGN KEY (`casestudy_id`) REFERENCES `casestudies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_post`
--
ALTER TABLE `category_post`
  ADD CONSTRAINT `category_post_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `client_translations`
--
ALTER TABLE `client_translations`
  ADD CONSTRAINT `client_translations_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_dates`
--
ALTER TABLE `course_dates`
  ADD CONSTRAINT `course_dates_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_date_translations`
--
ALTER TABLE `course_date_translations`
  ADD CONSTRAINT `course_date_translations_course_date_id_foreign` FOREIGN KEY (`course_date_id`) REFERENCES `course_dates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_page`
--
ALTER TABLE `course_page`
  ADD CONSTRAINT `course_page_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_translations`
--
ALTER TABLE `course_translations`
  ADD CONSTRAINT `course_translations_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `custom_field_translations`
--
ALTER TABLE `custom_field_translations`
  ADD CONSTRAINT `custom_field_translations_custom_field_id_foreign` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_translations`
--
ALTER TABLE `employee_translations`
  ADD CONSTRAINT `employee_translations_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `event_dates`
--
ALTER TABLE `event_dates`
  ADD CONSTRAINT `event_dates_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `event_date_translations`
--
ALTER TABLE `event_date_translations`
  ADD CONSTRAINT `event_date_translations_event_date_id_foreign` FOREIGN KEY (`event_date_id`) REFERENCES `event_dates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `event_translations`
--
ALTER TABLE `event_translations`
  ADD CONSTRAINT `event_translations_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_translations`
--
ALTER TABLE `job_translations`
  ADD CONSTRAINT `job_translations_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `location_translations`
--
ALTER TABLE `location_translations`
  ADD CONSTRAINT `location_translations_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `logo_slider_translations`
--
ALTER TABLE `logo_slider_translations`
  ADD CONSTRAINT `logo_slider_translations_logo_slider_id_foreign` FOREIGN KEY (`logo_slider_id`) REFERENCES `logo_sliders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mediables`
--
ALTER TABLE `mediables`
  ADD CONSTRAINT `mediables_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mediable_translations`
--
ALTER TABLE `mediable_translations`
  ADD CONSTRAINT `mediable_translations_mediable_id_foreign` FOREIGN KEY (`mediable_id`) REFERENCES `mediables` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_translations`
--
ALTER TABLE `post_translations`
  ADD CONSTRAINT `post_translations_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `testimonial_translations`
--
ALTER TABLE `testimonial_translations`
  ADD CONSTRAINT `testimonial_translations_testimonial_id_foreign` FOREIGN KEY (`testimonial_id`) REFERENCES `testimonials` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
